library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;
use ieee.math_real.all;

entity tiny_chn_controller is
  generic (
    C_COMP_WIDTH : integer := 8
    );
  port (
    clk     : in std_logic;
    aresetn : in std_logic;

    -- CMD interface
    cmd_tvalid : out std_logic;
    cmd_tready : in  std_logic;
    cmd_tdata  : out std_logic_vector(40 downto 0);

    -- STS interface
    sts_tvalid : in std_logic;
    sts_tdata  : in std_logic_vector(3 downto 0);

    -- Unpacker configuration
    config_data  : out std_logic_vector(8 downto 0);
    config_wr    : out std_logic;
    config_ready : in  std_logic;

    -- Control / status
    control_length_reg : in  std_logic_vector(31 downto 0);
    base_reg           : in  std_logic_vector(31 downto 0);
    width_reg          : in  std_logic_vector(31 downto 0);
    block_reg          : in  std_logic_vector(31 downto 0);
    block_skip_reg     : in  std_logic_vector(31 downto 0);
    num_blocks_reg     : in  std_logic_vector(31 downto 0);
    offset_reg         : in  std_logic_vector(31 downto 0);
    status_reg_rd      : out std_logic_vector(31 downto 0);
    status_reg_wr      : in  std_logic_vector(31 downto 0);

    -- IRQ output
    irq_out : out std_logic
    );
end tiny_chn_controller;

architecture impl of tiny_chn_controller is
  type t_state is (S_IDLE, S_RUNNING, S_WAIT_COMPLETE, S_ERROR);

  -- Regs
  signal start_address : unsigned(31 downto 0);
  signal length_bytes  : std_logic_vector(22 downto 0);

  -- Control signals from state machine
  signal start_pulse   : std_logic;
  signal next_state    : t_state;
  signal en_cnt        : std_logic;
  signal clr_sts       : std_logic;
  signal reset_channel : std_logic;
  signal cmd_valid     : std_logic;

  -- Control signals to state machine
  signal cmd_handshake  : std_logic;
  signal sts_handshake  : std_logic;
  signal last_pixel     : std_logic;
  signal transfer_done  : std_logic;
  signal reset_complete : std_logic;

  signal sts_error_mask : std_logic_vector(1 downto 0);
  signal irq_reg        : std_logic_vector(1 downto 0);

  signal completion_tally : integer range -128 to 127;

  -- Bits in control registers
  signal depth           : std_logic_vector(7 downto 0);
  signal width           : std_logic_vector(19 downto 0);
  signal block_width     : std_logic_vector(11 downto 0);
  signal block_height    : std_logic_vector(11 downto 0);
  signal block_skip      : std_logic_vector(15 downto 0);
  signal block_skip_last : std_logic_vector(15 downto 0);
  signal num_blocks_x    : std_logic_vector(8 downto 0);
  signal num_blocks_y    : std_logic_vector(8 downto 0);
  signal length          : std_logic_vector(19 downto 0);
  signal start           : std_logic;
  signal irq_mask        : std_logic_vector(1 downto 0);
  signal irq_clear       : std_logic_vector(1 downto 0);

  signal sts_error           : std_logic;
  signal sts_error_pulse     : std_logic;
  signal transfer_done_pulse : std_logic;

  signal offset             : unsigned(2 downto 0);
  signal truncate_last_word : std_logic;

begin

  --------------------------------------------------------------------------------
  -- Registers
  --------------------------------------------------------------------------------
  width <= width_reg(19 downto 0);

  block_width  <= block_reg(11 downto 0);
  block_height <= block_reg(23 downto 12);
  depth        <= block_reg(31 downto 24);

  num_blocks_y <= num_blocks_reg(8 downto 0);
  num_blocks_x <= num_blocks_reg(17 downto 9);

  block_skip      <= block_skip_reg(15 downto 0);
  block_skip_last <= block_skip_reg(31 downto 16);

  start    <= control_length_reg(0);
  irq_mask <= control_length_reg(5 downto 4);
  length   <= control_length_reg(31 downto 12);

  status_reg_rd(0)            <= transfer_done;
  status_reg_rd(2 downto 1)   <= sts_error_mask;
  status_reg_rd(9 downto 8)   <= irq_reg;
  status_reg_rd(31 downto 10) <= (others => '0');

  irq_clear <= status_reg_wr(9 downto 8);

  --------------------------------------------------------------------------------
  -- Pulse generators
  --------------------------------------------------------------------------------
  b_start : block is
    signal start_reg         : std_logic;
    signal sts_error_reg     : std_logic;
    signal transfer_done_reg : std_logic;
  begin
    process (clk) is
    begin
      if (rising_edge(clk)) then
        if (aresetn = '0') then
          start_reg         <= '0';
          sts_error_reg     <= '0';
          transfer_done_reg <= '0';
        else
          start_reg         <= start;
          sts_error_reg     <= sts_error;
          transfer_done_reg <= transfer_done;
        end if;
      end if;
    end process;

    start_pulse         <= '1' when start_reg = '0' and start = '1'                 else '0';
    sts_error_pulse     <= '1' when sts_error_reg = '0' and sts_error = '1'         else '0';
    transfer_done_pulse <= '1' when transfer_done_reg = '0' and transfer_done = '1' else '0';
  end block b_start;

  --------------------------------------------------------------------------------
  -- IRQ generator
  --
  -- Latches corresponding bits in irq_reg when trigger events happen. Bits are
  -- cleared when the corresponding bit in irq_clear is set. The output signal,
  -- irq_out, is asserted whenever a bit in irq_reg is set and the
  -- corresponding bit in irq_mask is set.
  --------------------------------------------------------------------------------
  b_irq : block is
    signal irq_trigger : std_logic_vector(1 downto 0);
  begin

    irq_trigger(0) <= sts_error_pulse;
    irq_trigger(1) <= transfer_done_pulse;

    process (clk) is
    begin
      if (rising_edge(clk)) then
        if (aresetn = '0') then
          irq_reg <= (others => '0');
        else
          for i in 0 to irq_reg'high loop
            if (irq_trigger(i) = '1') then
              irq_reg(i) <= '1';
            elsif (irq_clear(i) = '1') then
              irq_reg(i) <= '0';
            end if;
          end loop;
        end if;
      end if;
    end process;

    -- The IRQ output is the OR of every bit in irq_reg anded with the
    -- corresponding mask bit
    process (irq_reg, irq_mask) is
      variable irq : std_logic;
    begin
      irq := '0';
      for i in 0 to irq_reg'high loop
        irq := irq or (irq_reg(i) and irq_mask(i));
      end loop;
      irq_out <= irq;
    end process;
  end block b_irq;

  --------------------------------------------------------------------------------
  -- Set up command word for TinyMover
  --------------------------------------------------------------------------------
  process (last_pixel, start_address, length_bytes, cmd_valid, cmd_tready) is
    alias cmd_saddr is cmd_tdata(31 downto 0);
    alias cmd_length is cmd_tdata(39 downto 32);
    alias cmd_last is cmd_tdata(40);
  begin
    cmd_saddr  <= std_logic_vector(start_address);
    cmd_length <= std_logic_vector(length_bytes(7 downto 0));
    cmd_last   <= last_pixel;

    cmd_tvalid    <= cmd_valid;
    cmd_handshake <= cmd_valid and cmd_tready;
  end process;

  --------------------------------------------------------------------------------
  -- State machine
  --------------------------------------------------------------------------------
  b_fsm : block is
    type t_state is (S_IDLE, S_RUNNING, S_WAIT_COMPLETE, S_ERROR);
    signal state      : t_state;
    signal next_state : t_state;
  begin
    process (state, start_pulse, cmd_handshake, config_ready, last_pixel,
             transfer_done, sts_error_pulse, completion_tally)
    begin
      cmd_valid     <= '0';
      next_state    <= state;
      clr_sts       <= '0';
      reset_channel <= '0';
      en_cnt        <= '0';
      config_wr     <= '0';

      case state is
        when S_RUNNING =>
          if (config_ready = '1') then
            cmd_valid <= '1';
          end if;

          if (cmd_handshake = '1') then
            config_wr <= '1';
            if (last_pixel = '1') then
              next_state <= S_WAIT_COMPLETE;
            else
              en_cnt <= '1';
            end if;
          end if;

        when S_IDLE =>
          if start_pulse = '1' then
            next_state <= S_RUNNING;
            clr_sts    <= '1';
          end if;

        when S_WAIT_COMPLETE =>
          if (transfer_done = '1') then
            next_state <= S_IDLE;
          end if;

        -- If we got an error in one of the status words, we will wait until we have
        -- gotten as many status words as the number of commands we have issued.
        when S_ERROR =>
          if (completion_tally = 0) then
            next_state <= S_IDLE;
          end if;
      end case;

      -- Always handle hard errors (assertion of channel error line), but errors
      -- in status words should not be handled if we are already in any of the
      -- states related to handling hard errors.
      if (sts_error_pulse = '1') then
        next_state <= S_ERROR;
      end if;
    end process;

    process (clk) is
    begin
      if rising_edge(clk) then
        if aresetn = '0' then
          state <= S_IDLE;
        else
          state <= next_state;
        end if;
      end if;
    end process;
  end block b_fsm;


  --------------------------------------------------------------------------------
  -- Component address generation for block and plane transfers
  --
  -- Input values (from register interface):
  --
  -- length          - the number of components per transfer
  -- depth           - the number of planes in the cube
  -- num_blocks_y/x  - number of blocks in y and x directions
  -- block_width     - block width in number of pixels
  -- block_height    - block height in number of pixels
  -- width           - width of image in number of components
  -- block_skip      - number of components to skip to get to the next block
  -- block_skip_last - number of components to skip to get to the next block when
  --                   currently in the last block in a row
  --
  --
  -- Internal variables/signals:
  --
  -- block_y, block_x   - used to keep track of the current block being transferred
  -- x, y               - used to keep track of the current pixel within the current
  --                      block (when doing a planewise transfer)
  -- block_address      - temporary register to keep track of the start address of
  --                      the current block
  -- row_address        - temporary register to keep track of the start of the
  --                      current row
  --------------------------------------------------------------------------------
  b_address_gen : block is
    signal block_y      : integer range 0 to 2**9-1;
    signal block_x      : integer range 0 to 2**9-1;
    signal y            : integer range 0 to 2**12-1;
    signal x            : integer range 0 to 2**12-1;
    signal comp_address : unsigned(31 downto 0);
  begin
    process (clk)
      variable row_address   : unsigned(31 downto 0);
      variable block_address : unsigned(31 downto 0);
    begin
      if (rising_edge(clk)) then
        if (aresetn = '0') then
          block_y       <= 0;
          block_x       <= 0;
          y             <= 0;
          x             <= 0;
          block_address := to_unsigned(0, 32);
          row_address   := to_unsigned(0, 32);
          comp_address  <= to_unsigned(0, 32);
        elsif (start_pulse = '1') then
          block_y       <= to_integer(unsigned(num_blocks_y));
          block_x       <= to_integer(unsigned(num_blocks_x));
          y             <= to_integer(unsigned(block_height));
          x             <= to_integer(unsigned(block_width));
          block_address := unsigned(offset_reg);
          row_address   := unsigned(offset_reg);
          comp_address  <= block_address;
        else
          if (en_cnt = '1') then

            -- Move to next block when x = 0 and y = 0
            if (y = 0 and x = 0) then
              if (block_x /= 0) then
                block_x       <= block_x - 1;
                block_address := block_address + unsigned(block_skip);
              else
                block_x       <= to_integer(unsigned(num_blocks_x));
                block_address := block_address + unsigned(block_skip_last);
                if (block_y /= 0) then
                  block_y <= block_y - 1;
                end if;
              end if;
            end if;

            -- Go through block
            if (x /= 0) then
              x            <= x - 1;
              comp_address <= comp_address + unsigned(depth);
            else
              x <= to_integer(unsigned(block_width));
              if (y /= 0) then
                y           <= y - 1;
                row_address := row_address + unsigned(width);
              else
                y           <= to_integer(unsigned(block_height));
                row_address := block_address;
              end if;
              comp_address <= row_address;
            end if;
          end if;
        end if;
      end if;
    end process;

    last_pixel <= '1' when block_y = 0 and block_x = 0 and x = 0 and y = 0 else '0';

    --------------------------------------------------------------------------------
    -- Convert component address to memory address, start offset and control signals
    --
    -- A component address has a unit of the selected component width. When
    -- converted to byte unit addresses, this results in a byte-level address (a
    -- regular memory address), and an offset within this byte.
    --
    -- The memory address is found by multiplying the component address by the
    -- component width and dividing by 8 bits. The offset is found by taking the
    -- remainder of this division.
    --------------------------------------------------------------------------------
    process (comp_address, base_reg)
      constant C_COMP_WIDTH_BITS : integer := integer(log2(real(C_COMP_WIDTH))) + 1;

      variable bit_address  : unsigned(31 + C_COMP_WIDTH_BITS downto 0);
      variable byte_address : unsigned(31 + C_COMP_WIDTH_BITS downto 0);
    begin
      bit_address   := comp_address * to_unsigned(C_COMP_WIDTH, C_COMP_WIDTH_BITS);
      byte_address  := unsigned(base_reg) + bit_address/8;
      start_address <= byte_address(31 downto 0);
      offset        <= bit_address(2 downto 0);
    end process;

    process (offset, length) is
      constant C_COMP_WIDTH_BITS : integer := integer(log2(real(C_COMP_WIDTH))) + 1;

      variable length_bits : unsigned(19 + C_COMP_WIDTH_BITS downto 0);
      variable length_temp : unsigned(19 + C_COMP_WIDTH_BITS downto 0);
    begin
      length_bits  := unsigned(length(19 downto 0)) * to_unsigned(C_COMP_WIDTH, C_COMP_WIDTH_BITS);
      length_temp  := offset + length_bits;
      length_bytes <= std_logic_vector((length_temp(22 downto 0) + 7)/8);

      -- Check if removing the offset reduces the number of words that are
      -- needed. If so, the unpacker must be informed of this so it can truncate
      -- the last word in the offset shifter.
      if ((length_temp(6 downto 0) + 63)/64 /= (length_bits(6 downto 0) + 63)/64) then
        truncate_last_word <= '1';
      else
        truncate_last_word <= '0';
      end if;
    end process;

    config_data <= truncate_last_word & length(4 downto 0) & std_logic_vector(offset(2 downto 1)) & last_pixel;
  end block b_address_gen;


  --------------------------------------------------------------------------------
  --
  -- Status word interface
  --
  -- In normal operation, the latest status received will be kept in
  -- the sts_reg register.
  --
  -- When an error status is received, that value is latched until clr_sts is
  -- asserted by the control logic.
  --
  --------------------------------------------------------------------------------
  b_sts : block is
    signal sts_reg : std_logic_vector(3 downto 0);
    alias sts_last is sts_reg(3);
    alias sts_slv_err is sts_reg(2);
    alias sts_dec_err is sts_reg(1);
    alias sts_okay is sts_reg(0);
  begin
    p_sts : process (clk)
    begin
      if (rising_edge(clk)) then
        if (aresetn = '0' or clr_sts = '1') then
          sts_reg <= (others => '0');
        elsif (sts_tvalid = '1') then
          -- Only overwrite status word register if no error in previous
          if (sts_error = '0') then
            sts_reg <= sts_tdata;
          end if;
        end if;
      end if;
    end process p_sts;

    sts_error      <= sts_slv_err or sts_dec_err;
    sts_error_mask <= sts_reg(2 downto 1);

    -- The STS interface is always ready, so a handshake occurs whenever valid
    -- is high
    sts_handshake <= sts_tvalid;

    -- We are done when we get an OKAY status word back with 1 as the tag
    transfer_done <= '1' when sts_last = '1' and sts_okay = '1' else '0';
  end block b_sts;

  --------------------------------------------------------------------------------
  -- Completion tally
  --
  -- Keeps track of the number of issued commands vs the number of received
  -- status words. When 0, the two are balanced.
  --------------------------------------------------------------------------------
  process (clk) is
    variable incr : integer range -1 to 1 := 0;
  begin
    if (rising_edge(clk)) then
      if (aresetn = '0' or clr_sts = '1') then
        completion_tally <= 0;
      else
        incr := 0;
        if (cmd_handshake = '1') then
          incr := 1;
        end if;
        if (sts_handshake = '1') then
          incr := incr - 1;
        end if;
        completion_tally <= completion_tally + incr;
      end if;
    end if;
  end process;
end impl;
