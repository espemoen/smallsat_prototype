load 'raw1.mat'
raw = floor(rescale(raw, 0, 4095));
filename = 'raw1.bin';
fileid = fopen(filename, 'wb');
fwrite(fileid, raw', 'int16');
fclose(fileid);

load 'raw2.mat'
raw = floor(rescale(raw, 0, 4095));
filename = 'raw2.bin';
fileid = fopen(filename, 'wb');
fwrite(fileid, raw', 'int16');
fclose(fileid);

load 'raw3.mat'
raw = floor(rescale(raw, 0, 4095));
filename = 'raw3.bin';
fileid = fopen(filename, 'wb');
fwrite(fileid, raw', 'int16');
fclose(fileid);

load 'raw4.mat'
raw = floor(rescale(raw, 0, 4095));
filename = 'raw4.bin';
fileid = fopen(filename, 'wb');
fwrite(fileid, raw', 'int16');
fclose(fileid);

%%

load 'raw_large.mat'
raw = rescale(raw, 0, 4095);
raw = floor(raw);
filename = 'raw_large.bin';
fileid = fopen(filename, 'wb');
fwrite(fileid, raw', 'int16');
fclose(fileid);

 %%
 load 'pigm_refl.mat';
 filename = 'ref_spectra.bin';
 fileid = fopen(filename, 'wb');
 fwrite(fileid, pigm_refl', 'float');
 fclose(fileid);

 load 'mean_refl.mat';
 filename = 'mean_spectra.bin';
 fileid = fopen(filename, 'wb');
 fwrite(fileid, mean_refl', 'float');
 fclose(fileid);
 
 %%
 filename = 'corrected1_2_17.bin';
 fileid = fopen(filename);
 a = fread(fileid, 'float');
 w = 52;
 h = 250000;
 corrected1_10_5 = reshape(a, [w h])';
 fclose(fileid);
 
 %%
 [corrected, P, M, G, K] = emsc(raw, pigm_refl, mean_refl, 2);
 
 
 %%
 filename = 'p1_new.bin';
 fileid = fopen(filename);
 a = fread(fileid, 'int64');
 w = 8;
 h = 250000;
 P_zynq = reshape(a, [w h])';
 
  %%
 filename = 'raw_large.bin';
 fileid = fopen(filename);
 a = fread(fileid, 'int16');
 w = 52;
 h = 250000;
 P_zynq = reshape(a, [w h])';
 