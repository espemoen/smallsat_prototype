function [corrected_rounded, P, G1] = emsc_rounded(raw ,ref_spectra, mean_spectra, mult)

K = ref_spectra;
m = mean_spectra;

nVars = size(raw, 2);
nObs = size(raw, 1);

wlens = linspace(0,1,nVars);
wlensSQ = wlens.^2;

M = [ones(1, nVars);
    wlens;
    wlensSQ
    K;
    m];

G =pinv(M);

r_G = floor(G*mult);
r_raw = floor(rescale(raw,0,4095));


G1 = r_G;
corrected_rounded = r_raw;
for idx = 1:nObs
    p = r_raw(idx,:)*r_G; %x 1000
    P(idx,:) = p/mult;   
    corrected_rounded(idx,:) = (r_raw(idx,:) - P(idx,1) - (P(idx,2)*wlens + P(idx,3)*wlensSQ)) / P(idx,end);
end

end

