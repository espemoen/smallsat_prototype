%K = bsxfun(@minus,ref_spectra(2:end,:), ref_spectra(1,:));
K = pigm_refl;
m = mean_refl;
nVars = size(raw, 2);
nObs = size(raw, 1);

wlens = linspace(0,1,nVars);
wlensSQ = wlens.^2;

M = [ones(1, nVars);
    wlens;
    wlensSQ
    K;
    m];


raw_s = rescale(raw, 0, 4095);

G_s =pinv(M);
G_s = floor(G_s * 2^17);

for idx = 1:nObs
    p_s = raw_s(idx,:)*G_s;
    P_s(idx,:) = p_s; 
end
P_s = P_zynq;
P_s = P_s/2^17;

corrected_s = raw_s;
for idx = 1:nObs
    corrected_s(idx,:) = (raw_s(idx,:) - P_s(idx,1) - P_s(idx,2)*wlens - P_s(idx,3)*wlensSQ ) / P_s(idx,end);
end

%%
%K = bsxfun(@minus,ref_spectra(2:end,:), ref_spectra(1,:));
K = pigm_refl;
m = mean_refl;
nVars = size(raw, 2);
nObs = size(raw, 1);

wlens = linspace(0,1,nVars);
wlensSQ = wlens.^2;

M = [ones(1, nVars);
    wlens;
    wlensSQ
    K;
    m];

G =pinv(M);

corrected = raw;
for idx = 1:nObs
    p = raw(idx,:)*G;
    P(idx,:) = p;   
    corrected(idx,:) = (raw(idx,:) - P(idx,1) - P(idx,2)*wlens - P(idx,3)*wlensSQ ) / P(idx,end);
end