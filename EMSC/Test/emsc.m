function [ corrected, P, M, G, K] = emsc(raw, ref_spectra, mean_spectra, mOrder)
%EMSC Summary of this function goes here
%   Detailed explanation goes here
%K = bsxfun(@minus,ref_spectra(2:end,:), ref_spectra(1,:));
% m = mean(ref_spectra);
K = ref_spectra;
m = mean_spectra;

nVars = size(raw, 2);
nObs = size(raw, 1);

wLens = zeros(mOrder, nVars);
wLens(1,:) = linspace(0,1,nVars);

for j = 2:mOrder
    wLens(j,:) = wLens(1,:).^j;
end

M = [ones(1, nVars);
    wLens;
    K;
    m];

%G = pinv(M);

G = M'*pinv(M*M');
%G = pinv(M);
P = zeros(nObs,size(M,1));

corrected = raw;
for idx = 1:nObs
    p = raw(idx,:)*G;                                       
    P(idx,:) = p;                      
    corrected(idx,:) = (raw(idx,:) - p(1:mOrder+1) *M(1:mOrder+1,:)) / p(end);
end

end