close all
clear all
load test_data.mat

figure
plot(test_data');
title('Raw');
axis tight


raw = test_data;
ref_spectra = test_data([3 93],:);
mean_spectra = mean(ref_spectra);
mOrder = 2;

corrected = emsc_simple(raw, ref_spectra, mean_spectra);

K = ref_spectra;
m = mean_spectra;

nVars = size(raw, 2);
nObs = size(raw, 1);

wlens = linspace(0,1,nVars);
wlensSQ = wlens.^2;

M = [ones(1, nVars);
    wlens;
    wlensSQ
    K;
    m];

G = M'*pinv(M*M');

corrected = raw;
for idx = 1:nObs
    p = raw(idx,:)*G;
    P(idx,:) = p;   
    corrected(idx,:) = (raw(idx,:) - p(1) - p(2)*wlens - p(3)*wlensSQ ) / p(end);
end

figure
plot(corrected');
title('emsc');
