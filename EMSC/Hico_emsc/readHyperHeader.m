function info = readHyperHeader(hdrfile)
% reads header file ".hdr"
% INPUT:
%   hdrfile - file name of header file
% OUTPUT:
%   info - structure containing info from header file

[path name ext] = fileparts(hdrfile);
current_dir = pwd;
if ~isempty(path)
    cd(path)
end
hdrfile = [name '.hdr'];
fid = fopen(hdrfile);
if fid > 0
    info.headerExists = 1;
    while fid;
        line = fgetl(fid);
        if line == -1
            break
        else
            eqsn = findstr(line,'=');
            if ~isempty(eqsn)
                param = strtrim(line(1:eqsn-1));
                param(findstr(param,' ')) = '_';
                param(findstr(param,'(')) = '_';
                param(findstr(param,')')) = '_';
                param(findstr(param,'-')) = '_';
                param(findstr(param,'/')) = '_';
                value = strtrim(line(eqsn+1:end));
                if isempty(str2num(value))
                    if ~isempty(findstr(value,'{')) && isempty(findstr(value,'}'))
                        while isempty(findstr(value,'}'))
                            line = fgetl(fid);
                            value = [value,strtrim(line)];
                        end
                    end
                    eval(['info.',param,' = ''',value,''';'])
                else
                    eval(['info.',param,' = ',value,';'])
                end
            end
        end
    end
    fclose(fid);
    %% Put wavelengths into an array:
    if isfield(info,'wavelength')
        lambda = sscanf(info.wavelength(2:end-1), '%f ,');
        info.wavelength = lambda;
    else
        info.wavelength = [];
    end
    if isfield(info,'data_gain_values')
        gain = sscanf(info.data_gain_values(2:end-1), '%f ,');
        info.data_gain_values = gain;
    else
        info.data_gain_values = [];
    end
    if isfield(info,'data_offset_values')
        doffset = sscanf(info.data_offset_values(2:end-1), '%f ,');
        info.data_offset_values = doffset;
    else
        info.data_gain_values = [];
    end
    if isfield(info,'default_bands')
        rgbBands = cell2mat(textscan(info.default_bands,'{%d8,%d8,%d8}'));
        info.default_bands = rgbBands;
    else
        info.default_bands = [];
    end
    if isfield(info,'description')
        description = info.description(2:end-1);
        info.description = description;
    else
        info.description = '';
    end
    %% Set binary format parameters
    switch info.byte_order
        case {0}
            info.byte_order = 'ieee-le';
        case {1}
            info.byte_order = 'ieee-be';
        otherwise
            info.byte_order = 'n';
    end
    switch info.data_type
        case {1}
            info.data_type = 'uint8';
        case {2}
            info.data_type= 'int16';
        case{3}
            info.data_type= 'int32';
        case {4}
            info.data_type= 'single';
        case {5}
            info.data_type= 'double';
        case {6}
            disp('>> Sorry, Complex (2x32 bits)data currently not supported');
            disp('>> Importing as double-precision instead');
            info.data_type= 'double';
        case {9}
            error('Sorry, double-precision complex (2x64 bits) data currently not supported');
        case {12}
            info.data_type= 'uint16';
        case {13}
            info.data_type= 'uint32';
        case {14}
            info.data_type= 'int64';
        case {15}
            info.data_type= 'uint64';
        otherwise
            error(['File type number: ',num2str(dtype),' not supported']);
    end
else
    info.headerExists = 0;
end
cd(current_dir)