function [ corrected ] = emsc_simple(raw, ref_spectra, mean_spectra)
%EMSC_JF Summary of this function goes here
%   Detailed explanation goes here

%K = bsxfun(@minus,ref_spectra(2:end,:), ref_spectra(1,:));
K = ref_spectra;
m = mean_spectra;

nVars = size(raw, 2);
nObs = size(raw, 1);

wlens = linspace(0,1,nVars);
wlensSQ = wlens.^2;

M = [ones(1, nVars);
    wlens;
    wlensSQ
    K;
    m];

G =pinv(M);

corrected = raw;
for idx = 1:nObs
    p = raw(idx,:)*G;
    P(idx,:) = p;   
    corrected(idx,:) = (raw(idx,:) - p(1) - p(2)*wlens - p(3)*wlensSQ ) / p(end);
end

end

