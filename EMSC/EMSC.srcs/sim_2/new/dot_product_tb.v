`timescale 1ns/1ps

module dot_product_tb;
    parameter RAW_BIT_WIDTH = 12;
    parameter G_BIT_WIDTH = 32;
    parameter PERIOD = 10;
    
    reg clk, en, reset_n;
    reg[RAW_BIT_WIDTH-1:0] in_raw;
    reg[G_BIT_WIDTH-1:0] in_G;
    reg[11:0] v_len;
    reg[31:0] in_raw_temp;
    
    wire p_rdy;
    wire[47:0] p;
    
    dot_product
        #(.bit_depth_raw(RAW_BIT_WIDTH),
          .bit_depth_G(G_BIT_WIDTH))
    DUT
         (.clk(clk),
          .en(en),
          .reset_n(reset_n),
          .in_raw(in_raw),
          .in_G(in_G),
          .v_len(v_len),
          .p_rdy(p_rdy),
          .p(p));
         
   always #(PERIOD/2) clk = ~clk;
   
   always @(in_raw_temp) in_raw <= in_raw_temp[11:0];
   
   integer          iter, i;
   integer          f_in_G, f_in_raw;
   
   
   
   
   
   initial begin
    clk <= 1'b0;
    reset_n <= 1'b0;
    en <= 1'b0;
    
    repeat(4) @(posedge clk);
    reset_n <= 1'b1;
    
    repeat(4) @(posedge clk);
    v_len <= 10;
    
    repeat(2) @(posedge clk);
    
    
    f_in_G = $fopen("D:/MasterOppgave/smallsat_prototype/EMSC/in_G.bin", "rb");
    f_in_raw = $fopen("D:/MasterOppgave/smallsat_prototype/EMSC/in_raw.bin", "rb");
    
    if (f_in_G == 0) begin
        $display("Failed to open input file %s", "D:/MasterOppgave/smallsat_prototype/EMSC/in_G.bin");
        $finish;
    end
    
    if (f_in_raw == 0) begin
        $display("Failed to open input file %s", "D:/MasterOppgave/smallsat_prototype/EMSC/in_raw.bin");
        $finish;
    end
    
    repeat(2) @(posedge clk);
    en <= 1'b1;
    
    for (iter = 0; iter < 103; iter = iter + 1) begin
        if ((iter >= 40) && (iter <= 43)) begin
            en <= 1'b0;
        end
        else
        begin
            en <= 1'b1;
            for( i = 0; i < 4; i = i + 1) begin
                in_G[i*8  +: 8] <= $fgetc(f_in_G);
                in_raw_temp[i*8  +: 8] <= $fgetc(f_in_raw);
            end
       end
        
    #10;
   end
    
   end
   
   
   
endmodule