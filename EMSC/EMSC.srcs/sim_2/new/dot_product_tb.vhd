library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;
-----------------------------------------------------------
entity dot_product_tb is

end entity dot_product_tb;
-----------------------------------------------------------
architecture testbench of dot_product_tb is

	-- Testbench DUT generics as constants
	constant bit_depth_raw 	: positive := 12;
	constant bit_depth_G	: positive := 25;

	-- Testbench DUT ports as signals
	signal clk 		:   std_logic;
	signal en 		:	std_logic;
	signal reset_n 	:   std_logic;
	signal in_raw	:   std_logic_vector(bit_depth_raw-1 downto 0);
	signal in_G		:   std_logic_vector(bit_depth_G-1 downto 0);
	signal v_len	:   integer range 0 to 400;
    signal p_rdy    :   std_logic;
	signal p        :   std_logic_vector(47 downto 0);
	-- Other constants
	constant C_CLK_PERIOD : real := 10.0e-9; -- NS
begin
	-----------------------------------------------------------
	-- Clocks and Reset
	-----------------------------------------------------------
	CLK_GEN : process
	begin
		clk <= '1';
		wait for C_CLK_PERIOD / 2.0 * (1 SEC);
		clk <= '0';
		wait for C_CLK_PERIOD / 2.0 * (1 SEC);
	end process CLK_GEN;

	RESET_GEN : process
	begin
		reset_n <= '0',
		         '1' after 15.0*C_CLK_PERIOD * (1 SEC);
		wait;
	end process RESET_GEN;
	-----------------------------------------------------------
	-- Testbench Stimulus
	-----------------------------------------------------------
process-- en
begin
    for i in 0 to 14 loop
        wait until rising_edge(clk);
    end loop;
    en <= '1';
end process;

process-- v_len
begin
    for i in 0 to 9 loop
        wait until rising_edge(clk);
    end loop;
    v_len <= 8;
end process;


process-- RAW
    begin
        for i in 0 to 14 loop
            wait until rising_edge(clk);
        end loop;
        in_raw <=  std_logic_vector(to_unsigned(1, in_raw'length));
        wait until rising_edge(clk);
        in_raw <=  std_logic_vector(to_unsigned(200, in_raw'length));
        wait until rising_edge(clk);
        in_raw <=  std_logic_vector(to_unsigned(112, in_raw'length));
        wait until rising_edge(clk);
        in_raw <=  std_logic_vector(to_unsigned(421, in_raw'length));
        wait until rising_edge(clk);
        in_raw <=  std_logic_vector(to_unsigned(721, in_raw'length));
        wait until rising_edge(clk);
        in_raw <=  std_logic_vector(to_unsigned(109, in_raw'length));
        wait until rising_edge(clk);
        in_raw <=  std_logic_vector(to_unsigned(417, in_raw'length));
        wait until rising_edge(clk);
        in_raw <=  std_logic_vector(to_unsigned(611, in_raw'length));
        for i in 0 to 99 loop
            wait until rising_edge(clk);
        end loop;
    end process;

process-- G
    begin
        for i in 0 to 14 loop
            wait until rising_edge(clk);
        end loop;
        in_G   <=  std_logic_vector(to_unsigned(1, in_G'length));
        wait until rising_edge(clk);
        in_G   <=  std_logic_vector(to_unsigned(456, in_G'length));
        wait until rising_edge(clk);
        in_G   <=  std_logic_vector(to_unsigned(941, in_G'length));
        wait until rising_edge(clk);
        in_G   <=  std_logic_vector(to_unsigned(321, in_G'length));
        wait until rising_edge(clk);
        in_G   <=  std_logic_vector(to_unsigned(123, in_G'length));
        wait until rising_edge(clk);
        in_G   <=  std_logic_vector(to_unsigned(456, in_G'length));
        wait until rising_edge(clk);
        in_G   <=  std_logic_vector(to_unsigned(941, in_G'length));
        wait until rising_edge(clk);
        in_G   <=  std_logic_vector(to_unsigned(321, in_G'length));
        for i in 0 to 99 loop
            wait until rising_edge(clk);
        end loop;
    end process;
	-----------------------------------------------------------
	-- Entity Under Test
	-----------------------------------------------------------
    dp: entity work.dot_product
        generic map(
            bit_depth_raw => bit_depth_raw,
            bit_depth_G   => bit_depth_G
        )
        port map(
            clk         =>         	clk,
            en 		    => 			en,
            reset_n     =>         	reset_n,
            in_raw      =>        	in_raw,
            in_G        =>        	in_G,
            v_len		=>			v_len,
            p_rdy       =>          p_rdy,
            p           =>        	p
        );

end architecture testbench;