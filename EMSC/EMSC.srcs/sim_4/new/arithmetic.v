`timescale 1ns/1ps

module arithmetic_tb;
    parameter PERIOD = 10;
    parameter RAW_BIT_WIDTH = 32;
    parameter P_BIT_WIDTH = 48;
    parameter WLENS_BIT_WIDTH = 16;
    parameter CORRECTED_BIT_WIDTH = 64;
    
    reg CLK, aresetn, CE, valid_input;
    reg[31:0] in_raw;
    reg[(4*48)-1:0] P;
    reg[(2*16)-1:0] WLENS;
    
    wire[63:0] OUT_RES;
    wire valid_output;
    
    arithmetic
        #(.RAW_BIT_WIDTH(RAW_BIT_WIDTH),
          .P_BIT_WIDTH(P_BIT_WIDTH),
          .WLENS_BIT_WIDTH(WLENS_BIT_WIDTH),
          .CORRECTED_BIT_WIDTH(CORRECTED_BIT_WIDTH))
    DUT(.CLK(CLK),
        .aresetn(aresetn),
        .valid_input(valid_input),
        .in_raw(in_raw),
        .P(P),
        .CE(CE),
        .WLENS(WLENS),
        .valid_output(valid_output),
        .OUT_RES(OUT_RES)    
    );
    
    always #(PERIOD/2) CLK = ~CLK;
    
    
    initial begin
        CLK <= 1'b0;
        aresetn <= 1'b0;
        CE <= 1'b0;
        in_raw <= 0;
        P[47:0] <= 0;
        P[95:48] <= 0;
        P[143:96] <= 0;
        P[191:144] <= 0;
        WLENS[15:0] <= 0;
        WLENS[31:16] <= 0;
        valid_input <= 1'b0;
        
        repeat(4) @(posedge CLK);
        aresetn <= 1'b1;
        CE <= 1'b1;
        
        in_raw <= 100;
        P[47:0] <= 2;
        P[95:48] <= 3;
        P[143:96] <= 2;
        P[191:144] <= 1;
        WLENS[15:0] <= 1;
        WLENS[31:16] <= 2;
        valid_input <= 1'b1;
        #10
        in_raw <= 59;
        P[47:0] <= 2;
        P[95:48] <= 3;
        P[143:96] <= 2;
        P[191:144] <= 1;
        WLENS[15:0] <= 1;
        WLENS[31:16] <= 2;
        valid_input <= 1'b0;
        #10
        in_raw <= 59;
        P[47:0] <= 2;
        P[95:48] <= 3;
        P[143:96] <= 2;
        P[191:144] <= 1;
        WLENS[15:0] <= 1;
        WLENS[31:16] <= 2;
        valid_input <= 1'b1;
        #10
        valid_input <= 1'b0;
        repeat(200) @(posedge CLK);   
    end
endmodule