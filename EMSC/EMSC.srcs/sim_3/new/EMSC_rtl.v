`timescale 1ns/1ps

module EMSC_rtl_tb;
    parameter B_RAM_SIZE = 52;
    parameter B_RAM_BIT_WIDTH = 32;
    parameter RAW_BIT_WIDTH = 16;
    parameter NUM_B_RAM = 8;
    parameter P_BIT_WIDTH = 48;
    parameter PERIOD = 10;
    
    reg clk, aresetn, init, valid_input, enable;
    reg[11:0] G_size;
    reg[B_RAM_BIT_WIDTH-1:0] in_G;
    reg[RAW_BIT_WIDTH-1:0] in_raw;
        
    wire p_rdy;
    wire[NUM_B_RAM*P_BIT_WIDTH-1:0] p_out;
    wire[P_BIT_WIDTH-1:0] corrected;
    
    reg[31:0] in_raw_temp;
    
    EMSC_rtl
        #(.B_RAM_SIZE(B_RAM_SIZE),
          .B_RAM_BIT_WIDTH(B_RAM_BIT_WIDTH),
          .RAW_BIT_WIDTH(RAW_BIT_WIDTH),
          .NUM_B_RAM(NUM_B_RAM),
          .P_BIT_WIDTH(P_BIT_WIDTH)
        )
    DUT
        (.clk(clk),
         .aresetn(aresetn),
         .init(init),
         .valid_input(valid_input),
         .enable(enable),
         .G_size(G_size),
         .in_G(in_G),
         .in_raw(in_raw),
         .p_rdy(p_rdy),
         //.p_out(p_out),
         .corrected(corrected)
        );
    
    always #(PERIOD/2) clk = ~clk;

    always @(in_raw_temp) in_raw <= in_raw_temp[11:0];
    
    integer          iter, i;
    integer          f_in_G, f_in_raw, f_out;
    
    initial begin
        clk <= 1'b0;
        aresetn <= 1'b0;
        init <= 1'b0;
        valid_input <= 1'b0;
        in_raw_temp <= 32'b0;
        enable <= 1'b0;
        G_size <= 0;
        in_G <= 0;
        in_raw <= 0;
        f_in_raw <= 0;
        f_in_G <= 0;
        
        repeat(4) @(posedge clk);
        aresetn <= 1'b1; 
        
        repeat(4) @(posedge clk);
        G_size <= 52;
        
        //Initializing the Block RAM with data from file.
        //////////////////////////////////////////////////////////////////////////////////////////////////////|
        f_in_G = $fopen("D:/MasterOppgave/smallsat_prototype/EMSC/in_G.bin", "rb");                         //|
        if (f_in_G == 0) begin                                                                              //|
            $display("Failed to open input file %s", "D:/MasterOppgave/smallsat_prototype/EMSC/in_G.bin");  //|
            $finish;                                                                                        //|
        end                                                                                                 //|
                                                                                                            //|
        repeat(2) @(posedge clk);                                                                           //|
                                                                                                            //|
        init <= 1'b1;                                                                                       //|
        valid_input <= 1'b1;                                                                                //|
        for (iter = 0; iter < 416; iter = iter + 1) begin                                                   //|
            for( i = 0; i < 4; i = i + 1) begin                                                             //|
                in_G[i*8  +: 8] <= $fgetc(f_in_G);                                                          //|
            end                                                                                             //|
            #10;                                                                                            //|
        end                                                                                                 //|
        init <= 1'b0;                                                                                       //|
        valid_input <= 1'b0;                                                                                //|
        //////////////////////////////////////////////////////////////////////////////////////////////////////|
        
        //Streaming RAW
        //////////////////////////////////////////////////////////////////////////////////////////////////////|
        repeat(2) @(posedge clk);  
        f_in_raw = $fopen("D:/MasterOppgave/smallsat_prototype/EMSC/in_raw.bin", "rb");                         
        
        if (f_in_raw == 0) begin                                                                              
            $display("Failed to open input file %s", "D:/MasterOppgave/smallsat_prototype/EMSC/in_raw.bin");  
            $finish;                                                                                        
        end  
        
        repeat(2) @(posedge clk);
        valid_input <= 1'b1;
        enable <= 1'b1;
        #10;
        for (iter = 0; iter < 108; iter = iter + 1) begin
            if(iter >= 40 && iter <= 43) begin
                valid_input <= 1'b0;
            end
            else
                begin           
                valid_input <= 1'b1;                                       
                for( i = 0; i < 4; i = i + 1) begin                                                            
                    in_raw_temp[i*8  +: 8] <= $fgetc(f_in_raw);                                                         
                end                                                                                                                                                                                     
            end
            #10;     
        end
        #20;
        enable <= 1'b0;
        //////////////////////////////////////////////////////////////////////////////////////////////////////|
        
        
        
        
        
        repeat(1000) @(posedge clk);
        $fclose(f_in_G);
        $fclose(f_in_raw);
    end
    
    initial begin
        
    end
    
    
        //Writing Result to file
    integer p_idx,j;
    reg first = 1'b0;
    
    
     always
     begin
        @ (posedge p_rdy) begin
            f_out = $fopen("D:/MasterOppgave/smallsat_prototype/EMSC/out.log", "a");
            //$fprintf(f_out, "%d", p_out[48:0]);
            //$fwrite(f_out, "%d %d %d %d %d %d %d %d", p_out[47:0],p_out[96:48],p_out[47:0],p_out[47:0],p_out[47:0],p_out[47:0]);
            for(p_idx = 0; p_idx <NUM_B_RAM; p_idx = p_idx + 1) begin
             $fwrite(f_out, "%0d;", $signed(p_out[p_idx*48 +: 48]));
            end
            $fwrite(f_out, "\n");
            $fclose(f_out);
        end
    end
    
        
        
        

endmodule