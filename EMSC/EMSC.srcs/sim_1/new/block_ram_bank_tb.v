`timescale 1ns/1ps

module block_ram_tb;
    parameter B_RAM_SIZE = 100;
    parameter B_RAM_BIT_WIDTH = 32;
    parameter NUM_B_RAM = 5;
    parameter PERIOD = 10;
    
    
    reg clk, aresetn, valid_input, read_enable, init;
    reg [11:0] G_size;
    reg [B_RAM_BIT_WIDTH-1:0] data_in;
    wire [B_RAM_BIT_WIDTH*NUM_B_RAM-1:0] data_out;
    wire init_flag;
    
    
    b_ram_bank
        #(.B_RAM_SIZE(B_RAM_SIZE),
          .B_RAM_BIT_WIDTH(B_RAM_BIT_WIDTH),
          .NUM_B_RAM(NUM_B_RAM))
    DUT
          (.clk(clk),
            .aresetn(aresetn),
            .valid_input(valid_input),
            .read_enable(read_enable),
            .G_size(G_size),
            .init(init),
            .init_flag(init_flag),
            .data_in(data_in),
            .data_out(data_out));
          
    always #(PERIOD/2) clk = ~clk;
    
    integer iter, i;
    integer          f_in, f_out;
    
    
    initial begin
        clk <= 1'b0;
        aresetn <= 1'b0;
        init <= 1'b0;
        valid_input <= 1'b0;
        read_enable <= 1'b0;
        G_size <= 10;
        repeat(4) @(posedge clk);
        aresetn <= 1'b1;
        
        f_in = $fopen("D:/MasterOppgave/smallsat_prototype/EMSC/test.bin", "rb");
        
        if (f_in == 0) begin
            $display("Failed to open input file %s", "D:/MasterOppgave/smallsat_prototype/EMSC/test.bin");
            $finish;
        end

        
        repeat(2) @(posedge clk);
        init <= 1'b1;
        valid_input <= 1'b1;
        for (iter = 0; iter < 25; iter = iter + 1) begin
            for( i = 0; i < 4; i = i + 1) begin
                data_in[i*8  +: 8] <= $fgetc(f_in);
            end
            #10;
            if(iter == 24) begin
                valid_input <= 1'b0;
            end
        end
        #30;
        valid_input <= 1'b1;
            for (iter = 0; iter < 25; iter = iter + 1) begin
                for( i = 0; i < 4; i = i + 1) begin
                    data_in[i*8  +: 8] <= $fgetc(f_in);
                 end
                #10;
            end
        
        
        init <= 1'b0;
        #30;
        read_enable <= 1'b1;
        #100;
        read_enable <= 1'b0;
        
        
    end
    
endmodule