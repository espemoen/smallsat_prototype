
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity EMSC_rtl is
   Generic(
        B_RAM_SIZE : integer := 100;
        B_RAM_BIT_WIDTH : integer := 32;   
        RAW_BIT_WIDTH : positive := 12;
        NUM_B_RAM     : positive := 8;
        MULTIPLIER    : positive := 17;
        DELAY_STEPS   : positive := 52;
        P_BIT_WIDTH   : positive := 48   
   );
   Port ( 
        clk         :   in  std_logic;
        aresetn     :   in  std_logic;
        init        :   in  std_logic;
        valid_input :   in  std_logic;
        enable      :   in  std_logic;
        G_size      :   in  std_logic_vector(11 downto 0);
        num_pixels  :   in  std_logic_vector(31 downto 0);
        in_G        :   in  std_logic_vector(B_RAM_BIT_WIDTH-1 downto 0);
        in_raw      :   in  std_logic_vector(RAW_BIT_WIDTH-1 downto 0);
        p_rdy       :   inout std_logic;
        initialized :   out std_logic;
        --p_out       :   out std_logic_vector(NUM_B_RAM*P_BIT_WIDTH-1 downto 0);
        corrected   :   out std_logic_vector(P_BIT_WIDTH-1 downto 0)
   );
end EMSC_rtl;

architecture Behavioral of EMSC_rtl is
signal dp_enable, init_flag, read_enable  : std_logic;
signal b_ram_out   : std_logic_vector(B_RAM_BIT_WIDTH*NUM_B_RAM-1 downto 0);
signal start_flag : std_logic;
signal start : std_logic;
signal valid_dp : std_logic;

signal delay_register : std_logic_vector(48*DELAY_STEPS-1 downto 0);
signal shift_register : std_logic_vector(47 downto 0);
signal out_raw        : std_logic_vector(47 downto 0);

signal p_reg          : std_logic_vector(NUM_B_RAM*P_BIT_WIDTH-1 downto 0);
signal p_out          : std_logic_vector(NUM_B_RAM*P_BIT_WIDTH-1 downto 0);

TYPE state_type IS (idle, setup, calculate);
SIGNAL state : state_type;

begin

read_enable <= '1' when enable='1' and valid_input = '1' else '0';



process(clk,aresetn)
begin
    if(aresetn = '0') then
        corrected <= (others => '0');
        dp_enable <= '0';
        shift_register <= (others => '0');
        delay_register <= (others => '0');
--        p_reg <= (others => '0');
    elsif(rising_edge(clk))then
        if(valid_input='1' and enable='1') then
            shift_register(31 downto 31-(RAW_BIT_WIDTH-1)) <= in_raw;
            delay_register <= delay_register(48*DELAY_STEPS-49 downto 0) & shift_register;
            out_raw <= delay_register(48*DELAY_STEPS-1 downto 48*DELAY_STEPS-48);
            dp_enable <= enable;
            if(p_rdy = '1') then
                p_reg <= p_out;
            end if;
        else
            dp_enable <= '0';
        end if;
    end if;
end process;





b_ram: entity work.b_ram_bank
        generic map(
            B_RAM_SIZE      => B_RAM_SIZE,
            B_RAM_BIT_WIDTH => B_RAM_BIT_WIDTH,
            NUM_B_RAM       => NUM_B_RAM
        )
        port map(
            clk         =>         	clk,
            aresetn 	=> 			aresetn,
            valid_input =>         	valid_input,
            read_enable =>        	read_enable,
            G_size      =>        	G_size,
            init		=>			init,
            init_flag   =>          init_flag,
            data_in     =>          in_G,
            data_out    =>        	b_ram_out
        );
        
dp: entity work.dot_product_module
        generic map(
            RAW_BIT_WIDTH   =>  RAW_BIT_WIDTH,
            G_BIT_WIDTH     =>  B_RAM_BIT_WIDTH,
            NUM_B_RAM       =>  NUM_B_RAM,
            P_BIT_WIDTH     =>  P_BIT_WIDTH
        )
        port map(
            clk             =>  clk,
            aresetn 	    => 	aresetn,
            en              => 	dp_enable,
            in_G            =>  b_ram_out,
            in_raw          =>  in_raw,
            v_len		    =>	G_size,
            p_rdy           =>  p_rdy,
            p_out           =>  p_out
        );
        
        
        
--        arith: entity work.arithmetic
--            generic map(
--                RAW_BIT_WIDTH   =>  RAW_BIT_WIDTH,
--                P_BIT_WIDTH     =>  P_BIT_WIDTH,
--                WLENS_BIT_WIDTH =>  WLENS_BIT_WIDTH,
--                CORRECTED_BIT_WIDTH => CORRECTED_BIT_WIDTH,
--            )
--            port map(
--                CLK     =>  CLK,
--                aresetn =>  aresetn,
--                valid_input =>  valid_input,
--                in_raw      =>  in_raw,
--                P           =>  P,
--                CE          =>  CE,
--                WLENS       =>  WLENS,
--                valid_output=>  valid_output,
--                OUT_RES     =>  OUT_RES
--            );
end Behavioral;





