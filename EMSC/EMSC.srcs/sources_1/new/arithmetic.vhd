
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity arithmetic is
      Generic(
        RAW_BIT_WIDTH       : integer := 32;
        P_BIT_WIDTH         : integer := 48;
        WLENS_BIT_WIDTH     : integer := 16;
        CORRECTED_BIT_WIDTH : integer := 64
      );
      Port ( 
        CLK         : IN STD_LOGIC;
        aresetn     : IN STD_LOGIC;
        valid_input : IN STD_LOGIC;
        in_raw      : IN STD_LOGIC_VECTOR(RAW_BIT_WIDTH-1 downto 0);
        P           : IN STD_LOGIC_VECTOR((4*P_BIT_WIDTH)-1 DOWNTO 0); --1-3
        CE          : IN STD_LOGIC;
        WLENS       : IN STD_LOGIC_VECTOR((2*WLENS_BIT_WIDTH)-1 DOWNTO 0);
        valid_output: OUT STD_LOGIC;
        OUT_RES     : OUT STD_LOGIC_VECTOR(CORRECTED_BIT_WIDTH-1 DOWNTO 0)
      );
end arithmetic;

architecture Behavioral of arithmetic is

signal S1 : STD_LOGIC_VECTOR(P_BIT_WIDTH-1 downto 0);
signal S2 : STD_LOGIC_VECTOR(CORRECTED_BIT_WIDTH-1 downto 0);
signal delay_reg : STD_LOGIC_VECTOR((P_BIT_WIDTH*8)-1 downto 0);
signal RES : STD_LOGIC_VECTOR((2*(P_BIT_WIDTH+WLENS_BIT_WIDTH))-1 DOWNTO 0);
signal valid_reg : std_logic_vector(15 downto 0);
--signal input_reg();
begin

valid_output <= valid_reg(15);

process(clk)
begin
    if(aresetn = '0') then
        delay_reg <= (others => '0');
        valid_reg <= (others => '0');
    elsif(rising_edge(clk)) then
        valid_reg <= valid_reg(14 downto 0) & valid_input;
        delay_reg <= delay_reg((P_BIT_WIDTH*8)-(P_BIT_WIDTH+1) downto 0) & S1;
    end if;
end process;

sub: entity work.c_addsub_1
    port map(
        A   =>  delay_reg((P_BIT_WIDTH*8)-1 downto (P_BIT_WIDTH*8)-P_BIT_WIDTH),
        B   =>  S2,
        CLK =>  CLK,
        CE  =>  CE,
        S   =>  OUT_RES
    );

add2: entity work.adder_64bit
    port map(
        A   =>  RES((2*(P_BIT_WIDTH+WLENS_BIT_WIDTH))-1 downto P_BIT_WIDTH+WLENS_BIT_WIDTH),
        B   =>  RES(P_BIT_WIDTH+WLENS_BIT_WIDTH-1 downto 0),
        CLK =>  CLK,
        CE  =>  CE,
        S   =>  S2
    );

add: entity work.c_addsub_0
    port map(
        A   =>  in_raw,
        B   =>  P(P_BIT_WIDTH-1 downto 0),
        CLK =>  CLK,
        CE  =>  CE,
        S   =>  S1 
    );

gen: for i in 0 to 1 generate
mult: entity work.Multiplicator_48bit
    port map(
        CLK     =>      clk,
        A       =>      P((i+1)*P_BIT_WIDTH+P_BIT_WIDTH-1 downto P_BIT_WIDTH*(i+1)),
        B       =>      WLENS(i*WLENS_BIT_WIDTH+WLENS_BIT_WIDTH-1 downto WLENS_BIT_WIDTH*i),
        P       =>      RES(i*CORRECTED_BIT_WIDTH+CORRECTED_BIT_WIDTH-1 downto CORRECTED_BIT_WIDTH*i)   
    );
end generate gen;

end Behavioral;
