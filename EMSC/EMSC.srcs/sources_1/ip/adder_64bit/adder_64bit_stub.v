// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Thu Apr 12 10:17:52 2018
// Host        : DESKTOP-0KA73P0 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               d:/MasterOppgave/smallsat_prototype/EMSC/EMSC.srcs/sources_1/ip/adder_64bit/adder_64bit_stub.v
// Design      : adder_64bit
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "c_addsub_v12_0_11,Vivado 2017.4" *)
module adder_64bit(A, B, CLK, CE, S)
/* synthesis syn_black_box black_box_pad_pin="A[63:0],B[63:0],CLK,CE,S[63:0]" */;
  input [63:0]A;
  input [63:0]B;
  input CLK;
  input CE;
  output [63:0]S;
endmodule
