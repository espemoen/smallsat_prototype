// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Thu Apr 12 10:08:38 2018
// Host        : DESKTOP-0KA73P0 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               d:/MasterOppgave/smallsat_prototype/EMSC/EMSC.srcs/sources_1/ip/c_addsub_0/c_addsub_0_sim_netlist.v
// Design      : c_addsub_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "c_addsub_0,c_addsub_v12_0_11,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_11,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module c_addsub_0
   (A,
    B,
    CLK,
    CE,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [31:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [47:0]B;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW" *) input CE;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef" *) output [47:0]S;

  wire [31:0]A;
  wire [47:0]B;
  wire CE;
  wire CLK;
  wire [47:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_ADD_MODE = "1" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "1" *) 
  (* C_A_WIDTH = "32" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "0" *) 
  (* C_B_TYPE = "0" *) 
  (* C_B_VALUE = "000000000000000000000000000000000000000000000000" *) 
  (* C_B_WIDTH = "48" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "1" *) 
  (* C_LATENCY = "2" *) 
  (* C_OUT_WIDTH = "48" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  c_addsub_0_c_addsub_v12_0_11 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(CE),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADD_MODE = "1" *) (* C_AINIT_VAL = "0" *) (* C_A_TYPE = "1" *) 
(* C_A_WIDTH = "32" *) (* C_BORROW_LOW = "1" *) (* C_BYPASS_LOW = "0" *) 
(* C_B_CONSTANT = "0" *) (* C_B_TYPE = "0" *) (* C_B_VALUE = "000000000000000000000000000000000000000000000000" *) 
(* C_B_WIDTH = "48" *) (* C_CE_OVERRIDES_BYPASS = "1" *) (* C_CE_OVERRIDES_SCLR = "0" *) 
(* C_HAS_BYPASS = "0" *) (* C_HAS_CE = "1" *) (* C_HAS_C_IN = "0" *) 
(* C_HAS_C_OUT = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "1" *) (* C_LATENCY = "2" *) 
(* C_OUT_WIDTH = "48" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "zynq" *) (* ORIG_REF_NAME = "c_addsub_v12_0_11" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module c_addsub_0_c_addsub_v12_0_11
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [31:0]A;
  input [47:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [47:0]S;

  wire \<const0> ;
  wire [31:0]A;
  wire [47:0]B;
  wire CE;
  wire CLK;
  wire [47:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_ADD_MODE = "1" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "1" *) 
  (* C_A_WIDTH = "32" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "0" *) 
  (* C_B_TYPE = "0" *) 
  (* C_B_VALUE = "000000000000000000000000000000000000000000000000" *) 
  (* C_B_WIDTH = "48" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "1" *) 
  (* C_LATENCY = "2" *) 
  (* C_OUT_WIDTH = "48" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  c_addsub_0_c_addsub_v12_0_11_viv xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B(B),
        .BYPASS(1'b0),
        .CE(CE),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
iO2Bdkfy0dqqValMR4KhTWXpD0gDQF+kyoly3tZBTZTVs0CbWJ4Owhu4jxMCf8X2gbWR6iweF6Ks
B5dmLHZTDA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
dbcEbgyZfx3YLmYpvjegvD9sRQCV1qBv0GqFBvCakC3SMR/H82zqo5uv5MZldBGUVmNHnxF3Vejx
zSqxUKfTNc90CS6quuoQe0eeq3T5XSdgwbNtjPZKvJuJTmQKT96yB3CfQOz13fGjaLrn/8NBUBBh
I7OEoGGg7ADph9V3vRg=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bD3a4YgAnaoJx9/hljj2C1rODcUhawTVE1gtdPkNj8/YjemaFM6/sF7Q0CXbDJ7a+OBrB5pUgj3O
Vesi4yVmFp+mGmFarftWat5KmZiP3RVWrXwdzMj+f8T7p+lE3iD4njqUxIUz0TsUaNvFeW0xVNNb
OwTEX04nyt5HrU82dltJCclpFxE6yrP9YvI7l328bphwnC63xxk8T3yXwCrvj3VrIYuDT2yMRxrB
TBCv/Fe2f07JQyV73J7+DGAeJG0B1dTHeu48auQT63g1HsYaUXREihEUKgZe70QlOqlPbrr6Quhx
2LXE8LSdCA+FbJ7LlQc/Sgasj3ZYjM5lhEKleQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GCfR7acMSeEtOw1DhZKkUXjh9Uw/vUar7CGDRG9rZcB9NFDtQTltJeuKjFg8eaeKH9HFBMryuX72
/tmzhtFaiSTjr2na4ncL2XV3QRXe7nQaiHdc7cKBcZDvdSSMzOSYcIxLunwLwQTLC7sCvINmlxO1
NXnYzJVL1xb9HP8QVnSYpo1p+gCXcRBZzrOjZjCUnl7F2t3ZZStSGjBEyXVLnV+ouU3+247oJAOa
kC7v+pOtG2ho4KclIg0MGijjPs+jyOFU+b5C+ufQp/zL9GiZ5waCjb/0Y1vkBc9jZKR7YRnv+ASG
ju1uP8oqEXR9742kXRnW4HkMKkCK1MLDgWYdqw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L+AGKmFZ1zoRJFd2cA+zxJhkgQ1R0aEjGQCGRFLNNhLHZXpzGDIjdSLjralBVRJ2rD6UcJutapF5
YaMoV9kphGGG2B07dxBuIimVjOxS3ZQJ7ru59ddfGBxUe9EHrv00Q5hTwoxig0lxqnmjSSnfsDeF
weTIqNnXkG5kqqezKC8a2FvUD5QWQBibhK69OAdmhhIOwZmpfvQKbEKgLX70BzcNlmLnttRL7G+q
XZ3fabZ42+JJHDLiIfveB3Gp2Lf2tzTH1u2xx5aEUr9154pnC9PWIwL3y3VBAT1oHR7ScdoGDOEy
HoYUiDibldOidIeKW0KrTeAIuBNmtM4R0R+RSA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
V5ClnklUs5Wo++EDemG/KeowZlAfqB8SUrvSxPQGrdIwGfUvoCajhuABAWdS/L/pQl7Eyz51aiuw
KzPMrWtQozAEITf1xzvzgKbWZqoi4PQD3rThywFsFq60u8DdvHYM/kEvit0cZVFvG8rAbtlseHLu
0vU1kbrNgxb3bxjOovg=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
cRqAgScIUeXUwYGfCC0XDtpcc+mFNm3p8oTcFdtIU1nnlMagpBMqRm5ELc+m/Yw8jBwvcvt4tUFv
u/ypEEw+y12B+5Pr6SmnLJ+NVB3Q3Eyh4Q/d7p3jReIIsUxrlENpCTi4PVXMKr1B1Htzm8F8mXDq
y2UV+0SC+4yrBIntsdS0S8jPBERhfJhzNC5z38pPHANtM4wGGIUuKxIALLz1aq+2AjLbEgFHNrzw
2bJiDwRSTwrY4Yx2MSzYJk3O+cQBUe8nJDPx+aGEvDzQ4ZdJMNg2z+iaiE7OTaqK492Jb/1jvU0j
wlI+n35s2rrnc9QgfljdOJuueruPuYDi5vTTxA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EvqAUTpZ7wUJLf7eOTi329cvrLJNMOIXtfjTv5Yt8PzpAUQ8IWtSNdz3TXwUX/iXG7Ev09NRbYTe
tNdCObVXilTKsLpBkjrWdEKkusIwX1M3xwsMAqmFSPCxNPE08HzFrvzhhQ9mvPu64mrSA7YdrDDY
z2kU+ROoZ+mul82d88S+M7OJeY4B2h/aTTtKGbXa4L/PuGRa2Jv54UdBnxeh7ITYoqfTTMus0xtd
GljALWPBOsYw16UJZmi1Pkuwg0SLLQKA9eXiuH2rz8s9rfOVmBhwJniXC7nmM98X4K80VIDNZLXf
O+GApO8dDlVLCPzceJI5ORY4ckWzB6rSsuew8A==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
d6013r6KDhh92KrqMoL8y/4p8UEOExN4z1++6g6t9VVuaBJZjLXxrPrZnCaTOrhRqNldC3qwH/Bn
m7ZaDKvCaM/vR8sPfWO73DXPc9dHw5QIPppA4/p7nHnq2Mn4ql+NGjClYkZRdsuZRvrdu+YoWhVx
/JYLGnItg0wV/LyD7pViclCXcSdPpXnnyATXeVEWxJ219CZTztdiCCcdspgg0py6RTleasu3aNFf
h2SB3Fyg4C6u3Mp2yjWHGIFA89neWHIz/KhSvGxh1MlOHYvAcQP9V67bfi2OHWFA0NceXZ8KFeKs
W5RlQE7kK6UGA1RBXLhpm0EvyBzQtS0z4Wx2hg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 10480)
`pragma protect data_block
RYNtlJaVShnnhXHP/rBemBU28+Pc+MzS2HPcXtOhl0FbXSaiWBmtMurZRhIjExQeBXkMi9BPR5LD
kVTCP2Su6qmdzHnK0s/9swS90EW+9o4kyZ/85X9VIApJkWhhq//etP0PElCMtqhA2Fmat7T87Mqb
LuBo1WkHkaZJL6HNq7ajLPNGi3qmVE5DJKs3raRCfKX4mHcGWqOJyFU3xE33iHhEndsgMIAG9XxW
b9IX0ZsOgLECdoWKECNgyQN2aidgRvjHGOov0PHzqBnjf7om9VdBLTC80u+Zp6MeoRGPNavC8gRq
LoRdu9APfETC+irICK9iyLS+wTpsEcRf5iP9nfZYcPUv+H6g2d3yxEkib4GpD2FJ0eq00qN1GIAF
ox7WFKXSzAsy1JaI7W9WUBUbMF2cyAfO7edaNffqttj0Z1Nl+h8n6O3k4maC3xK+R8ibCkHIK5Qv
XLkCeiDYGz7aqO+GCuVWYYpjQAnajUMndMyVU+ulsr7D4KJbnPusLnxoa+Xvba8AH/wuqIrXpxP/
NGxoCBh+9waF2p+dSP/kZ5duGIWGmq74/nd/itSizNsz1jZBspUtFf7JL0YUd2MxZ4CFMLqkPxYP
dOYM+fnwwT05urqLHiaONr1I5KlOZPs6sgYZ5SDuPtZB8T09SYDWkjTcNgYVcKX51SpmThvRjdq+
Zra9EHFcL9FazBMGLhehta8xesa5IV3+W+YeicJm9Div4Yj7IscJdzaK2X+HahSQF6nWfRIhPGXZ
78BtG2WQ9y6eOhzzqZzZOf3lqW4b9u8K4+TGpUCPO3WmtEyIpVWv9nxbEDvbGRI6BvTxJ6JXMtF4
FFK4mrtcHzzsfXQNbm75q5NgmCd1+LDmaRnNpJrWhA29ODP6BkRbvOn+lJkgeZTaZqV4+cHLSxeZ
jcNPjCyxUr/9KSyzn33UOsvC8eHBjt0k6pMkOE/SJiBvLG0NJvqd709C/NaFUT+dSyYPecDRFU79
QQEm7fxyE7FXXhZ2WZmCPyuWDiQA5mPxu5HejSfaXuynoNfjBiR8VrZC9FchM1l4KZUsqf9s+R3q
2wTcD/iUj73rMT7zTdNX6XSpti9E24NCgq1atJScFZCeu8NGewlFj/wwpw/LRwZcalSysNo1qPjQ
CbAAcMEF6R6LX/3Tl0IYYvUogji86CwVE4xINIPYATkggEOJ6zV4osaN7ItS9mQuKCULTue1pqUS
4eeEnTJJwgFQXVHFlsGtG9gbErhM41tAbYN9EFYhR0FHkNL9D0z0WlTIMvQFu/kznfREW2QJH+Bp
IEINKxZUXpua30J936Tvd5pkJ6Ly4ZM/ZUtOOheJq7lI6xK2HIe0X8IKuhoqV2AIOLM2sispM5r1
STbyJThN228YyPUBtN0iNR3MOMOcym0s4dQj9DCvwmPHhAhm/CgLk59mu0e70vkZl6UdTleftLzb
rSzA1vSFMuj9otlQaGjoYUeZj14tHgt5qMBBRLospVOiMuRy2ppJ5xm3UhGqBzo+MFbObBN8Uvok
fTDT8xfsZsMKVwkULj/ptrnpGg7yxU96ja+kUe+w2CLzdyJA+dOjHG86mkslNE3rX0V9zouwsi/p
vbkX/U/D7VNRywhXK84eYhLcw9KmLtZCuDPb6t2XvKdZ2gyFaU8vZEAX0u5VyHoGJOjrm8B4Px7h
XIVGdo8SkwHwAV3le2e76nPr9TooE0FqbmPGJpPa1D+6DmPThlnzD7gLBTQd18uYqKV8f8jccyRh
swoD6c9KCLGQ9g3PH8qDfQA628AZg4equ/HSVA4bhxQeoOHB3Q1yAfycThPmuMg51+1bW8ujnar9
vz9Jmj658j6D6xU1oper3+6K+4Y8SAQTqDSmifYvfELS9PdLG3RuzSTMkPHUE+RB86UaWj/Mw0co
MT23eUTnFx3/DYsSWA3XtO7UhvIJlVeLjIsOUzf6tcJy0UlxzNYiCHQO2izJ2pRpmZxiRyMyg6lA
E9R4ciCreqCrRepCCT6wSXUiR0Iaac9td0kjrYTl2ofh3QUUC0e1DahZSPEOhBWyx1cqn7eEZHve
VqaO+UbmBZLv8NkVVX8tmjTJ1l4VlbWndhbjKWCQUcyR7e9VowTminE/LjVcoiTeD2rRgLSG5CgS
kx1u3Yx/WKpTxvLCHG11ouTvrrLQq2vBNkk7JMW2lP4xicTDKyC8OLsM7s2g/mITBDh2KF2/FCCA
NfZ6gip4A5xxrw1CdhjyiQXeCfWjOrF5b4ovrzAqOguxLkjir9zeLJ0a/j2yZ4kQNlQ0ccuC4FPO
/ge+Rw3KCnccYaDvoT+SqJW95WWJ+akqZZpYIb3e3HMvnNbYliDOaFVCcFbGKAelPQLnagaiodYR
/beYkObld0UcU7nKWtmwTqcScxrtAlKSoFA6Uo4cTUJFlx9AKZ04+gzvGaRqgteBP/QE9RwFP/MW
k3BNhNU01rwrsMwriw2YoDdsK8zGuLNSGGOXH66GjhipJl+lCIYbyg/BLbnLUjt7w5Ju8wGzJ9a+
hCjIRt+hI/5Xo2B5VZe3zj54ZxxapOhNL8/2VlVU37M5plYh2yuk1hOHXQhyrQXkXqpZYIoBvZF0
S0X80vYRHQKJ3I/I8ZNWqJd9T+sCvwTONpmEKVweG/xleCzt1XDQxNu3aghNx9pmhvbaWGrV7Tx0
mPyetDYVpKSCgPbHL7MJ+Nkdq65dHAHvYJjgflF5+pCjhzRSas8/V+lBU8YmvZnR8w2z53HPVClR
h/9ROnq8gg6XHg8xi869LGJkC6n/TCybHlTUZ6DWAYlyJtQFvLYVsxhOStrHM+wo9R0c6gZdZsbS
wLzVNyNeLcsvxK5MEJA/TaxNLNVRLgiXkbI4vw/NvBuKO92v6t+Ofr4XbuGU2s/I6nh3QpY1gLUG
3Ric+cy0WIQ4wfdwQsQz+1IeWyTwNm/lKeK1ZN8Rw1mzEXtQZ4Ehwuk8QsClCNqM+HifnDmiTsJx
H4L+EtBoVGswJ62p+0NtseJRs5J2m3LiWBzEXCADqluxiwFVD/WDlUru4L7h3sESLxJ9inZebVcR
1TK2LCt9IxPKov72+mhD/f9XEyFibM+s7VjSBm5CobjIKz53DIlfrkfXJ4YtN4cDGLyeI6puJica
BnDn34VHrnSQNTOYLI8axvZQZZIAE5qqf26sZLEbDIjaNskXAc/RYS0SgGWv2v3316FP0W/b47rX
rx+u54QBe6XSZrmRBjO4q21HKccD7wMTp7g4s6FHc3RrJcUKK352e1HiGk2sQsF0LHrUYMGieNdr
maui49EB/RAYiUHvr0SlJJA1IfgmqzMpVtMr8C+ZtCwSJ+s5oewR3TZYDBzbjiHKpiGd373sDv2y
pGz+W2B1lUC1Me1I75VIgpZYmwqNU//hKNjLykUeEYTDEipgJ1cqyOC7djCLlLoXyiJYV4fpUoMN
gNq1ZAFTBTawndwzW2KD2g1dVHdFlaK5Go/6MzUpL3nPU/ql9/Tn8B0asmKo1ErOROoFV4S5BPPz
bDHy0/mZQp7liR+xrBdlV90m5uVix+6w9emK7W8vWqCopGDFBPNc/l+fxK0I6nAQeVGcq5xcJnaz
OyZHSLbdfxrtsla16A4gLnjWHF8wGc7fgO12NQAiTNtQl6+kMJWGcIyHiKhKMetwfACOwMzT/xZn
0KSYne9yz+qnOXAObOKioYlcYT5S4vA3AWq97WDiFkrcSEHLLN03fkSagkswpHXi1Bk9GITBt4xg
ex0hPnxiQtovWSBABVNmFtdc9OH6RniTqEi9j5V6BYkQMLgfd/j0GqbkLjdJpy/dBNPh/E4VqEHV
NFjGMzbh6PJAmMixWDiGoy9FeF79QRsz4oFGgaScVT6QXGZUeCPoi/AR6um449AWkAz7HVVgjZUt
E5gzgmTGhUUUDV4k7hVFUIroLjGFN89UbNIGJOT9hg5B55y7vPEGYKRorCOe5j9ATA+doauROBtE
HaxWJV7mnMS8/PzQWqwn14cd28TWhv5jw8+98rZuJo0eiaHqK3AfpJ9/7HUAEjypaObWDxntdXco
ISh6BXCGt08DK0SiYh2NTHrGZdpBWFxk5xc4/05tdMlKMHZevQBkOgGfNSzZFqcCIGbj8jDBdh3W
82gO1ZQ8M+jzmrykskgMUrM1QipHA68jNzV3oRN0ti+MajA30aqsCaNH0Nqz7gMh85oEHUA8O9z1
gc075YR2bRNWLESqJfPOi66wtMI2+dSKwg9iRIuW4o8bH4KVllABNQlUTK4t9admt/ZWJ2sSm0DA
8UgM8p/VDbAnUubgr1BBO7QUFdM+SxVsuGD1fsJ/KbwD91seH1ZrluLXaisjEImr9llF9J7exR0i
9GJvvGwrTjfkhGHnKBlo9HxzZBNAHs0+vrpjMv/EXwC52PchaIs61jizmD/G9S6UPJf8gX02V5lU
x+N7ZJnhLdUplFumUlMXMfgn/rKctpkQ3r5PkDP19o4aPYZvt7lMaaq5QZvDqf3kaz4XXnCfoicg
cyJ929w8EnBZhlzrljNDE2aJsKXz1PSaHxxDVS3Do1tS7y2jiu6DFy4Jg1fHN5+mAZgf+JDNBfLi
ci9nSlYgtHDKxuTqtSQjP/+w5dv1QYoGlIHk0Z5w/0nYOThcVEz9o9n3ka9Jgi+j2uGgafTKzhNE
YNf7v6wfnApmQMuatkei95TrvZhszn4zrty0MHUxo7NV/b4h2r2tVGsvUJv8g4hpN7/5qHOJXw0W
be9QdlrSpqFFZ2TufL7oQJRslT8f2qrbbmfg34Qb337AQW8CAq334b708iEe61K2ReAViFeRhQAe
RWzVUrB+0O6Mpu+ZZptmsmjWNDbcLUwTXs3npfuJDOY0tcqcKYDM41/XW4qpTc5ipUV3/fy2wn5u
/FSzJFBNGmTCBZpLrRASHscd4vEgfOXrLnw7HwItrTmU/RsyFgWK5L4mG9IsXekugapuGEvBZb6E
u68krIsrIE+I9OuAKol8NGlevQDHO7Tryuqj5ZXX4cA/cG8jRMzxC2G7vv9eK9e+RzbvEmpWG7lC
VWBzD1x3wsZkyrga8N+62QwT8JwVtSPfDlEX+fzEKa8ItjUQPRo4O4ZU2rGJwd/2ikNRtgHp6dKZ
zNDlRMYqhy97DFB4gd002Iqp/q7di5vsIqiMaYRJMko2C6/1O8f8perq6kAxTffhXAVCrmZTFECr
kC4zLtgrC453VsmKxmk0ZcedXJHKxAUDRfrkrNtMXidaMaDqYRMlW7/Lzrx0npArr4OJhXAE6UiK
xzJ4js0OW/BEXZC+F7PtrIxvoJ+GdU4X2FftTKLIDeTXuM46iWH9gOFzgi4woJI0l4TyylmU8dlN
+uNWFOlaDouUrkLPLbPX3b+SkuYHWrys/jJeiqmrSYWiR2iKVyORmAEh4FCXOF+mWuYrOcXk1gZ3
vnuc+4AASv7CtOkGcA+LYtFZCYypYrxEwb1l3mfWI+6g+/5oZQDtcx6DlRqH4pZZPx8dBNig13Ca
GloACERnz1mCSjJFW6OKg9XQhn0lBpCqvQZXa9K9G0j+T1hsErKNOu8DQ6CuEUjxLGqEmt7RAlr+
SUhGrEjtd2DCO/xAzWG+rwJgGY4bxaeZTxJ3vrcr+9klCZpcgGohm+gYXlgY6e2Ir7UacWcmW5Lc
2fKLBtUP3l5nWuuwSxSimURlqjPUzqZVkbd/5TPpZ1g60d6of80/2ftkVbNE54g3jCgsqCE1v99P
tGjpPXiAKNaVKK6p+HeXD1Mb1Bi6pp9/912Odsx1AEPZ47vnMwI98WEKeG29/yeULqP2Y5qJ5oAn
jHSkb6mdI44UkkuwvG0kkn7dV/+a5+rBiLL/jes1+jR7G+kVhm20yzGeSHxhBbVi4c2ETNY69DqW
A87s6pAaZfOOpJLTz1HfF8p0kU4+iaYz4Kp+pNxEW9xQ1Lfx0QF3JFqDZ0EumWS8CqUEZqXPQVZg
D0hCzldcHs0RMITldQfSKusxWhr+JK/wY9LvWV+zGWMBcLQWhGdTp8wB7I+Q6ZcjYPyj5ZXw/UFp
mVp67J3GMZ/7fzLTAhvN/wBq2K0SIziXLdsYEh4+xWcwapI+vM3vB5+QJjkMoI6L0rRDIxRpoesa
rsoxYCzWBlesXE9rMTdTooPRdcozzFYpr5pm4hpUAs4s+tM2m/ydWMY1zue709D3eNW8wIHKMSe1
r6xhHcTkPc2F25/8VaP3Wmq6eO8lEWxhHymd3HmpokNd/Y+UZmoR0nPT5xMquAD7MY75r9LcvFe3
jCofBPBzAZvBVNr5tDOBQMTPL7NMggQA4tCocHBAu2kXjnJoxN8W7diBRnD94MIir+3hvt914GCj
0DS+lNSRylJp3Qk7Xmj7vf96EB02sa4QBHFJspEdo2Ti3n4KBIA+vgi1Tp0E5R1q2yYxPkvzBa82
1c9L+/kyTDE/fZgZhmVh8YKgIhaAcCk3EvdfxSnqaWZAk++Tfdlxis70Zy/FY/B+n4MnQnx411yF
cSfRwZuk2WPblaXWen4HE+iczY/R1jVhynFbW162J9rwG83LRb0b5aHvHP32P5ErxgLoYwxUZ6uz
8+TEVBTV6wqw4N5Ott5aDP3E0S4eJw0P0irSRsthIWzM4uh06LH6gRyMXrtKV19fC0FaiR7T+APm
8yjs2E2J/Gwr4OKYIeXic9fL301s3F6o1ppVPzSwXZllaVvzkfsRGgrKVj86Z9otCKSJNazPZxrJ
47docn7LLA8kdujGvvIxbJ84OQ2IeJ+0cbtz2cZ5irUbqbWHMLbCjdZ03U2blo0mQsWfOz/VSGV7
INRBMvy6UVH3IRdqhbdGwmH4pieYk8mIkAOoIpbp4og4fefLWepxxgyvu5zSpCcVH4WFgiZi/iiA
m0RHJ7NhrKFjSF+HTmNgSvdeVuO/sBhV821aX8gHxe9VEnt+zEWgMAmyCgkPWmgrqXPDGDpLhlLf
cNTE7/CJGTy8Ubdu/9G15n9RomXinCvoBVlccYELx04FvyQbWPZ6pqhrNqmeE8IyDKUzbXJ/XN8l
8kLZ5bHmUvDvLHD2FoAUNTrck5BuDQxW4A5k3m3fL43GjZNM9f2xLu9qhvfriSA1oUriju6Z3BpH
wEAw3sXl7uFXIdQfRDqai280wKRDwcU5ngqMkL+Rk5RowploCtgS2KYNTFWVdWQagxCdUV+0stG2
ga3VQwWpIvj2458iy2/Pp7m2aNkTlNcnXuvM5x5UpTEGJ5bcdu2D+XdcEWYo7/xJUTMAq741NPXL
6a5j/OqLTj6lh62tf1Cpj631IDekN5aZJVcUPOAXARy5eezVgUMfCPzyMA4nAkMd6HigBe9YwWoG
T5YlYm+eLd9jPQmM2XWnSJgiq7EVRD4VBfEfb1di3/Gd5vl8WPYAvFLmR9wKA0LB+cTFZHmdfPrD
UBDW/hKjJ88tayRd4+ZfvduG4aEilxQecAnvlmvLciAR6oxrzvkthiFFSVIn1Hht/6UqyNLCFoo2
kYOwXrpz4QZCL0MMo28mTUf6AkWi29z5JB1tE1JqdDuStOtuMsB4WLPA+E3ajf5g9+JT3LBVYrDC
C1th2vCmBpfUY8v3UrmazVn4ytbJ/z6RvJ3C5Auktcg8pa7NiXZxIDdU32B+9D+m7TDx+AC/U71L
HRiFYTa8J3pUR2BnCAbQFu4LwK34cvp6i9xxKKsheNC/YbgXED+6afdmlpBCDVYZrLLlzFHbU6X9
31sxY5KJN7cI+ZRz3F34K7aTIP18tg0v0S/V8Afze3V2vawu9KYltEIHNp8TKPhtBarYed7qg74J
N5JJk71hm3Jc6V+7BznuECFIp4OXwWlyu1ISlm5QEIdz4oL912ZSiqxKuQC40WL/vTLeym5z+jB4
K3Aq7k+p57hqIx10MqO1E6BTARTTSodob5JFr1/qd9t+u4wTmal0mxQWjGf7wViP9ZN2ZjKHHgwx
S9b3GtU0NLUGF3G+IzJqi8d2wysM/dTAy3369n+d3QXpz/W3vYhJ2JBlCAT3NYUuySXclyDL3xoT
a6G9N+w5U4CRf0LU0EUXbQfybg0x6IwDgl6fFEnoibQFYM2bljvvhLc0S0G/4JXdCSkX0jxlb8u6
iqoKQVQCJBGI4aUVcOe8ELyxth3wUC45jWnYZEt3M0aB2WEY9AXJQA39H+6qWKupxi6HsKc0mdIZ
HtrpS/NjHJ1Hp7Icn6OU9YCNJi6v9oiQ+ZIdqqv8ojPt6A66FYDmlHC7A9ybJvT0SY5/iEwqLOLI
RlYopLyLeCzTYCn/iap9TzVCIujWsTWV4BVeM8ZarWNKznEyUi93DGAwIYIg2ieg8Vdx9n188bsF
/kpGldQs2b7jGuzTf3ao/x3V/yfDoFLtZF/u1k2SIWScFttxREV0NTd4OOrMMg9bzBjTJw61YVDt
bvb/hhszxx1K6eqWjxuHlWWb1MYj25y+3JD/KJ9JSponPgmVRAIox5Xl2L51vLOvsAZ7Ins7XYct
y8Zj+fxYz3ZDtsuZaVlzGGg29uxD+/MP73CROpIaBfcyiOZtINVaQrHIY71IB9ptUivLDfqlEo//
XMNBkL5DZx85b2xX03xtVWI3n6V6EpJYVoVnW/8IrcOpyB4jXbIdkgXpT6VVzdp2XSotw2vCWjlU
Er6arpxcZwAAOMEXRnF9EFGmOu810szsiFdmSpzMPQd8EnSXwUb3FkAcnZG1ZyF3SZuHgZMynvlB
P+uKy6J13d+DVnUEDASBR/nU1B1lPGnHHs7oY0mWibKyffdFVJuZm2eDm6aU29cVNisbqCwSU1ir
6Iog5u4zuQLf1y4aqE95DUpTU6O6EB8rUonFUTCKpNgeYFSdoIEpWdL26TJd7PlWBseOSXOEjmv3
mAD9uPNwi2FcpPiQLrskf7Lzf0LptmE9xDcHamnPMHa2rVWqV0LL52dZYLD+peu3hJpzrwpzVdAi
qLOIBGaFCrvkrcy6H8WxNrmzF1O3LoDivpzuzFsqQcskHIa7eAWmwPY3lWtaTISFu+uwRdAS+RXR
cCVqg13uGo8N4Da6xAgvaFefFpVGGZA0Cp9AIpDDSWkMZzJifXI3VliSo/pggGRTMsy5fwFMWByQ
4218wMoG1hwLZV+rQubIZdaH2gGtkmWE0PL0XnoT+kHD/gv+DaEosknEJB073WyRPjRd7AFbJU5S
gCkE4eg3J8ad1h6Iv8P4EXxzmfDctV3reT2RcVZ+7aiS2LbNE7E/EX9jFeis2ftkK4scXpqTa7gZ
TRWe1IHDgd+ofvg2V30QeNd0fcwijynSZyXHQMK2rR9zFUsVSIn9K+A1uFOsEZ/BhzSDrDCv8VjQ
kIuPA3BoKqwGIxrlH8pnT72rrDYMOwtdaI/8qs8yDLhPg2ky2MGjTu8Ta9QgPSTF6H7Iof8m7KaB
EGqM3GtPjhorvdbDNhhpPxH6saKGUMzJgqqPR5aO4Hi455pc63ajNFaXqpHy8SvwEYFSOyKR1XZZ
A5uKmcsWBfIu/5xgngukxFXkXzzWFT+Ipy7Spu0iV+PScl94SJhmszAM5MjQfD33j3lgbHNIzjhs
1KKtO/GwkkxaM4GgH1kJIqofeq/S1yMIcy0QHhGjN4L+9Xe2y0ya9fEEgXsVHbn9VB1DEm4QmfUb
5+n6jdj+zfN63VOjf7khicAyD6zFOLJ99oBkFeKrWcp50gH9FHrG8vkX6nX/tFr3vzhWGO/9AaLx
QhcTMbw3+LribQim/ycV1jA6XnCfRS1AH83Mxd1GZubHtkFdQbB+Z4lzBOOodGG6b5o/Ei0wxmk8
89XJ2RhTxMjzZyzOxcDfrhNCfYVfA++BTqNXBp6p17HCt97GLVKyZvi9/N/7IHFMLua6Uq88nWP+
G43mlUBgiZ3s5mBFjfharkk1XTH8wlCVeeIDuEmxqd4F/R/kwwGukUGQYtvb4a3Mh4eSS5iRnARa
QJVt4Blo+rP92LoacL9Z8pGstwPKdIEX0MMQM6NW0a5xFQJDz7ZD5rf0OQ6yzat2PlZrtWmtZNmC
C8IWyioqc7w5vIUybPHXq2rAWAMfPmfQ4Ddt0iYOCGBGJoL8/VDCM3N8LFZYn6vXU16x2cOLSdFN
XSKdyXgqySROcWtZSuKTsUWLxXL4r6DFojsiWUVDz+CSWYozD5vVMgsUCZHAWRs72M7MG89sn4tv
DI0JQuioZbIm0C6ksqW/+5ZNy+7bYFSPdqiIHWA/4kvbUqip5qo2UG9+UM5n0LTH52SiFj0tqCPB
R6wr17AVDfQyLaRb0xV64rfTk+uLyGrCPcpNg21XVTgQr4Sdx19z+NUZgRK2Dl8G4YrthjzmIQ14
Lf/TVtTtWWZF4T0LjYOYreQCApTOo6gp22DX1lOgDmgV4Mbrs/TcM43zn8T4EciZGC2gG6K+8FI3
9bV5/Jz/Ekx32s6+b2ivOyx/3g2W9hSM9Bgv4eLIpM/nuAuvJFLBWJGnkZ8tb/jNiA4BHLpjegGu
BikfK9bTWXm5OHJwrT7tdbw8lTPlpna59gqodvMK1B2p0RRADUap98TW7pQNS781pz66IoVwU+Ws
bvhh7IvQnUguSkfffk+qLRnh/cgBtjHjK9mcDE9dOcAqOaXaUkCOPbZ/c0KuiwprdRx7bDyuT9wo
WSjDCeBNzbN01YqWtIpg1bX8SmMnkntOl7FIfb/rACb7Rzn+Cgp+XVkRylQB5mD+9u4KCxPDLWbV
eQAPLFYCInYLldhDEmhKdfLNU0dZG0PKCHQerg7YInH193rIW827CJAYsuGT6M8ov/Q59CXDc7+z
XUX4wo35P6Zrt5Fgar8JNroSYersdhkGEWT9ZDX+LY1Ilpx1mJ/yDkJF+2YRvpWwCf681fbo5NJM
JCb5TRJIUdjuQVFSP+hm//DyGY7qOpsNCUy5mLrTejROj0hHd6SiFTSWKbxXej/GA6wlVqAFjG+K
Ws2WMzyM+z6Eid0OQqSvZnVRlEB4ehWR10Gvx//u/qeRkfO5Wj2g539plEpq0QK5/GW9dJagBfOm
nCs8rqanZZnVQe/JTUHNouphwbZosbkt70x6VGvxobF2fHK9kZ0+PaqCyMmBoVTj5f2eisYcOKHD
WpBitePBqtA1MPWn64PhitktQgLXYD8FyfO0Klp82b2NPkUO6Q9fD+8rD51Xyz+BSDg7OGa9Uaix
84uGsnICqTwDE5MPMcAGOVxVWvzxZlbpbfq95m7ETG+EkdCa5RTifH2bpSdI990JIfnS334GYSLY
sPabGNw63OBszZAfCPPIsKET5Q5jUc3379oGdHRVtyi7rRh2/bVCD1ZA5BMe8HlDrJHzeM7Fq0Ul
Nw15PBUptPlUiFtdtBD7WAZjX0L8TuBmUussbvMQsSKG14jxSkGgT5hSTJNX3NWHg7F7MCtHnNKW
TZ4xxKSvMsJ53JTgmmRzQhqnGzcLdnnyFRXXzb53CQOoKZzi3SwSaiMbjXKakFRiLKXyUIM8P/ZC
ld+71Lrh+DOH5qwaDb5pCP1OhxuszI93XRlKFrBI6qo5G3R3/cRLLqMYl93GO817+U+ytiXD5EcX
shU0SXGJoTqCNz+Ye2sXZuEFLZ7obO0Id8hDH28eXVj4yLGg3deQefW4KX5ZjV8ejuYuyI6TI2Vt
bDSz6LuhLInV7cy2XJQDIpHFsVLtiF96p4llDIJavxddm/nUIo+C6LD4qryaplS+GXXS3vKB62gO
ksMZZKJWsZ6NMOOgl+hT2lr1w7iekG2rRATg2Y9gXgPk3QmNnqFRLqLWutE/WpqIw+iPKm2sKZnE
+4uwp9UQFzuxfQzpi4YsTDkEZylkiKYYeI0/c61tqyLcxtvryCuazUOBJm1liEEUqkZLxkb0w2nK
dwG9kn9oqfCKLDcazrJHmbq99NMMoCetjeFO2gPhwTp23XNXi5jGRX7YZiRsm20ey9LiarQGu6pX
k0DnTZ+EPbtBFmkGrsv/uTuGVOVWCmsvTnDmvcI48mg3JvkqZoo/ByzNM+8Kurjv3F9QaMLW0MK4
5MYzFA9fAHr8Cl1oOsq1IZdv3AlmKR0QP74zOsmMDL1l8ZjfcpFquREIbC5+uHQ1enltGnF2Qo3p
xbmZjOhbZqt4TpfAf4QNsnVAOc+O/aZpiwZr0o8jvlQMI+iejdDnYkR7FtVcea7K0iqj6MqtAcU1
VEoi5oXnJkcfU022xg2fvN7+L2QMMnCU77zSKQXdQ289JyLTsekFmT77uWPrDW0abiD/uYoXnTP5
By0HeJkdmNXrdftWH5xLITROlGjqNL//CxvFUrG0G3Gac+EsY62WzQ6U4Yw6brL8ag+TGfXr/E6/
lVBOD8rVocb/txXr6E0GBDVArn/N7Z8f4ulzOo81ZLjHfV7k9llYtPqTuscTA+Ct/aapgCQkebqq
F0TlD3Lv0TE99CQyF/4qYa6dCuLESBFigjF4gbg6kg83CRQuUq2MIguQ6zV8IBx1VaXFQoqOngYq
iLjd2KI68exaoFvjOryvPvq/VlzaY5O8hmoTwuXF5uxAX0jPfdsiXhktvYQTuy3HPNQ+CMLgsXYg
xh/gbzJ2v/3RUqBIa01Rr0OSFwYfL1ydFteH3P6BQ+MGL0S+Jo8QXs9d3YIPcruWR1o9SWvonwB6
3l7dR5dAVBGNuDuw/ySLsYo2qfaTUVQztfb0J7OlUqnXlZi9lUvG33k1ZM8XVbvJ7qUNqTw0C5CG
P/+BllJpT1/CFQq4w8eqQQaLytta3bLmGH+jyw7Cd9zkwMVsfAqSwx/lUI4XyPLCIUup2WD8QbXz
Bsh+W9ijerYYjHwiQn9uHhpn2uGzrZWB0ofpg36cgAIaFfohWjNsHVXOoomKpWvRJ0YCUNkD/JKJ
zZ+9+XacVBQeDvgXpOi0qfwW8Z7lma23W7k8zOBb6I3QGMW0AYQtB0ZlE9rgXbtvL4igPikcnYeo
Y/dEyuXw0Bm90MAHKR/1yizpmoiIZ9zxjnEyGT8LFrAz+t6tDAZsLi286zcpY5U7FbU2g9JzZW1u
66l7ZtSlRp6pHyPqbd+Q2q4VoYtrwPVF57m0uunuGKVz1u4Wjiyqs9v6cl/GjCrW+xgH44apGU3V
awlkhsNaM8sUOJvzdsbLoIrAWKv/PyB8VoG0iPrm+At+5jQXCzCbd4YY12cfTTkW1NfSHZSk407H
G4fszLfF+uid+tx5KPh+liI1RVp9pvo+J1eEc4IwIcg8DaYNkcH8r1g6bC5e3Lep4TB1jCWPJb3t
bXgzWdF1Pd6ST9LwQZZAOETiXbaJG5pcX+8QDH6Vv+vp7lzG0wWZaEffdfNlDIlxTc9LPPGulPh8
YH2IP6FIaBny2CH+LTwuUPD5BaC8QErwmQ4d9lQeOr6cVkymwW5uEB4eKpjA0/QdJhlvEBFosByT
jkePihE0NGlwRVPtI9U0ANcphioWCJzUZ3aumgI9Y1d2sEzY8TutcYKSv+aVQJ+VX8QxOrolF6fp
iTXqrNeFXVtaps2bJrGrGTKgOcyF9oPYWZWAg55Y0lswrTt/25mF/25jdZzK5H8TDywLy5/xyYRA
q42KynoFLX7JugHH3nO9DFPt/kU8l/52BXUvapqpEW0CBda8TMSeJdh7dJKYZR8tYpuM1hYIalCC
6Y9zukqoVW6hQ6Zu2+vH6sIgpbIDMKpfvm32AUWmVmyUv3h6YKpYAOINpqIg9HWjeFBuj5z4+7xX
Ae3Flum4RGtp9XxF7vWwkBcOEeTzgdLCm/JP9CVKDnAL0zJl9P7P0FvZ9sRtLgdDyV/zeBW0kriu
xanz8AAsn3fITaHB0t5XuNAUGjwqTojxLt2sdcgFsFQY6gsgRjjYcrseOPEYpnG/W+EGIQTd9tm4
NusHH9dMVUX9x7TQmxzyXzxtMoklx8D4vDgmlfS9Ef9otLZerzM3E0N/nCaIFdiouAHVyPVvRsEQ
hZrvxUcwvdxgLt12Qqbez/G7Z+yIUuOxMHnq5PiH1nvoRiAK/859he2t2T5SK/IEi+07LG0v22AS
NQ99PeN15Ism7JOaxWiKa+iefq4T43Yg/WJ1p/wQ65Bw9rf3xUI7KuaFnEXB4kNP/g==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
