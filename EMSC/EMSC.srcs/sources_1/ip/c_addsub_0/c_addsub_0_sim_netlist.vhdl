-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
-- Date        : Thu Apr 12 10:08:38 2018
-- Host        : DESKTOP-0KA73P0 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               d:/MasterOppgave/smallsat_prototype/EMSC/EMSC.srcs/sources_1/ip/c_addsub_0/c_addsub_0_sim_netlist.vhdl
-- Design      : c_addsub_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
hZClrjQL2xcvpkOw5/Rk0YfU4cLpkzqVyCWW+vyfGfIwRXry56MPNeJiKWSo2kvUzFNpnQa+st5p
3la0itKKWw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
cJPY46dnrBJ9tXvtxt0uojXUpj8Xad3TGOGCLvqfcn4WvGhwrOJZFUe/HwihZ6YPBs7rBkt5Uhyh
Xkm+k6ryH9Zyr/Cf0z3ghL5tiNSKvqVnr07tvQetVbBj1mTMYyrz9PaJbZ2GSQ3ef7FulEtNjb5d
Ef3ip+c6Tj3HkCyyiY8=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
TFQ0extCZz/E4dn7YXorUbY21QuuDSbveq26pUf3opJgYPyzCKX0OQxTJaKwiG/DkXlSQ4470vtG
F64mjUEEXYREg0yfX2fIKjT3/pF8aLzpCfQ1udOc8Cqg/Nloo+JsSd2tPEDJWk2su97x6eFnk78x
PW3TR2MiO42VBivqermCpO29mieSZnNoskYUOHLuzvhIR/J/cMXMmiRcjbEh7EJOVeq/jItPudpb
5A7hITRte89rFpkFg/VWLnuc5MEctO7uT/RZTQKLJOglWXp7f+uSlAE8dDm9YI/IS/OO6o9HzTnl
ZjoPWmmJNO5eEka7WEI14Wnl+k/UI8CLPr7knw==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
vcA6tVBzywmJsvOGZta29NRAU5c4+e//Nq67cIVRUCEbQtu/TBzGuVvmTJqBcU2b72sDpgdn6TWW
HdNlgPm1q0gl2L3X27zzFiw+iTqSprZuK9pz0e0O+7oFIGbnzvM76Betk1rhRGfCV3NKsrQsUZ4u
rDVDPXN7BJIa08/V/boRGCX5871PZGtOEHw8dBNIr2CfDxytdwsQYl6TPm+s32UscdK1DyJij6yT
56KtqClpqYfV67ZmyPtdLKDbmf+XaEr/i5QPah1raC09d1fb7MNxnT1kH7oV8klk6QbDqAwl7To9
5v+jCauuNWvCyX1my3fzbWm8CuK5jAU2vXrvKQ==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
X0GXe4413l+ZBbytkXE7IOL3xzGFtpeplzzSs/s0NIrsuzQG59hJqM3d6liI4/SHNkEwiUHF5fcA
qPHT1aga/AfSC2fylyJNGOz0sQfh2IYqtvq5E9GT0jShxRibVeFndZ+Y3JIt0LKOKaJRH6y2b8xy
6wfF/6pZIu/XRu0+C7TwIViyLBIOEVkhGghVsgslnz5RcdCiMXcPgHGucu1btmub7Xd0v11aqvjw
nRQYV1gduDrGtNJFU50Dx44Rm8IdndMJI86N8vZpVgUQ/OMe8SMOXjkeT2h3y/ijSSOtaOGLwc4J
4FjK+n1vUWs8aoq0C7jQl8iaVQ0ALnmzBmX20w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
em7m0sQoFdMSKYlp8fNnnGHtha7+YDLScVsLXUfGGlxdfLt8ouCJKEWVOgI2bd9p+aNlNqsE2wgE
0TfwWzF8YzQRyG5k4D71zPHOQYn/Jz0UmLVWoRmjot05b2PQFE7C+HkI08wo5c05ZZCxl6GDqV5l
4gtb5/kTvmII6wfHYVw=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
jnQk15tdONqg9/ukBwbkokOqw7S046x6VLYIf5awLeVUFwP97gQPSyGyxab08piQmf8PTrUAKX72
uf2gl+T9YzH+MSUDS3lz9X2ZIxf9dJ952dR2W7jJmggGx1ffSB14bOmNaMusHDQuFAc7oIVIlV0N
BQamQACENzbxrEWdKe45iLSoK6YHZ3irufuSJGd0q0JgQk5V5ZCDAo3EeTV45HBV6fY/7cH8XdgX
13Oz8nv27TkWrLmJhkJ7DFi9uNOrMz165v4vI6iRZqSkOSjRpL7Kc10mXKFv7RY+K1N27WQyNX0l
GYRoGLAwwvJfLg9SAlAh9XgCAb9ZxD1SGt9wJw==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
T5T6o8GBJ3bbUjsxQ+68yohFGYD6dV9FVG+5UkdknHnr00jzB39uhQbOc+D8pWMDH3IxH81tkkDo
YZWz8Qhr6G3IzCRh3z+1lu9WnH1ddi0ptwm9+wn00d6dYbGjDaYLN6jZnvDdU/kg4Ot5qEtsQpkF
SZOSI6BRM6/kKiu5LBXdF6s1E3Qjp4C6EeDVpPD4Tz999nGtAL5hcM3a0gvKXl05nKjayQGsd5pP
3MEgI7uB6zgcDcSMhXshQCjgchTYSb+ek4cif76ypqAykNFpjvJSwywA7bBTJPu+Xvi8KLlTqqm6
kBN/3jwFMTaGtMnr68OtPSL65oCkytQdvVnfVw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rHIC9sT13rF9CzruTvi0o/E+nkJQcm6rqYV4AF3/1zO1aAcMWfA783c0whMuf1NWtC8b8jM93Z4A
FXtDmoPXp0qEvJDmd+ESoUgnelmFw2oxjsXS7WpRDco8kylvCGlV6k1dlJ9L9/Nimwv1cblp7I2e
H+QvkPkMKwg6lJa8j9QVJNAUmkiEsJUor0MCBLGTej68xeALPVb1jtIXgxus9Ey/sJZRK9geO8fg
BGnNKgTntGdVJsjcUdlUjDmHRLRQr44g60edop7qpGK5jkyCBZ2F4y0FYqxDiRz8zj+UYLyYeV+Z
/ZwpM6GNg7pS4RZ0XT+6rgfJOTYhlEExOUEfFg==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18144)
`protect data_block
YhLDZDjoPMRloucRukRE6P0RtdDW4SE2qXgwRZUMWs8mqhT4CALYbztssOESkb2R5agjOw5080Av
wk2BUEMEDFWHK40F6t/TvHAvgnQW71qJyEsGsSgmItrAaBySCbcnmlxLtwoDv7fzsD747hEsECQX
0Gjf3eSVHE+mi+lI+i2LF3WE9F8nMFRQ2ZrXhqAJCTGoxuQ6eY0weGw3Exq3iF1JsBkE3RaLZT0M
RbDsRdd/3mixihMiOykyc9XalCC4xaupfLgoj0L+zQHrPmQoqB6Rr0GhNqGYsExQkmxEeuMY4O/w
okq6c/T51uoujIwxQW805nvsikMatoijyF5+ZR0czzg3k/hVhZNkp2wJQYedKc5T3x6wanwv0t62
R5GkghJdAdPfCvdqYvbsxFKbc9lO/Yn+MRDR8tawbzHbBYbAlYlfSh6e7vI9TX3SoeHlWorHsidN
1PQrFSshiNMYSWhfVqZTUNbUjhPatI885RXYhyigjdaue7OQtIVMUpdYhlY37BYNEsxRvPV/btI9
61CSo/Zl7swYEwQ0qpXisG3L8q41Ma+qv3E7Ifjk+yiAM5etq+WkUWvyImdE9Di6wnopm+K8vwbM
IETmfyqVkClx3oCzVoa+kBwdZH7b7gKcD41hLrou79in8y++R2th+3M4r5x3uCmMc/woSUDuwUKB
WedkQHhbirkVMiJFiqSBDuoaZG8LUNUFFro0YhrM52ECqI1mu5O6yTcpZREPuu7/QebHUR7AXG9O
Ar/vmO+9/84i273zgCfSJBLcM4p5Qtqvk/crJxnDxlkDULmOGCjbHvoSKTEQv6I6In+Vd7PfPCsE
+ajXiySW2vU6/NUTzIL772D5EHqwplLaMFNAdq/4xZdkPT+5y23VkbaRfW7TmHRSIxvOIcoLTmOf
IpmAnX+Swo4pZvNXs76D32T7JgGOgjkIF+5GyU7PM2NPkxqMRcUoiJby8GpHRqB5T0R2looLQI00
wZTeHd2rfPO3do9DTF+xx+1roXFwdDWYMEyGObAQdPP1CXrkmKj57V/1nKYPRCOEgJw7gbvJqpOz
3USr0mTscOaa6HurTlun1eK3MoJY4lWv1z7YlAaPEvjwP+nfl3tNuKfdllIAKHryr/eT4F+z/Uhm
g4GdVNjP+92pP+f6uOWJrpFJ8ZPZjQIjgq2UMW6gzUzzTcNO1mfOOvcpZIHoBRXt8PcktblYmWrf
hvqMDzyKPG6Nyv3I1yZg/sBdYVPMLu94j88ryTjCMJEZpy3MHGAaZ+bVY8RYX6zIrUn+cBsNUVF+
dxkAkIN/or47TU+ZOUNY3iDxangTLqEAL75zVvNoXPr0Ng8byptnZ/J7g2goX9Pw9in03WKn7cEi
zertsEm9OIaaBL4nGkoaG1iMULY8yQD7XQBuU9PmPHqJQdkpKiLx2y0KAUaUmIc2IBO0n6KSCw3Z
LpAQCPtoSttK8dCLsYWFMcwwh2fNKGCrniF1YQUZNmMOjmHKizcUUKInfXqBo+jHRcp5IjtPW4lT
HKIwy97qL+3pa4qcMjMDs/ZHoYGXXYdC4yeqjBKFAqA8tArjQ1RLpJWu4yMW4iqZZnP6eoKdbTim
1ASYj+yuSMGs6HWugOTbBKs8eZ32lv+j9b8EqLH/DadS709MSUOVBiyq3Zzhc83xfqMqrF52A78m
l/WDotsgVHoOqGc4VE4E1z/ml1PZGTDdCXJ8xpjRaBo2LBOeSXCRze4qtCNYu43O6pdbbG+/Z51D
UPPuQ91ODzPVBTENjG6k0OnZKvTObJFUmaKSTzzDaYPOYCqlPUof4QQ6zJId+rdkQwdA2aFMG1Tu
GaQMdz+BLV66xHnyM7j5TKw/ZkU1zl+TY0AM7q1BpA61bUL6dw4bjX5kAwZ7VQtYWoglbnKhPqdD
XPZOex4hswvK0EkjfsMuq9eBPZzW0RY7IgD88ev6vExNlUuE1Od6g1UJ+K9sdVMb5iWQatHrfsKD
xlYi2X2954CuCe7J7LV9I6QJkRTLlsDhSy/y+QK6cbp238j+mYudke0Qk5IDqlvfCPOsbHMPFXDA
kxIX3eblpgVNw5u1EuWKrBoFomOx8WBmHY2l0bOkmdsW+zn0ohzMld39GQwOSx7R2zXidCWun9dp
lZ/qDUaFeOW5MWlLZWQ0QI7ZIQcZlqFQAPNE4G/+6ct4GU0dZeD+Oj+gFj7f0ATEwDB4BQ1KrLis
heOolTrRpbOh/6OTAbajdbN8pjpGnm2BogTV70ZKGy24ub4G/VDteGqtE7+ZIhnPP2awp6JWyOw8
7S+urB6uoLtHx7eBQjQO7YAuBv6zENYZfnasV/saFuEktrLtsB6rOMYSrOUD9qiSmtJ34M5nTLTH
S7u1Kw1UZ4ue690MsxOM4LBHXy31FexH5EEjru8bWVK05XkWF/XT3lzJoG1U+J4sacUHIK8xXVkT
bpadLzedxN2vY8Bf+vLneNETAze7HLF7eCqk74g4XKPbJdS7zwAcImy0pKqhxkO6qD2X/RsQqIf7
22PkJHcKa1e7gEYlX5CrY7KuJKjExQggL82IIFhASk53bc1jKdY8Uod7remsLNijLSNYxZOcr23r
+JeksfBy1e4doyF7VxUatiWhWkz1/+fHSu5bdI8mg39Hm5rOvb8KaxzwXZV7Eib8ttbCU7AJPBPS
4Ea1L+n++/MUmUJ2W1048OZDyebiuiVn54Ddhjv+R2V9yAgUD2YV5HTkjyvha/B+A4ZhTZKquGRv
0Q8Ha9w4KYgX1LjFvzhqfBmq8eec5DdSTArFLFXMn1uzGIQMlCxxFOl8XlrYmLugMT5yUolaaKOx
RA8D0SJOvGjAJd14tzgtgSWXbrAoMTn/TGfa/lKhhv0BfXvJF3EyKOISnbAklj9yORuWpMyJ90Wu
0sGIX6WiKFJ5mbeJEwgvdj9wjQv0Gi1f1XjAWnMK+x/OxnWRGRTyk39FYzNQKn31HYPDkj3t5FmJ
GRIguUbeirplEFqGewkWQDcftSN8KmH40Oi44lwykyficesGv6DqhKI9K6rJdmiZl4hejxNvHS8X
FDKslEA3nGXLYPOzhwcfB55hVqRmcvMMY0t4/lupq9M7dyRfh4kUuGvB8RJpJSnMepLyvOweL2js
5LZAW3qXQvd+xAoerhpyot7feGqBoyZUik3q2cor22pM5kqtqQxkNpTaY7NAVY4b/P5knMgz3LE9
0apBKmN7PWYLtduxOC3hMwA63TfUPOsFe5wnNuOYH88Jo6orOnMOWpyUJs1UoGZ5aOiikhKZuZY4
CYnwjGM+yoHu45HZSAbEYzsrNBI4tUmGdkuXJufKqAWWLfIcnKWAcc+JqZpR0glhl4u/EeGptSDw
QYOo8SC0ULZ5MkUmxUH2PgWPVeGziWb7d8sehMQzdgjiGaiDxgJ/GVkPuEeLPh+DW1O+od4qp1HM
rsQsQ10SZ162y3xIkudLHsV7iMCcp9gwOD1ayx8FOamlpI+0cnRg0AdtFXMWbFMJWInV8AyNSmZl
trWYGOh1i3lRl4eV6P5YymvN4fQ3RRCHPCnwZ4s72xAbEdIVx5OTu3ZBsrOm6surivZ9IWFzOpmz
MHB8uLqjvwpFGhkw4wYZoonFoy6ZZ5UQv09TUxhytIA/1JYK+D7lTPaJiK6fcn4UElTteGZUWqUX
9okoSWrEZyFAF5vTZWM4kGXorrMoDVDg91DJqcP6cxOm+LiXrzebhQ9l3SLB+6ljkqlrrjcRuWoj
SJq2cEMBUTi5uwv6BNyK3j7DE+00NJG0WXOKno/3WsCkKobB4yhnYIHEXqCKGa0nqwSSEq21hz0Z
VBzZXhzDC96rPxV9MHrHB5AciTe60Ex/hWbLJunNivhxL+vYD6fAm54+8Jxnc0ykLwRo7LD/5Nm/
gXtgr8+TpP505AFUp4aZZ37k1FeUdMx8D/9JpeACIKPEWMrEmNV7m6Ot7ehjFTX9sz2m4hxAn2OL
P0FKrA5CjahdANS61gzblCw0SIejA/Vj3C3TLjJmC1NF7v6aDSJH5jnTMxmojWZeJDKtem2s+uB5
6Y4aD/kW9ShkPzjbD6FZDby8udjgPiYUkJNppd6utcuNSRCPqFyL3gjuh/UhVvsju8l1W5jB1FGV
EgKb3z73BvDSB131JNYWzzzaWLnHakAWoFqAUguDOU1O2jqsdiAzzMMubkNTX1lUdT2+SuCcTW/o
r58jmf++ipd8ZTrInwkddyi7erN0hrf6/GG1l+/3B6Mx84EbxEyDqBbfSIYAlggRHvb2x96baXaO
wi8/UaRQl7Da8yogvFOloguiWjSazHhHDWM08k243T2B9Zr1fON51/P8KMUGA8rd3Ka1letDK3+c
0xrF4sA8DMSkiq+K0fEKnGGnNCzM6t52cNAXQ1AwjyzilXfppykKzH99XWXqWdXkI5exbTuuYS79
0qf8qkML6NF4/OmrKlKa3UH9kTAF1jXSd9equuPelCtztW9ImWcL5tXul/Ko/0cpRNJjUffaPP8n
1oMKn0rvP4zdxTYzsH+34Hac4mUabMisqZAzmeiN1XzlkVMxIavmPo/08Fp4EODRwUU46KgJv5lz
gFnAPc1L8lCKuFK9hfrk29BnPqXRrTiUfioIr1+sjpiwUn+k2PmKYi0n+s67bqEZ4KqLBRlIUXFY
X7XV7JwOc89NSFDrya2Y9hQybxcx/w0HO80bY1LgqtlQugU4kieKzZtCbXxIO42Rp0UgK4k/574C
R17UrvSjrLvBgn0i0nuqFGTRjVGT0qi/GxPsZJrP+F8zyzBzGmL1KBtiYpGy57HTcQu9EesbkeH3
e/3x9L7mvyujdqF6O2RMKH7ITsKKArmTlCs2kRmoSp4a9O6w1iB7JMs9wRgDW/0VR0WtlpHbLPFo
CoOBrP8PbKDgM/MEqtucnrTrtjYF6Om7HHw0sS/DGb0h5tyor73rKI9srraZk/H9DHn5083+JtVN
LDX1+1hQNF4d5z7M7LsYR+GQhV2qXSj2vPa2owYYp8bWc4WGGHj2bAjTlZA9ZX2Jznp8MEnKIez2
Phhlg8rFF7qhiL+vGx2BP0417W2LHZCP6dT6yQwhISRh8QBqUjuN3ijv/VxZtfJZgsh51au86gJL
tGR8SqGT8XPiLcLKHOywioP0b+z2cdjASiVW1MHDz4nydG6274G7o2JBjYdSHL9ybwxc4ZkkdlqX
BFhVYZ4fIFJYwtFfCIv8DvyN2ePyKkf5+TM1l/86ybObfFWh5L6ITnPdbBmYukp7Q6D1J4XrRdDd
07HPZQ+PFtbbyQggBrmoL4a6rsuVKE+FER0Oux91GsJ7m919VyQGcwC+H+hufTAK4Unu+8nheYKF
v0Etw6J3O252jUNSG3uJ6Iw3XwA4YDc+LHsYoXI5e6F8dJa/Io4qrdyenwOYIOguSHla7gsQY2yH
aOiVNW5dVwIWV6um9fzKCAQwOFTeH1MArHaaZl7VqIbXyUeqZpbtgsg2Fai1tg8E89c5pPPOk9zP
nnZZqY19nsK/U6iIpnRT3fraoHL9hEVhf4sIBLl5YuBh778Zy6WyfDZyf0QPwQm48Vf+vYFweKa8
43/yyaJRzkrD9FbLLdBjBMKNoxlFUYDKVonw1XbWaa8d1VHBOCEzdKAgRqEu7DuKO8Do2uDuC3r8
6hnWUT1KF+Ax35j/4ZkecWGA/dnr0oLjp1EGcV2trJFY11iedzWlWpU8wpyPgv2bKLD2kOj+Reo4
xzNtmTumhXo5bc7lv/M5m6ESjJuokcgy5iewJjo9LIZOn1ae6mIDgYxDRh89f2w/x5O1C1Oqtb8J
+8WLI3MZnRFakLbfr7R7rl3uMocOn1IlPORP7q1YFzYYRygsoQsUqwIT1obiGkxPi6CbrehdmDDp
kpZS0h4PlfI1aT4inv6Lu+Z82TQWJ6RNzAZX4B8Otl5kYtXNoW3eVw1M6eiqxffEnCwcY7q8FbLt
wpT2UD2u/Y3SL/L0zkfYV+KWby9VkD7/L/+RW3mVxwrz8RHyeZ5+IkTiTTmZ7Jgo6SoZlJB75otg
5+eYsam795hlM6UVBNFTMhIkmnp7djwA4qrmebtRgp8R2b3wiKPyyt0FbKq0ckMkiikLZCPzH6p5
pyxEC6xJ0Ukwudz0Bw7Nb/bxQGJAlN+qT9qN5nBxWjWJbIZnCAJz7fx3hKlS9D/x4ZTR8Pp1nIKc
/G/Rp7p3X+7shR/EgJIVk1FqUBB3xtmS73ylw81vk6q5Ph79gsvCl8U/8BK9OjoIramyrWrEMSG4
5CliO3YZLQqmtuqueQemR669PYQPtzyUrc8wIIM6FJaYor9tBa5GdkS9b6NUgYLlBpdn3MuCeXEY
7QzvBp8zgfLlMLu1uK2iFZLXAWILLI1zEiS6PY/DhDhVJgwSqsmx59gDpiSso7EU5Snt8CA57/oe
+1AHLrZjNeB6dkmzc3r7kmK9J3D0Wt0GGztCOIP6egRb+BrTuNXvjyxFZC8B+1ssDx/p8KXWLIQ5
G+zS4GfDqoY+bDn18pJU6f9PAejD+vxufsoHFfsrxb7U2TjstGdCyybiTx5grjh8kpBYK+SvrXwN
4jCZv06/lI43qoR28ESbkEp+BlIm0djsvJGA86RDWVhmAS9ZF7O/FeUwP8NNUC3pW9iutst+V/Ja
Q1Wn3UEHaTSZ+OSvNmoTSW3bIl8nnDkxKparVsqsCDNMlz3Y8lQFfdx7F2GTRw4rzTl005pHYURm
dJDj3pGhhzEzl3pXA8U1f2j64jXhskgJYaEqAQgkLgjA8HHtsC9ej+ix02cofGUthMY0OKyw0/LC
495nTsKeaOUcAQ9ZSjDnxDPmWWt/Vin+Ex5mnvdFFfz0wafg0oGIo6gZ4YRqG80uIoJf+Jgnt/L7
IFrD1tbinIdlTRDF38ybRVvx2UUQ46LvtpjpaS005XzpZQimuYLPMKBY9Uy8zloHRHftZGjwYT8o
6Ec9BGRhcdrEDQk8JchW6i8o21S3vbLaNYaIbHd6/mcLmHVSrjdmnk+PZjnXfvi1xsqp0PBzIX/h
89nhXo9NHwZflBU8Dxm3DJd79Yd2l5tjIWTy8aJT2pe3IGmrpiMGB46638pUm1PCkBqrVOHBMccu
baworLLLTUvw/tP172TA6tCNBmS3lkPO5yVTavSx25V+3q+kiuDVnAG4Zc0fg9LpFJHBcMU59IuR
9W92wxUStmEwtJ/aj3ecxsfYfAfI+5rdAvDhjuCKV/JtuBzys8oO83P8D3xQhKPtbRaDk72hW40b
VaPLvoJRLIU+MGhwnUMMGunGR5ATgW8wjmXIHXVm96V2uyKWKB+ArRl6X+C+U8sGxeDb7ofkIifb
cn98EzLsmsXl6PeQzStHLtFhmTmVD5vPiKp38pbXoG2gczcZwjqZCVf5K2W5DxBp/x/yk6Me2Qnw
bB8zT+lIdTbPdokf/HCKa0FalXEvmFcz5ofgF3TDvBE6EOyi4hNLXG0lzSw+yOZGlJwVBUDM1/M5
yN+HzZUI82A16HA9bkCd7dBpuJPlL8/PIMFy8VBJK0igI6B7Lx5rFEffCnNTKUQiVXBsjE+MJaxf
s2iYYnI1ytynNyIxZq5tSVLzJ5Dta5mDohUjqF2Ic/opJD6lGVR4QxwwN+Tbc3GUWCje3u/MntgX
O91AaiiRMt15iv8ZLaBc0XHCrEbmOm4DS8Ucvhe+GoCCjvUfS11m1ob4j9bGRmZYCeGW6u9n/cch
g00bJAiaEVFC0vKPPABKPemGppOkNNZ5C9+uqjBjLGCFo60CiH1DO7TMkx6MqyE6ykYYrSyPtuPW
ohQtCNMn28YqETeo54pqTgbiRc3mxSJxSnI8EcUOvHFeMg7f+k3vQk+bkKo+CR8TxSCM9RUpCoD5
iouHgSG8aEX61IYR2Figsnu5FFoREJ8lHq2IqSzdI7T/rQ0RrFY/bBX9e8m9YfBqztlSM3YLrDho
hxXSjyyhKGw5n8yALWt+wPfXZQQAxCIncZGpMv8n5Q6msOK0VJr0DEUlzl3jdA4LrG1awifrr3V5
Fke6hGQhvdj/5zvlPIqVoQHZuTgogY5fHB4ryamOGztmo0zRe/7bEClejDrXb9U9hoh6vpQyE1wN
byLcE22d2jR4MhD8hAYqrCYRBUGVDv4EbosC3Qpdafn26J1dfOGfrjpXbFciiNupCbw4eQhkspko
EdbRweQWFFemFwbbcFr8897zHHDGf7vzIyisVTANtNaSPjWtVaSd0rjTrtSlWyAQy3dN3Egdjr8V
DKbhkS4F9fOCcLWoG/rd5NfIIr813B1IApUls6iNefPZupQ4eLA2T91p6iqOACSeO5b7j/YAkb9S
d6IfFf0rYqcmx48P3T8EHAyI9KGmbecEKgWPfHF0CwW3lUaPN1VTDprjQ1zddygnw2lC5NBpkrHW
YYE9ltVZ0taKK+dtVxdw41X6+EqhABGR1Bk5PYpKxSpfyZcIVN5eugo4AcrKo6fYb43XfulCPBUm
Hp99yv1yybBMkR1u+c/xwr8u39cdObR2WKUtA+TCeOVSMM0YBvdONxfK85vpUFu53OczTa3RFgWI
hSwuGi0hYLMF/FsDLnMa2twaZeQ55ZTQNtapnNkHVL4iaDh0fhoObB/+HUHEY0eiu9MteVH1VOpd
VbQWk3IWZLZA7ndyUziJr+F0rIJ9tZ4VrrhMztoA4hyvc7XpmE8Tk56yoLVcdtmz+qpvB5n/f3Zc
ORgy8rmfOMIgQd6R7xnu9Rubu9a5eEksgpcXsfuUKayin3byPapOmqJu/w0nRa+ZEiRlFiMkeCrp
1OSBDPZGwzMEXTXAqbppH41aFJ7VbVbjYa8QAwlqccpuNTZbmykI4NR6OCuImC1ywqBjmI/2t0cR
gXh/LpzCUFtgYwAPxUT1giUE+9t7cIeAgeeIRb+UoLlpg+Fetn3iBV4+6i8SmGUkAaET6n77fKP9
VoYF2oMB4NyQwbvJg+nuOCwAyC+vyg1/+h9iHrdcXDF8kea87QhqPOs4V4z1NCaWVJa+2fZLDwLo
BB3mV6UCvPQ5dlYqjsyOjXT3XC3Be9ENQrHJe7OJCrs+mfxgwMmfI+fwjsyDmTS3/R1Ls7MDd1NR
7Unr3sBovxAogfwUXci12MoM2aaGPTzQCH1C9TlRb3/9q6fjpMuqCWnb0gNjj3KzntqsSID+qsSr
TtdZNbuw3pzWw6FbQGsgoXCJLoELDfny1PCL+llCnU/BPO7y+2I+cWofB1KJ21T/vqLSeqA6YS+9
35c+k8lNHI4dgKHkAnK5c+uz9wZkos8fcPVv27B6c7HvMskB58kg2CujGEmULD1I0Mh5y7vr80Et
z3KXM5Uz1JjSKsZPnxJMqrsveHNNoRfpNqsVYJfAsIDSMHL18kFaXx6W3BO8yY9ViCq0FnggCuLc
j9hTa09pvCJ0jr8XeUwg7veNNXshdI/JRal3jFTpsXG5kwSAgdJ/kqZO7aTs5tkt64M4otwDsni2
1EIIr67KUZndoWfKHUtndFbB4tVK+VLmenafegro9Dsy5nnRYZdM7nQGq+BkBgVeCKk83HH4xIf9
PLZVhs25GY1zFLHu5lEd686SnoXEAroopeBUZ389owI6vkdTJ8Uq4b3e7NRT/6jDsimVWeaSlX2Y
u21FPMQTJmWysAELlhabVu1RLCzBwkYDCRiomR1crczIulrVkSMDTeevnXbdSmTbTJlfvEcz7uh8
uGJLdKh+aPJvLdAmbjDA6jTcLw7NmdLb4eODTrHKYUGTYtvBLjVHbiZpd0ZT77K2YTo0htFp+Gy+
3tBs+oB8kgUiky2uc/WM+TV6eckZjvp/FZjdK7eKlCTf8cgI+rVH4WHNp7KORPjmNgrpprRg7oAF
vjEWR4a8bDC9pxD3y8/2JzIZHppPmvcBxt2TiYuCfINeD0af8eYUYwGNxdoIEr7qN1+UVEmtY9S0
AkNFaclmVJ5NU0cq9Wf8aUm0MDcXvze0U1fCVAZ52ugIi1z3olswAWJOtpJckLj8slOMq7YLoCly
cz0pGu8+uGULg+dEclvcruz5T6KuTyJTdJEfVQ2ckKcMBHM63JCci2D4fQXUm6wU3eF2p8HEetVt
huw3VNru84K4o2cxDd80DhKKNWCSQzV+rBoc6BUcx3rDQpWgMZmHF+SERT4qcQm44CS2Cc/5DeT5
XBMaTRPA43wDIVXqUuKvDnQMVT36SPBbPygY56dr9kEYctwl66H3a0cuD4Fy1q0U1wICnDdcV40f
7gSApAiKCfI4aClcvjDNZCMS6UaA+vo3oP+cbXBQ59JgTn+FGoXRZGEBI7ddbs0BbeXSSO+Qukpp
lTN0++mMlg9hICriGg5RrmM8jKDBOaCJhhPZW+mQJF4z7njUVMg++qCDAATaqGdTGJi7jKdliSNQ
t+2ytR5P46kUsoSgVvRch91lxja5R2s2fhRh3Cg6rLwvop4BUArSd9hM42hQjrHRtutMb/nRk8c4
9dPphw3DhdY0YNxLmkfYasBO0QCZSzUc7uw08sPshifMTpmiWwVU+IlRRasGloVx/oWMWAWSYUXl
pC2SrQTCF52OHSJtJeu07MT9IrVNMNZZyyDogTiG+lk76086cOq0nDwOPrrfGj0dZ95PIfKFqbtb
0wTL6s2jNeyaY8dXXeJh2Pe1rzDx3fk/KO8Rtv79VsbrZ7U/qTC5B8iuJrzP35znKpiP7Z7jdKvp
FXES+jHqvT1/2kLqHqLi5UGPPNEzCkRcrxPOdDP+9EYITQwTbdjCFtD2JOddRU6j+CGCIv9sKRFs
7IQ224b7Ua5lgXMBy+l+cv+h80cca0u8rh+vVzBk0hr8YmvgfxQLOLaJ8xDMHRuNY0vjktgmuPvi
Yg/6A+wESFjX7kddHPaVQ7BquPkwicJHBKYDYQzw/NIJskG3xgKUCO7q/VqMprIzp+GWR/ZGi0bv
nTb2gwmT+3vrUyasQtuTSVp1CgJnmU9RccLnX54h8pP3NhRwDdO8ZQb1j3sOUHgUfoJiap4q45tP
0DcnL7thcwwS9NDj5N+ocp5qDxSTln3/pemi28ItZMxiX3IJ88TONwvcHPYekaIgt5lK++3Lbp7a
lXgQC00DfTJKOwwctVbNzCEoua56Y/ULiyms4X2iI96LYqvny1DQBHcs9tg+ztn4PJLZVC9S0DR1
aD1b+e10uxPFW0zQA7rjkU+C3mZkK0Gf7a1qMg8IQufgSWAlXsgH18vsnU04hfewyNsPfB5mCsA5
yLKT+O52sf1zopuhTx2S1+6CD1sj4ZI7Y2GBfc1aSh78oWCMAao7CZErDSSEaSCRBgmRhCXIODrd
cSEXBdNe+yVn6VEAXae+/UGveXxjKnZjoaT60nXXgLQK87r/rlZcbT5XxX5StVTP/wRoBV+kXAk2
kZ2WeNex0rW4yo/NHDZHcFOcMXVVjH7OGW03yWxf8pqnk+A4S37T76jIaG9UGE4Np9VxnXaK0p4a
KRsnefmzBjtokigYFdWECqpqvRX32oGVimZiyd6W3oJQ0FQnPLhFJ/7BzgMHuYlMVd9R/MPIaitS
eLOpCroF31YzgxWh2qGexpzzYhWWbtUH0EMansQOjZdy+8gYe2hGf2T6l08UCWnmMGg9m1CJWxdi
+mZ0/CoSLq9tFxyJwTQcWMBItmvIM/VuDmCvQ/kRLbyqlrETE/y62nYh0dl85IaeGaFkI2btMesx
OEMENnH2+lSqdZiJM8cUMBidf+tGGkAPU8MGRCCwz//IflXST8sQq9d0NIzUheHUmSiqtvkHKia3
v1SBbZbgPjQaE4tCfcuka2pTGqDo4+6Ct9ETQFKwVK0NWu+8IOVP92i3LHfUf8BSLs+XYZ5xD+y1
PRKl+EFz0jaDW2lGIKH4yWVNZBhxRG2+tjIuQSakmRfzYDvgw4xUR4D9+olmaanGjUMinxfnjW52
E2wbb9WG6tp7aqUCT5357+YFiKEKuUzAcuw1rQphwg++Odk2fuClXQn/pEJJvI6ooaMzRg2filPH
ed0QibYLRbr1uVYh4xhXU+fApoZATADTl9r8ImPEOwVyJ3+8k0vCNNul2XKOVmLPVhoGn57bFCwI
JFSPOT5CRcpLgg/DQYowAKCUK2kVQ65B2fgdU5J50GB1BFfNV10jdElnnA0wO78Hz0xezjW3DGFP
utGsAt5VjfHsVDszU3ys/Da9YV8psqaW30tszV6a+/hY8LGBUmjvuKW3i0lEOVAG6xzBn5GH/j0r
cEDWl67kXbQs9VMkQt0RcmIvoBGEOBHUpInX5Yt+7DbWvW4i78gVxCUl1HTKhAJcufD0+q8hx/iC
doT7SMwMKSE+WQ4u+N1W1Us+zYklGpcia4yNJyXNQkzxwo4+vCLFYB3E1gwvd6LSbcQ9Da4XE157
D6r1LDs+hWTTPxcUZAUdnm0NKSBdoZhO/opIS70yPNAI3nfR0zPkQK6KMd0Anm8fSb7JoSPPjvQ8
xyRWB1elKDsMGxXBTMWJ/4vzLs1Uv3ry68EbKbP8hBpyo4S6YIqNMpaKANo1n28IW4IcSSqX6N2n
5we2XkzskauvuAgrx76FNw6NUC8xk6Q59XNwdqDTjNibfIxA7SPUQIf/IfFtxP4R5QVZMqT8gnkX
vGNsBjBP9tWh/7cEvgrf9S96HNaYQiacZsJLboH8IHIrDDGWE1W7DvyLd0kVu/I1Oqo19c4kujxM
9RRpoDFX0FCusJAPanzY/iG5IknDDb7ojItZEA1xP921kIyCGwSOe/WZYafBxBQ7hnJ878Onul+I
e1hrhDs/+0cAC5GQ6XlOy960KLNabedRTgg3CmZ6MseItpFOG1DZ6dHP7uxCZwWrGSlQzUFoQsIy
nSK45MLIHPt3cGVRIFnNmxjpTf+qhxUbpQRZOl0IHrSKETt+QvXnTqXtGxfFneuWSCoWhL6nlnMa
qVDO/sb5xvzwYSZHWzr7fyd4HLqibyMRvQNKXeUQOX4wN0cdN/WsI3B2ZucoQ+BPXToBV1frIdHE
9YgT3z9YEZqBnQYPk9M9CjoMAVcrU7WfL+zowwLZVmdtwwZ1hoOfTaMJGMkm+9PRqrQr3xB+YwGk
eeQV2DFs5ffbcOGcg7TAZW5wo8HPhzja7DU66POIIoW3sO6Wy2W9e4T4eB7+1JufybJFHFgVCTSL
xI/iZzm8Z4gi9aG3/lphrPBbJoqKOcX6pRVhvzWBW7tJMrq1Oc0VVeHN/OQNHk7NegXUsZyYM3gA
oypbBszRbQY5le223zckzOM3y0j8DZClHVRVLuUbZWyulW8z1uQPwTt0gwc4ho/sWVgPdmrBU/Go
XOR++ObSNb9xG9cp43jEGDr5kjMZwIt9UPLZAQIou4XuspHHMOojZNbrUxFsxsmpGibAeH3bxoAV
OoJFNkmNk7qeOcik/H47AH3Gm4YPZNY0C0YvjXAWQlkuXFLcRS6hOl3+qTj9jU/NHjhMOjsgN0Xc
/chXA1UxfpIsvi4xZwdwMbfXWO6jdu9I0p1XfHQ87ZM8wVuHUADJb1ork4Pbx1k75Glqm0CIhlm8
7h0IEWK6sKztP1dvmYpJH2ML2WHbtN6dzECrEc7WdoeqGm/ZVQ3Ey6NbKSNOvj/FlhnM/BwFifIi
x2ogfBkwuLtGwMxEvLdNVCzgijuJOz57kyJmDSHJMgFc89gR6a4sAt4pwPdZ+MeU6O7vlcZjjyhL
whiofAEwMDSjpd4+uA73SwYOFZ+98k7pop/m/tlPAucrp3jHMJk6R4XC5o6YEmUHE7JHGHh5xbCO
AWNwFP9MpI7lr0NM7oHp+ohfdQm0Y2ObJKtDwwBLJF1qkDoMzNS6uqwGmJ33rabhQOQ3RmoK6z8s
7MGUq2lIziZv6xtKS7wo07TOdE7VzZcMWJ5xIAQaQcuNQoQtH2ksD1cOuLUk8ZaCNE0vUS4Nld/F
X18eDycmWhPnFIug0rFlKIMUXsZ7vuOww2TxeWqw87D9piV9lXv29+hr4NphzZRuE/jVeXWfQoFY
bvoE/iORkUO4KIzXgWiTNvssQ2VlZRuWuHrBgpeCkiWtZ35jxUXMeFDxB21ovN5wNxVYkm6YBYtF
sAvs0MtLoJA3+efEO1+IkMhL0EpCf3X0Nj1yL30HcKXOWjIykLVSWF78z646cJtslYyHLoPw05v5
QAsVJU9qOjH2y1aIRTdKX+EchR/7owYRfSD8kmjYDA/KZ/SXkCNsgavFNnYhZEObgnoJuYR8oamC
6sHWyOGHQhQudHSmsxTBdQ8e/uvytWQeLao/fYOeWHywAxeQTV5fBfOsVTtmlDKxMcl76fnenBGb
p4G3Tw9pRhRQ7CMjc3ZQRnKDRsOFhgWfELbOX2jGFcASaKvQLa7qOnq/wC0PYA+ws0WyI5JWi0UU
b0d7lQRpYXF7fMHo2DT2dJs1+Hr36XRFfhKT5wZt6mum7sZVpWI9BgRzno5C8W6rx7COGQOp2WaI
xrzKAEkbufb4fT9xs4c+KefVhBhauVRZWvAQvXSpfxH+SNqcMgW5I2jxesQwZ2lwHQGCBeYaaTCP
OVE3SRu2WV2BKnTO1AGGz1/fXDdNXvC67vltOsKKz9Hue4YDNEYcsS4o2qau/1fWiX5RRad9xv11
aS6Qi2DmQ9QBU4yFRO+fFqP5awYak6RU44UvIR5gCOMk2OmXaeGhLPTmFSPD8ETeFmFDs3dZ12mt
yR2ZKl+ggFI1hlwPgqett+r/ZEyqI/8lQioz/iu359Qdi1BDa0MgNcbjQYfM5Yo/k2AIaStUCYpX
eVIElLVzb7f7sLxXy321uf6dhSA2jQINDhAnwFiVoJ3svzv9vtvr5PcbieSfTar8aLyd6nGcpp8t
38yGzUM6krh2WiBB5509gOz5rBd1fFk50y8cb8CIrl5bpa7YbtFI6oXu9SvrpQ49RbXKM0oOO3D+
6GaRgwaREmTxzrF77jK8NueD3nmIU1N5bfokIgtMIoad69HWRk0cbc3e6X4gtFuN+30yY0uDJEHn
zMy3LSCIUF3O9UMOop3yVnzmQwz7SQnzLmTxWAAH1Jmo5w56nAOGRGgpbbZ0EgXFvI/wN5LOdnT6
DHXp2q9eKbQVFn/gIXK7t8I2V/jmx1n9e/gXa8cJigyZnr2sAt/zGUlK9aJ3DMdZ4BOk6l/FQ0WA
XqzUogsbEnYKgQro6kPeL1KtsauIg8E1K7m0x93BKn6zaJzM3RC2+V+o2IOCP7oMSxZ8twwrBGHE
AxmN5/5cdgjw7t1dnUZrQvFr9sgxQbcB6yzzJAinfwWtPCD224c79oZwFkzmiqDFg4Lbfw7b+rq3
WMshub0KBwyi9PCs8Qurm9wGPh1wBdgH63cs/ZZeBKHUM9KVdj2/RZPhCeyu6EuqvZURgJOz0HDA
FclGTL1mvn3EWz6WLge5qm+y8nOx1+HWV/r5toIIsGl1IRpMLgYO6L7c11Ucs/UsLUkuYGD5P5m1
M20Kam7GN7Ysm5ciWnYrOd1UArmEe3OSXqNHlX4tX+WXpa0Lo/5n9dW9xl+WqwMuQW0mFVdIJqTf
2dDC51uePZv12b17Mw6KchpgWce1IBhZCThyZaCD+j/JBqbfZBOwrCOiiVGh34/WsYcbKIvTEkCX
E7Jq96D+7cn7efvWVi4JQkgYbpcfXYkVy3oGzzvrXL06/JdkTgt/j8r4+60uVWEnF0OTVzAqH8V0
JtJn83YU+no2+I0xSVmSPqQxVdoiyhSoK/Yum5wMYR+7X2Tznb/B5gju4C7MR827z+xvdNtMrxoD
CM3qxNZJW1IDteEVm7ziWdiIv86mym9LyEptlbg4JmoJhk6aUAsITljNl08/fTVos30WTWn0WKub
15yDJJ9GpYzt8As9HJnuhygv0aqg6jexTks32HjTnmds3MAso20sy8aImNtG3zyDGMCuzqB1PHgp
1lKHJ8CSW47PoApr/lDwkHacfqBHOKNw8zoxRAWEbfJuGFHsRbp5rDzYnK0o3RcebxnXBNQNtCkX
ff8fCotFXVg+5f0SRHzJvB1BxOaFvnYPSqONjnsLgB8QhzvrWeRt6Dg4odQUd0j8JDPpTWZ2kBnq
/yYYxQSMfq0QizJE2GzEcu5fy4AbzP4HWt6NzMI/0N0og/48s8ZDEBuA6oBEMnPulcevzLyNaBdQ
jc9F4EEQ3DCe675mkJn/cg3Vv3zw7rUHl+4wFvbvbPD7jWSyMpNxr4N/eheJs4O40j0ul/N/EkcC
1U8yVbJ2L255HD1vzkSSgGzmbHGS91zh5eTQINDQ+1Lxpql625yfoUtC8sJdWzNFcd0jod/QjF0V
UeyyFCHHnzw8uyxz28ycPADm2sx1/WkwaOFwb4yeFlHx8PkZLllmvSg1zxlVQA/gnkrBXEdd2z/A
OKguvflbWSLFPWBis0OdLtKalCJC5L731/1oSJjuFXPIFbZb4iz/TL2vMSBltRk7C6qHQJV1bezh
SG2pLwwQT6bRwbr4sFveyDpYbZTz6J+2pW9spV+0WuUOdH7SF68Qn8TuspsfTYrJ0gIWdRW15E01
JAVIyrTj1EoVdZuPHxVDA+vFAsYwA8OrgpI59BkodK81br8o1ci8jKJcz76nUWcOF5PSWalNLB/q
zAT3kKeuwlPqx7ScpiMo9Xg7p0f71UNHe3IDK/Dw9N5LkzhKO14EbkPBX75snPbVykJD6xyEvrqz
NuVgxlPZmLNYGSqhHXjuHXQ44owS7b3L0Gury+c7qgLefx3t+UlBCWBojYOhN/fsW4s5jyE1V34q
JJGFd3Q0SGmIBWYjE//IwKtKynmPiSEvbx10XYMSQe3G8tjI816NSaL/eN0AISMrGe4rTupZtNz2
KtMtYUqlaTKE/3NxZY8HR/MtLPVS+wcGo08S8dcequfvb6Y3VS6Z0fdDqvncJqJAi88SuJYsUazM
yg9sHO6wd4wx40HFu93dkJpJpXDBxBZjQXWXcfcShOQKa84enKKCODdC3xQK6Sw9UwzfkFstzQ72
xoMpzaieCGT5Z5yUXNQnP5pvRjSv1Fo1juSQkGahrDLG6vXzTdUrmP57omc1NY/31BnrClIpcg/K
qrVdWfj4xynOl0p4cteqfSksdsNaXClFauEcwAY32gy0A7oAb0yNf/nkUypHtuM0bQvqnS+g+bMU
8I+ivSoLxiRIvka+0NINnB9Qv+yJW9oIMb/rESyzcI1nl1vcHaJzSqImIgj5Fq6qVOvVABDrAXmC
UP4a1ncuvn38gOo6yq+uYOQNRALIPQm3zLfXWEuJqwyXCUD7l1xO8H8lI8A4Bboj8qDpUwykc2Lf
PTaI0CmjABwle3qIfQiPeWXx27zCdJ3D6sc+hIZcbCD57f1sU5RVbRAV5mQ/yUeY094iEn4EUw7D
HmXkHxuNeSq5xmiATv2RHZZnXb14oNdzqGcDVw4nrBMDvMsczkxj+9uZ7FgzXWdTEo2piGoo++AM
X4HAu7BFnCLmICKyZv5omCBik4CBYFwBVV9j/Bs0Aj/okMIn6SWIlpder8CNtlJHC+4Wc4G/bbi6
r0m9RcVSCi8c/hLjUW/UojUcgQwmbFAOcAeT0xwYTRPt0rPvMHWV/gdb6Xo+dnLinr8X8+10gtZX
DzD2YeK4xxcxRLDj3+t2b2M5Zqoyts9X2Rw30widug+ttov9y/ArL+0cTqBn1z0iRtDDA3M9WIeW
ucQEujPogJePXBoh6GDLG579AOEjFlZ8zEVa0+CuWODCmPix/tEuyX0fu3Ki6P14prXRE1ahu+cu
L7RhdCxWlm0JX3hkcFfCwS2/TVGPBzFmYVT7J5KHGagg5YYebSUc7uMDNYKIEMsX/KIX/WZEEkCJ
8/aVCEbH23xe6E0or3BrrgOOLcGCE2wFqm0ikOLKRXrMOv2U7HtOHNkj3aBKdsc4QeqQO2N/GaUv
B+cLIqWYxc0FCgUnsri1CeOl2GJV8VdbX/AJmT1H0LlsTcSztwmzzFWVyUfoT+JXa4VBGulPi6KS
auXJczoG6taDBMWG49ZkhVdu2fFsftzaVrEgVgrgFEykHGMKvJO7XEF3riABwBMHi2+8LTZdRfca
v3eQEYvZ4wsxbdzZVEhqxo03zZVQTWgIzyhMST2PD2NgdE+ybNECtqb3sThrtOzYnddqlByVmIH1
ZFZEvgxHm4vnzV9JerhJ2XlxrOcCCBBRxIsm7/nif6Z0UmnV2EjyroYKshg+KeIutRoCv9UQdjAr
8aGvDXLUYK4wJvGPQGIDwtLsMSdY4zV2LU240UGVcBXTJe9Sn2RUdYxqeqP7PUtcIlpDPlZqx+g+
dRcFesubmMWbu5ER2/YnDRbe03nO1NYMUEmHTzVQ8YvUugCIDiiacPyP5CUoVfr9JX5M+PAknf85
UEuTI0+RNHKnMwfnvQCv4Bdz9RMA1Gw/ijFJ2wkWdJwh8qkroQ7/c091WkqsrLjmSQ2brL0C/E5J
+VBRmXEuNxGFJhpks0bDZwjGthvuGMllFoVnZ1KooEmsHTnYaYuWUETIZkoaywN4Q/naBvwI5ESD
XBpbF+RPF8IOFHMm4sYRlvyUsq1Vrp4TIyyCxIM0ajx01uUjUyHDkR+LkKPX56475w8eZyqM5qO+
4K3aRJf9rXiG8L/tVGIVrYJmYZ0bAPFttZ4152tgVKMGfrH0uiL9uNzXycrKVNa5OjEUVti709Tf
lPTyWhf1BtOExS8T39fW82uVitovke7dt5LhWlmT9besImESrrnrL7ffd2QgA7PH0kvzX3EXBa54
37PsjDDKoDpRT8syZwhCSJsdfTTjY4I+hgtfdh35UxFkoJV+OJOrXf7Wf2PEJtp5qZeqQt5PgwFI
9mmdBwBW38s85X4YOfaRBrUfuPxt8+eCJ3WpZWXgpCFiNDFgMxQDIgQ7uwKDwhqKYoXR6298LKgk
8KonpvyyaUFTUJlJtSYnxCRZUEFbTZxWQOA1JCh9CF9HTa2JNAR1+iUv7/6rGZcIgE9ApB8RCwE2
pw+5p3pkPto6jeROISB4KJRLvby/9eaUVFm8HhTmRbdYRdqWwG0MoCmzP4sf4Q6McyCICRNdYJOQ
Dg2gweoDUtlfnFY+TYEXG6SqhW2PdQhLuUYwziBeLdZSoico+pmsamEwdznrQpa3qzSTFfx7UhSE
l0LzNsM/CLImyf0gZauWGjolADQoy1M59x+wC1n8h18bfi6tbNk3nlejrsXp3mu888lOKyqtBybI
+OxKUs74d2EoCYYwsK1xc0TTQRmEAIrKP9ltjblmjrqT1rCNX6LAhIET171E09MbE/5XrLbAExXv
Jk43XfSdBZBQETmoGpPLxXwf/KnCKgkdBbFhUMEFZQrr4YHVDFuI28ZV2kzoFHf35ZhNxGsunYsg
gl38lGwoNsz8BJVm9AIA37IMbQzSS+dE2kRXnzHLf2ymWsUHjkZiHFq8NrtwZLZEMj2T7Bg0DX7F
B54S5YnuQVX+QKR9c5WyaNRHySjoMfx75ODZyH4xoby16QXrhyN4Iu964EJKzNVxcWkQVpyzIKEw
h8I5qfZJS+dqbr9g3+YlIoAd6NAmwJWyLYXfsLy1n3D0R5hWtecGcDLSTMgD5rvoDAn+aqpN62wh
9CLViAkEvzCvUGlM6FgJWlbeQcAKO59WzJe1BRZ5ztTr4wVsjsvXmEzd7doHPUFuvMHxVLzpKgCb
E5x+bk0KW//ON+AtRlRC9d3e3Ljq1UamMarV+2rWUZy6uOILYabbD/8n5atG30mDEpmPVIDJaeq/
HUra7AgOLP4/EW36RoCut81g16ROlQxIquZqY3yeS9zB+1JOoTB3lSWP5suLvJfxJgkQlevvJxmf
gVdQ8GT35r6Y9a9ECqh5vVzBVhGDlrWJ5WrLJniDBTeT40MImeatLYmlJ1kqTjer3jIEI1rvADB6
mRTHKzHly83z+LfKbliW4SvaB4kfDaVU3AhxZ0MCkj7bAGxXuRikPcIdkp2ROQw8f05tqr+nPXew
hGRt/INI88vXiq3TxX1VU87YHPUCz1kjivonTMDKnhAT/vGDs32mzbLxE0jvmK/zfM0ZCPeegkBy
ygsL7HmyLzYRiucqZZd/QztuKkV3T1CrCdG4VxNxP85cU/8QAw5x4wSG6/jl+b7DAr0fidcq9nO9
Z63MnO45nO05gKpsruHhPpHE+UPWDiFBEw7uSSw0J5CXU3q9V522nWoZer3bCigGsBqllJDBRvnE
s8RxPNoBgTG7sWy2/Dj1z5s5mDbWg+Mc31bQRDPWOhJmYYuJ7WDbpXVaN73PVIvLFGJwGpseD2/O
9Xd2wTS/rSNt4yLGgiNA87gXHqyhXC2EJWZvz9m8wswotWcIH43VX+RMwgOkYXOCecs3Ep/y0ZET
TyM0P2+lt5LmKy3C3y4UCIJM4y4iNrL89t73nMySE7myHlk/gBvYYTCi0WTThYhKvzPEVw6M2T6P
wkcfWm4JRg2vltfbYbrEbyCMdPKU2LDz2t5tACjKaTLij8SrUoq3q94QAIuxnauWzISb9TkrfQnT
X60kTDRyFGaJSul7mAUSy3b3NXQuscAcnjqLtVPEN63xCoE9kXCN/pKxxEwx32bpCehuspqJz8jH
yqEDQ7k/MoKcHwS/4s7vB7Tmne+0hXp/aP4lGDAw4LIM+RVih/27Z58t0dNOoFrE0/D6snANc/8S
ux/hjUDSooAU8vnqosrnvtlP7OFGlTBbrUQrh1nu7vyPlu2F/7zMg3YxU+NHNys5nLom9OvhtKbA
sn1xV3hjhWqzQzCPiiKvKci7GmZZ76vuIjfif1HLShN10lV3VwBl61my2S/+IYVa3iDIdpCYNU7D
OhmSQK5qOZvoYXJ+3wxdMsQrmRvfTbGpSu9IJo0yKfMvDfg8symKMNardSJzul1ZFFssvxdsSLnI
sKC/MyxQH0zc9fMF/GEegIb3iDCXZc+Bo9UNYyTKE60ozU672dFb1cdnWZmY79gsfijPn867i/ck
JeEsGPGlLRCgenQq5uCCMBSd1RqRGbMsj8M+fX0jDjf9PK1KfRkKXQrAUKGnyt1X/rFBCWLFn7pA
8ctLqFqAi6fg+hFp+1LKISRDAV11JiVDV0bOTF9uqQBuFddlAC6C3u9SBc/xk5LueNW3B7nYgkuc
2q+2TjI8CDCRHUBbcSvr9Ho61Z8eNC+EZxCzboe43jZtISYP9A+rLVQlHa53hqyUfY3qkN5vrrf6
Do2SAv2w5RyX/wk+aUIiVgZzRZhqx5IZXi6IDkMdetDKp6H3AxGr3LR0MiN/igzo/8VRii/k3A9o
Sjd1pYTbOeyrUYzE+msAXINUkndgV876SMiu/9hs44ThgScDs0l/KcXIB85XYo0Ny2DT5u8l/23x
0H5V68jxaoblQRfRMn35dR9av6wMXriWMaO5dCrf2uJEtBdRaG/ByDn7EnrZVnp2FtkWyGrn672X
hM1sTNj/G+qyeT9Y+WzVzOj8Pkbf5gp8SsGFZfgDMhPaMs7Ynv/Q97gJHyVqiMETwgtqq2X8cdhh
ZjMms59BFl0ihYcr9/WXNAYEmhVcqrlvp2jEZTj3YGPisG+Hq2KBU9TXOWA7nJgOJqaYD3werpuM
KbakZANm8amMXZC0HosmP5cbmJ3/crgMKUXYRAMHU6qhHGQAgMQmiMbTHim/X/xeu/UXV67uPACq
9zthbFCmuTcQ6b/9AP+hni7lBPhbxdPoY1y6M0sYSpP4GWAvuZ8OhsSkwEK2LXFKe2veAsKdPUZL
hozuqbYHfjn1YBg+fDBeKnubKrMw2CEphlJBHB+p+O7qeQCg7zqNO/DuEeoqeM+Wte6aaXDMEivj
6daxSkzqp6wAXtlsVUp3Pbl7YBDOwye74f6/N8ui4bBgtSF9J4YLz5MgNFwwRZMs5Ri3TY/tdFN6
DEyvQkKNVRcVVb8v5WCejFt1WbICIlpOWjeYOLTyvuU66SVOQYbKv42wTdyErBZKrMvGs5QG8mDo
3+K2+jlrjP9wRQMqBeEP3QDKXbXStrruc8mXx+EVCerU91Gfw6NJJvc1W9SQRueADhEJ/XQjJzHT
z/6g/X3bcdKJdCmENolDzA789Ljr3NwZRAKPtK+qEn/7uOw+oixuzHCK9cQepeiyL/BFH/qcs+7V
1Ti2FdW0mMYDHm6Y29pNDc9SadZu0OsHYQC8HjugNcZFxYuQERD2NnM5k5U+urJDUr3ldjO/MirT
aKCjS7JeKwjuXL2ks8McGTjVeBREJMzd1bR7Ft7Y9loOjo83AjyZmJVqh2l07HJPQz5T7aJlBYrV
0xUlxUgaEaHGsxaBkAUXHTC5Wf2smJ32lfuz7/wNZKMFmFy0YdpI5hMi98CvwYi07/pbVizqGMb3
xaO/vzGpbzb0a/xgbel01QgqIIhz+wmOpLKkUfhBpEmqE9yLnfiYhwbw4QY+ilDo2chdqQJYRKTr
aVX41KrYmB4MVeiD+8Ka3qZAqefJLYXmjf+Xnn7nKc8XzqyFThLDPKuRgs8N8mS0sW6I1AOpLOHS
xEMyB5VeAKNmHG/Q70Q7v4aPlAa7DZK8L5jJBFHQquEmDTd2zb9NZepwEZ241yS1C/1gcA4q/vts
YQGJqB90ANp2X2NvpAOu2Zagk3rEEmccmfEtMKTD2B+Ybl87YTUtWBfsOe6qEvxy8QImw0oowCIv
/APo5CqZFY8gLcs/+NrtbgOY432Mrc8b0QBgAJkAoEKR5bnzchDBgAQbPiMLmfD72JEp6LRPCQ5C
FHIlPMtuzFhmBaxGFgoFMLQELd0YHVT8UcwZ15Mmk5+FgjWH1htFGotsN2cbBwT/yGojfs3z7bPr
dHDMn9iQ45hwy4V+hbyHzpgERbz/mcox0PfTnhKuYehzF9wGh1oCe3ahNX9z9USMSMw4XGbP32tm
aprA4SReiFOIT7wt1BqumN8mnqdcSCjx15laYO0GLr/VTlzSijkLR/qgmOOeOwJK7coqkbwd9PA1
UtJ+D09KIgFbxRldidgKzuU/kiZZnL5tD0JT4JkpNYifqS/t1OudEbNtvFW9MG8/X8ANWUolnGhS
ZYu958XXucV3+QFphABEX+JHv2cmRRpsRz3PcTFFoOTYMwu1vkEKKlEahVA2gNPOBz9sNh6KsYGt
BXeR+L/k38NKt9TubvBcZi7NOpmT4zSo+0DXedIpTRYz9AyEjLXP1tNkf6G1hZ4pbFOM/lY5AnQN
VqkmIUReO5MlAS/fP8U1W0bobPEFNus92yMvTHzEcZFDEjBPkoafj8WgA0fsx0TYYbxXttNf/JV8
YHL98jwBvkN7sWqBGrSdwg1BPP7cktiBLMwAdWcvPd1X2x1My54XjDpyt0MglzFFQreS0y8aL4tM
3B+8sodFVRUHznWEhqIr0t9FYeFtBcncZSUGPnkvUj2QulPcRX2V9t//KKsO412+FuxSHAlZzmIR
C8OlEWrzaBZ10CmuSu+otB6ZzKq7kUWH/CwLnoznykOFQ9FXUxkKQyRS0v3m6pSimQMByCKFvM46
hylihNFB8rD66Wacd9Rtbtei7kLBJhhDm3sj+pxUDhQ7mhBzYDwfDkYIU3QO/A0vAqVmWL+Q7Jqy
5QVYPFRUemzeG1tg9SANS8SY5yo/ffL9tIHtjOhzKio11AV6bkCyXsO7JF3FLiLEq+5usoMEVRTf
1vAh3yWYhFMOvLGZi9w5yFZ/KXQiV1sXiQcMdDT3CJRNawpvC5b6wyhv38EoFhtVDRCxqJ49qvjS
GHK+JyHnL3FjYVr3P0boVPYB7oMN7QOw+0hJhoUNg0BMovhkCMdAUElZxv1sYiSnONwbORAjooH1
ri0JHNjO/7IcuyiWMmk26fZSJVqZGrkbEYS7ONVrWEf13buPWprYciPIgRvamF/OwS8xBo/JCPmU
ZcJtK9m7wDuFE6OTtZj5DLCKtV39fmSv/9ioMIp63a8bVRUKWxZFmasxcUMaavwm7limMLWKfLiP
2XoyN+x91nehjsCi+n6oVFK28fuhuOy9O47UNKqVBGfrNRmMsFr7MQDAVwc1gQU16aBHYhF/t4oY
rFaAXHTbJfxd7Od6ke7E3AOr5r1ommo/VOauV3Pi0wpEOJiZephk9sNEwEs/WNNUor4+TeOpGEQE
RRc49mmvIcM9fOI6FIZzG4KgWjS7PMT2X6wYFU7GW38Pb8v9b4CQ8WYOMMYcrjt+7t36hCR7l98F
XLt3+kSq2RPOrrVGEeSoEvUk4OvVWDHQirGdgtbqKRBzLe9g+wwRtx2K6A8m99S7jRDtMHYsv1pp
QfDwaco/5iCx7dPy8TpHtz7xnJRL3tP42D+wASnHSOfLinfFd5vTYq0wpp+JtMeHXZE0i/vsSthz
0V1143hu+jZty0RlG1eg4rLN
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity c_addsub_0_c_addsub_v12_0_11 is
  port (
    A : in STD_LOGIC_VECTOR ( 31 downto 0 );
    B : in STD_LOGIC_VECTOR ( 47 downto 0 );
    CLK : in STD_LOGIC;
    ADD : in STD_LOGIC;
    C_IN : in STD_LOGIC;
    CE : in STD_LOGIC;
    BYPASS : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    C_OUT : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 47 downto 0 )
  );
  attribute C_ADD_MODE : integer;
  attribute C_ADD_MODE of c_addsub_0_c_addsub_v12_0_11 : entity is 1;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of c_addsub_0_c_addsub_v12_0_11 : entity is "0";
  attribute C_A_TYPE : integer;
  attribute C_A_TYPE of c_addsub_0_c_addsub_v12_0_11 : entity is 1;
  attribute C_A_WIDTH : integer;
  attribute C_A_WIDTH of c_addsub_0_c_addsub_v12_0_11 : entity is 32;
  attribute C_BORROW_LOW : integer;
  attribute C_BORROW_LOW of c_addsub_0_c_addsub_v12_0_11 : entity is 1;
  attribute C_BYPASS_LOW : integer;
  attribute C_BYPASS_LOW of c_addsub_0_c_addsub_v12_0_11 : entity is 0;
  attribute C_B_CONSTANT : integer;
  attribute C_B_CONSTANT of c_addsub_0_c_addsub_v12_0_11 : entity is 0;
  attribute C_B_TYPE : integer;
  attribute C_B_TYPE of c_addsub_0_c_addsub_v12_0_11 : entity is 0;
  attribute C_B_VALUE : string;
  attribute C_B_VALUE of c_addsub_0_c_addsub_v12_0_11 : entity is "000000000000000000000000000000000000000000000000";
  attribute C_B_WIDTH : integer;
  attribute C_B_WIDTH of c_addsub_0_c_addsub_v12_0_11 : entity is 48;
  attribute C_CE_OVERRIDES_BYPASS : integer;
  attribute C_CE_OVERRIDES_BYPASS of c_addsub_0_c_addsub_v12_0_11 : entity is 1;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of c_addsub_0_c_addsub_v12_0_11 : entity is 0;
  attribute C_HAS_BYPASS : integer;
  attribute C_HAS_BYPASS of c_addsub_0_c_addsub_v12_0_11 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of c_addsub_0_c_addsub_v12_0_11 : entity is 1;
  attribute C_HAS_C_IN : integer;
  attribute C_HAS_C_IN of c_addsub_0_c_addsub_v12_0_11 : entity is 0;
  attribute C_HAS_C_OUT : integer;
  attribute C_HAS_C_OUT of c_addsub_0_c_addsub_v12_0_11 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of c_addsub_0_c_addsub_v12_0_11 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of c_addsub_0_c_addsub_v12_0_11 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of c_addsub_0_c_addsub_v12_0_11 : entity is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of c_addsub_0_c_addsub_v12_0_11 : entity is 1;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of c_addsub_0_c_addsub_v12_0_11 : entity is 2;
  attribute C_OUT_WIDTH : integer;
  attribute C_OUT_WIDTH of c_addsub_0_c_addsub_v12_0_11 : entity is 48;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of c_addsub_0_c_addsub_v12_0_11 : entity is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of c_addsub_0_c_addsub_v12_0_11 : entity is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of c_addsub_0_c_addsub_v12_0_11 : entity is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of c_addsub_0_c_addsub_v12_0_11 : entity is "zynq";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of c_addsub_0_c_addsub_v12_0_11 : entity is "c_addsub_v12_0_11";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of c_addsub_0_c_addsub_v12_0_11 : entity is "yes";
end c_addsub_0_c_addsub_v12_0_11;

architecture STRUCTURE of c_addsub_0_c_addsub_v12_0_11 is
  signal \<const0>\ : STD_LOGIC;
  signal NLW_xst_addsub_C_OUT_UNCONNECTED : STD_LOGIC;
  attribute C_ADD_MODE of xst_addsub : label is 1;
  attribute C_AINIT_VAL of xst_addsub : label is "0";
  attribute C_A_TYPE of xst_addsub : label is 1;
  attribute C_A_WIDTH of xst_addsub : label is 32;
  attribute C_BORROW_LOW of xst_addsub : label is 1;
  attribute C_BYPASS_LOW of xst_addsub : label is 0;
  attribute C_B_CONSTANT of xst_addsub : label is 0;
  attribute C_B_TYPE of xst_addsub : label is 0;
  attribute C_B_VALUE of xst_addsub : label is "000000000000000000000000000000000000000000000000";
  attribute C_B_WIDTH of xst_addsub : label is 48;
  attribute C_CE_OVERRIDES_BYPASS of xst_addsub : label is 1;
  attribute C_CE_OVERRIDES_SCLR of xst_addsub : label is 0;
  attribute C_HAS_BYPASS of xst_addsub : label is 0;
  attribute C_HAS_CE of xst_addsub : label is 1;
  attribute C_HAS_C_IN of xst_addsub : label is 0;
  attribute C_HAS_C_OUT of xst_addsub : label is 0;
  attribute C_HAS_SCLR of xst_addsub : label is 0;
  attribute C_HAS_SINIT of xst_addsub : label is 0;
  attribute C_HAS_SSET of xst_addsub : label is 0;
  attribute C_IMPLEMENTATION of xst_addsub : label is 1;
  attribute C_LATENCY of xst_addsub : label is 2;
  attribute C_OUT_WIDTH of xst_addsub : label is 48;
  attribute C_SCLR_OVERRIDES_SSET of xst_addsub : label is 1;
  attribute C_SINIT_VAL of xst_addsub : label is "0";
  attribute C_VERBOSITY of xst_addsub : label is 0;
  attribute C_XDEVICEFAMILY of xst_addsub : label is "zynq";
  attribute downgradeipidentifiedwarnings of xst_addsub : label is "yes";
begin
  C_OUT <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
xst_addsub: entity work.c_addsub_0_c_addsub_v12_0_11_viv
     port map (
      A(31 downto 0) => A(31 downto 0),
      ADD => '0',
      B(47 downto 0) => B(47 downto 0),
      BYPASS => '0',
      CE => CE,
      CLK => CLK,
      C_IN => '0',
      C_OUT => NLW_xst_addsub_C_OUT_UNCONNECTED,
      S(47 downto 0) => S(47 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity c_addsub_0 is
  port (
    A : in STD_LOGIC_VECTOR ( 31 downto 0 );
    B : in STD_LOGIC_VECTOR ( 47 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 47 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of c_addsub_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of c_addsub_0 : entity is "c_addsub_0,c_addsub_v12_0_11,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of c_addsub_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of c_addsub_0 : entity is "c_addsub_v12_0_11,Vivado 2017.4";
end c_addsub_0;

architecture STRUCTURE of c_addsub_0 is
  signal NLW_U0_C_OUT_UNCONNECTED : STD_LOGIC;
  attribute C_ADD_MODE : integer;
  attribute C_ADD_MODE of U0 : label is 1;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_A_TYPE : integer;
  attribute C_A_TYPE of U0 : label is 1;
  attribute C_A_WIDTH : integer;
  attribute C_A_WIDTH of U0 : label is 32;
  attribute C_BORROW_LOW : integer;
  attribute C_BORROW_LOW of U0 : label is 1;
  attribute C_BYPASS_LOW : integer;
  attribute C_BYPASS_LOW of U0 : label is 0;
  attribute C_B_CONSTANT : integer;
  attribute C_B_CONSTANT of U0 : label is 0;
  attribute C_B_TYPE : integer;
  attribute C_B_TYPE of U0 : label is 0;
  attribute C_B_VALUE : string;
  attribute C_B_VALUE of U0 : label is "000000000000000000000000000000000000000000000000";
  attribute C_B_WIDTH : integer;
  attribute C_B_WIDTH of U0 : label is 48;
  attribute C_CE_OVERRIDES_BYPASS : integer;
  attribute C_CE_OVERRIDES_BYPASS of U0 : label is 1;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of U0 : label is 0;
  attribute C_HAS_BYPASS : integer;
  attribute C_HAS_BYPASS of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 1;
  attribute C_HAS_C_IN : integer;
  attribute C_HAS_C_IN of U0 : label is 0;
  attribute C_HAS_C_OUT : integer;
  attribute C_HAS_C_OUT of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of U0 : label is 1;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of U0 : label is 2;
  attribute C_OUT_WIDTH : integer;
  attribute C_OUT_WIDTH of U0 : label is 48;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "zynq";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CE : signal is "xilinx.com:signal:clockenable:1.0 ce_intf CE";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CE : signal is "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW";
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000";
  attribute x_interface_info of A : signal is "xilinx.com:signal:data:1.0 a_intf DATA";
  attribute x_interface_parameter of A : signal is "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef";
  attribute x_interface_info of B : signal is "xilinx.com:signal:data:1.0 b_intf DATA";
  attribute x_interface_parameter of B : signal is "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef";
  attribute x_interface_info of S : signal is "xilinx.com:signal:data:1.0 s_intf DATA";
  attribute x_interface_parameter of S : signal is "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef";
begin
U0: entity work.c_addsub_0_c_addsub_v12_0_11
     port map (
      A(31 downto 0) => A(31 downto 0),
      ADD => '1',
      B(47 downto 0) => B(47 downto 0),
      BYPASS => '0',
      CE => CE,
      CLK => CLK,
      C_IN => '0',
      C_OUT => NLW_U0_C_OUT_UNCONNECTED,
      S(47 downto 0) => S(47 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
