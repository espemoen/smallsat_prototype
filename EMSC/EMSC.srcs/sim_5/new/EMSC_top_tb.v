`timescale 1ns/1ps

module top_tb;
    parameter C_S_AXI_DATA_WIDTH = 32;
    parameter C_S_AXI_ADDR_WIDTH = 6;
    parameter B_RAM_SIZE = 100;
    parameter B_RAM_BIT_WIDTH = 32;
    parameter NUM_B_RAM = 8;
    parameter P_BIT_WIDTH = 48;
    parameter PERIOD = 10;
    
    reg clk, aresetn;
    
    reg[31:0] in_G_temp;
    
    
    //Stream in
    reg s_axis_tvalid, s_axis_tlast;
    wire s_axis_tready;
    reg[15:0] s_axis_tdata;
    
    //Stream out
    reg m_axis_tready;
    wire [31:0] m_axis_tdata;
    wire m_axis_tvalid, m_axis_tlast;
    
    //Control interface
    reg s_axi_awvalid, s_axi_wvalid, s_axi_bready, s_axi_arvalid, s_axi_rready;
    reg [C_S_AXI_ADDR_WIDTH-1:0] s_axi_awaddr;
    reg [2:0] s_axi_awprot;
    wire s_axi_awready, s_axi_wready, s_axi_bvalid, s_axi_arready, s_axi_rvalid;
    reg[C_S_AXI_DATA_WIDTH-1:0] s_axi_wdata;
    reg[(C_S_AXI_DATA_WIDTH/8)-1:0] s_axi_wstrb;
    wire [1:0] s_axi_bresp;
    reg[C_S_AXI_ADDR_WIDTH-1:0] s_axi_araddr;
    reg[2:0] s_axi_arprot;
    wire[C_S_AXI_DATA_WIDTH-1:0] s_axi_rdata;
    wire[1:0] s_axi_rresp;
    
    
    EMSC_top
        #(.C_S_AXI_DATA_WIDTH(C_S_AXI_DATA_WIDTH),
          .C_S_AXI_ADDR_WIDTH(C_S_AXI_ADDR_WIDTH),
          .B_RAM_SIZE(B_RAM_SIZE),
          .B_RAM_BIT_WIDTH(B_RAM_BIT_WIDTH),
          .NUM_B_RAM(NUM_B_RAM),
          .P_BIT_WIDTH(P_BIT_WIDTH))
    DUT
        (.clk(clk),
         .aresetn(aresetn),
         
         .s_axis_tdata(s_axis_tdata),
         .s_axis_tvalid(s_axis_tvalid),
         .s_axis_tready(s_axis_tready),
         .s_axis_tlast(s_axis_tlast),
         
         .m_axis_tdata(m_axis_tdata),
         .m_axis_tvalid(m_axis_tvalid),
         .m_axis_tready(m_axis_tready),
         .m_axis_tlast(m_axis_tlast),
         
         .s_axi_awaddr(s_axi_awaddr),
         .s_axi_awprot(s_axi_awprot),
         .s_axi_awvalid(s_axi_awvalid),
         .s_axi_awready(s_axi_awready),
         .s_axi_wdata(s_axi_wdata),
         .s_axi_wstrb(s_axi_wstrb),
         .s_axi_wvalid(s_axi_wvalid),
         .s_axi_wready(s_axi_wready),
         .s_axi_bresp(s_axi_bresp),
         .s_axi_bvalid(s_axi_bvalid),
         .s_axi_bready(s_axi_bready),
         .s_axi_araddr(s_axi_araddr),
         .s_axi_arprot(s_axi_arprot),
         .s_axi_arvalid(s_axi_arvalid),
         .s_axi_arready(s_axi_arready),
         .s_axi_rdata(s_axi_rdata),
         .s_axi_rresp(s_axi_rresp),
         .s_axi_rvalid(s_axi_rvalid),
         .s_axi_rready(s_axi_rready)
        );
    
        always #(PERIOD/2) clk = ~clk;
        
        integer          f_in_G, f_in_raw;
        integer          iter, i;
        initial begin
            clk <= 1'b0;
            aresetn <= 1'b0;
            write_to_reg(6'h3C, 32'b0);
            s_axi_wdata <= 32'b0;
            in_G_temp <= 32'b0;
            
            s_axi_awprot <= 'b0;
            s_axi_bready <= 1'b0;
            s_axi_wstrb <= 4'hF;
            
            f_in_G = $fopen("D:/MasterOppgave/smallsat_prototype/EMSC/in_G.bin", "rb");

            if (f_in_G == 0) begin
                $display("Failed to open input file %s", "D:/MasterOppgave/smallsat_prototype/EMSC/in_G.bin");
                $finish;
            end
            
            repeat(4) @(posedge clk);
            aresetn <= 1'b1;
            write_to_reg(6'h30, 32'h68);
            write_to_reg(6'h38, 32'hA);
            repeat(2) @(posedge clk);
            write_to_reg(6'h38, 32'h2034);
            

           
            for (iter = 0; iter < 416; iter = iter + 1) begin                                                   //|
                for( i = 0; i < 4; i = i + 1) begin                                                             //|
                    in_G_temp[i*8  +: 8] = $fgetc(f_in_G);                                                          //|
                end
                write_to_reg(6'h3C, in_G_temp);                                                                                             //|
            @(posedge clk);                                                                                          //|
            end  
            
            
            
            
            @(posedge clk);
            write_to_reg(6'h38, 32'h34);
            
            repeat(4)@(posedge clk);
            
            
            
            f_in_raw = $fopen("D:/MasterOppgave/smallsat_prototype/EMSC/in_raw.bin", "rb");
            
            if (f_in_raw == 0) begin
                $display("Failed to open input file %s", "D:/MasterOppgave/smallsat_prototype/EMSC/in_raw.bin");
                $finish;
            end
            write_to_reg(6'h38, 32'h1034);
            
            repeat(4)@(posedge clk);
            s_axis_tvalid = 1'b1;
            
 
            
            for (iter = 0; iter < 104; iter = iter + 1) begin                                                   //|
                for( i = 0; i < 4; i = i + 1) begin                                                             //|
                    s_axis_tdata[i*8  +: 8] = $fgetc(f_in_raw);                                                          //|
                end                                                                                          //|
                @(posedge clk);                                                                                          //|
            end  
            
            @(posedge m_axis_tvalid); 
            @(posedge clk);
            s_axis_tvalid <= 1'b0;
            
            $fclose(f_in_G);
            $fclose(f_in_raw);
        end
        
        
        task write_to_reg;
              input [5:0] address;
              input [31:0] data;
              begin
                @(posedge clk);
                s_axi_awaddr <= address;
                s_axi_awvalid <= 1'b1;
                s_axi_wvalid <= 1'b1;
                s_axi_wdata <= data;
        
                while (!(s_axi_awready == 1'b1 && s_axi_wready == 1'b1)) begin
                   @(posedge clk);
                end
        
                s_axi_awvalid <= 1'b0;
                s_axi_wvalid <= 1'b0;
            end
        endtask
        
endmodule
