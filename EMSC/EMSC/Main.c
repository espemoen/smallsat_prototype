#include <windows.h> //UART

#include <stdio.h>
#include <stdlib.h> 
#include <math.h>
#include <float.h>

#include <conio.h>;
#include <string.h>;


void write_file(double ** arr, char * filename, int r, int c);

double mean(int length, double x[]) {
	double sum = 0;
	for (int i = 0; i < length; i++) {
		sum += x[i];
	}
	return((float)sum / length);
}

void calculateSD(double* std, double ** data, int nVars, int refOrder)
{
	double sum = 0.0;
	double smean = 0.0;
	double * mean = (double *)malloc(nVars * sizeof(double));
	int i;

	if (refOrder > 1) {
		for (int col = 0; col < nVars; col++) {
			for (int row = 0; row < refOrder; row++) {
				sum += data[row][col];
			}
			mean[col] = (sum / refOrder);
			sum = 0;
		}

		for (int col = 0; col < nVars; col++) {
			for (int row = 0; row < refOrder; row++) {
				sum += pow(data[row][col] - mean[col], 2);
			}
			if (refOrder > 1) {
				std[col] = sqrt(sum / (refOrder - 1));
			}
			sum = 0;
		}
	}
	else {
		for (int i = 0; i < nVars; i++) {
			sum += data[0][i];
		}
		smean = sum / nVars;
		sum = 0.0;
		for (int i = 0; i < nVars; i++) {
			sum += pow(data[0][i] - smean,2);
		}
		
		std[0] = sqrt(sum / (nVars - 1));
	}
	free(mean);

}

void bsxfun(double **res ,double ** ref_spectra, int nVars, int refOrder) {

	for (int col = 0; col < nVars; col++) {
		for (int row = 1; row < refOrder; row++) {
			res[row-1][col] = (ref_spectra[row][col] - ref_spectra[0][col]);
		}
	}
}


void getTransposeMatrix(double arr[5][100], double res[100][5]) {
	for (int i = 0; i < 100; i++) {
		for (int y = 0; y < 5; y++) {
			res[i][y] = arr[y][i];
		}
	}
}

void matMult(double **arrA, double **arrB, int rowA, int colB, int elem, double **res) {
	long double sum = 0;
	double probe1 = 0;
	double probe2 = 0;
	for (int row = 0; row < rowA; row++) {
		for (int col = 0; col < colB; col++) {


			for (int y = 0; y < elem; y++) {
				probe1 = arrA[row][y];
				probe2 = arrB[y][col];
				sum += arrA[row][y] * arrB[y][col];
			}
			res[row][col] = sum;
			sum = 0;
		}
	}
}



//---------------------------------------------------------------------
// http://www.sanfoundry.com/c-program-find-inverse-matrix/
//---------------------------------------------------------------------
double determinant(double ** a, int k)
{
	double ** b = (double **)malloc(k * sizeof(double*));
	for (int row = 0; row < k; row++) {
		b[row] = (double*)malloc(k * sizeof(double));
	}

	double s = 1, det = 0;
	int i, j, m, n, c;
	if (k == 1)
	{
		return (a[0][0]);
	}
	else
	{
		det = 0;
		for (c = 0; c < k; c++)
		{
			m = 0;
			n = 0;
			for (i = 0; i < k; i++)
			{
				for (j = 0; j < k; j++)
				{
					b[i][j] = 0;
					if (i != 0 && j != c)
					{
						b[m][n] = a[i][j];
						if (n < (k - 2))
							n++;
						else
						{
							n = 0;
							m++;
						}
					}
				}
			}
			det = det + s * (a[0][c] * determinant(b, k - 1));
			s = -1 * s;
		}
	}

	return (det);
}

/*Finding transpose of matrix*/
void transpose(double ** num, double ** fac, double r, double ** res)
{
	int i, j;
	//double b[5][5], inverse[5][5], d;
	double d;
	double ** b = (double **)malloc(r * sizeof(double*));
	double ** inverse = (double **)malloc(r * sizeof(double*));
	for (int row = 0; row < r; row++) {
		b[row] = (double*)malloc(r * sizeof(double));
		inverse[row] = (double*)malloc(r * sizeof(double));
	}
	for (i = 0; i < r; i++)
	{
		for (j = 0; j < r; j++)
		{
			b[i][j] = fac[j][i];
		}
	}
	d = determinant(num, r);
	for (i = 0; i < r; i++)
	{
		for (j = 0; j < r; j++)
		{
			inverse[i][j] = b[i][j] / d;
		}
	}

	for (i = 0; i < r; i++)
	{
		for (j = 0; j < r; j++)
		{
			res[i][j] = inverse[i][j];
		}
	}
}

void cofactor(double ** num, double f, double ** res)
{
	double ** b = (double **)malloc(f * sizeof(double*));
	double ** fac = (double **)malloc(f * sizeof(double*));
	for (int row = 0; row < f; row++) {
		b[row] = (double*)malloc(f * sizeof(double));
		fac[row] = (double*)malloc(f * sizeof(double));
	}
	//double fac[6][6];
	int p, q, m, n, i, j;
	for (q = 0; q < f; q++)
	{
		for (p = 0; p < f; p++)
		{
			m = 0;
			n = 0;
			for (i = 0; i < f; i++)
			{
				for (j = 0; j < f; j++)
				{
					if (i != q && j != p)
					{
						b[m][n] = num[i][j];
						if (n < (f - 2))
							n++;
						else
						{
							n = 0;
							m++;
						}
					}
				}
			}
			fac[q][p] = pow(-1, q + p) * determinant(b, f - 1);
		}
	}
	transpose(num, fac, f, res);
}

//----------------------------------------------------------------------

double ** initialize(int rows, int columns) {
	double **temp;
	temp = (double **)malloc(rows * sizeof(double*));
	for (int row = 0; row < rows; row++) {
		temp[row] = (double*)malloc(columns * sizeof(double));
	}
	return temp;
}


// nObs = pixels, nVar = depth
void EMSC(double ** raw, double ** ref_spectra, double ** corrected, int nVars, int nObs, int refOrder) {

	//---------------------DECLARATIONS---------------------
	double temp[2] = { 0 };
	double ** M = initialize(refOrder + 3, nObs);
	double ** M_trans = initialize(nObs, refOrder + 3);
	double ** mult2_res = initialize(nObs, refOrder + 3);
	double ** mult1_res = initialize(refOrder+3, refOrder+3);
	double * std = (double *)malloc(nVars * sizeof(double));
	double * p = (double *)malloc((refOrder + 3) * sizeof(double));
	double ** K = initialize(refOrder-1, nVars);
	double mean_t, num = 0;

	//------------------------------------------------------

	//Calculates standard deviation of the ref_spectra
	//calculateSD(std, ref_spectra, nVars, refOrder);


	//something
	bsxfun(K,	ref_spectra, nVars, refOrder);


	//Construct the M matrix
	for (int i = 0; i < nVars; i++) {

		//Add 1 in first row
		M[0][i] = 1;
		M_trans[i][0] = 1;

		//Add mean in second row
		temp[0] = ref_spectra[0][i];
		temp[1] = ref_spectra[1][i];
		mean_t = mean(2, temp);
		M[1][i] = mean_t;
		M_trans[i][1] = mean_t;


		for (int y = 0; y < refOrder-1; y++) {
			M[y + 2][i] = K[y][i];
			M_trans[i][y+2] = K[y][i];
		}
		

		//Add linspace and linspace squared
		M[refOrder+1][i] = num;
		M_trans[i][refOrder+1] = num;

		M[refOrder + 2][i] = pow(num, 2);
		M_trans[i][refOrder + 2] = pow(num, 2);
		num += (1.0 / (nVars-1));
	}

	//for (int i = 0; i < 52; i++) {
	//	printf("%f  %f  %f  %f  %f\n", M[0][i], M[1][i], M[2][i], M[3][i], M[4][i]);
	//}

	//Multiply M with M transpose, stores the result in mult1_res.
	matMult(M, M_trans, refOrder+3, refOrder+3, nVars, mult1_res);

	//for (int i = 0; i < 52; i++) {
	//	printf("%f  %f  %f  %f  %f\n", M_trans[i][0], M_trans[i][1], M_trans[i][2], M_trans[i][3], M_trans[i][4]);
	//}

	//for (int i = 0; i < 5; i++) {
	//	printf("%f  %f  %f  %f  %f\n", mult1_res[i][0], mult1_res[i][1], mult1_res[i][2], mult1_res[i][3], mult1_res[i][4]);
	//}

	//Calculates inverse
	double ** inverse = initialize(refOrder + 3, refOrder + 3);
	cofactor(mult1_res, refOrder + 3, inverse);


	//Multiplies the M transpose with the inverse to achieve: M'*inv(M*M'), stores the result in mult2_res.
	matMult(M_trans, inverse, nObs, refOrder + 3, refOrder + 3, mult2_res);
	

	//THIS PART SHOULD BE DONE IN HARDWARE
	double sum = 0;
	for (int idx = 0; idx < nObs; idx++) {

		for (int i = 0; i < refOrder + 3; i++) {
			for (int y = 0; y <nVars; y++) {
				sum += raw[idx][y] * mult2_res[y][i];
			}
			p[i] = sum;
			
			sum = 0;
		}
		
		for (int t = 0; t < nVars; t++) {
			corrected[idx][t] = (raw[idx][t] - p[0] - p[3] * M[refOrder + 1][t] - p[4] * M[refOrder + 2][t]) / p[1];
		}
	}


	//--------------------------------------

	free(M);
	free(M_trans);
	free(mult2_res);
	free(mult1_res);
	free(K);
	free(std);
	free(p);
	}
	

void write_file(double ** arr, char * filename, int nVars, int nObs) {
	FILE *fp;
	fp = fopen(filename, "w");
	for (int row = 0; row < nObs; row++) {
		for (int col = 0; col < nVars; col++) {
			fprintf(fp, "%f\n", arr[row][col]);
		}
		//fprintf(fp, "\n");
	}
	fclose(fp);
}

void read_file(double ** arr, char * filename, int nVars, int nObs) {
	FILE *fp;
	int row = 0;
	int column = 0;
	char buff[20];
	fp = fopen(filename, "r");
	while (fgets(buff, 10, (FILE*)fp) != NULL) {
		arr[row][column] = atof(buff);
		column += 1;
		if (column >= nVars) {
			row += 1;
			column = 0;
		}
	}
	fclose(fp);

}


void write_test_file() {
	FILE *write_ptr;
	unsigned char buffer[10];
	write_ptr = fopen("test.bin", "wb");
	for (int row = 0; row < 100; row++) {
		for (int col = 0; col < 100; col++) {
			sprintf(buffer, "%g", col);
			fwrite(buffer, sizeof(buffer), 1, write_ptr);
		}
	}
	fclose(write_ptr);
}


void execute_EMSC(double ** raw, double ** ref_spectra, double ** corrected, int nVars, int nObs, int refOrder, int bin_enable, int bin_size){
	double **raw_bin = initialize(bin_size, nVars);
	double **corrected_bin = initialize(bin_size, nVars);
	int it = nObs / bin_size;

	if (bin_enable == 1) {

		for (int round = 0; round < it; round++) {
			
			for (int x = 0; x < bin_size; x++) {
				for (int y = 0; y < nVars; y++) {
					raw_bin[x][y] = raw[x][y];
				}
			}

		EMSC(raw_bin, ref_spectra, corrected_bin, nVars, bin_size, refOrder);

		for (int row = 0; row < bin_size; row++) {
			for (int col = 0; col < nVars; col++) {
				corrected[row + round * 100][col] = corrected_bin[row][col];
			}
		}

		}
	}
	else {
		EMSC(raw, ref_spectra, corrected, nVars, nObs, refOrder);
	}

}

void main() {
	
	int nVars = 52; //wavelenghts
	int nObs  = 10000; //m*n
	int refOrder = 2; //numbers of species in spectra
	int bin_enable = 0; //1 = enabled
	int bin_size = 10000; // size of bins
	int it = nObs / bin_size;

	double **raw = initialize(nObs,nVars);

	
	
	double **ref_spectra = initialize(refOrder, nVars);
	double **corrected = initialize(nObs, nVars);
	double ***bin_ptr = initialize(nObs, nVars);

	
	read_file(raw,"cube_test.txt",nVars, nObs);
	for (int i = 0; i < nVars; i++) {
		ref_spectra[0][i] = raw[2][i];
		//ref_spectra[1][i] = raw[43][i];
		ref_spectra[1][i] = raw[84][i];
	}
	
	/*
	for (int x = 0; x < bin_size; x++) {
		for (int y = 0; y < nVars; y++){
			raw1[x][y] = raw[x][y];
		}
	}
	*/

	printf("%f  %f  %f\n",raw[0][0], raw[0][1], raw[0][2]);
	//printf("%f\n",std[0]);
	

	//EMSC(raw, ref_spectra, corrected, nVars, nObs, refOrder);
	
	execute_EMSC(raw, ref_spectra, corrected, nVars, nObs, refOrder, bin_enable, bin_size);
	
	printf("%f  %f  %f\n", corrected[0][0], corrected[0][1], corrected[0][2]);
	write_file(corrected,"corrected.txt", nVars, nObs);
	
	
	system("PAUSE");

}



