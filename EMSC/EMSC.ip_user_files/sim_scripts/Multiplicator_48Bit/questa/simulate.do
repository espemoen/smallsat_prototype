onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib Multiplicator_48Bit_opt

do {wave.do}

view wave
view structure
view signals

do {Multiplicator_48Bit.udo}

run -all

quit -force
