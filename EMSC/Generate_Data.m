%%Block Ram data
A = randi(1000, 50, 1);
A = uint32(A);

filename = 'test.bin';
fileid = fopen(filename, 'wb');
fwrite(fileid,A,'int');
fclose(fileid);

%% DotProduct Data
v_len = 10;
it = 10;
in_G = randi(1000, v_len*it, 1);
in_raw = randi(4095, v_len*it, 1);

file_in_G = 'in_G.bin';
fileid = fopen(file_in_G, 'wb');
fwrite(fileid,in_G,'int');
fclose(fileid);


file_in_raw = 'in_raw.bin';
fileid = fopen(file_in_raw, 'wb');
fwrite(fileid,in_raw,'int');
fclose(fileid);
%% Result

in_raw_val = in_raw';
in_G_val = in_G';

add = 0;
mul = 0;
mul_res = zeros(1,v_len);
add_res = zeros(1,v_len);
for i=1:v_len
   mul = in_raw_val(i)*in_G_val(i);
   add = add + mul;
   mul_res(i) = mul;
   add_res(i) = add;
end
clc;
fprintf("MUL = ");
disp(mul_res);
fprintf("\nADD = ");
disp(add_res);
fprintf("\nRES = ");
disp(dot(in_raw_val,in_G_val));

%% Res
clc;

in_raw_val = in_raw';
in_G_val = in_G';

for i=1:it
   %disp(in_raw_val((i-1)*v_len+1:(i-1)*v_len+10));
   %disp(in_G_val((i-1)*v_len+1:(i-1)*v_len+10));
   disp(dot(in_raw_val((i-1)*v_len+1:(i-1)*v_len+10),in_G_val((i-1)*v_len+1:(i-1)*v_len+10))); 
end

%% G
%clear all;
load r_G.mat;
load raw.mat;


in_G = zeros(1,52*8);

it = 1;
for col = 1:8
    for row = 1:52
        in_G(it) = r_G(row,col);
        it = it + 1;
    end
end

in_G = in_G';
%%
file_in_G = 'in_G.bin';
fileid = fopen(file_in_G, 'wb');
fwrite(fileid,in_G,'int');
fclose(fileid);

%% RAW
in_raw = zeros(1,52);
raw = floor(raw);
it = 1;

for row = 1:10000
    for col = 1:52
        in_raw(it) = raw(row,col);
        it = it + 1;
    end
end

in_raw = in_raw';

%%
for idx = 1:10000
    p = raw(idx,:)*r_G;
    P(idx,:) = p;   
end

%%
file_in_raw = 'in_raw.bin';
fileid = fopen(file_in_raw, 'wb');
fwrite(fileid,in_raw,'int16');
fclose(fileid);

%% 

mul_add = zeros(2,53);
add = 0;
B = r_G(:,1:8);
A = raw(1,:);

for i=1:52
   mul_add(1,i) = A(i)*B(i);
   add = add + (in_raw(i)*in_G(i));
   mul_add(2,i+1) = add;
end


%%
clear all;
f = fopen('out.log', 'r+');
it = 1;
while(~feof(f))
    tline = fgetl(f);
    res(it,:) = strsplit(tline,';');
    it = it +1;
end
res = str2double(res(:,1:8));

fclose(f);

%% DELETE CONTENT OF out.log
f = fopen('out.log','w');
fclose(f);