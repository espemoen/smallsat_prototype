/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: Pseudoinverse_v2_terminate.c
 *
 * MATLAB Coder version            : 3.3
 * C/C++ source code generated on  : 17-Jan-2018 17:00:17
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "Pseudoinverse_v2.h"
#include "Pseudoinverse_v2_terminate.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void Pseudoinverse_v2_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for Pseudoinverse_v2_terminate.c
 *
 * [EOF]
 */
