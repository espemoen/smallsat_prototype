/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: Pseudoinverse_v2.c
 *
 * MATLAB Coder version            : 3.3
 * C/C++ source code generated on  : 17-Jan-2018 17:00:17
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "Pseudoinverse_v2.h"
#include "svd.h"

/* Function Definitions */

/*
 * Arguments    : const double M[500]
 *                const double raw[10000]
 *                const double wlens[100]
 *                const double wlensSQ[100]
 *                double nObs
 *                double corrected[10000]
 * Return Type  : void
 */
void Pseudoinverse_v2(const double M[500], const double raw[10000], const double
                      wlens[100], const double wlensSQ[100], double nObs, double
                      corrected[10000])
{
  int idx;
  int i0;
  boolean_T p;
  int k;
  double y[25];
  double X[25];
  int vcol;
  double U[25];
  double s[5];
  double V[25];
  double absxk;
  int exponent;
  int r;
  double b_p[5];
  int ar;
  int ic;
  int ib;
  int ia;
  memcpy(&corrected[0], &raw[0], 10000U * sizeof(double));
  for (idx = 0; idx < (int)nObs; idx++) {
    for (i0 = 0; i0 < 5; i0++) {
      for (k = 0; k < 5; k++) {
        y[i0 + 5 * k] = 0.0;
        for (vcol = 0; vcol < 100; vcol++) {
          y[i0 + 5 * k] += M[i0 + 5 * vcol] * M[k + 5 * vcol];
        }
      }
    }

    p = false;
    for (k = 0; k < 25; k++) {
      X[k] = 0.0;
      if (p || rtIsInf(y[k]) || rtIsNaN(y[k])) {
        p = true;
      } else {
        p = false;
      }
    }

    if (p) {
      for (i0 = 0; i0 < 25; i0++) {
        X[i0] = rtNaN;
      }
    } else {
      svd(y, U, s, V);
      absxk = fabs(s[0]);
      if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
        if (absxk <= 2.2250738585072014E-308) {
          absxk = 4.94065645841247E-324;
        } else {
          frexp(absxk, &exponent);
          absxk = ldexp(1.0, exponent - 53);
        }
      } else {
        absxk = rtNaN;
      }

      absxk *= 5.0;
      r = 0;
      k = 1;
      while ((k < 6) && (s[k - 1] > absxk)) {
        r++;
        k++;
      }

      if (r > 0) {
        vcol = 0;
        for (ar = 1; ar <= r; ar++) {
          absxk = 1.0 / s[ar - 1];
          for (k = vcol; k + 1 <= vcol + 5; k++) {
            V[k] *= absxk;
          }

          vcol += 5;
        }

        for (k = 0; k <= 21; k += 5) {
          for (ic = k; ic + 1 <= k + 5; ic++) {
            X[ic] = 0.0;
          }
        }

        vcol = -1;
        for (k = 0; k <= 21; k += 5) {
          ar = -1;
          vcol++;
          i0 = (vcol + 5 * (r - 1)) + 1;
          for (ib = vcol; ib + 1 <= i0; ib += 5) {
            if (U[ib] != 0.0) {
              ia = ar;
              for (ic = k; ic + 1 <= k + 5; ic++) {
                ia++;
                X[ic] += U[ib] * V[ia];
              }
            }

            ar += 5;
          }
        }
      }
    }

    for (i0 = 0; i0 < 5; i0++) {
      s[i0] = 0.0;
      for (k = 0; k < 100; k++) {
        s[i0] += raw[idx + 100 * k] * M[i0 + 5 * k];
      }
    }

    for (i0 = 0; i0 < 5; i0++) {
      b_p[i0] = 0.0;
      for (k = 0; k < 5; k++) {
        b_p[i0] += s[k] * X[k + 5 * i0];
      }
    }

    for (i0 = 0; i0 < 100; i0++) {
      corrected[idx + 100 * i0] = (((raw[idx + 100 * i0] - b_p[0]) - b_p[3] *
        wlens[i0]) - b_p[4] * wlensSQ[i0]) / b_p[1];
    }
  }
}

/*
 * File trailer for Pseudoinverse_v2.c
 *
 * [EOF]
 */
