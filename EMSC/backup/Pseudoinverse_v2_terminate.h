/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: Pseudoinverse_v2_terminate.h
 *
 * MATLAB Coder version            : 3.3
 * C/C++ source code generated on  : 17-Jan-2018 17:00:17
 */

#ifndef PSEUDOINVERSE_V2_TERMINATE_H
#define PSEUDOINVERSE_V2_TERMINATE_H

/* Include Files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "Pseudoinverse_v2_types.h"

/* Function Declarations */
extern void Pseudoinverse_v2_terminate(void);

#endif

/*
 * File trailer for Pseudoinverse_v2_terminate.h
 *
 * [EOF]
 */
