/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: xswap.h
 *
 * MATLAB Coder version            : 3.3
 * C/C++ source code generated on  : 17-Jan-2018 17:00:17
 */

#ifndef XSWAP_H
#define XSWAP_H

/* Include Files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "Pseudoinverse_v2_types.h"

/* Function Declarations */
extern void xswap(double x[25], int ix0, int iy0);

#endif

/*
 * File trailer for xswap.h
 *
 * [EOF]
 */
