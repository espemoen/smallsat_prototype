/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: svd.h
 *
 * MATLAB Coder version            : 3.3
 * C/C++ source code generated on  : 17-Jan-2018 17:00:17
 */

#ifndef SVD_H
#define SVD_H

/* Include Files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "Pseudoinverse_v2_types.h"

/* Function Declarations */
extern void svd(const double A[25], double U[25], double s[5], double V[25]);

#endif

/*
 * File trailer for svd.h
 *
 * [EOF]
 */
