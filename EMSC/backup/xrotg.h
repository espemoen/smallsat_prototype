/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: xrotg.h
 *
 * MATLAB Coder version            : 3.3
 * C/C++ source code generated on  : 17-Jan-2018 17:00:17
 */

#ifndef XROTG_H
#define XROTG_H

/* Include Files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "Pseudoinverse_v2_types.h"

/* Function Declarations */
extern void xrotg(double *a, double *b, double *c, double *s);

#endif

/*
 * File trailer for xrotg.h
 *
 * [EOF]
 */
