set_property IOSTANDARD LVCMOS33 [get_ports FCLK_CLK0_0]


set_property SLEW SLOW [get_ports FCLK_CLK0_0]
set_property DRIVE 12 [get_ports FCLK_CLK0_0]
set_property PACKAGE_PIN E4 [get_ports FCLK_CLK0_0]
