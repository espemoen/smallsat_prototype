library IEEE;
Library xpm;
use IEEE.STD_LOGIC_1164.ALL;
use xpm.vcomponents.all;

entity fifo_module is
    Generic(
            FIFO_DEPTH : integer := 16;
            FIFO_SIZE : integer := 52;
            WRITE_DATA_WIDTH : integer := 64;
            WR_DATA_COUNT_WIDTH : integer := 5;
            FIFO_MARGIN : integer := 5;
            RD_DATA_COUNT_WIDTH : integer := 5;
            READ_DATA_WIDTH : integer := 16;
            LATENCY_CYCLES  : integer := 13
    );
    Port ( 
            clk : in std_logic;
            aresetn : in std_logic;
            enable : in std_logic;
            fifo_read : in std_logic_vector(3 downto 0);
            fifo_in : in std_logic_vector(63 downto 0);
            fifo_out: out std_logic_vector(16*4-1 downto 0);
            fifo_empty : out std_logic_vector(3 downto 0);
            fifo_full : out std_logic_vector(3 downto 0)
    );
end fifo_module;

architecture Behavioral of fifo_module is
    signal reset : std_logic;
    signal fifo_enable : std_logic_vector(3 downto 0);
    signal fifo_sel : integer range 0 to 4;
    signal fifo_in_w : std_logic_vector(64*4-1 downto 0);
    signal rd_flag : std_logic;
    signal fifo_in_delay : std_logic_vector(63 downto 0);
    
    TYPE State_type IS (IDLE, FIFO1, FIFO2, FIFO3, FIFO4);  -- Define the states
    SIGNAL state : State_Type;    -- Create a signal that uses 
    
begin

reset <= not aresetn;


process(clk, aresetn)
    variable counter : integer := 0;
    variable start : std_logic := '0';
begin
    if(aresetn = '0') then
        --fifo_enable <= (others => '0');
        fifo_sel <= 1;
        state <= IDLE;
        counter := 0;
        fifo_in_delay <= (others => '0');
        start := '0';
    elsif(rising_edge(clk)) then
        case state is
            when IDLE =>
                if(enable = '1') then
                    --fifo_enable(0) <= '1';
                    fifo_sel <= 1;
                    state <= FIFO1;
                end if;
            when FIFO1 =>
                if(enable = '1') then
                   -- fifo_enable(0) <= '1';
                    counter := counter + 1;
                    if(start = '0') then
                        if(counter >= LATENCY_CYCLES-1) then
                            state <= FIFO2;
                            --fifo_enable(1) <= '1';
                            fifo_sel <= 2;
                            counter := 0;
                        end if;
                    else
                        if(counter >= LATENCY_CYCLES) then
                            state <= FIFO2;
                            --fifo_enable(1) <= '1';
                            fifo_sel <= 2;
                            counter := 0;
                        end if;
                   end if;
                end if;
            when FIFO2 =>
                if(enable = '1') then
                    --fifo_enable(1) <= '1';
                    counter := counter +1;
                    if(counter >= LATENCY_CYCLES) then
                        state <= FIFO3;
                        --fifo_enable(1) <= '1';
                        fifo_sel <= 3;
                        counter := 0;
                    end if;
                end if;    
            when FIFO3 =>
                if(enable = '1') then
                    --fifo_enable(2) <= '1';
                    counter := counter +1;
                    if(counter >= LATENCY_CYCLES) then
                        state <= FIFO4;
                        --fifo_enable(1) <= '1';
                        fifo_sel <= 4;
                        counter := 0;
                    end if;
                end if;    
            when FIFO4 =>
                if(enable = '1') then
                    --fifo_enable(3) <= '1';
                    counter := counter +1;
                    if(counter >= LATENCY_CYCLES) then
                        state <= FIFO1;
                        --fifo_enable(1) <= '1';
                        fifo_sel <= 1;
                        counter := 0;
                        start := '1';
                    end if;
                end if;    
        end case;
        fifo_in_delay <= fifo_in;
    end if;
end process;

fifo_enable(0) <= '1' when ((state=FIFO1 or state=IDLE) and enable = '1') else '0';
fifo_enable(1) <= '1' when (state=FIFO2 and enable = '1') else '0';
fifo_enable(2) <= '1' when (state=FIFO3 and enable = '1') else '0';
fifo_enable(3) <= '1' when (state=FIFO4 and enable = '1') else '0';
--process(state,enable,fifo_sel)
--begin
--    case state is
--        when IDLE =>
--            if(enable = '1') then
--                fifo_enable(0) <= '1';
--            else
--                fifo_enable(0) <= '0';
--            end if;
--        when FIFO1 =>
--            if(enable = '1') then
--                fifo_enable(0) <= '1';
--            else
--                fifo_enable(0) <= '0';
--            end if;
--        when FIFO2 =>
--            if(enable = '1') then
--                fifo_enable(1) <= '1';
--            else
--                fifo_enable(1) <= '0';
--            end if; 
--        when FIFO3 =>
--            if(enable = '1') then
--                fifo_enable(2) <= '1';
--            else
--                fifo_enable(2) <= '0';
--            end if;  
--        when FIFO4 =>
--            if(enable = '1') then
--                fifo_enable(3) <= '1';
--            else
--                fifo_enable(3) <= '0';
--            end if;     
--    end case;
--end process;   




process(fifo_sel, fifo_in_delay)
begin
    case fifo_sel is
        when 1 =>
            fifo_in_w(63 downto 0) <= fifo_in_delay;
            fifo_in_w(255 downto 64) <= (others => '0');
        when 2 =>
            fifo_in_w(63 downto 0) <= (others => '0');
            fifo_in_w(127 downto 64) <= fifo_in_delay;
            fifo_in_w(255 downto 128) <= (others => '0');
        when 3 =>
            fifo_in_w(127 downto 0) <= (others => '0');
            fifo_in_w(191 downto 128) <= fifo_in_delay;
            fifo_in_w(255 downto 192) <= (others => '0');
        when 4 =>
            fifo_in_w(192 downto 0) <= (others => '0');
            fifo_in_w(255 downto 192) <= fifo_in_delay;
        when others =>
            fifo_in_w <= (others => '0');
    end case;
end process;





fifo_gen: for i in 0 to 3 generate
    fifo: entity work.fifo
     Generic map(
                FIFO_DEPTH          =>  FIFO_DEPTH,
                FIFO_SIZE           =>  FIFO_SIZE,
                WRITE_DATA_WIDTH    =>  WRITE_DATA_WIDTH,
                WR_DATA_COUNT_WIDTH =>  WR_DATA_COUNT_WIDTH,
                FIFO_MARGIN         =>  FIFO_MARGIN,
                RD_DATA_COUNT_WIDTH =>  RD_DATA_COUNT_WIDTH,
                READ_DATA_WIDTH     =>  READ_DATA_WIDTH,
                LATENCY_CYCLES      =>  LATENCY_CYCLES
        )
        Port  map( 
                clk                 =>  clk,
                aresetn             =>  aresetn,
                wr_en               =>  fifo_enable(i),
                rd_en               =>  fifo_read(i),
                fifo_in             =>  fifo_in_w(64*i+63 downto 64*i),
                fifo_out            =>  fifo_out(16*i+15 downto 16*i),
                fifo_empty          =>  fifo_empty(i),
                fifo_full           =>  fifo_full(i)
        );
    end generate fifo_gen;


end Behavioral;
