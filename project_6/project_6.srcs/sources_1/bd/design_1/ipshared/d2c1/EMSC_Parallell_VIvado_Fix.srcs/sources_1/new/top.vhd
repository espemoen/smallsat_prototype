library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;




entity top is
    Generic(
        B_RAM_SIZE      : integer := 100;
        B_RAM_BIT_WIDTH : integer := 32;
        NUM_B_RAM       : integer := 8;
        RAW_BIT_WIDTH : positive := 64;
        G_BIT_WIDTH   : positive := 32;
        P_BIT_WIDTH   : positive := 48;
        C_S_AXI_DATA_WIDTH : integer := 32;
        C_S_AXI_ADDR_WIDTH : integer := 6;
        NUM_PIXELS         : integer := 250000;
        
        FIFO_DEPTH : integer := 128;
        FIFO_SIZE : integer := 52;
        WRITE_DATA_WIDTH : integer := 64;
        WR_DATA_COUNT_WIDTH : integer := 7;
        FIFO_MARGIN : integer := 5;
        RD_DATA_COUNT_WIDTH : integer := 7;
        READ_DATA_WIDTH : integer := 16;
        LATENCY_CYCLES  : integer := 13
    );
   Port ( 
        clk     :   in std_logic;
        aresetn :   in std_logic;
        p_irq   :   out std_logic;
        
        --AXI in-stream
        s_axis_tdata  : in  std_logic_vector(RAW_BIT_WIDTH-1 downto 0);
        --DMA is ready to send data
        s_axis_tvalid : in  std_logic;
        --EMSC is ready to receive data
        s_axis_tready : out std_logic;
        --DMA say this is last data
        s_axis_tlast  : in  std_logic;
        
        --AXI out-stream
        m_axis_tdata  : out std_logic_vector(63 downto 0);
        --EMSC is ready to send to DMA.
        m_axis_tvalid : out std_logic;
        --DMA is ready to receive data
        m_axis_tready : in  std_logic;
        --Tell DMA this is last data
        m_axis_tlast  : out std_logic;
        
        
        -- Register interface
        s_axi_ctrl_status_awaddr  : in  std_logic_vector(5 downto 0);
        s_axi_ctrl_status_awprot  : in  std_logic_vector(2 downto 0);
        s_axi_ctrl_status_awvalid : in  std_logic;
        s_axi_ctrl_status_awready : out std_logic;
        s_axi_ctrl_status_wdata   : in  std_logic_vector(31 downto 0);
        s_axi_ctrl_status_wstrb   : in  std_logic_vector(3 downto 0);
        s_axi_ctrl_status_wvalid  : in  std_logic;
        s_axi_ctrl_status_wready  : out std_logic;
        s_axi_ctrl_status_bresp   : out std_logic_vector(1 downto 0);
        s_axi_ctrl_status_bvalid  : out std_logic;
        s_axi_ctrl_status_bready  : in  std_logic;
        s_axi_ctrl_status_araddr  : in  std_logic_vector(5 downto 0);
        s_axi_ctrl_status_arprot  : in  std_logic_vector(2 downto 0);
        s_axi_ctrl_status_arvalid : in  std_logic;
        s_axi_ctrl_status_arready : out std_logic;
        s_axi_ctrl_status_rdata   : out std_logic_vector(31 downto 0);
        s_axi_ctrl_status_rresp   : out std_logic_vector(1 downto 0);
        s_axi_ctrl_status_rvalid  : out std_logic;
        s_axi_ctrl_status_rready  : in  std_logic
   );
end top;

architecture Behavioral of top is

-- AXI in-stream signals
signal in_stream_data  : std_logic_vector(RAW_BIT_WIDTH-1 downto 0);
signal in_stream_valid : std_logic;
signal in_stream_ready : std_logic;
signal in_stream_last  : std_logic;
signal in_stream_handshake : std_logic;
signal in_stream_valid_delay : std_logic;
signal in_stream_ready_delay : std_logic;
signal in_raw_delay :   std_logic_vector(RAW_BIT_WIDTH-1 downto 0);


-- AXI out-stream signals
signal out_stream_data  : std_logic_vector(63 downto 0);
signal out_stream_valid : std_logic;
signal out_stream_ready : std_logic;
signal out_stream_last  : std_logic;
signal out_stream_handshake : std_logic;

-- Signals from/to b_ram_bank
signal read_enable : std_logic;
signal b_ram_out   : std_logic_vector(B_RAM_BIT_WIDTH*NUM_B_RAM-1 downto 0);
signal enable      : std_logic;
signal v_len       : std_logic_vector(11 downto 0);
signal initialized : std_logic;

-- Signals from/to dot_product_module
signal p_rdy    :   std_logic_vector(3 downto 0);
signal p_out    :   std_logic_vector(4*NUM_B_RAM*P_BIT_WIDTH-1 downto 0);
signal dp_extend_end : std_logic;
signal dp_enable : std_logic_vector(3 downto 0);
signal dp_data_in : std_logic_vector(B_RAM_BIT_WIDTH*NUM_B_RAM*4-1 downto 0);
signal dp_enable_reg : std_logic_vector(3*LATENCY_CYCLES downto 0);
-- Signals from/to AXI gear box
signal last_p : std_logic;

--Signal for fifo
signal fifo_enable: std_logic;
signal fifo_read: std_logic_vector(3 downto 0);
signal fifo_in : std_logic_vector(63 downto 0);
signal fifo_out : std_logic_vector(16*4-1 downto 0);
signal fifo_empty, fifo_full : std_logic_vector(3 downto 0);

--Signal FIFO delay Registers
signal fifo1_reg : std_logic_vector(NUM_B_RAM*G_BIT_WIDTH*(LATENCY_CYCLES+1)-1 downto 0);
signal fifo2_reg : std_logic_vector(NUM_B_RAM*G_BIT_WIDTH*((2*LATENCY_CYCLES)+1)-1 downto 0);
signal fifo3_reg : std_logic_vector(NUM_B_RAM*G_BIT_WIDTH*((3*LATENCY_CYCLES)+1)-1 downto 0);

signal last_flag : std_logic;
signal valid_in : std_logic;
signal p_irq_w : std_logic;


signal b_ram_register_timing_opt : std_logic_vector(B_RAM_BIT_WIDTH*NUM_B_RAM-1 downto 0);
signal fifo_register_timing_opt  : std_logic_vector(16*4-1 downto 0);

signal fifo_init : std_logic_vector(3 downto 0);
signal prev_fifo_init : std_logic_vector(3 downto 0);
signal dp_enable_delay : std_logic_vector(3 downto 0);
signal fifo_init_delay : std_logic_vector(3 downto 0);
signal in_stream_counter : integer range 0 to 20000000;

signal last_p_delay : std_logic;

TYPE State_type IS (INITIAL, CONTINOUS);  -- Define the states
SIGNAL state : State_Type;
begin

--Connections
in_stream_data  <= s_axis_tdata;
in_stream_valid <= s_axis_tvalid;
s_axis_tready   <= in_stream_ready; 
in_stream_last  <= s_axis_tlast;


in_stream_handshake <= '1' when (in_stream_valid = '1' and in_stream_ready = '1') else '0';

valid_in <= '1' when (in_stream_handshake = '1' and m_axis_tready = '1' and enable = '1' and initialized = '1') else '0';


fifo_enable <= '1' when ( in_stream_valid_delay = '1' and in_stream_ready_delay = '1' and enable = '1' and initialized = '1') or last_flag = '1' else '0';


p_irq <= p_irq_w;



process(clk, aresetn)
    variable p_count : integer := 0;
begin
    if(aresetn = '0') then
        b_ram_register_timing_opt <= (others => '0');
        fifo_register_timing_opt <= (others => '0');
        p_count := 0;
        last_p <= '0';
        last_p_delay <= '0';
    elsif(rising_edge(clk)) then
        last_p_delay <= p_rdy(3);
        b_ram_register_timing_opt <= b_ram_out;
        fifo_register_timing_opt <= fifo_out;
        if(last_p_delay = '1') then
            p_count := p_count + 1;
        end if;
        if(p_count = (NUM_PIXELS/4)) then
            last_p <= '1';
            p_count := 0;
        end if;
    end if;
end process;


read_enable <= '1' when fifo_init_delay(0) = '1' else '0';

fifo_read <= fifo_init_delay;

process(clk, aresetn)
    variable counter : integer;
    variable last_counter : integer;
    variable fifo : integer;
    variable invalid_flag : std_logic;
begin
    if(aresetn = '0') then
        counter := 0;
        last_counter := 0;
        fifo_init <= "0000";
        prev_fifo_init <= "0000";
        fifo := 0;
        last_flag <= '0';
        fifo_init_delay <= (others => '0');
        dp_enable_delay <= (others => '0');
    elsif(rising_edge(clk)) then
        fifo_init_delay <= fifo_init;
        dp_enable_delay <= fifo_init_delay;
        dp_enable <= dp_enable_delay;
        --last_p <= '0';
        if(in_stream_last  = '1') then
            last_flag <= '1';
        end if;
        if(valid_in = '1') then
            if(invalid_flag = '1') then
                fifo_init <= prev_fifo_init;
                invalid_flag := '0';
            end if;
            counter := counter + 1;
            if(counter = LATENCY_CYCLES) then
                fifo_init(fifo) <= '1';
                fifo := fifo + 1;
                if(fifo > 3) then
                    fifo := 0;
                end if;
                counter := 0;
            end if;
        elsif(last_flag = '1') then
            fifo_init <= "1111";
            last_counter := last_counter + 1;
            if(last_p = '1') then
                last_flag <= '0';
                last_counter := 0;
            end if;
        else
            if(invalid_flag = '0') then
                prev_fifo_init <= fifo_init;
            end if;
            invalid_flag := '1';
            fifo_init <= (others => '0');
        end if;
    end if;
end process;

--process(clk, aresetn)
--    variable counter : integer := 0;
--    variable counter_2 : integer := 0;
--    --variable last_flag : std_logic := '0';
--begin
--    if(aresetn = '0') then
--        counter := 0;
--        counter_2 := 0;
--        dp_enable_reg <= (others => '0');
--        read_enable <= '0';
--        last_p <= '0';
--        last_flag <= '0';
--    elsif(rising_edge(clk)) then
--        last_p <= in_stream_last;
--        if(last_p = '1') then
--            last_flag <= '1';
--        end if;
--        if(fifo_enable = '1' or last_flag = '1') then
--            if(counter >= LATENCY_CYCLES) then
--                read_enable <= '1';
--            end if;
--            if(counter >= LATENCY_CYCLES+1) then
--                dp_enable_reg <= dp_enable_reg(3*LATENCY_CYCLES-1 downto 0) & '1';
--            end if;
--            if(p_rdy(0) = '1') then
--                last_flag <= '0';
--            end if;
--            counter := counter + 1;
--        else
--            dp_enable_reg <= dp_enable_reg(3*LATENCY_CYCLES-1 downto 0) & '0';
--        end if; 
--    end if;
--end process;



-- Controls delay from b_ram
dp_data_in(NUM_B_RAM*G_BIT_WIDTH-1 downto 0) <= b_ram_register_timing_opt;
dp_data_in(2*NUM_B_RAM*G_BIT_WIDTH-1 downto NUM_B_RAM*G_BIT_WIDTH) <= fifo1_reg(NUM_B_RAM*G_BIT_WIDTH*(LATENCY_CYCLES+1)-G_BIT_WIDTH*NUM_B_RAM-1 downto NUM_B_RAM*G_BIT_WIDTH*(LATENCY_CYCLES+1)-G_BIT_WIDTH*NUM_B_RAM-(NUM_B_RAM*G_BIT_WIDTH));
dp_data_in(3*NUM_B_RAM*G_BIT_WIDTH-1 downto 2*NUM_B_RAM*G_BIT_WIDTH) <= fifo2_reg(NUM_B_RAM*G_BIT_WIDTH*((2*LATENCY_CYCLES)+1)-G_BIT_WIDTH*NUM_B_RAM-1 downto NUM_B_RAM*G_BIT_WIDTH*((2*LATENCY_CYCLES)+1)-G_BIT_WIDTH*NUM_B_RAM-(NUM_B_RAM*G_BIT_WIDTH));
dp_data_in(4*NUM_B_RAM*G_BIT_WIDTH-1 downto 3*NUM_B_RAM*G_BIT_WIDTH) <= fifo3_reg(NUM_B_RAM*G_BIT_WIDTH*((3*LATENCY_CYCLES)+1)-G_BIT_WIDTH*NUM_B_RAM-1 downto NUM_B_RAM*G_BIT_WIDTH*((3*LATENCY_CYCLES)+1)-G_BIT_WIDTH*NUM_B_RAM-(NUM_B_RAM*G_BIT_WIDTH));
process(clk, aresetn)
begin
    if(aresetn = '0') then
        fifo1_reg <= (others => '0');
        fifo2_reg <= (others => '0');
        fifo3_reg <= (others => '0');
    elsif(rising_edge(clk)) then
        if (dp_enable(0) = '1') then
            fifo1_reg <= fifo1_reg(NUM_B_RAM*G_BIT_WIDTH*(LATENCY_CYCLES+1)-G_BIT_WIDTH*NUM_B_RAM-1 downto 0) & b_ram_register_timing_opt;
            fifo2_reg <= fifo2_reg(NUM_B_RAM*G_BIT_WIDTH*((2*LATENCY_CYCLES)+1)-G_BIT_WIDTH*NUM_B_RAM-1 downto 0) & b_ram_register_timing_opt;
            fifo3_reg <= fifo3_reg(NUM_B_RAM*G_BIT_WIDTH*((3*LATENCY_CYCLES)+1)-G_BIT_WIDTH*NUM_B_RAM-1 downto 0) & b_ram_register_timing_opt;
        end if;
    end if;
end process;

--in_stream_ready <= '1' when (enable = '1' and initialized = '1' and m_axis_tready = '1') else '0';
-- Sets in_handshake and delay
process(clk, aresetn)
    variable counter : integer := 0;
begin
    if (aresetn = '0') then
        in_stream_ready <= '0';
        in_stream_ready_delay <= '0';
        in_stream_valid_delay <= '0';
        counter := 0;
        in_raw_delay <= (others => '0');
    elsif(rising_edge(clk)) then
        in_raw_delay <= in_stream_data;
        in_stream_valid_delay <= in_stream_valid;
        in_stream_ready_delay <= in_stream_ready;
        if(enable = '1' and initialized = '1' and m_axis_tready = '1') then
            in_stream_ready <= '1';
        else
            in_stream_ready <= '0';
        end if;
    end if;
end process;



fifo: entity work.fifo_module
    generic map(
        FIFO_DEPTH          =>  FIFO_DEPTH,
        FIFO_SIZE           =>  FIFO_SIZE,
        WRITE_DATA_WIDTH    =>  WRITE_DATA_WIDTH,
        WR_DATA_COUNT_WIDTH =>  WR_DATA_COUNT_WIDTH,
        FIFO_MARGIN         =>  FIFO_MARGIN,
        RD_DATA_COUNT_WIDTH =>  RD_DATA_COUNT_WIDTH,
        READ_DATA_WIDTH     =>  READ_DATA_WIDTH,
        LATENCY_CYCLES      =>  LATENCY_CYCLES
    )
    port map(
        clk         =>  clk,
        aresetn     =>  aresetn,
        enable      =>  fifo_enable,
        fifo_read   =>  fifo_read,
        fifo_in     =>  in_stream_data,
        fifo_out    =>  fifo_out,
        fifo_empty  =>  fifo_empty,
        fifo_full   =>  fifo_full
    );

b_ram: entity work.b_ram_bank
  Generic map(
          B_RAM_SIZE         =>  B_RAM_SIZE,
          B_RAM_BIT_WIDTH    =>  B_RAM_BIT_WIDTH,
          NUM_B_RAM          =>  NUM_B_RAM,
          C_S_AXI_DATA_WIDTH =>  C_S_AXI_DATA_WIDTH,
          C_S_AXI_ADDR_WIDTH =>  C_S_AXI_ADDR_WIDTH
       )
  Port map( 
        clk             =>  clk,
        aresetn         =>  aresetn,
        
        -- B_ram interface
        read_enable     =>  read_enable,

        data_out        =>  b_ram_out,
        v_len           =>  v_len,
        init_flag       =>  initialized,
        
        
        -- Register interface
        enable          =>  enable,
        s_axi_ctrl_status_awaddr    =>  s_axi_ctrl_status_awaddr,
        s_axi_ctrl_status_awprot    =>  s_axi_ctrl_status_awprot,
        s_axi_ctrl_status_awvalid   =>  s_axi_ctrl_status_awvalid,
        s_axi_ctrl_status_awready   =>  s_axi_ctrl_status_awready,
        s_axi_ctrl_status_wdata     =>  s_axi_ctrl_status_wdata,
        s_axi_ctrl_status_wstrb     =>  s_axi_ctrl_status_wstrb,
        s_axi_ctrl_status_wvalid    =>  s_axi_ctrl_status_wvalid,
        s_axi_ctrl_status_wready    =>  s_axi_ctrl_status_wready,
        s_axi_ctrl_status_bresp     =>  s_axi_ctrl_status_bresp,
        s_axi_ctrl_status_bvalid    =>  s_axi_ctrl_status_bvalid,
        s_axi_ctrl_status_bready    =>  s_axi_ctrl_status_bready,
        s_axi_ctrl_status_araddr    =>  s_axi_ctrl_status_araddr,
        s_axi_ctrl_status_arprot    =>  s_axi_ctrl_status_arprot,
        s_axi_ctrl_status_arvalid   =>  s_axi_ctrl_status_arvalid,
        s_axi_ctrl_status_arready   =>  s_axi_ctrl_status_arready,
        s_axi_ctrl_status_rdata     =>  s_axi_ctrl_status_rdata,
        s_axi_ctrl_status_rresp     =>  s_axi_ctrl_status_rresp,
        s_axi_ctrl_status_rvalid    =>  s_axi_ctrl_status_rvalid,
        s_axi_ctrl_status_rready    =>  s_axi_ctrl_status_rready
  );


gen_dp: for i in 0 to 3 generate
dp: entity work.dot_product_module
    generic map(
        RAW_BIT_WIDTH   =>  16,
        G_BIT_WIDTH     =>  G_BIT_WIDTH,
        NUM_B_RAM       =>  NUM_B_RAM,
        P_BIT_WIDTH     =>  P_BIT_WIDTH
    )
    port map(
        clk             =>  clk,
        aresetn         =>  aresetn,
        en              =>  dp_enable(i),
        in_G            =>  dp_data_in((G_BIT_WIDTH*NUM_B_RAM)*i+(G_BIT_WIDTH*NUM_B_RAM)-1 downto (G_BIT_WIDTH*NUM_B_RAM)*i),
        in_raw          =>  fifo_register_timing_opt(i*16+15 downto i*16),
        v_len           =>  v_len,
        p_rdy           =>  p_rdy(i),
        p_out           =>  p_out(NUM_B_RAM*P_BIT_WIDTH*i + NUM_B_RAM*P_BIT_WIDTH-1 downto NUM_B_RAM*P_BIT_WIDTH*i)
    );
end generate gen_dp;

gb: entity work.axi_gearbox
    generic map(
        B_RAM_SIZE  =>  B_RAM_SIZE,
        B_RAM_BIT_WIDTH => B_RAM_BIT_WIDTH,
        NUM_B_RAM   =>  NUM_B_RAM,
        RAW_BIT_WIDTH   =>  RAW_BIT_WIDTH,
        G_BIT_WIDTH     =>  G_BIT_WIDTH,
        P_BIT_WIDTH     =>  P_BIT_WIDTH,
        C_S_AXI_DATA_WIDTH  =>  C_S_AXI_DATA_WIDTH,
        C_S_AXI_ADDR_WIDTH  =>  C_S_AXI_ADDR_WIDTH
    )
    port map(
        clk     =>      clk,
        aresetn =>      aresetn,
        p_out   =>      p_out,
        p_rdy   =>      p_rdy,
        p_int   =>      p_irq_w,
        last_p  =>      last_p,
        m_axis_tdata    =>  m_axis_tdata,
        m_axis_tvalid   =>  m_axis_tvalid,
        m_axis_tready   =>  m_axis_tready,
        m_axis_tlast    =>  m_axis_tlast,
        out_stream_handshake => out_stream_handshake
    );


end Behavioral;
