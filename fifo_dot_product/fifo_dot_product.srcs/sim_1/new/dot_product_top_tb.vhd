library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity dot_product_top_tb is
--  Port ( );
end dot_product_top_tb;

architecture Behavioral of dot_product_top_tb is


component dot_product_top
      port (
        clk     : in std_logic;
        aresetn : in std_logic;
    
        -- Stream in
        s_axis_tdata  : in  std_logic_vector(31 downto 0);
        s_axis_tvalid : in  std_logic;
        s_axis_tready : out std_logic;
        s_axis_tlast  : in  std_logic;
    
        -- Stream out
        m_axis_tdata  : out std_logic_vector(63 downto 0);
        m_axis_tvalid : out std_logic;
        m_axis_tready : in  std_logic;
        m_axis_tlast  : out std_logic
        );
end component;

signal aresetn, s_axis_tvalid, s_axis_tready, s_axis_tlast, m_axis_tvalid, m_axis_tready, m_axis_tlast    : std_logic;
signal s_axis_tdata  :  std_logic_vector(31 downto 0);
signal m_axis_tdata  :  std_logic_vector(63 downto 0);

signal clk : std_logic;
    constant clk_period : time := 1 ns;
begin
--CLOCK SIGNAL--------
process
begin
    clk <= '0';
    wait for clk_period/2;  --for 0.5 ns signal is '0'.
    clk <= '1';
    wait for clk_period/2;  --for next 0.5 ns signal is '1'.
 end process;
----------------------

uut: dot_product_top port map(
            clk             =>      clk, 
            aresetn         =>      aresetn, 
            s_axis_tdata    =>      s_axis_tdata, 
            s_axis_tvalid   =>      s_axis_tvalid, 
            s_axis_tlast    =>      s_axis_tlast,
            m_axis_tdata    =>      m_axis_tdata,
            m_axis_tready   =>      m_axis_tready,
            m_axis_tlast    =>      m_axis_tlast
            );





end Behavioral;
