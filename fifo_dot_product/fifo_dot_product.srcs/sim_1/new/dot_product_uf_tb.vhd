library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


entity dot_product_uf_tb is
--  Port ( );
end dot_product_uf_tb;



architecture Behavioral of dot_product_uf_tb is

component dot_product
    Generic ( REGSIZE: integer := 10;
            bit_width : integer := 32; 
            num_elements: integer := 10
            );
    Port ( 
        clk : in std_logic;
        reset_n : in std_logic;
        enable : in std_logic;
        data_in : in signed(31 downto 0);
        data_out : out signed(63 downto 0);
        res_rdy : out std_logic
        );
end component;

component clk
    Port ( clk : out STD_LOGIC);
end component;

signal res_rdy, clock, reset_n, enable : std_logic;
signal data_in : signed(31 downto 0);
signal data_out : signed(63 downto 0);

begin
uut: dot_product port map(clk => clock,reset_n => reset_n,enable => enable,data_in => data_in, data_out => data_out, res_rdy => res_rdy);
cl: clk port map(clk => clock);


process
begin
    reset_n <= '0';
    enable <= '0';
    data_in <= (others => '0');
    wait for 1 ns;
    reset_n <= '1';
    wait for 1 ns;
    enable <= '1';
    for i in 0 to 9 loop
       data_in <= data_in + "00000000000000000000000000000001";
       wait for 1 ns;
    end loop;
    enable <= '0';
    wait for 2 ns;
    enable <= '1';
    data_in <= x"0000000b";
    wait for 1 ns;
    for i in 0 to 58 loop
        data_in <= data_in + "00000000000000000000000000000001";
        wait for 1 ns;
    end loop;
    enable <= '0';
    wait for 400 ns;
end process;
end Behavioral;
