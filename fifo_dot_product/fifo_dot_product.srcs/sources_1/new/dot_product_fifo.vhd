library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

package dot_product_reg_small_pkg is
    type bus_array_small is array(natural range <>) of std_logic_vector(31 downto 0);
end package dot_product_reg_small_pkg;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

package dot_product_pkg is
    type bus_array is array(natural range <>) of std_logic_vector(63 downto 0);
end package dot_product_pkg;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.dot_product_pkg.all;
use work.dot_product_reg_small_pkg.all;

entity dot_product_fifo is
    Generic (
            bit_width : integer := 32; 
            num_elements_vec: integer := 10
            );
            
    Port ( 
        clk : in std_logic;
        reset : in std_logic;
        enable : in std_logic;
        valid : in std_logic; -- out
        data_in : in signed(31 downto 0);
        data_out : out signed(63 downto 0);
        res_rdy : out std_logic;
        out_last : out std_logic
        );
end dot_product_fifo;

architecture Behavioral of dot_product_fifo is

component fifo_generator_0
    PORT (
        clk : IN STD_LOGIC;
        srst : IN STD_LOGIC;
        din : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        wr_en : IN STD_LOGIC;
        rd_en : IN STD_LOGIC;
        dout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        full : OUT STD_LOGIC;
        empty : OUT STD_LOGIC
  );
end component;

component multiplicator
    Port ( 
    inA : in std_logic_vector(31 downto 0);
    inB : in std_logic_vector(31 downto 0);
    Res : out std_logic_vector(63 downto 0)
    );
end component;

component adder
     Generic(
       bit_width : integer := 64
   );
   
   Port ( 
       inA : in std_logic_vector(bit_width-1 downto 0);
       inB : in std_logic_vector(bit_width-1 downto 0);
       Res : out std_logic_vector(bit_width-1 downto 0)
   );
end component;

component mult_gen_0
    PORT (
        CLK : IN STD_LOGIC;
        A : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        B : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        P : OUT STD_LOGIC_VECTOR(63 DOWNTO 0)
      );
end component;

signal wr_en,rd_en, full, empty : std_logic_vector(5 downto 0);
signal intermediate_wire : bus_array_small(0 to 5);
signal intermediate_reg : bus_array(0 to 4);
signal res_wire : bus_array(0 to 4);
signal res_reg : bus_array(0 to 4);
signal add_wire : bus_array(0 to 4);
signal std_in : std_logic_vector(31 downto 0);
signal valid_flag : std_logic;

TYPE State_type IS (A, B, C, D, E, F,G, H, I, J); -- Define the states
SIGNAL State : State_Type;    -- Create a signal that uses 

begin

std_in <= std_logic_vector(data_in);

fifo: for I in 0 to 5 generate
    fifox: fifo_generator_0 port map(clk => clk, srst => reset, din => std_in, wr_en=>wr_en(I), rd_en=>rd_en(I), dout => intermediate_wire(I), full=>full(I), empty=>empty(I));
end generate fifo;

mult: for I in 0 to 4 generate
    multx: mult_gen_0 port map(CLK=>clk, A => intermediate_wire(0), B => intermediate_wire(I+1), P=>res_wire(I));
end generate mult;

add: for I in 0 to 4 generate
   addx: adder port map(inA=>intermediate_reg(I), inB=>res_reg(I), Res=>add_wire(I)); 
end generate add; 


process(clk, reset)
    variable counter : integer := 0;
begin
    if(reset = '1') then
        for i in 0 to 4 loop
            res_reg(i) <= (others => '0');
            intermediate_reg(i) <= (others => '0');
        end loop;
        --wr_en <= (others => '0');
        rd_en <= (others => '0');
        data_out <= (others => '0');
        res_rdy <= '0';
        out_last <= '0';
    elsif(rising_edge(clk))then
            case state is
                when A =>
                    for i in 0 to 4 loop
                        res_reg(i) <= (others => '0');
                        intermediate_reg(i) <= (others => '0');
                    end loop;
                    if(enable = '1' and valid = '1') then
                        wr_en(0) <= '1';
                        wr_en(5 downto 1) <= (others => '0');
                        state <= B;
                    end if;
                    counter := 0;
                when B =>
                    if(valid = '1') then
                        counter := counter +1; --fill fifo 1
                        wr_en(0) <= '1';
                        wr_en(5 downto 1) <= (others => '0');
                        state <= B;
                    else 
                        wr_en(5 downto 0) <= (others => '0');
                    end if;
                    if(counter >= num_elements_vec) then
                        state <= C;
                        wr_en(1) <= '1';
                        wr_en(5 downto 2) <= (others => '0');
                        wr_en(0) <= '0';
                        counter := 0;
                    end if;
                when C =>
                    if(valid = '1') then    
                        counter := counter + 1; --fill fifo 2
                        wr_en(1) <= '1';
                        wr_en(5 downto 2) <= (others => '0');
                        wr_en(0) <= '0';
                    else 
                        wr_en(5 downto 0) <= (others => '0');
                    end if;
                    if(counter >= num_elements_vec) then
                        state <= D;
                        wr_en(2) <= '1';
                        wr_en(5 downto 3) <= (others => '0');
                        wr_en(1 downto 0) <= (others => '0');
                        counter := 0;
                    end if;
                when D =>
                    if(valid = '1') then
                        counter := counter + 1; --fill fifo 3
                        wr_en(2) <= '1';
                        wr_en(5 downto 3) <= (others => '0');
                        wr_en(1 downto 0) <= (others => '0');
                    else 
                        wr_en(5 downto 0) <= (others => '0');
                    end if;
                    if(counter >= num_elements_vec) then
                        state <= E;
                        wr_en(3) <= '1';
                        wr_en(5 downto 4) <= (others => '0');
                        wr_en(2 downto 0) <= (others => '0');
                        counter := 0;
                    end if;
                when E =>
                    if(valid = '1') then
                        wr_en(3) <= '1';
                        wr_en(5 downto 4) <= (others => '0');
                        wr_en(2 downto 0) <= (others => '0');
                        counter := counter + 1; --fill fifo 4
                    else 
                        wr_en(5 downto 0) <= (others => '0');
                    end if;
                    if(counter >= num_elements_vec) then
                        state <= F;
                        wr_en(4) <= '1';
                        wr_en(5) <= '0';
                        wr_en(3 downto 0) <= (others => '0');
                        counter := 0;
                    end if;
                when F =>
                    if(valid = '1') then
                        wr_en(4) <= '1';
                        wr_en(5) <= '0';
                        wr_en(3 downto 0) <= (others => '0');
                        counter := counter + 1; --fill fifo 5
                    else 
                        wr_en(5 downto 0) <= (others => '0');
                    end if;
                    if(counter >= num_elements_vec) then
                        state <= G;
                        wr_en(5) <= '1';
                        wr_en(4 downto 0) <= (others => '0');
                        counter := 0;
                    end if;
                when G =>
                    if(valid = '1') then
                        counter := counter + 1; --fill fifo 6
                        wr_en(5) <= '1';
                        wr_en(4 downto 0) <= (others => '0');
                    else 
                        wr_en(5 downto 0) <= (others => '0');
                    end if;
                    if(counter >= num_elements_vec) then
                        state <= H;
                        wr_en(5 downto 0) <= (others => '0');
                        rd_en(5 downto 0) <= (others => '1');
                        counter := 0;
                    end if;
                when H =>
                    counter := counter + 1;
                    if(counter >= 6) then
                        for i in 0 to 4 loop
                            intermediate_reg(i) <= res_wire(i);
                        end loop;
                        State <= I;
                        counter := 0;
                    end if;
                when I =>
                    for i in 0 to 4 loop
                        intermediate_reg(i) <= res_wire(i);
                        res_reg(i) <= add_wire(i);
                    end loop;
                    counter := counter + 1;
                    if(counter >= num_elements_vec) then
                        state <= J;
                        counter := 0;
                        rd_en <= (others => '0');
                    end if;
                when J =>
                    if(counter = 0) then
                        res_rdy <= '1';
                        data_out <= signed(res_reg(0));
                    elsif(counter = 1) then
                        data_out <= signed(res_reg(1));
                    elsif(counter = 2) then
                        data_out <= signed(res_reg(2));
                    elsif(counter = 3) then
                        data_out <= signed(res_reg(3));
                    elsif(counter = 4) then
                        data_out <= signed(res_reg(4));
                        out_last <= '1';
                    elsif(counter >4) then
                        data_out <= (others => '0');
                        state <= A;
                        out_last <= '0';
                        res_rdy <= '0';
                    end if;
                    counter := counter + 1;     
            end case;
    end if;
end process;


end Behavioral;
