library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity clk is
    Port ( clk : out STD_LOGIC);
end clk;

architecture Behavioral of clk is
   constant clk_period : time := 1 ns;
begin
  process
  begin
     clk <= '0';
     wait for clk_period/2;  --for 0.5 ns signal is '0'.
     clk <= '1';
     wait for clk_period/2;  --for next 0.5 ns signal is '1'.
  end process;
end Behavioral;