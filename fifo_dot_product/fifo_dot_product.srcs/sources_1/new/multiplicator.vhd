library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity multiplicator is
  Port ( 
        inA : in std_logic_vector(31 downto 0);
        inB : in std_logic_vector(31 downto 0);
        Res : out std_logic_vector(63 downto 0)
  );
end multiplicator;

architecture Behavioral of multiplicator is

begin

Res <= std_logic_vector(signed(inA) * signed(inB));

end Behavioral;
