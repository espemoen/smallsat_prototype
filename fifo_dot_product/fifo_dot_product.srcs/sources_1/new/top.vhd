library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity dot_product_top is
  port (
    clk     : in std_logic;
    aresetn : in std_logic;

    -- Stream in
    s_axis_tdata  : in  std_logic_vector(31 downto 0);
    s_axis_tvalid : in  std_logic;
    s_axis_tready : out std_logic;
    s_axis_tlast  : in  std_logic;

    -- Stream out
    m_axis_tdata  : out std_logic_vector(63 downto 0);
    m_axis_tvalid : out std_logic;
    m_axis_tready : in  std_logic;
    m_axis_tlast  : out std_logic
    );
end dot_product_top;



architecture rtl of dot_product_top is

  signal in_data  : signed(31 downto 0);
  signal in_valid : std_logic;
  signal in_ready : std_logic;
  signal in_last  : std_logic;

  signal out_data  : signed(63 downto 0);
  signal out_valid : std_logic;
  signal out_ready : std_logic;
  signal out_last  : std_logic;
  
  signal enable     : std_logic;
  

  -- Helper signals
  signal in_handshake  : std_logic;
  signal out_handshake : std_logic;
  
  signal reset : std_logic;
  signal last_reg : std_logic; 
 
begin

  in_data       <= signed(s_axis_tdata);
  in_valid      <= s_axis_tvalid;
  in_last       <= s_axis_tlast;
  s_axis_tready <= in_ready;

  m_axis_tdata  <= std_logic_vector(out_data);
  m_axis_tvalid <= out_valid;
  m_axis_tlast  <= out_last;
  out_ready     <= m_axis_tready;
    
  reset <= not aresetn;
  
dot_product : entity work.dot_product_fifo
        Generic map (
                bit_width => 32, 
                num_elements_vec => 100
                )
        Port map ( 
            clk => clk,
            reset => reset,
            enable => enable,
            valid => in_valid,
            data_in => in_data,
            data_out => out_data,
            res_rdy =>out_valid,
            out_last => out_last
        );



  process (clk)
  begin
    if (rising_edge(clk)) then
      if (aresetn = '0') then
        out_data  <= (others => '0');
        out_valid <= '0';
        out_last  <= '0';
        last_reg <= '1';
      else
        if (in_handshake = '1') then
            last_reg <= in_last;
        elsif (out_handshake = '1') then
          out_valid <= '0';
          out_last  <= '0';
        end if;
      end if;
    end if;
  end process;

  in_handshake  <= in_valid and in_ready; -- should include reset
  out_handshake <= out_valid and out_ready; -- should include reset
  in_ready      <= '1' when out_valid = '0' or out_handshake = '1' else '0';
  enable        <= last_reg and in_handshake;

end rtl;
