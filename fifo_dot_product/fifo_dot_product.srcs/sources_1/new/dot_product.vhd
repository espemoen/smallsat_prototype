library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

package dot_product_reg_small_pkg is
    type bus_array_small is array(natural range <>) of std_logic_vector(63 downto 0);
end package dot_product_reg_small_pkg;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

package dot_product_pkg is
    type bus_array is array(natural range <>) of std_logic_vector(319 downto 0);
end package dot_product_pkg;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.dot_product_pkg.all;
use work.dot_product_reg_small_pkg.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity dot_product is
  Generic ( REGSIZE: integer := 10;
            bit_width : integer := 32; 
            num_elements: integer := 10
            );
            
    Port ( 
        clk : in std_logic;
        reset_n : in std_logic;
        enable : in std_logic;
        data_in : in signed(31 downto 0);
        data_out : out signed(63 downto 0);
        res_rdy : out std_logic
        );
end dot_product;

architecture Behavioral of dot_product is

component multiplicator
    Port ( 
    inA : in std_logic_vector(31 downto 0);
    inB : in std_logic_vector(31 downto 0);
    Res : out std_logic_vector(63 downto 0)
    );
end component;

component adder
     Generic(
       bit_width : integer := 64
   );
   
   Port ( 
       inA : in std_logic_vector(63 downto 0);
       inB : in std_logic_vector(63 downto 0);
       Res : out std_logic_vector(63 downto 0)
   );
end component;

signal vecx_reg : bus_array(0 to 5);
signal intermediate_reg : bus_array_small(0 to 4);
signal intermediate_wire : bus_array_small(0 to 4);
signal res_reg : bus_array_small(0 to 4);
signal res_wire : bus_array_small(0 to 4);

TYPE State_type IS (A, B, C, D, E, F, G, H, I);  -- Define the states
SIGNAL State : State_Type;    -- Create a signal that uses 

begin

mult: for I in 0 to 4 generate
    multx: multiplicator port map(inA => vecx_reg(0)((REGSIZE*bit_width)-1 downto ((REGSIZE*bit_width)-32)), inB => vecx_reg(I+1)((REGSIZE*bit_width)-1 downto ((REGSIZE*bit_width)-32)), Res =>intermediate_wire(I));
    add: adder port map(inA => intermediate_reg(I), inB => res_reg(I), Res=>res_wire(I));
    end generate mult;
    
    
process(clk, reset_n)
    variable counter : integer := 0;
begin
    if(reset_n = '0') then
        for i in 0 to 5 loop
            vecx_reg(i) <= (others =>'0');
        end loop;
        for i in 0 to 4 loop
            intermediate_reg(i) <= (others => '0');
            res_reg(i) <= (others => '0');
        end loop;
        data_out <= (others => '0');
        res_rdy <= '0';
        State <= A;
    elsif(rising_edge(clk)) then
        CASE State IS
        when A => --Fill vector 1
            if(enable = '1') then
                vecx_reg(0) <= std_logic_vector(signed(vecx_reg(0)(REGSIZE*bit_width-33 downto 0)) & signed(data_in));
                counter := counter + 1;
                if(counter >= num_elements) then
                    State <= B;
                    counter := 0;
                end if;
            else 
                for i in 0 to 5 loop
                    vecx_reg(i) <= (others =>'0');
                end loop;
                for i in 0 to 4 loop
                    intermediate_reg(i) <= (others => '0');
                    res_reg(i) <= (others => '0');
                end loop;
                counter := 0;
            end if;
        when B =>  --Fill vector 2
            if(enable = '1') then
                vecx_reg(1) <= std_logic_vector(signed(vecx_reg(1)(REGSIZE*bit_width-33 downto 0)) & signed(data_in));
                counter := counter + 1;
                if(counter >= num_elements) then
                    State <= C;
                    counter := 0;
                end if;
            end if;
        when C =>  --Fill vector 3
            if(enable = '1') then
                vecx_reg(2) <= std_logic_vector(signed(vecx_reg(2)(REGSIZE*bit_width-33 downto 0)) & signed(data_in));
                counter := counter + 1;
                if(counter >= num_elements) then
                    State <= D;
                    counter := 0;
                end if;
            end if;
        when D =>  --Fill vector 4
            if(enable = '1') then
                vecx_reg(3) <= std_logic_vector(signed(vecx_reg(3)(REGSIZE*bit_width-33 downto 0)) & signed(data_in));
                counter := counter + 1;
                if(counter >= num_elements) then
                    State <= E;
                    counter := 0;
                end if;
            end if;
        when E =>  -- Fill vector 5
            if(enable = '1') then
                vecx_reg(4) <= std_logic_vector(signed(vecx_reg(4)(REGSIZE*bit_width-33 downto 0)) & signed(data_in));
                counter := counter + 1;
                if(counter >= num_elements) then
                    State <= F;
                    counter := 0;
                end if;
            end if;
        when F =>  -- Fill vector 6
            if(enable = '1') then
                vecx_reg(5) <= std_logic_vector(signed(vecx_reg(5)(REGSIZE*bit_width-33 downto 0)) & signed(data_in));
                counter := counter + 1;
                if(counter >= num_elements) then
                    State <= G;
                    counter := 0;
                end if;
            end if;
            
        when G =>  --Sample Multiplication
            for i in 0 to 5 loop
                vecx_reg(i) <= vecx_reg(i)(REGSIZE*bit_width-33 downto 0) & "00000000000000000000000000000000";
            end loop;
            for i in 0 to 4 loop
                intermediate_reg(i) <= intermediate_wire(i);
            end loop;
            State <= H;
        when H =>  --Initiate adder in pipeline
            for i in 0 to 5 loop
                vecx_reg(i) <= vecx_reg(i)(REGSIZE*bit_width-33 downto 0) & "00000000000000000000000000000000";
            end loop;
            for i in 0 to 4 loop
                intermediate_reg(i) <= intermediate_wire(i);
                res_reg(I) <= res_wire(I);
            end loop;
            counter := counter + 1;
            if(counter >= num_elements) then
                State <= I;
                Counter := 0;
                res_rdy <= '1';
            end if;
        when I =>  --Output Result
            if(counter = 0) then
                data_out <= signed(res_reg(0));
            elsif(counter = 1) then
                data_out <= signed(res_reg(1));
            elsif(counter = 2) then
                data_out <= signed(res_reg(2));
            elsif(counter = 3) then
                data_out <= signed(res_reg(3));
            elsif(counter = 4) then
                data_out <= signed(res_reg(4));
            else
                state <= A;
                res_rdy <= '0';
            end if;
            Counter := counter + 1;
        end CASE; 
    end if;

end process;



end Behavioral;
