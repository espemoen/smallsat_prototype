library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity axi_gearbox is
      Generic(
        B_RAM_SIZE      : integer := 100;
        B_RAM_BIT_WIDTH : integer := 32;
        NUM_B_RAM       : integer := 8;
        RAW_BIT_WIDTH : positive := 16;
        G_BIT_WIDTH   : positive := 32;
        P_BIT_WIDTH   : positive := 48;
        C_S_AXI_DATA_WIDTH : integer := 32;
        C_S_AXI_ADDR_WIDTH : integer := 6
     );  
    Port ( 
        clk     :   in  std_logic;
        aresetn :   in  std_logic;
        p_out   :   in  std_logic_vector(P_BIT_WIDTH*NUM_B_RAM-1 downto 0);
        p_rdy   :   in  std_logic;
        last_p  :   in  std_logic;
        
        m_axis_tdata  : out std_logic_vector(63 downto 0);
        --EMSC is ready to send to DMA.
        m_axis_tvalid : out std_logic;
        --DMA is ready to receive data
        m_axis_tready : in  std_logic;
        --Tell DMA this is last data
        m_axis_tlast  : out std_logic
    );
end axi_gearbox;

architecture Behavioral of axi_gearbox is
signal res_mem : std_logic_vector(P_BIT_WIDTH*NUM_B_RAM-1 downto 0);
signal start : std_logic;
signal t_valid_flag : std_logic;
signal valid_last : std_logic;
--signal out_handshake : std_logic;
begin


process(clk,aresetn)
    variable counter : integer range 0 to 50 := 0;
begin
    if(aresetn = '0') then
        m_axis_tdata  <= (others => '0');
        m_axis_tvalid <= '0';
        t_valid_flag <= '0';
        counter := 0;
    elsif(rising_edge(clk)) then
        if(p_rdy = '1') then
            res_mem <= p_out;
            start <= '1';
        elsif(start = '1') then
            if(counter >= NUM_B_RAM) then
                m_axis_tvalid <= '0';
                t_valid_flag <= '0';
                counter := 0;
                start <= '0';
                m_axis_tdata <= (others => '0');
            else
                if(to_integer(signed(res_mem((P_BIT_WIDTH*counter + P_BIT_WIDTH-1) downto (P_BIT_WIDTH*counter)))) < 0)then
                    m_axis_tdata <="1111111111111111"&res_mem((P_BIT_WIDTH*counter + P_BIT_WIDTH-1) downto (P_BIT_WIDTH*counter));
                else
                    m_axis_tdata <="0000000000000000"&res_mem((P_BIT_WIDTH*counter + P_BIT_WIDTH-1) downto (P_BIT_WIDTH*counter));
                end if;
                --m_axis_tdata <=res_mem((P_BIT_WIDTH*counter + P_BIT_WIDTH-1) downto (P_BIT_WIDTH*counter));
                m_axis_tvalid <= '1';
                t_valid_flag <= '1';
            end if;
            if(m_axis_tready = '1' and m_axis_tready = '1') then
                counter := counter + 1;
            end if;
        else
            counter := 0;
        end if;
        
    end if;
end process;


process(clk, aresetn)
    variable last_flag : std_logic;
    variable counter : integer range 0 to 50;
begin
    if(aresetn = '0') then
        last_flag := '0';
        counter := 0;
        m_axis_tlast <= '0';
        valid_last <= '0';
    elsif(rising_edge(clk)) then
        m_axis_tlast <= '0';
        valid_last <= t_valid_flag;
        if(last_p = '1') then
            last_flag := '1';
        elsif(last_flag = '1' and t_valid_flag = '1') then
            counter := counter + 1;
            if(counter >= NUM_B_RAM-1) then
                m_axis_tlast <= '1';
                counter := 0;
                last_flag := '0';
            end if;
        end if;
    end if;
end process;

--out_handshake <= '1' when m_axis_tready = '1' and valid_last  and t_valid_flag = '0' else '0';

end Behavioral;
