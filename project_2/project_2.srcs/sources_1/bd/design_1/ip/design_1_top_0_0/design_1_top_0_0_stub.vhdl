-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
-- Date        : Sat Apr 21 14:51:10 2018
-- Host        : DESKTOP-0KA73P0 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               d:/MasterOppgave/smallsat_prototype/project_2/project_2.srcs/sources_1/bd/design_1/ip/design_1_top_0_0/design_1_top_0_0_stub.vhdl
-- Design      : design_1_top_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_top_0_0 is
  Port ( 
    clk : in STD_LOGIC;
    aresetn : in STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axis_tvalid : in STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    s_axis_tlast : in STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axis_tvalid : out STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    m_axis_tlast : out STD_LOGIC;
    s_axi_ctrl_status_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_ctrl_status_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_ctrl_status_awvalid : in STD_LOGIC;
    s_axi_ctrl_status_awready : out STD_LOGIC;
    s_axi_ctrl_status_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_ctrl_status_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_ctrl_status_wvalid : in STD_LOGIC;
    s_axi_ctrl_status_wready : out STD_LOGIC;
    s_axi_ctrl_status_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_ctrl_status_bvalid : out STD_LOGIC;
    s_axi_ctrl_status_bready : in STD_LOGIC;
    s_axi_ctrl_status_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_ctrl_status_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_ctrl_status_arvalid : in STD_LOGIC;
    s_axi_ctrl_status_arready : out STD_LOGIC;
    s_axi_ctrl_status_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_ctrl_status_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_ctrl_status_rvalid : out STD_LOGIC;
    s_axi_ctrl_status_rready : in STD_LOGIC
  );

end design_1_top_0_0;

architecture stub of design_1_top_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,aresetn,s_axis_tdata[15:0],s_axis_tvalid,s_axis_tready,s_axis_tlast,m_axis_tdata[63:0],m_axis_tvalid,m_axis_tready,m_axis_tlast,s_axi_ctrl_status_awaddr[5:0],s_axi_ctrl_status_awprot[2:0],s_axi_ctrl_status_awvalid,s_axi_ctrl_status_awready,s_axi_ctrl_status_wdata[31:0],s_axi_ctrl_status_wstrb[3:0],s_axi_ctrl_status_wvalid,s_axi_ctrl_status_wready,s_axi_ctrl_status_bresp[1:0],s_axi_ctrl_status_bvalid,s_axi_ctrl_status_bready,s_axi_ctrl_status_araddr[5:0],s_axi_ctrl_status_arprot[2:0],s_axi_ctrl_status_arvalid,s_axi_ctrl_status_arready,s_axi_ctrl_status_rdata[31:0],s_axi_ctrl_status_rresp[1:0],s_axi_ctrl_status_rvalid,s_axi_ctrl_status_rready";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "top,Vivado 2017.4";
begin
end;
