/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include <stdlib.h>     /* atof */
#include "platform.h"
#include "xil_printf.h"


int main()
{
	/*
	int * mem_ptr = (uint32_t*)0x10000000;
	int * in_G = (uint32_t*)0x43C10004;
	int * init = (uint32_t*)0x43C10000;
	int a = mem_ptr[0];



	*init = 0x2034;


	for(int i = 0; i<416; i++){
		*in_G = mem_ptr[i];
	}

	*init = 0x34;


	*/
	/*
	int16_t * mem_ptr1 = (uint32_t*)0x10010000;



	int16_t a = mem_ptr1[0];
	int16_t b = mem_ptr1[1];
	int16_t c = mem_ptr1[519999];

	char nums[12];
	nums[0] = (b >> 12) & 0xF;
	nums[1] = (b >> 8) & 0xF;
	nums[2] = (b >> 4) & 0xF;
	nums[3] = b & 0xFF;

	nums[4] = (a >> 28) & 0xF;
	nums[5] = (a >> 24) & 0xF;
	nums[6] = (a >> 20) & 0xF;
	nums[7] = (a >> 16) & 0xF;
	nums[8] = (a >> 12) & 0xF;
	nums[9] = (a >> 8)  & 0xF;
	nums[10] =(a >> 4)  & 0xF;
	nums[11] = a & 0xF;

	int64_t u = 0;


	for(int i = 0; i<11; i++){
		u = u + (int)nums[i]*pow(16,11-i);
	}

	//double u = atof(bytes);
	*/

	int64_t * mem_ptr = (uint32_t*)0x10110000;
	int64_t P[8];

	for(int i = 0; i<10000; i++){
		P[0] = mem_ptr[0+i*8];
		P[1] = mem_ptr[1+i*8];
		P[2] = mem_ptr[2+i*8];
		P[3] = mem_ptr[3+i*8];
		P[4] = mem_ptr[4+i*8];
		P[5] = mem_ptr[5+i*8];
		P[6] = mem_ptr[6+i*8];
		P[7] = mem_ptr[7+i*8];
		//gs
	}

    return 0;

}
