library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity dot_product_module is
      Generic(
            RAW_BIT_WIDTH : positive := 12;
            G_BIT_WIDTH   : positive := 32;
            NUM_B_RAM     : positive := 5;
            P_BIT_WIDTH   : positive := 48            
      );
      Port ( 
            clk     :   in  std_logic;
            aresetn :   in  std_logic;
            en      :   in  std_logic;
            in_G    :   in  std_logic_vector(G_BIT_WIDTH*NUM_B_RAM-1 downto 0);
            in_raw  :   in  std_logic_vector(RAW_BIT_WIDTH-1 downto 0);
            v_len   :   in  std_logic_vector(11 downto 0);
            p_rdy   :   out std_logic_vector(NUM_B_RAM-1 downto 0);
            p_out   :   out std_logic_vector(NUM_B_RAM*P_BIT_WIDTH-1 downto 0)
      );
end dot_product_module;

architecture Behavioral of dot_product_module is

begin

dot_prod: for i in 0 to NUM_B_RAM-1 generate

dp: entity work.dot_product
        generic map(
            bit_depth_raw => RAW_BIT_WIDTH,
            bit_depth_G   => G_BIT_WIDTH
        )
        port map(
            clk         =>         	clk,
            en 		    => 			en,
            reset_n     =>         	aresetn,
            in_raw      =>        	in_raw,
            in_G        =>        	in_G(G_BIT_WIDTH*i + G_BIT_WIDTH-1 downto G_BIT_WIDTH*i),
            v_len		=>			v_len,
            p_rdy       =>          p_rdy(i),
            p           =>        	p_out(P_BIT_WIDTH*i + P_BIT_WIDTH-1 downto P_BIT_WIDTH*i)
        );
end generate dot_prod;

end Behavioral;
