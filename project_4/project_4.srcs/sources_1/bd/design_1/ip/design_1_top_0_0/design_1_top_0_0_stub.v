// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Sat May  5 07:59:14 2018
// Host        : DESKTOP-0KA73P0 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               D:/MasterOppgave/smallsat_prototype/project_4/project_4.srcs/sources_1/bd/design_1/ip/design_1_top_0_0/design_1_top_0_0_stub.v
// Design      : design_1_top_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "top,Vivado 2017.4.1" *)
module design_1_top_0_0(clk, aresetn, p_irq, s_axis_tdata, s_axis_tvalid, 
  s_axis_tready, s_axis_tlast, m_axis_tdata, m_axis_tvalid, m_axis_tready, m_axis_tlast, 
  s_axi_ctrl_status_awaddr, s_axi_ctrl_status_awprot, s_axi_ctrl_status_awvalid, 
  s_axi_ctrl_status_awready, s_axi_ctrl_status_wdata, s_axi_ctrl_status_wstrb, 
  s_axi_ctrl_status_wvalid, s_axi_ctrl_status_wready, s_axi_ctrl_status_bresp, 
  s_axi_ctrl_status_bvalid, s_axi_ctrl_status_bready, s_axi_ctrl_status_araddr, 
  s_axi_ctrl_status_arprot, s_axi_ctrl_status_arvalid, s_axi_ctrl_status_arready, 
  s_axi_ctrl_status_rdata, s_axi_ctrl_status_rresp, s_axi_ctrl_status_rvalid, 
  s_axi_ctrl_status_rready)
/* synthesis syn_black_box black_box_pad_pin="clk,aresetn,p_irq,s_axis_tdata[15:0],s_axis_tvalid,s_axis_tready,s_axis_tlast,m_axis_tdata[63:0],m_axis_tvalid,m_axis_tready,m_axis_tlast,s_axi_ctrl_status_awaddr[5:0],s_axi_ctrl_status_awprot[2:0],s_axi_ctrl_status_awvalid,s_axi_ctrl_status_awready,s_axi_ctrl_status_wdata[31:0],s_axi_ctrl_status_wstrb[3:0],s_axi_ctrl_status_wvalid,s_axi_ctrl_status_wready,s_axi_ctrl_status_bresp[1:0],s_axi_ctrl_status_bvalid,s_axi_ctrl_status_bready,s_axi_ctrl_status_araddr[5:0],s_axi_ctrl_status_arprot[2:0],s_axi_ctrl_status_arvalid,s_axi_ctrl_status_arready,s_axi_ctrl_status_rdata[31:0],s_axi_ctrl_status_rresp[1:0],s_axi_ctrl_status_rvalid,s_axi_ctrl_status_rready" */;
  input clk;
  input aresetn;
  output p_irq;
  input [15:0]s_axis_tdata;
  input s_axis_tvalid;
  output s_axis_tready;
  input s_axis_tlast;
  output [63:0]m_axis_tdata;
  output m_axis_tvalid;
  input m_axis_tready;
  output m_axis_tlast;
  input [5:0]s_axi_ctrl_status_awaddr;
  input [2:0]s_axi_ctrl_status_awprot;
  input s_axi_ctrl_status_awvalid;
  output s_axi_ctrl_status_awready;
  input [31:0]s_axi_ctrl_status_wdata;
  input [3:0]s_axi_ctrl_status_wstrb;
  input s_axi_ctrl_status_wvalid;
  output s_axi_ctrl_status_wready;
  output [1:0]s_axi_ctrl_status_bresp;
  output s_axi_ctrl_status_bvalid;
  input s_axi_ctrl_status_bready;
  input [5:0]s_axi_ctrl_status_araddr;
  input [2:0]s_axi_ctrl_status_arprot;
  input s_axi_ctrl_status_arvalid;
  output s_axi_ctrl_status_arready;
  output [31:0]s_axi_ctrl_status_rdata;
  output [1:0]s_axi_ctrl_status_rresp;
  output s_axi_ctrl_status_rvalid;
  input s_axi_ctrl_status_rready;
endmodule
