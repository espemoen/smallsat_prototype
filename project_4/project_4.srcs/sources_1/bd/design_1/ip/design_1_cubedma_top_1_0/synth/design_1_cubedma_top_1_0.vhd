-- (c) Copyright 1995-2018 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: xilinx.com:user:cubedma_top:1.0
-- IP Revision: 4

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY design_1_cubedma_top_1_0 IS
  PORT (
    clk : IN STD_LOGIC;
    aresetn : IN STD_LOGIC;
    m_axi_mem_araddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_axi_mem_arlen : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axi_mem_arsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axi_mem_arburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_mem_arprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axi_mem_arcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axi_mem_arvalid : OUT STD_LOGIC;
    m_axi_mem_arready : IN STD_LOGIC;
    m_axi_mem_rdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axi_mem_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_mem_rlast : IN STD_LOGIC;
    m_axi_mem_rvalid : IN STD_LOGIC;
    m_axi_mem_rready : OUT STD_LOGIC;
    m_axi_mem_awaddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_axi_mem_awlen : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axi_mem_awsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axi_mem_awburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_mem_awprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axi_mem_awcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axi_mem_awvalid : OUT STD_LOGIC;
    m_axi_mem_awready : IN STD_LOGIC;
    m_axi_mem_wdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axi_mem_wstrb : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axi_mem_wlast : OUT STD_LOGIC;
    m_axi_mem_wvalid : OUT STD_LOGIC;
    m_axi_mem_wready : IN STD_LOGIC;
    m_axi_mem_bresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_mem_bvalid : IN STD_LOGIC;
    m_axi_mem_bready : OUT STD_LOGIC;
    m_axis_mm2s_tdata : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    m_axis_mm2s_tlast : OUT STD_LOGIC;
    m_axis_mm2s_tvalid : OUT STD_LOGIC;
    m_axis_mm2s_tready : IN STD_LOGIC;
    mm2s_ctrl : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axis_s2mm_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    s_axis_s2mm_tlast : IN STD_LOGIC;
    s_axis_s2mm_tvalid : IN STD_LOGIC;
    s_axis_s2mm_tready : OUT STD_LOGIC;
    s_axi_ctrl_status_awaddr : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    s_axi_ctrl_status_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_ctrl_status_awvalid : IN STD_LOGIC;
    s_axi_ctrl_status_awready : OUT STD_LOGIC;
    s_axi_ctrl_status_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_ctrl_status_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_ctrl_status_wvalid : IN STD_LOGIC;
    s_axi_ctrl_status_wready : OUT STD_LOGIC;
    s_axi_ctrl_status_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_ctrl_status_bvalid : OUT STD_LOGIC;
    s_axi_ctrl_status_bready : IN STD_LOGIC;
    s_axi_ctrl_status_araddr : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    s_axi_ctrl_status_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_ctrl_status_arvalid : IN STD_LOGIC;
    s_axi_ctrl_status_arready : OUT STD_LOGIC;
    s_axi_ctrl_status_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_ctrl_status_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_ctrl_status_rvalid : OUT STD_LOGIC;
    s_axi_ctrl_status_rready : IN STD_LOGIC;
    mm2s_irq : OUT STD_LOGIC;
    s2mm_irq : OUT STD_LOGIC
  );
END design_1_cubedma_top_1_0;

ARCHITECTURE design_1_cubedma_top_1_0_arch OF design_1_cubedma_top_1_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF design_1_cubedma_top_1_0_arch: ARCHITECTURE IS "yes";
  COMPONENT cubedma_top IS
    GENERIC (
      C_MM2S_AXIS_WIDTH : INTEGER;
      C_MM2S_COMP_WIDTH : INTEGER;
      C_MM2S_NUM_COMP : INTEGER;
      C_TINYMOVER : BOOLEAN;
      C_S2MM_AXIS_WIDTH : INTEGER;
      C_S2MM_COMP_WIDTH : INTEGER;
      C_S2MM_NUM_COMP : INTEGER
    );
    PORT (
      clk : IN STD_LOGIC;
      aresetn : IN STD_LOGIC;
      m_axi_mem_araddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      m_axi_mem_arlen : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      m_axi_mem_arsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      m_axi_mem_arburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      m_axi_mem_arprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      m_axi_mem_arcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      m_axi_mem_arvalid : OUT STD_LOGIC;
      m_axi_mem_arready : IN STD_LOGIC;
      m_axi_mem_rdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axi_mem_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
      m_axi_mem_rlast : IN STD_LOGIC;
      m_axi_mem_rvalid : IN STD_LOGIC;
      m_axi_mem_rready : OUT STD_LOGIC;
      m_axi_mem_awaddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      m_axi_mem_awlen : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      m_axi_mem_awsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      m_axi_mem_awburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      m_axi_mem_awprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      m_axi_mem_awcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      m_axi_mem_awvalid : OUT STD_LOGIC;
      m_axi_mem_awready : IN STD_LOGIC;
      m_axi_mem_wdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axi_mem_wstrb : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      m_axi_mem_wlast : OUT STD_LOGIC;
      m_axi_mem_wvalid : OUT STD_LOGIC;
      m_axi_mem_wready : IN STD_LOGIC;
      m_axi_mem_bresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
      m_axi_mem_bvalid : IN STD_LOGIC;
      m_axi_mem_bready : OUT STD_LOGIC;
      m_axis_mm2s_tdata : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      m_axis_mm2s_tlast : OUT STD_LOGIC;
      m_axis_mm2s_tvalid : OUT STD_LOGIC;
      m_axis_mm2s_tready : IN STD_LOGIC;
      mm2s_ctrl : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      s_axis_s2mm_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      s_axis_s2mm_tlast : IN STD_LOGIC;
      s_axis_s2mm_tvalid : IN STD_LOGIC;
      s_axis_s2mm_tready : OUT STD_LOGIC;
      s_axi_ctrl_status_awaddr : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
      s_axi_ctrl_status_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      s_axi_ctrl_status_awvalid : IN STD_LOGIC;
      s_axi_ctrl_status_awready : OUT STD_LOGIC;
      s_axi_ctrl_status_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_axi_ctrl_status_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      s_axi_ctrl_status_wvalid : IN STD_LOGIC;
      s_axi_ctrl_status_wready : OUT STD_LOGIC;
      s_axi_ctrl_status_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      s_axi_ctrl_status_bvalid : OUT STD_LOGIC;
      s_axi_ctrl_status_bready : IN STD_LOGIC;
      s_axi_ctrl_status_araddr : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
      s_axi_ctrl_status_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      s_axi_ctrl_status_arvalid : IN STD_LOGIC;
      s_axi_ctrl_status_arready : OUT STD_LOGIC;
      s_axi_ctrl_status_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_axi_ctrl_status_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      s_axi_ctrl_status_rvalid : OUT STD_LOGIC;
      s_axi_ctrl_status_rready : IN STD_LOGIC;
      mm2s_irq : OUT STD_LOGIC;
      s2mm_irq : OUT STD_LOGIC
    );
  END COMPONENT cubedma_top;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF design_1_cubedma_top_1_0_arch: ARCHITECTURE IS "cubedma_top,Vivado 2017.4";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF design_1_cubedma_top_1_0_arch : ARCHITECTURE IS "design_1_cubedma_top_1_0,cubedma_top,{}";
  ATTRIBUTE CORE_GENERATION_INFO : STRING;
  ATTRIBUTE CORE_GENERATION_INFO OF design_1_cubedma_top_1_0_arch: ARCHITECTURE IS "design_1_cubedma_top_1_0,cubedma_top,{x_ipProduct=Vivado 2017.4,x_ipVendor=xilinx.com,x_ipLibrary=user,x_ipName=cubedma_top,x_ipVersion=1.0,x_ipCoreRevision=4,x_ipLanguage=VHDL,x_ipSimLanguage=MIXED,C_MM2S_AXIS_WIDTH=16,C_MM2S_COMP_WIDTH=16,C_MM2S_NUM_COMP=1,C_TINYMOVER=false,C_S2MM_AXIS_WIDTH=64,C_S2MM_COMP_WIDTH=64,C_S2MM_NUM_COMP=1}";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF s2mm_irq: SIGNAL IS "XIL_INTERFACENAME s2mm_irq, SENSITIVITY LEVEL_HIGH, PortWidth 1";
  ATTRIBUTE X_INTERFACE_INFO OF s2mm_irq: SIGNAL IS "xilinx.com:signal:interrupt:1.0 s2mm_irq INTERRUPT";
  ATTRIBUTE X_INTERFACE_PARAMETER OF mm2s_irq: SIGNAL IS "XIL_INTERFACENAME mm2s_irq, SENSITIVITY LEVEL_HIGH, PortWidth 1";
  ATTRIBUTE X_INTERFACE_INFO OF mm2s_irq: SIGNAL IS "xilinx.com:signal:interrupt:1.0 mm2s_irq INTERRUPT";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_ctrl_status_rready: SIGNAL IS "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status RREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_ctrl_status_rvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status RVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_ctrl_status_rresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status RRESP";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_ctrl_status_rdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status RDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_ctrl_status_arready: SIGNAL IS "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status ARREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_ctrl_status_arvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status ARVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_ctrl_status_arprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status ARPROT";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_ctrl_status_araddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status ARADDR";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_ctrl_status_bready: SIGNAL IS "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status BREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_ctrl_status_bvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status BVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_ctrl_status_bresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status BRESP";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_ctrl_status_wready: SIGNAL IS "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status WREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_ctrl_status_wvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status WVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_ctrl_status_wstrb: SIGNAL IS "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status WSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_ctrl_status_wdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status WDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_ctrl_status_awready: SIGNAL IS "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status AWREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_ctrl_status_awvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status AWVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_ctrl_status_awprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status AWPROT";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axi_ctrl_status_awaddr: SIGNAL IS "XIL_INTERFACENAME s_axi_ctrl_status, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 1e+08, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_ctrl_status_awaddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status AWADDR";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_s2mm_tready: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_s2mm TREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_s2mm_tvalid: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_s2mm TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_s2mm_tlast: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_s2mm TLAST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_s2mm_tdata: SIGNAL IS "XIL_INTERFACENAME s_axis_s2mm, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_s2mm_tdata: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_s2mm TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_mm2s_tready: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_mm2s TREADY";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_mm2s_tvalid: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_mm2s TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_mm2s_tlast: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_mm2s TLAST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_mm2s_tdata: SIGNAL IS "XIL_INTERFACENAME m_axis_mm2s, TDATA_NUM_BYTES 2, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_mm2s_tdata: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_mm2s TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_bready: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem BREADY";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_bvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem BVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_bresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem BRESP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_wready: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem WREADY";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_wvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem WVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_wlast: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem WLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_wstrb: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem WSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_wdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem WDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_awready: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem AWREADY";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_awvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem AWVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_awcache: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem AWCACHE";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_awprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem AWPROT";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_awburst: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem AWBURST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_awsize: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem AWSIZE";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_awlen: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem AWLEN";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_awaddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem AWADDR";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_rready: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem RREADY";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_rvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem RVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_rlast: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem RLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_rresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem RRESP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_rdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem RDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_arready: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem ARREADY";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_arvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem ARVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_arcache: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem ARCACHE";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_arprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem ARPROT";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_arburst: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem ARBURST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_arsize: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem ARSIZE";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_arlen: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem ARLEN";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axi_mem_araddr: SIGNAL IS "XIL_INTERFACENAME m_axi_mem, DATA_WIDTH 64, PROTOCOL AXI3, FREQ_HZ 1e+08, ID_WIDTH 0, ADDR_WIDTH 32, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 1, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 1, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 16, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_mem_araddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 m_axi_mem ARADDR";
  ATTRIBUTE X_INTERFACE_PARAMETER OF aresetn: SIGNAL IS "XIL_INTERFACENAME aresetn, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF aresetn: SIGNAL IS "xilinx.com:signal:reset:1.0 aresetn RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF clk: SIGNAL IS "XIL_INTERFACENAME clk, ASSOCIATED_BUSIF m_axis_mm2s:s_axis_s2mm:m_axi_mem:s_axi_ctrl_status, ASSOCIATED_RESET aresetn, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0";
  ATTRIBUTE X_INTERFACE_INFO OF clk: SIGNAL IS "xilinx.com:signal:clock:1.0 clk CLK";
BEGIN
  U0 : cubedma_top
    GENERIC MAP (
      C_MM2S_AXIS_WIDTH => 16,
      C_MM2S_COMP_WIDTH => 16,
      C_MM2S_NUM_COMP => 1,
      C_TINYMOVER => false,
      C_S2MM_AXIS_WIDTH => 64,
      C_S2MM_COMP_WIDTH => 64,
      C_S2MM_NUM_COMP => 1
    )
    PORT MAP (
      clk => clk,
      aresetn => aresetn,
      m_axi_mem_araddr => m_axi_mem_araddr,
      m_axi_mem_arlen => m_axi_mem_arlen,
      m_axi_mem_arsize => m_axi_mem_arsize,
      m_axi_mem_arburst => m_axi_mem_arburst,
      m_axi_mem_arprot => m_axi_mem_arprot,
      m_axi_mem_arcache => m_axi_mem_arcache,
      m_axi_mem_arvalid => m_axi_mem_arvalid,
      m_axi_mem_arready => m_axi_mem_arready,
      m_axi_mem_rdata => m_axi_mem_rdata,
      m_axi_mem_rresp => m_axi_mem_rresp,
      m_axi_mem_rlast => m_axi_mem_rlast,
      m_axi_mem_rvalid => m_axi_mem_rvalid,
      m_axi_mem_rready => m_axi_mem_rready,
      m_axi_mem_awaddr => m_axi_mem_awaddr,
      m_axi_mem_awlen => m_axi_mem_awlen,
      m_axi_mem_awsize => m_axi_mem_awsize,
      m_axi_mem_awburst => m_axi_mem_awburst,
      m_axi_mem_awprot => m_axi_mem_awprot,
      m_axi_mem_awcache => m_axi_mem_awcache,
      m_axi_mem_awvalid => m_axi_mem_awvalid,
      m_axi_mem_awready => m_axi_mem_awready,
      m_axi_mem_wdata => m_axi_mem_wdata,
      m_axi_mem_wstrb => m_axi_mem_wstrb,
      m_axi_mem_wlast => m_axi_mem_wlast,
      m_axi_mem_wvalid => m_axi_mem_wvalid,
      m_axi_mem_wready => m_axi_mem_wready,
      m_axi_mem_bresp => m_axi_mem_bresp,
      m_axi_mem_bvalid => m_axi_mem_bvalid,
      m_axi_mem_bready => m_axi_mem_bready,
      m_axis_mm2s_tdata => m_axis_mm2s_tdata,
      m_axis_mm2s_tlast => m_axis_mm2s_tlast,
      m_axis_mm2s_tvalid => m_axis_mm2s_tvalid,
      m_axis_mm2s_tready => m_axis_mm2s_tready,
      mm2s_ctrl => mm2s_ctrl,
      s_axis_s2mm_tdata => s_axis_s2mm_tdata,
      s_axis_s2mm_tlast => s_axis_s2mm_tlast,
      s_axis_s2mm_tvalid => s_axis_s2mm_tvalid,
      s_axis_s2mm_tready => s_axis_s2mm_tready,
      s_axi_ctrl_status_awaddr => s_axi_ctrl_status_awaddr,
      s_axi_ctrl_status_awprot => s_axi_ctrl_status_awprot,
      s_axi_ctrl_status_awvalid => s_axi_ctrl_status_awvalid,
      s_axi_ctrl_status_awready => s_axi_ctrl_status_awready,
      s_axi_ctrl_status_wdata => s_axi_ctrl_status_wdata,
      s_axi_ctrl_status_wstrb => s_axi_ctrl_status_wstrb,
      s_axi_ctrl_status_wvalid => s_axi_ctrl_status_wvalid,
      s_axi_ctrl_status_wready => s_axi_ctrl_status_wready,
      s_axi_ctrl_status_bresp => s_axi_ctrl_status_bresp,
      s_axi_ctrl_status_bvalid => s_axi_ctrl_status_bvalid,
      s_axi_ctrl_status_bready => s_axi_ctrl_status_bready,
      s_axi_ctrl_status_araddr => s_axi_ctrl_status_araddr,
      s_axi_ctrl_status_arprot => s_axi_ctrl_status_arprot,
      s_axi_ctrl_status_arvalid => s_axi_ctrl_status_arvalid,
      s_axi_ctrl_status_arready => s_axi_ctrl_status_arready,
      s_axi_ctrl_status_rdata => s_axi_ctrl_status_rdata,
      s_axi_ctrl_status_rresp => s_axi_ctrl_status_rresp,
      s_axi_ctrl_status_rvalid => s_axi_ctrl_status_rvalid,
      s_axi_ctrl_status_rready => s_axi_ctrl_status_rready,
      mm2s_irq => mm2s_irq,
      s2mm_irq => s2mm_irq
    );
END design_1_cubedma_top_1_0_arch;
