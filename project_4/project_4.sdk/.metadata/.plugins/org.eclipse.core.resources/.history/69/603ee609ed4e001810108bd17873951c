/*
 * Empty C++ Application
 */

#include <stdio.h>
#include "xil_printf.h"
#include "Eigen/dense"

#include <stdio.h>
#include <stdlib.h>     /* atof */
#include <math.h>
#include <float.h>
#include "xparameters.h"
#include "xuartps.h"


//Interrupt
#include "xscugic.h"
#include "xil_exception.h"

//Axi Timer
#include "xtmrctr.h"

#include <string.h>


using Eigen::MatrixXd;


//Axi timer
#define TMRCTR_DEVICE_ID  XPAR_TMRCTR_0_DEVICE_ID
#define TIMER_COUNTER_0	 0
XTmrCtr TimerCounter;

//Interrupt
bool mm2s_complete;
bool s2mm_complete;
const u32 MM2S_INT = 61U;
const u32 S2MM_INT = 62U;
const u32 P_INT = 63U;
XScuGic_Config* scugic_config;
XScuGic scugic_inst;
int int_counter = 0;
u32* dma_regs = (u32*)0x43C00000;


#define UART_DEVICE_ID		XPAR_PS7_UART_1_DEVICE_ID
XUartPs Uart_Ps;

/*Function Prototypes ******************************/
void mean(double** ref_spectra, double* mean,  int nVars, int refOrder);
void EMSC(double ** ref_spectra,
		  double ** corrected, int nVars,
		  int nObs, int refOrder);
double ** initialize(int rows, int columns);
int init_timer(u16 DeviceId, u8 TmrCtrNumber);
u32 start_timer(u8 TmrCtrNumber);
u32 stop_timer(u8 TmrCtrNumber);

/**************************************************/

template <class MatT>
Eigen::Matrix<typename MatT::Scalar, MatT::ColsAtCompileTime, MatT::RowsAtCompileTime>
pseudoinverse(const MatT &mat, typename MatT::Scalar tolerance = typename MatT::Scalar{1e-4}) // choose appropriately
{
    typedef typename MatT::Scalar Scalar;
    auto svd = mat.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
    const auto &singularValues = svd.singularValues();
    Eigen::Matrix<Scalar, MatT::ColsAtCompileTime, MatT::RowsAtCompileTime> singularValuesInv(mat.cols(), mat.rows());
    singularValuesInv.setZero();
    for (unsigned int i = 0; i < singularValues.size(); ++i) {
        if (singularValues(i) > tolerance)
        {
            singularValuesInv(i, i) = Scalar{1} / singularValues(i);
        }
        else
        {
            singularValuesInv(i, i) = Scalar{0};
        }
    }
    return svd.matrixV() * singularValuesInv * svd.matrixU().adjoint();
}


double ** initialize(int rows, int columns) {
	double **temp;
	temp = (double **)malloc(rows * sizeof(double*));
	for (int row = 0; row < rows; row++) {
		temp[row] = (double*)malloc(columns * sizeof(double));
	}
	return temp;
}

void mean(double** ref_spectra, double* mean,  int nVars, int refOrder) {
	double sum = 0;
	for (int col = 0; col < nVars; col++) {
		for (int row = 0; row < refOrder; row++) {
			sum += ref_spectra[row][col];
		}
		mean[col] = sum / refOrder;
		sum = 0;
	}
}

static void dma_irq_handler(void* ref) {
    int instance = (int)ref;
    int status_reg;
    u32 mask = 0;

    if (instance == 0) {
        status_reg = 1;
        mm2s_complete = true;
    }
    else{
        status_reg = 9;
        s2mm_complete = true;
    }
    //else{
    //	int_counter = int_counter + 1;
    //}
    mask = dma_regs[status_reg];
    dma_regs[status_reg] = (1 << 5);
}

static void p_int_irq_handler(void* ref){
	int_counter += 1;
}

void EMSC(double** ref_spectra, double* mean_spectra, double** corrected, int nVars, int nObs, int refOrder) {
	int counter = 0;
	//---------------------DECLARATIONS---------------------
	MatrixXd M(refOrder + 4, nVars);
	//double ** G = initialize(nVars, refOrder+4);
	double* p = (double*)malloc((refOrder + 4) * sizeof(double));
	double num = 0;
	double multiplier = pow(2.0,17.0);
	double ** wlensQ = initialize(2, nVars);
	int **G;
    G = (int **)malloc(nVars * sizeof(int*));
	for (int row = 0; row < nVars; row++) {
		G[row] = (int*)malloc(refOrder+4 * sizeof(int));
	}




	//------------------------------------------------------
	xil_printf("Constructing M!\n");

	for (int i = 0; i < nVars; i++) {

		//Add 1 in first row
		M(0,i) = 1;

		//Add linspace and linspace squared
		M(1,i) = num;
		wlensQ[0][i] = num;

		M(2,i) = pow(num, 2);
		wlensQ[1][i] = pow(num,2);
		num += (1.0 / (nVars - 1));

		//Add reference spectra
		for (int y = 0; y < refOrder; y++) {
			M(y + 3,i) = ref_spectra[y][i];
		}

		//Add mean in last row
		M(refOrder+3,i) = mean_spectra[i];
	}


	//INITIALIZE BLOCK RAM
	//  - G is loaded into the b-ram
	//-------------------------------
	u32 * mem_ptr = (u32*)0x10000000;
	u32 * init = (u32*)0x43c10000;
	u32 * in_G = (u32*)0x43c10004;
	*init = 0x2034;
//	int * save_G = (int*)0x0F0BDBF0;
//	int a;
//	int a1;
//	int a2;
//	int a3;
//	int a4;
//	int a5;
//	int a6;
//	int a7;
//	int a8;
	//Execute pseudo-inverse of M
	MatrixXd M_M = M*M.transpose();
	MatrixXd p_inv = M.transpose() * M_M.completeOrthogonalDecomposition().pseudoInverse();
	//MatrixXf p_inv = pseudoinverse(M,1e-4);
	xil_printf("Pseudo-Inverse Completed!\n");
	for(int y = 0; y<refOrder+4; y++){
		for(int i = 0; i<nVars; i++){
			*in_G  =(int) floor(p_inv(i,y)* multiplier);
//			a = (int) floor(p_inv(i,0)* multiplier);
//			a1 = (int) floor(p_inv(i,1)* multiplier);
//			a2 = (int) floor(p_inv(i,2)* multiplier);
//			a3 = (int) floor(p_inv(i,3)* multiplier);
//			a4 = (int) floor(p_inv(i,4)* multiplier);
//			a5 = (int) floor(p_inv(i,5)* multiplier);
//			a6 = (int) floor(p_inv(i,6)* multiplier);
//			a7 = (int) floor(p_inv(i,7)* multiplier);
		}
	}
	//-------------------------------

	*init = 0x34;
	*init = 0x1034;
	print("Starting HW part.\n\r");

	u32* mm2s = (u32*)0x43c00000;
	u32* s2mm = (u32*)0x43c00020;

	*init = 0x1034;
	// Program S2MM DMA

	s2mm[0] = 0x0;
	s2mm[2] = 0x0F0BDBF0;
	s2mm[0] = (1 << 5) | 1;


	// Program MM2S DMA
	mm2s[0] = 0;
	mm2s[2] = 0x10010000;

	//mm2s[3] = 0x1001001; //liten kube
	//mm2s[5] = 520000; //liten kube

	mm2s[3] = 0x341F41F4; //stor kube
	mm2s[5] = 0x6590;//stor kube


	mm2s[0] = (1 << 8) | (1 << 5) | 1;

	/*while (!s2mm_complete || !mm2s_complete) {
	}*/

	int64_t * test_ptr = (int64_t*)0x0F0BDBF0;
	u16 * raw_ptr = (u16*)0x10010000;
	double p_st[8];
	u16 ah;

	xil_printf("corrected: %d\n", *corrected[0]);

	while(counter < nObs){
		if((int_counter - counter > 10) || (nObs - int_counter < 10)){
			for(int i = 0; i < 8; i++){
				p_st[i] = test_ptr[i+counter*8]/multiplier;
			}
			for(int cols = 0; cols < nVars; cols++){
				ah = raw_ptr[counter*52+cols];
				corrected[counter][cols] = (ah - (p_st[0] + p_st[1]*wlensQ[0][cols] + p_st[2]*wlensQ[1][cols]))/p_st[7];
			}
			counter++;

		}
	}


	//res1 = res1/pow(2,17);

	//--------------------------------------------------------------------------
	xil_printf("int_counter: %d\n", int_counter);
	print("Done\n");

	/*
	for(int i = 0; i< 10000000; i++){

	}
	*/
	xil_printf("int_counter: %d\n", int_counter);
	print("Done\n");
}




int init_interrupt_system(){

	 //Initialize Interrupt system
	//--------------------------------------------------------------------------

	int ret;


	scugic_config = XScuGic_LookupConfig(XPAR_PS7_SCUGIC_0_DEVICE_ID);
	if(NULL == scugic_config){
		return XST_FAILURE;
	}
	ret = XScuGic_CfgInitialize(&scugic_inst, scugic_config, scugic_config->CpuBaseAddress);
	if (ret != XST_SUCCESS) {
		print("Failed to initialize GIC\n");
		return ret;
	}



	u32 id_full = XScuGic_CPUReadReg(&scugic_inst, XSCUGIC_INT_ACK_OFFSET);
	XScuGic_CPUWriteReg(&scugic_inst, XSCUGIC_EOI_OFFSET, id_full);

	ret = XScuGic_SelfTest(&scugic_inst);
	if (ret != XST_SUCCESS) {
		return XST_FAILURE;
	}


	ret = XScuGic_Connect(&scugic_inst, MM2S_INT, (Xil_InterruptHandler)dma_irq_handler, (void*)0);
	   if (ret != XST_SUCCESS)
		   return ret;

   ret = XScuGic_Connect(&scugic_inst, S2MM_INT, (Xil_InterruptHandler)dma_irq_handler, (void*)1);
	   if (ret != XST_SUCCESS)
		   return ret;

	ret = XScuGic_Connect(&scugic_inst, P_INT, (Xil_InterruptHandler)p_int_irq_handler, (void*)2);
	if (ret != XST_SUCCESS){
			print("Failed to initialize GIC 3\n");
			return ret;
	}

	XScuGic_SetPriorityTriggerType(&scugic_inst, MM2S_INT, 0xA0, 0x3);
	XScuGic_SetPriorityTriggerType(&scugic_inst, S2MM_INT, 0xA0, 0x3);
	XScuGic_SetPriorityTriggerType(&scugic_inst, P_INT, 0xA0, 0x3);

	XScuGic_Enable(&scugic_inst, MM2S_INT);
	XScuGic_Enable(&scugic_inst, S2MM_INT);
	XScuGic_Enable(&scugic_inst, P_INT);


	Xil_ExceptionInit();
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
								(Xil_ExceptionHandler)XScuGic_InterruptHandler,
								 &scugic_inst);
	Xil_ExceptionEnable();

	//--------------------------------------------------------------------------
}

int init_timer(u16 DeviceId, u8 TmrCtrNumber){
	int Status;
		XTmrCtr *TmrCtrInstancePtr = &TimerCounter;
		/*
		 * Initialize the timer counter so that it's ready to use,
		 * specify the device ID that is generated in xparameters.h
		 */
		Status = XTmrCtr_Initialize(TmrCtrInstancePtr, DeviceId);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
		/*
		 * Perform a self-test to ensure that the hardware was built
		 * correctly, use the 1st timer in the device (0)
		 */
		Status = XTmrCtr_SelfTest(TmrCtrInstancePtr, TmrCtrNumber);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
		/*
		* Enable the Autoreload mode of the timer counters.
		*/
		return XST_SUCCESS;}

u32 start_timer(u8 TmrCtrNumber){
	XTmrCtr *TmrCtrInstancePtr = &TimerCounter;
	XTmrCtr_SetOptions(TmrCtrInstancePtr, TmrCtrNumber,
							XTC_AUTO_RELOAD_OPTION);
	u32 val = XTmrCtr_GetValue(TmrCtrInstancePtr, TmrCtrNumber);
	XTmrCtr_Start(TmrCtrInstancePtr, TmrCtrNumber);
	return val;}

u32 stop_timer(u8 TmrCtrNumber){
	XTmrCtr *TmrCtrInstancePtr = &TimerCounter;
	u32 val = XTmrCtr_GetValue(TmrCtrInstancePtr, TmrCtrNumber);
	XTmrCtr_SetOptions(TmrCtrInstancePtr, TmrCtrNumber, 0);
	return val;
}


int main(){

	init_interrupt_system();


	//Adding pointer to location of stored cube.
	float * mem_ptr = (float*)0x10010000;
	int nVars = 52; //number of wavelenghts
	int nObs  = 250000; //total number of pixels
	int refOrder = 4; //numbers of species in spectra


	double ** ref_spectra = initialize(refOrder, nVars);
	double ** corrected = initialize(nObs, nVars);
	double * mean_v =  (double*)malloc(nVars * sizeof(double));

	float a;
	//Fill raw matrix
	int index = 0;


	//Construct some reference spectra
	//Just using some spectras from raw in this case
	//as an example.
	index = 0;
	mem_ptr = (float*)0x10000000;
	for(int rows = 0; rows<refOrder; rows++){
		for(int cols = 0; cols < nVars; cols++){
			ref_spectra[rows][cols] = mem_ptr[index++];
			a = ref_spectra[rows][cols];
		}
	}

	mem_ptr = (float*)0x10001000;
	for(int i = 0; i<nVars; i++){
		mean_v[i] = mem_ptr[i];
		a = mean_v[i];
	}
	//calculate mean of ref_spectra
	//mean(ref_spectra, mean_v, nVars, refOrder);


	xil_printf("ESMC Starting!\n");

	//Start the EMSC

	u32 value1, value2;
	init_timer(TMRCTR_DEVICE_ID, TIMER_COUNTER_0);
	value1 = start_timer(TIMER_COUNTER_0);

	EMSC(ref_spectra, mean_v, corrected, nVars, nObs, refOrder);

	value2 = stop_timer(TIMER_COUNTER_0);
	printf("Transfer time: %d\n", value2-value1);


	//Point to location for storing data


	mem_ptr = (float*)0x13197508;
		    index = 0;
		    for(int i = 0; i<nObs; i++){
		    	for(int y = 0; y<nVars; y++){
		    		mem_ptr[index++] = (float)corrected[i][y];
		    	}
		    }


	xil_printf("Done");

	xil_printf("%d \n", sizeof(float));



	XScuGic_Disable(&scugic_inst, MM2S_INT);
	XScuGic_Disable(&scugic_inst, S2MM_INT);
	XScuGic_Disable(&scugic_inst, P_INT);

	XScuGic_Disconnect(&scugic_inst, MM2S_INT);
	XScuGic_Disconnect(&scugic_inst, S2MM_INT);
	XScuGic_Disconnect(&scugic_inst, P_INT);

	Xil_ExceptionDisable();



}
