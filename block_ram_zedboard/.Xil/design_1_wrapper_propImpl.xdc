set_property SRC_FILE_INFO {cfile:d:/user/My_Documents/Espen_Moen/prototype/block_ram_zedboard/block_ram_zedboard.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0/design_1_processing_system7_0_0.xdc rfile:../block_ram_zedboard.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0/design_1_processing_system7_0_0.xdc id:1 order:EARLY scoped_inst:design_1_i/processing_system7_0/inst} [current_design]
set_property SRC_FILE_INFO {cfile:D:/user/My_Documents/Espen_Moen/prototype/block_ram_zedboard/block_ram_zedboard.srcs/constrs_1/new/debug.xdc rfile:../block_ram_zedboard.srcs/constrs_1/new/debug.xdc id:2} [current_design]
set_property src_info {type:SCOPED_XDC file:1 line:21 export:INPUT save:INPUT read:READ} [current_design]
set_input_jitter clk_fpga_0 0.3
set_property src_info {type:XDC file:2 line:43 export:INPUT save:INPUT read:READ} [current_design]
create_debug_core u_ila_0 ila
set_property src_info {type:XDC file:2 line:44 export:INPUT save:INPUT read:READ} [current_design]
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
set_property src_info {type:XDC file:2 line:45 export:INPUT save:INPUT read:READ} [current_design]
set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_0]
set_property src_info {type:XDC file:2 line:46 export:INPUT save:INPUT read:READ} [current_design]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
set_property src_info {type:XDC file:2 line:47 export:INPUT save:INPUT read:READ} [current_design]
set_property C_DATA_DEPTH 1024 [get_debug_cores u_ila_0]
set_property src_info {type:XDC file:2 line:48 export:INPUT save:INPUT read:READ} [current_design]
set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_0]
set_property src_info {type:XDC file:2 line:49 export:INPUT save:INPUT read:READ} [current_design]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_0]
set_property src_info {type:XDC file:2 line:50 export:INPUT save:INPUT read:READ} [current_design]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
set_property src_info {type:XDC file:2 line:51 export:INPUT save:INPUT read:READ} [current_design]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
set_property src_info {type:XDC file:2 line:52 export:INPUT save:INPUT read:READ} [current_design]
set_property port_width 1 [get_debug_ports u_ila_0/clk]
set_property src_info {type:XDC file:2 line:53 export:INPUT save:INPUT read:READ} [current_design]
connect_debug_port u_ila_0/clk [get_nets [list design_1_i/processing_system7_0/inst/FCLK_CLK0]]
set_property src_info {type:XDC file:2 line:54 export:INPUT save:INPUT read:READ} [current_design]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
set_property src_info {type:XDC file:2 line:55 export:INPUT save:INPUT read:READ} [current_design]
set_property port_width 32 [get_debug_ports u_ila_0/probe0]
set_property src_info {type:XDC file:2 line:56 export:INPUT save:INPUT read:READ} [current_design]
connect_debug_port u_ila_0/probe0 [get_nets [list {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[0]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[1]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[2]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[3]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[4]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[5]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[6]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[7]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[8]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[9]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[10]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[11]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[12]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[13]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[14]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[15]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[16]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[17]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[18]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[19]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[20]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[21]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[22]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[23]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[24]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[25]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[26]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[27]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[28]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[29]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[30]} {design_1_i/ps7_0_axi_periph_M00_AXI_WDATA[31]}]]
set_property src_info {type:XDC file:2 line:57 export:INPUT save:INPUT read:READ} [current_design]
create_debug_port u_ila_0 probe
set_property src_info {type:XDC file:2 line:58 export:INPUT save:INPUT read:READ} [current_design]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe1]
set_property src_info {type:XDC file:2 line:59 export:INPUT save:INPUT read:READ} [current_design]
set_property port_width 6 [get_debug_ports u_ila_0/probe1]
set_property src_info {type:XDC file:2 line:60 export:INPUT save:INPUT read:READ} [current_design]
connect_debug_port u_ila_0/probe1 [get_nets [list {design_1_i/ps7_0_axi_periph_M00_AXI_AWADDR[0]} {design_1_i/ps7_0_axi_periph_M00_AXI_AWADDR[1]} {design_1_i/ps7_0_axi_periph_M00_AXI_AWADDR[2]} {design_1_i/ps7_0_axi_periph_M00_AXI_AWADDR[3]} {design_1_i/ps7_0_axi_periph_M00_AXI_AWADDR[4]} {design_1_i/ps7_0_axi_periph_M00_AXI_AWADDR[5]}]]
set_property src_info {type:XDC file:2 line:61 export:INPUT save:INPUT read:READ} [current_design]
create_debug_port u_ila_0 probe
set_property src_info {type:XDC file:2 line:62 export:INPUT save:INPUT read:READ} [current_design]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe2]
set_property src_info {type:XDC file:2 line:63 export:INPUT save:INPUT read:READ} [current_design]
set_property port_width 1 [get_debug_ports u_ila_0/probe2]
set_property src_info {type:XDC file:2 line:64 export:INPUT save:INPUT read:READ} [current_design]
connect_debug_port u_ila_0/probe2 [get_nets [list design_1_i/ps7_0_axi_periph_M00_AXI_AWREADY]]
set_property src_info {type:XDC file:2 line:65 export:INPUT save:INPUT read:READ} [current_design]
create_debug_port u_ila_0 probe
set_property src_info {type:XDC file:2 line:66 export:INPUT save:INPUT read:READ} [current_design]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe3]
set_property src_info {type:XDC file:2 line:67 export:INPUT save:INPUT read:READ} [current_design]
set_property port_width 1 [get_debug_ports u_ila_0/probe3]
set_property src_info {type:XDC file:2 line:68 export:INPUT save:INPUT read:READ} [current_design]
connect_debug_port u_ila_0/probe3 [get_nets [list design_1_i/ps7_0_axi_periph_M00_AXI_AWVALID]]
set_property src_info {type:XDC file:2 line:69 export:INPUT save:INPUT read:READ} [current_design]
create_debug_port u_ila_0 probe
set_property src_info {type:XDC file:2 line:70 export:INPUT save:INPUT read:READ} [current_design]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe4]
set_property src_info {type:XDC file:2 line:71 export:INPUT save:INPUT read:READ} [current_design]
set_property port_width 1 [get_debug_ports u_ila_0/probe4]
set_property src_info {type:XDC file:2 line:72 export:INPUT save:INPUT read:READ} [current_design]
connect_debug_port u_ila_0/probe4 [get_nets [list design_1_i/ps7_0_axi_periph_M00_AXI_WREADY]]
set_property src_info {type:XDC file:2 line:73 export:INPUT save:INPUT read:READ} [current_design]
create_debug_port u_ila_0 probe
set_property src_info {type:XDC file:2 line:74 export:INPUT save:INPUT read:READ} [current_design]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe5]
set_property src_info {type:XDC file:2 line:75 export:INPUT save:INPUT read:READ} [current_design]
set_property port_width 1 [get_debug_ports u_ila_0/probe5]
set_property src_info {type:XDC file:2 line:76 export:INPUT save:INPUT read:READ} [current_design]
connect_debug_port u_ila_0/probe5 [get_nets [list design_1_i/ps7_0_axi_periph_M00_AXI_WVALID]]
set_property src_info {type:XDC file:2 line:77 export:INPUT save:INPUT read:READ} [current_design]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property src_info {type:XDC file:2 line:78 export:INPUT save:INPUT read:READ} [current_design]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property src_info {type:XDC file:2 line:79 export:INPUT save:INPUT read:READ} [current_design]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
set_property src_info {type:XDC file:2 line:80 export:INPUT save:INPUT read:READ} [current_design]
connect_debug_port dbg_hub/clk [get_nets u_ila_0_FCLK_CLK0]
