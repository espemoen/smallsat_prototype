library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity block_ram_top is
  generic (
    C_S_AXI_DATA_WIDTH : integer := 32;
    C_S_AXI_ADDR_WIDTH : integer := 6
    );
  port (
    clk     : in std_logic;
    aresetn : in std_logic;

    debug_leds : out std_logic_vector(7 downto 0);

    -- Stream in
    s_axis_tdata  : in  std_logic_vector(31 downto 0);
    s_axis_tvalid : in  std_logic;
    s_axis_tready : out std_logic;
    s_axis_tlast  : in  std_logic;

    -- Stream out
    m_axis_tdata  : out std_logic_vector(31 downto 0);
    m_axis_tvalid : out std_logic;
    m_axis_tready : in  std_logic;
    m_axis_tlast  : out std_logic;

    -- Control interface
    s_axi_awaddr  : in  std_logic_vector(c_s_axi_addr_width-1 downto 0);
    s_axi_awprot  : in  std_logic_vector(2 downto 0);
    s_axi_awvalid : in  std_logic;
    s_axi_awready : out std_logic;
    s_axi_wdata   : in  std_logic_vector(c_s_axi_data_width-1 downto 0);
    s_axi_wstrb   : in  std_logic_vector((c_s_axi_data_width/8)-1 downto 0);
    s_axi_wvalid  : in  std_logic;
    s_axi_wready  : out std_logic;
    s_axi_bresp   : out std_logic_vector(1 downto 0);
    s_axi_bvalid  : out std_logic;
    s_axi_bready  : in  std_logic;
    s_axi_araddr  : in  std_logic_vector(c_s_axi_addr_width-1 downto 0);
    s_axi_arprot  : in  std_logic_vector(2 downto 0);
    s_axi_arvalid : in  std_logic;
    s_axi_arready : out std_logic;
    s_axi_rdata   : out std_logic_vector(c_s_axi_data_width-1 downto 0);
    s_axi_rresp   : out std_logic_vector(1 downto 0);
    s_axi_rvalid  : out std_logic;
    s_axi_rready  : in  std_logic
    );
end block_ram_top;

architecture rtl of block_ram_top is
  signal n     : std_logic_vector(15 downto 0);
  signal count : std_logic_vector(31 downto 0);

  signal in_data  : unsigned(31 downto 0);
  signal in_valid : std_logic;
  signal in_ready : std_logic;
  signal in_last  : std_logic;

  signal out_data  : unsigned(31 downto 0);
  signal out_valid : std_logic;
  signal out_ready : std_logic;
  signal out_last  : std_logic;

  type data_array is array(0 to 4) of std_logic_vector(31 downto 0);
  signal data_in         : data_array;
  signal data_out_wire   : data_array; 
  
  signal in_handshakes : std_logic_vector(4 downto 0);
  signal read_enable : std_logic_vector(4 downto 0);
  
  signal read_index : integer range 0 to 99;
  
  
  -- Helper signals
  signal in_handshake  : std_logic;
  signal out_handshake : std_logic;
begin

  in_data       <= unsigned(s_axis_tdata);
  in_valid      <= s_axis_tvalid;
  in_last       <= s_axis_tlast;
  s_axis_tready <= in_ready;

  m_axis_tdata  <= std_logic_vector(out_data);
  m_axis_tvalid <= out_valid;
  m_axis_tlast  <= out_last;
  out_ready     <= m_axis_tready;

  i_register_interface : entity work.register_interface
    generic map (
      C_S_AXI_DATA_WIDTH => C_S_AXI_DATA_WIDTH,
      C_S_AXI_ADDR_WIDTH => C_S_AXI_ADDR_WIDTH)
    port map (
      data1_in      =>  data_in(0),
      data2_in      =>  data_in(1),
      data3_in      =>  data_in(2),
      data4_in      =>  data_in(3),
      data5_in      =>  data_in(4),
      in_handshakes => in_handshakes,
      read_enable   => read_enable,
      count         => count,
      S_AXI_ACLK    => CLK,
      S_AXI_ARESETN => ARESETN,
      S_AXI_AWADDR  => S_AXI_AWADDR,
      S_AXI_AWPROT  => S_AXI_AWPROT,
      S_AXI_AWVALID => S_AXI_AWVALID,
      S_AXI_AWREADY => S_AXI_AWREADY,
      S_AXI_WDATA   => S_AXI_WDATA,
      S_AXI_WSTRB   => S_AXI_WSTRB,
      S_AXI_WVALID  => S_AXI_WVALID,
      S_AXI_WREADY  => S_AXI_WREADY,
      S_AXI_BRESP   => S_AXI_BRESP,
      S_AXI_BVALID  => S_AXI_BVALID,
      S_AXI_BREADY  => S_AXI_BREADY,
      S_AXI_ARADDR  => S_AXI_ARADDR,
      S_AXI_ARPROT  => S_AXI_ARPROT,
      S_AXI_ARVALID => S_AXI_ARVALID,
      S_AXI_ARREADY => S_AXI_ARREADY,
      S_AXI_RDATA   => S_AXI_RDATA,
      S_AXI_RRESP   => S_AXI_RRESP,
      S_AXI_RVALID  => S_AXI_RVALID,
      S_AXI_RREADY  => S_AXI_RREADY);

--b_ramx: for i in 0 to 4 generate
--    signal count_i : integer range 0 to 99;
--    signal b_ram_data : bus_array;
--begin
--process(clk)
--begin
--    if(rising_edge(clk)) then
--        if(in_handshakes(i) = '1') then
--            b_ram_data(count_i) <= data_in(i);
--        end if;
--    end if;
--end process;


--process(clk)
--begin
--    if(rising_edge(clk)) then
--        if(read_enable(i) = '1' and i = 0) then
--            out_data <= unsigned(b_ram_data(read_index));
--        end if;
--    end if;
    
--end process;

--process(clk)
--begin
--    if(rising_edge(clk)) then
--        if(aresetn = '0') then
--            count_i <= 0;
--        elsif(in_handshakes(i) = '1') then
--            count_i <= count_i + 1;
--        end if;
--    end if;
--end process;

--b_rams_data(i) <= b_ram_data;

--end generate b_ramx;

b_ramx: for i in 0 to 4 generate
    b_ramx : entity work.block_ram
    port map (
        clk             =>      clk,
        aresetn         =>      aresetn,
        data_in         =>      data_in(i),
        write_enable    =>      in_handshakes(i),
        read_enable     =>      read_enable(i),
        read_address    =>      read_index,
        data_out        =>      data_out_wire(i)
    );
        
end generate b_ramx;

process(clk)
begin
    case read_enable is
        when "00001" =>
            out_data <= unsigned(data_out_wire(0));
        when "00010" =>
            out_data <= unsigned(data_out_wire(1));
        when "00100" =>
            out_data <= unsigned(data_out_wire(2));
        when "01000" =>
            out_data <= unsigned(data_out_wire(3));
        when "10000" =>
            out_data <= unsigned(data_out_wire(4));
        when others =>
            out_data <= (others => '0');
    end case;
end process;


  read_index <= to_integer(in_data);
  in_handshake  <= in_valid and in_ready; -- should include reset
  out_handshake <= out_valid and out_ready; -- should include reset
  in_ready      <= '1' when out_valid = '0' or out_handshake = '1' else '0';

  debug_leds <= count(7 downto 0);

end rtl;