// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4.1 (win64) Build 2117270 Tue Jan 30 15:32:00 MST 2018
// Date        : Thu Mar  8 09:59:17 2018
// Host        : HR3-ELLAB-P10 running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode funcsim -rename_top design_1_block_ram_top_0_1 -prefix
//               design_1_block_ram_top_0_1_ design_1_block_ram_top_0_2_sim_netlist.v
// Design      : design_1_block_ram_top_0_2
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_block_ram_top_0_1_block_ram
   (\data_out_wire[0]_16 ,
    clk,
    Q,
    s_axis_tdata,
    \slv_regs_reg[0][31] ,
    E,
    SR);
  output [31:0]\data_out_wire[0]_16 ;
  input clk;
  input [0:0]Q;
  input [6:0]s_axis_tdata;
  input [31:0]\slv_regs_reg[0][31] ;
  input [0:0]E;
  input [0:0]SR;

  wire [0:0]E;
  wire [0:0]Q;
  wire [0:0]SR;
  wire clk;
  wire \count_i[6]_i_2_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [31:0]\data_out_wire[0]_16 ;
  wire [6:0]p_0_in;
  wire [6:0]s_axis_tdata;
  wire [31:0]\slv_regs_reg[0][31] ;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,s_axis_tdata,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(\slv_regs_reg[0][31] [15:0]),
        .DIBDI(\slv_regs_reg[0][31] [31:16]),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(\data_out_wire[0]_16 [15:0]),
        .DOBDO(\data_out_wire[0]_16 [31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({E,E,E,E}));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[2]_i_1 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[2]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \count_i[4]_i_1 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .I3(count_i_reg__0[3]),
        .I4(count_i_reg__0[4]),
        .O(p_0_in[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \count_i[5]_i_1 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[4]),
        .I5(count_i_reg__0[5]),
        .O(p_0_in[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[6]_i_1 
       (.I0(\count_i[6]_i_2_n_0 ),
        .I1(count_i_reg__0[5]),
        .I2(count_i_reg__0[6]),
        .O(p_0_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \count_i[6]_i_2 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .I4(count_i_reg__0[3]),
        .O(\count_i[6]_i_2_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(E),
        .D(p_0_in[0]),
        .Q(count_i_reg__0[0]),
        .R(SR));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(E),
        .D(p_0_in[1]),
        .Q(count_i_reg__0[1]),
        .R(SR));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(E),
        .D(p_0_in[2]),
        .Q(count_i_reg__0[2]),
        .R(SR));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(E),
        .D(p_0_in[3]),
        .Q(count_i_reg__0[3]),
        .R(SR));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(E),
        .D(p_0_in[4]),
        .Q(count_i_reg__0[4]),
        .R(SR));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(E),
        .D(p_0_in[5]),
        .Q(count_i_reg__0[5]),
        .R(SR));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(E),
        .D(p_0_in[6]),
        .Q(count_i_reg__0[6]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_block_ram_top_0_1_block_ram_0
   (\data_out_wire[1]_17 ,
    clk,
    Q,
    s_axis_tdata,
    \slv_regs_reg[1][31] ,
    E,
    SR);
  output [31:0]\data_out_wire[1]_17 ;
  input clk;
  input [0:0]Q;
  input [6:0]s_axis_tdata;
  input [31:0]\slv_regs_reg[1][31] ;
  input [0:0]E;
  input [0:0]SR;

  wire [0:0]E;
  wire [0:0]Q;
  wire [0:0]SR;
  wire clk;
  wire \count_i[6]_i_2__0_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [31:0]\data_out_wire[1]_17 ;
  wire [6:0]p_0_in__0;
  wire [6:0]s_axis_tdata;
  wire [31:0]\slv_regs_reg[1][31] ;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,s_axis_tdata,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(\slv_regs_reg[1][31] [15:0]),
        .DIBDI(\slv_regs_reg[1][31] [31:16]),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(\data_out_wire[1]_17 [15:0]),
        .DOBDO(\data_out_wire[1]_17 [31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({E,E,E,E}));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1__0 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1__0 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[2]_i_1__0 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[2]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1__0 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \count_i[4]_i_1__0 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .I3(count_i_reg__0[3]),
        .I4(count_i_reg__0[4]),
        .O(p_0_in__0[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \count_i[5]_i_1__0 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[4]),
        .I5(count_i_reg__0[5]),
        .O(p_0_in__0[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[6]_i_1__0 
       (.I0(\count_i[6]_i_2__0_n_0 ),
        .I1(count_i_reg__0[5]),
        .I2(count_i_reg__0[6]),
        .O(p_0_in__0[6]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \count_i[6]_i_2__0 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .I4(count_i_reg__0[3]),
        .O(\count_i[6]_i_2__0_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__0[0]),
        .Q(count_i_reg__0[0]),
        .R(SR));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__0[1]),
        .Q(count_i_reg__0[1]),
        .R(SR));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__0[2]),
        .Q(count_i_reg__0[2]),
        .R(SR));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__0[3]),
        .Q(count_i_reg__0[3]),
        .R(SR));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__0[4]),
        .Q(count_i_reg__0[4]),
        .R(SR));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__0[5]),
        .Q(count_i_reg__0[5]),
        .R(SR));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__0[6]),
        .Q(count_i_reg__0[6]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_block_ram_top_0_1_block_ram_1
   (\data_out_wire[2]_18 ,
    clk,
    Q,
    s_axis_tdata,
    \slv_regs_reg[2][31] ,
    E,
    SR);
  output [31:0]\data_out_wire[2]_18 ;
  input clk;
  input [0:0]Q;
  input [6:0]s_axis_tdata;
  input [31:0]\slv_regs_reg[2][31] ;
  input [0:0]E;
  input [0:0]SR;

  wire [0:0]E;
  wire [0:0]Q;
  wire [0:0]SR;
  wire clk;
  wire \count_i[6]_i_2__1_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [31:0]\data_out_wire[2]_18 ;
  wire [6:0]p_0_in__1;
  wire [6:0]s_axis_tdata;
  wire [31:0]\slv_regs_reg[2][31] ;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,s_axis_tdata,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(\slv_regs_reg[2][31] [15:0]),
        .DIBDI(\slv_regs_reg[2][31] [31:16]),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(\data_out_wire[2]_18 [15:0]),
        .DOBDO(\data_out_wire[2]_18 [31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({E,E,E,E}));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1__1 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1__1 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[2]_i_1__1 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[2]),
        .O(p_0_in__1[2]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1__1 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in__1[3]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \count_i[4]_i_1__1 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .I3(count_i_reg__0[3]),
        .I4(count_i_reg__0[4]),
        .O(p_0_in__1[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \count_i[5]_i_1__1 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[4]),
        .I5(count_i_reg__0[5]),
        .O(p_0_in__1[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[6]_i_1__1 
       (.I0(\count_i[6]_i_2__1_n_0 ),
        .I1(count_i_reg__0[5]),
        .I2(count_i_reg__0[6]),
        .O(p_0_in__1[6]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \count_i[6]_i_2__1 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .I4(count_i_reg__0[3]),
        .O(\count_i[6]_i_2__1_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__1[0]),
        .Q(count_i_reg__0[0]),
        .R(SR));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__1[1]),
        .Q(count_i_reg__0[1]),
        .R(SR));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__1[2]),
        .Q(count_i_reg__0[2]),
        .R(SR));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__1[3]),
        .Q(count_i_reg__0[3]),
        .R(SR));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__1[4]),
        .Q(count_i_reg__0[4]),
        .R(SR));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__1[5]),
        .Q(count_i_reg__0[5]),
        .R(SR));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__1[6]),
        .Q(count_i_reg__0[6]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_block_ram_top_0_1_block_ram_2
   (\data_out_wire[3]_19 ,
    clk,
    Q,
    s_axis_tdata,
    \slv_regs_reg[3][31] ,
    E,
    SR);
  output [31:0]\data_out_wire[3]_19 ;
  input clk;
  input [0:0]Q;
  input [6:0]s_axis_tdata;
  input [31:0]\slv_regs_reg[3][31] ;
  input [0:0]E;
  input [0:0]SR;

  wire [0:0]E;
  wire [0:0]Q;
  wire [0:0]SR;
  wire clk;
  wire \count_i[6]_i_2__2_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [31:0]\data_out_wire[3]_19 ;
  wire [6:0]p_0_in__2;
  wire [6:0]s_axis_tdata;
  wire [31:0]\slv_regs_reg[3][31] ;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,s_axis_tdata,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(\slv_regs_reg[3][31] [15:0]),
        .DIBDI(\slv_regs_reg[3][31] [31:16]),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(\data_out_wire[3]_19 [15:0]),
        .DOBDO(\data_out_wire[3]_19 [31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({E,E,E,E}));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1__2 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in__2[0]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1__2 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in__2[1]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[2]_i_1__2 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[2]),
        .O(p_0_in__2[2]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1__2 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in__2[3]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \count_i[4]_i_1__2 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .I3(count_i_reg__0[3]),
        .I4(count_i_reg__0[4]),
        .O(p_0_in__2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \count_i[5]_i_1__2 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[4]),
        .I5(count_i_reg__0[5]),
        .O(p_0_in__2[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[6]_i_1__2 
       (.I0(\count_i[6]_i_2__2_n_0 ),
        .I1(count_i_reg__0[5]),
        .I2(count_i_reg__0[6]),
        .O(p_0_in__2[6]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \count_i[6]_i_2__2 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .I4(count_i_reg__0[3]),
        .O(\count_i[6]_i_2__2_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__2[0]),
        .Q(count_i_reg__0[0]),
        .R(SR));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__2[1]),
        .Q(count_i_reg__0[1]),
        .R(SR));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__2[2]),
        .Q(count_i_reg__0[2]),
        .R(SR));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__2[3]),
        .Q(count_i_reg__0[3]),
        .R(SR));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__2[4]),
        .Q(count_i_reg__0[4]),
        .R(SR));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__2[5]),
        .Q(count_i_reg__0[5]),
        .R(SR));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__2[6]),
        .Q(count_i_reg__0[6]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_block_ram_top_0_1_block_ram_3
   (\data_out_wire[4]_20 ,
    clk,
    Q,
    s_axis_tdata,
    \slv_regs_reg[4][31] ,
    E,
    SR);
  output [31:0]\data_out_wire[4]_20 ;
  input clk;
  input [0:0]Q;
  input [6:0]s_axis_tdata;
  input [31:0]\slv_regs_reg[4][31] ;
  input [0:0]E;
  input [0:0]SR;

  wire [0:0]E;
  wire [0:0]Q;
  wire [0:0]SR;
  wire clk;
  wire \count_i[6]_i_2__3_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [31:0]\data_out_wire[4]_20 ;
  wire [6:0]p_0_in__3;
  wire [6:0]s_axis_tdata;
  wire [31:0]\slv_regs_reg[4][31] ;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,s_axis_tdata,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(\slv_regs_reg[4][31] [15:0]),
        .DIBDI(\slv_regs_reg[4][31] [31:16]),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(\data_out_wire[4]_20 [15:0]),
        .DOBDO(\data_out_wire[4]_20 [31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(Q),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({E,E,E,E}));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1__3 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in__3[0]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1__3 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in__3[1]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[2]_i_1__3 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[2]),
        .O(p_0_in__3[2]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1__3 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in__3[3]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \count_i[4]_i_1__3 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .I3(count_i_reg__0[3]),
        .I4(count_i_reg__0[4]),
        .O(p_0_in__3[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \count_i[5]_i_1__3 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[4]),
        .I5(count_i_reg__0[5]),
        .O(p_0_in__3[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[6]_i_1__3 
       (.I0(\count_i[6]_i_2__3_n_0 ),
        .I1(count_i_reg__0[5]),
        .I2(count_i_reg__0[6]),
        .O(p_0_in__3[6]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \count_i[6]_i_2__3 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .I4(count_i_reg__0[3]),
        .O(\count_i[6]_i_2__3_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__3[0]),
        .Q(count_i_reg__0[0]),
        .R(SR));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__3[1]),
        .Q(count_i_reg__0[1]),
        .R(SR));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__3[2]),
        .Q(count_i_reg__0[2]),
        .R(SR));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__3[3]),
        .Q(count_i_reg__0[3]),
        .R(SR));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__3[4]),
        .Q(count_i_reg__0[4]),
        .R(SR));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__3[5]),
        .Q(count_i_reg__0[5]),
        .R(SR));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__3[6]),
        .Q(count_i_reg__0[6]),
        .R(SR));
endmodule

module design_1_block_ram_top_0_1_block_ram_top
   (s_axi_awready,
    s_axi_wready,
    s_axi_arready,
    s_axi_rvalid,
    s_axi_rdata,
    m_axis_tdata,
    s_axi_bvalid,
    s_axi_awvalid,
    s_axi_wvalid,
    aresetn,
    s_axi_arvalid,
    clk,
    s_axi_awaddr,
    s_axi_wdata,
    s_axi_araddr,
    s_axi_wstrb,
    s_axis_tdata,
    s_axi_bready,
    s_axi_rready);
  output s_axi_awready;
  output s_axi_wready;
  output s_axi_arready;
  output s_axi_rvalid;
  output [31:0]s_axi_rdata;
  output [31:0]m_axis_tdata;
  output s_axi_bvalid;
  input s_axi_awvalid;
  input s_axi_wvalid;
  input aresetn;
  input s_axi_arvalid;
  input clk;
  input [3:0]s_axi_awaddr;
  input [31:0]s_axi_wdata;
  input [3:0]s_axi_araddr;
  input [3:0]s_axi_wstrb;
  input [6:0]s_axis_tdata;
  input s_axi_bready;
  input s_axi_rready;

  wire aresetn;
  wire clk;
  wire [31:0]\data_in[0]_0 ;
  wire [31:0]\data_in[1]_1 ;
  wire [31:0]\data_in[2]_2 ;
  wire [31:0]\data_in[3]_3 ;
  wire [31:0]\data_in[4]_4 ;
  wire [31:0]\data_out_wire[0]_16 ;
  wire [31:0]\data_out_wire[1]_17 ;
  wire [31:0]\data_out_wire[2]_18 ;
  wire [31:0]\data_out_wire[3]_19 ;
  wire [31:0]\data_out_wire[4]_20 ;
  wire i_register_interface_n_1;
  wire [4:0]in_handshakes;
  wire [31:0]m_axis_tdata;
  wire out_data_n_0;
  wire [4:0]read_enable;
  wire [3:0]s_axi_araddr;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [3:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire s_axi_rready;
  wire s_axi_rvalid;
  wire [31:0]s_axi_wdata;
  wire s_axi_wready;
  wire [3:0]s_axi_wstrb;
  wire s_axi_wvalid;
  wire [6:0]s_axis_tdata;

  design_1_block_ram_top_0_1_block_ram \b_ramx[0].b_ramx 
       (.E(in_handshakes[0]),
        .Q(read_enable[0]),
        .SR(i_register_interface_n_1),
        .clk(clk),
        .\data_out_wire[0]_16 (\data_out_wire[0]_16 ),
        .s_axis_tdata(s_axis_tdata),
        .\slv_regs_reg[0][31] (\data_in[0]_0 ));
  design_1_block_ram_top_0_1_block_ram_0 \b_ramx[1].b_ramx 
       (.E(in_handshakes[1]),
        .Q(read_enable[1]),
        .SR(i_register_interface_n_1),
        .clk(clk),
        .\data_out_wire[1]_17 (\data_out_wire[1]_17 ),
        .s_axis_tdata(s_axis_tdata),
        .\slv_regs_reg[1][31] (\data_in[1]_1 ));
  design_1_block_ram_top_0_1_block_ram_1 \b_ramx[2].b_ramx 
       (.E(in_handshakes[2]),
        .Q(read_enable[2]),
        .SR(i_register_interface_n_1),
        .clk(clk),
        .\data_out_wire[2]_18 (\data_out_wire[2]_18 ),
        .s_axis_tdata(s_axis_tdata),
        .\slv_regs_reg[2][31] (\data_in[2]_2 ));
  design_1_block_ram_top_0_1_block_ram_2 \b_ramx[3].b_ramx 
       (.E(in_handshakes[3]),
        .Q(read_enable[3]),
        .SR(i_register_interface_n_1),
        .clk(clk),
        .\data_out_wire[3]_19 (\data_out_wire[3]_19 ),
        .s_axis_tdata(s_axis_tdata),
        .\slv_regs_reg[3][31] (\data_in[3]_3 ));
  design_1_block_ram_top_0_1_block_ram_3 \b_ramx[4].b_ramx 
       (.E(in_handshakes[4]),
        .Q(read_enable[4]),
        .SR(i_register_interface_n_1),
        .clk(clk),
        .\data_out_wire[4]_20 (\data_out_wire[4]_20 ),
        .s_axis_tdata(s_axis_tdata),
        .\slv_regs_reg[4][31] (\data_in[4]_4 ));
  design_1_block_ram_top_0_1_register_interface i_register_interface
       (.Q(\data_in[0]_0 ),
        .SR(i_register_interface_n_1),
        .aresetn(aresetn),
        .b_ram_data_reg(\data_in[1]_1 ),
        .b_ram_data_reg_0(\data_in[2]_2 ),
        .b_ram_data_reg_1(\data_in[3]_3 ),
        .b_ram_data_reg_2(\data_in[4]_4 ),
        .b_ram_data_reg_3(read_enable),
        .clk(clk),
        .\data_out_wire[0]_16 (\data_out_wire[0]_16 ),
        .\data_out_wire[1]_17 (\data_out_wire[1]_17 ),
        .\data_out_wire[2]_18 (\data_out_wire[2]_18 ),
        .\data_out_wire[3]_19 (\data_out_wire[3]_19 ),
        .\data_out_wire[4]_20 (\data_out_wire[4]_20 ),
        .in_handshakes(in_handshakes),
        .m_axis_tdata(m_axis_tdata),
        .s_axi_araddr(s_axi_araddr),
        .s_axi_arready(s_axi_arready),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awaddr(s_axi_awaddr),
        .s_axi_awready(s_axi_awready),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_bready(s_axi_bready),
        .s_axi_bvalid(s_axi_bvalid),
        .s_axi_rdata(s_axi_rdata),
        .s_axi_rready(s_axi_rready),
        .s_axi_rvalid(s_axi_rvalid),
        .s_axi_wdata(s_axi_wdata),
        .s_axi_wready(s_axi_wready),
        .s_axi_wstrb(s_axi_wstrb),
        .s_axi_wvalid(s_axi_wvalid),
        .\slv_regs_reg[5][0]_0 (out_data_n_0));
  LUT5 #(
    .INIT(32'h00010116)) 
    out_data
       (.I0(read_enable[0]),
        .I1(read_enable[1]),
        .I2(read_enable[2]),
        .I3(read_enable[3]),
        .I4(read_enable[4]),
        .O(out_data_n_0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_block_ram_top_0_2,block_ram_top,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "block_ram_top,Vivado 2017.4.1" *) 
(* NotValidForBitStream *)
module design_1_block_ram_top_0_1
   (clk,
    aresetn,
    debug_leds,
    s_axis_tdata,
    s_axis_tvalid,
    s_axis_tready,
    s_axis_tlast,
    m_axis_tdata,
    m_axis_tvalid,
    m_axis_tready,
    m_axis_tlast,
    s_axi_awaddr,
    s_axi_awprot,
    s_axi_awvalid,
    s_axi_awready,
    s_axi_wdata,
    s_axi_wstrb,
    s_axi_wvalid,
    s_axi_wready,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,
    s_axi_araddr,
    s_axi_arprot,
    s_axi_arvalid,
    s_axi_arready,
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rvalid,
    s_axi_rready);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk, ASSOCIATED_BUSIF m_axis:s_axis:s_axi, ASSOCIATED_RESET aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0" *) input clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 aresetn RST" *) (* x_interface_parameter = "XIL_INTERFACENAME aresetn, POLARITY ACTIVE_LOW" *) input aresetn;
  output [7:0]debug_leds;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 s_axis TDATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef" *) input [31:0]s_axis_tdata;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 s_axis TVALID" *) input s_axis_tvalid;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 s_axis TREADY" *) output s_axis_tready;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 s_axis TLAST" *) input s_axis_tlast;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 m_axis TDATA" *) (* x_interface_parameter = "XIL_INTERFACENAME m_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef" *) output [31:0]m_axis_tdata;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 m_axis TVALID" *) output m_axis_tvalid;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 m_axis TREADY" *) input m_axis_tready;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 m_axis TLAST" *) output m_axis_tlast;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axi, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 4, NUM_WRITE_THREADS 4, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [5:0]s_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi AWPROT" *) input [2:0]s_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi AWVALID" *) input s_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi AWREADY" *) output s_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi WDATA" *) input [31:0]s_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi WSTRB" *) input [3:0]s_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi WVALID" *) input s_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi WREADY" *) output s_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi BRESP" *) output [1:0]s_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi BVALID" *) output s_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi BREADY" *) input s_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi ARADDR" *) input [5:0]s_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi ARPROT" *) input [2:0]s_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi ARVALID" *) input s_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi ARREADY" *) output s_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi RDATA" *) output [31:0]s_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi RRESP" *) output [1:0]s_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi RVALID" *) output s_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi RREADY" *) input s_axi_rready;

  wire \<const0> ;
  wire \<const1> ;
  wire aresetn;
  wire clk;
  wire [31:0]m_axis_tdata;
  wire [5:0]s_axi_araddr;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [5:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire s_axi_rready;
  wire s_axi_rvalid;
  wire [31:0]s_axi_wdata;
  wire s_axi_wready;
  wire [3:0]s_axi_wstrb;
  wire s_axi_wvalid;
  wire [31:0]s_axis_tdata;

  assign s_axi_bresp[1] = \<const0> ;
  assign s_axi_bresp[0] = \<const0> ;
  assign s_axi_rresp[1] = \<const0> ;
  assign s_axi_rresp[0] = \<const0> ;
  assign s_axis_tready = \<const1> ;
  GND GND
       (.G(\<const0> ));
  design_1_block_ram_top_0_1_block_ram_top U0
       (.aresetn(aresetn),
        .clk(clk),
        .m_axis_tdata(m_axis_tdata),
        .s_axi_araddr(s_axi_araddr[5:2]),
        .s_axi_arready(s_axi_arready),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awaddr(s_axi_awaddr[5:2]),
        .s_axi_awready(s_axi_awready),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_bready(s_axi_bready),
        .s_axi_bvalid(s_axi_bvalid),
        .s_axi_rdata(s_axi_rdata),
        .s_axi_rready(s_axi_rready),
        .s_axi_rvalid(s_axi_rvalid),
        .s_axi_wdata(s_axi_wdata),
        .s_axi_wready(s_axi_wready),
        .s_axi_wstrb(s_axi_wstrb),
        .s_axi_wvalid(s_axi_wvalid),
        .s_axis_tdata(s_axis_tdata[6:0]));
  VCC VCC
       (.P(\<const1> ));
endmodule

module design_1_block_ram_top_0_1_register_interface
   (s_axi_awready,
    SR,
    s_axi_wready,
    s_axi_arready,
    s_axi_bvalid,
    s_axi_rvalid,
    Q,
    b_ram_data_reg,
    b_ram_data_reg_0,
    b_ram_data_reg_1,
    b_ram_data_reg_2,
    in_handshakes,
    b_ram_data_reg_3,
    s_axi_rdata,
    m_axis_tdata,
    clk,
    s_axi_awvalid,
    s_axi_wvalid,
    aresetn,
    s_axi_arvalid,
    s_axi_bready,
    s_axi_rready,
    s_axi_awaddr,
    s_axi_wdata,
    s_axi_araddr,
    s_axi_wstrb,
    \slv_regs_reg[5][0]_0 ,
    \data_out_wire[3]_19 ,
    \data_out_wire[4]_20 ,
    \data_out_wire[0]_16 ,
    \data_out_wire[2]_18 ,
    \data_out_wire[1]_17 );
  output s_axi_awready;
  output [0:0]SR;
  output s_axi_wready;
  output s_axi_arready;
  output s_axi_bvalid;
  output s_axi_rvalid;
  output [31:0]Q;
  output [31:0]b_ram_data_reg;
  output [31:0]b_ram_data_reg_0;
  output [31:0]b_ram_data_reg_1;
  output [31:0]b_ram_data_reg_2;
  output [4:0]in_handshakes;
  output [4:0]b_ram_data_reg_3;
  output [31:0]s_axi_rdata;
  output [31:0]m_axis_tdata;
  input clk;
  input s_axi_awvalid;
  input s_axi_wvalid;
  input aresetn;
  input s_axi_arvalid;
  input s_axi_bready;
  input s_axi_rready;
  input [3:0]s_axi_awaddr;
  input [31:0]s_axi_wdata;
  input [3:0]s_axi_araddr;
  input [3:0]s_axi_wstrb;
  input \slv_regs_reg[5][0]_0 ;
  input [31:0]\data_out_wire[3]_19 ;
  input [31:0]\data_out_wire[4]_20 ;
  input [31:0]\data_out_wire[0]_16 ;
  input [31:0]\data_out_wire[2]_18 ;
  input [31:0]\data_out_wire[1]_17 ;

  wire [31:0]Q;
  wire [0:0]SR;
  wire aresetn;
  wire axi_arready_i_1_n_0;
  wire axi_awready_i_2_n_0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[0]_i_4_n_0 ;
  wire \axi_rdata[0]_i_5_n_0 ;
  wire \axi_rdata[0]_i_6_n_0 ;
  wire \axi_rdata[0]_i_7_n_0 ;
  wire \axi_rdata[10]_i_4_n_0 ;
  wire \axi_rdata[10]_i_5_n_0 ;
  wire \axi_rdata[10]_i_6_n_0 ;
  wire \axi_rdata[10]_i_7_n_0 ;
  wire \axi_rdata[11]_i_4_n_0 ;
  wire \axi_rdata[11]_i_5_n_0 ;
  wire \axi_rdata[11]_i_6_n_0 ;
  wire \axi_rdata[11]_i_7_n_0 ;
  wire \axi_rdata[12]_i_4_n_0 ;
  wire \axi_rdata[12]_i_5_n_0 ;
  wire \axi_rdata[12]_i_6_n_0 ;
  wire \axi_rdata[12]_i_7_n_0 ;
  wire \axi_rdata[13]_i_4_n_0 ;
  wire \axi_rdata[13]_i_5_n_0 ;
  wire \axi_rdata[13]_i_6_n_0 ;
  wire \axi_rdata[13]_i_7_n_0 ;
  wire \axi_rdata[14]_i_4_n_0 ;
  wire \axi_rdata[14]_i_5_n_0 ;
  wire \axi_rdata[14]_i_6_n_0 ;
  wire \axi_rdata[14]_i_7_n_0 ;
  wire \axi_rdata[15]_i_4_n_0 ;
  wire \axi_rdata[15]_i_5_n_0 ;
  wire \axi_rdata[15]_i_6_n_0 ;
  wire \axi_rdata[15]_i_7_n_0 ;
  wire \axi_rdata[16]_i_4_n_0 ;
  wire \axi_rdata[16]_i_5_n_0 ;
  wire \axi_rdata[16]_i_6_n_0 ;
  wire \axi_rdata[16]_i_7_n_0 ;
  wire \axi_rdata[17]_i_4_n_0 ;
  wire \axi_rdata[17]_i_5_n_0 ;
  wire \axi_rdata[17]_i_6_n_0 ;
  wire \axi_rdata[17]_i_7_n_0 ;
  wire \axi_rdata[18]_i_4_n_0 ;
  wire \axi_rdata[18]_i_5_n_0 ;
  wire \axi_rdata[18]_i_6_n_0 ;
  wire \axi_rdata[18]_i_7_n_0 ;
  wire \axi_rdata[19]_i_4_n_0 ;
  wire \axi_rdata[19]_i_5_n_0 ;
  wire \axi_rdata[19]_i_6_n_0 ;
  wire \axi_rdata[19]_i_7_n_0 ;
  wire \axi_rdata[1]_i_4_n_0 ;
  wire \axi_rdata[1]_i_5_n_0 ;
  wire \axi_rdata[1]_i_6_n_0 ;
  wire \axi_rdata[1]_i_7_n_0 ;
  wire \axi_rdata[20]_i_4_n_0 ;
  wire \axi_rdata[20]_i_5_n_0 ;
  wire \axi_rdata[20]_i_6_n_0 ;
  wire \axi_rdata[20]_i_7_n_0 ;
  wire \axi_rdata[21]_i_4_n_0 ;
  wire \axi_rdata[21]_i_5_n_0 ;
  wire \axi_rdata[21]_i_6_n_0 ;
  wire \axi_rdata[21]_i_7_n_0 ;
  wire \axi_rdata[22]_i_4_n_0 ;
  wire \axi_rdata[22]_i_5_n_0 ;
  wire \axi_rdata[22]_i_6_n_0 ;
  wire \axi_rdata[22]_i_7_n_0 ;
  wire \axi_rdata[23]_i_4_n_0 ;
  wire \axi_rdata[23]_i_5_n_0 ;
  wire \axi_rdata[23]_i_6_n_0 ;
  wire \axi_rdata[23]_i_7_n_0 ;
  wire \axi_rdata[24]_i_4_n_0 ;
  wire \axi_rdata[24]_i_5_n_0 ;
  wire \axi_rdata[24]_i_6_n_0 ;
  wire \axi_rdata[24]_i_7_n_0 ;
  wire \axi_rdata[25]_i_4_n_0 ;
  wire \axi_rdata[25]_i_5_n_0 ;
  wire \axi_rdata[25]_i_6_n_0 ;
  wire \axi_rdata[25]_i_7_n_0 ;
  wire \axi_rdata[26]_i_4_n_0 ;
  wire \axi_rdata[26]_i_5_n_0 ;
  wire \axi_rdata[26]_i_6_n_0 ;
  wire \axi_rdata[26]_i_7_n_0 ;
  wire \axi_rdata[27]_i_4_n_0 ;
  wire \axi_rdata[27]_i_5_n_0 ;
  wire \axi_rdata[27]_i_6_n_0 ;
  wire \axi_rdata[27]_i_7_n_0 ;
  wire \axi_rdata[28]_i_4_n_0 ;
  wire \axi_rdata[28]_i_5_n_0 ;
  wire \axi_rdata[28]_i_6_n_0 ;
  wire \axi_rdata[28]_i_7_n_0 ;
  wire \axi_rdata[29]_i_4_n_0 ;
  wire \axi_rdata[29]_i_5_n_0 ;
  wire \axi_rdata[29]_i_6_n_0 ;
  wire \axi_rdata[29]_i_7_n_0 ;
  wire \axi_rdata[2]_i_4_n_0 ;
  wire \axi_rdata[2]_i_5_n_0 ;
  wire \axi_rdata[2]_i_6_n_0 ;
  wire \axi_rdata[2]_i_7_n_0 ;
  wire \axi_rdata[30]_i_4_n_0 ;
  wire \axi_rdata[30]_i_5_n_0 ;
  wire \axi_rdata[30]_i_6_n_0 ;
  wire \axi_rdata[30]_i_7_n_0 ;
  wire \axi_rdata[31]_i_1_n_0 ;
  wire \axi_rdata[31]_i_5_n_0 ;
  wire \axi_rdata[31]_i_6_n_0 ;
  wire \axi_rdata[31]_i_7_n_0 ;
  wire \axi_rdata[31]_i_8_n_0 ;
  wire \axi_rdata[3]_i_4_n_0 ;
  wire \axi_rdata[3]_i_5_n_0 ;
  wire \axi_rdata[3]_i_6_n_0 ;
  wire \axi_rdata[3]_i_7_n_0 ;
  wire \axi_rdata[4]_i_4_n_0 ;
  wire \axi_rdata[4]_i_5_n_0 ;
  wire \axi_rdata[4]_i_6_n_0 ;
  wire \axi_rdata[4]_i_7_n_0 ;
  wire \axi_rdata[5]_i_4_n_0 ;
  wire \axi_rdata[5]_i_5_n_0 ;
  wire \axi_rdata[5]_i_6_n_0 ;
  wire \axi_rdata[5]_i_7_n_0 ;
  wire \axi_rdata[6]_i_4_n_0 ;
  wire \axi_rdata[6]_i_5_n_0 ;
  wire \axi_rdata[6]_i_6_n_0 ;
  wire \axi_rdata[6]_i_7_n_0 ;
  wire \axi_rdata[7]_i_4_n_0 ;
  wire \axi_rdata[7]_i_5_n_0 ;
  wire \axi_rdata[7]_i_6_n_0 ;
  wire \axi_rdata[7]_i_7_n_0 ;
  wire \axi_rdata[8]_i_4_n_0 ;
  wire \axi_rdata[8]_i_5_n_0 ;
  wire \axi_rdata[8]_i_6_n_0 ;
  wire \axi_rdata[8]_i_7_n_0 ;
  wire \axi_rdata[9]_i_4_n_0 ;
  wire \axi_rdata[9]_i_5_n_0 ;
  wire \axi_rdata[9]_i_6_n_0 ;
  wire \axi_rdata[9]_i_7_n_0 ;
  wire \axi_rdata_reg[0]_i_2_n_0 ;
  wire \axi_rdata_reg[0]_i_3_n_0 ;
  wire \axi_rdata_reg[10]_i_2_n_0 ;
  wire \axi_rdata_reg[10]_i_3_n_0 ;
  wire \axi_rdata_reg[11]_i_2_n_0 ;
  wire \axi_rdata_reg[11]_i_3_n_0 ;
  wire \axi_rdata_reg[12]_i_2_n_0 ;
  wire \axi_rdata_reg[12]_i_3_n_0 ;
  wire \axi_rdata_reg[13]_i_2_n_0 ;
  wire \axi_rdata_reg[13]_i_3_n_0 ;
  wire \axi_rdata_reg[14]_i_2_n_0 ;
  wire \axi_rdata_reg[14]_i_3_n_0 ;
  wire \axi_rdata_reg[15]_i_2_n_0 ;
  wire \axi_rdata_reg[15]_i_3_n_0 ;
  wire \axi_rdata_reg[16]_i_2_n_0 ;
  wire \axi_rdata_reg[16]_i_3_n_0 ;
  wire \axi_rdata_reg[17]_i_2_n_0 ;
  wire \axi_rdata_reg[17]_i_3_n_0 ;
  wire \axi_rdata_reg[18]_i_2_n_0 ;
  wire \axi_rdata_reg[18]_i_3_n_0 ;
  wire \axi_rdata_reg[19]_i_2_n_0 ;
  wire \axi_rdata_reg[19]_i_3_n_0 ;
  wire \axi_rdata_reg[1]_i_2_n_0 ;
  wire \axi_rdata_reg[1]_i_3_n_0 ;
  wire \axi_rdata_reg[20]_i_2_n_0 ;
  wire \axi_rdata_reg[20]_i_3_n_0 ;
  wire \axi_rdata_reg[21]_i_2_n_0 ;
  wire \axi_rdata_reg[21]_i_3_n_0 ;
  wire \axi_rdata_reg[22]_i_2_n_0 ;
  wire \axi_rdata_reg[22]_i_3_n_0 ;
  wire \axi_rdata_reg[23]_i_2_n_0 ;
  wire \axi_rdata_reg[23]_i_3_n_0 ;
  wire \axi_rdata_reg[24]_i_2_n_0 ;
  wire \axi_rdata_reg[24]_i_3_n_0 ;
  wire \axi_rdata_reg[25]_i_2_n_0 ;
  wire \axi_rdata_reg[25]_i_3_n_0 ;
  wire \axi_rdata_reg[26]_i_2_n_0 ;
  wire \axi_rdata_reg[26]_i_3_n_0 ;
  wire \axi_rdata_reg[27]_i_2_n_0 ;
  wire \axi_rdata_reg[27]_i_3_n_0 ;
  wire \axi_rdata_reg[28]_i_2_n_0 ;
  wire \axi_rdata_reg[28]_i_3_n_0 ;
  wire \axi_rdata_reg[29]_i_2_n_0 ;
  wire \axi_rdata_reg[29]_i_3_n_0 ;
  wire \axi_rdata_reg[2]_i_2_n_0 ;
  wire \axi_rdata_reg[2]_i_3_n_0 ;
  wire \axi_rdata_reg[30]_i_2_n_0 ;
  wire \axi_rdata_reg[30]_i_3_n_0 ;
  wire \axi_rdata_reg[31]_i_3_n_0 ;
  wire \axi_rdata_reg[31]_i_4_n_0 ;
  wire \axi_rdata_reg[3]_i_2_n_0 ;
  wire \axi_rdata_reg[3]_i_3_n_0 ;
  wire \axi_rdata_reg[4]_i_2_n_0 ;
  wire \axi_rdata_reg[4]_i_3_n_0 ;
  wire \axi_rdata_reg[5]_i_2_n_0 ;
  wire \axi_rdata_reg[5]_i_3_n_0 ;
  wire \axi_rdata_reg[6]_i_2_n_0 ;
  wire \axi_rdata_reg[6]_i_3_n_0 ;
  wire \axi_rdata_reg[7]_i_2_n_0 ;
  wire \axi_rdata_reg[7]_i_3_n_0 ;
  wire \axi_rdata_reg[8]_i_2_n_0 ;
  wire \axi_rdata_reg[8]_i_3_n_0 ;
  wire \axi_rdata_reg[9]_i_2_n_0 ;
  wire \axi_rdata_reg[9]_i_3_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready_i_1_n_0;
  wire [31:0]b_ram_data_reg;
  wire [31:0]b_ram_data_reg_0;
  wire [31:0]b_ram_data_reg_1;
  wire [31:0]b_ram_data_reg_2;
  wire [4:0]b_ram_data_reg_3;
  wire clk;
  wire [31:0]\data_out_wire[0]_16 ;
  wire [31:0]\data_out_wire[1]_17 ;
  wire [31:0]\data_out_wire[2]_18 ;
  wire [31:0]\data_out_wire[3]_19 ;
  wire [31:0]\data_out_wire[4]_20 ;
  wire [4:0]in_handshakes;
  wire \in_handshakes[1]_i_1_n_0 ;
  wire \in_handshakes[2]_i_1_n_0 ;
  wire \in_handshakes[3]_i_1_n_0 ;
  wire \in_handshakes[4]_i_1_n_0 ;
  wire \in_handshakes[4]_i_2_n_0 ;
  wire [31:0]m_axis_tdata;
  wire \m_axis_tdata[0]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[10]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[11]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[12]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[13]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[14]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[15]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[16]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[17]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[18]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[19]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[1]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[20]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[21]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[22]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[23]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[24]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[25]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[26]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[27]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[28]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[29]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[2]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[30]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[31]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[3]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[4]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[5]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[6]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[7]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[8]_INST_0_i_1_n_0 ;
  wire \m_axis_tdata[9]_INST_0_i_1_n_0 ;
  wire [3:0]p_0_in;
  wire [31:7]p_1_in;
  wire [31:0]\read_data[0]_21 ;
  wire [3:0]s_axi_araddr;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [3:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire s_axi_rready;
  wire s_axi_rvalid;
  wire [31:0]s_axi_wdata;
  wire s_axi_wready;
  wire [3:0]s_axi_wstrb;
  wire s_axi_wvalid;
  wire [3:0]sel0;
  wire slv_reg_wren__2;
  wire \slv_regs[10][15]_i_1_n_0 ;
  wire \slv_regs[10][23]_i_1_n_0 ;
  wire \slv_regs[10][31]_i_1_n_0 ;
  wire \slv_regs[10][7]_i_1_n_0 ;
  wire \slv_regs[11][15]_i_1_n_0 ;
  wire \slv_regs[11][23]_i_1_n_0 ;
  wire \slv_regs[11][31]_i_1_n_0 ;
  wire \slv_regs[11][7]_i_1_n_0 ;
  wire \slv_regs[12][15]_i_1_n_0 ;
  wire \slv_regs[12][23]_i_1_n_0 ;
  wire \slv_regs[12][31]_i_1_n_0 ;
  wire \slv_regs[12][7]_i_1_n_0 ;
  wire \slv_regs[13][15]_i_1_n_0 ;
  wire \slv_regs[13][23]_i_1_n_0 ;
  wire \slv_regs[13][31]_i_1_n_0 ;
  wire \slv_regs[13][7]_i_1_n_0 ;
  wire \slv_regs[14][15]_i_1_n_0 ;
  wire \slv_regs[14][23]_i_1_n_0 ;
  wire \slv_regs[14][31]_i_1_n_0 ;
  wire \slv_regs[14][7]_i_1_n_0 ;
  wire \slv_regs[15][15]_i_1_n_0 ;
  wire \slv_regs[15][23]_i_1_n_0 ;
  wire \slv_regs[15][31]_i_1_n_0 ;
  wire \slv_regs[15][7]_i_1_n_0 ;
  wire \slv_regs[1][15]_i_1_n_0 ;
  wire \slv_regs[1][23]_i_1_n_0 ;
  wire \slv_regs[1][31]_i_1_n_0 ;
  wire \slv_regs[1][7]_i_1_n_0 ;
  wire \slv_regs[2][15]_i_1_n_0 ;
  wire \slv_regs[2][23]_i_1_n_0 ;
  wire \slv_regs[2][31]_i_1_n_0 ;
  wire \slv_regs[2][7]_i_1_n_0 ;
  wire \slv_regs[3][15]_i_1_n_0 ;
  wire \slv_regs[3][23]_i_1_n_0 ;
  wire \slv_regs[3][31]_i_1_n_0 ;
  wire \slv_regs[3][7]_i_1_n_0 ;
  wire \slv_regs[4][15]_i_1_n_0 ;
  wire \slv_regs[4][23]_i_1_n_0 ;
  wire \slv_regs[4][31]_i_1_n_0 ;
  wire \slv_regs[4][7]_i_1_n_0 ;
  wire \slv_regs[5][15]_i_1_n_0 ;
  wire \slv_regs[5][23]_i_1_n_0 ;
  wire \slv_regs[5][31]_i_1_n_0 ;
  wire \slv_regs[5][7]_i_1_n_0 ;
  wire \slv_regs[6][15]_i_1_n_0 ;
  wire \slv_regs[6][23]_i_1_n_0 ;
  wire \slv_regs[6][31]_i_1_n_0 ;
  wire \slv_regs[6][7]_i_1_n_0 ;
  wire \slv_regs[7][15]_i_1_n_0 ;
  wire \slv_regs[7][23]_i_1_n_0 ;
  wire \slv_regs[7][31]_i_1_n_0 ;
  wire \slv_regs[7][7]_i_1_n_0 ;
  wire \slv_regs[8][15]_i_1_n_0 ;
  wire \slv_regs[8][23]_i_1_n_0 ;
  wire \slv_regs[8][31]_i_1_n_0 ;
  wire \slv_regs[8][7]_i_1_n_0 ;
  wire \slv_regs[9][15]_i_1_n_0 ;
  wire \slv_regs[9][23]_i_1_n_0 ;
  wire \slv_regs[9][31]_i_1_n_0 ;
  wire \slv_regs[9][7]_i_1_n_0 ;
  wire [7:7]\slv_regs_reg[0]__0 ;
  wire [31:0]\slv_regs_reg[10]_10 ;
  wire [31:0]\slv_regs_reg[11]_11 ;
  wire [31:0]\slv_regs_reg[12]_12 ;
  wire [31:0]\slv_regs_reg[13]_13 ;
  wire [31:0]\slv_regs_reg[14]_14 ;
  wire [31:0]\slv_regs_reg[15]_15 ;
  wire \slv_regs_reg[5][0]_0 ;
  wire [31:5]\slv_regs_reg[5]_5 ;
  wire [31:0]\slv_regs_reg[6]_6 ;
  wire [31:0]\slv_regs_reg[7]_7 ;
  wire [31:0]\slv_regs_reg[8]_8 ;
  wire [31:0]\slv_regs_reg[9]_9 ;

  FDSE \axi_araddr_reg[2] 
       (.C(clk),
        .CE(axi_arready_i_1_n_0),
        .D(s_axi_araddr[0]),
        .Q(sel0[0]),
        .S(SR));
  FDSE \axi_araddr_reg[3] 
       (.C(clk),
        .CE(axi_arready_i_1_n_0),
        .D(s_axi_araddr[1]),
        .Q(sel0[1]),
        .S(SR));
  FDSE \axi_araddr_reg[4] 
       (.C(clk),
        .CE(axi_arready_i_1_n_0),
        .D(s_axi_araddr[2]),
        .Q(sel0[2]),
        .S(SR));
  FDSE \axi_araddr_reg[5] 
       (.C(clk),
        .CE(axi_arready_i_1_n_0),
        .D(s_axi_araddr[3]),
        .Q(sel0[3]),
        .S(SR));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s_axi_arvalid),
        .I1(s_axi_arready),
        .O(axi_arready_i_1_n_0));
  FDRE axi_arready_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_arready_i_1_n_0),
        .Q(s_axi_arready),
        .R(SR));
  FDRE \axi_awaddr_reg[2] 
       (.C(clk),
        .CE(axi_awready_i_2_n_0),
        .D(s_axi_awaddr[0]),
        .Q(p_0_in[0]),
        .R(SR));
  FDRE \axi_awaddr_reg[3] 
       (.C(clk),
        .CE(axi_awready_i_2_n_0),
        .D(s_axi_awaddr[1]),
        .Q(p_0_in[1]),
        .R(SR));
  FDRE \axi_awaddr_reg[4] 
       (.C(clk),
        .CE(axi_awready_i_2_n_0),
        .D(s_axi_awaddr[2]),
        .Q(p_0_in[2]),
        .R(SR));
  FDRE \axi_awaddr_reg[5] 
       (.C(clk),
        .CE(axi_awready_i_2_n_0),
        .D(s_axi_awaddr[3]),
        .Q(p_0_in[3]),
        .R(SR));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(aresetn),
        .O(SR));
  LUT3 #(
    .INIT(8'h08)) 
    axi_awready_i_2
       (.I0(s_axi_awvalid),
        .I1(s_axi_wvalid),
        .I2(s_axi_awready),
        .O(axi_awready_i_2_n_0));
  FDRE axi_awready_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_awready_i_2_n_0),
        .Q(s_axi_awready),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s_axi_wready),
        .I1(s_axi_wvalid),
        .I2(s_axi_awready),
        .I3(s_axi_awvalid),
        .I4(s_axi_bready),
        .I5(s_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s_axi_bvalid),
        .R(SR));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[0]_i_4 
       (.I0(b_ram_data_reg_1[0]),
        .I1(b_ram_data_reg_0[0]),
        .I2(sel0[1]),
        .I3(Q[0]),
        .I4(sel0[0]),
        .O(\axi_rdata[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_5 
       (.I0(\slv_regs_reg[7]_7 [0]),
        .I1(\slv_regs_reg[6]_6 [0]),
        .I2(sel0[1]),
        .I3(b_ram_data_reg_3[0]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[0]),
        .O(\axi_rdata[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_6 
       (.I0(\slv_regs_reg[11]_11 [0]),
        .I1(\slv_regs_reg[10]_10 [0]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [0]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [0]),
        .O(\axi_rdata[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_7 
       (.I0(\slv_regs_reg[15]_15 [0]),
        .I1(\slv_regs_reg[14]_14 [0]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [0]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [0]),
        .O(\axi_rdata[0]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[10]_i_4 
       (.I0(b_ram_data_reg_1[10]),
        .I1(b_ram_data_reg_0[10]),
        .I2(sel0[1]),
        .I3(Q[10]),
        .I4(sel0[0]),
        .O(\axi_rdata[10]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_5 
       (.I0(\slv_regs_reg[7]_7 [10]),
        .I1(\slv_regs_reg[6]_6 [10]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [10]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[10]),
        .O(\axi_rdata[10]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_6 
       (.I0(\slv_regs_reg[11]_11 [10]),
        .I1(\slv_regs_reg[10]_10 [10]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [10]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [10]),
        .O(\axi_rdata[10]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_7 
       (.I0(\slv_regs_reg[15]_15 [10]),
        .I1(\slv_regs_reg[14]_14 [10]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [10]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [10]),
        .O(\axi_rdata[10]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[11]_i_4 
       (.I0(b_ram_data_reg_1[11]),
        .I1(b_ram_data_reg_0[11]),
        .I2(sel0[1]),
        .I3(Q[11]),
        .I4(sel0[0]),
        .O(\axi_rdata[11]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_5 
       (.I0(\slv_regs_reg[7]_7 [11]),
        .I1(\slv_regs_reg[6]_6 [11]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [11]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[11]),
        .O(\axi_rdata[11]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_6 
       (.I0(\slv_regs_reg[11]_11 [11]),
        .I1(\slv_regs_reg[10]_10 [11]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [11]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [11]),
        .O(\axi_rdata[11]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_7 
       (.I0(\slv_regs_reg[15]_15 [11]),
        .I1(\slv_regs_reg[14]_14 [11]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [11]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [11]),
        .O(\axi_rdata[11]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[12]_i_4 
       (.I0(b_ram_data_reg_1[12]),
        .I1(b_ram_data_reg_0[12]),
        .I2(sel0[1]),
        .I3(Q[12]),
        .I4(sel0[0]),
        .O(\axi_rdata[12]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_5 
       (.I0(\slv_regs_reg[7]_7 [12]),
        .I1(\slv_regs_reg[6]_6 [12]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [12]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[12]),
        .O(\axi_rdata[12]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_6 
       (.I0(\slv_regs_reg[11]_11 [12]),
        .I1(\slv_regs_reg[10]_10 [12]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [12]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [12]),
        .O(\axi_rdata[12]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_7 
       (.I0(\slv_regs_reg[15]_15 [12]),
        .I1(\slv_regs_reg[14]_14 [12]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [12]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [12]),
        .O(\axi_rdata[12]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[13]_i_4 
       (.I0(b_ram_data_reg_1[13]),
        .I1(b_ram_data_reg_0[13]),
        .I2(sel0[1]),
        .I3(Q[13]),
        .I4(sel0[0]),
        .O(\axi_rdata[13]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_5 
       (.I0(\slv_regs_reg[7]_7 [13]),
        .I1(\slv_regs_reg[6]_6 [13]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [13]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[13]),
        .O(\axi_rdata[13]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_6 
       (.I0(\slv_regs_reg[11]_11 [13]),
        .I1(\slv_regs_reg[10]_10 [13]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [13]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [13]),
        .O(\axi_rdata[13]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_7 
       (.I0(\slv_regs_reg[15]_15 [13]),
        .I1(\slv_regs_reg[14]_14 [13]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [13]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [13]),
        .O(\axi_rdata[13]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[14]_i_4 
       (.I0(b_ram_data_reg_1[14]),
        .I1(b_ram_data_reg_0[14]),
        .I2(sel0[1]),
        .I3(Q[14]),
        .I4(sel0[0]),
        .O(\axi_rdata[14]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_5 
       (.I0(\slv_regs_reg[7]_7 [14]),
        .I1(\slv_regs_reg[6]_6 [14]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [14]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[14]),
        .O(\axi_rdata[14]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_6 
       (.I0(\slv_regs_reg[11]_11 [14]),
        .I1(\slv_regs_reg[10]_10 [14]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [14]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [14]),
        .O(\axi_rdata[14]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_7 
       (.I0(\slv_regs_reg[15]_15 [14]),
        .I1(\slv_regs_reg[14]_14 [14]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [14]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [14]),
        .O(\axi_rdata[14]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[15]_i_4 
       (.I0(b_ram_data_reg_1[15]),
        .I1(b_ram_data_reg_0[15]),
        .I2(sel0[1]),
        .I3(Q[15]),
        .I4(sel0[0]),
        .O(\axi_rdata[15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_5 
       (.I0(\slv_regs_reg[7]_7 [15]),
        .I1(\slv_regs_reg[6]_6 [15]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [15]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[15]),
        .O(\axi_rdata[15]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_6 
       (.I0(\slv_regs_reg[11]_11 [15]),
        .I1(\slv_regs_reg[10]_10 [15]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [15]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [15]),
        .O(\axi_rdata[15]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_7 
       (.I0(\slv_regs_reg[15]_15 [15]),
        .I1(\slv_regs_reg[14]_14 [15]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [15]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [15]),
        .O(\axi_rdata[15]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[16]_i_4 
       (.I0(b_ram_data_reg_1[16]),
        .I1(b_ram_data_reg_0[16]),
        .I2(sel0[1]),
        .I3(Q[16]),
        .I4(sel0[0]),
        .O(\axi_rdata[16]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_5 
       (.I0(\slv_regs_reg[7]_7 [16]),
        .I1(\slv_regs_reg[6]_6 [16]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [16]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[16]),
        .O(\axi_rdata[16]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_6 
       (.I0(\slv_regs_reg[11]_11 [16]),
        .I1(\slv_regs_reg[10]_10 [16]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [16]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [16]),
        .O(\axi_rdata[16]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_7 
       (.I0(\slv_regs_reg[15]_15 [16]),
        .I1(\slv_regs_reg[14]_14 [16]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [16]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [16]),
        .O(\axi_rdata[16]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[17]_i_4 
       (.I0(b_ram_data_reg_1[17]),
        .I1(b_ram_data_reg_0[17]),
        .I2(sel0[1]),
        .I3(Q[17]),
        .I4(sel0[0]),
        .O(\axi_rdata[17]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_5 
       (.I0(\slv_regs_reg[7]_7 [17]),
        .I1(\slv_regs_reg[6]_6 [17]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [17]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[17]),
        .O(\axi_rdata[17]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_6 
       (.I0(\slv_regs_reg[11]_11 [17]),
        .I1(\slv_regs_reg[10]_10 [17]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [17]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [17]),
        .O(\axi_rdata[17]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_7 
       (.I0(\slv_regs_reg[15]_15 [17]),
        .I1(\slv_regs_reg[14]_14 [17]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [17]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [17]),
        .O(\axi_rdata[17]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[18]_i_4 
       (.I0(b_ram_data_reg_1[18]),
        .I1(b_ram_data_reg_0[18]),
        .I2(sel0[1]),
        .I3(Q[18]),
        .I4(sel0[0]),
        .O(\axi_rdata[18]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_5 
       (.I0(\slv_regs_reg[7]_7 [18]),
        .I1(\slv_regs_reg[6]_6 [18]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [18]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[18]),
        .O(\axi_rdata[18]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_6 
       (.I0(\slv_regs_reg[11]_11 [18]),
        .I1(\slv_regs_reg[10]_10 [18]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [18]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [18]),
        .O(\axi_rdata[18]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_7 
       (.I0(\slv_regs_reg[15]_15 [18]),
        .I1(\slv_regs_reg[14]_14 [18]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [18]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [18]),
        .O(\axi_rdata[18]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[19]_i_4 
       (.I0(b_ram_data_reg_1[19]),
        .I1(b_ram_data_reg_0[19]),
        .I2(sel0[1]),
        .I3(Q[19]),
        .I4(sel0[0]),
        .O(\axi_rdata[19]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_5 
       (.I0(\slv_regs_reg[7]_7 [19]),
        .I1(\slv_regs_reg[6]_6 [19]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [19]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[19]),
        .O(\axi_rdata[19]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_6 
       (.I0(\slv_regs_reg[11]_11 [19]),
        .I1(\slv_regs_reg[10]_10 [19]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [19]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [19]),
        .O(\axi_rdata[19]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_7 
       (.I0(\slv_regs_reg[15]_15 [19]),
        .I1(\slv_regs_reg[14]_14 [19]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [19]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [19]),
        .O(\axi_rdata[19]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[1]_i_4 
       (.I0(b_ram_data_reg_1[1]),
        .I1(b_ram_data_reg_0[1]),
        .I2(sel0[1]),
        .I3(Q[1]),
        .I4(sel0[0]),
        .O(\axi_rdata[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_5 
       (.I0(\slv_regs_reg[7]_7 [1]),
        .I1(\slv_regs_reg[6]_6 [1]),
        .I2(sel0[1]),
        .I3(b_ram_data_reg_3[1]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[1]),
        .O(\axi_rdata[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_6 
       (.I0(\slv_regs_reg[11]_11 [1]),
        .I1(\slv_regs_reg[10]_10 [1]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [1]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [1]),
        .O(\axi_rdata[1]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_7 
       (.I0(\slv_regs_reg[15]_15 [1]),
        .I1(\slv_regs_reg[14]_14 [1]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [1]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [1]),
        .O(\axi_rdata[1]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[20]_i_4 
       (.I0(b_ram_data_reg_1[20]),
        .I1(b_ram_data_reg_0[20]),
        .I2(sel0[1]),
        .I3(Q[20]),
        .I4(sel0[0]),
        .O(\axi_rdata[20]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_5 
       (.I0(\slv_regs_reg[7]_7 [20]),
        .I1(\slv_regs_reg[6]_6 [20]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [20]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[20]),
        .O(\axi_rdata[20]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_6 
       (.I0(\slv_regs_reg[11]_11 [20]),
        .I1(\slv_regs_reg[10]_10 [20]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [20]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [20]),
        .O(\axi_rdata[20]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_7 
       (.I0(\slv_regs_reg[15]_15 [20]),
        .I1(\slv_regs_reg[14]_14 [20]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [20]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [20]),
        .O(\axi_rdata[20]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[21]_i_4 
       (.I0(b_ram_data_reg_1[21]),
        .I1(b_ram_data_reg_0[21]),
        .I2(sel0[1]),
        .I3(Q[21]),
        .I4(sel0[0]),
        .O(\axi_rdata[21]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_5 
       (.I0(\slv_regs_reg[7]_7 [21]),
        .I1(\slv_regs_reg[6]_6 [21]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [21]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[21]),
        .O(\axi_rdata[21]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_6 
       (.I0(\slv_regs_reg[11]_11 [21]),
        .I1(\slv_regs_reg[10]_10 [21]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [21]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [21]),
        .O(\axi_rdata[21]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_7 
       (.I0(\slv_regs_reg[15]_15 [21]),
        .I1(\slv_regs_reg[14]_14 [21]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [21]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [21]),
        .O(\axi_rdata[21]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[22]_i_4 
       (.I0(b_ram_data_reg_1[22]),
        .I1(b_ram_data_reg_0[22]),
        .I2(sel0[1]),
        .I3(Q[22]),
        .I4(sel0[0]),
        .O(\axi_rdata[22]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_5 
       (.I0(\slv_regs_reg[7]_7 [22]),
        .I1(\slv_regs_reg[6]_6 [22]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [22]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[22]),
        .O(\axi_rdata[22]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_6 
       (.I0(\slv_regs_reg[11]_11 [22]),
        .I1(\slv_regs_reg[10]_10 [22]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [22]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [22]),
        .O(\axi_rdata[22]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_7 
       (.I0(\slv_regs_reg[15]_15 [22]),
        .I1(\slv_regs_reg[14]_14 [22]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [22]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [22]),
        .O(\axi_rdata[22]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[23]_i_4 
       (.I0(b_ram_data_reg_1[23]),
        .I1(b_ram_data_reg_0[23]),
        .I2(sel0[1]),
        .I3(Q[23]),
        .I4(sel0[0]),
        .O(\axi_rdata[23]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_5 
       (.I0(\slv_regs_reg[7]_7 [23]),
        .I1(\slv_regs_reg[6]_6 [23]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [23]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[23]),
        .O(\axi_rdata[23]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_6 
       (.I0(\slv_regs_reg[11]_11 [23]),
        .I1(\slv_regs_reg[10]_10 [23]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [23]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [23]),
        .O(\axi_rdata[23]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_7 
       (.I0(\slv_regs_reg[15]_15 [23]),
        .I1(\slv_regs_reg[14]_14 [23]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [23]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [23]),
        .O(\axi_rdata[23]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[24]_i_4 
       (.I0(b_ram_data_reg_1[24]),
        .I1(b_ram_data_reg_0[24]),
        .I2(sel0[1]),
        .I3(Q[24]),
        .I4(sel0[0]),
        .O(\axi_rdata[24]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_5 
       (.I0(\slv_regs_reg[7]_7 [24]),
        .I1(\slv_regs_reg[6]_6 [24]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [24]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[24]),
        .O(\axi_rdata[24]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_6 
       (.I0(\slv_regs_reg[11]_11 [24]),
        .I1(\slv_regs_reg[10]_10 [24]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [24]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [24]),
        .O(\axi_rdata[24]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_7 
       (.I0(\slv_regs_reg[15]_15 [24]),
        .I1(\slv_regs_reg[14]_14 [24]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [24]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [24]),
        .O(\axi_rdata[24]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[25]_i_4 
       (.I0(b_ram_data_reg_1[25]),
        .I1(b_ram_data_reg_0[25]),
        .I2(sel0[1]),
        .I3(Q[25]),
        .I4(sel0[0]),
        .O(\axi_rdata[25]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_5 
       (.I0(\slv_regs_reg[7]_7 [25]),
        .I1(\slv_regs_reg[6]_6 [25]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [25]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[25]),
        .O(\axi_rdata[25]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_6 
       (.I0(\slv_regs_reg[11]_11 [25]),
        .I1(\slv_regs_reg[10]_10 [25]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [25]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [25]),
        .O(\axi_rdata[25]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_7 
       (.I0(\slv_regs_reg[15]_15 [25]),
        .I1(\slv_regs_reg[14]_14 [25]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [25]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [25]),
        .O(\axi_rdata[25]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[26]_i_4 
       (.I0(b_ram_data_reg_1[26]),
        .I1(b_ram_data_reg_0[26]),
        .I2(sel0[1]),
        .I3(Q[26]),
        .I4(sel0[0]),
        .O(\axi_rdata[26]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_5 
       (.I0(\slv_regs_reg[7]_7 [26]),
        .I1(\slv_regs_reg[6]_6 [26]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [26]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[26]),
        .O(\axi_rdata[26]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_6 
       (.I0(\slv_regs_reg[11]_11 [26]),
        .I1(\slv_regs_reg[10]_10 [26]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [26]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [26]),
        .O(\axi_rdata[26]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_7 
       (.I0(\slv_regs_reg[15]_15 [26]),
        .I1(\slv_regs_reg[14]_14 [26]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [26]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [26]),
        .O(\axi_rdata[26]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[27]_i_4 
       (.I0(b_ram_data_reg_1[27]),
        .I1(b_ram_data_reg_0[27]),
        .I2(sel0[1]),
        .I3(Q[27]),
        .I4(sel0[0]),
        .O(\axi_rdata[27]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_5 
       (.I0(\slv_regs_reg[7]_7 [27]),
        .I1(\slv_regs_reg[6]_6 [27]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [27]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[27]),
        .O(\axi_rdata[27]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_6 
       (.I0(\slv_regs_reg[11]_11 [27]),
        .I1(\slv_regs_reg[10]_10 [27]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [27]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [27]),
        .O(\axi_rdata[27]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_7 
       (.I0(\slv_regs_reg[15]_15 [27]),
        .I1(\slv_regs_reg[14]_14 [27]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [27]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [27]),
        .O(\axi_rdata[27]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[28]_i_4 
       (.I0(b_ram_data_reg_1[28]),
        .I1(b_ram_data_reg_0[28]),
        .I2(sel0[1]),
        .I3(Q[28]),
        .I4(sel0[0]),
        .O(\axi_rdata[28]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_5 
       (.I0(\slv_regs_reg[7]_7 [28]),
        .I1(\slv_regs_reg[6]_6 [28]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [28]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[28]),
        .O(\axi_rdata[28]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_6 
       (.I0(\slv_regs_reg[11]_11 [28]),
        .I1(\slv_regs_reg[10]_10 [28]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [28]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [28]),
        .O(\axi_rdata[28]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_7 
       (.I0(\slv_regs_reg[15]_15 [28]),
        .I1(\slv_regs_reg[14]_14 [28]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [28]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [28]),
        .O(\axi_rdata[28]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[29]_i_4 
       (.I0(b_ram_data_reg_1[29]),
        .I1(b_ram_data_reg_0[29]),
        .I2(sel0[1]),
        .I3(Q[29]),
        .I4(sel0[0]),
        .O(\axi_rdata[29]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_5 
       (.I0(\slv_regs_reg[7]_7 [29]),
        .I1(\slv_regs_reg[6]_6 [29]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [29]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[29]),
        .O(\axi_rdata[29]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_6 
       (.I0(\slv_regs_reg[11]_11 [29]),
        .I1(\slv_regs_reg[10]_10 [29]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [29]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [29]),
        .O(\axi_rdata[29]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_7 
       (.I0(\slv_regs_reg[15]_15 [29]),
        .I1(\slv_regs_reg[14]_14 [29]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [29]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [29]),
        .O(\axi_rdata[29]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[2]_i_4 
       (.I0(b_ram_data_reg_1[2]),
        .I1(b_ram_data_reg_0[2]),
        .I2(sel0[1]),
        .I3(Q[2]),
        .I4(sel0[0]),
        .O(\axi_rdata[2]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_5 
       (.I0(\slv_regs_reg[7]_7 [2]),
        .I1(\slv_regs_reg[6]_6 [2]),
        .I2(sel0[1]),
        .I3(b_ram_data_reg_3[2]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[2]),
        .O(\axi_rdata[2]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_6 
       (.I0(\slv_regs_reg[11]_11 [2]),
        .I1(\slv_regs_reg[10]_10 [2]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [2]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [2]),
        .O(\axi_rdata[2]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_7 
       (.I0(\slv_regs_reg[15]_15 [2]),
        .I1(\slv_regs_reg[14]_14 [2]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [2]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [2]),
        .O(\axi_rdata[2]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[30]_i_4 
       (.I0(b_ram_data_reg_1[30]),
        .I1(b_ram_data_reg_0[30]),
        .I2(sel0[1]),
        .I3(Q[30]),
        .I4(sel0[0]),
        .O(\axi_rdata[30]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_5 
       (.I0(\slv_regs_reg[7]_7 [30]),
        .I1(\slv_regs_reg[6]_6 [30]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [30]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[30]),
        .O(\axi_rdata[30]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_6 
       (.I0(\slv_regs_reg[11]_11 [30]),
        .I1(\slv_regs_reg[10]_10 [30]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [30]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [30]),
        .O(\axi_rdata[30]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_7 
       (.I0(\slv_regs_reg[15]_15 [30]),
        .I1(\slv_regs_reg[14]_14 [30]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [30]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [30]),
        .O(\axi_rdata[30]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(s_axi_arready),
        .I1(s_axi_arvalid),
        .I2(s_axi_rvalid),
        .O(\axi_rdata[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[31]_i_5 
       (.I0(b_ram_data_reg_1[31]),
        .I1(b_ram_data_reg_0[31]),
        .I2(sel0[1]),
        .I3(Q[31]),
        .I4(sel0[0]),
        .O(\axi_rdata[31]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_6 
       (.I0(\slv_regs_reg[7]_7 [31]),
        .I1(\slv_regs_reg[6]_6 [31]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [31]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[31]),
        .O(\axi_rdata[31]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_7 
       (.I0(\slv_regs_reg[11]_11 [31]),
        .I1(\slv_regs_reg[10]_10 [31]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [31]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [31]),
        .O(\axi_rdata[31]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_8 
       (.I0(\slv_regs_reg[15]_15 [31]),
        .I1(\slv_regs_reg[14]_14 [31]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [31]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [31]),
        .O(\axi_rdata[31]_i_8_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[3]_i_4 
       (.I0(b_ram_data_reg_1[3]),
        .I1(b_ram_data_reg_0[3]),
        .I2(sel0[1]),
        .I3(Q[3]),
        .I4(sel0[0]),
        .O(\axi_rdata[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_5 
       (.I0(\slv_regs_reg[7]_7 [3]),
        .I1(\slv_regs_reg[6]_6 [3]),
        .I2(sel0[1]),
        .I3(b_ram_data_reg_3[3]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[3]),
        .O(\axi_rdata[3]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_6 
       (.I0(\slv_regs_reg[11]_11 [3]),
        .I1(\slv_regs_reg[10]_10 [3]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [3]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [3]),
        .O(\axi_rdata[3]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_7 
       (.I0(\slv_regs_reg[15]_15 [3]),
        .I1(\slv_regs_reg[14]_14 [3]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [3]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [3]),
        .O(\axi_rdata[3]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[4]_i_4 
       (.I0(b_ram_data_reg_1[4]),
        .I1(b_ram_data_reg_0[4]),
        .I2(sel0[1]),
        .I3(Q[4]),
        .I4(sel0[0]),
        .O(\axi_rdata[4]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_5 
       (.I0(\slv_regs_reg[7]_7 [4]),
        .I1(\slv_regs_reg[6]_6 [4]),
        .I2(sel0[1]),
        .I3(b_ram_data_reg_3[4]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[4]),
        .O(\axi_rdata[4]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_6 
       (.I0(\slv_regs_reg[11]_11 [4]),
        .I1(\slv_regs_reg[10]_10 [4]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [4]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [4]),
        .O(\axi_rdata[4]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_7 
       (.I0(\slv_regs_reg[15]_15 [4]),
        .I1(\slv_regs_reg[14]_14 [4]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [4]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [4]),
        .O(\axi_rdata[4]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[5]_i_4 
       (.I0(b_ram_data_reg_1[5]),
        .I1(b_ram_data_reg_0[5]),
        .I2(sel0[1]),
        .I3(Q[5]),
        .I4(sel0[0]),
        .O(\axi_rdata[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_5 
       (.I0(\slv_regs_reg[7]_7 [5]),
        .I1(\slv_regs_reg[6]_6 [5]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [5]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[5]),
        .O(\axi_rdata[5]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_6 
       (.I0(\slv_regs_reg[11]_11 [5]),
        .I1(\slv_regs_reg[10]_10 [5]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [5]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [5]),
        .O(\axi_rdata[5]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_7 
       (.I0(\slv_regs_reg[15]_15 [5]),
        .I1(\slv_regs_reg[14]_14 [5]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [5]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [5]),
        .O(\axi_rdata[5]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[6]_i_4 
       (.I0(b_ram_data_reg_1[6]),
        .I1(b_ram_data_reg_0[6]),
        .I2(sel0[1]),
        .I3(Q[6]),
        .I4(sel0[0]),
        .O(\axi_rdata[6]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_5 
       (.I0(\slv_regs_reg[7]_7 [6]),
        .I1(\slv_regs_reg[6]_6 [6]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [6]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[6]),
        .O(\axi_rdata[6]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_6 
       (.I0(\slv_regs_reg[11]_11 [6]),
        .I1(\slv_regs_reg[10]_10 [6]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [6]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [6]),
        .O(\axi_rdata[6]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_7 
       (.I0(\slv_regs_reg[15]_15 [6]),
        .I1(\slv_regs_reg[14]_14 [6]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [6]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [6]),
        .O(\axi_rdata[6]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[7]_i_4 
       (.I0(b_ram_data_reg_1[7]),
        .I1(b_ram_data_reg_0[7]),
        .I2(sel0[1]),
        .I3(Q[7]),
        .I4(sel0[0]),
        .O(\axi_rdata[7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_5 
       (.I0(\slv_regs_reg[7]_7 [7]),
        .I1(\slv_regs_reg[6]_6 [7]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [7]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[7]),
        .O(\axi_rdata[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_6 
       (.I0(\slv_regs_reg[11]_11 [7]),
        .I1(\slv_regs_reg[10]_10 [7]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [7]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [7]),
        .O(\axi_rdata[7]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_7 
       (.I0(\slv_regs_reg[15]_15 [7]),
        .I1(\slv_regs_reg[14]_14 [7]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [7]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [7]),
        .O(\axi_rdata[7]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[8]_i_4 
       (.I0(b_ram_data_reg_1[8]),
        .I1(b_ram_data_reg_0[8]),
        .I2(sel0[1]),
        .I3(Q[8]),
        .I4(sel0[0]),
        .O(\axi_rdata[8]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_5 
       (.I0(\slv_regs_reg[7]_7 [8]),
        .I1(\slv_regs_reg[6]_6 [8]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [8]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[8]),
        .O(\axi_rdata[8]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_6 
       (.I0(\slv_regs_reg[11]_11 [8]),
        .I1(\slv_regs_reg[10]_10 [8]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [8]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [8]),
        .O(\axi_rdata[8]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_7 
       (.I0(\slv_regs_reg[15]_15 [8]),
        .I1(\slv_regs_reg[14]_14 [8]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [8]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [8]),
        .O(\axi_rdata[8]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[9]_i_4 
       (.I0(b_ram_data_reg_1[9]),
        .I1(b_ram_data_reg_0[9]),
        .I2(sel0[1]),
        .I3(Q[9]),
        .I4(sel0[0]),
        .O(\axi_rdata[9]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_5 
       (.I0(\slv_regs_reg[7]_7 [9]),
        .I1(\slv_regs_reg[6]_6 [9]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_5 [9]),
        .I4(sel0[0]),
        .I5(b_ram_data_reg_2[9]),
        .O(\axi_rdata[9]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_6 
       (.I0(\slv_regs_reg[11]_11 [9]),
        .I1(\slv_regs_reg[10]_10 [9]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_9 [9]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_8 [9]),
        .O(\axi_rdata[9]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_7 
       (.I0(\slv_regs_reg[15]_15 [9]),
        .I1(\slv_regs_reg[14]_14 [9]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_13 [9]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_12 [9]),
        .O(\axi_rdata[9]_i_7_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [0]),
        .Q(s_axi_rdata[0]),
        .R(SR));
  MUXF8 \axi_rdata_reg[0]_i_1 
       (.I0(\axi_rdata_reg[0]_i_2_n_0 ),
        .I1(\axi_rdata_reg[0]_i_3_n_0 ),
        .O(\read_data[0]_21 [0]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[0]_i_2 
       (.I0(\axi_rdata[0]_i_4_n_0 ),
        .I1(\axi_rdata[0]_i_5_n_0 ),
        .O(\axi_rdata_reg[0]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[0]_i_3 
       (.I0(\axi_rdata[0]_i_6_n_0 ),
        .I1(\axi_rdata[0]_i_7_n_0 ),
        .O(\axi_rdata_reg[0]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[10] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [10]),
        .Q(s_axi_rdata[10]),
        .R(SR));
  MUXF8 \axi_rdata_reg[10]_i_1 
       (.I0(\axi_rdata_reg[10]_i_2_n_0 ),
        .I1(\axi_rdata_reg[10]_i_3_n_0 ),
        .O(\read_data[0]_21 [10]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[10]_i_2 
       (.I0(\axi_rdata[10]_i_4_n_0 ),
        .I1(\axi_rdata[10]_i_5_n_0 ),
        .O(\axi_rdata_reg[10]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[10]_i_3 
       (.I0(\axi_rdata[10]_i_6_n_0 ),
        .I1(\axi_rdata[10]_i_7_n_0 ),
        .O(\axi_rdata_reg[10]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[11] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [11]),
        .Q(s_axi_rdata[11]),
        .R(SR));
  MUXF8 \axi_rdata_reg[11]_i_1 
       (.I0(\axi_rdata_reg[11]_i_2_n_0 ),
        .I1(\axi_rdata_reg[11]_i_3_n_0 ),
        .O(\read_data[0]_21 [11]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[11]_i_2 
       (.I0(\axi_rdata[11]_i_4_n_0 ),
        .I1(\axi_rdata[11]_i_5_n_0 ),
        .O(\axi_rdata_reg[11]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[11]_i_3 
       (.I0(\axi_rdata[11]_i_6_n_0 ),
        .I1(\axi_rdata[11]_i_7_n_0 ),
        .O(\axi_rdata_reg[11]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[12] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [12]),
        .Q(s_axi_rdata[12]),
        .R(SR));
  MUXF8 \axi_rdata_reg[12]_i_1 
       (.I0(\axi_rdata_reg[12]_i_2_n_0 ),
        .I1(\axi_rdata_reg[12]_i_3_n_0 ),
        .O(\read_data[0]_21 [12]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[12]_i_2 
       (.I0(\axi_rdata[12]_i_4_n_0 ),
        .I1(\axi_rdata[12]_i_5_n_0 ),
        .O(\axi_rdata_reg[12]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[12]_i_3 
       (.I0(\axi_rdata[12]_i_6_n_0 ),
        .I1(\axi_rdata[12]_i_7_n_0 ),
        .O(\axi_rdata_reg[12]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[13] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [13]),
        .Q(s_axi_rdata[13]),
        .R(SR));
  MUXF8 \axi_rdata_reg[13]_i_1 
       (.I0(\axi_rdata_reg[13]_i_2_n_0 ),
        .I1(\axi_rdata_reg[13]_i_3_n_0 ),
        .O(\read_data[0]_21 [13]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[13]_i_2 
       (.I0(\axi_rdata[13]_i_4_n_0 ),
        .I1(\axi_rdata[13]_i_5_n_0 ),
        .O(\axi_rdata_reg[13]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[13]_i_3 
       (.I0(\axi_rdata[13]_i_6_n_0 ),
        .I1(\axi_rdata[13]_i_7_n_0 ),
        .O(\axi_rdata_reg[13]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[14] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [14]),
        .Q(s_axi_rdata[14]),
        .R(SR));
  MUXF8 \axi_rdata_reg[14]_i_1 
       (.I0(\axi_rdata_reg[14]_i_2_n_0 ),
        .I1(\axi_rdata_reg[14]_i_3_n_0 ),
        .O(\read_data[0]_21 [14]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[14]_i_2 
       (.I0(\axi_rdata[14]_i_4_n_0 ),
        .I1(\axi_rdata[14]_i_5_n_0 ),
        .O(\axi_rdata_reg[14]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[14]_i_3 
       (.I0(\axi_rdata[14]_i_6_n_0 ),
        .I1(\axi_rdata[14]_i_7_n_0 ),
        .O(\axi_rdata_reg[14]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[15] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [15]),
        .Q(s_axi_rdata[15]),
        .R(SR));
  MUXF8 \axi_rdata_reg[15]_i_1 
       (.I0(\axi_rdata_reg[15]_i_2_n_0 ),
        .I1(\axi_rdata_reg[15]_i_3_n_0 ),
        .O(\read_data[0]_21 [15]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[15]_i_2 
       (.I0(\axi_rdata[15]_i_4_n_0 ),
        .I1(\axi_rdata[15]_i_5_n_0 ),
        .O(\axi_rdata_reg[15]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[15]_i_3 
       (.I0(\axi_rdata[15]_i_6_n_0 ),
        .I1(\axi_rdata[15]_i_7_n_0 ),
        .O(\axi_rdata_reg[15]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[16] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [16]),
        .Q(s_axi_rdata[16]),
        .R(SR));
  MUXF8 \axi_rdata_reg[16]_i_1 
       (.I0(\axi_rdata_reg[16]_i_2_n_0 ),
        .I1(\axi_rdata_reg[16]_i_3_n_0 ),
        .O(\read_data[0]_21 [16]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[16]_i_2 
       (.I0(\axi_rdata[16]_i_4_n_0 ),
        .I1(\axi_rdata[16]_i_5_n_0 ),
        .O(\axi_rdata_reg[16]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[16]_i_3 
       (.I0(\axi_rdata[16]_i_6_n_0 ),
        .I1(\axi_rdata[16]_i_7_n_0 ),
        .O(\axi_rdata_reg[16]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[17] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [17]),
        .Q(s_axi_rdata[17]),
        .R(SR));
  MUXF8 \axi_rdata_reg[17]_i_1 
       (.I0(\axi_rdata_reg[17]_i_2_n_0 ),
        .I1(\axi_rdata_reg[17]_i_3_n_0 ),
        .O(\read_data[0]_21 [17]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[17]_i_2 
       (.I0(\axi_rdata[17]_i_4_n_0 ),
        .I1(\axi_rdata[17]_i_5_n_0 ),
        .O(\axi_rdata_reg[17]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[17]_i_3 
       (.I0(\axi_rdata[17]_i_6_n_0 ),
        .I1(\axi_rdata[17]_i_7_n_0 ),
        .O(\axi_rdata_reg[17]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[18] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [18]),
        .Q(s_axi_rdata[18]),
        .R(SR));
  MUXF8 \axi_rdata_reg[18]_i_1 
       (.I0(\axi_rdata_reg[18]_i_2_n_0 ),
        .I1(\axi_rdata_reg[18]_i_3_n_0 ),
        .O(\read_data[0]_21 [18]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[18]_i_2 
       (.I0(\axi_rdata[18]_i_4_n_0 ),
        .I1(\axi_rdata[18]_i_5_n_0 ),
        .O(\axi_rdata_reg[18]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[18]_i_3 
       (.I0(\axi_rdata[18]_i_6_n_0 ),
        .I1(\axi_rdata[18]_i_7_n_0 ),
        .O(\axi_rdata_reg[18]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[19] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [19]),
        .Q(s_axi_rdata[19]),
        .R(SR));
  MUXF8 \axi_rdata_reg[19]_i_1 
       (.I0(\axi_rdata_reg[19]_i_2_n_0 ),
        .I1(\axi_rdata_reg[19]_i_3_n_0 ),
        .O(\read_data[0]_21 [19]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[19]_i_2 
       (.I0(\axi_rdata[19]_i_4_n_0 ),
        .I1(\axi_rdata[19]_i_5_n_0 ),
        .O(\axi_rdata_reg[19]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[19]_i_3 
       (.I0(\axi_rdata[19]_i_6_n_0 ),
        .I1(\axi_rdata[19]_i_7_n_0 ),
        .O(\axi_rdata_reg[19]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[1] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [1]),
        .Q(s_axi_rdata[1]),
        .R(SR));
  MUXF8 \axi_rdata_reg[1]_i_1 
       (.I0(\axi_rdata_reg[1]_i_2_n_0 ),
        .I1(\axi_rdata_reg[1]_i_3_n_0 ),
        .O(\read_data[0]_21 [1]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[1]_i_2 
       (.I0(\axi_rdata[1]_i_4_n_0 ),
        .I1(\axi_rdata[1]_i_5_n_0 ),
        .O(\axi_rdata_reg[1]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[1]_i_3 
       (.I0(\axi_rdata[1]_i_6_n_0 ),
        .I1(\axi_rdata[1]_i_7_n_0 ),
        .O(\axi_rdata_reg[1]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[20] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [20]),
        .Q(s_axi_rdata[20]),
        .R(SR));
  MUXF8 \axi_rdata_reg[20]_i_1 
       (.I0(\axi_rdata_reg[20]_i_2_n_0 ),
        .I1(\axi_rdata_reg[20]_i_3_n_0 ),
        .O(\read_data[0]_21 [20]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[20]_i_2 
       (.I0(\axi_rdata[20]_i_4_n_0 ),
        .I1(\axi_rdata[20]_i_5_n_0 ),
        .O(\axi_rdata_reg[20]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[20]_i_3 
       (.I0(\axi_rdata[20]_i_6_n_0 ),
        .I1(\axi_rdata[20]_i_7_n_0 ),
        .O(\axi_rdata_reg[20]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[21] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [21]),
        .Q(s_axi_rdata[21]),
        .R(SR));
  MUXF8 \axi_rdata_reg[21]_i_1 
       (.I0(\axi_rdata_reg[21]_i_2_n_0 ),
        .I1(\axi_rdata_reg[21]_i_3_n_0 ),
        .O(\read_data[0]_21 [21]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[21]_i_2 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(\axi_rdata[21]_i_5_n_0 ),
        .O(\axi_rdata_reg[21]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[21]_i_3 
       (.I0(\axi_rdata[21]_i_6_n_0 ),
        .I1(\axi_rdata[21]_i_7_n_0 ),
        .O(\axi_rdata_reg[21]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[22] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [22]),
        .Q(s_axi_rdata[22]),
        .R(SR));
  MUXF8 \axi_rdata_reg[22]_i_1 
       (.I0(\axi_rdata_reg[22]_i_2_n_0 ),
        .I1(\axi_rdata_reg[22]_i_3_n_0 ),
        .O(\read_data[0]_21 [22]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[22]_i_2 
       (.I0(\axi_rdata[22]_i_4_n_0 ),
        .I1(\axi_rdata[22]_i_5_n_0 ),
        .O(\axi_rdata_reg[22]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[22]_i_3 
       (.I0(\axi_rdata[22]_i_6_n_0 ),
        .I1(\axi_rdata[22]_i_7_n_0 ),
        .O(\axi_rdata_reg[22]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[23] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [23]),
        .Q(s_axi_rdata[23]),
        .R(SR));
  MUXF8 \axi_rdata_reg[23]_i_1 
       (.I0(\axi_rdata_reg[23]_i_2_n_0 ),
        .I1(\axi_rdata_reg[23]_i_3_n_0 ),
        .O(\read_data[0]_21 [23]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[23]_i_2 
       (.I0(\axi_rdata[23]_i_4_n_0 ),
        .I1(\axi_rdata[23]_i_5_n_0 ),
        .O(\axi_rdata_reg[23]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[23]_i_3 
       (.I0(\axi_rdata[23]_i_6_n_0 ),
        .I1(\axi_rdata[23]_i_7_n_0 ),
        .O(\axi_rdata_reg[23]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[24] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [24]),
        .Q(s_axi_rdata[24]),
        .R(SR));
  MUXF8 \axi_rdata_reg[24]_i_1 
       (.I0(\axi_rdata_reg[24]_i_2_n_0 ),
        .I1(\axi_rdata_reg[24]_i_3_n_0 ),
        .O(\read_data[0]_21 [24]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[24]_i_2 
       (.I0(\axi_rdata[24]_i_4_n_0 ),
        .I1(\axi_rdata[24]_i_5_n_0 ),
        .O(\axi_rdata_reg[24]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[24]_i_3 
       (.I0(\axi_rdata[24]_i_6_n_0 ),
        .I1(\axi_rdata[24]_i_7_n_0 ),
        .O(\axi_rdata_reg[24]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[25] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [25]),
        .Q(s_axi_rdata[25]),
        .R(SR));
  MUXF8 \axi_rdata_reg[25]_i_1 
       (.I0(\axi_rdata_reg[25]_i_2_n_0 ),
        .I1(\axi_rdata_reg[25]_i_3_n_0 ),
        .O(\read_data[0]_21 [25]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[25]_i_2 
       (.I0(\axi_rdata[25]_i_4_n_0 ),
        .I1(\axi_rdata[25]_i_5_n_0 ),
        .O(\axi_rdata_reg[25]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[25]_i_3 
       (.I0(\axi_rdata[25]_i_6_n_0 ),
        .I1(\axi_rdata[25]_i_7_n_0 ),
        .O(\axi_rdata_reg[25]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[26] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [26]),
        .Q(s_axi_rdata[26]),
        .R(SR));
  MUXF8 \axi_rdata_reg[26]_i_1 
       (.I0(\axi_rdata_reg[26]_i_2_n_0 ),
        .I1(\axi_rdata_reg[26]_i_3_n_0 ),
        .O(\read_data[0]_21 [26]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[26]_i_2 
       (.I0(\axi_rdata[26]_i_4_n_0 ),
        .I1(\axi_rdata[26]_i_5_n_0 ),
        .O(\axi_rdata_reg[26]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[26]_i_3 
       (.I0(\axi_rdata[26]_i_6_n_0 ),
        .I1(\axi_rdata[26]_i_7_n_0 ),
        .O(\axi_rdata_reg[26]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[27] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [27]),
        .Q(s_axi_rdata[27]),
        .R(SR));
  MUXF8 \axi_rdata_reg[27]_i_1 
       (.I0(\axi_rdata_reg[27]_i_2_n_0 ),
        .I1(\axi_rdata_reg[27]_i_3_n_0 ),
        .O(\read_data[0]_21 [27]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[27]_i_2 
       (.I0(\axi_rdata[27]_i_4_n_0 ),
        .I1(\axi_rdata[27]_i_5_n_0 ),
        .O(\axi_rdata_reg[27]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[27]_i_3 
       (.I0(\axi_rdata[27]_i_6_n_0 ),
        .I1(\axi_rdata[27]_i_7_n_0 ),
        .O(\axi_rdata_reg[27]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[28] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [28]),
        .Q(s_axi_rdata[28]),
        .R(SR));
  MUXF8 \axi_rdata_reg[28]_i_1 
       (.I0(\axi_rdata_reg[28]_i_2_n_0 ),
        .I1(\axi_rdata_reg[28]_i_3_n_0 ),
        .O(\read_data[0]_21 [28]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[28]_i_2 
       (.I0(\axi_rdata[28]_i_4_n_0 ),
        .I1(\axi_rdata[28]_i_5_n_0 ),
        .O(\axi_rdata_reg[28]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[28]_i_3 
       (.I0(\axi_rdata[28]_i_6_n_0 ),
        .I1(\axi_rdata[28]_i_7_n_0 ),
        .O(\axi_rdata_reg[28]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[29] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [29]),
        .Q(s_axi_rdata[29]),
        .R(SR));
  MUXF8 \axi_rdata_reg[29]_i_1 
       (.I0(\axi_rdata_reg[29]_i_2_n_0 ),
        .I1(\axi_rdata_reg[29]_i_3_n_0 ),
        .O(\read_data[0]_21 [29]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[29]_i_2 
       (.I0(\axi_rdata[29]_i_4_n_0 ),
        .I1(\axi_rdata[29]_i_5_n_0 ),
        .O(\axi_rdata_reg[29]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[29]_i_3 
       (.I0(\axi_rdata[29]_i_6_n_0 ),
        .I1(\axi_rdata[29]_i_7_n_0 ),
        .O(\axi_rdata_reg[29]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[2] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [2]),
        .Q(s_axi_rdata[2]),
        .R(SR));
  MUXF8 \axi_rdata_reg[2]_i_1 
       (.I0(\axi_rdata_reg[2]_i_2_n_0 ),
        .I1(\axi_rdata_reg[2]_i_3_n_0 ),
        .O(\read_data[0]_21 [2]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[2]_i_2 
       (.I0(\axi_rdata[2]_i_4_n_0 ),
        .I1(\axi_rdata[2]_i_5_n_0 ),
        .O(\axi_rdata_reg[2]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[2]_i_3 
       (.I0(\axi_rdata[2]_i_6_n_0 ),
        .I1(\axi_rdata[2]_i_7_n_0 ),
        .O(\axi_rdata_reg[2]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[30] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [30]),
        .Q(s_axi_rdata[30]),
        .R(SR));
  MUXF8 \axi_rdata_reg[30]_i_1 
       (.I0(\axi_rdata_reg[30]_i_2_n_0 ),
        .I1(\axi_rdata_reg[30]_i_3_n_0 ),
        .O(\read_data[0]_21 [30]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[30]_i_2 
       (.I0(\axi_rdata[30]_i_4_n_0 ),
        .I1(\axi_rdata[30]_i_5_n_0 ),
        .O(\axi_rdata_reg[30]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[30]_i_3 
       (.I0(\axi_rdata[30]_i_6_n_0 ),
        .I1(\axi_rdata[30]_i_7_n_0 ),
        .O(\axi_rdata_reg[30]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[31] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [31]),
        .Q(s_axi_rdata[31]),
        .R(SR));
  MUXF8 \axi_rdata_reg[31]_i_2 
       (.I0(\axi_rdata_reg[31]_i_3_n_0 ),
        .I1(\axi_rdata_reg[31]_i_4_n_0 ),
        .O(\read_data[0]_21 [31]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[31]_i_3 
       (.I0(\axi_rdata[31]_i_5_n_0 ),
        .I1(\axi_rdata[31]_i_6_n_0 ),
        .O(\axi_rdata_reg[31]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[31]_i_4 
       (.I0(\axi_rdata[31]_i_7_n_0 ),
        .I1(\axi_rdata[31]_i_8_n_0 ),
        .O(\axi_rdata_reg[31]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[3] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [3]),
        .Q(s_axi_rdata[3]),
        .R(SR));
  MUXF8 \axi_rdata_reg[3]_i_1 
       (.I0(\axi_rdata_reg[3]_i_2_n_0 ),
        .I1(\axi_rdata_reg[3]_i_3_n_0 ),
        .O(\read_data[0]_21 [3]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[3]_i_2 
       (.I0(\axi_rdata[3]_i_4_n_0 ),
        .I1(\axi_rdata[3]_i_5_n_0 ),
        .O(\axi_rdata_reg[3]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[3]_i_3 
       (.I0(\axi_rdata[3]_i_6_n_0 ),
        .I1(\axi_rdata[3]_i_7_n_0 ),
        .O(\axi_rdata_reg[3]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[4] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [4]),
        .Q(s_axi_rdata[4]),
        .R(SR));
  MUXF8 \axi_rdata_reg[4]_i_1 
       (.I0(\axi_rdata_reg[4]_i_2_n_0 ),
        .I1(\axi_rdata_reg[4]_i_3_n_0 ),
        .O(\read_data[0]_21 [4]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[4]_i_2 
       (.I0(\axi_rdata[4]_i_4_n_0 ),
        .I1(\axi_rdata[4]_i_5_n_0 ),
        .O(\axi_rdata_reg[4]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[4]_i_3 
       (.I0(\axi_rdata[4]_i_6_n_0 ),
        .I1(\axi_rdata[4]_i_7_n_0 ),
        .O(\axi_rdata_reg[4]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[5] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [5]),
        .Q(s_axi_rdata[5]),
        .R(SR));
  MUXF8 \axi_rdata_reg[5]_i_1 
       (.I0(\axi_rdata_reg[5]_i_2_n_0 ),
        .I1(\axi_rdata_reg[5]_i_3_n_0 ),
        .O(\read_data[0]_21 [5]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[5]_i_2 
       (.I0(\axi_rdata[5]_i_4_n_0 ),
        .I1(\axi_rdata[5]_i_5_n_0 ),
        .O(\axi_rdata_reg[5]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[5]_i_3 
       (.I0(\axi_rdata[5]_i_6_n_0 ),
        .I1(\axi_rdata[5]_i_7_n_0 ),
        .O(\axi_rdata_reg[5]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[6] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [6]),
        .Q(s_axi_rdata[6]),
        .R(SR));
  MUXF8 \axi_rdata_reg[6]_i_1 
       (.I0(\axi_rdata_reg[6]_i_2_n_0 ),
        .I1(\axi_rdata_reg[6]_i_3_n_0 ),
        .O(\read_data[0]_21 [6]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[6]_i_2 
       (.I0(\axi_rdata[6]_i_4_n_0 ),
        .I1(\axi_rdata[6]_i_5_n_0 ),
        .O(\axi_rdata_reg[6]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[6]_i_3 
       (.I0(\axi_rdata[6]_i_6_n_0 ),
        .I1(\axi_rdata[6]_i_7_n_0 ),
        .O(\axi_rdata_reg[6]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[7] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [7]),
        .Q(s_axi_rdata[7]),
        .R(SR));
  MUXF8 \axi_rdata_reg[7]_i_1 
       (.I0(\axi_rdata_reg[7]_i_2_n_0 ),
        .I1(\axi_rdata_reg[7]_i_3_n_0 ),
        .O(\read_data[0]_21 [7]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[7]_i_2 
       (.I0(\axi_rdata[7]_i_4_n_0 ),
        .I1(\axi_rdata[7]_i_5_n_0 ),
        .O(\axi_rdata_reg[7]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[7]_i_3 
       (.I0(\axi_rdata[7]_i_6_n_0 ),
        .I1(\axi_rdata[7]_i_7_n_0 ),
        .O(\axi_rdata_reg[7]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[8] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [8]),
        .Q(s_axi_rdata[8]),
        .R(SR));
  MUXF8 \axi_rdata_reg[8]_i_1 
       (.I0(\axi_rdata_reg[8]_i_2_n_0 ),
        .I1(\axi_rdata_reg[8]_i_3_n_0 ),
        .O(\read_data[0]_21 [8]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[8]_i_2 
       (.I0(\axi_rdata[8]_i_4_n_0 ),
        .I1(\axi_rdata[8]_i_5_n_0 ),
        .O(\axi_rdata_reg[8]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[8]_i_3 
       (.I0(\axi_rdata[8]_i_6_n_0 ),
        .I1(\axi_rdata[8]_i_7_n_0 ),
        .O(\axi_rdata_reg[8]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[9] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_21 [9]),
        .Q(s_axi_rdata[9]),
        .R(SR));
  MUXF8 \axi_rdata_reg[9]_i_1 
       (.I0(\axi_rdata_reg[9]_i_2_n_0 ),
        .I1(\axi_rdata_reg[9]_i_3_n_0 ),
        .O(\read_data[0]_21 [9]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[9]_i_2 
       (.I0(\axi_rdata[9]_i_4_n_0 ),
        .I1(\axi_rdata[9]_i_5_n_0 ),
        .O(\axi_rdata_reg[9]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[9]_i_3 
       (.I0(\axi_rdata[9]_i_6_n_0 ),
        .I1(\axi_rdata[9]_i_7_n_0 ),
        .O(\axi_rdata_reg[9]_i_3_n_0 ),
        .S(sel0[2]));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s_axi_arvalid),
        .I1(s_axi_arready),
        .I2(s_axi_rvalid),
        .I3(s_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s_axi_rvalid),
        .R(SR));
  LUT3 #(
    .INIT(8'h08)) 
    axi_wready_i_1
       (.I0(s_axi_awvalid),
        .I1(s_axi_wvalid),
        .I2(s_axi_wready),
        .O(axi_wready_i_1_n_0));
  FDRE axi_wready_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_wready_i_1_n_0),
        .Q(s_axi_wready),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \in_handshakes[0]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(\slv_regs_reg[0]__0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    \in_handshakes[1]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(\in_handshakes[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    \in_handshakes[2]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(p_0_in[0]),
        .O(\in_handshakes[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h1000)) 
    \in_handshakes[3]_i_1 
       (.I0(p_0_in[3]),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(\in_handshakes[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h2AAAAAAA)) 
    \in_handshakes[4]_i_1 
       (.I0(aresetn),
        .I1(s_axi_awvalid),
        .I2(s_axi_awready),
        .I3(s_axi_wvalid),
        .I4(s_axi_wready),
        .O(\in_handshakes[4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0002)) 
    \in_handshakes[4]_i_2 
       (.I0(p_0_in[2]),
        .I1(p_0_in[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(\in_handshakes[4]_i_2_n_0 ));
  FDRE \in_handshakes_reg[0] 
       (.C(clk),
        .CE(aresetn),
        .D(\slv_regs_reg[0]__0 ),
        .Q(in_handshakes[0]),
        .R(\in_handshakes[4]_i_1_n_0 ));
  FDRE \in_handshakes_reg[1] 
       (.C(clk),
        .CE(aresetn),
        .D(\in_handshakes[1]_i_1_n_0 ),
        .Q(in_handshakes[1]),
        .R(\in_handshakes[4]_i_1_n_0 ));
  FDRE \in_handshakes_reg[2] 
       (.C(clk),
        .CE(aresetn),
        .D(\in_handshakes[2]_i_1_n_0 ),
        .Q(in_handshakes[2]),
        .R(\in_handshakes[4]_i_1_n_0 ));
  FDRE \in_handshakes_reg[3] 
       (.C(clk),
        .CE(aresetn),
        .D(\in_handshakes[3]_i_1_n_0 ),
        .Q(in_handshakes[3]),
        .R(\in_handshakes[4]_i_1_n_0 ));
  FDRE \in_handshakes_reg[4] 
       (.C(clk),
        .CE(aresetn),
        .D(\in_handshakes[4]_i_2_n_0 ),
        .Q(in_handshakes[4]),
        .R(\in_handshakes[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[0]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [0]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [0]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[0]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[0]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[0]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [0]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [0]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [0]),
        .O(\m_axis_tdata[0]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[10]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [10]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [10]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[10]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[10]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[10]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [10]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [10]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [10]),
        .O(\m_axis_tdata[10]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[11]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [11]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [11]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[11]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[11]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[11]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [11]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [11]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [11]),
        .O(\m_axis_tdata[11]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[12]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [12]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [12]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[12]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[12]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[12]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [12]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [12]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [12]),
        .O(\m_axis_tdata[12]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[13]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [13]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [13]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[13]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[13]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[13]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [13]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [13]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [13]),
        .O(\m_axis_tdata[13]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[14]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [14]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [14]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[14]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[14]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[14]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [14]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [14]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [14]),
        .O(\m_axis_tdata[14]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[15]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [15]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [15]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[15]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[15]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[15]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [15]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [15]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [15]),
        .O(\m_axis_tdata[15]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[16]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [16]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [16]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[16]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[16]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[16]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [16]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [16]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [16]),
        .O(\m_axis_tdata[16]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[17]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [17]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [17]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[17]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[17]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[17]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [17]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [17]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [17]),
        .O(\m_axis_tdata[17]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[18]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [18]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [18]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[18]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[18]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[18]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [18]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [18]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [18]),
        .O(\m_axis_tdata[18]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[19]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [19]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [19]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[19]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[19]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[19]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [19]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [19]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [19]),
        .O(\m_axis_tdata[19]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[1]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [1]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [1]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[1]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[1]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[1]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [1]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [1]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [1]),
        .O(\m_axis_tdata[1]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[20]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [20]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [20]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[20]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[20]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[20]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [20]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [20]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [20]),
        .O(\m_axis_tdata[20]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[21]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [21]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [21]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[21]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[21]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[21]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [21]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [21]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [21]),
        .O(\m_axis_tdata[21]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[22]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [22]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [22]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[22]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[22]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[22]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [22]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [22]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [22]),
        .O(\m_axis_tdata[22]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[23]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [23]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [23]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[23]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[23]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[23]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [23]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [23]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [23]),
        .O(\m_axis_tdata[23]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[24]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [24]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [24]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[24]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[24]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[24]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [24]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [24]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [24]),
        .O(\m_axis_tdata[24]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[25]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [25]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [25]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[25]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[25]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[25]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [25]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [25]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [25]),
        .O(\m_axis_tdata[25]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[26]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [26]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [26]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[26]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[26]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[26]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [26]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [26]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [26]),
        .O(\m_axis_tdata[26]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[27]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [27]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [27]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[27]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[27]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[27]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [27]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [27]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [27]),
        .O(\m_axis_tdata[27]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[28]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [28]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [28]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[28]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[28]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[28]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [28]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [28]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [28]),
        .O(\m_axis_tdata[28]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[29]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [29]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [29]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[29]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[29]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[29]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [29]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [29]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [29]),
        .O(\m_axis_tdata[29]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[2]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [2]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [2]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[2]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[2]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[2]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [2]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [2]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [2]),
        .O(\m_axis_tdata[2]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[30]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [30]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [30]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[30]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[30]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[30]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [30]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [30]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [30]),
        .O(\m_axis_tdata[30]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[31]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [31]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [31]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[31]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[31]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[31]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [31]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [31]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [31]),
        .O(\m_axis_tdata[31]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[3]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [3]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [3]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[3]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[3]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[3]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [3]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [3]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [3]),
        .O(\m_axis_tdata[3]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[4]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [4]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [4]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[4]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[4]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[4]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [4]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [4]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [4]),
        .O(\m_axis_tdata[4]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[5]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [5]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [5]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[5]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[5]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[5]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [5]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [5]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [5]),
        .O(\m_axis_tdata[5]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[6]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [6]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [6]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[6]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[6]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[6]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [6]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [6]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [6]),
        .O(\m_axis_tdata[6]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[7]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [7]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [7]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[7]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[7]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[7]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [7]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [7]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [7]),
        .O(\m_axis_tdata[7]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[8]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [8]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [8]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[8]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[8]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[8]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [8]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [8]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [8]),
        .O(\m_axis_tdata[8]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAA808080)) 
    \m_axis_tdata[9]_INST_0 
       (.I0(\slv_regs_reg[5][0]_0 ),
        .I1(\data_out_wire[3]_19 [9]),
        .I2(b_ram_data_reg_3[3]),
        .I3(\data_out_wire[4]_20 [9]),
        .I4(b_ram_data_reg_3[4]),
        .I5(\m_axis_tdata[9]_INST_0_i_1_n_0 ),
        .O(m_axis_tdata[9]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \m_axis_tdata[9]_INST_0_i_1 
       (.I0(\data_out_wire[0]_16 [9]),
        .I1(b_ram_data_reg_3[0]),
        .I2(b_ram_data_reg_3[2]),
        .I3(\data_out_wire[2]_18 [9]),
        .I4(b_ram_data_reg_3[1]),
        .I5(\data_out_wire[1]_17 [9]),
        .O(\m_axis_tdata[9]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_regs[0][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[1]),
        .O(p_1_in[15]));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_regs[0][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[2]),
        .O(p_1_in[23]));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_regs[0][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[3]),
        .O(p_1_in[31]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_regs[0][31]_i_2 
       (.I0(s_axi_wready),
        .I1(s_axi_wvalid),
        .I2(s_axi_awready),
        .I3(s_axi_awvalid),
        .O(slv_reg_wren__2));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_regs[0][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[0]),
        .O(p_1_in[7]));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[10][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[10][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[10][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[10][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[10][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[10][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[10][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[10][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[11][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[11][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[11][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[11][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[11][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[11][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[11][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[11][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[12][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[12][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[12][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[12][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[12][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[12][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[12][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[12][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[13][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[13][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[13][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[13][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[13][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[13][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[13][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[13][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[14][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[14][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[14][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[14][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[14][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[14][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[14][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[14][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_regs[15][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[15][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_regs[15][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[15][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_regs[15][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[15][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_regs[15][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[15][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[1][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[1][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[1][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[1][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[1][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[1][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[1][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[1][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[2][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[2][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[2][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[2][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[2][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[2][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[2][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[2][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[3][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(p_0_in[3]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[3][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[3][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(p_0_in[3]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[3][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[3][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(p_0_in[3]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[3][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[3][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(p_0_in[3]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[3][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[4][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[4][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[4][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[4][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[4][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[4][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[4][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[4][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[5][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[5][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[5][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[5][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[5][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[5][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[5][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[5][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[6][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[6][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[6][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[6][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[6][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[6][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[6][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[6][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[7][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[7][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[7][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[7][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[7][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[7][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[7][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[7][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[8][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[8][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[8][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[8][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[8][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[8][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[8][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[8][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[9][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_regs[9][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[9][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_regs[9][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[9][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_regs[9][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[9][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_regs[9][7]_i_1_n_0 ));
  FDRE \slv_regs_reg[0][0] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE \slv_regs_reg[0][10] 
       (.C(clk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[10]),
        .Q(Q[10]),
        .R(SR));
  FDRE \slv_regs_reg[0][11] 
       (.C(clk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[11]),
        .Q(Q[11]),
        .R(SR));
  FDRE \slv_regs_reg[0][12] 
       (.C(clk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[12]),
        .Q(Q[12]),
        .R(SR));
  FDRE \slv_regs_reg[0][13] 
       (.C(clk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[13]),
        .Q(Q[13]),
        .R(SR));
  FDRE \slv_regs_reg[0][14] 
       (.C(clk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[14]),
        .Q(Q[14]),
        .R(SR));
  FDRE \slv_regs_reg[0][15] 
       (.C(clk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[15]),
        .Q(Q[15]),
        .R(SR));
  FDRE \slv_regs_reg[0][16] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[16]),
        .Q(Q[16]),
        .R(SR));
  FDRE \slv_regs_reg[0][17] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[17]),
        .Q(Q[17]),
        .R(SR));
  FDRE \slv_regs_reg[0][18] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[18]),
        .Q(Q[18]),
        .R(SR));
  FDRE \slv_regs_reg[0][19] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[19]),
        .Q(Q[19]),
        .R(SR));
  FDRE \slv_regs_reg[0][1] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE \slv_regs_reg[0][20] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[20]),
        .Q(Q[20]),
        .R(SR));
  FDRE \slv_regs_reg[0][21] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[21]),
        .Q(Q[21]),
        .R(SR));
  FDRE \slv_regs_reg[0][22] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[22]),
        .Q(Q[22]),
        .R(SR));
  FDRE \slv_regs_reg[0][23] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[23]),
        .Q(Q[23]),
        .R(SR));
  FDRE \slv_regs_reg[0][24] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[24]),
        .Q(Q[24]),
        .R(SR));
  FDRE \slv_regs_reg[0][25] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[25]),
        .Q(Q[25]),
        .R(SR));
  FDRE \slv_regs_reg[0][26] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[26]),
        .Q(Q[26]),
        .R(SR));
  FDRE \slv_regs_reg[0][27] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[27]),
        .Q(Q[27]),
        .R(SR));
  FDRE \slv_regs_reg[0][28] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[28]),
        .Q(Q[28]),
        .R(SR));
  FDRE \slv_regs_reg[0][29] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[29]),
        .Q(Q[29]),
        .R(SR));
  FDRE \slv_regs_reg[0][2] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[2]),
        .Q(Q[2]),
        .R(SR));
  FDRE \slv_regs_reg[0][30] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[30]),
        .Q(Q[30]),
        .R(SR));
  FDRE \slv_regs_reg[0][31] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[31]),
        .Q(Q[31]),
        .R(SR));
  FDRE \slv_regs_reg[0][3] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[3]),
        .Q(Q[3]),
        .R(SR));
  FDRE \slv_regs_reg[0][4] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[4]),
        .Q(Q[4]),
        .R(SR));
  FDRE \slv_regs_reg[0][5] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[5]),
        .Q(Q[5]),
        .R(SR));
  FDRE \slv_regs_reg[0][6] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[6]),
        .Q(Q[6]),
        .R(SR));
  FDRE \slv_regs_reg[0][7] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[7]),
        .Q(Q[7]),
        .R(SR));
  FDRE \slv_regs_reg[0][8] 
       (.C(clk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[8]),
        .Q(Q[8]),
        .R(SR));
  FDRE \slv_regs_reg[0][9] 
       (.C(clk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[9]),
        .Q(Q[9]),
        .R(SR));
  FDRE \slv_regs_reg[10][0] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[10]_10 [0]),
        .R(SR));
  FDRE \slv_regs_reg[10][10] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[10]_10 [10]),
        .R(SR));
  FDRE \slv_regs_reg[10][11] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[10]_10 [11]),
        .R(SR));
  FDRE \slv_regs_reg[10][12] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[10]_10 [12]),
        .R(SR));
  FDRE \slv_regs_reg[10][13] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[10]_10 [13]),
        .R(SR));
  FDRE \slv_regs_reg[10][14] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[10]_10 [14]),
        .R(SR));
  FDRE \slv_regs_reg[10][15] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[10]_10 [15]),
        .R(SR));
  FDRE \slv_regs_reg[10][16] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[10]_10 [16]),
        .R(SR));
  FDRE \slv_regs_reg[10][17] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[10]_10 [17]),
        .R(SR));
  FDRE \slv_regs_reg[10][18] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[10]_10 [18]),
        .R(SR));
  FDRE \slv_regs_reg[10][19] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[10]_10 [19]),
        .R(SR));
  FDRE \slv_regs_reg[10][1] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[10]_10 [1]),
        .R(SR));
  FDRE \slv_regs_reg[10][20] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[10]_10 [20]),
        .R(SR));
  FDRE \slv_regs_reg[10][21] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[10]_10 [21]),
        .R(SR));
  FDRE \slv_regs_reg[10][22] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[10]_10 [22]),
        .R(SR));
  FDRE \slv_regs_reg[10][23] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[10]_10 [23]),
        .R(SR));
  FDRE \slv_regs_reg[10][24] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[10]_10 [24]),
        .R(SR));
  FDRE \slv_regs_reg[10][25] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[10]_10 [25]),
        .R(SR));
  FDRE \slv_regs_reg[10][26] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[10]_10 [26]),
        .R(SR));
  FDRE \slv_regs_reg[10][27] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[10]_10 [27]),
        .R(SR));
  FDRE \slv_regs_reg[10][28] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[10]_10 [28]),
        .R(SR));
  FDRE \slv_regs_reg[10][29] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[10]_10 [29]),
        .R(SR));
  FDRE \slv_regs_reg[10][2] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[10]_10 [2]),
        .R(SR));
  FDRE \slv_regs_reg[10][30] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[10]_10 [30]),
        .R(SR));
  FDRE \slv_regs_reg[10][31] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[10]_10 [31]),
        .R(SR));
  FDRE \slv_regs_reg[10][3] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[10]_10 [3]),
        .R(SR));
  FDRE \slv_regs_reg[10][4] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[10]_10 [4]),
        .R(SR));
  FDRE \slv_regs_reg[10][5] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[10]_10 [5]),
        .R(SR));
  FDRE \slv_regs_reg[10][6] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[10]_10 [6]),
        .R(SR));
  FDRE \slv_regs_reg[10][7] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[10]_10 [7]),
        .R(SR));
  FDRE \slv_regs_reg[10][8] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[10]_10 [8]),
        .R(SR));
  FDRE \slv_regs_reg[10][9] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[10]_10 [9]),
        .R(SR));
  FDRE \slv_regs_reg[11][0] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[11]_11 [0]),
        .R(SR));
  FDRE \slv_regs_reg[11][10] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[11]_11 [10]),
        .R(SR));
  FDRE \slv_regs_reg[11][11] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[11]_11 [11]),
        .R(SR));
  FDRE \slv_regs_reg[11][12] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[11]_11 [12]),
        .R(SR));
  FDRE \slv_regs_reg[11][13] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[11]_11 [13]),
        .R(SR));
  FDRE \slv_regs_reg[11][14] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[11]_11 [14]),
        .R(SR));
  FDRE \slv_regs_reg[11][15] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[11]_11 [15]),
        .R(SR));
  FDRE \slv_regs_reg[11][16] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[11]_11 [16]),
        .R(SR));
  FDRE \slv_regs_reg[11][17] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[11]_11 [17]),
        .R(SR));
  FDRE \slv_regs_reg[11][18] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[11]_11 [18]),
        .R(SR));
  FDRE \slv_regs_reg[11][19] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[11]_11 [19]),
        .R(SR));
  FDRE \slv_regs_reg[11][1] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[11]_11 [1]),
        .R(SR));
  FDRE \slv_regs_reg[11][20] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[11]_11 [20]),
        .R(SR));
  FDRE \slv_regs_reg[11][21] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[11]_11 [21]),
        .R(SR));
  FDRE \slv_regs_reg[11][22] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[11]_11 [22]),
        .R(SR));
  FDRE \slv_regs_reg[11][23] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[11]_11 [23]),
        .R(SR));
  FDRE \slv_regs_reg[11][24] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[11]_11 [24]),
        .R(SR));
  FDRE \slv_regs_reg[11][25] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[11]_11 [25]),
        .R(SR));
  FDRE \slv_regs_reg[11][26] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[11]_11 [26]),
        .R(SR));
  FDRE \slv_regs_reg[11][27] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[11]_11 [27]),
        .R(SR));
  FDRE \slv_regs_reg[11][28] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[11]_11 [28]),
        .R(SR));
  FDRE \slv_regs_reg[11][29] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[11]_11 [29]),
        .R(SR));
  FDRE \slv_regs_reg[11][2] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[11]_11 [2]),
        .R(SR));
  FDRE \slv_regs_reg[11][30] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[11]_11 [30]),
        .R(SR));
  FDRE \slv_regs_reg[11][31] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[11]_11 [31]),
        .R(SR));
  FDRE \slv_regs_reg[11][3] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[11]_11 [3]),
        .R(SR));
  FDRE \slv_regs_reg[11][4] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[11]_11 [4]),
        .R(SR));
  FDRE \slv_regs_reg[11][5] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[11]_11 [5]),
        .R(SR));
  FDRE \slv_regs_reg[11][6] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[11]_11 [6]),
        .R(SR));
  FDRE \slv_regs_reg[11][7] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[11]_11 [7]),
        .R(SR));
  FDRE \slv_regs_reg[11][8] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[11]_11 [8]),
        .R(SR));
  FDRE \slv_regs_reg[11][9] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[11]_11 [9]),
        .R(SR));
  FDRE \slv_regs_reg[12][0] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[12]_12 [0]),
        .R(SR));
  FDRE \slv_regs_reg[12][10] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[12]_12 [10]),
        .R(SR));
  FDRE \slv_regs_reg[12][11] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[12]_12 [11]),
        .R(SR));
  FDRE \slv_regs_reg[12][12] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[12]_12 [12]),
        .R(SR));
  FDRE \slv_regs_reg[12][13] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[12]_12 [13]),
        .R(SR));
  FDRE \slv_regs_reg[12][14] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[12]_12 [14]),
        .R(SR));
  FDRE \slv_regs_reg[12][15] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[12]_12 [15]),
        .R(SR));
  FDRE \slv_regs_reg[12][16] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[12]_12 [16]),
        .R(SR));
  FDRE \slv_regs_reg[12][17] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[12]_12 [17]),
        .R(SR));
  FDRE \slv_regs_reg[12][18] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[12]_12 [18]),
        .R(SR));
  FDRE \slv_regs_reg[12][19] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[12]_12 [19]),
        .R(SR));
  FDRE \slv_regs_reg[12][1] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[12]_12 [1]),
        .R(SR));
  FDRE \slv_regs_reg[12][20] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[12]_12 [20]),
        .R(SR));
  FDRE \slv_regs_reg[12][21] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[12]_12 [21]),
        .R(SR));
  FDRE \slv_regs_reg[12][22] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[12]_12 [22]),
        .R(SR));
  FDRE \slv_regs_reg[12][23] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[12]_12 [23]),
        .R(SR));
  FDRE \slv_regs_reg[12][24] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[12]_12 [24]),
        .R(SR));
  FDRE \slv_regs_reg[12][25] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[12]_12 [25]),
        .R(SR));
  FDRE \slv_regs_reg[12][26] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[12]_12 [26]),
        .R(SR));
  FDRE \slv_regs_reg[12][27] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[12]_12 [27]),
        .R(SR));
  FDRE \slv_regs_reg[12][28] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[12]_12 [28]),
        .R(SR));
  FDRE \slv_regs_reg[12][29] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[12]_12 [29]),
        .R(SR));
  FDRE \slv_regs_reg[12][2] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[12]_12 [2]),
        .R(SR));
  FDRE \slv_regs_reg[12][30] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[12]_12 [30]),
        .R(SR));
  FDRE \slv_regs_reg[12][31] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[12]_12 [31]),
        .R(SR));
  FDRE \slv_regs_reg[12][3] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[12]_12 [3]),
        .R(SR));
  FDRE \slv_regs_reg[12][4] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[12]_12 [4]),
        .R(SR));
  FDRE \slv_regs_reg[12][5] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[12]_12 [5]),
        .R(SR));
  FDRE \slv_regs_reg[12][6] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[12]_12 [6]),
        .R(SR));
  FDRE \slv_regs_reg[12][7] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[12]_12 [7]),
        .R(SR));
  FDRE \slv_regs_reg[12][8] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[12]_12 [8]),
        .R(SR));
  FDRE \slv_regs_reg[12][9] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[12]_12 [9]),
        .R(SR));
  FDRE \slv_regs_reg[13][0] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[13]_13 [0]),
        .R(SR));
  FDRE \slv_regs_reg[13][10] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[13]_13 [10]),
        .R(SR));
  FDRE \slv_regs_reg[13][11] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[13]_13 [11]),
        .R(SR));
  FDRE \slv_regs_reg[13][12] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[13]_13 [12]),
        .R(SR));
  FDRE \slv_regs_reg[13][13] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[13]_13 [13]),
        .R(SR));
  FDRE \slv_regs_reg[13][14] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[13]_13 [14]),
        .R(SR));
  FDRE \slv_regs_reg[13][15] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[13]_13 [15]),
        .R(SR));
  FDRE \slv_regs_reg[13][16] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[13]_13 [16]),
        .R(SR));
  FDRE \slv_regs_reg[13][17] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[13]_13 [17]),
        .R(SR));
  FDRE \slv_regs_reg[13][18] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[13]_13 [18]),
        .R(SR));
  FDRE \slv_regs_reg[13][19] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[13]_13 [19]),
        .R(SR));
  FDRE \slv_regs_reg[13][1] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[13]_13 [1]),
        .R(SR));
  FDRE \slv_regs_reg[13][20] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[13]_13 [20]),
        .R(SR));
  FDRE \slv_regs_reg[13][21] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[13]_13 [21]),
        .R(SR));
  FDRE \slv_regs_reg[13][22] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[13]_13 [22]),
        .R(SR));
  FDRE \slv_regs_reg[13][23] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[13]_13 [23]),
        .R(SR));
  FDRE \slv_regs_reg[13][24] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[13]_13 [24]),
        .R(SR));
  FDRE \slv_regs_reg[13][25] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[13]_13 [25]),
        .R(SR));
  FDRE \slv_regs_reg[13][26] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[13]_13 [26]),
        .R(SR));
  FDRE \slv_regs_reg[13][27] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[13]_13 [27]),
        .R(SR));
  FDRE \slv_regs_reg[13][28] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[13]_13 [28]),
        .R(SR));
  FDRE \slv_regs_reg[13][29] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[13]_13 [29]),
        .R(SR));
  FDRE \slv_regs_reg[13][2] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[13]_13 [2]),
        .R(SR));
  FDRE \slv_regs_reg[13][30] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[13]_13 [30]),
        .R(SR));
  FDRE \slv_regs_reg[13][31] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[13]_13 [31]),
        .R(SR));
  FDRE \slv_regs_reg[13][3] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[13]_13 [3]),
        .R(SR));
  FDRE \slv_regs_reg[13][4] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[13]_13 [4]),
        .R(SR));
  FDRE \slv_regs_reg[13][5] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[13]_13 [5]),
        .R(SR));
  FDRE \slv_regs_reg[13][6] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[13]_13 [6]),
        .R(SR));
  FDRE \slv_regs_reg[13][7] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[13]_13 [7]),
        .R(SR));
  FDRE \slv_regs_reg[13][8] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[13]_13 [8]),
        .R(SR));
  FDRE \slv_regs_reg[13][9] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[13]_13 [9]),
        .R(SR));
  FDRE \slv_regs_reg[14][0] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[14]_14 [0]),
        .R(SR));
  FDRE \slv_regs_reg[14][10] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[14]_14 [10]),
        .R(SR));
  FDRE \slv_regs_reg[14][11] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[14]_14 [11]),
        .R(SR));
  FDRE \slv_regs_reg[14][12] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[14]_14 [12]),
        .R(SR));
  FDRE \slv_regs_reg[14][13] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[14]_14 [13]),
        .R(SR));
  FDRE \slv_regs_reg[14][14] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[14]_14 [14]),
        .R(SR));
  FDRE \slv_regs_reg[14][15] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[14]_14 [15]),
        .R(SR));
  FDRE \slv_regs_reg[14][16] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[14]_14 [16]),
        .R(SR));
  FDRE \slv_regs_reg[14][17] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[14]_14 [17]),
        .R(SR));
  FDRE \slv_regs_reg[14][18] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[14]_14 [18]),
        .R(SR));
  FDRE \slv_regs_reg[14][19] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[14]_14 [19]),
        .R(SR));
  FDRE \slv_regs_reg[14][1] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[14]_14 [1]),
        .R(SR));
  FDRE \slv_regs_reg[14][20] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[14]_14 [20]),
        .R(SR));
  FDRE \slv_regs_reg[14][21] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[14]_14 [21]),
        .R(SR));
  FDRE \slv_regs_reg[14][22] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[14]_14 [22]),
        .R(SR));
  FDRE \slv_regs_reg[14][23] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[14]_14 [23]),
        .R(SR));
  FDRE \slv_regs_reg[14][24] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[14]_14 [24]),
        .R(SR));
  FDRE \slv_regs_reg[14][25] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[14]_14 [25]),
        .R(SR));
  FDRE \slv_regs_reg[14][26] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[14]_14 [26]),
        .R(SR));
  FDRE \slv_regs_reg[14][27] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[14]_14 [27]),
        .R(SR));
  FDRE \slv_regs_reg[14][28] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[14]_14 [28]),
        .R(SR));
  FDRE \slv_regs_reg[14][29] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[14]_14 [29]),
        .R(SR));
  FDRE \slv_regs_reg[14][2] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[14]_14 [2]),
        .R(SR));
  FDRE \slv_regs_reg[14][30] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[14]_14 [30]),
        .R(SR));
  FDRE \slv_regs_reg[14][31] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[14]_14 [31]),
        .R(SR));
  FDRE \slv_regs_reg[14][3] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[14]_14 [3]),
        .R(SR));
  FDRE \slv_regs_reg[14][4] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[14]_14 [4]),
        .R(SR));
  FDRE \slv_regs_reg[14][5] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[14]_14 [5]),
        .R(SR));
  FDRE \slv_regs_reg[14][6] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[14]_14 [6]),
        .R(SR));
  FDRE \slv_regs_reg[14][7] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[14]_14 [7]),
        .R(SR));
  FDRE \slv_regs_reg[14][8] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[14]_14 [8]),
        .R(SR));
  FDRE \slv_regs_reg[14][9] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[14]_14 [9]),
        .R(SR));
  FDRE \slv_regs_reg[15][0] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[15]_15 [0]),
        .R(SR));
  FDRE \slv_regs_reg[15][10] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[15]_15 [10]),
        .R(SR));
  FDRE \slv_regs_reg[15][11] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[15]_15 [11]),
        .R(SR));
  FDRE \slv_regs_reg[15][12] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[15]_15 [12]),
        .R(SR));
  FDRE \slv_regs_reg[15][13] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[15]_15 [13]),
        .R(SR));
  FDRE \slv_regs_reg[15][14] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[15]_15 [14]),
        .R(SR));
  FDRE \slv_regs_reg[15][15] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[15]_15 [15]),
        .R(SR));
  FDRE \slv_regs_reg[15][16] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[15]_15 [16]),
        .R(SR));
  FDRE \slv_regs_reg[15][17] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[15]_15 [17]),
        .R(SR));
  FDRE \slv_regs_reg[15][18] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[15]_15 [18]),
        .R(SR));
  FDRE \slv_regs_reg[15][19] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[15]_15 [19]),
        .R(SR));
  FDRE \slv_regs_reg[15][1] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[15]_15 [1]),
        .R(SR));
  FDRE \slv_regs_reg[15][20] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[15]_15 [20]),
        .R(SR));
  FDRE \slv_regs_reg[15][21] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[15]_15 [21]),
        .R(SR));
  FDRE \slv_regs_reg[15][22] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[15]_15 [22]),
        .R(SR));
  FDRE \slv_regs_reg[15][23] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[15]_15 [23]),
        .R(SR));
  FDRE \slv_regs_reg[15][24] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[15]_15 [24]),
        .R(SR));
  FDRE \slv_regs_reg[15][25] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[15]_15 [25]),
        .R(SR));
  FDRE \slv_regs_reg[15][26] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[15]_15 [26]),
        .R(SR));
  FDRE \slv_regs_reg[15][27] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[15]_15 [27]),
        .R(SR));
  FDRE \slv_regs_reg[15][28] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[15]_15 [28]),
        .R(SR));
  FDRE \slv_regs_reg[15][29] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[15]_15 [29]),
        .R(SR));
  FDRE \slv_regs_reg[15][2] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[15]_15 [2]),
        .R(SR));
  FDRE \slv_regs_reg[15][30] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[15]_15 [30]),
        .R(SR));
  FDRE \slv_regs_reg[15][31] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[15]_15 [31]),
        .R(SR));
  FDRE \slv_regs_reg[15][3] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[15]_15 [3]),
        .R(SR));
  FDRE \slv_regs_reg[15][4] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[15]_15 [4]),
        .R(SR));
  FDRE \slv_regs_reg[15][5] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[15]_15 [5]),
        .R(SR));
  FDRE \slv_regs_reg[15][6] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[15]_15 [6]),
        .R(SR));
  FDRE \slv_regs_reg[15][7] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[15]_15 [7]),
        .R(SR));
  FDRE \slv_regs_reg[15][8] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[15]_15 [8]),
        .R(SR));
  FDRE \slv_regs_reg[15][9] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[15]_15 [9]),
        .R(SR));
  FDRE \slv_regs_reg[1][0] 
       (.C(clk),
        .CE(\slv_regs[1][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(b_ram_data_reg[0]),
        .R(SR));
  FDRE \slv_regs_reg[1][10] 
       (.C(clk),
        .CE(\slv_regs[1][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(b_ram_data_reg[10]),
        .R(SR));
  FDRE \slv_regs_reg[1][11] 
       (.C(clk),
        .CE(\slv_regs[1][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(b_ram_data_reg[11]),
        .R(SR));
  FDRE \slv_regs_reg[1][12] 
       (.C(clk),
        .CE(\slv_regs[1][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(b_ram_data_reg[12]),
        .R(SR));
  FDRE \slv_regs_reg[1][13] 
       (.C(clk),
        .CE(\slv_regs[1][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(b_ram_data_reg[13]),
        .R(SR));
  FDRE \slv_regs_reg[1][14] 
       (.C(clk),
        .CE(\slv_regs[1][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(b_ram_data_reg[14]),
        .R(SR));
  FDRE \slv_regs_reg[1][15] 
       (.C(clk),
        .CE(\slv_regs[1][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(b_ram_data_reg[15]),
        .R(SR));
  FDRE \slv_regs_reg[1][16] 
       (.C(clk),
        .CE(\slv_regs[1][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(b_ram_data_reg[16]),
        .R(SR));
  FDRE \slv_regs_reg[1][17] 
       (.C(clk),
        .CE(\slv_regs[1][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(b_ram_data_reg[17]),
        .R(SR));
  FDRE \slv_regs_reg[1][18] 
       (.C(clk),
        .CE(\slv_regs[1][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(b_ram_data_reg[18]),
        .R(SR));
  FDRE \slv_regs_reg[1][19] 
       (.C(clk),
        .CE(\slv_regs[1][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(b_ram_data_reg[19]),
        .R(SR));
  FDRE \slv_regs_reg[1][1] 
       (.C(clk),
        .CE(\slv_regs[1][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(b_ram_data_reg[1]),
        .R(SR));
  FDRE \slv_regs_reg[1][20] 
       (.C(clk),
        .CE(\slv_regs[1][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(b_ram_data_reg[20]),
        .R(SR));
  FDRE \slv_regs_reg[1][21] 
       (.C(clk),
        .CE(\slv_regs[1][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(b_ram_data_reg[21]),
        .R(SR));
  FDRE \slv_regs_reg[1][22] 
       (.C(clk),
        .CE(\slv_regs[1][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(b_ram_data_reg[22]),
        .R(SR));
  FDRE \slv_regs_reg[1][23] 
       (.C(clk),
        .CE(\slv_regs[1][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(b_ram_data_reg[23]),
        .R(SR));
  FDRE \slv_regs_reg[1][24] 
       (.C(clk),
        .CE(\slv_regs[1][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(b_ram_data_reg[24]),
        .R(SR));
  FDRE \slv_regs_reg[1][25] 
       (.C(clk),
        .CE(\slv_regs[1][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(b_ram_data_reg[25]),
        .R(SR));
  FDRE \slv_regs_reg[1][26] 
       (.C(clk),
        .CE(\slv_regs[1][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(b_ram_data_reg[26]),
        .R(SR));
  FDRE \slv_regs_reg[1][27] 
       (.C(clk),
        .CE(\slv_regs[1][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(b_ram_data_reg[27]),
        .R(SR));
  FDRE \slv_regs_reg[1][28] 
       (.C(clk),
        .CE(\slv_regs[1][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(b_ram_data_reg[28]),
        .R(SR));
  FDRE \slv_regs_reg[1][29] 
       (.C(clk),
        .CE(\slv_regs[1][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(b_ram_data_reg[29]),
        .R(SR));
  FDRE \slv_regs_reg[1][2] 
       (.C(clk),
        .CE(\slv_regs[1][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(b_ram_data_reg[2]),
        .R(SR));
  FDRE \slv_regs_reg[1][30] 
       (.C(clk),
        .CE(\slv_regs[1][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(b_ram_data_reg[30]),
        .R(SR));
  FDRE \slv_regs_reg[1][31] 
       (.C(clk),
        .CE(\slv_regs[1][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(b_ram_data_reg[31]),
        .R(SR));
  FDRE \slv_regs_reg[1][3] 
       (.C(clk),
        .CE(\slv_regs[1][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(b_ram_data_reg[3]),
        .R(SR));
  FDRE \slv_regs_reg[1][4] 
       (.C(clk),
        .CE(\slv_regs[1][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(b_ram_data_reg[4]),
        .R(SR));
  FDRE \slv_regs_reg[1][5] 
       (.C(clk),
        .CE(\slv_regs[1][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(b_ram_data_reg[5]),
        .R(SR));
  FDRE \slv_regs_reg[1][6] 
       (.C(clk),
        .CE(\slv_regs[1][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(b_ram_data_reg[6]),
        .R(SR));
  FDRE \slv_regs_reg[1][7] 
       (.C(clk),
        .CE(\slv_regs[1][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(b_ram_data_reg[7]),
        .R(SR));
  FDRE \slv_regs_reg[1][8] 
       (.C(clk),
        .CE(\slv_regs[1][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(b_ram_data_reg[8]),
        .R(SR));
  FDRE \slv_regs_reg[1][9] 
       (.C(clk),
        .CE(\slv_regs[1][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(b_ram_data_reg[9]),
        .R(SR));
  FDRE \slv_regs_reg[2][0] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(b_ram_data_reg_0[0]),
        .R(SR));
  FDRE \slv_regs_reg[2][10] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(b_ram_data_reg_0[10]),
        .R(SR));
  FDRE \slv_regs_reg[2][11] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(b_ram_data_reg_0[11]),
        .R(SR));
  FDRE \slv_regs_reg[2][12] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(b_ram_data_reg_0[12]),
        .R(SR));
  FDRE \slv_regs_reg[2][13] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(b_ram_data_reg_0[13]),
        .R(SR));
  FDRE \slv_regs_reg[2][14] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(b_ram_data_reg_0[14]),
        .R(SR));
  FDRE \slv_regs_reg[2][15] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(b_ram_data_reg_0[15]),
        .R(SR));
  FDRE \slv_regs_reg[2][16] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(b_ram_data_reg_0[16]),
        .R(SR));
  FDRE \slv_regs_reg[2][17] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(b_ram_data_reg_0[17]),
        .R(SR));
  FDRE \slv_regs_reg[2][18] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(b_ram_data_reg_0[18]),
        .R(SR));
  FDRE \slv_regs_reg[2][19] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(b_ram_data_reg_0[19]),
        .R(SR));
  FDRE \slv_regs_reg[2][1] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(b_ram_data_reg_0[1]),
        .R(SR));
  FDRE \slv_regs_reg[2][20] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(b_ram_data_reg_0[20]),
        .R(SR));
  FDRE \slv_regs_reg[2][21] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(b_ram_data_reg_0[21]),
        .R(SR));
  FDRE \slv_regs_reg[2][22] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(b_ram_data_reg_0[22]),
        .R(SR));
  FDRE \slv_regs_reg[2][23] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(b_ram_data_reg_0[23]),
        .R(SR));
  FDRE \slv_regs_reg[2][24] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(b_ram_data_reg_0[24]),
        .R(SR));
  FDRE \slv_regs_reg[2][25] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(b_ram_data_reg_0[25]),
        .R(SR));
  FDRE \slv_regs_reg[2][26] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(b_ram_data_reg_0[26]),
        .R(SR));
  FDRE \slv_regs_reg[2][27] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(b_ram_data_reg_0[27]),
        .R(SR));
  FDRE \slv_regs_reg[2][28] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(b_ram_data_reg_0[28]),
        .R(SR));
  FDRE \slv_regs_reg[2][29] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(b_ram_data_reg_0[29]),
        .R(SR));
  FDRE \slv_regs_reg[2][2] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(b_ram_data_reg_0[2]),
        .R(SR));
  FDRE \slv_regs_reg[2][30] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(b_ram_data_reg_0[30]),
        .R(SR));
  FDRE \slv_regs_reg[2][31] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(b_ram_data_reg_0[31]),
        .R(SR));
  FDRE \slv_regs_reg[2][3] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(b_ram_data_reg_0[3]),
        .R(SR));
  FDRE \slv_regs_reg[2][4] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(b_ram_data_reg_0[4]),
        .R(SR));
  FDRE \slv_regs_reg[2][5] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(b_ram_data_reg_0[5]),
        .R(SR));
  FDRE \slv_regs_reg[2][6] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(b_ram_data_reg_0[6]),
        .R(SR));
  FDRE \slv_regs_reg[2][7] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(b_ram_data_reg_0[7]),
        .R(SR));
  FDRE \slv_regs_reg[2][8] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(b_ram_data_reg_0[8]),
        .R(SR));
  FDRE \slv_regs_reg[2][9] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(b_ram_data_reg_0[9]),
        .R(SR));
  FDRE \slv_regs_reg[3][0] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(b_ram_data_reg_1[0]),
        .R(SR));
  FDRE \slv_regs_reg[3][10] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(b_ram_data_reg_1[10]),
        .R(SR));
  FDRE \slv_regs_reg[3][11] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(b_ram_data_reg_1[11]),
        .R(SR));
  FDRE \slv_regs_reg[3][12] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(b_ram_data_reg_1[12]),
        .R(SR));
  FDRE \slv_regs_reg[3][13] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(b_ram_data_reg_1[13]),
        .R(SR));
  FDRE \slv_regs_reg[3][14] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(b_ram_data_reg_1[14]),
        .R(SR));
  FDRE \slv_regs_reg[3][15] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(b_ram_data_reg_1[15]),
        .R(SR));
  FDRE \slv_regs_reg[3][16] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(b_ram_data_reg_1[16]),
        .R(SR));
  FDRE \slv_regs_reg[3][17] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(b_ram_data_reg_1[17]),
        .R(SR));
  FDRE \slv_regs_reg[3][18] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(b_ram_data_reg_1[18]),
        .R(SR));
  FDRE \slv_regs_reg[3][19] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(b_ram_data_reg_1[19]),
        .R(SR));
  FDRE \slv_regs_reg[3][1] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(b_ram_data_reg_1[1]),
        .R(SR));
  FDRE \slv_regs_reg[3][20] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(b_ram_data_reg_1[20]),
        .R(SR));
  FDRE \slv_regs_reg[3][21] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(b_ram_data_reg_1[21]),
        .R(SR));
  FDRE \slv_regs_reg[3][22] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(b_ram_data_reg_1[22]),
        .R(SR));
  FDRE \slv_regs_reg[3][23] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(b_ram_data_reg_1[23]),
        .R(SR));
  FDRE \slv_regs_reg[3][24] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(b_ram_data_reg_1[24]),
        .R(SR));
  FDRE \slv_regs_reg[3][25] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(b_ram_data_reg_1[25]),
        .R(SR));
  FDRE \slv_regs_reg[3][26] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(b_ram_data_reg_1[26]),
        .R(SR));
  FDRE \slv_regs_reg[3][27] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(b_ram_data_reg_1[27]),
        .R(SR));
  FDRE \slv_regs_reg[3][28] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(b_ram_data_reg_1[28]),
        .R(SR));
  FDRE \slv_regs_reg[3][29] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(b_ram_data_reg_1[29]),
        .R(SR));
  FDRE \slv_regs_reg[3][2] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(b_ram_data_reg_1[2]),
        .R(SR));
  FDRE \slv_regs_reg[3][30] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(b_ram_data_reg_1[30]),
        .R(SR));
  FDRE \slv_regs_reg[3][31] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(b_ram_data_reg_1[31]),
        .R(SR));
  FDRE \slv_regs_reg[3][3] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(b_ram_data_reg_1[3]),
        .R(SR));
  FDRE \slv_regs_reg[3][4] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(b_ram_data_reg_1[4]),
        .R(SR));
  FDRE \slv_regs_reg[3][5] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(b_ram_data_reg_1[5]),
        .R(SR));
  FDRE \slv_regs_reg[3][6] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(b_ram_data_reg_1[6]),
        .R(SR));
  FDRE \slv_regs_reg[3][7] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(b_ram_data_reg_1[7]),
        .R(SR));
  FDRE \slv_regs_reg[3][8] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(b_ram_data_reg_1[8]),
        .R(SR));
  FDRE \slv_regs_reg[3][9] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(b_ram_data_reg_1[9]),
        .R(SR));
  FDRE \slv_regs_reg[4][0] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(b_ram_data_reg_2[0]),
        .R(SR));
  FDRE \slv_regs_reg[4][10] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(b_ram_data_reg_2[10]),
        .R(SR));
  FDRE \slv_regs_reg[4][11] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(b_ram_data_reg_2[11]),
        .R(SR));
  FDRE \slv_regs_reg[4][12] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(b_ram_data_reg_2[12]),
        .R(SR));
  FDRE \slv_regs_reg[4][13] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(b_ram_data_reg_2[13]),
        .R(SR));
  FDRE \slv_regs_reg[4][14] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(b_ram_data_reg_2[14]),
        .R(SR));
  FDRE \slv_regs_reg[4][15] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(b_ram_data_reg_2[15]),
        .R(SR));
  FDRE \slv_regs_reg[4][16] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(b_ram_data_reg_2[16]),
        .R(SR));
  FDRE \slv_regs_reg[4][17] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(b_ram_data_reg_2[17]),
        .R(SR));
  FDRE \slv_regs_reg[4][18] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(b_ram_data_reg_2[18]),
        .R(SR));
  FDRE \slv_regs_reg[4][19] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(b_ram_data_reg_2[19]),
        .R(SR));
  FDRE \slv_regs_reg[4][1] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(b_ram_data_reg_2[1]),
        .R(SR));
  FDRE \slv_regs_reg[4][20] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(b_ram_data_reg_2[20]),
        .R(SR));
  FDRE \slv_regs_reg[4][21] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(b_ram_data_reg_2[21]),
        .R(SR));
  FDRE \slv_regs_reg[4][22] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(b_ram_data_reg_2[22]),
        .R(SR));
  FDRE \slv_regs_reg[4][23] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(b_ram_data_reg_2[23]),
        .R(SR));
  FDRE \slv_regs_reg[4][24] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(b_ram_data_reg_2[24]),
        .R(SR));
  FDRE \slv_regs_reg[4][25] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(b_ram_data_reg_2[25]),
        .R(SR));
  FDRE \slv_regs_reg[4][26] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(b_ram_data_reg_2[26]),
        .R(SR));
  FDRE \slv_regs_reg[4][27] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(b_ram_data_reg_2[27]),
        .R(SR));
  FDRE \slv_regs_reg[4][28] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(b_ram_data_reg_2[28]),
        .R(SR));
  FDRE \slv_regs_reg[4][29] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(b_ram_data_reg_2[29]),
        .R(SR));
  FDRE \slv_regs_reg[4][2] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(b_ram_data_reg_2[2]),
        .R(SR));
  FDRE \slv_regs_reg[4][30] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(b_ram_data_reg_2[30]),
        .R(SR));
  FDRE \slv_regs_reg[4][31] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(b_ram_data_reg_2[31]),
        .R(SR));
  FDRE \slv_regs_reg[4][3] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(b_ram_data_reg_2[3]),
        .R(SR));
  FDRE \slv_regs_reg[4][4] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(b_ram_data_reg_2[4]),
        .R(SR));
  FDRE \slv_regs_reg[4][5] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(b_ram_data_reg_2[5]),
        .R(SR));
  FDRE \slv_regs_reg[4][6] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(b_ram_data_reg_2[6]),
        .R(SR));
  FDRE \slv_regs_reg[4][7] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(b_ram_data_reg_2[7]),
        .R(SR));
  FDRE \slv_regs_reg[4][8] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(b_ram_data_reg_2[8]),
        .R(SR));
  FDRE \slv_regs_reg[4][9] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(b_ram_data_reg_2[9]),
        .R(SR));
  FDRE \slv_regs_reg[5][0] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(b_ram_data_reg_3[0]),
        .R(SR));
  FDRE \slv_regs_reg[5][10] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[5]_5 [10]),
        .R(SR));
  FDRE \slv_regs_reg[5][11] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[5]_5 [11]),
        .R(SR));
  FDRE \slv_regs_reg[5][12] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[5]_5 [12]),
        .R(SR));
  FDRE \slv_regs_reg[5][13] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[5]_5 [13]),
        .R(SR));
  FDRE \slv_regs_reg[5][14] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[5]_5 [14]),
        .R(SR));
  FDRE \slv_regs_reg[5][15] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[5]_5 [15]),
        .R(SR));
  FDRE \slv_regs_reg[5][16] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[5]_5 [16]),
        .R(SR));
  FDRE \slv_regs_reg[5][17] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[5]_5 [17]),
        .R(SR));
  FDRE \slv_regs_reg[5][18] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[5]_5 [18]),
        .R(SR));
  FDRE \slv_regs_reg[5][19] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[5]_5 [19]),
        .R(SR));
  FDRE \slv_regs_reg[5][1] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(b_ram_data_reg_3[1]),
        .R(SR));
  FDRE \slv_regs_reg[5][20] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[5]_5 [20]),
        .R(SR));
  FDRE \slv_regs_reg[5][21] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[5]_5 [21]),
        .R(SR));
  FDRE \slv_regs_reg[5][22] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[5]_5 [22]),
        .R(SR));
  FDRE \slv_regs_reg[5][23] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[5]_5 [23]),
        .R(SR));
  FDRE \slv_regs_reg[5][24] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[5]_5 [24]),
        .R(SR));
  FDRE \slv_regs_reg[5][25] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[5]_5 [25]),
        .R(SR));
  FDRE \slv_regs_reg[5][26] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[5]_5 [26]),
        .R(SR));
  FDRE \slv_regs_reg[5][27] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[5]_5 [27]),
        .R(SR));
  FDRE \slv_regs_reg[5][28] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[5]_5 [28]),
        .R(SR));
  FDRE \slv_regs_reg[5][29] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[5]_5 [29]),
        .R(SR));
  FDRE \slv_regs_reg[5][2] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(b_ram_data_reg_3[2]),
        .R(SR));
  FDRE \slv_regs_reg[5][30] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[5]_5 [30]),
        .R(SR));
  FDRE \slv_regs_reg[5][31] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[5]_5 [31]),
        .R(SR));
  FDRE \slv_regs_reg[5][3] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(b_ram_data_reg_3[3]),
        .R(SR));
  FDRE \slv_regs_reg[5][4] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(b_ram_data_reg_3[4]),
        .R(SR));
  FDRE \slv_regs_reg[5][5] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[5]_5 [5]),
        .R(SR));
  FDRE \slv_regs_reg[5][6] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[5]_5 [6]),
        .R(SR));
  FDRE \slv_regs_reg[5][7] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[5]_5 [7]),
        .R(SR));
  FDRE \slv_regs_reg[5][8] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[5]_5 [8]),
        .R(SR));
  FDRE \slv_regs_reg[5][9] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[5]_5 [9]),
        .R(SR));
  FDRE \slv_regs_reg[6][0] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[6]_6 [0]),
        .R(SR));
  FDRE \slv_regs_reg[6][10] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[6]_6 [10]),
        .R(SR));
  FDRE \slv_regs_reg[6][11] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[6]_6 [11]),
        .R(SR));
  FDRE \slv_regs_reg[6][12] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[6]_6 [12]),
        .R(SR));
  FDRE \slv_regs_reg[6][13] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[6]_6 [13]),
        .R(SR));
  FDRE \slv_regs_reg[6][14] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[6]_6 [14]),
        .R(SR));
  FDRE \slv_regs_reg[6][15] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[6]_6 [15]),
        .R(SR));
  FDRE \slv_regs_reg[6][16] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[6]_6 [16]),
        .R(SR));
  FDRE \slv_regs_reg[6][17] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[6]_6 [17]),
        .R(SR));
  FDRE \slv_regs_reg[6][18] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[6]_6 [18]),
        .R(SR));
  FDRE \slv_regs_reg[6][19] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[6]_6 [19]),
        .R(SR));
  FDRE \slv_regs_reg[6][1] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[6]_6 [1]),
        .R(SR));
  FDRE \slv_regs_reg[6][20] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[6]_6 [20]),
        .R(SR));
  FDRE \slv_regs_reg[6][21] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[6]_6 [21]),
        .R(SR));
  FDRE \slv_regs_reg[6][22] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[6]_6 [22]),
        .R(SR));
  FDRE \slv_regs_reg[6][23] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[6]_6 [23]),
        .R(SR));
  FDRE \slv_regs_reg[6][24] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[6]_6 [24]),
        .R(SR));
  FDRE \slv_regs_reg[6][25] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[6]_6 [25]),
        .R(SR));
  FDRE \slv_regs_reg[6][26] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[6]_6 [26]),
        .R(SR));
  FDRE \slv_regs_reg[6][27] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[6]_6 [27]),
        .R(SR));
  FDRE \slv_regs_reg[6][28] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[6]_6 [28]),
        .R(SR));
  FDRE \slv_regs_reg[6][29] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[6]_6 [29]),
        .R(SR));
  FDRE \slv_regs_reg[6][2] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[6]_6 [2]),
        .R(SR));
  FDRE \slv_regs_reg[6][30] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[6]_6 [30]),
        .R(SR));
  FDRE \slv_regs_reg[6][31] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[6]_6 [31]),
        .R(SR));
  FDRE \slv_regs_reg[6][3] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[6]_6 [3]),
        .R(SR));
  FDRE \slv_regs_reg[6][4] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[6]_6 [4]),
        .R(SR));
  FDRE \slv_regs_reg[6][5] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[6]_6 [5]),
        .R(SR));
  FDRE \slv_regs_reg[6][6] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[6]_6 [6]),
        .R(SR));
  FDRE \slv_regs_reg[6][7] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[6]_6 [7]),
        .R(SR));
  FDRE \slv_regs_reg[6][8] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[6]_6 [8]),
        .R(SR));
  FDRE \slv_regs_reg[6][9] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[6]_6 [9]),
        .R(SR));
  FDRE \slv_regs_reg[7][0] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[7]_7 [0]),
        .R(SR));
  FDRE \slv_regs_reg[7][10] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[7]_7 [10]),
        .R(SR));
  FDRE \slv_regs_reg[7][11] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[7]_7 [11]),
        .R(SR));
  FDRE \slv_regs_reg[7][12] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[7]_7 [12]),
        .R(SR));
  FDRE \slv_regs_reg[7][13] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[7]_7 [13]),
        .R(SR));
  FDRE \slv_regs_reg[7][14] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[7]_7 [14]),
        .R(SR));
  FDRE \slv_regs_reg[7][15] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[7]_7 [15]),
        .R(SR));
  FDRE \slv_regs_reg[7][16] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[7]_7 [16]),
        .R(SR));
  FDRE \slv_regs_reg[7][17] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[7]_7 [17]),
        .R(SR));
  FDRE \slv_regs_reg[7][18] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[7]_7 [18]),
        .R(SR));
  FDRE \slv_regs_reg[7][19] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[7]_7 [19]),
        .R(SR));
  FDRE \slv_regs_reg[7][1] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[7]_7 [1]),
        .R(SR));
  FDRE \slv_regs_reg[7][20] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[7]_7 [20]),
        .R(SR));
  FDRE \slv_regs_reg[7][21] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[7]_7 [21]),
        .R(SR));
  FDRE \slv_regs_reg[7][22] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[7]_7 [22]),
        .R(SR));
  FDRE \slv_regs_reg[7][23] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[7]_7 [23]),
        .R(SR));
  FDRE \slv_regs_reg[7][24] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[7]_7 [24]),
        .R(SR));
  FDRE \slv_regs_reg[7][25] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[7]_7 [25]),
        .R(SR));
  FDRE \slv_regs_reg[7][26] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[7]_7 [26]),
        .R(SR));
  FDRE \slv_regs_reg[7][27] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[7]_7 [27]),
        .R(SR));
  FDRE \slv_regs_reg[7][28] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[7]_7 [28]),
        .R(SR));
  FDRE \slv_regs_reg[7][29] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[7]_7 [29]),
        .R(SR));
  FDRE \slv_regs_reg[7][2] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[7]_7 [2]),
        .R(SR));
  FDRE \slv_regs_reg[7][30] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[7]_7 [30]),
        .R(SR));
  FDRE \slv_regs_reg[7][31] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[7]_7 [31]),
        .R(SR));
  FDRE \slv_regs_reg[7][3] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[7]_7 [3]),
        .R(SR));
  FDRE \slv_regs_reg[7][4] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[7]_7 [4]),
        .R(SR));
  FDRE \slv_regs_reg[7][5] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[7]_7 [5]),
        .R(SR));
  FDRE \slv_regs_reg[7][6] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[7]_7 [6]),
        .R(SR));
  FDRE \slv_regs_reg[7][7] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[7]_7 [7]),
        .R(SR));
  FDRE \slv_regs_reg[7][8] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[7]_7 [8]),
        .R(SR));
  FDRE \slv_regs_reg[7][9] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[7]_7 [9]),
        .R(SR));
  FDRE \slv_regs_reg[8][0] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[8]_8 [0]),
        .R(SR));
  FDRE \slv_regs_reg[8][10] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[8]_8 [10]),
        .R(SR));
  FDRE \slv_regs_reg[8][11] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[8]_8 [11]),
        .R(SR));
  FDRE \slv_regs_reg[8][12] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[8]_8 [12]),
        .R(SR));
  FDRE \slv_regs_reg[8][13] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[8]_8 [13]),
        .R(SR));
  FDRE \slv_regs_reg[8][14] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[8]_8 [14]),
        .R(SR));
  FDRE \slv_regs_reg[8][15] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[8]_8 [15]),
        .R(SR));
  FDRE \slv_regs_reg[8][16] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[8]_8 [16]),
        .R(SR));
  FDRE \slv_regs_reg[8][17] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[8]_8 [17]),
        .R(SR));
  FDRE \slv_regs_reg[8][18] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[8]_8 [18]),
        .R(SR));
  FDRE \slv_regs_reg[8][19] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[8]_8 [19]),
        .R(SR));
  FDRE \slv_regs_reg[8][1] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[8]_8 [1]),
        .R(SR));
  FDRE \slv_regs_reg[8][20] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[8]_8 [20]),
        .R(SR));
  FDRE \slv_regs_reg[8][21] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[8]_8 [21]),
        .R(SR));
  FDRE \slv_regs_reg[8][22] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[8]_8 [22]),
        .R(SR));
  FDRE \slv_regs_reg[8][23] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[8]_8 [23]),
        .R(SR));
  FDRE \slv_regs_reg[8][24] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[8]_8 [24]),
        .R(SR));
  FDRE \slv_regs_reg[8][25] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[8]_8 [25]),
        .R(SR));
  FDRE \slv_regs_reg[8][26] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[8]_8 [26]),
        .R(SR));
  FDRE \slv_regs_reg[8][27] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[8]_8 [27]),
        .R(SR));
  FDRE \slv_regs_reg[8][28] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[8]_8 [28]),
        .R(SR));
  FDRE \slv_regs_reg[8][29] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[8]_8 [29]),
        .R(SR));
  FDRE \slv_regs_reg[8][2] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[8]_8 [2]),
        .R(SR));
  FDRE \slv_regs_reg[8][30] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[8]_8 [30]),
        .R(SR));
  FDRE \slv_regs_reg[8][31] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[8]_8 [31]),
        .R(SR));
  FDRE \slv_regs_reg[8][3] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[8]_8 [3]),
        .R(SR));
  FDRE \slv_regs_reg[8][4] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[8]_8 [4]),
        .R(SR));
  FDRE \slv_regs_reg[8][5] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[8]_8 [5]),
        .R(SR));
  FDRE \slv_regs_reg[8][6] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[8]_8 [6]),
        .R(SR));
  FDRE \slv_regs_reg[8][7] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[8]_8 [7]),
        .R(SR));
  FDRE \slv_regs_reg[8][8] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[8]_8 [8]),
        .R(SR));
  FDRE \slv_regs_reg[8][9] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[8]_8 [9]),
        .R(SR));
  FDRE \slv_regs_reg[9][0] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(\slv_regs_reg[9]_9 [0]),
        .R(SR));
  FDRE \slv_regs_reg[9][10] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(\slv_regs_reg[9]_9 [10]),
        .R(SR));
  FDRE \slv_regs_reg[9][11] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(\slv_regs_reg[9]_9 [11]),
        .R(SR));
  FDRE \slv_regs_reg[9][12] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(\slv_regs_reg[9]_9 [12]),
        .R(SR));
  FDRE \slv_regs_reg[9][13] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(\slv_regs_reg[9]_9 [13]),
        .R(SR));
  FDRE \slv_regs_reg[9][14] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(\slv_regs_reg[9]_9 [14]),
        .R(SR));
  FDRE \slv_regs_reg[9][15] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(\slv_regs_reg[9]_9 [15]),
        .R(SR));
  FDRE \slv_regs_reg[9][16] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(\slv_regs_reg[9]_9 [16]),
        .R(SR));
  FDRE \slv_regs_reg[9][17] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(\slv_regs_reg[9]_9 [17]),
        .R(SR));
  FDRE \slv_regs_reg[9][18] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(\slv_regs_reg[9]_9 [18]),
        .R(SR));
  FDRE \slv_regs_reg[9][19] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(\slv_regs_reg[9]_9 [19]),
        .R(SR));
  FDRE \slv_regs_reg[9][1] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(\slv_regs_reg[9]_9 [1]),
        .R(SR));
  FDRE \slv_regs_reg[9][20] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(\slv_regs_reg[9]_9 [20]),
        .R(SR));
  FDRE \slv_regs_reg[9][21] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(\slv_regs_reg[9]_9 [21]),
        .R(SR));
  FDRE \slv_regs_reg[9][22] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(\slv_regs_reg[9]_9 [22]),
        .R(SR));
  FDRE \slv_regs_reg[9][23] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(\slv_regs_reg[9]_9 [23]),
        .R(SR));
  FDRE \slv_regs_reg[9][24] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(\slv_regs_reg[9]_9 [24]),
        .R(SR));
  FDRE \slv_regs_reg[9][25] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(\slv_regs_reg[9]_9 [25]),
        .R(SR));
  FDRE \slv_regs_reg[9][26] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(\slv_regs_reg[9]_9 [26]),
        .R(SR));
  FDRE \slv_regs_reg[9][27] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(\slv_regs_reg[9]_9 [27]),
        .R(SR));
  FDRE \slv_regs_reg[9][28] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(\slv_regs_reg[9]_9 [28]),
        .R(SR));
  FDRE \slv_regs_reg[9][29] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(\slv_regs_reg[9]_9 [29]),
        .R(SR));
  FDRE \slv_regs_reg[9][2] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(\slv_regs_reg[9]_9 [2]),
        .R(SR));
  FDRE \slv_regs_reg[9][30] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(\slv_regs_reg[9]_9 [30]),
        .R(SR));
  FDRE \slv_regs_reg[9][31] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(\slv_regs_reg[9]_9 [31]),
        .R(SR));
  FDRE \slv_regs_reg[9][3] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(\slv_regs_reg[9]_9 [3]),
        .R(SR));
  FDRE \slv_regs_reg[9][4] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(\slv_regs_reg[9]_9 [4]),
        .R(SR));
  FDRE \slv_regs_reg[9][5] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(\slv_regs_reg[9]_9 [5]),
        .R(SR));
  FDRE \slv_regs_reg[9][6] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(\slv_regs_reg[9]_9 [6]),
        .R(SR));
  FDRE \slv_regs_reg[9][7] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(\slv_regs_reg[9]_9 [7]),
        .R(SR));
  FDRE \slv_regs_reg[9][8] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(\slv_regs_reg[9]_9 [8]),
        .R(SR));
  FDRE \slv_regs_reg[9][9] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(\slv_regs_reg[9]_9 [9]),
        .R(SR));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
