library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EMSC_tb is
--  Port ( );
end EMSC_tb;

architecture Behavioral of EMSC_tb is

component EMSC_rtl
    generic(
        BIT_WIDTH_RAW : positive := 12;
        BIT_WIDTH_G   : positive := 18;
        NUM_B_RAM     : positive := 5;
        B_RAM_SIZE : integer := 100;
        B_RAM_BIT_WIDTH : integer := 32   
    );
    Port ( 
        clk         :   in std_logic;
        aresetn     :   in std_logic;
        write_bram  :   in std_logic;
        G_size      :   in integer range 0 to 275;
        raw_stream  :   in std_logic_vector(bit_width_raw-1 downto 0);
        b_ram_in    :   in std_logic_vector(bit_width_G-1 downto 0);
        p_out       :   out bus_array(0 to NUM_B_RAM-1);
        p_ready     :   out std_logic
    );
end component;

begin


end Behavioral;
