-------------------------------------------------------------------------------
-- Title       : <Title Block>
-- Project     : Default Project Name
-------------------------------------------------------------------------------
-- File        : _tb.vhd
-- Author      : User Name <user.email@user.company.com>
-- Company     : User Company Name
-- Created     : Tue Apr 10 12:18:39 2018
-- Last update : Tue Apr 10 12:23:16 2018
-- Platform    : Default Part Number
-- Standard    : <VHDL-2008 | VHDL-2002 | VHDL-1993 | VHDL-1987>
-------------------------------------------------------------------------------
-- Copyright (c) 2018 User Company Name
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

-----------------------------------------------------------

entity b_ram_bank_tb is

end entity b_ram_bank_tb;

-----------------------------------------------------------

architecture testbench of b_ram_bank_tb is

	-- Testbench DUT generics as constants
	constant	B_RAM_SIZE      : integer := 100;
    constant    B_RAM_BIT_WIDTH : integer := 32;
    constant    NUM_B_RAM       : integer := 5

	-- Testbench DUT ports as signals
	signal	  clk             :       std_logic;
    signal    aresetn         :       std_logic;
    signal    valid_input     :       std_logic;
    signal    read_enable     :       std_logic;
    signal    G_size          :       integer range 0 to B_RAM_SIZE;
    signal    init            :       std_logic;
    signal    data_in         :       std_logic_vector(B_RAM_BIT_WIDTH-1 downto 0);
    signal    data_out        :       bus_array(0 to NUM_B_RAM-1);


	-- Other constants
	constant C_CLK_PERIOD : real := 10.0e-9; -- NS

begin
	-----------------------------------------------------------
	-- Clocks and Reset
	-----------------------------------------------------------
	CLK_GEN : process
	begin
		clk <= '1';
		wait for C_CLK_PERIOD / 2.0 * (1 SEC);
		clk <= '0';
		wait for C_CLK_PERIOD / 2.0 * (1 SEC);
	end process CLK_GEN;

	RESET_GEN : process
	begin
		reset <= '1',
		         '0' after 20.0*C_CLK_PERIOD * (1 SEC);
		wait;
	end process RESET_GEN;

	-----------------------------------------------------------
	-- Testbench Stimulus
	-----------------------------------------------------------

	-----------------------------------------------------------
	-- Entity Under Test
	-----------------------------------------------------------
    DUT : entity work.b_ram_bank
    Generic map(
            B_RAM_SIZE 			=> B_RAM_SIZE,
            B_RAM_BIT_WIDTH 	=> B_RAM_BIT_WIDTH,
            NUM_B_RAM     		=>   NUM_B_RAM
    )
    port map(
        clk             =>         clk,
        aresetn         =>         aresetn,
        valid_input     =>         valid_input,
        read_enable     =>         read_enable,
        G_size          =>         G_size,
        init            =>         init,
        data_in         =>         data_in_w,
        data_out        =>         data_out(i)
    );

end architecture testbench;