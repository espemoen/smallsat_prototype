library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

-----------------------------------------------------------

entity block_ram_tb is
end entity block_ram_tb;

-----------------------------------------------------------

architecture testbench of block_ram_tb is

	-- Testbench DUT generics as constants


	-- Testbench DUT ports as signals
	signal clk 			: std_logic;
    signal aresetn  	: std_logic;
    signal data_in      : std_logic_vector(31 downto 0);
    signal write_enable : std_logic;
    signal read_enable  : std_logic;
    signal read_address : integer range 0 to 100;
    signal data_out     : std_logic_vector(31 downto 0);

	-- Other constants
	constant C_CLK_PERIOD : real := 10.0e-9; -- NS

begin
	-----------------------------------------------------------
	-- Clocks and Reset
	-----------------------------------------------------------
	CLK_GEN : process
	begin
		clk <= '1';
		wait for C_CLK_PERIOD / 2.0 * (1 SEC);
		clk <= '0';
		wait for C_CLK_PERIOD / 2.0 * (1 SEC);
	end process CLK_GEN;

	RESET_GEN : process
	begin
		aresetn <= '0',
		         '1' after 2.0*C_CLK_PERIOD * (1 SEC);
		wait;
	end process RESET_GEN;

	-----------------------------------------------------------
	-- Testbench Stimulus
	-----------------------------------------------------------
	process
	variable add : std_logic_vector(31 downto 0) := x"00000002";
	begin
	write_enable <= '0';
	for i in 0 to 9 loop
	   wait until rising_edge(clk);
	end loop;
	write_enable <= '1';
	read_enable <= '0';
	data_in <= x"00000001";
	wait until rising_edge(clk);
	for i in 0 to 10 loop
		data_in <= std_logic_vector(signed(data_in) + signed(add));
		wait until rising_edge(clk);
	end loop;
	write_enable <= '0';
	wait until rising_edge(clk);
	read_enable <= '1';
	for i in 0 to 11 loop
            wait for C_CLK_PERIOD * (1 SEC);
            read_address <= read_address + 1;
        end loop;
    wait for C_CLK_PERIOD * (100 SEC);
	end process;

	-----------------------------------------------------------
	-- Entity Under Test
	-----------------------------------------------------------
    DUT : entity work.block_ram
    	port map(
    		clk 	 		=> 		clk,
    		aresetn	 		=> 		aresetn,
    		data_in	 		=> 		data_in,
    		write_enable    => 		write_enable,
    		read_enable	    => 		read_enable,
    		read_address    => 		read_address,
    		data_out	    => 		data_out

    	);

end architecture testbench;