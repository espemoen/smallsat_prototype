library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


-----------------------------------------------------------

entity b_ram_bank_tb is

end entity b_ram_bank_tb;

-----------------------------------------------------------

architecture testbench of b_ram_bank_tb is

	-- Testbench DUT generics as constants
	constant	B_RAM_SIZE      : integer := 100;
    constant    B_RAM_BIT_WIDTH : integer := 32;
    constant    NUM_B_RAM       : integer := 5;

	-- Testbench DUT ports as signals
	signal	  clk             :       std_logic;
    signal    aresetn         :       std_logic;
    signal    valid_input     :       std_logic;
    signal    read_enable     :       std_logic;
    signal    G_size          :       integer range 0 to B_RAM_SIZE;
    signal    init            :       std_logic;
    signal    data_in         :       std_logic_vector(B_RAM_BIT_WIDTH-1 downto 0);
    signal    data_out        :       std_logic_vector(NUM_B_RAM*B_RAM_BIT_WIDTH-1 downto 0);


	-- Other constants
	constant C_CLK_PERIOD : real := 1.0e-9; -- NS

begin
	-----------------------------------------------------------
	-- Clocks and Reset
	-----------------------------------------------------------
	CLK_GEN : process
	begin
		clk <= '1';
		wait for C_CLK_PERIOD / 2.0 * (1 SEC);
		clk <= '0';
		wait for C_CLK_PERIOD / 2.0 * (1 SEC);
	end process CLK_GEN;

	RESET_GEN : process
	begin
		aresetn <= '0',
		         '1' after 15.0*C_CLK_PERIOD * (1 SEC);
		wait;
	end process RESET_GEN;

	-----------------------------------------------------------
	-- Testbench Stimulus
	-----------------------------------------------------------
    process
        variable add : std_logic_vector(31 downto 0) := x"00000001";
    begin
    G_size <= 10;
    for i in 0 to 19 loop
        wait until rising_edge(clk);
    end loop;
    init <= '1';
    valid_input <= '1';
    data_in <= x"00000001";
    wait until rising_edge(clk);
    for i in 0 to 48 loop
        data_in <= std_logic_vector(signed(data_in) + signed(add));
        wait until rising_edge(clk);
    end loop;
    init <= '0';
    for i in 0 to 99 loop
        wait until rising_edge(clk);
    end loop;
    
    read_enable <= '1';
    for i in 0 to 49 loop
            wait until rising_edge(clk);
    end loop;
    
    read_enable <= '0';
    
    for i in 0 to 500 loop
                wait until rising_edge(clk);
    end loop;
    
    end process;
	-----------------------------------------------------------
	-- Entity Under Test
	-----------------------------------------------------------
    DUT : entity work.b_ram_bank
    Generic map(
            B_RAM_SIZE 			=> B_RAM_SIZE,
            B_RAM_BIT_WIDTH 	=> B_RAM_BIT_WIDTH,
            NUM_B_RAM     		=>   NUM_B_RAM
    )
    port map(
        clk             =>         clk,
        aresetn         =>         aresetn,
        valid_input     =>         valid_input,
        read_enable     =>         read_enable,
        G_size          =>         G_size,
        init            =>         init,
        data_in         =>         data_in,
        data_out        =>         data_out
    );

end architecture testbench;