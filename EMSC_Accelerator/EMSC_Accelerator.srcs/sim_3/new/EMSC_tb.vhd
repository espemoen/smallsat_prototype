library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.EMSC_pkg.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EMSC_tb is
--  Port ( );
end EMSC_tb;

-----------------------------------------------------------
architecture testbench of EMSC_tb is
	-- Testbench DUT generics as constants
	constant BIT_WIDTH_RAW : positive := 12;
    constant BIT_WIDTH_G   : positive := 32;
    constant NUM_B_RAM     : positive := 5;
    constant B_RAM_SIZE : integer := 10;
    constant B_RAM_BIT_WIDTH : integer := 32;   

	-- Testbench DUT ports as signals
	signal clk 		    :   std_logic;
	signal aresetn	    :	std_logic;
	signal init_bram   :   std_logic;
	signal G_size	    :   integer range 0 to 275;
	signal raw_stream   :   std_logic_vector(bit_width_raw-1 downto 0);
	signal b_ram_in	    :   std_logic_vector(bit_width_G-1 downto 0);
    signal p_out        :   bus_array(0 to NUM_B_RAM-1);
	signal p_ready      :   std_logic;
	signal initialized  :   std_logic;
	signal enable        :   std_logic;
	-- Other constants
	constant C_CLK_PERIOD : real := 10.0e-9; -- NS
begin
	-----------------------------------------------------------
	-- Clocks and Reset
	-----------------------------------------------------------
	CLK_GEN : process
	begin
		clk <= '1';
		wait for C_CLK_PERIOD / 2.0 * (1 SEC);
		clk <= '0';
		wait for C_CLK_PERIOD / 2.0 * (1 SEC);
	end process CLK_GEN;

	RESET_GEN : process
	begin
		aresetn <= '0',
		         '1' after 3.0*C_CLK_PERIOD * (1 SEC);
		wait;
	end process RESET_GEN;
	-----------------------------------------------------------
	-- Testbench Stimulus
	-----------------------------------------------------------
process-- en
begin
    b_ram_in <= std_logic_vector(to_unsigned(0, b_ram_in'length));
    G_size <= 10;
    wait for 15.0*C_CLK_PERIOD *(1 SEC);
    init_bram <='1';
    wait for 1.0*C_CLK_PERIOD *(1 SEC);
    b_ram_in <= std_logic_vector(to_unsigned(1, b_ram_in'length));
    init_bram <='1';
    wait for 1.0*C_CLK_PERIOD *(1 SEC);
    for i in 0 to 48 loop
        b_ram_in <= std_logic_vector(unsigned(b_ram_in) + 1);
        wait for 1.0*C_CLK_PERIOD *(1 SEC);
    end loop;
    b_ram_in <= (others => '0');
    init_bram <='0';
    enable <= '1';
    --wait for 1.0*C_CLK_PERIOD *(1 SEC);
    raw_stream <= x"001";
    wait for 1.0*C_CLK_PERIOD *(1 SEC);
    for i in 0 to 8 loop
        raw_stream <= std_logic_vector(unsigned(raw_stream) + 1);
        wait for 1.0*C_CLK_PERIOD *(1 SEC);
    end loop;
    raw_stream <= x"00b";
    wait for 1.0*C_CLK_PERIOD *(1 SEC);
    enable <= '0';
    wait for 3.0*C_CLK_PERIOD *(1 SEC);
    enable <= '1';
    wait for 1.0*C_CLK_PERIOD *(1 SEC);
    for i in 0 to 8 loop
        raw_stream <= std_logic_vector(unsigned(raw_stream) + 1);
        wait for 1.0*C_CLK_PERIOD *(1 SEC);
    end loop;
    enable <= '0';
    wait;
end process;


	-----------------------------------------------------------
	-- Entity Under Test
	-----------------------------------------------------------
    dp: entity work.EMSC_rtl
        generic map(
            BIT_WIDTH_RAW   => BIT_WIDTH_RAW,
            BIT_WIDTH_G     => BIT_WIDTH_G,
            NUM_B_RAM       => NUM_B_RAM,
            B_RAM_SIZE      => B_RAM_SIZE,
            B_RAM_BIT_WIDTH => B_RAM_BIT_WIDTH   
        )
        port map(
            clk         =>     clk,
            aresetn     =>     aresetn,
            init_bram  =>      init_bram,
            enable       =>     enable,
            G_size      =>     G_size,
            raw_stream  =>	   raw_stream,
            b_ram_in    =>     b_ram_in,
            p_out       =>     p_out,
            p_ready     =>     p_ready,
            initialized =>     initialized
        );

end architecture testbench;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

package EMSC_pkg is
    type bus_array is array(natural range <>) of std_logic_vector(47 downto 0);
end package EMSC_pkg;