// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Tue Apr  3 12:57:45 2018
// Host        : DESKTOP-0KA73P0 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               d:/MasterOppgave/smallsat_prototype/EMSC_Accelerator/EMSC_Accelerator.srcs/sources_1/ip/c_sub_1/c_sub_1_sim_netlist.v
// Design      : c_sub_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "c_sub_1,c_addsub_v12_0_11,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_11,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module c_sub_1
   (A,
    B,
    CLK,
    CE,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [17:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [47:0]B;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_LOW" *) input CE;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef" *) output [47:0]S;

  wire [17:0]A;
  wire [47:0]B;
  wire CE;
  wire CLK;
  wire [47:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* c_a_type = "0" *) 
  (* c_a_width = "18" *) 
  (* c_add_mode = "1" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "0" *) 
  (* c_b_value = "000000000000000000000000000000000000000000000000" *) 
  (* c_b_width = "48" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "48" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  c_sub_1_c_addsub_v12_0_11 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(CE),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADD_MODE = "1" *) (* C_AINIT_VAL = "0" *) (* C_A_TYPE = "0" *) 
(* C_A_WIDTH = "18" *) (* C_BORROW_LOW = "1" *) (* C_BYPASS_LOW = "0" *) 
(* C_B_CONSTANT = "0" *) (* C_B_TYPE = "0" *) (* C_B_VALUE = "000000000000000000000000000000000000000000000000" *) 
(* C_B_WIDTH = "48" *) (* C_CE_OVERRIDES_BYPASS = "1" *) (* C_CE_OVERRIDES_SCLR = "0" *) 
(* C_HAS_BYPASS = "0" *) (* C_HAS_CE = "1" *) (* C_HAS_C_IN = "0" *) 
(* C_HAS_C_OUT = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) 
(* C_OUT_WIDTH = "48" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "zynq" *) (* ORIG_REF_NAME = "c_addsub_v12_0_11" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module c_sub_1_c_addsub_v12_0_11
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [17:0]A;
  input [47:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [47:0]S;

  wire \<const0> ;
  wire [17:0]A;
  wire [47:0]B;
  wire CE;
  wire CLK;
  wire [47:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* c_a_type = "0" *) 
  (* c_a_width = "18" *) 
  (* c_add_mode = "1" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "0" *) 
  (* c_b_value = "000000000000000000000000000000000000000000000000" *) 
  (* c_b_width = "48" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "48" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  c_sub_1_c_addsub_v12_0_11_viv xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B(B),
        .BYPASS(1'b0),
        .CE(CE),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
iO2Bdkfy0dqqValMR4KhTWXpD0gDQF+kyoly3tZBTZTVs0CbWJ4Owhu4jxMCf8X2gbWR6iweF6Ks
B5dmLHZTDA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
dbcEbgyZfx3YLmYpvjegvD9sRQCV1qBv0GqFBvCakC3SMR/H82zqo5uv5MZldBGUVmNHnxF3Vejx
zSqxUKfTNc90CS6quuoQe0eeq3T5XSdgwbNtjPZKvJuJTmQKT96yB3CfQOz13fGjaLrn/8NBUBBh
I7OEoGGg7ADph9V3vRg=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bD3a4YgAnaoJx9/hljj2C1rODcUhawTVE1gtdPkNj8/YjemaFM6/sF7Q0CXbDJ7a+OBrB5pUgj3O
Vesi4yVmFp+mGmFarftWat5KmZiP3RVWrXwdzMj+f8T7p+lE3iD4njqUxIUz0TsUaNvFeW0xVNNb
OwTEX04nyt5HrU82dltJCclpFxE6yrP9YvI7l328bphwnC63xxk8T3yXwCrvj3VrIYuDT2yMRxrB
TBCv/Fe2f07JQyV73J7+DGAeJG0B1dTHeu48auQT63g1HsYaUXREihEUKgZe70QlOqlPbrr6Quhx
2LXE8LSdCA+FbJ7LlQc/Sgasj3ZYjM5lhEKleQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GCfR7acMSeEtOw1DhZKkUXjh9Uw/vUar7CGDRG9rZcB9NFDtQTltJeuKjFg8eaeKH9HFBMryuX72
/tmzhtFaiSTjr2na4ncL2XV3QRXe7nQaiHdc7cKBcZDvdSSMzOSYcIxLunwLwQTLC7sCvINmlxO1
NXnYzJVL1xb9HP8QVnSYpo1p+gCXcRBZzrOjZjCUnl7F2t3ZZStSGjBEyXVLnV+ouU3+247oJAOa
kC7v+pOtG2ho4KclIg0MGijjPs+jyOFU+b5C+ufQp/zL9GiZ5waCjb/0Y1vkBc9jZKR7YRnv+ASG
ju1uP8oqEXR9742kXRnW4HkMKkCK1MLDgWYdqw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L+AGKmFZ1zoRJFd2cA+zxJhkgQ1R0aEjGQCGRFLNNhLHZXpzGDIjdSLjralBVRJ2rD6UcJutapF5
YaMoV9kphGGG2B07dxBuIimVjOxS3ZQJ7ru59ddfGBxUe9EHrv00Q5hTwoxig0lxqnmjSSnfsDeF
weTIqNnXkG5kqqezKC8a2FvUD5QWQBibhK69OAdmhhIOwZmpfvQKbEKgLX70BzcNlmLnttRL7G+q
XZ3fabZ42+JJHDLiIfveB3Gp2Lf2tzTH1u2xx5aEUr9154pnC9PWIwL3y3VBAT1oHR7ScdoGDOEy
HoYUiDibldOidIeKW0KrTeAIuBNmtM4R0R+RSA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
V5ClnklUs5Wo++EDemG/KeowZlAfqB8SUrvSxPQGrdIwGfUvoCajhuABAWdS/L/pQl7Eyz51aiuw
KzPMrWtQozAEITf1xzvzgKbWZqoi4PQD3rThywFsFq60u8DdvHYM/kEvit0cZVFvG8rAbtlseHLu
0vU1kbrNgxb3bxjOovg=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
cRqAgScIUeXUwYGfCC0XDtpcc+mFNm3p8oTcFdtIU1nnlMagpBMqRm5ELc+m/Yw8jBwvcvt4tUFv
u/ypEEw+y12B+5Pr6SmnLJ+NVB3Q3Eyh4Q/d7p3jReIIsUxrlENpCTi4PVXMKr1B1Htzm8F8mXDq
y2UV+0SC+4yrBIntsdS0S8jPBERhfJhzNC5z38pPHANtM4wGGIUuKxIALLz1aq+2AjLbEgFHNrzw
2bJiDwRSTwrY4Yx2MSzYJk3O+cQBUe8nJDPx+aGEvDzQ4ZdJMNg2z+iaiE7OTaqK492Jb/1jvU0j
wlI+n35s2rrnc9QgfljdOJuueruPuYDi5vTTxA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bN4d7yurMyLzBW+/KtJ5L6nhCnX9Ih1Zzhv5mC1PcIvIPh+BDpcL2GBDD9kRQ8A8jomoCCF/pUet
HBIMayrbdxHJoHZc+1Tz2sG99ROuJ+nXYGM8kMgG0SmLj6bDYA07Kd4StvC2sgtAx9A8+rv7Y14Z
lZcHTjumiV4BF5MC0KLkaJGI3o6i7ho5nnLeVIlOteWpnNAlRF1X0rge57nszkJTJdbCFuASK2pi
kRtvnNY+GKymemut0TBLOl+pj4tm5rMAoHTONx7+r0KFs5WnJLVATPT3TEcjm1AKyb/3ZKNj21IR
j0Ey6CjIlcW4clMih0kvWSChajp2Q1Sn6aIwEw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
OZBP9tV5MOHrSlot6kSpOAn9BNgDoKcwkpgqq+59j3OA1Lc9z74XGzCKEhGNKrRRDfDuTVqVZi36
6g4u2XBTG2OxP29zVGVFYhP4StAaPdpUSG4UxdHy0Z3xaHa1/BtvwTfGCsI7yUGgwZTyb8NLT4Dx
p56X7ALBqmqMgXAP9SaHO/ytS2irfbqhROMuxl3nUnEsEI8FMYM24m8nOAdXElTERabC+BxHXCbd
bp16I50KbAw4E1H6d3w+UYAIuc1zQTz6mWDNbsRhR0MpnauUxwFs3LrqxtEoX6d3H4a+7A0aK1X7
k9jq6y//WaK/j3vprJ7BMW7YYEZmobQDxpk93Q==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 33488)
`pragma protect data_block
iUfTfU/1poOspyyIyI4aJtkPjyTgJsz0nTjSJEAlKbR2jpuBBjyH0dTDbXnCHju+60dsllCplxl4
X2640sCiH02oztWJ0mHvz4LAusY+1VJflXFbrgr330kF+IP1W/fpgYoTDCGWCaYHAuaeAo6o4XaJ
REjtGN79SGb4JAOMSFZ+RzYswi/PhBzdRk6xN6LCUsSBjOB4h7jb0iWfbRuXs2uRi0F/qSNXxqGw
HoIfrr5PzmdaG10qLqEb40BuHsuf9zgqexdlS10B+3Kq0aX2741uAHwRboHgy+C2o/o+aBoQrLs3
Rm0K4SSzmfxQNuP5TtPpK9t+nqCg9msLf8ywv41cQ26C8M1FxVSu4CIFTYq6kXPOtOp3TgYIQmY5
/rVQ3k1KMUU49wWVn0DFJlUmkYUILmtsxlG26n1PmthVQvgawrejnwEWg+pwLimiLLj2Zbk0DGSu
PCysjw4RFUQhvriBL1qtqef4z1ZMJIzHMrDXXog4uAN0b1CZhxnHnEKKVkXgFfnyV2Z4iozzltcB
twFkUk3giJIbWGq6GOT/MbsshQG+mYNSNESjoaJWdgQ+WMLLLVo1ftWGmpJ+Orii3Vd65JPhZZwY
OjmPrJJA5xQ3wyocfKCb1N0qmEWpk2db1AswzHkQW52GQYBXotn47K0RdgZxxHZ5pGkyNg9JZN8k
m5NJz3yLqrk8dRacf7yRjNZPltmkYnQy4vToHW28YtID+MBOhE2r2uY0tQOMtlQvqEshU1DBiDFn
TSsAoFaupYH+vqhmcRfMfPYwGZeE2SyGfovixlvr9UYSep5EZlx+oy02MeQm+BCGTz9v2IywroQ7
cCW4UDSNL88AIuxlTb4Srl3aWpmgI939k3YULxkVTS/58oQ8BtEfTY3Ax3L5a/6w+tmSaB140MI4
GpJwh5/TgVz5YS86gAb9boRgMQRcp+5coBqS56DP8vGfjo+6nfTbppuMcXQrs75luj2nKSXRiwZt
w01c+8CrUxNZhm3ev+W2S5TyQGEwHWlHZu67EMcxyS17zoCPXc9TuQ72TuvHuFLcyMpoisnyigQl
o09/cJuBn3ZsbMMXqjBq63Edgq5m6bW5SEV6U38/kqGpxKSmCETPeD8rvnBIf14PnJHQIKbU41ek
xTLXYLxQGKFXi5FQDEc6jTq38lTFNdJK1Ybn0nj3/Xa+JEGFDqqCk/Y82bRccL8EPj5Ujd2RtfPv
ZqemyqkrCJA8BEWUdqMYc6E8QjhGnvFniZDJvnKvhfJsbjt4qQ5JpuD8/+aK2Elir24dg+YdB2Vt
t075BKCjLWLmtAWJm8ltdVnLcSyy11uARmLtURbeeP6WO/tCOjPqJxOmbxqDD9O7hwcPwT+667Li
8anY1jryNHqeLNdeicq4aoFRl4hOZ3rfSFrKO45tw4bNEbvK8qvydKDy4bp5rNQQ9LgPG8p44ubd
uVNd5wmrv/Khv/kJ8qlXyCIdStOMKrFveSmnRxD9BmvHI6FiSwmzWtRzLyP3ZjWF3Z9p9NWZRIe2
w1CHxg7qGMppvVTPOg0AsQ3fzYKvW+o1/fMPBrr6S4xU/iZxWkCMUVv5HLuwOUx6jmYf7tcCt4EH
1c3x7617MsXHfr66ZsiN0nFgGk6tu5jmhStl3mZsng3n2jiswxHxekN77YCIP/EcuZGgv4emfPpw
EzppRbutB0F3aBaAlUOLCjRkDXRneO6UHSC95XdRvzqFTlynymAumlTe/chm3AOrjYmHvnhxJ1No
i3TjCK5N0/ni5ce5ZuvI3to4Altz+OCI+twx6ciYS5QY9x9LMXs2Mhx4TNr2tBeIXHlq81bxH4oV
dOyb/TOKvJs/kgVwMZxNFjK72PJ34sbqgB/JGrmofHBQ6IMSvFhCOaSeiRPWLY2lGNFLnMQbnbjY
wfRTRW8P0ngW7ltmM47+FswZIqRJUXR/jGZv5q+FZizBlO+sLyJ7kJUaCPKK2RzD5gT941pBcI2b
AxSo26XKtXXzjdOU4Rk31Iu0sHBzPNBmkinZ7hLdrVyyMpRxY3z+jIxF8ReU0alRpGW4He6Gilp0
WUhtn9hV+vD0XyP+qv2bIqkXLzdHOmgVGNCEy4Rf35qY8B+zG/XfeiP3406suGPr0/uAoPXgWD7M
n6bMKlI7wdqNOxgmhL2RiRIg78lpiyveHXMiKhuoCKGm/VkpIGysVGWyTyQ5D77xTk/+5fOPdwTT
t+b0hrzEGnRbvJoy58vHeW/JvrKW4wWFC1SyGELE65RYRqgsqId3rytA3Vg+2cwt35HE/x/foqhi
gLVEGP8iAsbUOPqTXx50FbKqDJWr37Aq41163Hl7Lli11QWeTCAFN7FhLz5FA3HBpB/C50JwWo6H
hQxEjjs+fi2lr7GLO1hLhGN41Zwd+whVeLgBx5nOSv4qgR3bP7gCNK10c0m3j7RRcUecBRyniTg0
hlrRSV4nv0Z+KTQChIaNzX+PwnLNrx/aptLVH9DBeqXclwWAEwFPPYLZiTtUD+Kn0ZxTU1Cme7PZ
eL0mzh5m9HfQ+dGf5E3mCIA574AtPqYikfQML8t+qLDehU8SShzakTCMY6QYSZ5AMz7uj05tRRCH
P2REGnWvLz7U6qdvweTxMFoCr0XPZ6lfTagQN8/YX4xlTG05Sm+WqMAZysPCqR/C667zjEaggRS0
LzSNEUCdMVwC9kF/wdSDdr2YM57P9CTrVohPukpZVlpwOvxgBEoKMl1l9+vGGfwG4GKC44LPVLw0
egenb9MprGQoVsUUZY+PY4hp9BUmwqAReqm56cwCPSMJa5+4Fcnvv5ThlbQbp1vJFixTKSsLGtpO
fk8oBBOSr53dMtoACDtGBTVinP1dCv6mXDUdWqgShmg0rpeaSf1ghNP2pPiIfNqa/F6BCqbhUunV
MLbYE7BAwcDw0HROmkBBUp+6X9y3Ufu+Y63ltdDROxOjtQ27SLVn/802xFmIBL/NXvN/GOrsQHMr
k1x/hZaOKO4bv6zhO+2CAvOEYcgoXbFkrkYq3ovolPEQMPhL0hWiYLEpReLT8A7MitNXPFLQ8b4Z
Lf8T9aOD26pMX1AioTo1BujsMEzQ+wwK+UJd7f9GHEq3nEOYgoyH4SwL2FzBV6pzYaWVPSJ9iXu9
MWSMzcuvbmCId6af+uq7Z9O1b2vf8DL+M4SqKETs6lPWp03raVsXrHYPwNhxJ8VbUO4qiNLFjo43
6lTj1fPqKYBLK/EmLO+i/Efoq9fwfDkGhNsQg6ikhOc3bsiCVAdF0hhnJ1ZkK994kgwbNyzHcxKS
ynYHeqLZOO5aQSZGxen6QWW4+piunKmtF+NR3WPQMlJKce5+avtHNE/rXPaB5moHb7MPTbZ0fwjI
d5CPLrahYElrl9ZxrXNKWuQ/wvE6bB5ehVxiI0LmQwYSEYI+I9ByDl5N9/w1jW16Vd8d76Sr/pXN
TXFw1lRabzWFvUepWTg9JZS5GS2bODlmUQbtUc07dE8Nit49F/xn8zIlsux5XmYq/bs+1t1YjiTf
2sGKRnGqyfC7gGhja5v+qGt3MHj81JppXZtVaYiKcGnUJvtD4FloDzBc8n3iN767gqND9U2/J5r9
P1Glr1cMu7vIIU3VXiarR7wrUIJfGOznJ+XAb4kqr4M46RybyYFVN38WypJ6vbTl8elsZeKF9BIO
g/rMxoY/EXa8boGXlv2SCtmKQJx4FwCCtmCliKpwrw91z9ArjSrtYpRt1B8jn0UlUsSZrJHjugbf
LkPNuCAc8OV3t/ypbpxmBYw/lS20vbkONuYUnit3gXTLjVIWanBRiuHzYaG4rxXrf8SLnIG8VZUP
XydiHIKn4Nz1OylmMy3JouCKvFNpaJpFfotlazw6le+V4i8I1a+WA8rMOjmk+lCaBAgvjGKM+v1f
pN8/MtHCCQITRd99GOCUErX9zGSc5JiX0lq6nqWoDqdBIxWZhkHf13iwL8O/yjsaBn49ZqLkX1Oy
wfv4Yzb/HmdoqjGg8Ud5rG0SfukYRNH+y3/kQeEv64Wo7rCTHCjIZnVSrKtTxpeOM6pUhK1rEB2/
zgkLWfk2OPrJJEYzey4TrJAhRq74Kxd1N3Omr8Uh/40FQT89NOvWGnOeZSW9pkXauOwujvM6CPCm
NCDGUGQktnPVxJXI4IQ0hXOgBUbZTIWdnmTU2hkFwaRZheK2fknbX39AYVPu2IBVLims3ZL0afHe
7g7c7ilXpVJJJRIYJOCChdMywfwhBrRDjCpayfvblASy94DiKkCsxXcze2/SWzcj15/ywgrWLYPD
K61AhMrtAQ5M3DP/pTIWJzMr75QehPSmIBD/AcOwLLR7IUbB9NHApWSjsVo+0eRxNHey4FvbTXVY
Dq/qaDbbTza08cgozfADGQan1iqId9I6utM6mbM+9YYbNWFy/3m1fDYJGiYzkuqGPf4UMQuCFbsk
xNQcZJuM62wEoQ/MsjtYbb9fMQdVv8JuLddG1111yAOFqBIaHhdz3QnG/GNe1KYBO7aqBzRTN9W1
w5gRBRPTEY/9MiBs87Z0BK7bsobKGv1U9iMWJO8q+08YjvIzV0nd3hJzS6lQJ+hLNpaU+8JEZXQI
cPHsRQWfMA4Zzx99nhJcCbjBze36mVIGuDSYIUkocb7LoI6ijS3BUJDTRdiz9WFJtPv+QxOecXDW
Y9LH0ytCEsnvFI/Uc8xS0kpvKIjBovjsTMisDhqd7kR1P6fBjLEag6bu40N8s6wT2MAfILvB1nUe
8vtt6cWOXkTDvPosTq340S+4hdL4AJGbKvD7+kcUjrj1ijlDuMdgD/+BjvJlf+CkxTTAN4cXy8Jt
9Evp8PknmgW2NGyIgOQUT8Cmhl9qd0vGKHbwADd+BcLjDKNbT41oHn3T34TOuWsbtwalJJDtV1wP
SXbFOuB6Musz2+sFEGbTRlWsc881WLCYKwle/9hNE+6zr50j4UV3NG3RcyNMTRpkvaHFoqHxENIm
isfGlE8FLRp07/oi2l5v469fDhgs1v+EO+6hka3vIFNrJXc701yiRbcZFJmXvRaJP+qxEDBQcDcv
Y8KE+USGVDMGn6zbB4pE9uzTvz5TWc6h4UYjLK5sJzRzb4+8xayRbSAh2syigRjI7EPvSCBmsw9H
4ClbBB4azd6NYQjsffBFXtEs+yH85OznXz+MecOCnlKIuD0eXNodbwyqTO5gC3ClVG/VuWhPNBzC
WARWo3YT5tabZ3RNX8XvgL0tRie17pV5YXPEqvfaKkpwCFiwwudQOhbfneV3wwyfPQXHpyAvxNEF
OI6LkSs1d6EbGGzi0WHi6xAcq5fDPf/yfiEwbQ9lQBkjJC50cTwy65xKZI+8lPXlgFfUW6GUacd3
x8WNV6oRTGad4g41Gq90Y6MdliTSU1pWMIr/DMWZufsLPNlGvhPPcN5Qsuf0DH5av52Wr1dlPo9m
s1Z+b7yWQHqTL7n/reUzxwNMxOehZn4ATdEFfp0Lrpd0iGl0OvWY9WD0qrrA/Ydf7Mo2e0M4+bW2
mwv6dbB+tdV5feFG0dYPHA9v3Di3CYBc6OuyZlOkKz04gF1WBSPJxB+AuNuo7If5e4UsVvO6hRhG
Svv5Cy7A21hqEg/Ov5X3kB6LDfnqeeQsXS7J0qIY6QI3fk63mUc97B8wqhJLW8XqUj7GwmsgOg/X
4gfIwAAeuDSmyPOR+Pptw6X2nlDRiVT/U3bUuggVRAYGA9I4lNYm3Cb5flK+JCbt5cWdzHln4UaH
HUJROaRkkP8fDCSDU9cA6xdHAiFBNSfgCRs+RIcpsp1rh2pVlOp06j1LX54JpNSqXhF4Kptm8ncD
NdB7Zalo8b5tfpwqQeugiXWMS4lB7UyH5WHJzGqACZwIIVxd+Z5T2E3DNJBpZM5H3F7eKA0/ts9j
4wMBdntBFG/3bHipDIRUIyQ84+KpDTN8FaQCBk74qZHT2Ypnhfk9PAvc4uB+ctzsc9hXmj8bdFXf
lbI96QHjZsUWqxsyp6+y/i1lydBQ0RgZv4y9Bw2cdOrmsvebNIac98nTZx15rdQ/lBrtWBb31CpY
+kGejhtZb+31NZLnjJ0D46VSb7BCRZvsXUjuYTDedMxpuOThKzCWwOl54OMhnHAuYqfY8RJD6qs4
6qa5zYKBtC75q4Y7k8MUXOEQbl5OUPoB8pcc6DXSv1vCOvC4pfzQFBOuFapmDMQwAtkNvmEjhU8v
3CbPhEe9x5or7igyqqjhrZPZbNo07SWnaUW+0RDnScr1qabRdsNS3ZvY/ldy3P5ZvczXCDyhTlq4
OQkga2KbSRjyIscW2uMynlB8/mrjVApJTGaPiDFNRwZxoNfTE8ioIFF/6MstHeMi0oWIpqYi1huF
LWPpZa1CRM3W9COhWw8EfiZtvXYRA6xqqCOX0nXsZeuomMynaEPdBhTTRnED/Ue2gJVJd+y+35oO
BJKbafMs97FR3lOqZsjzh+67Dh3tlWjEwmgM9q6beJfbZLdO8caihBcjOTSpLD8VQaJPjmTW17sP
ksd6N4DVGYpWqnPdH2dWgF3EMeISG2zmh1BpymexzCLKP6irYxhEbv43THOvvUDvzSCtBfz1uEzj
tGszoSf+MLTuBA7C7DurWcOqvhk/59xKXsyd1IttlW3oH5yzAuKfrCtq/YMRboV+GUrf45Ffrqzj
PKPHysk/g1jTLn1kJOHGH88TE4gCXBXeQg2X/PLYxvu9D6hMuc6ZPCzTZDofKCCY72I+uTHxNu4K
o8ePTZQQMoe2VyOrJkj7qpcnuYGANvM2bq+8NMOP5/STsGeXpvKLBsMSHNpuX6JwNMM2muegroEY
NDLUnTN8sIXBF806wSzhrzyF6u7NCx005nX8YT01eYjv0KQ687qJT5eNzgPPaKMQ6tOJVyhusk1V
m+6zSHjHXswCECMT3Lvrz2fhqHNYmgWqdbfHWKNJDEaHGT3VKCv4OoCrMoNE2SnOGVqNEHi0cQ9L
Cwl7xFfslvjsoL4UBaovPJp+GRMd9GCrPzh2lRDHp8ftinp/pWvdq7bFSy7jyXwMPZ8uEUAHamsI
RKS8fQcElMvqBECiR8TD6VTYugLnLrLYotj0S7si37ljyKUAAuvQBBj0b3N1MKoBfBU1fK1dggYW
RFTO3e3ta2ZXXuNc+AUTTPH6Jtr9HmHdPFZh9LlKFg0WWTmkG/toCd0Pw+kQiQlli1QQCiCCf1gX
gRiIUoigheBx7hUAL1EcW5ZPN8BpIVEH5orXIQOxM7QK3IpTScDnh/tHep7b603uuCw0ZiwyOtqL
ms5gegQ5lYsrNU03sKe5e2Bi+DRUpfscj4ki2qQl8AjntY47qBo2rL+HczP0mguDHP2gwhlOWHGv
Te5dEJ/6XIpfgrz8cGglN4rULPndscRfu22fBMHR90btgaD1P947w2EiE2vqnpOQmdIJWFiG2heS
vFaMIF1MpACuz7OOsMR6oBGAEPPCgDQfoJaEAzZaVxsx+fNZKMywxW22zAyWCiwiVSfYhV9Ltw5R
I7DR3dDd3cyu7igCKL1bvo1cyuMAUHHfrI20nPTQNu+WixdM3wV6FezZ86ZJCxXvL1T5StldNFdu
WG6UHUA9cBVAeIbU0t0oRGT7hQAepSO72Vvqy2eXUJ7nKF9raNAK/EWtFiaiWoFZoBez6c9YFmDb
RKwx5DLXlkN9XbyuOebSsrtT8q2v8HO8VRE9WlUmnKVMKyaeUH7ynWTQSXpvmgTqCOEE/HIh7PYc
6cAzBBZujKIq8xEnwYOJHdkwR73O84275urwgN+SPqixwIcW4lSQKmnaSiXPS4R19Trn6huG1Yyf
3wJRYOlpphknbaG3TY2PMA/OEoan2N6cbNOhLLKXlp14UlZ2BaA/AYx0LJFGnB8SR472uj2DED1S
w2RvcIvb7YwNEOu6kQ+g6RXQ5O2VzgB5Df5KIUfWGDbA0uWoMcORqB0xyYq69yfrtkOahC06Ml0W
H/RzYfd/CQRPuaNwqb/xb1ANxSzQfJ6jRvitkj9kzDMYy6JV9YjdjhhGZ860dcknxjIUbHuEYqK5
MJdTjGrr1ZTiKGxlydt9y/Emi3J/n/QOQRDN3jgm+A/u2xRtbTTuGcWpUfAgg65bPqgmnt19JSyP
FdaKRLqurwQmnJILtoTemK7OatyVAddZrMPHerK3+gnst+nlj7tP/zasejFkr7w+9gBEVb1GrWRa
9TCsqYizu/E3G1pjbeWAvKGLn1mL6eIwL7jiYiXKVbDBOVOi5EjW+vTY30//40dBo/zpwhXlNf3f
cNRUds1xn+ZhQff1ueqKEPcgbQLoVVmzaML31PTqOKkTYlChqPLMXWa2i2h2SKCUAjlcSCj4KAQm
vWUUEDhDnzKh0z6bJQSQQpy7M7d6HFlSeAwjlu25bduVcmzf6GCA/R3VaT342qunOkUvHNOcoZi6
Z7K0DYMP8j98iZvMKMz+JOTA+yjou2GQLe/eW+oaRr5tJA9EcPUY89qDJNsBG4D0p/C/GML47DQq
Jk+u/G+C3EcZ7m7rUooJziNOI4I1MZE4twkeIAvvCNu+8BbRLzRVTOzJWzVSgr71yWgscn+cDY9k
Xz6EZmUJ8JvQJver5Y+um6AOt0CEfMOsGh2aBVQrHMdpwyAVDz1vUlKOIvhBS4VnQiNLO6JtCmI5
Pu3P3Fhu50UUgTYK+ZwuAYSVmLj/BIlSGPqPkbV8ondgV6ba4XAkEixITVns/nq0xdXcZBmz3rad
8d13KqvtwDbOPm0/+of4TcQtPEvcv1oXEaNQxb1vqOjSTrcyT83De6Fsb8Fi6BOthcqNZWKWXnyr
/hJHiwfkHluTsvz92oj+PsYHJ7qP1gvC2bCQJi99cbE0GL2B6kYd6SmjpzhyAsxqZ50RoWPeeYYZ
TyWfShaW6TfweyPzbdJ0F1t760kR5mtrI17GMkHglTN/Mf1Y0BkTfoSiMBwYPTfTVSaXa/s2sVKi
qTWoNSEJWY8rQMv9AktzM4HXYNtcs7WMxB5uCaOhABmMwIEBBPrStI4dgyWnKSdOcYLQd/kB2dxC
t58n2vsBBa+Db4EgHCg2EGGt8g9r/AgKjQsLYv6BpISPBWyTMYbSMMtztmdpOVemKWVkp/9UoaWF
OBRdI7kkgsCT29XKBAyuHazCAdeLUgOLnpya9DblnHxJNPBG4TMe+rzVhuvTQI9c+i4/0NUxLR9E
WR+v589IZnjH7oMoWWxSRmTCd9dzGlRzj6QTshg4aFsYUopKIj6UAtGRSWG6R3XAv5pCwihDpYMq
RVVRwINltT3P4qdAUU8pRzrRLurfWGQk8TbBEWIDFUCoaqayRQiO+tJE9m9tXBF0aS5xJ4OnYPDV
NYsdjsrl7WGCEOyqKbmraDeWvqe3T1t+kdqweZpjHLCygcC0DYlRuYmZOvfUMFwxJbto4PFXhLTD
bsoHwq4TJ518BvObBC7Dye7k9jukVmocCIQi0JSJeZp7rfCUVHAQiTIDchXre72NTuyaCoFbBh+8
XGL1tmF2CAMe8QPvVSSs0jiOyTpM8cKvRqz6nNreAdbVeSxM90VX+WM10hgob3s32MhmYvZ6JhZ5
WixhorES4G+m9fLczRPFIC/FZuiQdT/zXq09xmi8UvF2wDd+NFVwRfAtiGHdvWgxEjN8WjvUV7TD
5PpCDN9EB6TSA/OmeQmmF9lrnBHc1ejAnXV9cjtxAsH/kzN5KsAvAcikWnzXrb1ZxPSn2PprLOjD
jvFCcIaMQ30gslioA09odqdmadrRRCh8mmqbmmoWFgC0EPjtw0/wgg/hXrumQ5vCad/emxE1NcKD
vSbdfYgEbc397hnTn7R97CWYYBYiBci/+PxhQBTZiugMvdMchzG8YF87ZiNHMSBM5pnd5S1sV+4L
HP54xqwRru7alzJCMCoeh2RqBPp4hb0y9Aw33wkphQaNHS6wu1vAY8bBXWS39wFw26J8hMn1PQAr
FPzoz1y5c/L3nuKa4TIwJm8QraY0ye4pHn65HsaucivjFqvm/Tl6TCoDBcahvpP0KoSok8MynEPW
bCQ9U9tGe+4CD/59wCEKdMlFSpSVhR6RPzB5zGN1uZS4dO4+3I7zkxuEDXofkfN+aiaXgpI7h6jR
oqNSiFytnzkbXZ0CY4iTFl2xlRgv3pANF2BhFzYDBMMVdmve9zY68+6+fKulwONXZLmnmaU2IXHN
a73quQEy8+cm6yc6uzaL4GxMOeKfUFXLt286TgOZqH23fKEDVNu11QY1zX29+oS5JoDFs+fyMWIV
1CbgaauvxDU9CwrJ8oPPFrB+thS1ZmsHeexg3MW+duuUitoj8JsCx47EutJ+5uYQdpYL/c1mz8EI
ggb/EVDWA//8SxuFlOyLyuZkRfGgFR3SitritkacWAETbhnds1ahMHCL+lv1dSs+r06am6G8tQyP
RZDM5chfnLfwaTbE0/SONGCuIV69aVKTYsdHQAVndbxb5a4AX0D1Nd1LPF6whqtmo0V31/D29EE0
9baQCEYzwYgRPLaAsHDtlllqJ1JlT7ulXxkFJFSiGdlMxWdax89TNTIBUeLt1JV9AdpzZcCNHjfK
kD0XkFhzKONX8DRT8LHxbVEGQyTgvVRrDZpaiML+nT1uhdLGCjoM6wT3fwu4h9erBWd8szYNZnwM
s+8O9bJ9u9LjXyorgYFY47Z8QDspXzqJtjPIc2S7dqCJfX5yfvyKkKgm6JtSgaZp+TbaBRr13K6A
djWVs7Mt1kPx3Hmpa3rhqWtDJ0iSDbOCuKuXp00xPxjsQfOyNYp32jZfIPgJNo+/I31NE1OUFa9x
qStJtrtIcT/11CG4yzZBvLgbUVPu5SNCh0IVotvsnfrhIpvs85UTFSkb+tusZjPRtM9nemFvTgb1
YcQQaW1RZd6apbHhQ7QOkL7201kAua+pBIUkDYnA4ZVPa9trIBFRvaXOUCbZhttg7CItMmCqY4V1
6FQIBbB2+dwKoNkK3kbswrNfgljDu3QaJzi+CFi8plwPM+Xauco0M8C9u5iu6brH2PRuvInpjP1q
Xlih6Z0feX4YFqt682OpGMXySuKpOkpmB6GbvYvFSxSzgXh4bPLEwbm9mgCFDl3ZnEUvMC3R9F13
hOufCqKjI8k+9oVN1dQDXD1JDrTWXeTj7/rFJx39+JJxC/m0sWZXw5Y1S4DJrN9WrJ+qeF17Jj5n
v063M0vQTmMc+AJvd6oOh6eCOSFa25PZ0AXVTbdoMKYOdbUO6S2xj2OjU24HbyijN+T8j3Ja53DA
NYmOMrnRdeHSiyCHWPrCBNhbMXT7Q+R+kdviW5S1DrEWVpPWDocK/6e3TC8df3AuB0l/m/6sQSgV
6ifWAxMiYQzBzLFE2jKAgF8n4bMo71NvdMJiCOBErPW06BdsXook7ZsfWzGOAmPibuNFbVUn7tSU
dQ7Md7h963+WCqaCMtFvVv42Gg5p1fMnf+wlDMu0Pytsez4C785IwhMbvfWfjX6cjIOne0yuTtGC
fH46Ng3Q/jbu92yM2Njv4NpiUPUE9iHLe6SnfrChokGzIZcJRuqrqQrtAtjWFT5DcLF5O7WWZPqz
8BqKJCE1pCbcXbrLiyMpjSJFT7SG5eOuzm/78Hz61CQwMh/5gimnpliYRsNINyKu6YmA6I1zdFx4
opYqdX8RBoj4onw8qbFztsoR8Bt6P8jvLy4B3fv5K1WMTwW9e4p1mf5+vesgrneP7+IyIZWlTQI6
2RQ1vCG4lQzUaGNiGQ5f9qBCVO742ClkKVKV5wVVXhvltYePlako5ZiushoMEU/9s6Y7vOEh7V3U
5qkgqxfGqy6WS3HTQAM3cDqW6ipmJdub/XOLnsV1BgyjeR+Ns47BTpm2sb1ZM5VX3UDB3Zl+VP4F
IXquADYizTOePpk44lzQVKw14gDCdAMk1acuj1BSQpIcRjRaURkgyyLDifwWu1EIyFQJguA10abw
WIHzqcVA4RfM5g3JXqGlXYix76sm+prq9fa0MK/sLlUMBRutrfgHewb9D/seXaItxgMQOj9B+LVK
5pCIT1F/q4yuD7fYyrfJjHDRWfHi73Uo/dwDONYWMib2nr7IITBqIh2ZJrpW6ANIlN5UZZncT6mW
0Gof6SnXL9kskiRHBYhblxEk0CKd+lNCcL56R/T0BrhXm0SGiLz/0XtcVoXzDSsPlQz7PByo2CKp
VR4yOCq62OGbcUXYxMgc9rulbLBn42Tt7chlCKTiFc/06Qd1rfZMSUj65awcVQfT1DZMWZtj5tRX
6zlobZgNttd747P/gxGXBQgpAKhj2grK60+QvgHHNmqi6AnejSSTjYpQwzXMSqahxx55DLB0ljN7
spzOKohJAbrDVxFgnoxDxqnwGfv+H6ZNDfDjlpHj4qLtUoSfiDgJIpaxfcW0r4y9MJpzuapA0J+u
ZuXBflO51q2Ddd1sVLRLgujW7vlE4jw4IKX1ti/sNHk/EZND+zD1AKvbjxC2zfZasY+J/KtAjDDQ
ZQv1J/nGY0rOusXsYO1TY8f9FsuBORklvfrtdv59EfVrHh/epx2/0Mvrf4bEM+7CkGkkOLK253jZ
qbk9ymLJ1yc+/LyMPQCVCNQ7+plVdKfqqMXdHcdarpQCTKlWN32bkHULCzAMSS7RysD60nBhoVgU
0UnwSm/FItetT9c/2GQB6x2GynbdY1M5ZxF4HGLhE0TeiHJH6QP7PgFyQ5d7c2j7XBSGMdxUyspf
eZbqxIXMUIp3mzCBsovYyzuKK1JFD5E2QYsa8w7vPhhIXBteg3dDEP2VscOHQ8oTX27jAmcJ6fdb
Qt2CgKNtK0w9wqFLiC0lbjo+o/jcloP8kiV9WnKOJK2rFWFjcxBMMmvZgxxeseKvAg8CiYAmU6vL
vrzQ7LDR7J0Hi6B1at5lBxUmtJCrqSK1TsIdzNZhJn6cDEw006vZVpvmnjGE1PDxcseket6VboKV
h+K3dm7TffO8rsdtnbmHR2jiX3vAtm3JpF7Wjwq/bu6LE0w8nL4GLZcCbcayWjOuGRCmCUx9VUor
lwb6GDVdW8wzqq6ubII8XCIbxWxciwqpgqf9B8aceJrFcDTHjqdixOExsjjxrBK73+Q4JgS+JOaS
dKb5BNQcIExfpdgkcEpY6CY//YRH7UHFlSGarubiur65tuQJwVVgJ3r2yF70muK9WsV+j0Akzimb
WSQs2j+HQeJP6y0p6Dr7O6EkRxPtLDFNtAabnWkAGTLzyjRew5UpYmcQj7in9hm4hmpfXxHxZfzJ
2gyBshFjXBpfcYVhaayahB4RxULq79JIriv0cPpugFY1sLhx4T90lTKfu7V71ePt2nt7JBpbNJVK
mb9HYkGq1HVt7PpOgpS/cwHxnRHxtIZqCmMDY7+PFdOzv1cFbKjjY7AQ/QmblW96ff0gXQDrKKjc
Y4NARJKU8et5Y2bx3Nj3OWnNBBIs+imc3qTniFlYD/rI5zIJFdNd9YdAhAE8oqpR8niHpEicikZm
08wISigbzpgoOFOZB0gN2MC2H7Mwildor4ymKgPVzXVOA0+26vlJjtR6Y63v6H9L8h+qCVU6m/0B
ZuW4wLAmyxvnJ+st0mKdYZXPrr5iTxlwLXYmigl8t9HYigkku3tbC06b+5yWPDAyWUTiMdNZkLKA
b0746muTJNfdzz+9IYr/oKb0peWemYvqjkr/QnfdlAQKe2FOgFrMkPVUKIB3ZcEK6j0MdC5Pjjnj
80dW/LtNb+2/Q8FCBsw8qUOLUcSeN5q8wMoFbzJpVviUfzwEYOCgOFwNXs/TZvo3grrSOpWSksT8
0ABgJIlFpYE3MKjzS0hFpWbCW+tkZNKaEGfNDOYI7K+lwQJdYkOW1WwfyvpCG00kvBD2wGoVFscQ
OmFtsWgptOJPKhfoQQ2atWPLK0XhQIM1F5sW41q4tr3CYy660VGYj/IIcNX/TEpQXwp8n/doEdi9
DV0hcqdZvskIX231Z58rpqctyuXJebgBoBlFuYf+YEd8fjz5H1OgLXtLAc29nUgJtvVmHyFutEqu
t//ws5a4odilE21iuBVadsiFmfFMtUAnNBLtXaUNiySeoG7FJYEsRXopeuCThjMilMy6dbcw+cAG
2lQcdrBjcBTEMeT7c/Yeem+eGBbe8Y7mbCDQ3GctDvLk19ngY/slHRGKGWZVdUEHo5oVEP2pz5GZ
Z7fl7jSrfpq+4SiryvU/LNYDkttjU7mKwf/nLH95GKTVQIBexA+Wsl3awpt8QNxK51xdRH5MKcd0
Aaq4lT4ezMtbnCaj93cgXxqKIKTzSypUfqxUGUoaJchbJRI9kaEtuHtqCB4XtQ+W1ahK41gxMWSy
B/X82HH6E2amuR3xo4PTcvbCMo/v3/F1r3KniyMPM1cUaFeCkHrZV4vOhDdwTy4D74yJycyxYK/y
2Mn7+SZn+OL/TA//UskoaGW+ilQztpgcSCgzhS2IkHLgelpPGq+sQipEq2Y1qhxKW/W8FTYQFIXM
v/AK8o0PXFatT4cR6i6jcYE2Z+6Qfhc/89VJDlwDVmsOvc+/4tvKr7aMF1CJbyCgpMWYa/ZvmUVx
AGyxGFe6sS4AxRY2nU2UaHbZ7WJlgdeO4yEmGtF9UZ4+U3tCg7s6mWjH/iMa6OhCwcQb0S+Lstnd
yYBIv8eI6ykbDaCH7l0JuqJeXbANMeZHfwNGdeAam32y4fP5gwziW5nPJha0vBkLHi3gvZtNSrwu
hYy1YU4dgXMg+g3h8RviTiTZ2o0s7hF4K7L0+vRiYKyLcQojoxQhgd3do3YiHqIrBDgRYdUEwoaW
SlCTHIVAd84dDHE8DNc2BefPHE2XCx+ND2YnSVqSc6TFSJdZTT9lk2/EPyRSez0Kv6Imvk8XUWkB
w4bCAXYok6ujyjLZUDvFGzdgmKQnq8lab+vDuwaJNPcWwb77yCMIT6Gv1YnHpKNw33xIgCYszMwi
1xA0zUJ3WtlPKr7uvORpj5+3lZUGNRP3EJM5OpJl86vKWTvUKpa/OMgpq57Nbd0PwA/hjdCuMcYG
fdvUBjHF5qZsEz17pHgmgTEUbrvScZf5PagYGywgWiQj6MU8I5yr7vd0xfgTfhFyvhW0YypYggzt
CsP0CX0DuUMYuoGXX1P6jU2rQCWhPbtW6UdZ8xKx8Xhzcn6iyWx3d9FNS3LpYGO5Frc0hP0b9RXf
7wOTbDuM/QRritWHkPTgyrjaowNLcOMBtFtsZS/8ofp9pC0JufxarApGoSVcC1tcuMwuN4J51IUZ
nM4LOaHn4ws+LkQ++QkxczgvrTDOLL2xiNIBAKweezwNyo30CNwO1lvnJXgdTWFIV3bFpyvJQlcX
bP0QrINifQ5nPDoztlNFBdM3qGGZkfBNRSo/hk1vOwrzwrJBd2fSPBdPar3Lx76lk7AFzDQYkXbt
nJ7R7DZg7EJW5IuBrEUTUpnHEFU6It3Eoag94sjJaFRr38jtvrRuu1AfUpHgxh4VvKcLmS8rAtYk
sKBFy+nS257ZbtsG+VN64Ky+eoA5RJ4Df9zJPElIYDDpjHObzL07Ne32ATqULRxfwbzC9puAnD/P
2/73LiM1qTmtOaEuy7/7Rqa4Go2hx6Ng2SnBjpU0rWGBuQ0o/0v2kGI4kuYV9PArq/33q7BpC0zR
WyjuIK3hDguLW4hqaVfQp2RTauza8P4FK7lvs5Jtp141iR1y5BNjswWozEY35HS5tBbtzavMmVEA
t6nOGCykjNoKRWXoITm/Bcv5r3P30D6oCl08mqE+CigWL+ZOS/vknY3E6FUKyTFbZFRaxToNjoNn
KXu0EJ8Mw8TYJLW4FKVxN3XIKaRWtp16F7LYwjmAiwwON/IHVcuLu5/raNW81LQ30AeABrr7pWqx
eshQDIraCYnuFxKjhcedpCHj+vHsZVKT/h8J4X7h+9U8fvKQgS/LesuAz4gqtF1bZaz/oNLgS3k1
2mae0E9Nj5IxQyUkOyaxn4fhV49uzmM+oBgJukx9ySykvXcwrAdxc+lox2g9qP9MO2Uynigmp2Ve
CM/XAIQEUvQHDW2utB/L30ec/pubiJ+aUuDb0f3vFTA+z+keITYccErZTQqWF4sVqcnDVFDp7H9N
UZ5m7ANelN8Tf4AN9Nv21iyOKFvpOFQYBubxzdqByouoUMnpvZJxQGfoQnPj/Zuza03GC594N1+W
JngctyYBebrgq2P/btW64JXRBKPQxbaH9upirMLf1oIsAcs2gq8FbKJx9q6uDP+5dXsz0mnq4jdD
x3f/oyKOO/XuxxnOt+23Dno2DSMCBTM5JYM3MEK/O2+mJJ3k0ST9EiEN+KknBMrAnk8EZTVzMXnZ
YE1Wmif/elxkHYCCr15XPRmwtdGm1gUAiNkN3VOOWDL7rvt2oCFTb0dKexHsRCmGk79vVzqP3zLX
xpeYW/attCOyKh1q73cighkTK0QNxcE0o6q6E2fKzfMVIkzTgM+/qxNUOwKqxIGJ5wiGl7rEU1yt
5TwdcX35s6zwv4nPwU3DvyiwXR5GeEuJezAMkshmqu05nxUwbQx+nQgBPGdrgX4bVJkoc+v+W7iT
PiHA2hw83fkIbeeCJo05OyFbp3wj5nSbCPCcQgpaB9RwhFleAf69xTU/gxEm3CK80lRY83leLomp
7W08u/LADxMzGeya4RDXTiQ1Hd9YHMHVSMnODHKZdFApPaIRfH+VbeeSJ5lfoKXzCXHBsslHB8nW
zOBZ4Jphx01oLepeSH6ZVHoG+rSto/dR6ho+JbWTghoZCu615HuVuPVgzVyHlXNv8cRBXESm71Wo
GdDNMy/g0lNR902kFlTcDJd7gujQsuDDuqU73hgi/eBrV9SPE+SBnkJTMHEIAFtHFO7QKm5ucJoa
QnXEwMzNo+w3wSjhla/EimhSlopFupXuxVpM+KBYBJPZ404COgeCnfO1KospsiVLlXAwC1gWi/nU
m3Q31pzWF2beK55EoCSei0YxB8g2+5pOnrFeuX5BF2bl5sXEFvJdswuIO+Oyd98b/lKgRLV6sfA+
yRdJ5WZnzJdJ7UGwwSrv8r7iICmlFrC8KGSlSGSKfEWWoaCEOVjzmC7TitiaMl57+6rjiU1g/Te2
FzQjNNXTglZ1BZ5f71CTPuFn3Xosw0IekHiNsF3Q9t7r7p//DALpx9FI3c5eV8PMzcgFt9qo/8cj
lPx1sHTLgfLKggzeKLI4DGBT2pJ7dGLpqKnUEPmWiyFk6sWi2/c3TaHbTu3vzynBprV8hZYy8Gr9
F0XDGs0+aLY6b/nsSp1GuzclCfFe0+Oph6/xapm35YOkOTEnn9brh8QpgBWkqO4ytQj7ZVzM3bfx
d+w0SHkbp/tyOl63XCqrrLtgJOOwLUZKLcKxIw8YghMZDL4ghixUJOBsQYRvSlQOjnnYC9ynUHhR
5hqVeiZ2Sul7vvH77mMYhFMaEOq1bBHtNXBGPKXtfjsqlClK3CqTLv4EQ1RMJKk2/68Mmc9nn/2q
2GbuYbQgx5ZY0rPTEwmUlzmzaDCrAtSnrjofHfbQT5gCfl639eFn/5uKG4ECaFOitj7eyfxmO267
gIMxO/lwboFzxRazcrjU+kGpbQ3XLLNOBJzH4QTf7r/JY9l1+E0/RSGeQ3xGN0MLSZyPImb1zuRP
qltBuJ1oYFNrAGpZbIyI8fpJ6VWtHu7pHBRiYSTQGeyPVrO7/rSJg+ayib4sKmAUj7omJHggslzZ
lRALBB/+5RD1sSAEF37pY9kwf3mW1GLQTtiJViEcGcQkHQM4O1Gl8OXfiYuRuiBX8iqV8tLotr/6
IIJ9tD9tfYyH147sipb/gLuC/0TJYxaX47eoOeF2u1BKpIcSTbS7FHo4WGowqz+E7Jk1cySq83ha
XgTmhTY4Yp0yF5oBx5CVZKW0xotnCvwA/vwU2wkB5WKw+uHxnGj2blbeKtIqHsJV/nxIRD73uif4
3Kpwju7Xf1dDEU2qTWjOsn32jyittHcWhVSZbbBi2PgEpFUv2I6SyNr2emMi/+8CIwvyhCMatL/5
aF7WrbdVEcwr2Q+ZSuPhl8/LypRq/b6n2pA8w4OTkr7R+mK6yonGKiS1xQ/STZph98htcOp38bNM
y6GpB9+PEy4CKClMdn/G52dLe6uO3KEBe/+ryhSS0gbZtgpv0wR77qDtWt4uL3W9rbco60pyfyED
Fe48Yt3lsDumqeqXxX7ItsN5235GIEWtLQMCMaVrqZgOiCls9EbF6BdLpGwIFV6SNv5Ka8urTqeY
w+/OijDr8sPCTvpkTzQHPHn2NrqeL2Zr4pVZjpvKtbLT1Q/IV5DpD2tu2jJH4clqcq43DVJX88dX
h00VmN6K8GC/iupgR2Kp2xxDFYTa7q6ZKvVVFjgse35lxCtHpB8wz0BDt3eFMGNHfrf9mN+EREKp
sspUEixj9wZZLBg6NbVsEYjft9R2IKrYx3VjG+++XiZc0uAk5ymzl0ECHyA04inLIHryoxcfftfn
30GEaZi7RIEFnGZKZ50DDJU047IivrwVo2tZad56xk63xCLtFJLnuD/qr4LyMdlzhJ5Djq1PQddE
dmc0igIDuMoUpoaPbvXGKTT/EMJjVAaujGQI4GbaYJ7Ax1avU4bJIVbucBYUIixi+WJ1UPdxcrwt
FM29XWWDYqF8zZyZMqW4cr0yHbqfahSRKqPmBYpnjfvSm7e0RLN/h7jFHgCnsI6A8jlrs9pAD8Uu
ODcFaHUeKgwXCC0tuZjkcVqEYKIdw2PrSUjreGRTv9Y19kterNltPBaJojGORte4y1BKrQ0NelJV
jqR659LCrELwWo//vHKhpBHMOQH9fupvqwwRgyCs+Q9eV3Bd8QuLk57MJniGrdk6hi8/SemB4ZFs
j2391+AbdY1DuNNa6pP4lFIR3hixYPwHYCaGu8sTupvEyE79zH315MXIidd4d0g0HnTfe0At1Jzx
wDA0eLaZVP9HL2wqAIeKDpex9RecnpvK1xyLaz/mboFtD0FVmRaPN7Qww6dpDHxryOZeOb808saS
SRk/aBDR9Qn2xmKRDjgtQjJPWvJ1q7lSTj+thMgWwe+jOh8vooRRX53J1l8gNt+i2llcgRbayC0I
uujRmKOy2FkKS13YFN4UVT+VSpJisVBooIs03wa/KDz4bhTMaiEoO150CU3oZ8SLXHJYuCqI80ki
EdQAkGBStlOGEVzAdE9NNwALIC5qNgY39F5J1VS9cYgFNw5wJ/5+CPcPhBtxj9Y/98dUPlXxj9ml
ANBw6rtn5xtW1+S7yUz02m6Pb9NKKOQF/+npXi9aPHtYWhnLIoPcIt9kabpNb5AqcBQwe614pzek
ETvG3U7FKlGViI2xFJPBoEroa/V4KML0J3zR8t2J8Wt8G9MnCoVg8WtPsb/7uLLKrDWXDuTSnllV
DfVIKlYZpmoIjUDLi1NhcbeVwBJr4MErMmaGJawpJujTMD5gSYvXF9K4hatH0fqWd1Q8DapmZpNf
e2y5TLfZ7BHFvrxCK6ftSL+fBOWbj0WEcjM4kT2uQw9tHthF93UwXVPkJwT+63f5rHGi/No9kIl2
9yQuAchOnTwgHWiHjZtTMcQEf51B2QKD/dBxqWPUhAPqu2LRYphb9BpK7dbsEcj/+9vxutpS9tlU
dzhx9ObQjFaSmgcxHouApa30QJmi39z8slyDKBC3mElQCYt14o9TVwNx69NKTmFFcu59AX526a7n
49LfYKTzvC6Obq5wn869b1sBksB2k+MgCaixQXSZiw3gTBmNTg2z4R6YzH+Oif5bplAJqgmXqP4X
sCk0PSLDSdfRFUbX/DfSpeGYfR5EPbPrqBow0ApMZAv6rSkK1RYHbLZpRXiMfihYxRTO/G1PkVxG
apIONqmOAkQ9D/VwPY1+pazVe8F1UciA4nsyoN772ut9zdjUGWAyDHlcfSQSZNPTUs1scjivQkFW
IGtviQU5aAtRgYjHPM6E6Rt1GTLbHLmYk4f/v0bt5hughUB+5QnIz8uT8yrthiZ3r9xQ5n/Q/mnv
PR9fAUAhZtpnB6kbZ9yqmTPe0DFNud6IuouULA0ud+ZNeup6oYxeCYEHcVp/vYcMmbY2254xW1oF
F097anQzdwNz8vN/xQ0i/wl0kyHfgVeOR+RDNZe3IweS+n0jnql6RYWz9XLJwZh1wc2OA5+l1qBL
GlCj+CSBCzQ6NFp8/84yeGBYvUrpNqFIs4Hwt+BMdppdF+eEERRjdm/2Zb502nDfGQAIiMbRQFBX
WNGXHeA3Dj8ZN6NxHOjITDdpywkhgEfG70do9cvQ+DwKw955GY5aBqVe5nKFqbaabcM3B2Tx0+tY
Yhw8N0vC2chHDNZsA5LD9taZIHyIkN63yuRvNF0mG5MFAlmWCnaBypggoS0umBDrE1a9r1Se6m09
HgtwXeJtTeEdZ5OHAHET2FXvlDbIf0Vs/kIvpHuCxHNR2vsK7irdAVNg47QtZygCrbjAffgCLPTZ
+2TIIC7/H5Xf3896vRhPFbBWQssa9cu4mRVo6aeOrr86nWJCbtzzWLckePvE8OQH4x6MdyFDb61Q
N6wibeqXyze2e1p8GdQirBDtS7WsTM9bWXbVGf9Cv9zqd3U1RELCTCV8kX0KZr4DmowqlcgSkxzD
XELSU7njw0oRFHpHzGNkeFSN/1O9jf9+kdFX/W6bIaiWoEyLKAaqZDKiBxymAbCKr0onsX04TTzg
NLqVKizmOPXF+wTZ1XlLKU9Ig2BEKeGicrbwRfuyO0UOkSQUnVvwidlcWJEPepPRCbwqj75WmEZ2
DwLk17hfYWwI2Dk4EMs/+BDtjJciTPTaLUs3PUd3EyrpYP0XH6ld/M+y2NcY8ZF/GCngon/a3fMq
54CW5zSODX/A9ZuDQNu4bpDLgelSKPDKkt8h1+g+v32lptWAkK+Rgg0XGto5cIh8LtTXDhhwIByV
qG8tApdW8Kk+cLZNBZzIJesDW/YxwkYdllxMYHzTwE41t962ibHV3aWGPznd8d/C4+GqKZ94IDq6
ec82ymxd80hg2VmO0JKBmyZPRi/PEZEKZeBHgA4iKtLh1nthJAsoVYC65ClHV5An60YJkX4rxKJ5
IBpkC7y93wOBlozWvyEk5Tp9Z+9vsKAAeFdSY0X9zxqxvR93wtMNLVzSvrBt15+v86Z/M5kf9W4c
ZFvK3hp+T2Cg6bInr1gWnw0i3qtTFh6jYCqvup1+21rZfWeDY3owkvUqx7I9s2dH9lsj8AGVXNaa
EATAVtCHh8Dpzwez80Fev8FnjkMvzoFT4OZc6tfTs64RWY1vNssZog4sMWkklEbjXJdaM8yzh36j
GZSVxr7DPkxTf/RvuX8mRKyuPlZbUaF36f6Ni6Bcm0k/9TCZRdbYKMLr0x5XkSVLEMbJ1vXGelyY
2eO9CvpJSthglzP5kAz7X+xaXAsWmwuoKPa5A1hdQPxHToxGMxl6fvvZbCLrE1KVTHEnKRKyBwRs
8GFnGooY+LLKT3pDWxUz6MUGpN+YQ58BOcFVG4iFZlfTVta9+7as6qYKY0dBqgMO0VvLtUSaj1Gn
STGtXS84CHv7KI9ykt6ARnDxlheQAkCAlHGpEcsZJi7ew/vJriMEpS6luSAGLvJvIyv+HMI2RrRM
YmmQzZ1eHRFzh2Lj7rf5OYIhL9bIeEjmF1i8BExgU2jf7vcmCPkak5r4wAPYxz57pwAne2xjk6lG
APz9XfVLU972hZSyQ21EOiC93TSmEeL6K4LzM3QG04+cor2E+lKRQ8M10vZ7bUiU5Wt/+UkosRQR
BZeVkEm/rGK3kCPBLyJuIlALbCVQfuKT4WVgdNlpsYupZ9vF+CotkCxHqP5AdQj+7EQc/pDCAp8e
HLcAgwLNlGXfeD1QqPHqPYb1P9rN/Y3itRkTDlgwLq1z38/E26ysb45nsjl/M3emNugD0ZuErFtZ
NhIVRG0BLonXEXBLoXe1gpqzXHE39aEXznrzsinuPry1iPuH+uN5UXqiu+RXP4KwaPe9GQ3j/USY
4c20XJC72dhk0hzyefKBK/GUAMAJlyaR4Rg5kYRzYlF164aBxUZHZWb5mSdaz6lD9X9TTFD8tLd/
dzBuVgmMJCUhEAX41AsfaFli/UXnXcKT1naKr4fd1mMd+TkYaMZc+1itHF5ItlM9BwY3NryExcOG
Jw6Ba/sFQZDSAkM1s14l9pjUfN/HwCAMPKS0MquGVKOnI6Py2zhJQrNNm4t12L1r9/1J2xBGGIPS
KXsgIlI5fkYfu9PWxOIX2TjVgBCP9sosxALW2fdueXRkcrNrK0d3DFZn6ECxy77apZMjcTIB6KB9
DyaG7sHshhq5Xk8clFcK/RYlB6zfZmdBhIHXs3Bs7GXDLBE/BypDNGCMtv3cN+jWtjfsjU9oeKYe
dD8FI6p6mhuZJj1GJMYcfofC5Kusv+lKHqBixRDii4AajHE03BqcW36UhO0qBmDDsbN+i1ip0aC8
X6KajovJyC7hCgKYb/l7EbbrS74JPzdPTrsvM6frMsHuAzhae/cuHzM2vUlQsultLUWwUdD60lgP
ZB0IhO2dQwTDUdHOBJ2i/l9+zh14nkSUweMVFILA2BIZY8O3j+VYsIQwtT/vwK+YWgX3JTvmkN5y
Ufd/8c7x3iyDCAaBNk1ZSW6NHxM/vT7ZMCJY+aF3kD+VMb5rZbdKYZ/wi3P0jg7CwYGaKw+bLC9I
taHgWeAdwVswnY0xrq/EPB1xtF/zrEkdy7at6AutBS1B8FB28/9sZJx4AvachQv2hV5ngVY/5fz4
pEY3HtQ5epFx+svqs/9WlLnqhPnhBpnbXChcgsUn0D0A1s9ipH5ddsfLI4JoG2Dudi0W032Spmdw
91c+Zz06Q+zvleJGL9EFMqRW5NvFRfZO0vLUfMR1BG/uaLSn0e3AHZ00hEJ5nO7cnIKtAGuC296X
ztjkoX8bspWF+yP/0ZhRxvGLa7zSDhXidXuDdxtXYVNOgRiDbTgmBGP6smfPf7vH8Gfd5JjfqVwM
M8i9j5IMvMUtUdwIzqPQ4sLMjHSbUxPbwAFSjIS3OeA2s5g+GUD+qzB6UL80QDX7A0SIuWKt3gTb
GGAu/r2ebzr7tDBch6agNTrwRPsIx/3YDoA6kHUd77d0KKRmqQm7MOuaeYK+86JznAzV3IY++wjE
3qw2ofqNPJY7dPz39WMyLY0mrzKBG7dCDN++Cc3N3WpkcZYMCzGMHnYfQE213lmEeg9uHcvorJCs
QDcOQxbnS6gFh2rAWPNuhrVOgeevDOUucBmWGSkckwerjdZK0Ms1tdl04L8f4vHWhbXHXg/fEWus
t7rh9PS8b0KYGzxT82Sr+i4gipFDEmLMoIio3ADniRPVBsHXZWJ9PFHO8pJNN0KNelrbJq66RNsz
sSa8k8afan37KJoknldowuwM1xHx+nQAA8HNFSh6+IBVLgw+p0ew/DPEQj98RkF5QORoZYC6klCP
MDAbS6SGfeqd1I+sLvlQUNMJnJyKgFvJ9s9qabeOxGi4Qw8FDrw1WXe6GDV6oORUdfYGgH+bkJWF
mPIwZm3NN8hR5pUV8frBpN0qd6QcaixeBjLJxdin5pbBAC2kacx1Hc2EKIi31tjhPA8nqxtN/NTj
3kESmSHWmdR/LaECmq1b6cxa/S0S10yb2FjkZCPIE+jzWjmPADmDNIOmR2fxe4puxuPL29dJHqhX
qLkY1cygreISzsLYG+I528E514hLT02SL1i8kRM7GzUib2ROmXiZJ4I33Bw8oA+xfd3D6tz1TP7R
aXPbCulMt9vDVs4n4Xx46ZJkzgSd757iXqAlMWU/dKPyhFakPF0/CCoeZ9+QTmJMsp5Ez5SVzD15
1YXxu06JoQPCm9cz77H8+OQe2griymUbuJ4fj8FkfHGCe4YnPbU1mWRW38khRjENRznYJaRzMfe+
jVEd5OZacR3On7tfdbdcFpLXzHYjkE4mwnnWt/IAIiU7QMaMWN+fV6hWww+EQfYB6iWhquV/a5Ca
agzK5+epVFH/SEUsLrp7DlUwjQ68C3e+V8XqYqf2sMVpth9G4CKG9YfY5BiyQdjzs5b+VZ2zdGpG
tY7mOKUaFEy2hkIQNKL0/X/j5wUwFNCRjOR6YKalXNms8jo9IF/drNXvhG9yb6pKCPc8wqkZfOqI
MZJpOTQJDr8JGuPYrDJS66cGfvFEjKDQAWvXUcQ2t4czEtd5Xa7xCohqe4Tjb/yWnBmQSTKBPpjG
OmTb48Hymh+KYycL6clK3e315L0zju642Svs1OXeTyJRsReCOqIh4kCzcpLNHhCXS6vTWsUD7Hy1
Wnx/ODYdaPO+Kf9lD19+tofcEDgJNNXIGFVBbtG+ryWylYSJj7f8Ups6rFM6u1LMSivdBrULRDwk
rF2qsZo0T/3wVE6llgkj8MWnQrAJw5GG4ba/WEoB/COVvXDQ1VfmUVvy7VYQXD+6scwN7xFtVJEf
qPsTZmydSoK/Jvw8Og9Gu3elpmm/gmuyJ9YJ+RnwlKq87zExQSJzk3UbWJZ9ws2zLGaN0QYlmNjZ
Hi3BuAibi6oq1VLPSk/zB2GZnbWcVHbTkQgH/jRPApxedowBlsI9fQTzyLenfJYfVYqA0aSkxSuJ
hhvWOxhR6RKJfRFV2W/nj9gOgtsUpBtfBDVxREQS+jalO54Y7kPw8f5CKbZ0tFH9FYDQu8YJIN9V
zKTWx8aXX0uUnskC/tdEcuZs/l9EbTQizGta4EkDdip7kiqan3qNhVUSiwgbRDPHrIatZcj1OUZx
qMLV8oYy/Wjm8xwENUUp/CH8+VqK7kK8RI8vKOfQXesm9KB9z21jG62dFn9TsFXPzE+vwKQTYJ5e
RgKHrL80b45cLkRmw0ZW9Eujj+Us79pDzAa1FRy0Gkey7TjSb1mi1QWOo6TyhYFt1Y6tWQqwBkiB
B7dqL7GkMfhM4C5Wa7m4gtlzFYzYu91C2cN/z4pNhmZHGudLBMswMvR93EQCOKhtCLQp+ygG3NYM
fGXDvF9JsRpphjsS69tEvPWfoF5xhpNp5lnfArTD+VW9vWk7R2/bEcsmknv1si3HHF3bpjv0aEUA
aGdj/bf9+HHjDyXECgb6pcfuAyWotjbPgADpFurKwWdMeyCdBgCJLmQHyF4SV776uBiD36VQStLG
Fta+5odWVwBBjES+Zel8BekAYwY3oPxZBVZtcTobtiedII6wQsTjjQJE6gAOb4p/7cp8VPOYtGis
17yOoglhpSUMxNfgFADmcnfIFiI2OyCsUhEf0IKN/s+IgL99hmOiSPa3T3amEc2M8T884MQdFAqp
XJYl7PdrwU40aEDuO1xWEtIFRe6P5QpN/4stQy2+PKwfP25pZOfqlNx5Pyq901pfZ48AwdhjVCWB
JXwH9gtsaW0+8F5VZW9xFqoughltlDNT7VCIUfopr7gpYUc/QHUWuhF57FTeE/Zj7H011a6Akq5c
rnLOerUdFrXFusxDaHWFHuiKivgssfNaorta2+Ehh6XCw40evuwA4FNT3D8asPFNifWYL14va0wc
3ANUTdKTCeXEW55FcqA1JNlLbU0GrSCSvha3JMwegV5bmQBkOzJMGTS/IRTHfW1YFOVB1PHmQNEn
QN6ic8zBxoWARWgDAb0uBYLpMlYu/olFxbRSBQqqQit6Ry/Hw5fVKhYtSAPYcuYjBZP7idXcCuJ0
kZU/ihsjxYkJcwa6g0b4xiBuSBCSJMuUC8llXYle98kYnN0vuwkoMO2mhBxuERNEnEwLOVo+V5Tn
uTXcwG/rOkgtldMWnVXOHwRaHUHHsS+3DS+wPeeBZBeoN7prygiC1nJgSlOLDTse4SVdz9BQ5K/T
MQlbNugpvaK88anGWR/VzviwFvJHFVCsMdVxUWMmch5e6DNY9Zm44Y2nOPc1yP436EY750cEkNK0
lqdlodIBhZrtgkVnoOBcTfpLMopRTO5EZQQcF0GY48CzzkyhUV9jTbdTN6B4Skah/67LBR0s/GPh
empgN6N75Xy1U9ZsOCi7hzuxP4NG0TTpJ3N3R8E51H3sb8Q7YOIyqbGlyAR8BM5zCewJ+sT4SCk+
JUTO2wug3e+vbDZbXJLfb+t8H0kXHg8Kn52h0zQeLZWwmsUQ6UzcACmTl1vp4dR52NcBQOja1POG
RCUOsH9O7S9AgEY7cRzqh+ubObIbh8x0QYTxrfGzwNmWyKvdQGvargpZMgOTWwQg0QwnIhgpXCDI
4zAYcZPnI7TL9Rzx0+fuxC7wS8z4Nxj7NRMZYgLONalrCIrsCyOfwdib9JRU5v+DUMzR30OKMfVP
H8W6WCS+FLpbmiFDNt4w8wyH/yr2fV8Q4q874z9h0T+7okJa5/CUtMCV7yA/TtbAACuWeBlrbEUv
k5BUBy4QikcLRdqwivBD+CcLPAZbGNddWyEwI19seAHTZP1oDD39yITR1YJWEpAqKfXuRost7Psj
LhdNfKdluV9OHzrodzcO6pFToDC87owHOFBLmU07LP2q9EXflzUgEMhCIS7WnJlxOXqCmaZIE5Aj
BZknrGlwC1V9HB1QJEAjK3uWWlW+bAqcxG3PcvaiS1OIahRAnj3ImLyUkQMfAHleMeadx1G3tnXC
qGaMX+/EEjTYzN87Uo+0xZiH0f9fu1ewAahqqPqH4hwSyv3SgTDhNWHhhfKCYDCrStSWw4yyUrXX
fW7jfI34h8OPkm+tN0Q80eCN3kTrqsbzLVPnduITPPsnbbt9OwiCuHT2E3njVHxehBY8gwjWLPQ2
T7KN+C9xuBbJXvtLv3mcgVeTznT9EKWdVNpJT+RASkawFhzDFMxpTU1sJpuIUiytUE7fy1i1W/3s
gDuu7fgvlM7KBko7hNvXxAcX6iW+x6uY/nq7YaVB1OF73Vt0ZdLPVMxGbD1tlwYHpRtrJDGvb4ic
844OXIYVbP+iZ5YFADRJyj9F4+eJdEFezeW0VJ9Wsu3bS2sz9xrjBY2DStrOVQdRmH+IWsei4LFH
MjCL1b6fwiXpD+UGqeydm3t+tFrhgLumdGSRdE8pIo1j8jGs5trTwkGyEiG5C1wLS8vFxQ9zc0xi
ltaCagOQAca8A0AIx0sSujArt3G1Gbunwn/WLARoFzbSWtIW5ct/GPKiOjqwwdyjZYFjy/oV5o31
DwSMxifupGSOeuDtyHylchRYuNFqZwa9TBIxVObm/IVwp5/oh7zOQ9b2ifqGKkMEEY1Cas8YmDrQ
vPh6AS4urwCCe0kAurknWLpBYvgQB4VBfQp/QP131A1kIEob8HRjJcYONLNAQYnDRgV3MoHMy1Q8
6V7W1LqWPyCRHGijBLJ33P91x4ax/4JTkYrQBi+XMiK/laiExtVHsRtPVvQs6ArQk4E49v0lsNlk
d4mfnlxLFwU2IywOS6yk8htyGlITj46qqAniVxApCZjY2ugPxYJbCeMYPzuzhVVSSYJ9ERi3kQ3V
NXNoYKCas5egIr8oisWjsXW3k4yI4Gck0FdusStOiJR/LgzBYKnUJieounfb0cdkUk+8B4e+Eg9u
YJIN7i22AJ5fSEGNSCT7Z+R5FxnambVIJec1ExwhuacWwhXRwUjILT5Fz4doMuaJeQCmimJTWslR
LPK/C3txsrfF5XQf0NC7Vp4GXQtXVUa6HbcvgSRFZEUW3WxbH23AhQxOqSyr+YBfvSkcog/XC0Wz
gHrT/YcUeH/LPYTBIfdKqBAGWdmnH9aF1FbNh7bAVXlzh334IsQGj+8y5aUtqNxXFo2dvmwDXj9a
DEPqUjis2YEmbugtrTW2OVSfWnt7JeNyRIxdKj68Owm6k77rVW8oR311jSyenaPzR+bGpTA7CzIN
+vCO7ivIZdqe2X2qoWwwkSA7Zefl02RDSURAfcwDhShtKugZ6tETfOSvDZBe1QPM2frUmCDR0Ftp
7d2bP2nEOXvsrhXChMVn0pkz2RKCzGreVy2gezJY8LxZ5FS5wWayxUCVKvYk8a8uokoMEOTVlwvD
36JC8ys3zHBmFrfIGpD0vBCLbgHMY9F2wTUoGv5yp66/z2v1J/SljT40rishcbXHN1rW1ueMY49R
QUSndfq3HZpbJRv4Hu7jeOiZk3aQLNuZ0B7TJ5wQ/lIzeoOGOY04nQohAF8/+TnMtxvkjWR8M2Yh
q1QZxKUbfVlKYpXqo7Y02vjmfBKNEdT3ksfhv2jdTlSc6bU9nm7gIipErUhR4PHjqpcdYDEJm/Dv
R/yq+KeIhr1sN+6iNMam7gonUhKRXUrtKRo8bGlbgS0+2ZPEOcvPSz4eGWjt0bsUsIDNq1wlGdB/
97OcBlMnMJwjLF7kQ/dDLoa7rDTHZXlj5/u/baDvrXe3ZfNIkJppOx6C19sui6GyaFHE3OseIB91
Ft21eIYHp6wPIgh5PzDB+JZ5CYq7Ebs/57tYyc5MdwGqUj860uS1p4Mf023Z7KoJLZpcXJ+32cOc
K5qBM0FbkReP/g4OOSERuhwqKuEiiyrfig8rzFiPVXQp5TerD//J2TDq0/r5q2L+k9BmXoGUl4Uq
oo7JbURttir6dYF/ZDbSR/DLbJdTZMg7s7nZcNI+B2WavN1OQwSZqvPFab8RWAksqg6L2Gso7bd0
T06RmKWLQlw5lf3Z9yjop6PN/DmDbhBHavvbOFrsq/fb4jb1UK1BMAuZGnC/NUxKlihmCk8nrfN3
QmusA5SGI9+h4C1JJ66uHJy+bYMdCc8hM/SB5tRUwe10GrSj00zuoqzVv/fsanpTLW0q02Bk3YQA
D2pFOyRmIdBRZ6Y871+AedpKNh1PsSHMhCSNUiP3OeBfa33STRi8xFEYmDvwBIMHtIEOl7p4YLPV
CCNUPSM+YMzKGknhon7S6GclosSsVGcKllMs/5Q6p1gkL6UzRJC7W1s5YsZi5vySxko3ml0EYeTf
IyBMwJkE0TTRI9xsKfdH7fwQYcBw3rpDH+pSSUkXCAU9LPf1DNitx+M1Ssy4SPkNL5/DKiAeVD++
juMwXWBC2y+H6BjoG+79BG3fvtkmJuSmsy+T7uRMQcYmFJoGfBHIbH/foG4RjSjhboNY24P/QJ0I
y2TxDA0NQYPQ/6l5/88EKFTZ17tqy5/jx1BEzATRA8rNMrAug7aDBOqMBeAgLuijDhr5vUFvm1ve
U+bLGYATOKzYa1OB5itbF906zWTE2pdlmDidtda3oYCBmroFz9kraI5NWGol9nblluvUP38xyxoo
dw96Akhu9STqIInyfxJcogG0M6Pa4JHg+HGy+KTPuMS97Q4g53gBringhbJ5yDW4nZRKXveVIW8d
M1/jFUR7LAm41q2On4YS1AATu7YbUt0sZEya7Fd7xxwj/nUZLb3g1nruAs0IhMLWl8UopVciDRCS
Hjy9SEjXPuyV3rqWG3cDT+kRc/aq6xNlSOn43cS/K5MUC7QTt2pSghCW9aOcL5ribw168MkJX40e
HlFXJ/x/IP7PhYFugzGTajtoMebxIB53ntMkP+Jo9AAyiU/0NTukOAMworwmu5936BkZ6NjAtz4j
MRaQtAli07HsKKbjLfQMd7d1F9MpxY+4vzczqgQhtbCJfWViO5/gfJ3wjQUoXVo6lwHAn84n+Tmb
u1JltGoPntYXmL5KLk64z8DPMzm4yKTyjEiHCNFPd/b+iE3ArY+RmJrxHgNhzIPOa9xlPYbwOzY9
8qbeYwN1xN1qjdKW073kVYFwemiywtegydRc1BzXv4D10Kw5ek952inUyw9lsLh1TKLUdhW2s96j
2V1mK2CBR10lN0RKt44IVG0eNmJ4HJI3Arje+qRMh6L7bFUU0FxcgtY5ikdbEbaA7yYTTnOjanVJ
VEYKIDN7Df+6+mATg2R2gE6KWMV2u4RPgGnhFaJTx9MTeqfv5fOKfieSok1y2g2BEmaAuW5lBCQQ
1vGxZ5C04ibdZdS6OaLVnl7oyoAVqurj55DmGzc8otUo1NTvq1uFaxnf21e4XAivcIK5F8Gpsfmg
NhUidWpO9dj7KhnAyaYKWGJy2gxdXlVsRYVBOGYFFmgesO/OOMHSJf/YylYo6Nn/RDvw2qMl+3bX
l4Rc52wAdJbfuju0OAiZ1jocX6IZrxw4eQKvpAGR/gIdP+R0bs3b8Ghvcv0cmAneFHxHDSzb0gmY
LZyXJbbLZhH84TbQQIP4/oajUqMQT5am2L2EukN4K3bCLeSt98fGSUED0ET1TdXaMJMEY+15SJRs
nnpSCccFkbsbSmdXt6giw/2VYyZjRlloiUyXCFFKpC+/mdQkWr6x+LTTkwaiM/WY/zmD+FYhAVHk
IrpdmhP9bMqAaQ1btvEm8+f+Xl3lgc93V1eJNy+sq9V8q6jRzBUUrPrNZUZOnACkqbbVeSdr4eC9
f4CmMrdtBXAanQrblq/rxlpaUUE94NHUsm1PvZClB70fg8KeTXbsxoO1x2u4rwuglGr/egXNgPmh
Sp53O7Ug3HJxT6nUfVSFOTLZ/u1mYVEf/wVyQEAWT1nfk7zg1SBx9kL/fuoXvzfrXFMim68WgivH
B3PGREE1Os8HHSQIbEy57l5vCKPqHBbQdBMxnXJlDm6n2wWUe3evlKgGlDlPaxmHlbY5X+xP3r66
JwOXgstUOzicfbbhmiU1Yiksw7NpPW0nLdmHR5ulYFTPz+aNxoKKfeGW6L6UvIgxDEkXOZo0ytEo
60Elp2qMoiu3VPCRd+ZC8w70Pap1rClYE995PPASCg/OByu+QxYosAtmMvVMQYA8+7RMY6NdxM/G
3DaHPmHICmFRYpuAm26qVWR+HfQ7vFT9lCOGmWgT6n8obxDbaFQRJojP7PV9W3auclr4z2H9INff
l9xIMtUMFDbjI/4jhmI4+a2bwcPY/ocES1wIklL4R3l94B3gLv6KqkQ3edQS6s6vvTTKEml+wBhb
6B+UojaQQzS3jPJ4ujmmSkwUKqm4NgI7a8uSezNYCg6rRrV8uhjDLO5nEolpLk7aXOs2O9MmMNSw
8ZnyRoWnmL76Yhxqjye4ixH/UQhH7dpOEiWghG3NdvDXXAQx34kuEL+PVebHy6nYRKMctK2JOMB3
G96O6XYN1HvmtTfQ9J3MnohUFhBQkIJKw1J0sPhmxJ30YyKCmtVp8RuLWpcrjBUHPEalJgu/kW1N
qkYmtnX9TMmFlLgVyjNZfkKqTbDx8ZQOEVofgUeUFfh8WvCRoOrmLAs41CpE48SGCS93N/sAx64z
Wh/j9/0DQU0FkT8m6IIlXIDJIDyMLNiCO+pXRqTOhkG8ra5rRxEFREMTJWaUjdgRFhyhc5PhOS0I
zW4fvN9x7g98DjC91nmLGZAz/9aRNd7oGApiLlyBj59u5Zk4JKt9rXB6qEbCo+Wr/NfwSr1X/mRv
auLUffuXV7bqJpfc8lt2uaheo8YJsu9wPhBRdyjrmc3eZ4CUM+JIrITX8G5K6KjzMY79+YXvH3T4
87xRvL5V4sUnduDHOB3OFx6jGawBOPj6go0QgA6/JDrXR6DwDctiHNxGGE1/1DvJc3D9kbprOzbT
i3L7/Eyda0y4+aq/S/gZv1lyoX3iTRqMshq1cpvhEqbpxq8vTodSypTNWis8abNwiP4GSq4pyMr7
s6wwfwXgFN2MZEIEZ6zHZOR/5gxjlm8HUC9MnBnLxioNcmfkq3bkilXATSOD/PpgDELWwJrfXLYI
EADhHFRAlVAJ6yBm86m80bIo+o+UcbAsywy5bMhXZnXIYZmqIdLJU1ncvmcKx8SieZUhvMlI2zwu
GNX8RqtLoEY4nIgWz1Yz6f/rZb4PvW3LWQuHTw2WKP2cu3NMhc8BXsXxyYOqIVEhIy8nUheVLXmV
HisgVVDVdqsE+C0zPi0iT/BnqqRZSjO2N9mrW8xuvsouadt13bx87ZMs9/z6J4xTrZArUpQ1TM4D
71L/C2GjvZufxilY7od3S6V7FdiePK9DjD9wrKS+qL47jUmTlW4Aqwz7Ee5OVKK29oK9kJ6IVCoy
Lu6n6awk8QIzXS45Hc8pBFaCwBOTPyJAThFvedxju1wbgNJUaXbSrgq5/hLilwJU1LGvsopmLgu+
9Su3PyTmR9KPwct2I7UBGQ+QNHYCvjHz2ZJUfw7kqFZ6OhiZyLIVBxQffvu85XsHxx2pev2u83dO
0f/iibgInHyA+DZVqJxRZfMBLC5ozq3KvahZxSe126zCmPj3Mqsc3LvDskbkDHJkmLm9U65enzs4
PxtTpj3xrHkLOglFLspLiiKdi+0RcESIIIWiZlxJYYy8oiv/xPiz7tvW3539qGXImZHYoM6xAOcr
TkoX2qY3XU0qRoIYWCQxHSDyuCONzKIx7O5TFQ17vsEI0TYHf7GYr28IKrOQNEIn2lEg7VaJP8K5
TqJPQwNf6dDckePtrCzk7aB90ZY64so6KphUl7iE1pc7w8Lvz3bDKb7UoqEpC+AcsKNhubb2jNsM
fZrj/wWqnGX+RxrRCTyryv6xJnlbAlauxTcpX6g1UX1K1N/COfXkM5mlGfaw41/gD/s8QhHn6veq
INgmRHNPdYg/pAODka3xtzm186PvgsWad2G1AUslEsT+1vRFRb5ZrndtcxcVjnyw4IShKtLzm/YL
fLWdsZEjw5zgEeK0FExteiTwXyzs632RDuGxdXrSfouK7RlVoOTTV6lcjKDrDIJK5SaMX+I+onYp
VfKRSiIXUeF9Gl2VE/d4S7rY1PHhDcKPri8cfmYsYnh1eXKHGkxLcuXvyioMMTaRrz0DpdDmLpXr
zrSzUdJu2uBgAXQaVY2RaNwP6ChOFO++izkH2hIEY/ZiTlxDEx830GXSUaX2YkrE0HgAMcb5aapU
JOKNT03+FdTEnW6MTHL6y+vtqYhZSehPofenyD0b291uD4OlN16mKL07y+7BpYo+7278oyXHNSPL
EOn2sQSKVXA8LVud8p01wmabUtbecjbV3PyfDh2n6EbKOtPKvhjjMBe1K7e+fKl1KT/NRyBjcsPM
XBntnhzTtNc5PjXrXOUnAEX/6ct2hB+1QUoC+so5etsLqdnIDtiJ6WDuldzIKxHZrt1vWXaNS9y8
YsgQvGjKN4ildW8VgqvJK2h07vxcZTtjpuG4WpJzLZbdTeENXxMdFlX6eyIch6/oJd1L/dHgCL56
fGPa9hua//6V/e/csjIIOcvF2eG9us1VhGVau5WUyvkwAPWzUi6gYD42P/UcTCqwOQzNM+xQfdRc
Tbe7gX7zSLDn9NFWVDlraotcCHPre0F6YFK70soW1wDsnqi93jylxuaslTCFZWTaPUOIO6wJQ9Vm
RrhX8N75qPdzfX3jq2Qfp2M2VB5njCy+WFDdG9MdLVB3QFUryMzV18M66Hk0q13cZDOf6heDRweq
5GRIMsnoerATjGuwSkPXkYuRBV/IGPRllTKtUHbfbmgkrduug0cyuleW+WMksfDfHukPGfPuHQQ6
5l50asX8uUqSK9sieqQhN/5oooKWOa9xWSxN7qZPO1Kz3kjAUp+clvEa2K4jDgTlR11HZDqXOmy0
GU1TA87JBe3sSBQz0Poq4dW79rUkuu30BPF7jcoUli9TtOpY9dElgrnRpXAV5ZsvK/h1yyjF+jPM
8ELglLrF7mEmzITRvtv+qHLISrlTRtM7oz4sZJhrM+4BrNxynfh/QM+gTnKW7hs8P+QtwQ2cqOC8
IZE3DVf2m5yHkaeC9aNsKMTWRZU2iRDUR4VqV1HK7nClzdBooDhoRlM12E/77fTR2NkdWobdVZhd
gORdBT6y5SZgQn1GhWUdWdTqS0Fkuz+eIGYiSLdtZ/fHkk8c2iv2qKH+1vKxsmK/qtvITqR1Y+VE
Dv/g/4Hlc81eKi0+ZZTedQL49XSNt/SKNilqacM0dghZDaS6ncq4/9utCcejzTt1VnVtL9tY9btg
ZoKlwqn9e369tIQ2ANsd4BaQCfcj/XFmXWfUBqRK8zEgCFsVU5c3qR/DurWqQD2OpUpNBpMUaTRl
LsxZPFcgM6lGNgVZ91ToGocSMZ/RYrYDZVazvIvHgM3PyDexxnyq4ov6njc86I/BXFtYLKmP3RzU
0Fj9AgTyPAyrNMY1uCAZd1mSx2eC3lt1Dr01tD+fBj2Icg3oYixYndDcd9j3vCCZkozBfdG9H67N
DlzBu0kCm5oZmjlTv1Se1YCryW8bi3p5rrIxxyv+nbhAHJfvhD6eyxmqEX4nQ3Ekhex+DN+RHNVp
96KJMVQ2yC/+rreMycQPoafMK1xP4uWVUQuN/zJvGGSqmW6kry0Jhocf9+Vs36S5xgN48CL4/F8s
KwfrhU7P1627+zqNK9WIHBTJJ/iBKiw7KXRMgyRovH6NOwhlzEOqwKA/kS4VLqfQFAj9r4WiZV75
lT4PRHFm2V78VVLdWBXloc+Q41kyl997IZdZrx8eeUNbX+U1YNCtA8vR1jb5t+TzH/pnTtQcjGpH
hV0skaKo7lAmv6GEWYHPOrQSGiTpDmt4lMybDhvBAremii1vgm/ED/gGjEIewGh/xtPNT0xmOK8G
EZroe7eQHka4OUmLAcvO1u+WSI1/Z9RAbpdTSQq1seQPGe1dH4YK+Sz+zBbSvbAl+8nUeK7Jbrr5
wlqUOH+0j4xmgBbA2nb9m81X+tdquoZQB98288yySP0R4052SypkZVwxtTkhekEJU0dqLhiVIsDj
/3obAkUraVj6KNUQXc5uT4ke/z5OrUDiKxRl5UzQnJ4so4+TqPutct4bh5gvEpnRwr+WkvmJPGwl
WES1yKTlt4ODsMgRoajkowPk/E4bNe14u5fdE87vf1WxOYZ9kOHu+rIXLjqGNVkglc1/Hz9M/0rV
fo3F05uF3ZVkb3iovKAWfcO9oyhb23H3ttsGoLQ28puE31qdb15fNbOYhaX9oXI///nulB0jc3u7
xu7LKwyn10VgWWpFeHpVdW2zAIA5+Tgo0F9iiYx5s5LVr/fNdFj1DOho/xdYJMG+mUqfDd8DPQ+D
INu1j7/P1YUZqVcvb41A4NtO0lvAgWOFuh2TKBPJw3b4AqjX3VF7yiuSqBq/rsoZ4kKyAatPNCl9
JSm/tO3+Bv8RW/KfnYKGbxRk1dLlkJoDlTRBs6aBLhpPr+tdZArqsCvMEeTIXMBuGcttOE1t2v6G
d4q9aW/iFJBzWhki4gon0Kzfh4xhR1sq4p2+6303rPK/hH4LTYbrdIiGYnkaP0V6ZzSAoZVvPnQ7
LzNLoKgbGv+qLLy+IDF+mhnfouV9ITljH0apPsp6kNoOx66gnjUO6IGfcrJoPgsFYOHGqyyvRJau
SK7u65VoUHPB73NH+8FWmZR4VKAwbxtAQb/AItsije12LYhbc2kK1f4fgprJjvlt+q5ECRZUKzR5
Ti05OF+q7XrHbWlC1gv+pOvbD43a75uljMzM3Lz/S74LsH0XEKPKJYW3uEFr0xT90RgkjbadLFhU
cNDqSlbIL4zMSUpfrM6mWwgeHIF9qhtVI89Rshmki0AW3T41pshHL+qbNSUIO7LgM7RWBJOKe+KB
Y/b9SkYkI3EuGU6maHmvyGAOWE0ynwnhs8LifEhotGC0bcIMktYFKLTEV4Jw9BXDS/OTYbuBbaJ2
yNeB03tgDorSeeUfiOSwf7Svgb3+eRugyyyHW25UGoL/ptQDWL97s9T1fIWKwhm78QPgIGnqzB1p
7ifk+bjz7bgt+Fc5JeKRcfOUoP9rQu+Ia+FKKcd5jga9VLMJPMaD7Oseouw9JspPyzJlPRV5v3nS
2nx4KM/noB0TBSKYy+SSaRQgoew7FTa2rkOFiSR/VPTR/iXAhPlqc2GiPC0VphwWE+wmSHAeKZn8
zSN0o2UlFfv1YfnR0nOAWSrbOGPTIvCLeQkVae8HrkA2LJdSIJnqitpbTNMUL+xSwA4aAo3XtrrY
HGjKcFoy4+NOWdKAC41CWGOJfa2jZMgKVcM/iK5u4uI5KeU5vTnVn3yAAdFLXZKJA0/bcc8Ru7jf
KZkO0huZRrrS+Ufv1BvznMBP789e1mbCIq4EqJPUaf7vLq7nM/RKCHLtl+ZThX3PXWWrZwWa6lTN
NbTTx79ZgWcqUQaFkRzpUTf6i1V/ZBB15wJvEOr60VRflRIensJJEoONCRKX8de6lWcoJsWV1VRN
S8I6T8lvX+WAeF66wuLrLDF8JwEU+ExMFoZNkIwT1Tf1lrOqyUf0hEu8ffgzdLmvtuvebI+TZtj0
ou1FrOv3Ok4XDBxd5kdxLWiYNlIBzIw3P+cVx/iWpizhY9Clbq7wR7j9duxj4QDYeiF3TGLg5hxA
5ACDyufvVEfEdN69qPbPMegcSx7SMxcRqPr3WDkG9DHfykhKdpPicu5tRM1IoeSo1RFkmIHKou1N
qdD5D3+on65pN3ugjrYx0Gmw3zgurg2q2ciR9GhWsbSOZxjRfIMffWtHJiQV/RFq4kM1K9ypxOma
uLbl8Sv8+AeDBjkR8lFv9FYsKHF5LXZmwMhCp0uwcPQa9D84PxIwxrJr4HtfXo3LTfoz101h+jDH
O0PKZ2AEbEM+ivd9OEBxhlsqJGQILbzpTXvkgKRJA+Usw3EOb3eIe8+IZjooxTSOPXU45I++Zic6
YUnd6JLEDNBtc3C4gFpQQsXZUedDsYimi/D6gA1e2DzVFIaUeJEpjFOrR4JMHqQdA4oo7flUFWZk
qcBsE1Uha02GhnYPTJ6g7fHoLDwVZFdQxiBO2NNxUoiHD6a+5GVkfZzQVD9HJj5WbH/aLDrU0Zbg
Vtn2gQz/ynAaC2D2cm5Im1dCmKIvebcrtJKHpslKQExLZsUWJopFqQqZJiSoKE6dl/MqyEp3FuNt
/QkFgO0lJKYJUgirw5wP7ZBB62cxtUbHsHfv3hmhGpIHHs+PK3geCzDruBLSB64+YytqqSpaOAPf
18SIQTo+5/I73SBk1J/rgYfEyHdXCQDCfEL5n/VxBZbE5usl/J8tquUX0NVGfrlHUy4+7CXj+/MI
E9AXKY/8LZiIx32V21OHMvayt3/IYxhssaA7XINFfL9raKnQQjTR+hQyTiuSG8ZpMocI++Fefpl2
aTtwvTcpeVAdXbFOcZJhqiS6B8jZTVh58bj6c28L7oo8HbKQUGjumxCkSJx2Tp58LZkpGN6DfM+k
U+2pw2Oz5r36aeyqsK91iamrsOpqTKNJgneOSGw8uoXuTvvN/gbHC8tJm/jv0mhuhNokkzrpfrFt
4oyu6qivIPILmlgZj3s1zCoxu4Ie5F6S3rwAc3MqMrl1ehf9z/p+agxAsxgBrFfyTDRCMcviyvGj
DFG+l5tFu0moXTYZx3NC8OoTf+a2Jtvzb+CyE/zNyvE/BcsblGu/hHTYi4JDTg7FJ+H9HPCFm4Wh
TkbDhKk20801mUSUYW/sc+2/MFyWjdEe0vrx80MgIYRZ67E+MZ1hTIfTC5E9BczPFLuqkXJsjFy/
e2SkMBeMUmj8HtIpiEjgc1PFks0GMIViZYWruVX6RZKVMSxXJt2/e4ulVdzR/dWDnlaKFK3WCEOf
+IaskmLACu4q8BCA/O0cOI/pvIE6VTXhkEjpp2oyguFMx3cswhaUHATp4X0oTbJDWyilT2wmdVNI
6VC1Fn5ExrQj8pxThCWjTMPPKw62PDUjDK/BYF9NATTG8BOLn9taOt3jAg9QNCzwJjEPa95eU0ln
wW9k7Rr5OFyhWBc2NVDWyFuKB2m12Pl2DNa3WiCkYQ3VZD43GfyjSBVEhLJFmxs32734HvLlMofY
tEgArwX48IClIYC5KPP9GZgzVVZeBG7OwZScc6ztWeHboGtkTEDC5XXylUBvhORezw3pBBMPFCNk
SouKL5TQB+x4H6zycr2UpBTURtJOAXZUv63jfaWX6fKB3rNbRx2CQcDg++uH5f2P4sVrmQBqpznw
WnaGLHdHnRWoBYV7gIyfl5BVtBtVhOw6Gi+fiIhBkrHVSFXzg70qgaJXhye3HRvAm+FM2Lv/Q/5d
679B+YGQqzzoYrzZFWBzGjqFl7gVxpl6V3jszhMhw05NDrXfWo+X+I5T+L4rpauzZ56KH0awkM9s
1dBiPiiwdtrbrxTYD2KT6w3yQsDANEhTfewqW88zWoXe3heQSn8YHFLKZpPMg+KoI9Mzn7z0vOMs
EoGpeXsazZ9MqEWAo6FTClERTgZ1iZHxiFTqpibBkY5xDML0JnZSxN8P9OmAUA/iVIE63WdpZWLs
ScXFaQ5KXhbBAFhj2wK0bHIYVMg6wJdNbA40RDrMWQJPwsQKNtjcvq6yPiwxH1pGSpWbVK2Cgueb
rHlDnzl/yOUOUapE5yZTKdxedCnQdDBMFhTdQvnFo6bGkZvIkm08ALt7PsdRt/Y8wYGcGRhTiVZw
o43a3eFz5/TPnpntqks11QsW8Ajq/tcCboBq/3mhAvMw7mN8kR5WPIU2RYPHKvWJLB9iUFOMi9Je
cI9PT6rWRgTI68PIknUE9kCxbW0tXTDO+rK2Se4edkAYzNx/V4e7vvoYQAntVxsWLTHjFuiqlon+
hDa3l4f2vmprGgw75+S4k194CFGckicMurN8TQhvKFiOL7Z3CR8R2YEmmHGUHeBNkDjiyG/vL1k8
LF0UQr6KOmlxiskyNlpZEA31KEt4nEhh5znto6TiO+mK8glE+4L2vu8e5aERg6+HkxZ50E38YdBC
ZKBN3u5tpHqkU8zvSiQWoV8vm1Eww13DjoOnvElM+OZK9UyFaRW50Fph1fsqES+n+LfGfv7sNya8
4duqB1rbb/vN6G5XekzWAYvmdhusr+ohdoCvwNqWVDPHCGW4gOqoyXyPjwGragB4b3FIueHFIFzE
DgGbvdo3SeTqkJ0Z9ldPgkTmnTacG7qaQX47kQth0BmvnKCIlVjMoQdRJgP3DrMBu0S7pSUfCQb2
6ci8D6hpgjHK9OfocwisRX77zko8VLonZOEB0/i7ZnhTAvu8zgTn+yuvnkhMyq6mOlIzrJhdKN/2
8G8wp6TGwEiP+25CE5Lr8hWURps/R/8aqMZsBoQyBrz1qPTgd3y7Zb7PeOk0eXx69kZaq65OHNMJ
GF2hz30AAL8VFlXxTh4lCsRFTJ7nb5HSdgBC84a3ShWM4VJJAKiuqWWFytENqBPFxfrnLsmozsuu
yhNxqzS2+mu+gaxBggUa587i0JuM0Ox90yX76jH6F6yVUrX8VR8CWU6186tjq06jAdJdvaWDk+GM
UMNczAqyYfyLKiwWxfN2BGd1HJ4pgPNv3cL7n0rzArS/+wRxJRXAH12cEuhgcbfXXrZsqdZIb5Qq
4hUWMZ4J1aqqgaVXCX7Gf6lp4MJ0W0SWvnYgedFitylngyBBDL/4H6jSZJ7BueIgSTyda7t/47sO
3MyCFbyUxT/p6M2JKP6KTw2+uQYX5JhrvEMy+UYnHHqQU6UAp3cpUzQMgK3q910eO7Z9P7mw5vDo
UmoLa+U4+WPO9DJE8fj8pqg6Ec2PUxx7ZDo9r0pj8h/Y+Ja1ynbkehAz67aKbyuO1S/AVn3WJT+E
q1p4Zpn6qc0EEUQ1x7soEyYi7crMZt5B2YDgabENKPPga+fjh2OzlrJ5dsvREYSysytRO9pZVS6z
Qb9MKiIWHvlCkh+3zKJCpVQvf7htyRHH9FRVJ8JTOtzjFhOQGCtua1e+E1pSpgE4Jhs9ItVQzFzf
3i4c2idtJEK1HkJA5lGTmxoYFKXMe94vmcYoMHD1eKWheVVzi/2v/yT0/rL7V0ZTSdsQ5GiStWYd
yQsfIf5Lxwx2e8fzOf5iQZWZyjnrZGy7By7md5um0pqVxCablqDEK2FFRwm6muc7YasixFd+pDwn
cwX1kwgx2hj727gsIcMKZj4xEOZbxoywA1hINIt5lSKf+y9J17jQjyn8RDqXgQPwTCt7QG/R7c21
1tC+ddvGwGa0T778g+jNrJaGBsoMDxQGubLXXRlsZYY21hbslMPkpNizTYWZwBqPg0LbNo6+F73f
RjpsPja5yG/DoZfxlkXZ0DC/pRPZu8jeftyi5E4uH0Ojh2mEMqyVKedwJbklUUT69e7Vu7DFxMPB
0CERHILSv/UqqCMqdQyZY86USmphUNxMj6CcHr4frQ09plAknycmHf6657BpqLW4uSpRQdrqNpAI
dtFGwQ5X7ee0iqr/EFcunXMcNRjYpCsPzcr6jUfyC6OkcEZ66k7Iu7twfzzfEsz7a1Ur/+vk6Gzt
EflXPIDlCNw5NZfJBXFn61MkvT+c5Zrg2Pg7KKRE4C8EidxUYi8GL+Bs2YSo2lsfdwOLXIA2oEks
z/oe70JEip4HnRmSuMCAZiiNWpgXCkX5DCMsSmtjDzHqsGfJH7KWGtlMBHitYW81TL+/SAtiRK4/
kioEIsGvMuoUm28mDbgevEW5E2HTn45g+Wk8lMBwszavJfPSh7Od/WOlC+RHVq06TnxDs26AxQdO
aO+wrEfuLiyyw+hLuFWN6fQWg5/Cz3k3urQgJcXMUv/nioKz5UQUhfVXazULLUTQZA56xvb90Pac
JI5umoiaYwbTQN1Sw0ukSdJvrv9Ty358ty2q4yjQ/LNlGsOY90fNnSmbzO3uZuc7lV0soonOnyDM
7Vfl9EwWWxTNGxGc7FTk8DCGbcs8jfMT9YW2AcGXxupAaYz/pyppQDeeIYD80NY9greSsBBzJJoU
N9vQALET+K+f39CeUvu1G9Qf2CUfOFfWW11KJkgiafQC6wp+Al0DJa7EYUx97xLaL+6azom61QQy
IbBZIb8SKqO8ij8uAtxMpTNg4PzY1mI11iuxDJyA+P5EdwTG8DWaveCM7g3GtD63VY4m/Y0BsNjh
tBW43VEToDgAHIixB4ZbUlnJYboW81qjFqkAgY+Hyk51hp+ziPnlv05L2TBLuC4u4BBDYm2Wuakf
hZIGeNWl4sl8kgiNrx3iq1TbwUG+B5hEkuP/Up3ETvnR0+VRnP/1cR4Qz7TGD1E+2w7X4/1PjEMH
yLHTl1oY+9CzwGYn2LSO2oub1BmK7ZamrPVKfo0caTXydaAPOMT0hBk3pDQQBSuvTlovHwoMafeR
+LMv5mTLLNB3R6sXQlZyrbSuEvmJhbhjE7cKk0PLRmhRDFrGrvFVDPGApYHlVLBh/+dETAmNpfg/
Hd7JB8uwB78wKLsGgAwt70MdhO4ECd0Reb9Dl9W5L7V5LpIO1zMkmSMlWe17YnBWuJyi4afNX02b
4kixZv0+BWyGJgVc5rSIvGmu5KxGJVeNkjfAx2C3k72AGLU1KQIPhBXlTzFrUWz0lv98mfW4BrZE
C0wPM2/Pr6CzEUPVj9V5U5BMjKdCL9VXKwPsjG+UkEtQu1mjXeXKuEuU3D6mJ3Pc0INVA73f8TN7
9NcEcAqiDmyEp/kYyyAn0C7d8a1XxWBdbxGxQXqiSH3wLnE359qL5dnud9Y6jt6hPX5P7o5GXCS9
4ELNJgjymgFUq64NF5NEaYYLfbcPCzA6h71KcVtlGc9G2laWgndsNlcz5MIjgFAcPP4yx98Z5LZ9
x+QzBmAd7mh6zBc50CQ1oWuDjWGpw1T8iPdL8TTiZFT44dbe8sa5zS5Nkvy28yxPzvAJ/i7HYdZl
9u8D6L7YFXTtHDpTwRch8hiXey4GJsyREbwz+EvlOUwAkKOb6Sgghq8EIP059vb2s9QRjEB39nki
0TzK6LZFj8Jbo0w/V8k6mezYraFKu9bsqJ7p7onBwmiSZsjthpsUmY6eIWPplrvX0X5PL1TQp/eQ
ri7qq85aFlEUPZiWpSDUWErUJdrF8Z3UO5mabCmsArWsVCN1WFp6fB0hbjl9/JqMic7hFPT4D37S
a8f7FWVzfzkWQx4mAf9LVp//R2nC/nRlkdUODwnBiPAen4lBai+M3WVeKvMRYls7QVucxOhEhxcK
m1r/Ev4HmFuhxi/KZ1uggXqXvo5OzdOiVoH/fnIMeZPwNeKn36xpRwh3lZ5EFOBB0C6TSAh4eEpg
4Ofq+w+gSuhgUxcVTcY9DserJhfttw8slNuneRZcZVQ4I6YO1zmqLDknDE/OctdvgIaJSOI7P+MP
sXH1fjHLlRWFRiQZXNGxZ7adCUsa0fAbAxL093LAhjyrqzaet6oEvAVIG6turioNl1qNPQ7s6EZn
8Zd+Go2xohD5AmcA1Ybh081MMoh31ZT3B9T9pe4+Iwljr7Z8kLcXbMhZD8lXY9NDaevXBned26V0
EqZW9prFIvyFOrwM8pifXlnlzfSfDTt7r4A8G/WG9kG/fNN3caHVA4L6HS1QiLRVnNpQGxL1t5dj
FBXKgkbQPTGfkfN+YakfEbFtVcq8UG24W1RtiMcZXpSNpYa3RoyimRUeeEApCwV+xkddhk+AQndM
Jt2og/u0GTXAq7GPvsoN5oZ03+ep+iFUHKFNM2uaFzaYOOveTBiESyT8YiePqVmCvPFhjg6DNj9K
hTcO6WkiDzoN6Wf3vU3F8m1dKl/XlytJXjU9wAt2fMUYANLrgnR9b7X6Buqq+hf7E5wJwIAzOJII
dmy82yHvJbBQpfLdFJHTOe1+RzwNmCQPmNS3HR7vBJhe4WK4eOlddUzhRSZ72ERRCz1qKgDAN9hE
iXOCFFcLineDEatvuHYzJMFvhR0ZLxuJi2CupLBHkLRDda58vwlMH/SsKDfhzTa2vd1tOTtCoWQf
8edB9jOOv6Xr/KjwbrXrl//d6miFNi8SWa3tmumGIBKoPiUyCbog1GfyODjfAGUcU3ZKhazWIJfb
UbuxOrGp6uar0NrnmKqvc6AXS45qaVfa+Xodn7pQbZ5+o0MlXzc60pQxmSO0IceniOK4EBPqme0j
jVVwAlwGbpQ5Vf2HpXzgt6Tw+jCFeQI8XZnRZo0QYqsTYPEwnkcp83ZtmLpx3vPOJRRb5A4sqYH1
YZ6y4vcgKnmtjn0pLN0MEdQfxAlStLA+Ooes5bA7pPhoWC9BjMcaLeNZgB1t2Zl5oSPOQqAyLz1V
HfKFIT+lkCKVU9JMxN5ouZw3k5PJGPiFIpw2rCQrrUweM3g1LzqzEUtv7ReOhmogQalawU5xbfvj
RzGtBwd432M0lELYGTR6x3+HcqddpohzcuylVmbyywCxVD5y/l2ZuhIQlSjVjV69YcN/O6PwNhJC
eJh756jaH1vffLbRW49X9ZjMvMggk00HqSIlpAKx3RTQ5bB8TQogDJu6wOWYPX8S7glc+b87H0BC
gxJLbiXJvD/yOpULCPlz3dKla4rCpz+SJSXVxDfUbcG0OhB5eHNDSHMOmigWBcfO9S70MigxB7Q1
Jv/pQomUwnzfpKM7Xt7kKoL3nwZythaE2w7N9bfW92ig1mtsanfOkph+3jfcGSXZWQrHSuzxPhLE
abHy2K7jbpdCRZIWsgtWRQKLovHJEhsS62oASyizFOK0yYBj7gT4Q8PTinMad5O1N6fsboEXmxa8
axy6y2LH4M+FR7JyhcdUbdYzUUi/CGxB0RVc/34G4vkZdb95rVFrylCBNGaOsrr0s9lGHGaHMld1
WBikRnqCJiwPrBN7Uz1mSa8hYz0tzyls6j3lv/RQyapgMGSrlCxLldk4f3zhkhlIHAb5pSPUYncZ
/gr+0HUZcvYAZfUwImdcJ9/BCnBFklyji17N3rMXHXfBTmGEJrLLvsOhIyuJHR6TjgOqOPLIvuk5
amoZtInPNwyGwJ0J5yUVbhOIdOHqEqI2KvEGhNWuf7o51qNYAIRcWsCSTcvwCz+Opy7UYNV9gc79
AKI9V2wFimC1DgHD3ibwQXUpn7VZhQIeh5AkWcWYPb/rstF/n1hKzwa+jmiDb3cOZ2DmAyQo6Uys
iyeZvi0y+I8BpCdYSXdyLS+dx0S4g0fGHS9B7pV50C64hUrwtVND/7Uzv6GgGg8JSsr26iSeYKMV
YVNObq6fd4ECfDZvxIgNAveso8lfJMQ6gpX+5v2IbcCyGMwUufDywak/eengZICceuFxtdewe4sl
tTXqBddjgGSTnElpt7inQMYbwBZNDp3rHCPus891fvegbkscwoGyBGOXhkhlxKTJwwUolODzW3oJ
wo767W7iAOY3AoEY006lJ4636jmNzU0q8Qx66SL2gRvz3AkS1VzfWulMu9yk3N36K5KgUeyCaoRA
RXYsEHrCBMWY6C+XpSexYgHAT3I8ucFfF80YAdihFCtwMbX6FvXhA0sowi3CS5LLNrz4BJrdIkEo
MsdBLhr5UpRRuePqoTIp+evKt+rqoyTtyPAHsam1jyE6qjY8A1ZGbnBZZ3oP/HIkTY06VI9qIDFZ
2D5j7eKHQS29dnUJhmRAGK1P4TCAkRkAyAoO+HzcDMeGWLtTym/cD0/aLr+sge+YDhS3OzelRyaG
OVmKoyvnQtWroIvUZQV9Wxm08h42I8aaSLyBvvJlj3LKpSnTHWBUYQbbkZse7+NHIdSxH5rCw3BC
y7arm+qWtW5pV+HU7zylI/DC9jeA/UkCsEvbzAutPaX6tORKM3kpP/yDqEsMfqiEsGrMf6VBye6R
oWmGhwBSHPGYMkLTpitfJVHnYu5DEAUh8FPNuAToHoYB4WcffeoHs3iRLcW+CGGtLQfNcwNy8JAz
jCChFilNlcBTukFi5JMWTaE86LhORYQHjjfa644uZ7kTBLIrIbkMGNWDFS1Z/Iv4fkh2/UDcCsiS
aTGbXgkhDyj1TaSJsFJ6/y2o7Zp0rem0+3+DAkB1eVAzFCcWSLZ+32ng/LgN0WMuEESUDp0CvNYr
7sAkKNYfK0fv0dPmgLA637M+k9LwpFLpwaaUolOI4yI3MSgB1+6e4GKBZEmy0yO8jQ/UheZHSmXX
k87eefIa6KEJCbLbZc/jYCymAfxNjLcCAtdG4t4ERY/wv1KZU+GeVgDLtULYBEiieClQVzBvW4pB
47+xojP4E460IPWvfPaqBjBZQ0GThwaEeXrlt284vpNkLsD3Xak7xE0kk0Sjft+VrhRlJgRx1RxF
AI1dTVEXfeuXw3OzHJ10znT5A8/PNBcYyL3z0WCE9sjeopyqJxIG/CKTYDI8cWz+PVrF0SE+zMdu
FIH6hw9GMc7dT0lDhQoqF9KKgIwCKLbSL3NEk5Q=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
