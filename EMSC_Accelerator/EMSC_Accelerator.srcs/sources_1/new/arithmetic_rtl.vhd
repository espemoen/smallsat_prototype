library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

package arith_pkg is
    type bus_array is array(natural range <>) of std_logic_vector(47 downto 0);
end package arith_pkg;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.arith_pkg.all;


entity arithmetic_rtl is
    Generic(
        RAW_BIT_WIDTH   : integer := 18;
        P_BIT_WIDTH     : integer := 48;
        WLENS_BIT_WIDTH : integer := 18;
        RES_BIT_WIDTH   : integer := 65;
        NUM_P           : integer := 5
    );
    Port ( 
        clk     : in std_logic;
        aresetn : in std_logic;
        raw_in  : in std_logic_vector(RAW_BIT_WIDTH-1 downto 0);
        p_in    : in  bus_array(0 to NUM_P-1);
        wlens   : in std_logic_vector(WLENS_BIT_WIDTH-1 downto 0);
        wlensSQ : in std_logic_vector(WLENS_BIT_WIDTH-1 downto 0);
        res     : out std_logic_vector(RES_BIT_WIDTH-1 downto 0)
    );
end arithmetic_rtl;

architecture Behavioral of arithmetic_rtl is


signal res_sub1 : std_logic_vector(47 downto 0);
signal res_mult1,res_mult2 : std_logic_vector(65 downto 0);
signal res_add_sub : std_logic_vector(64 downto 0);
component c_sub_1
  PORT (
    A : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
    B : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
    CLK : IN STD_LOGIC;
    CE : IN STD_LOGIC;
    S : OUT STD_LOGIC_VECTOR(47 DOWNTO 0)
  );
end component;

component c_sub_2
  PORT (
    A : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
    B : IN STD_LOGIC_VECTOR(64 DOWNTO 0);
    CLK : IN STD_LOGIC;
    CE : IN STD_LOGIC;
    S : OUT STD_LOGIC_VECTOR(64 DOWNTO 0)
 );
end component;

component mult_gen_0
  PORT (
    CLK : IN STD_LOGIC;
    A : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
    B : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
    P : OUT STD_LOGIC_VECTOR(65 DOWNTO 0)
  );
end component;

component c_addsub_0
  PORT (
   A : IN STD_LOGIC_VECTOR(64 DOWNTO 0);
   B : IN STD_LOGIC_VECTOR(64 DOWNTO 0);
   CLK : IN STD_LOGIC;
   CE : IN STD_LOGIC;
   S : OUT STD_LOGIC_VECTOR(64 DOWNTO 0)
 );
end component;


begin
sub1: c_sub_1 port map(A => raw_in, B => p_in(0), CLK => clk, CE => aresetn,  S => res_sub1);
mult1: mult_gen_0 port map(CLK => clk, A => wlens, B=>p_in(1), P => res_mult1);
mult2: mult_gen_0 port map(CLK => clk, A => wlensSQ, B=>p_in(2), P => res_mult2);
addsub: c_addsub_0 port map(CLK => clk, A => res_mult1(64 downto 0), B=>res_mult2(64 downto 0),CE => aresetn ,S => res_add_sub);
sub2: c_sub_2 port map(A=>res_sub1, B=>res_add_sub, CLK=>clk, CE=>aresetn, S=>res);
end Behavioral;
