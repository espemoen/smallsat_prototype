library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;	


entity dot_product is
	generic(
		bit_depth_raw : positive := 12;
		bit_depth_G	  : positive := 25
	);
	Port ( 
		clk 		: in    std_logic;
		en          : in    std_logic;
		reset_n 	: in    std_logic;
		in_raw		: in    std_logic_vector(bit_depth_raw-1 downto 0);
		in_G		: in    std_logic_vector(bit_depth_G-1 downto 0);
		v_len		: in 	integer;
		p_rdy       : out   std_logic;
		p           : out   std_logic_vector(47 downto 0)
	);
end dot_product;

architecture Behavioral of dot_product is

signal mul_r  : std_logic_vector((bit_depth_raw + bit_depth_G - 1) downto 0);
signal add_r  : std_logic_vector(47 downto 0);
signal counter : integer range 0 to 400; 
begin

p <= add_r;

p_rdy <= '1' when ((counter = (v_len)) and en = '1') else '0';

process(clk, reset_n)
begin	    
	if (rising_edge(clk))then
	   if(reset_n = '0') then
	       mul_r <= (others => '0');
           add_r <= (others => '0');
           --p_rdy <= '0';
           counter <= 0;
        elsif(en = '1') then
      	  counter <= counter + 1;
		  mul_r <= std_logic_vector(signed(in_raw)*signed(in_G));
		  if(counter = (v_len + 1)) then
		      add_r <= std_logic_vector(signed(mul_r)+ "000000000000000000000000000000000000000000000000");
		      counter <= 2;
		      --p_rdy <= '0';
		  else
		      add_r <= std_logic_vector(signed(mul_r)+signed(add_r));
		      --p_rdy <= '0';
--		      if(counter = (v_len)) then
--		          p_rdy <= '1';
--		      end if;
		  end if;
		end if;
--		if (counter = (v_len + 2)) then
--			add_r <= (others => '0');
--			counter <= 0;
--		end if;
	   end if;
end process;



end Behavioral;
