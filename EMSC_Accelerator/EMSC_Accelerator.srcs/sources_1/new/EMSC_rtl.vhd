library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

package EMSC_pkg is
    type bus_array is array(natural range <>) of std_logic_vector(47 downto 0);
end package EMSC_pkg;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.EMSC_pkg.all;


entity EMSC_rtl is
    generic(
        BIT_WIDTH_RAW : positive := 12;
        BIT_WIDTH_G   : positive := 32;
        NUM_B_RAM     : positive := 5;
        B_RAM_SIZE : integer := 100;
        B_RAM_BIT_WIDTH : integer := 32   
    );
    Port ( 
        clk         :   in std_logic;
        aresetn     :   in std_logic;
        init_bram  :    in std_logic;
        enable       :  in std_logic;
        G_size      :   in integer range 0 to 275;
        raw_stream  :   in std_logic_vector(bit_width_raw-1 downto 0);
        b_ram_in    :   in std_logic_vector(BIT_WIDTH_G-1 downto 0);
        p_out       :   out bus_array(0 to NUM_B_RAM-1);
        p_ready     :   out std_logic;
        initialized :   inout std_logic
    );
end EMSC_rtl;

architecture Behavioral of EMSC_rtl is

component block_ram
    Generic(
        B_RAM_SIZE : integer := 100;
        B_RAM_BIT_WIDTH : integer := 32    
    );
    port(
        clk             : in std_logic;
        aresetn         : in std_logic;
        data_in         : in std_logic_vector(B_RAM_BIT_WIDTH-1 downto 0);
        write_enable    : in std_logic;
        read_enable     : in std_logic;
        read_address    : in integer range 0 to B_RAM_SIZE-1;
        data_out        : out std_logic_vector(B_RAM_BIT_WIDTH-1 downto 0)
    );
end component;

component dot_product
   generic(
		bit_depth_raw : positive := bit_width_raw;
		bit_depth_G	  : positive := bit_width_G
	);
	Port ( 
		clk 		: in    std_logic;
		en          : in    std_logic;
		reset_n 	: in    std_logic;
		in_raw		: in    std_logic_vector(bit_depth_raw-1 downto 0);
		in_G		: in    std_logic_vector(bit_depth_G-1 downto 0);
		v_len		: in 	integer;
		p_rdy       : out   std_logic;
		p           : out   std_logic_vector(47 downto 0)
	);
end component;

type b_ram_array is array(0 to NUM_B_RAM-1) of std_logic_vector(B_RAM_BIT_WIDTH-1 downto 0);
signal b_ram_in_w, b_ram_out_w : b_ram_array;
signal read_enable, write_enable : std_logic_vector(NUM_B_RAM-1 downto 0);
signal read_address : integer range 0 to 275;
signal dp_en : std_logic;
signal test : integer;
signal b_ram_counter : integer;

TYPE state_type IS (idle, S1);
SIGNAL state : state_type;


TYPE B_ram_state_types IS (idle, S1);
SIGNAL B_ram_state : B_ram_state_types;



begin
b_ram: for i in 0 to NUM_B_RAM-1 generate

    b_ramx: block_ram 
    generic map (
        B_RAM_SIZE => B_RAM_SIZE, 
        B_RAM_BIT_WIDTH=> B_RAM_BIT_WIDTH) 
    port map(
        clk => clk, 
        aresetn => aresetn, 
        data_in => b_ram_in,
        write_enable => write_enable(i), 
        read_enable => read_enable(i),
        read_address => read_address,
        data_out => p_out(i)(B_RAM_BIT_WIDTH-1 downto 0)
        );
end generate b_ram;






end Behavioral;






