
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

package B_RAM_BANK_pkg is
    type bus_array is array(natural range <>) of std_logic_vector(31 downto 0);
end package B_RAM_BANK_pkg;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.B_RAM_BANK_pkg.all;


entity b_ram_bank is
  Generic(
          B_RAM_SIZE      : integer := 100;
          B_RAM_BIT_WIDTH : integer := 32;
          NUM_B_RAM       : integer := 5
       );
  Port ( 
        clk             :       in std_logic;
        aresetn         :       in std_logic;
        valid_input     :       in std_logic;
        read_enable     :       in std_logic;
        G_size          :       in std_logic_vector(11 downto 0);
        init            :       in std_logic;
        data_in         :       in std_logic_vector(B_RAM_BIT_WIDTH-1 downto 0);
        data_out        :       out std_logic_vector(B_RAM_BIT_WIDTH*NUM_B_RAM-1 downto 0)
  );
end b_ram_bank;

architecture Behavioral of b_ram_bank is


signal data_in_w    : std_logic_vector(B_RAM_BIT_WIDTH-1 downto 0);
signal read_address : integer range 0 to B_RAM_SIZE-1;
signal write_enable : std_logic_vector(NUM_B_RAM-1 downto 0);
signal initialized  : std_logic;
signal b_ram_sel   : std_logic_vector(NUM_B_RAM-1 downto 0);


TYPE state_type IS (idle, write, read);
SIGNAL state : state_type;

begin

data_in_w <= data_in when aresetn = '1' else (others => '0');

b_ram: for i in 0 to NUM_B_RAM-1 generate
    DUT : entity work.block_ram
    Generic map(
            B_RAM_SIZE => B_RAM_SIZE,
            B_RAM_BIT_WIDTH => B_RAM_BIT_WIDTH
    )
    port map(
        clk             =>         clk,
        aresetn         =>         aresetn,
        data_in         =>         data_in_w,
        write_enable    =>         write_enable(i),
        read_enable     =>         read_enable,
        read_address    =>         read_address,
        data_out        =>         data_out(B_RAM_BIT_WIDTH*i + B_RAM_BIT_WIDTH-1 downto B_RAM_BIT_WIDTH*i) 
    );
end generate b_ram;
    

process(clk, aresetn)
    variable counter : integer range 0 to B_RAM_SIZE-1 := 0;
    variable prev_b_ram_addr : std_logic_vector(NUM_B_RAM-1 downto 0);
    variable valid_prev : std_logic;
begin
    if(aresetn = '0') then
        initialized <= '0';
        b_ram_sel <= (others => '0');
        state <= idle;
    elsif(rising_edge(clk)) then
            case state is
                --Stays in idle until either a init or read should be
                --performed
                when idle =>
                    counter := 0;
                    if(init = '1') then
                        state <= write;
                        b_ram_sel <= (0 => '1', others => '0');
                        counter := counter + 1;
                    elsif(read_enable = '1' and initialized = '1') then
                        read_address <= read_address + 1;
                        state <= read;
                    end if;
               
                --Stays in write until initialization is completed.
                --Has to take care of bubbles in input data
                when write =>
                    if(valid_input = '1') then
                        counter := counter + 1;
                        if(counter >= to_integer(unsigned(G_size))) then
                            if(write_enable(NUM_B_RAM-1) = '1') then
                                state <= idle;
                                initialized <= '1';
                                --write_enable <= (others => '0');
                            else
                                --write_enable <= write_enable(NUM_B_RAM-2 downto 0) & '0';
                                b_ram_sel <= b_ram_sel(NUM_B_RAM-2 downto 0) & '0';
                                counter := 0;
                            end if; 
                        end if;
                    end if;
                
                --The read state should simply read out 1 value from each B_ram
                --each cycle.
                when read =>
                    if(read_enable = '1') then
                        read_address <= read_address + 1;
                        if(read_address >= to_integer(unsigned(G_size))-1) then
                            state <= idle;
                            read_address <= 0;
                        end if;
                    end if;
            end case;
    end if;    
end process;


process(b_ram_sel, state, init, valid_input)
begin
    if(state = write and valid_input = '1') then
        write_enable <= b_ram_sel;
    elsif(state = idle and init = '1') then
        write_enable <= (0 => '1', others =>'0');
    else
        write_enable <= (others => '0');
    end if;
end process;

end Behavioral;

