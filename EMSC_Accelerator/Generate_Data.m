%%Block Ram data
A = randi(1000, 50, 1);
A = uint32(A);

filename = 'test.bin';
fileid = fopen(filename, 'wb');
fwrite(fileid,A,'int');
fclose(fileid);

%% DotProduct Data
v_len = 10;
in_G = randi(1000, v_len, 1);
in_raw = randi(4095, v_len, 1);

file_in_G = 'in_G.bin';
fileid = fopen(file_in_G, 'wb');
fwrite(fileid,in_G,'int');
fclose(fileid);


file_in_raw = 'in_raw.bin';
fileid = fopen(file_in_raw, 'wb');
fwrite(fileid,in_raw,'int');
fclose(fileid);
%%

