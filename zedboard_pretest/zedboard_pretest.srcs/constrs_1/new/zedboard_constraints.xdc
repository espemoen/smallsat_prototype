

set_property MARK_DEBUG false [get_nets FCLK_CLK1_0_OBUF]


set_property IOSTANDARD LVCMOS33 [get_ports FCLK_CLK1_0]


set_property PACKAGE_PIN Y11 [get_ports {gpio_rtl_tri_io[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio_rtl_tri_io[0]}]
set_property PACKAGE_PIN P15 [get_ports FCLK_CLK1_0]




set_property PACKAGE_PIN AA11 [get_ports {gpio_rtl_tri_io[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio_rtl_tri_io[1]}]
