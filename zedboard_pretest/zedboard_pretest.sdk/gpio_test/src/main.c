/*
 * main.c
 *
 *  Created on: 19. feb. 2018
 *      Author: student
 */


#include "xparameters.h"
#include "xgpio.h"
#include "xil_printf.h"


XGpio GpioOutput;
XGpio Gpio;
#define GPIO_EXAMPLE_DEVICE_ID  XPAR_GPIO_0_DEVICE_ID



int main(void)
{
	int Status;
	volatile int Delay;

	/* Initialize the GPIO driver */
	Status = XGpio_Initialize(&Gpio, GPIO_EXAMPLE_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		xil_printf("Gpio Initialization Failed\r\n");
		return XST_FAILURE;
	}

	XGpio_SetDataDirection(&Gpio, 1, 0x00);

	XGpio_DiscreteWrite(&Gpio, 1, 0b00000011);
	XGpio_DiscreteClear(&Gpio, 1, 0b00000001);
}
