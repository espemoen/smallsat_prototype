/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include <stdlib.h>     /* atof */
#include <math.h>
#include <float.h>
#include "platform.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "xuartps.h"

#include <string.h>


#define UART_DEVICE_ID		XPAR_PS7_UART_1_DEVICE_ID
XUartPs Uart_Ps;

/************************** Function Prototypes ******************************/
void receiveData(double arr[100][100]);
double calculateSD(double data[]);
double mean(int length, double x[]);
double square(double a);
void bsxfun(double *res ,double *a1, double *a2, int length, double std);
void matrixMult(double arrA[5][100], double arrB[100][5], double res[5][5]);
void matMult(double **arrA, double **arrB, int rowA, int colB, int elem, double **res);
double determinant(double a[5][5], int k);
void transpose(double num[5][5], double fac[5][5], double r, double res[5][5]);
void cofactor(double num[5][5], double f, double res[5][5]);

//Receive test_data with UART. Remember to start python script first!
void receiveData(double arr[100][100]){
	int Status;

	    u8 temp[8];

	    XUartPs_Config *Config;

	    Config = XUartPs_LookupConfig(UART_DEVICE_ID);
	    Status = XUartPs_CfgInitialize(&Uart_Ps, Config, Config->BaseAddress);
	    XUartPs_SetBaudRate(&Uart_Ps, 115200);

	    print("Starting");

	    for(int row = 0; row < 100; row++){
	    	for(int col = 0; col < 100; col++){

	    			for(int i = 0; i < 8; i+= 1){
	    				temp[i] = XUartPs_RecvByte(XPAR_PS7_UART_1_BASEADDR);
	    			}

	    			arr[row][col] = atof(temp);


	    			sprintf(temp,"%f", arr[row][col]);
	    			print(temp);
	    	}
	    }
}

double mean(int length, double x[]) {
	double sum = 0;
	for (int i = 0; i < length; i++) {
		sum += x[i];
	}
	return((float)sum / length);
}

double square(double a){
	return a*a;
}

double power(double a, int b){
	double sum = 1;
	for(int i = 0; i<b; i++){
		sum *= a;
	}
}

double calculateSD(double data[])
{
	double sum = 0.0, mean, standardDeviation = 0.0;

	int i;

	for (i = 0; i<100; ++i)
	{
		sum += data[i];
	}

	mean = sum / 100;

	for (i = 0; i<100; ++i)
		standardDeviation +=  square(data[i]-mean);

	return sqrt(standardDeviation / 99);
}

void bsxfun(double *res ,double *a1, double *a2, int length, double std) {
	for (int i = 0; i < length; i++) {
		res[i] = (a1[i] - a2[i]) / std;
	}
}

void matrixMult(double arrA[][], double arrB[][], int rowA, int colB, int elem,  double res[5][5]) {
	double sum = 0;
	for (int row = 0; row < rowA; row++) {
		for (int col = 0; col < colB; col++) {

			for (int y = 0; y < elem; y++) {
				sum += arrA[row][y] * arrB[y][col];
			}
			res[row][col] = sum;
			sum = 0;
		}
	}
}

void matMult(double **arrA, double **arrB, int rowA, int colB, int elem, double **res) {
	double sum = 0;
	for (int row = 0; row < rowA; row++) {
		for (int col = 0; col < colB; col++) {


			for (int y = 0; y < elem; y++) {
				sum += arrA[row][y] * arrB[y][col];
			}
			res[row][col] = sum;
			sum = 0;
		}
	}
}

void EMSC(double raw[100][100], double ref_spectra[2][100], double corrected[100][100], int nVars, int nObs){
	double res[100] = { 0 };
	double M[5][100];
	double M_trans[100][5];
	double temp[2] = { 0 };
	double mean_t, num = 0;
	double mult_M[5][5];

	double std = calculateSD(ref_spectra[1]);

	bsxfun(res, ref_spectra[1], ref_spectra[0], 100, std);

	//Construct the M matrix
	for (int i = 0; i < 100; i++) {

		//Add 1 in first row
		M[0][i] = 1;
		M_trans[i][0] = 1;

		//Add mean in second row
		temp[0] = ref_spectra[0][i];
		temp[1] = ref_spectra[1][i];
		mean_t = mean(2, temp);
		M[1][i] = mean_t;
		M_trans[i][1] = mean_t;


		M[2][i] = res[i];
		M_trans[i][2] = res[i];

		//Add linspace and linspace squared
		M[3][i] = num;
		M_trans[i][3] = num;

		M[4][i] = square(num);
		M_trans[i][4] = square(num);
		num += (1.0 / 99.0);
	}
	matrixMult(M, M_trans,5,5,100 mult_M);

    double inverse[5][5];
    cofactor(mult_M, 5, inverse);

	char temp_t1[8];
	double test1 = inverse[0][0];
	   sprintf(temp_t1,"%f", test1);
	    print(temp_t1);
}


//---------------------------------------------------------------------
// http://www.sanfoundry.com/c-program-find-inverse-matrix/
//---------------------------------------------------------------------
double determinant(double a[5][5], int k)
{

	double s = 1, det = 0, b[5][5];
	int i, j, m, n, c;
	if (k == 1)
	{
		return (a[0][0]);
	}
	else
	{
		det = 0;
		for (c = 0; c < k; c++)
		{
			m = 0;
			n = 0;
			for (i = 0; i < k; i++)
			{
				for (j = 0; j < k; j++)
				{
					b[i][j] = 0;
					if (i != 0 && j != c)
					{
						b[m][n] = a[i][j];
						if (n < (k - 2))
							n++;
						else
						{
							n = 0;
							m++;
						}
					}
				}
			}
			det = det + s * (a[0][c] * determinant(b, k - 1));
			s = -1 * s;
		}
	}

	return (det);
}

/*Finding transpose of matrix*/
void transpose(double num[5][5], double fac[5][5], double r, double res[5][5])
{
	int i, j;
	double b[5][5], inverse[5][5], d;

	for (i = 0; i < r; i++)
	{
		for (j = 0; j < r; j++)
		{
			b[i][j] = fac[j][i];
		}
	}
	d = determinant(num, r);
	for (i = 0; i < r; i++)
	{
		for (j = 0; j < r; j++)
		{
			inverse[i][j] = b[i][j] / d;
		}
	}

	for (i = 0; i < r; i++)
	{
		for (j = 0; j < r; j++)
		{
			res[i][j] = inverse[i][j];
		}
	}
}

void cofactor(double num[5][5], double f, double res[5][5])
{
	double b[5][5], fac[5][5];
	int p, q, m, n, i, j;
	for (q = 0; q < f; q++)
	{
		for (p = 0; p < f; p++)
		{
			m = 0;
			n = 0;
			for (i = 0; i < f; i++)
			{
				for (j = 0; j < f; j++)
				{
					if (i != q && j != p)
					{
						b[m][n] = num[i][j];
						if (n < (f - 2))
							n++;
						else
						{
							n = 0;
							m++;
						}
					}
				}
			}
			fac[q][p] = pow(-1,q+p) * determinant(b, f - 1);
		}
	}
	transpose(num, fac, f, res);
}

//----------------------------------------------------------------------

int main()
{
    init_platform();


    double raw[100][100];
    double ref_spectra[2][100];
    double corrected[100][100];


    receiveData(raw);

    for(int i = 0; i<100; i++){
    	ref_spectra[0][i] = raw[2][i];
    	ref_spectra[1][i] = raw[92][i];
    }








    EMSC(raw, ref_spectra, corrected, 100, 100);


    cleanup_platform();
    return 0;
}
