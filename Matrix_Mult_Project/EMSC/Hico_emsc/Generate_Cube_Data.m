clear all
close all

load('pigments_pc.mat');


disp('This matlab script loads a full bil file into the matlab variable x_bil');
disp('and then makes an RGB image, and plots the spectrum of four pixels chosen');
disp('by the user. The first pixel should be from dark water -- its spectrum is');
disp('subtracted from the other three.');
disp(' '); disp(' ');

[FileName,PathName] = uigetfile('*.bil','Select the HICO *.bil file');

% Get HDR file
% [FileName,PathName] = uigetfile('*.hdr','Select the HICO *.hdr file');
hdr = readHyperHeader([PathName FileName]);
ISF = 50;  % HICO image_scale_factor FROM HDR FILE

% Get HICO file
% [FileName,PathName] = uigetfile('*.bil','Select the HICO *.bil file');

disp('          Loading BIL file ... please wait about 30 seconds to complete');
x_bil = multibandread([PathName FileName], [hdr.lines hdr.samples hdr.bands],...
    hdr.data_type, hdr.header_offset, hdr.interleave, hdr.byte_order, ...
    {'Row', 'Range', [651 1150]})./ISF;


dims_3d = size(x_bil);

% Create RGB Image
R = 42; G = 27; B =11;
%x_rgb = x_bil(321:420,141:240,[R G B]);
x_rgb = x_bil(:,:,[R G B]);

figure
imagesc((normalize(x_rgb)).^(1/2)); axis off; axis image;

%Query User for Four Input Pixels for Processing
disp('          Click on four pixels:');
disp('          The first pixel should be from dark water.');
disp('          Pixel colors: 1-Black, 2-Blue, 3-Green, 4-Magenta');

[p1y, p1x] = ginput(1); [p2y, p2x] = ginput(1); [p3y, p3x] = ginput(1); [p4y, p4x] = ginput(1);

p1y = round(p1y); p1x = round(p1x); p2y = round(p2y); p2x = round(p2x);
p3y = round(p3y); p3x = round(p3x); p4y = round(p4y); p4x = round(p4x);

hold on;
plot(p1y, p1x, 'o' ,'MarkerSize', 8, 'MarkerEdgeColor','y', 'MarkerFaceColor', 'k' );
plot(p2y, p2x, 'o' ,'MarkerSize', 8, 'MarkerEdgeColor','y', 'MarkerFaceColor', 'b' );
plot(p3y, p3x, 'o' ,'MarkerSize', 8, 'MarkerEdgeColor','y', 'MarkerFaceColor', 'g' );
plot(p4y, p4x, 'o' ,'MarkerSize', 8, 'MarkerEdgeColor','y', 'MarkerFaceColor', 'm' );

% HICO Wavelenght Data (nanometers) --- Column Vector (87 x 1)
f_hico = hdr.wavelength*1000;

% Create arrays for spectra of selected pixels, Column Vector (87 x 1)
p1  = squeeze(squeeze(x_bil(p1x, p1y, :)));
p2  = squeeze(squeeze(x_bil(p2x, p2y, :)));
p3  = squeeze(squeeze(x_bil(p3x, p3y, :)));
p4  = squeeze(squeeze(x_bil(p4x, p4y, :)));

% Plot Visible spectra
figure; hold on;
plot(f_hico, p1, '--k'); plot(f_hico, p2, '--b'); plot(f_hico, p3, '--g'); plot(f_hico, p4, '--m');
plot(f_hico, p1, '.k'); plot(f_hico, p2, '.b'); plot(f_hico, p3, '.g'); plot(f_hico, p4, '.m');
axis([f_hico(1) f_hico(end) 0 80]);
title('Specta at Sampled Pixels'); xlabel('Wavelength (nanometers)'); ylabel('Radiance (W/m^2/micrometer/sr)');

% Plot Visible spectra minus dark pixel
figure; hold on;
plot(f_hico, p1, '--k'); plot(f_hico, p2-p1, '--b'); plot(f_hico, p3-p1, '--g'); plot(f_hico, p4-p1, '--m');
plot(f_hico, p1, '.k'); plot(f_hico, p2-p1, '.b'); plot(f_hico, p3-p1, '.g'); plot(f_hico, p4-p1, '.m');
axis([f_hico(1) f_hico(end) 0 80]);
title('Specta at Sampled Pixels Minus Dark Pixel'); xlabel('Wavelength (nanometers)'); ylabel('Corrected Radiance (W/m^2/micrometer/sr)');

clear R G B RA GA BA;
clear ISF OS FRA;
clear p1x p1y p2x p2y p3x p3y p4x p4y;

%% Subset

x_small = x_bil(321:420,141:240,:);
data_flat = reshape(x_small(:,:,1:52), 100*100, 52);
%data_flat = reshape(x_bil(:,:,1:52), 500*500, 52);

data_flat = bsxfun(@rdivide, data_flat, p2(1:52)');


raw = data_flat;

for j = 1:size(coeff, 2)
    pigm_abs(j,:) = interp1(wavelength, coeff(:,j)', f_hico(1:52));
end

mean_abs = interp1(wavelength, mu, f_hico(1:52))';

pigm_refl = (10.^(-pigm_abs));
mean_refl = (10.^(-mean_abs));

raw = rescale(raw, 0, 4095);

wlens = f_hico(1:52);

%% Write cube to file

filename = 'cube_test.bin';
fileid = fopen(filename, 'wb');
w = 52;
h = 250000;
i = 0;
fwrite(fileid,raw','float');
% for x=1:h
%     for y=1:w
%             val = raw(x,y)*100000;
%             fwrite(fileid,val,'int32');
%             i = i + 1;
%             if( mod(i,130000) == 0)
%                 disp((i/(250000*52))*100);
%             end
%     end
% end
fclose(fileid);
clear fileid;
%% Read from Zynq
clear zynq_corrected;
%filename = 'zynq_final.bin';
filename = 'cube_test.bin';
w = 52;
h = 250000;TH
filename = 'zynq_final.bin';
w = 52;
h = 10000;
fileid = fopen(filename);
a = fread(fileid, 'float');
fclose(fileid);
%zynq_corrected = zeros(h, w); 
raw = reshape(a, [w h])';
%%
clear corr_image;
i = 1;
for x=1:500
    for y=1:500
        corr_image(y,x,:) = zynq_corrected(i,:);
        i = i +1;
    end
end

%% Create RGB Image
R = 42; G = 27; B =11;
% x_rgb = x_bil(321:420,141:240,[R G B]);
x_rgb = corr_image(:,:,[R G B]);

figure
imagesc((normalize(x_rgb)).^(1/2)); axis off; axis image;



%% Calculate Corrected
ref_spectra = raw([3 9939], :);
mean_spectra = mean(ref_spectra);
%corrected = emsc_simple(raw, ref_spectra, mean_spectra);

%a = 1;
%for i=1:25
%    corrected(a:i*10000,:) = emsc_simple(raw(a:i*10000,:),ref_spectra,mean_spectra);
%    a = a+10000;
%end

%K = bsxfun(@minus,ref_spectra(2:end,:), ref_spectra(1,:));
K = pigm_refl;
m = mean_refl;

nVars = size(raw, 2);
nObs = size(raw, 1);

wlens = linspace(0,1,nVars);
wlensSQ = wlens.^2;

M = [ones(1, nVars);
    wlens;
    wlensSQ
    K;
    m];

G =pinv(M);
corrected = raw;
for idx = 1:nObs
    p = raw(idx,:)*G;
    P2(idx,:) = p;   
    corrected(idx,:) = (raw(idx,:) - p(1) - p(2)*wlens - p(3)*wlensSQ ) / p(end);
end

%% Calculate Corrected with different precisions
clear corrected_rounded;
%G = floor(G*multiplier);
%12 bit : 4094
%16 bit : 65 535

mult = 10000;

r_G = floor(G*mult);
r_raw = floor(raw*mult);
r_wlens = floor(wlens*mult);
r_wlensSQ = floor(wlens.^2*mult);

corrected_rounded = r_raw;
for idx = 1:nObs
    p1 = raw(idx,:)*r_G; %x 1000
    P1(idx,:) = p1;   
    corrected_rounded(idx,:) = (r_raw(idx,:) - p1(1) - ((p1(2)*r_wlens + p1(3)*r_wlensSQ)/mult)) / p1(end);
    %m                               m         m       m            m  
end
%corrected_rounded = corrected_rounded/multiplier;
%%
clear corrected;
ref_spectra = raw([3 9939],:);
mean_spectra = mean(ref_spectra);
mOrder = 2;
corrected = emsc(raw, ref_spectra, mean_spectra, mOrder);


%%
clear p;
for idx = 1:nObs
    p(idx,:) = raw(idx,:)*G;
end

%%

i = 1;
for x=1:500
    for y=1:500
        corr_image(y,x,:) = corrected(i,:);
        i = i +1;
    end
end

%% Create RGB Image
R = 42; G = 27; B =11;
% x_rgb = x_bil(321:420,141:240,[R G B]);
x_rgb = corr_image(:,:,[R G B]);

figure
imagesc((normalize(x_rgb)).^(1/2)); axis off; axis image;

%%

figure;
plot(corrected');
title('MatLab');
saveas(gcf,'corrected_Mat.png');
clf;
%%
plot(zynq_corrected')
title('Zynq');
saveas(gcf,'corrected_zynq.png');
clf;

plot(raw')
title('raw');
saveas(gcf,'raw.png');
clf;

plot(ref_spectra')
title('ref_spectra');
saveas(gcf,'ref_spectra.png');
clf;

