function [corrected_rounded, P] = emsc_rounded(raw ,ref_spectra, mean_spectra, mOrder, mult)

K = ref_spectra;
m = mean_spectra;

nVars = size(raw, 2);
nObs = size(raw, 1);

wlens = linspace(0,1,nVars);
wlensSQ = wlens.^2;

M = [ones(1, nVars);
    wlens;
    wlensSQ
    K;
    m];

G =pinv(M);

r_G = floor(G*mult);
r_raw = floor(raw*mult);
r_wlens = floor(wlens*mult);
r_wlensSQ = r_wlens.^2;


corrected_rounded = r_raw;
for idx = 1:nObs
    p = raw(idx,:)*r_G; %x 1000
    P(idx,:) = p/mult;   
    corrected_rounded(idx,:) = (r_raw(idx,:) - p(1) - ((p(2)*wlens + p(3)*wlensSQ))) / p(end);
end

end

