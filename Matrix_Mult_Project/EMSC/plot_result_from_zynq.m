
filename = '../re_corrected.txt';
fileID = fopen(filename);



for x=1:100
    for y = 1:100
        line = fgets(fileID);
        re_corrected(x,y) = str2double(line);
    end;    
end;

fclose(fileID);
figure;
plot(re_corrected');
title('Plot from Zynq Results');
movegui('east');
axis tight