close all
clear all

load test_data.mat
test_data_test = randi(4095,100,100);
figure
plot(test_data');
title('Test data(input)');
movegui('west');
axis tight
raw = test_data;
ref_spectra = raw([3 85 93],:);

K = bsxfun(@minus,ref_spectra(2:end,:), ref_spectra(1,:));
m = mean(ref_spectra);

nVars = size(raw, 2);
nObs = size(raw, 1);

wlens = linspace(0,1,nVars);
wlensSQ = wlens.^2;

M = [ones(1, nVars);
    m;
    K;
    wlens;
    wlensSQ];

corrected = raw;
for idx = 1:nObs
    p = raw(idx,:)*M'*pinv(M*M');
    corrected(idx,:) = (raw(idx,:) - p(1) - p(4)*wlens - p(5)*wlensSQ ) / p(2);
end


figure
plot(corrected');
title('Plot from MATLAB calculations');
movegui('center');
axis tight
plot_result_from_zynq;

