library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity multiplier is
    Port ( 
        A : in std_logic_vector(17 downto 0);
        B : in std_logic_vector(24 downto 0);
        P : out std_logic_vector(42 downto 0)
    );
end multiplier;

architecture Behavioral of multiplier is
begin

P <= std_logic_vector(signed(A)*signed(B));
    

end behavioral;