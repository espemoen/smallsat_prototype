library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity EMSC_top is
  generic (
    C_S_AXI_DATA_WIDTH : integer := 32;
    C_S_AXI_ADDR_WIDTH : integer := 6;
    B_RAM_SIZE : integer := 100;
    B_RAM_BIT_WIDTH : integer := 32;   
    RAW_BIT_WIDTH : positive := 16;
    NUM_B_RAM     : positive := 8;
    P_BIT_WIDTH   : positive := 48   
    );
  port (
    clk     : in std_logic;
    aresetn : in std_logic;
    

    -- Stream in
    s_axis_tdata  : in  std_logic_vector(31 downto 0);
    s_axis_tvalid : in  std_logic;
    s_axis_tready : out std_logic;
    s_axis_tlast  : in  std_logic;

    -- Stream out
    m_axis_tdata  : out std_logic_vector(31 downto 0);
    m_axis_tvalid : out std_logic;
    m_axis_tready : in  std_logic;
    m_axis_tlast  : out std_logic;

    -- Control interface
    s_axi_awaddr  : in  std_logic_vector(c_s_axi_addr_width-1 downto 0);
    s_axi_awprot  : in  std_logic_vector(2 downto 0);
    s_axi_awvalid : in  std_logic;
    s_axi_awready : out std_logic;
    s_axi_wdata   : in  std_logic_vector(c_s_axi_data_width-1 downto 0);
    s_axi_wstrb   : in  std_logic_vector((c_s_axi_data_width/8)-1 downto 0);
    s_axi_wvalid  : in  std_logic;
    s_axi_wready  : out std_logic;
    s_axi_bresp   : out std_logic_vector(1 downto 0);
    s_axi_bvalid  : out std_logic;
    s_axi_bready  : in  std_logic;
    s_axi_araddr  : in  std_logic_vector(c_s_axi_addr_width-1 downto 0);
    s_axi_arprot  : in  std_logic_vector(2 downto 0);
    s_axi_arvalid : in  std_logic;
    s_axi_arready : out std_logic;
    s_axi_rdata   : out std_logic_vector(c_s_axi_data_width-1 downto 0);
    s_axi_rresp   : out std_logic_vector(1 downto 0);
    s_axi_rvalid  : out std_logic;
    s_axi_rready  : in  std_logic
    );
end EMSC_top;

architecture rtl of EMSC_top is
  signal n     : std_logic_vector(15 downto 0);
  signal count : std_logic_vector(31 downto 0);

  signal init, valid_input, enable, p_valid : std_logic;
  signal G_size     : std_logic_vector(11 downto 0);
  signal in_G       : std_logic_vector(B_RAM_BIT_WIDTH-1 downto 0);
  signal in_raw     : std_logic_vector(RAW_BIT_WIDTH-1 downto 0);
  signal in_raw_reg : std_logic_vector(RAW_BIT_WIDTH-1 downto 0);
  signal p_out      : std_logic_vector(NUM_B_RAM*P_BIT_WIDTH-1 downto 0);
  signal corrected  : std_logic_vector(P_BIT_WIDTH-1 downto 0);

  signal read_out_reg : std_logic_vector(NUM_B_RAM*P_BIT_WIDTH-1 downto 0);

  signal in_data  : std_logic_vector(15 downto 0);
  signal in_valid : std_logic;
  signal in_ready : std_logic;
  signal in_last  : std_logic;

  signal out_data  : std_logic_vector(31 downto 0);
  signal out_valid : std_logic;
  signal out_ready : std_logic;
  signal out_last  : std_logic;
    
  
  signal G_valid   : std_logic;
  signal initialized : std_logic;
  -- Helper signals
  signal in_handshake  : std_logic;
  signal out_handshake : std_logic;
begin

  in_data       <= std_logic_vector(s_axis_tdata(15 downto 0));
  in_valid      <= s_axis_tvalid;
  in_last       <= s_axis_tlast;
  s_axis_tready <= in_ready;

  m_axis_tdata  <= std_logic_vector(out_data);
 -- m_axis_tvalid <= out_valid;
  m_axis_tlast  <= out_last;
  out_ready     <= m_axis_tready;

  i_register_interface : entity work.register_interface
    generic map (
      P_BIT_WIDTH        => P_BIT_WIDTH,
      NUM_B_RAM          => NUM_B_RAM,
      C_S_AXI_DATA_WIDTH => C_S_AXI_DATA_WIDTH,
      C_S_AXI_ADDR_WIDTH => C_S_AXI_ADDR_WIDTH)
    port map (
      n             => n,
      G_size        => G_size,
      init          => init,
      enable        => enable,
      p_rdy         => p_valid,
      p_out         => read_out_reg,
      in_G          => in_G,
      G_valid       => G_valid,
      initialized   => initialized,
      count         => count,
      S_AXI_ACLK    => CLK,
      S_AXI_ARESETN => ARESETN,
      S_AXI_AWADDR  => S_AXI_AWADDR,
      S_AXI_AWPROT  => S_AXI_AWPROT,
      S_AXI_AWVALID => S_AXI_AWVALID,
      S_AXI_AWREADY => S_AXI_AWREADY,
      S_AXI_WDATA   => S_AXI_WDATA,
      S_AXI_WSTRB   => S_AXI_WSTRB,
      S_AXI_WVALID  => S_AXI_WVALID,
      S_AXI_WREADY  => S_AXI_WREADY,
      S_AXI_BRESP   => S_AXI_BRESP,
      S_AXI_BVALID  => S_AXI_BVALID,
      S_AXI_BREADY  => S_AXI_BREADY,
      S_AXI_ARADDR  => S_AXI_ARADDR,
      S_AXI_ARPROT  => S_AXI_ARPROT,
      S_AXI_ARVALID => S_AXI_ARVALID,
      S_AXI_ARREADY => S_AXI_ARREADY,
      S_AXI_RDATA   => S_AXI_RDATA,
      S_AXI_RRESP   => S_AXI_RRESP,
      S_AXI_RVALID  => S_AXI_RVALID,
      S_AXI_RREADY  => S_AXI_RREADY);



EMSC_rtl : entity work.EMSC_rtl
   generic map(
        B_RAM_SIZE      =>  B_RAM_SIZE,
        B_RAM_BIT_WIDTH =>  B_RAM_BIT_WIDTH,
        RAW_BIT_WIDTH   =>  RAW_BIT_WIDTH,
        NUM_B_RAM       =>  NUM_B_RAM
    )
    port map(
        clk             =>      clk,
        aresetn         =>      aresetn,
        init            =>      init,
        valid_input     =>      valid_input,
        enable          =>      enable,
        G_size          =>      G_size,
        in_G            =>      in_G,
        in_raw          =>      in_raw_reg,
        p_rdy           =>      p_valid,
        initialized     =>      initialized,
        p_out           =>      p_out,
        corrected       =>      corrected
    );

--G_valid;
--raw_valid;

valid_input <= '1' when in_handshake='1' or G_valid='1' else '0';
--in_G <= in_data;
in_raw <= in_data;
m_axis_tvalid <= p_valid;

  process (clk)
    variable count_i : integer range 0 to 2**16-1;
  begin
    if (rising_edge(clk)) then
      if (aresetn = '0') then
        out_data  <= (others => '0');
        out_valid <= '0';
        out_last  <= '0';
        in_raw_reg <= (others => '0');
        count_i   := 0;
        read_out_reg <= (others => '0');
      else
        in_raw_reg <= in_raw;
        if (in_handshake = '1') then
          count_i   := count_i + 1;
        end if;
        if(p_valid = '1') then
            read_out_reg <= p_out;
        else
            read_out_reg <= (others => '0');
        end if;
        count <= std_logic_vector(to_unsigned(count_i, 32));
      end if;
    end if;
  end process;



  in_handshake  <= in_valid and in_ready; -- should include reset
  out_handshake <= out_valid and out_ready; -- should include reset
  in_ready      <= '1' when out_valid = '0' or out_handshake = '1' else '0';

end rtl;




