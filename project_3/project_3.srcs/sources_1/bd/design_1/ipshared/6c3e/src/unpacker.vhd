library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use ieee.math_real.all;

entity unpacker is
  generic (
    C_IN_DATA_WIDTH  : integer := 64;
    C_OUT_DATA_WIDTH : integer := 64;
    C_COMP_WIDTH     : integer := 8;
    C_NUM_COMP       : integer := 8
    );
  port (
    clk     : in std_logic;
    aresetn : in std_logic;

    config_data  : in  std_logic_vector(8 downto 0);
    config_wr    : in  std_logic;
    config_ready : out std_logic;

    in_tdata  : in  std_logic_vector(C_IN_DATA_WIDTH-1 downto 0);
    in_tvalid : in  std_logic;
    in_tready : out std_logic;
    in_tlast  : in  std_logic;

    out_tdata  : out std_logic_vector(C_OUT_DATA_WIDTH-1 downto 0);
    out_tvalid : out std_logic;
    out_tready : in  std_logic;
    out_tlast  : out std_logic
    );
end unpacker;

architecture Behavioral of unpacker is
  constant C_COMP_PER_WORD      : integer := C_IN_DATA_WIDTH / C_COMP_WIDTH;
  constant C_TO_BUFFER_NUM_COMP : integer := integer(ceil(real(C_IN_DATA_WIDTH)/real(C_COMP_WIDTH)));
  constant C_TO_BUFFER_WIDTH    : integer := C_TO_BUFFER_NUM_COMP * C_COMP_WIDTH;

  signal in_delay_tdata     : std_logic_vector(C_IN_DATA_WIDTH-1 downto 0);
  signal in_delay_tvalid    : std_logic;
  signal in_delay_tready    : std_logic;
  signal in_delay_tlast     : std_logic;
  signal in_delay_handshake : std_logic;

  signal to_buffer_tdata     : std_logic_vector(C_TO_BUFFER_WIDTH-1 downto 0);
  signal to_buffer_tvalid    : std_logic;
  signal to_buffer_tready    : std_logic;
  signal to_buffer_tlast     : std_logic;
  signal to_buffer_num_valid : integer range 0 to C_TO_BUFFER_NUM_COMP-1;

  signal in_ready     : std_logic;
  signal in_handshake : std_logic;

  signal from_fifo_last               : std_logic;
  signal from_fifo_offset             : std_logic_vector(1 downto 0);
  signal from_fifo_num_last           : std_logic_vector(4 downto 0);
  signal from_fifo_truncate_last_word : std_logic;

  signal transfer_complete : std_logic;
begin
  in_handshake <= in_ready and in_tvalid;
  in_tready    <= in_ready;

  -- Configuration FIFO
  b_config_fifo : block is
    component unpacker_fifo
      port (
        clk   : in  std_logic;
        rst   : in  std_logic;
        din   : in  std_logic_vector(8 downto 0);
        wr_en : in  std_logic;
        rd_en : in  std_logic;
        dout  : out std_logic_vector(8 downto 0);
        full  : out std_logic;
        empty : out std_logic
        );
    end component;

    signal reset     : std_logic;
    signal fifo_rd   : std_logic;
    signal fifo_out  : std_logic_vector(8 downto 0);
    signal fifo_full : std_logic;
  begin
    reset <= not aresetn;

    from_fifo_last               <= fifo_out(0);
    from_fifo_offset             <= fifo_out(2 downto 1);
    from_fifo_num_last           <= fifo_out(7 downto 3);
    from_fifo_truncate_last_word <= fifo_out(8);

    config_ready <= not fifo_full;

    i_fifo : unpacker_fifo
      port map (
        clk   => clk,
        rst   => reset,
        din   => config_data,
        wr_en => config_wr,
        rd_en => fifo_rd,
        dout  => fifo_out,
        full  => fifo_full,
        empty => open
        );

    process (clk)
    begin
      if (rising_edge(clk)) then
        if (aresetn = '0') then
          transfer_complete <= '1';
        else
          if (in_handshake = '1' and in_tlast = '1') then
            transfer_complete <= '1';
          elsif (in_handshake = '1') then
            transfer_complete <= '0';
          end if;
        end if;
      end if;
    end process;

    fifo_rd <= in_handshake and transfer_complete;
  end block b_config_fifo;

  -- Delay the stream to align with FIFO
  process (clk)
  begin
    if (rising_edge(clk)) then
      if (aresetn = '0') then
        in_delay_tdata  <= (others => '0');
        in_delay_tvalid <= '0';
        in_delay_tlast  <= '0';
      else
        if (in_handshake = '1') then
          in_delay_tdata  <= in_tdata;
          in_delay_tvalid <= in_tvalid;
          in_delay_tlast  <= in_tlast;
        elsif (in_delay_handshake = '1') then
          in_delay_tvalid <= '0';
        end if;
      end if;
    end if;
  end process;

  in_delay_handshake <= in_delay_tvalid and in_delay_tready;

  process (in_delay_tvalid, in_delay_handshake)
  begin
    in_ready <= '0';
    if (in_delay_tvalid = '0' or in_delay_handshake = '1') then
      in_ready <= '1';
    end if;
  end process;

  g_aligner : if (C_COMP_WIDTH mod 8 /= 0) generate
    signal from_aligner_tlast    : std_logic;
    signal to_restructure_tlast  : std_logic;
    signal to_restructure_tvalid : std_logic;
    signal to_restructure_tready : std_logic;
    signal to_restructure_tdata  : std_logic_vector(C_IN_DATA_WIDTH-1 downto 0);
    signal last                  : std_logic;
    signal num_last              : std_logic_vector(4 downto 0);
  begin

    i_shifter : entity work.offset_shifter
      generic map (
        C_DATA_WIDTH   => C_IN_DATA_WIDTH,
        C_BLOCK_WIDTH  => 2,
        C_OFFSET_WIDTH => 2
        )
      port map (
        clk     => clk,
        aresetn => aresetn,

        offset        => from_fifo_offset,
        truncate_last => from_fifo_truncate_last_word,

        in_tdata  => in_delay_tdata,
        in_tvalid => in_delay_tvalid,
        in_tready => in_delay_tready,
        in_tlast  => in_delay_tlast,

        out_tdata  => to_restructure_tdata,
        out_tvalid => to_restructure_tvalid,
        out_tready => to_restructure_tready,
        out_tlast  => to_restructure_tlast
        );

    process (clk)
    begin
      if (rising_edge(clk)) then
        if (aresetn = '0') then
          last     <= '0';
          num_last <= (others => '0');
        elsif (in_delay_handshake = '1') then
          last     <= from_fifo_last;
          num_last <= from_fifo_num_last;
        end if;
      end if;
    end process;

    i_comp_aligner : entity work.component_aligner
      generic map (
        C_IN_DATA_WIDTH  => C_IN_DATA_WIDTH,
        C_OUT_DATA_WIDTH => C_TO_BUFFER_WIDTH,
        C_COMP_WIDTH     => C_COMP_WIDTH
        )
      port map (
        clk     => clk,
        aresetn => aresetn,

        num_last => num_last,

        in_tdata  => to_restructure_tdata,
        in_tvalid => to_restructure_tvalid,
        in_tready => to_restructure_tready,
        in_tlast  => to_restructure_tlast,

        out_tdata     => to_buffer_tdata,
        out_tvalid    => to_buffer_tvalid,
        out_tready    => to_buffer_tready,
        out_tlast     => from_aligner_tlast,
        out_num_valid => to_buffer_num_valid
        );

    to_buffer_tlast <= from_aligner_tlast and last;
  end generate g_aligner;

  g_noaligner : if (C_COMP_WIDTH mod 8 = 0) generate
    signal num_valid_last : integer range 0 to C_COMP_PER_WORD-1;
  begin
    num_valid_last      <= to_integer(unsigned(from_fifo_num_last(2 downto 0)));
    to_buffer_tdata     <= in_delay_tdata;
    to_buffer_tvalid    <= in_delay_tvalid;
    in_delay_tready     <= to_buffer_tready;
    to_buffer_tlast     <= in_delay_tlast and from_fifo_last;
    to_buffer_num_valid <= num_valid_last - 1 when in_delay_tlast = '1' and num_valid_last /= 0
                           else C_COMP_PER_WORD-1;
  end generate g_noaligner;

  -- Only instantiate the component buffer when we either have a component size
  -- not multiple of a byte, or when the number of components selected doesn't
  -- fit in one beat
  i_buffer : entity work.component_buffer
    generic map (
      C_IN_DATA_WIDTH  => C_TO_BUFFER_WIDTH,
      C_OUT_DATA_WIDTH => C_OUT_DATA_WIDTH,
      C_COMP_WIDTH     => C_COMP_WIDTH,
      C_MAX_NUM_OUTPUT => C_NUM_COMP,
      C_FIXED_OUTPUT   => true)
    port map (
      clk     => clk,
      aresetn => aresetn,

      in_tdata     => to_buffer_tdata,
      in_tvalid    => to_buffer_tvalid,
      in_tready    => to_buffer_tready,
      in_tlast     => to_buffer_tlast,
      in_num_valid => to_buffer_num_valid,

      out_num    => C_NUM_COMP-1,
      out_tdata  => out_tdata,
      out_tvalid => out_tvalid,
      out_tready => out_tready,
      out_tlast  => out_tlast);
end Behavioral;
