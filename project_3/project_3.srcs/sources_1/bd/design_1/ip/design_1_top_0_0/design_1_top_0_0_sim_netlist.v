// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Thu Apr 19 09:29:33 2018
// Host        : DESKTOP-0KA73P0 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               d:/MasterOppgave/smallsat_prototype/project_3/project_3.srcs/sources_1/bd/design_1/ip/design_1_top_0_0/design_1_top_0_0_sim_netlist.v
// Design      : design_1_top_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_top_0_0,top,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "top,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module design_1_top_0_0
   (clk,
    aresetn,
    s_axis_tdata,
    s_axis_tvalid,
    s_axis_tready,
    s_axis_tlast,
    m_axis_tdata,
    m_axis_tvalid,
    m_axis_tready,
    m_axis_tlast,
    s_axi_ctrl_status_awaddr,
    s_axi_ctrl_status_awprot,
    s_axi_ctrl_status_awvalid,
    s_axi_ctrl_status_awready,
    s_axi_ctrl_status_wdata,
    s_axi_ctrl_status_wstrb,
    s_axi_ctrl_status_wvalid,
    s_axi_ctrl_status_wready,
    s_axi_ctrl_status_bresp,
    s_axi_ctrl_status_bvalid,
    s_axi_ctrl_status_bready,
    s_axi_ctrl_status_araddr,
    s_axi_ctrl_status_arprot,
    s_axi_ctrl_status_arvalid,
    s_axi_ctrl_status_arready,
    s_axi_ctrl_status_rdata,
    s_axi_ctrl_status_rresp,
    s_axi_ctrl_status_rvalid,
    s_axi_ctrl_status_rready);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk, ASSOCIATED_BUSIF m_axis:s_axis:s_axi_ctrl_status, ASSOCIATED_RESET aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0" *) input clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 aresetn RST" *) (* x_interface_parameter = "XIL_INTERFACENAME aresetn, POLARITY ACTIVE_LOW" *) input aresetn;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 s_axis TDATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef" *) input [31:0]s_axis_tdata;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 s_axis TVALID" *) input s_axis_tvalid;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 s_axis TREADY" *) output s_axis_tready;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 s_axis TLAST" *) input s_axis_tlast;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 m_axis TDATA" *) (* x_interface_parameter = "XIL_INTERFACENAME m_axis, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef" *) output [63:0]m_axis_tdata;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 m_axis TVALID" *) output m_axis_tvalid;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 m_axis TREADY" *) input m_axis_tready;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 m_axis TLAST" *) output m_axis_tlast;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axi_ctrl_status, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [5:0]s_axi_ctrl_status_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status AWPROT" *) input [2:0]s_axi_ctrl_status_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status AWVALID" *) input s_axi_ctrl_status_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status AWREADY" *) output s_axi_ctrl_status_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status WDATA" *) input [31:0]s_axi_ctrl_status_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status WSTRB" *) input [3:0]s_axi_ctrl_status_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status WVALID" *) input s_axi_ctrl_status_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status WREADY" *) output s_axi_ctrl_status_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status BRESP" *) output [1:0]s_axi_ctrl_status_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status BVALID" *) output s_axi_ctrl_status_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status BREADY" *) input s_axi_ctrl_status_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status ARADDR" *) input [5:0]s_axi_ctrl_status_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status ARPROT" *) input [2:0]s_axi_ctrl_status_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status ARVALID" *) input s_axi_ctrl_status_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status ARREADY" *) output s_axi_ctrl_status_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status RDATA" *) output [31:0]s_axi_ctrl_status_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status RRESP" *) output [1:0]s_axi_ctrl_status_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status RVALID" *) output s_axi_ctrl_status_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status RREADY" *) input s_axi_ctrl_status_rready;

  wire \<const0> ;
  wire aresetn;
  wire [11:8]\b_ram/state1 ;
  wire clk;
  wire [47:0]\^m_axis_tdata ;
  wire m_axis_tready;
  wire m_axis_tvalid;
  wire \read_address[6]_i_10_n_0 ;
  wire \read_address[6]_i_9_n_0 ;
  wire [5:0]s_axi_ctrl_status_araddr;
  wire s_axi_ctrl_status_arready;
  wire s_axi_ctrl_status_arvalid;
  wire [5:0]s_axi_ctrl_status_awaddr;
  wire s_axi_ctrl_status_awready;
  wire s_axi_ctrl_status_awvalid;
  wire s_axi_ctrl_status_bready;
  wire s_axi_ctrl_status_bvalid;
  wire [31:0]s_axi_ctrl_status_rdata;
  wire s_axi_ctrl_status_rready;
  wire s_axi_ctrl_status_rvalid;
  wire [31:0]s_axi_ctrl_status_wdata;
  wire s_axi_ctrl_status_wready;
  wire [3:0]s_axi_ctrl_status_wstrb;
  wire s_axi_ctrl_status_wvalid;
  wire [31:0]s_axis_tdata;
  wire s_axis_tlast;
  wire s_axis_tready;
  wire s_axis_tvalid;

  assign m_axis_tdata[63] = \<const0> ;
  assign m_axis_tdata[62] = \<const0> ;
  assign m_axis_tdata[61] = \<const0> ;
  assign m_axis_tdata[60] = \<const0> ;
  assign m_axis_tdata[59] = \<const0> ;
  assign m_axis_tdata[58] = \<const0> ;
  assign m_axis_tdata[57] = \<const0> ;
  assign m_axis_tdata[56] = \<const0> ;
  assign m_axis_tdata[55] = \<const0> ;
  assign m_axis_tdata[54] = \<const0> ;
  assign m_axis_tdata[53] = \<const0> ;
  assign m_axis_tdata[52] = \<const0> ;
  assign m_axis_tdata[51] = \<const0> ;
  assign m_axis_tdata[50] = \<const0> ;
  assign m_axis_tdata[49] = \<const0> ;
  assign m_axis_tdata[48] = \<const0> ;
  assign m_axis_tdata[47:0] = \^m_axis_tdata [47:0];
  assign s_axi_ctrl_status_bresp[1] = \<const0> ;
  assign s_axi_ctrl_status_bresp[0] = \<const0> ;
  assign s_axi_ctrl_status_rresp[1] = \<const0> ;
  assign s_axi_ctrl_status_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  design_1_top_0_0_top U0
       (.S({\read_address[6]_i_9_n_0 ,\read_address[6]_i_10_n_0 }),
        .aresetn(aresetn),
        .clk(clk),
        .m_axis_tdata(\^m_axis_tdata ),
        .m_axis_tready(m_axis_tready),
        .m_axis_tvalid(m_axis_tvalid),
        .\read_address_reg[2] (\b_ram/state1 ),
        .s_axi_ctrl_status_araddr(s_axi_ctrl_status_araddr[5:2]),
        .s_axi_ctrl_status_arready(s_axi_ctrl_status_arready),
        .s_axi_ctrl_status_arvalid(s_axi_ctrl_status_arvalid),
        .s_axi_ctrl_status_awaddr(s_axi_ctrl_status_awaddr[5:2]),
        .s_axi_ctrl_status_awready(s_axi_ctrl_status_awready),
        .s_axi_ctrl_status_awvalid(s_axi_ctrl_status_awvalid),
        .s_axi_ctrl_status_bready(s_axi_ctrl_status_bready),
        .s_axi_ctrl_status_bvalid(s_axi_ctrl_status_bvalid),
        .s_axi_ctrl_status_rdata(s_axi_ctrl_status_rdata),
        .s_axi_ctrl_status_rready(s_axi_ctrl_status_rready),
        .s_axi_ctrl_status_rvalid(s_axi_ctrl_status_rvalid),
        .s_axi_ctrl_status_wdata(s_axi_ctrl_status_wdata),
        .s_axi_ctrl_status_wready(s_axi_ctrl_status_wready),
        .s_axi_ctrl_status_wstrb(s_axi_ctrl_status_wstrb),
        .s_axi_ctrl_status_wvalid(s_axi_ctrl_status_wvalid),
        .s_axis_tdata(s_axis_tdata[15:0]),
        .s_axis_tlast(s_axis_tlast),
        .s_axis_tready(s_axis_tready),
        .s_axis_tvalid(s_axis_tvalid));
  LUT2 #(
    .INIT(4'h1)) 
    \read_address[6]_i_10 
       (.I0(\b_ram/state1 [8]),
        .I1(\b_ram/state1 [9]),
        .O(\read_address[6]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \read_address[6]_i_9 
       (.I0(\b_ram/state1 [10]),
        .I1(\b_ram/state1 [11]),
        .O(\read_address[6]_i_9_n_0 ));
endmodule

(* ORIG_REF_NAME = "axi_gearbox" *) 
module design_1_top_0_0_axi_gearbox
   (m_axis_tvalid,
    start,
    Q,
    m_axis_tdata,
    clk,
    p_0_in__0,
    start_reg_0,
    p_rdy,
    m_axis_tready,
    E,
    \slv_regs_reg[0][12] ,
    D);
  output m_axis_tvalid;
  output start;
  output [0:0]Q;
  output [47:0]m_axis_tdata;
  input clk;
  input p_0_in__0;
  input start_reg_0;
  input p_rdy;
  input m_axis_tready;
  input [0:0]E;
  input [0:0]\slv_regs_reg[0][12] ;
  input [383:0]D;

  wire [383:0]D;
  wire [0:0]E;
  wire [0:0]Q;
  wire clk;
  wire \counter[0]_i_1__0_n_0 ;
  wire \counter[1]_i_1__0_n_0 ;
  wire \counter[2]_i_1__0_n_0 ;
  wire \counter[3]_i_2_n_0 ;
  wire \counter_reg_n_0_[0] ;
  wire \counter_reg_n_0_[1] ;
  wire \counter_reg_n_0_[2] ;
  wire [47:0]data1;
  wire [47:0]data2;
  wire [47:0]data3;
  wire [47:0]data4;
  wire [47:0]data5;
  wire [47:0]data6;
  wire [47:0]data7;
  wire [47:0]m_axis_tdata;
  wire \m_axis_tdata[0]_i_2_n_0 ;
  wire \m_axis_tdata[0]_i_3_n_0 ;
  wire \m_axis_tdata[10]_i_2_n_0 ;
  wire \m_axis_tdata[10]_i_3_n_0 ;
  wire \m_axis_tdata[11]_i_2_n_0 ;
  wire \m_axis_tdata[11]_i_3_n_0 ;
  wire \m_axis_tdata[12]_i_2_n_0 ;
  wire \m_axis_tdata[12]_i_3_n_0 ;
  wire \m_axis_tdata[13]_i_2_n_0 ;
  wire \m_axis_tdata[13]_i_3_n_0 ;
  wire \m_axis_tdata[14]_i_2_n_0 ;
  wire \m_axis_tdata[14]_i_3_n_0 ;
  wire \m_axis_tdata[15]_i_2_n_0 ;
  wire \m_axis_tdata[15]_i_3_n_0 ;
  wire \m_axis_tdata[16]_i_2_n_0 ;
  wire \m_axis_tdata[16]_i_3_n_0 ;
  wire \m_axis_tdata[17]_i_2_n_0 ;
  wire \m_axis_tdata[17]_i_3_n_0 ;
  wire \m_axis_tdata[18]_i_2_n_0 ;
  wire \m_axis_tdata[18]_i_3_n_0 ;
  wire \m_axis_tdata[19]_i_2_n_0 ;
  wire \m_axis_tdata[19]_i_3_n_0 ;
  wire \m_axis_tdata[1]_i_2_n_0 ;
  wire \m_axis_tdata[1]_i_3_n_0 ;
  wire \m_axis_tdata[20]_i_2_n_0 ;
  wire \m_axis_tdata[20]_i_3_n_0 ;
  wire \m_axis_tdata[21]_i_2_n_0 ;
  wire \m_axis_tdata[21]_i_3_n_0 ;
  wire \m_axis_tdata[22]_i_2_n_0 ;
  wire \m_axis_tdata[22]_i_3_n_0 ;
  wire \m_axis_tdata[23]_i_2_n_0 ;
  wire \m_axis_tdata[23]_i_3_n_0 ;
  wire \m_axis_tdata[24]_i_2_n_0 ;
  wire \m_axis_tdata[24]_i_3_n_0 ;
  wire \m_axis_tdata[25]_i_2_n_0 ;
  wire \m_axis_tdata[25]_i_3_n_0 ;
  wire \m_axis_tdata[26]_i_2_n_0 ;
  wire \m_axis_tdata[26]_i_3_n_0 ;
  wire \m_axis_tdata[27]_i_2_n_0 ;
  wire \m_axis_tdata[27]_i_3_n_0 ;
  wire \m_axis_tdata[28]_i_2_n_0 ;
  wire \m_axis_tdata[28]_i_3_n_0 ;
  wire \m_axis_tdata[29]_i_2_n_0 ;
  wire \m_axis_tdata[29]_i_3_n_0 ;
  wire \m_axis_tdata[2]_i_2_n_0 ;
  wire \m_axis_tdata[2]_i_3_n_0 ;
  wire \m_axis_tdata[30]_i_2_n_0 ;
  wire \m_axis_tdata[30]_i_3_n_0 ;
  wire \m_axis_tdata[31]_i_2_n_0 ;
  wire \m_axis_tdata[31]_i_3_n_0 ;
  wire \m_axis_tdata[32]_i_2_n_0 ;
  wire \m_axis_tdata[32]_i_3_n_0 ;
  wire \m_axis_tdata[33]_i_2_n_0 ;
  wire \m_axis_tdata[33]_i_3_n_0 ;
  wire \m_axis_tdata[34]_i_2_n_0 ;
  wire \m_axis_tdata[34]_i_3_n_0 ;
  wire \m_axis_tdata[35]_i_2_n_0 ;
  wire \m_axis_tdata[35]_i_3_n_0 ;
  wire \m_axis_tdata[36]_i_2_n_0 ;
  wire \m_axis_tdata[36]_i_3_n_0 ;
  wire \m_axis_tdata[37]_i_2_n_0 ;
  wire \m_axis_tdata[37]_i_3_n_0 ;
  wire \m_axis_tdata[38]_i_2_n_0 ;
  wire \m_axis_tdata[38]_i_3_n_0 ;
  wire \m_axis_tdata[39]_i_2_n_0 ;
  wire \m_axis_tdata[39]_i_3_n_0 ;
  wire \m_axis_tdata[3]_i_2_n_0 ;
  wire \m_axis_tdata[3]_i_3_n_0 ;
  wire \m_axis_tdata[40]_i_2_n_0 ;
  wire \m_axis_tdata[40]_i_3_n_0 ;
  wire \m_axis_tdata[41]_i_2_n_0 ;
  wire \m_axis_tdata[41]_i_3_n_0 ;
  wire \m_axis_tdata[42]_i_2_n_0 ;
  wire \m_axis_tdata[42]_i_3_n_0 ;
  wire \m_axis_tdata[43]_i_2_n_0 ;
  wire \m_axis_tdata[43]_i_3_n_0 ;
  wire \m_axis_tdata[44]_i_2_n_0 ;
  wire \m_axis_tdata[44]_i_3_n_0 ;
  wire \m_axis_tdata[45]_i_2_n_0 ;
  wire \m_axis_tdata[45]_i_3_n_0 ;
  wire \m_axis_tdata[46]_i_2_n_0 ;
  wire \m_axis_tdata[46]_i_3_n_0 ;
  wire \m_axis_tdata[47]_i_1_n_0 ;
  wire \m_axis_tdata[47]_i_4_n_0 ;
  wire \m_axis_tdata[47]_i_5_n_0 ;
  wire \m_axis_tdata[4]_i_2_n_0 ;
  wire \m_axis_tdata[4]_i_3_n_0 ;
  wire \m_axis_tdata[5]_i_2_n_0 ;
  wire \m_axis_tdata[5]_i_3_n_0 ;
  wire \m_axis_tdata[6]_i_2_n_0 ;
  wire \m_axis_tdata[6]_i_3_n_0 ;
  wire \m_axis_tdata[7]_i_2_n_0 ;
  wire \m_axis_tdata[7]_i_3_n_0 ;
  wire \m_axis_tdata[8]_i_2_n_0 ;
  wire \m_axis_tdata[8]_i_3_n_0 ;
  wire \m_axis_tdata[9]_i_2_n_0 ;
  wire \m_axis_tdata[9]_i_3_n_0 ;
  wire m_axis_tready;
  wire m_axis_tvalid;
  wire m_axis_tvalid_i_1_n_0;
  wire p_0_in__0;
  wire [47:0]p_1_in;
  wire p_rdy;
  wire \res_mem_reg_n_0_[0] ;
  wire \res_mem_reg_n_0_[10] ;
  wire \res_mem_reg_n_0_[11] ;
  wire \res_mem_reg_n_0_[12] ;
  wire \res_mem_reg_n_0_[13] ;
  wire \res_mem_reg_n_0_[14] ;
  wire \res_mem_reg_n_0_[15] ;
  wire \res_mem_reg_n_0_[16] ;
  wire \res_mem_reg_n_0_[17] ;
  wire \res_mem_reg_n_0_[18] ;
  wire \res_mem_reg_n_0_[19] ;
  wire \res_mem_reg_n_0_[1] ;
  wire \res_mem_reg_n_0_[20] ;
  wire \res_mem_reg_n_0_[21] ;
  wire \res_mem_reg_n_0_[22] ;
  wire \res_mem_reg_n_0_[23] ;
  wire \res_mem_reg_n_0_[24] ;
  wire \res_mem_reg_n_0_[25] ;
  wire \res_mem_reg_n_0_[26] ;
  wire \res_mem_reg_n_0_[27] ;
  wire \res_mem_reg_n_0_[28] ;
  wire \res_mem_reg_n_0_[29] ;
  wire \res_mem_reg_n_0_[2] ;
  wire \res_mem_reg_n_0_[30] ;
  wire \res_mem_reg_n_0_[31] ;
  wire \res_mem_reg_n_0_[32] ;
  wire \res_mem_reg_n_0_[33] ;
  wire \res_mem_reg_n_0_[34] ;
  wire \res_mem_reg_n_0_[35] ;
  wire \res_mem_reg_n_0_[36] ;
  wire \res_mem_reg_n_0_[37] ;
  wire \res_mem_reg_n_0_[38] ;
  wire \res_mem_reg_n_0_[39] ;
  wire \res_mem_reg_n_0_[3] ;
  wire \res_mem_reg_n_0_[40] ;
  wire \res_mem_reg_n_0_[41] ;
  wire \res_mem_reg_n_0_[42] ;
  wire \res_mem_reg_n_0_[43] ;
  wire \res_mem_reg_n_0_[44] ;
  wire \res_mem_reg_n_0_[45] ;
  wire \res_mem_reg_n_0_[46] ;
  wire \res_mem_reg_n_0_[47] ;
  wire \res_mem_reg_n_0_[4] ;
  wire \res_mem_reg_n_0_[5] ;
  wire \res_mem_reg_n_0_[6] ;
  wire \res_mem_reg_n_0_[7] ;
  wire \res_mem_reg_n_0_[8] ;
  wire \res_mem_reg_n_0_[9] ;
  wire [0:0]\slv_regs_reg[0][12] ;
  wire start;
  wire start_reg_0;

  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT4 #(
    .INIT(16'hA208)) 
    \counter[0]_i_1__0 
       (.I0(start),
        .I1(\counter_reg_n_0_[0] ),
        .I2(Q),
        .I3(m_axis_tready),
        .O(\counter[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT5 #(
    .INIT(32'h02080808)) 
    \counter[1]_i_1__0 
       (.I0(start),
        .I1(\counter_reg_n_0_[1] ),
        .I2(Q),
        .I3(m_axis_tready),
        .I4(\counter_reg_n_0_[0] ),
        .O(\counter[1]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h0220202020202020)) 
    \counter[2]_i_1__0 
       (.I0(start),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(m_axis_tready),
        .I5(\counter_reg_n_0_[1] ),
        .O(\counter[2]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \counter[3]_i_2 
       (.I0(start),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(m_axis_tready),
        .I5(\counter_reg_n_0_[1] ),
        .O(\counter[3]_i_2_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[0] 
       (.C(clk),
        .CE(E),
        .CLR(p_0_in__0),
        .D(\counter[0]_i_1__0_n_0 ),
        .Q(\counter_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[1] 
       (.C(clk),
        .CE(E),
        .CLR(p_0_in__0),
        .D(\counter[1]_i_1__0_n_0 ),
        .Q(\counter_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[2] 
       (.C(clk),
        .CE(E),
        .CLR(p_0_in__0),
        .D(\counter[2]_i_1__0_n_0 ),
        .Q(\counter_reg_n_0_[2] ));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[3] 
       (.C(clk),
        .CE(E),
        .CLR(p_0_in__0),
        .D(\counter[3]_i_2_n_0 ),
        .Q(Q));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[0]_i_1 
       (.I0(\m_axis_tdata[0]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[0]_i_3_n_0 ),
        .O(p_1_in[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[0]_i_2 
       (.I0(data3[0]),
        .I1(data2[0]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[0]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[0] ),
        .O(\m_axis_tdata[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[0]_i_3 
       (.I0(data7[0]),
        .I1(data6[0]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[0]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[0]),
        .O(\m_axis_tdata[0]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[10]_i_1 
       (.I0(\m_axis_tdata[10]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[10]_i_3_n_0 ),
        .O(p_1_in[10]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[10]_i_2 
       (.I0(data3[10]),
        .I1(data2[10]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[10]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[10] ),
        .O(\m_axis_tdata[10]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[10]_i_3 
       (.I0(data7[10]),
        .I1(data6[10]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[10]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[10]),
        .O(\m_axis_tdata[10]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[11]_i_1 
       (.I0(\m_axis_tdata[11]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[11]_i_3_n_0 ),
        .O(p_1_in[11]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[11]_i_2 
       (.I0(data3[11]),
        .I1(data2[11]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[11]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[11] ),
        .O(\m_axis_tdata[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[11]_i_3 
       (.I0(data7[11]),
        .I1(data6[11]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[11]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[11]),
        .O(\m_axis_tdata[11]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[12]_i_1 
       (.I0(\m_axis_tdata[12]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[12]_i_3_n_0 ),
        .O(p_1_in[12]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[12]_i_2 
       (.I0(data3[12]),
        .I1(data2[12]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[12]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[12] ),
        .O(\m_axis_tdata[12]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[12]_i_3 
       (.I0(data7[12]),
        .I1(data6[12]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[12]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[12]),
        .O(\m_axis_tdata[12]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[13]_i_1 
       (.I0(\m_axis_tdata[13]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[13]_i_3_n_0 ),
        .O(p_1_in[13]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[13]_i_2 
       (.I0(data3[13]),
        .I1(data2[13]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[13]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[13] ),
        .O(\m_axis_tdata[13]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[13]_i_3 
       (.I0(data7[13]),
        .I1(data6[13]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[13]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[13]),
        .O(\m_axis_tdata[13]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[14]_i_1 
       (.I0(\m_axis_tdata[14]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[14]_i_3_n_0 ),
        .O(p_1_in[14]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[14]_i_2 
       (.I0(data3[14]),
        .I1(data2[14]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[14]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[14] ),
        .O(\m_axis_tdata[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[14]_i_3 
       (.I0(data7[14]),
        .I1(data6[14]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[14]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[14]),
        .O(\m_axis_tdata[14]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[15]_i_1 
       (.I0(\m_axis_tdata[15]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[15]_i_3_n_0 ),
        .O(p_1_in[15]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[15]_i_2 
       (.I0(data3[15]),
        .I1(data2[15]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[15]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[15] ),
        .O(\m_axis_tdata[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[15]_i_3 
       (.I0(data7[15]),
        .I1(data6[15]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[15]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[15]),
        .O(\m_axis_tdata[15]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[16]_i_1 
       (.I0(\m_axis_tdata[16]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[16]_i_3_n_0 ),
        .O(p_1_in[16]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[16]_i_2 
       (.I0(data3[16]),
        .I1(data2[16]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[16]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[16] ),
        .O(\m_axis_tdata[16]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[16]_i_3 
       (.I0(data7[16]),
        .I1(data6[16]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[16]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[16]),
        .O(\m_axis_tdata[16]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[17]_i_1 
       (.I0(\m_axis_tdata[17]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[17]_i_3_n_0 ),
        .O(p_1_in[17]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[17]_i_2 
       (.I0(data3[17]),
        .I1(data2[17]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[17]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[17] ),
        .O(\m_axis_tdata[17]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[17]_i_3 
       (.I0(data7[17]),
        .I1(data6[17]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[17]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[17]),
        .O(\m_axis_tdata[17]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[18]_i_1 
       (.I0(\m_axis_tdata[18]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[18]_i_3_n_0 ),
        .O(p_1_in[18]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[18]_i_2 
       (.I0(data3[18]),
        .I1(data2[18]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[18]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[18] ),
        .O(\m_axis_tdata[18]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[18]_i_3 
       (.I0(data7[18]),
        .I1(data6[18]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[18]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[18]),
        .O(\m_axis_tdata[18]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[19]_i_1 
       (.I0(\m_axis_tdata[19]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[19]_i_3_n_0 ),
        .O(p_1_in[19]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[19]_i_2 
       (.I0(data3[19]),
        .I1(data2[19]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[19]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[19] ),
        .O(\m_axis_tdata[19]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[19]_i_3 
       (.I0(data7[19]),
        .I1(data6[19]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[19]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[19]),
        .O(\m_axis_tdata[19]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[1]_i_1 
       (.I0(\m_axis_tdata[1]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[1]_i_3_n_0 ),
        .O(p_1_in[1]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[1]_i_2 
       (.I0(data3[1]),
        .I1(data2[1]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[1]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[1] ),
        .O(\m_axis_tdata[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[1]_i_3 
       (.I0(data7[1]),
        .I1(data6[1]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[1]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[1]),
        .O(\m_axis_tdata[1]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[20]_i_1 
       (.I0(\m_axis_tdata[20]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[20]_i_3_n_0 ),
        .O(p_1_in[20]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[20]_i_2 
       (.I0(data3[20]),
        .I1(data2[20]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[20]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[20] ),
        .O(\m_axis_tdata[20]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[20]_i_3 
       (.I0(data7[20]),
        .I1(data6[20]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[20]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[20]),
        .O(\m_axis_tdata[20]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[21]_i_1 
       (.I0(\m_axis_tdata[21]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[21]_i_3_n_0 ),
        .O(p_1_in[21]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[21]_i_2 
       (.I0(data3[21]),
        .I1(data2[21]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[21]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[21] ),
        .O(\m_axis_tdata[21]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[21]_i_3 
       (.I0(data7[21]),
        .I1(data6[21]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[21]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[21]),
        .O(\m_axis_tdata[21]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[22]_i_1 
       (.I0(\m_axis_tdata[22]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[22]_i_3_n_0 ),
        .O(p_1_in[22]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[22]_i_2 
       (.I0(data3[22]),
        .I1(data2[22]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[22]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[22] ),
        .O(\m_axis_tdata[22]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[22]_i_3 
       (.I0(data7[22]),
        .I1(data6[22]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[22]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[22]),
        .O(\m_axis_tdata[22]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[23]_i_1 
       (.I0(\m_axis_tdata[23]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[23]_i_3_n_0 ),
        .O(p_1_in[23]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[23]_i_2 
       (.I0(data3[23]),
        .I1(data2[23]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[23]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[23] ),
        .O(\m_axis_tdata[23]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[23]_i_3 
       (.I0(data7[23]),
        .I1(data6[23]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[23]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[23]),
        .O(\m_axis_tdata[23]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[24]_i_1 
       (.I0(\m_axis_tdata[24]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[24]_i_3_n_0 ),
        .O(p_1_in[24]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[24]_i_2 
       (.I0(data3[24]),
        .I1(data2[24]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[24]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[24] ),
        .O(\m_axis_tdata[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[24]_i_3 
       (.I0(data7[24]),
        .I1(data6[24]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[24]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[24]),
        .O(\m_axis_tdata[24]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[25]_i_1 
       (.I0(\m_axis_tdata[25]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[25]_i_3_n_0 ),
        .O(p_1_in[25]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[25]_i_2 
       (.I0(data3[25]),
        .I1(data2[25]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[25]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[25] ),
        .O(\m_axis_tdata[25]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[25]_i_3 
       (.I0(data7[25]),
        .I1(data6[25]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[25]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[25]),
        .O(\m_axis_tdata[25]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[26]_i_1 
       (.I0(\m_axis_tdata[26]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[26]_i_3_n_0 ),
        .O(p_1_in[26]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[26]_i_2 
       (.I0(data3[26]),
        .I1(data2[26]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[26]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[26] ),
        .O(\m_axis_tdata[26]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[26]_i_3 
       (.I0(data7[26]),
        .I1(data6[26]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[26]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[26]),
        .O(\m_axis_tdata[26]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[27]_i_1 
       (.I0(\m_axis_tdata[27]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[27]_i_3_n_0 ),
        .O(p_1_in[27]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[27]_i_2 
       (.I0(data3[27]),
        .I1(data2[27]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[27]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[27] ),
        .O(\m_axis_tdata[27]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[27]_i_3 
       (.I0(data7[27]),
        .I1(data6[27]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[27]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[27]),
        .O(\m_axis_tdata[27]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[28]_i_1 
       (.I0(\m_axis_tdata[28]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[28]_i_3_n_0 ),
        .O(p_1_in[28]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[28]_i_2 
       (.I0(data3[28]),
        .I1(data2[28]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[28]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[28] ),
        .O(\m_axis_tdata[28]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[28]_i_3 
       (.I0(data7[28]),
        .I1(data6[28]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[28]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[28]),
        .O(\m_axis_tdata[28]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[29]_i_1 
       (.I0(\m_axis_tdata[29]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[29]_i_3_n_0 ),
        .O(p_1_in[29]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[29]_i_2 
       (.I0(data3[29]),
        .I1(data2[29]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[29]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[29] ),
        .O(\m_axis_tdata[29]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[29]_i_3 
       (.I0(data7[29]),
        .I1(data6[29]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[29]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[29]),
        .O(\m_axis_tdata[29]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[2]_i_1 
       (.I0(\m_axis_tdata[2]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[2]_i_3_n_0 ),
        .O(p_1_in[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[2]_i_2 
       (.I0(data3[2]),
        .I1(data2[2]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[2]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[2] ),
        .O(\m_axis_tdata[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[2]_i_3 
       (.I0(data7[2]),
        .I1(data6[2]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[2]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[2]),
        .O(\m_axis_tdata[2]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[30]_i_1 
       (.I0(\m_axis_tdata[30]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[30]_i_3_n_0 ),
        .O(p_1_in[30]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[30]_i_2 
       (.I0(data3[30]),
        .I1(data2[30]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[30]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[30] ),
        .O(\m_axis_tdata[30]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[30]_i_3 
       (.I0(data7[30]),
        .I1(data6[30]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[30]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[30]),
        .O(\m_axis_tdata[30]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[31]_i_1 
       (.I0(\m_axis_tdata[31]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[31]_i_3_n_0 ),
        .O(p_1_in[31]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[31]_i_2 
       (.I0(data3[31]),
        .I1(data2[31]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[31]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[31] ),
        .O(\m_axis_tdata[31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[31]_i_3 
       (.I0(data7[31]),
        .I1(data6[31]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[31]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[31]),
        .O(\m_axis_tdata[31]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[32]_i_1 
       (.I0(\m_axis_tdata[32]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[32]_i_3_n_0 ),
        .O(p_1_in[32]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[32]_i_2 
       (.I0(data3[32]),
        .I1(data2[32]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[32]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[32] ),
        .O(\m_axis_tdata[32]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[32]_i_3 
       (.I0(data7[32]),
        .I1(data6[32]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[32]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[32]),
        .O(\m_axis_tdata[32]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[33]_i_1 
       (.I0(\m_axis_tdata[33]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[33]_i_3_n_0 ),
        .O(p_1_in[33]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[33]_i_2 
       (.I0(data3[33]),
        .I1(data2[33]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[33]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[33] ),
        .O(\m_axis_tdata[33]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[33]_i_3 
       (.I0(data7[33]),
        .I1(data6[33]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[33]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[33]),
        .O(\m_axis_tdata[33]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[34]_i_1 
       (.I0(\m_axis_tdata[34]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[34]_i_3_n_0 ),
        .O(p_1_in[34]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[34]_i_2 
       (.I0(data3[34]),
        .I1(data2[34]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[34]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[34] ),
        .O(\m_axis_tdata[34]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[34]_i_3 
       (.I0(data7[34]),
        .I1(data6[34]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[34]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[34]),
        .O(\m_axis_tdata[34]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[35]_i_1 
       (.I0(\m_axis_tdata[35]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[35]_i_3_n_0 ),
        .O(p_1_in[35]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[35]_i_2 
       (.I0(data3[35]),
        .I1(data2[35]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[35]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[35] ),
        .O(\m_axis_tdata[35]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[35]_i_3 
       (.I0(data7[35]),
        .I1(data6[35]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[35]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[35]),
        .O(\m_axis_tdata[35]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[36]_i_1 
       (.I0(\m_axis_tdata[36]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[36]_i_3_n_0 ),
        .O(p_1_in[36]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[36]_i_2 
       (.I0(data3[36]),
        .I1(data2[36]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[36]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[36] ),
        .O(\m_axis_tdata[36]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[36]_i_3 
       (.I0(data7[36]),
        .I1(data6[36]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[36]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[36]),
        .O(\m_axis_tdata[36]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[37]_i_1 
       (.I0(\m_axis_tdata[37]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[37]_i_3_n_0 ),
        .O(p_1_in[37]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[37]_i_2 
       (.I0(data3[37]),
        .I1(data2[37]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[37]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[37] ),
        .O(\m_axis_tdata[37]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[37]_i_3 
       (.I0(data7[37]),
        .I1(data6[37]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[37]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[37]),
        .O(\m_axis_tdata[37]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[38]_i_1 
       (.I0(\m_axis_tdata[38]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[38]_i_3_n_0 ),
        .O(p_1_in[38]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[38]_i_2 
       (.I0(data3[38]),
        .I1(data2[38]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[38]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[38] ),
        .O(\m_axis_tdata[38]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[38]_i_3 
       (.I0(data7[38]),
        .I1(data6[38]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[38]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[38]),
        .O(\m_axis_tdata[38]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[39]_i_1 
       (.I0(\m_axis_tdata[39]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[39]_i_3_n_0 ),
        .O(p_1_in[39]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[39]_i_2 
       (.I0(data3[39]),
        .I1(data2[39]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[39]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[39] ),
        .O(\m_axis_tdata[39]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[39]_i_3 
       (.I0(data7[39]),
        .I1(data6[39]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[39]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[39]),
        .O(\m_axis_tdata[39]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[3]_i_1 
       (.I0(\m_axis_tdata[3]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[3]_i_3_n_0 ),
        .O(p_1_in[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[3]_i_2 
       (.I0(data3[3]),
        .I1(data2[3]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[3]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[3] ),
        .O(\m_axis_tdata[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[3]_i_3 
       (.I0(data7[3]),
        .I1(data6[3]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[3]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[3]),
        .O(\m_axis_tdata[3]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[40]_i_1 
       (.I0(\m_axis_tdata[40]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[40]_i_3_n_0 ),
        .O(p_1_in[40]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[40]_i_2 
       (.I0(data3[40]),
        .I1(data2[40]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[40]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[40] ),
        .O(\m_axis_tdata[40]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[40]_i_3 
       (.I0(data7[40]),
        .I1(data6[40]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[40]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[40]),
        .O(\m_axis_tdata[40]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[41]_i_1 
       (.I0(\m_axis_tdata[41]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[41]_i_3_n_0 ),
        .O(p_1_in[41]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[41]_i_2 
       (.I0(data3[41]),
        .I1(data2[41]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[41]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[41] ),
        .O(\m_axis_tdata[41]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[41]_i_3 
       (.I0(data7[41]),
        .I1(data6[41]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[41]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[41]),
        .O(\m_axis_tdata[41]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[42]_i_1 
       (.I0(\m_axis_tdata[42]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[42]_i_3_n_0 ),
        .O(p_1_in[42]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[42]_i_2 
       (.I0(data3[42]),
        .I1(data2[42]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[42]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[42] ),
        .O(\m_axis_tdata[42]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[42]_i_3 
       (.I0(data7[42]),
        .I1(data6[42]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[42]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[42]),
        .O(\m_axis_tdata[42]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[43]_i_1 
       (.I0(\m_axis_tdata[43]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[43]_i_3_n_0 ),
        .O(p_1_in[43]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[43]_i_2 
       (.I0(data3[43]),
        .I1(data2[43]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[43]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[43] ),
        .O(\m_axis_tdata[43]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[43]_i_3 
       (.I0(data7[43]),
        .I1(data6[43]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[43]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[43]),
        .O(\m_axis_tdata[43]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[44]_i_1 
       (.I0(\m_axis_tdata[44]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[44]_i_3_n_0 ),
        .O(p_1_in[44]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[44]_i_2 
       (.I0(data3[44]),
        .I1(data2[44]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[44]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[44] ),
        .O(\m_axis_tdata[44]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[44]_i_3 
       (.I0(data7[44]),
        .I1(data6[44]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[44]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[44]),
        .O(\m_axis_tdata[44]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[45]_i_1 
       (.I0(\m_axis_tdata[45]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[45]_i_3_n_0 ),
        .O(p_1_in[45]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[45]_i_2 
       (.I0(data3[45]),
        .I1(data2[45]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[45]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[45] ),
        .O(\m_axis_tdata[45]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[45]_i_3 
       (.I0(data7[45]),
        .I1(data6[45]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[45]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[45]),
        .O(\m_axis_tdata[45]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[46]_i_1 
       (.I0(\m_axis_tdata[46]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[46]_i_3_n_0 ),
        .O(p_1_in[46]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[46]_i_2 
       (.I0(data3[46]),
        .I1(data2[46]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[46]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[46] ),
        .O(\m_axis_tdata[46]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[46]_i_3 
       (.I0(data7[46]),
        .I1(data6[46]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[46]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[46]),
        .O(\m_axis_tdata[46]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \m_axis_tdata[47]_i_1 
       (.I0(start),
        .I1(p_rdy),
        .O(\m_axis_tdata[47]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[47]_i_2 
       (.I0(\m_axis_tdata[47]_i_4_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[47]_i_5_n_0 ),
        .O(p_1_in[47]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[47]_i_4 
       (.I0(data3[47]),
        .I1(data2[47]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[47]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[47] ),
        .O(\m_axis_tdata[47]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[47]_i_5 
       (.I0(data7[47]),
        .I1(data6[47]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[47]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[47]),
        .O(\m_axis_tdata[47]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[4]_i_1 
       (.I0(\m_axis_tdata[4]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[4]_i_3_n_0 ),
        .O(p_1_in[4]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[4]_i_2 
       (.I0(data3[4]),
        .I1(data2[4]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[4]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[4] ),
        .O(\m_axis_tdata[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[4]_i_3 
       (.I0(data7[4]),
        .I1(data6[4]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[4]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[4]),
        .O(\m_axis_tdata[4]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[5]_i_1 
       (.I0(\m_axis_tdata[5]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[5]_i_3_n_0 ),
        .O(p_1_in[5]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[5]_i_2 
       (.I0(data3[5]),
        .I1(data2[5]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[5]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[5] ),
        .O(\m_axis_tdata[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[5]_i_3 
       (.I0(data7[5]),
        .I1(data6[5]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[5]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[5]),
        .O(\m_axis_tdata[5]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[6]_i_1 
       (.I0(\m_axis_tdata[6]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[6]_i_3_n_0 ),
        .O(p_1_in[6]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[6]_i_2 
       (.I0(data3[6]),
        .I1(data2[6]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[6]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[6] ),
        .O(\m_axis_tdata[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[6]_i_3 
       (.I0(data7[6]),
        .I1(data6[6]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[6]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[6]),
        .O(\m_axis_tdata[6]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[7]_i_1 
       (.I0(\m_axis_tdata[7]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[7]_i_3_n_0 ),
        .O(p_1_in[7]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[7]_i_2 
       (.I0(data3[7]),
        .I1(data2[7]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[7]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[7] ),
        .O(\m_axis_tdata[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[7]_i_3 
       (.I0(data7[7]),
        .I1(data6[7]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[7]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[7]),
        .O(\m_axis_tdata[7]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[8]_i_1 
       (.I0(\m_axis_tdata[8]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[8]_i_3_n_0 ),
        .O(p_1_in[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[8]_i_2 
       (.I0(data3[8]),
        .I1(data2[8]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[8]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[8] ),
        .O(\m_axis_tdata[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[8]_i_3 
       (.I0(data7[8]),
        .I1(data6[8]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[8]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[8]),
        .O(\m_axis_tdata[8]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h3202)) 
    \m_axis_tdata[9]_i_1 
       (.I0(\m_axis_tdata[9]_i_2_n_0 ),
        .I1(Q),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\m_axis_tdata[9]_i_3_n_0 ),
        .O(p_1_in[9]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[9]_i_2 
       (.I0(data3[9]),
        .I1(data2[9]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data1[9]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(\res_mem_reg_n_0_[9] ),
        .O(\m_axis_tdata[9]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[9]_i_3 
       (.I0(data7[9]),
        .I1(data6[9]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(data5[9]),
        .I4(\counter_reg_n_0_[0] ),
        .I5(data4[9]),
        .O(\m_axis_tdata[9]_i_3_n_0 ));
  FDCE \m_axis_tdata_reg[0] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[0]),
        .Q(m_axis_tdata[0]));
  FDCE \m_axis_tdata_reg[10] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[10]),
        .Q(m_axis_tdata[10]));
  FDCE \m_axis_tdata_reg[11] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[11]),
        .Q(m_axis_tdata[11]));
  FDCE \m_axis_tdata_reg[12] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[12]),
        .Q(m_axis_tdata[12]));
  FDCE \m_axis_tdata_reg[13] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[13]),
        .Q(m_axis_tdata[13]));
  FDCE \m_axis_tdata_reg[14] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[14]),
        .Q(m_axis_tdata[14]));
  FDCE \m_axis_tdata_reg[15] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[15]),
        .Q(m_axis_tdata[15]));
  FDCE \m_axis_tdata_reg[16] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[16]),
        .Q(m_axis_tdata[16]));
  FDCE \m_axis_tdata_reg[17] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[17]),
        .Q(m_axis_tdata[17]));
  FDCE \m_axis_tdata_reg[18] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[18]),
        .Q(m_axis_tdata[18]));
  FDCE \m_axis_tdata_reg[19] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[19]),
        .Q(m_axis_tdata[19]));
  FDCE \m_axis_tdata_reg[1] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[1]),
        .Q(m_axis_tdata[1]));
  FDCE \m_axis_tdata_reg[20] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[20]),
        .Q(m_axis_tdata[20]));
  FDCE \m_axis_tdata_reg[21] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[21]),
        .Q(m_axis_tdata[21]));
  FDCE \m_axis_tdata_reg[22] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[22]),
        .Q(m_axis_tdata[22]));
  FDCE \m_axis_tdata_reg[23] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[23]),
        .Q(m_axis_tdata[23]));
  FDCE \m_axis_tdata_reg[24] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[24]),
        .Q(m_axis_tdata[24]));
  FDCE \m_axis_tdata_reg[25] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[25]),
        .Q(m_axis_tdata[25]));
  FDCE \m_axis_tdata_reg[26] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[26]),
        .Q(m_axis_tdata[26]));
  FDCE \m_axis_tdata_reg[27] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[27]),
        .Q(m_axis_tdata[27]));
  FDCE \m_axis_tdata_reg[28] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[28]),
        .Q(m_axis_tdata[28]));
  FDCE \m_axis_tdata_reg[29] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[29]),
        .Q(m_axis_tdata[29]));
  FDCE \m_axis_tdata_reg[2] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[2]),
        .Q(m_axis_tdata[2]));
  FDCE \m_axis_tdata_reg[30] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[30]),
        .Q(m_axis_tdata[30]));
  FDCE \m_axis_tdata_reg[31] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[31]),
        .Q(m_axis_tdata[31]));
  FDCE \m_axis_tdata_reg[32] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[32]),
        .Q(m_axis_tdata[32]));
  FDCE \m_axis_tdata_reg[33] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[33]),
        .Q(m_axis_tdata[33]));
  FDCE \m_axis_tdata_reg[34] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[34]),
        .Q(m_axis_tdata[34]));
  FDCE \m_axis_tdata_reg[35] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[35]),
        .Q(m_axis_tdata[35]));
  FDCE \m_axis_tdata_reg[36] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[36]),
        .Q(m_axis_tdata[36]));
  FDCE \m_axis_tdata_reg[37] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[37]),
        .Q(m_axis_tdata[37]));
  FDCE \m_axis_tdata_reg[38] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[38]),
        .Q(m_axis_tdata[38]));
  FDCE \m_axis_tdata_reg[39] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[39]),
        .Q(m_axis_tdata[39]));
  FDCE \m_axis_tdata_reg[3] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[3]),
        .Q(m_axis_tdata[3]));
  FDCE \m_axis_tdata_reg[40] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[40]),
        .Q(m_axis_tdata[40]));
  FDCE \m_axis_tdata_reg[41] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[41]),
        .Q(m_axis_tdata[41]));
  FDCE \m_axis_tdata_reg[42] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[42]),
        .Q(m_axis_tdata[42]));
  FDCE \m_axis_tdata_reg[43] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[43]),
        .Q(m_axis_tdata[43]));
  FDCE \m_axis_tdata_reg[44] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[44]),
        .Q(m_axis_tdata[44]));
  FDCE \m_axis_tdata_reg[45] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[45]),
        .Q(m_axis_tdata[45]));
  FDCE \m_axis_tdata_reg[46] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[46]),
        .Q(m_axis_tdata[46]));
  FDCE \m_axis_tdata_reg[47] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[47]),
        .Q(m_axis_tdata[47]));
  FDCE \m_axis_tdata_reg[4] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[4]),
        .Q(m_axis_tdata[4]));
  FDCE \m_axis_tdata_reg[5] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[5]),
        .Q(m_axis_tdata[5]));
  FDCE \m_axis_tdata_reg[6] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[6]),
        .Q(m_axis_tdata[6]));
  FDCE \m_axis_tdata_reg[7] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[7]),
        .Q(m_axis_tdata[7]));
  FDCE \m_axis_tdata_reg[8] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[8]),
        .Q(m_axis_tdata[8]));
  FDCE \m_axis_tdata_reg[9] 
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(p_1_in[9]),
        .Q(m_axis_tdata[9]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT1 #(
    .INIT(2'h1)) 
    m_axis_tvalid_i_1
       (.I0(Q),
        .O(m_axis_tvalid_i_1_n_0));
  FDCE m_axis_tvalid_reg
       (.C(clk),
        .CE(\m_axis_tdata[47]_i_1_n_0 ),
        .CLR(p_0_in__0),
        .D(m_axis_tvalid_i_1_n_0),
        .Q(m_axis_tvalid));
  FDRE \res_mem_reg[0] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[0]),
        .Q(\res_mem_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \res_mem_reg[100] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[100]),
        .Q(data2[4]),
        .R(1'b0));
  FDRE \res_mem_reg[101] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[101]),
        .Q(data2[5]),
        .R(1'b0));
  FDRE \res_mem_reg[102] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[102]),
        .Q(data2[6]),
        .R(1'b0));
  FDRE \res_mem_reg[103] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[103]),
        .Q(data2[7]),
        .R(1'b0));
  FDRE \res_mem_reg[104] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[104]),
        .Q(data2[8]),
        .R(1'b0));
  FDRE \res_mem_reg[105] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[105]),
        .Q(data2[9]),
        .R(1'b0));
  FDRE \res_mem_reg[106] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[106]),
        .Q(data2[10]),
        .R(1'b0));
  FDRE \res_mem_reg[107] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[107]),
        .Q(data2[11]),
        .R(1'b0));
  FDRE \res_mem_reg[108] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[108]),
        .Q(data2[12]),
        .R(1'b0));
  FDRE \res_mem_reg[109] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[109]),
        .Q(data2[13]),
        .R(1'b0));
  FDRE \res_mem_reg[10] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[10]),
        .Q(\res_mem_reg_n_0_[10] ),
        .R(1'b0));
  FDRE \res_mem_reg[110] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[110]),
        .Q(data2[14]),
        .R(1'b0));
  FDRE \res_mem_reg[111] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[111]),
        .Q(data2[15]),
        .R(1'b0));
  FDRE \res_mem_reg[112] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[112]),
        .Q(data2[16]),
        .R(1'b0));
  FDRE \res_mem_reg[113] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[113]),
        .Q(data2[17]),
        .R(1'b0));
  FDRE \res_mem_reg[114] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[114]),
        .Q(data2[18]),
        .R(1'b0));
  FDRE \res_mem_reg[115] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[115]),
        .Q(data2[19]),
        .R(1'b0));
  FDRE \res_mem_reg[116] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[116]),
        .Q(data2[20]),
        .R(1'b0));
  FDRE \res_mem_reg[117] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[117]),
        .Q(data2[21]),
        .R(1'b0));
  FDRE \res_mem_reg[118] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[118]),
        .Q(data2[22]),
        .R(1'b0));
  FDRE \res_mem_reg[119] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[119]),
        .Q(data2[23]),
        .R(1'b0));
  FDRE \res_mem_reg[11] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[11]),
        .Q(\res_mem_reg_n_0_[11] ),
        .R(1'b0));
  FDRE \res_mem_reg[120] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[120]),
        .Q(data2[24]),
        .R(1'b0));
  FDRE \res_mem_reg[121] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[121]),
        .Q(data2[25]),
        .R(1'b0));
  FDRE \res_mem_reg[122] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[122]),
        .Q(data2[26]),
        .R(1'b0));
  FDRE \res_mem_reg[123] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[123]),
        .Q(data2[27]),
        .R(1'b0));
  FDRE \res_mem_reg[124] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[124]),
        .Q(data2[28]),
        .R(1'b0));
  FDRE \res_mem_reg[125] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[125]),
        .Q(data2[29]),
        .R(1'b0));
  FDRE \res_mem_reg[126] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[126]),
        .Q(data2[30]),
        .R(1'b0));
  FDRE \res_mem_reg[127] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[127]),
        .Q(data2[31]),
        .R(1'b0));
  FDRE \res_mem_reg[128] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[128]),
        .Q(data2[32]),
        .R(1'b0));
  FDRE \res_mem_reg[129] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[129]),
        .Q(data2[33]),
        .R(1'b0));
  FDRE \res_mem_reg[12] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[12]),
        .Q(\res_mem_reg_n_0_[12] ),
        .R(1'b0));
  FDRE \res_mem_reg[130] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[130]),
        .Q(data2[34]),
        .R(1'b0));
  FDRE \res_mem_reg[131] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[131]),
        .Q(data2[35]),
        .R(1'b0));
  FDRE \res_mem_reg[132] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[132]),
        .Q(data2[36]),
        .R(1'b0));
  FDRE \res_mem_reg[133] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[133]),
        .Q(data2[37]),
        .R(1'b0));
  FDRE \res_mem_reg[134] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[134]),
        .Q(data2[38]),
        .R(1'b0));
  FDRE \res_mem_reg[135] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[135]),
        .Q(data2[39]),
        .R(1'b0));
  FDRE \res_mem_reg[136] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[136]),
        .Q(data2[40]),
        .R(1'b0));
  FDRE \res_mem_reg[137] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[137]),
        .Q(data2[41]),
        .R(1'b0));
  FDRE \res_mem_reg[138] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[138]),
        .Q(data2[42]),
        .R(1'b0));
  FDRE \res_mem_reg[139] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[139]),
        .Q(data2[43]),
        .R(1'b0));
  FDRE \res_mem_reg[13] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[13]),
        .Q(\res_mem_reg_n_0_[13] ),
        .R(1'b0));
  FDRE \res_mem_reg[140] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[140]),
        .Q(data2[44]),
        .R(1'b0));
  FDRE \res_mem_reg[141] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[141]),
        .Q(data2[45]),
        .R(1'b0));
  FDRE \res_mem_reg[142] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[142]),
        .Q(data2[46]),
        .R(1'b0));
  FDRE \res_mem_reg[143] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[143]),
        .Q(data2[47]),
        .R(1'b0));
  FDRE \res_mem_reg[144] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[144]),
        .Q(data3[0]),
        .R(1'b0));
  FDRE \res_mem_reg[145] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[145]),
        .Q(data3[1]),
        .R(1'b0));
  FDRE \res_mem_reg[146] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[146]),
        .Q(data3[2]),
        .R(1'b0));
  FDRE \res_mem_reg[147] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[147]),
        .Q(data3[3]),
        .R(1'b0));
  FDRE \res_mem_reg[148] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[148]),
        .Q(data3[4]),
        .R(1'b0));
  FDRE \res_mem_reg[149] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[149]),
        .Q(data3[5]),
        .R(1'b0));
  FDRE \res_mem_reg[14] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[14]),
        .Q(\res_mem_reg_n_0_[14] ),
        .R(1'b0));
  FDRE \res_mem_reg[150] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[150]),
        .Q(data3[6]),
        .R(1'b0));
  FDRE \res_mem_reg[151] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[151]),
        .Q(data3[7]),
        .R(1'b0));
  FDRE \res_mem_reg[152] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[152]),
        .Q(data3[8]),
        .R(1'b0));
  FDRE \res_mem_reg[153] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[153]),
        .Q(data3[9]),
        .R(1'b0));
  FDRE \res_mem_reg[154] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[154]),
        .Q(data3[10]),
        .R(1'b0));
  FDRE \res_mem_reg[155] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[155]),
        .Q(data3[11]),
        .R(1'b0));
  FDRE \res_mem_reg[156] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[156]),
        .Q(data3[12]),
        .R(1'b0));
  FDRE \res_mem_reg[157] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[157]),
        .Q(data3[13]),
        .R(1'b0));
  FDRE \res_mem_reg[158] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[158]),
        .Q(data3[14]),
        .R(1'b0));
  FDRE \res_mem_reg[159] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[159]),
        .Q(data3[15]),
        .R(1'b0));
  FDRE \res_mem_reg[15] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[15]),
        .Q(\res_mem_reg_n_0_[15] ),
        .R(1'b0));
  FDRE \res_mem_reg[160] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[160]),
        .Q(data3[16]),
        .R(1'b0));
  FDRE \res_mem_reg[161] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[161]),
        .Q(data3[17]),
        .R(1'b0));
  FDRE \res_mem_reg[162] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[162]),
        .Q(data3[18]),
        .R(1'b0));
  FDRE \res_mem_reg[163] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[163]),
        .Q(data3[19]),
        .R(1'b0));
  FDRE \res_mem_reg[164] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[164]),
        .Q(data3[20]),
        .R(1'b0));
  FDRE \res_mem_reg[165] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[165]),
        .Q(data3[21]),
        .R(1'b0));
  FDRE \res_mem_reg[166] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[166]),
        .Q(data3[22]),
        .R(1'b0));
  FDRE \res_mem_reg[167] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[167]),
        .Q(data3[23]),
        .R(1'b0));
  FDRE \res_mem_reg[168] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[168]),
        .Q(data3[24]),
        .R(1'b0));
  FDRE \res_mem_reg[169] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[169]),
        .Q(data3[25]),
        .R(1'b0));
  FDRE \res_mem_reg[16] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[16]),
        .Q(\res_mem_reg_n_0_[16] ),
        .R(1'b0));
  FDRE \res_mem_reg[170] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[170]),
        .Q(data3[26]),
        .R(1'b0));
  FDRE \res_mem_reg[171] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[171]),
        .Q(data3[27]),
        .R(1'b0));
  FDRE \res_mem_reg[172] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[172]),
        .Q(data3[28]),
        .R(1'b0));
  FDRE \res_mem_reg[173] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[173]),
        .Q(data3[29]),
        .R(1'b0));
  FDRE \res_mem_reg[174] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[174]),
        .Q(data3[30]),
        .R(1'b0));
  FDRE \res_mem_reg[175] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[175]),
        .Q(data3[31]),
        .R(1'b0));
  FDRE \res_mem_reg[176] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[176]),
        .Q(data3[32]),
        .R(1'b0));
  FDRE \res_mem_reg[177] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[177]),
        .Q(data3[33]),
        .R(1'b0));
  FDRE \res_mem_reg[178] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[178]),
        .Q(data3[34]),
        .R(1'b0));
  FDRE \res_mem_reg[179] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[179]),
        .Q(data3[35]),
        .R(1'b0));
  FDRE \res_mem_reg[17] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[17]),
        .Q(\res_mem_reg_n_0_[17] ),
        .R(1'b0));
  FDRE \res_mem_reg[180] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[180]),
        .Q(data3[36]),
        .R(1'b0));
  FDRE \res_mem_reg[181] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[181]),
        .Q(data3[37]),
        .R(1'b0));
  FDRE \res_mem_reg[182] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[182]),
        .Q(data3[38]),
        .R(1'b0));
  FDRE \res_mem_reg[183] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[183]),
        .Q(data3[39]),
        .R(1'b0));
  FDRE \res_mem_reg[184] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[184]),
        .Q(data3[40]),
        .R(1'b0));
  FDRE \res_mem_reg[185] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[185]),
        .Q(data3[41]),
        .R(1'b0));
  FDRE \res_mem_reg[186] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[186]),
        .Q(data3[42]),
        .R(1'b0));
  FDRE \res_mem_reg[187] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[187]),
        .Q(data3[43]),
        .R(1'b0));
  FDRE \res_mem_reg[188] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[188]),
        .Q(data3[44]),
        .R(1'b0));
  FDRE \res_mem_reg[189] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[189]),
        .Q(data3[45]),
        .R(1'b0));
  FDRE \res_mem_reg[18] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[18]),
        .Q(\res_mem_reg_n_0_[18] ),
        .R(1'b0));
  FDRE \res_mem_reg[190] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[190]),
        .Q(data3[46]),
        .R(1'b0));
  FDRE \res_mem_reg[191] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[191]),
        .Q(data3[47]),
        .R(1'b0));
  FDRE \res_mem_reg[192] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[192]),
        .Q(data4[0]),
        .R(1'b0));
  FDRE \res_mem_reg[193] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[193]),
        .Q(data4[1]),
        .R(1'b0));
  FDRE \res_mem_reg[194] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[194]),
        .Q(data4[2]),
        .R(1'b0));
  FDRE \res_mem_reg[195] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[195]),
        .Q(data4[3]),
        .R(1'b0));
  FDRE \res_mem_reg[196] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[196]),
        .Q(data4[4]),
        .R(1'b0));
  FDRE \res_mem_reg[197] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[197]),
        .Q(data4[5]),
        .R(1'b0));
  FDRE \res_mem_reg[198] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[198]),
        .Q(data4[6]),
        .R(1'b0));
  FDRE \res_mem_reg[199] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[199]),
        .Q(data4[7]),
        .R(1'b0));
  FDRE \res_mem_reg[19] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[19]),
        .Q(\res_mem_reg_n_0_[19] ),
        .R(1'b0));
  FDRE \res_mem_reg[1] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[1]),
        .Q(\res_mem_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \res_mem_reg[200] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[200]),
        .Q(data4[8]),
        .R(1'b0));
  FDRE \res_mem_reg[201] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[201]),
        .Q(data4[9]),
        .R(1'b0));
  FDRE \res_mem_reg[202] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[202]),
        .Q(data4[10]),
        .R(1'b0));
  FDRE \res_mem_reg[203] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[203]),
        .Q(data4[11]),
        .R(1'b0));
  FDRE \res_mem_reg[204] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[204]),
        .Q(data4[12]),
        .R(1'b0));
  FDRE \res_mem_reg[205] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[205]),
        .Q(data4[13]),
        .R(1'b0));
  FDRE \res_mem_reg[206] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[206]),
        .Q(data4[14]),
        .R(1'b0));
  FDRE \res_mem_reg[207] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[207]),
        .Q(data4[15]),
        .R(1'b0));
  FDRE \res_mem_reg[208] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[208]),
        .Q(data4[16]),
        .R(1'b0));
  FDRE \res_mem_reg[209] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[209]),
        .Q(data4[17]),
        .R(1'b0));
  FDRE \res_mem_reg[20] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[20]),
        .Q(\res_mem_reg_n_0_[20] ),
        .R(1'b0));
  FDRE \res_mem_reg[210] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[210]),
        .Q(data4[18]),
        .R(1'b0));
  FDRE \res_mem_reg[211] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[211]),
        .Q(data4[19]),
        .R(1'b0));
  FDRE \res_mem_reg[212] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[212]),
        .Q(data4[20]),
        .R(1'b0));
  FDRE \res_mem_reg[213] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[213]),
        .Q(data4[21]),
        .R(1'b0));
  FDRE \res_mem_reg[214] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[214]),
        .Q(data4[22]),
        .R(1'b0));
  FDRE \res_mem_reg[215] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[215]),
        .Q(data4[23]),
        .R(1'b0));
  FDRE \res_mem_reg[216] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[216]),
        .Q(data4[24]),
        .R(1'b0));
  FDRE \res_mem_reg[217] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[217]),
        .Q(data4[25]),
        .R(1'b0));
  FDRE \res_mem_reg[218] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[218]),
        .Q(data4[26]),
        .R(1'b0));
  FDRE \res_mem_reg[219] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[219]),
        .Q(data4[27]),
        .R(1'b0));
  FDRE \res_mem_reg[21] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[21]),
        .Q(\res_mem_reg_n_0_[21] ),
        .R(1'b0));
  FDRE \res_mem_reg[220] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[220]),
        .Q(data4[28]),
        .R(1'b0));
  FDRE \res_mem_reg[221] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[221]),
        .Q(data4[29]),
        .R(1'b0));
  FDRE \res_mem_reg[222] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[222]),
        .Q(data4[30]),
        .R(1'b0));
  FDRE \res_mem_reg[223] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[223]),
        .Q(data4[31]),
        .R(1'b0));
  FDRE \res_mem_reg[224] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[224]),
        .Q(data4[32]),
        .R(1'b0));
  FDRE \res_mem_reg[225] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[225]),
        .Q(data4[33]),
        .R(1'b0));
  FDRE \res_mem_reg[226] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[226]),
        .Q(data4[34]),
        .R(1'b0));
  FDRE \res_mem_reg[227] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[227]),
        .Q(data4[35]),
        .R(1'b0));
  FDRE \res_mem_reg[228] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[228]),
        .Q(data4[36]),
        .R(1'b0));
  FDRE \res_mem_reg[229] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[229]),
        .Q(data4[37]),
        .R(1'b0));
  FDRE \res_mem_reg[22] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[22]),
        .Q(\res_mem_reg_n_0_[22] ),
        .R(1'b0));
  FDRE \res_mem_reg[230] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[230]),
        .Q(data4[38]),
        .R(1'b0));
  FDRE \res_mem_reg[231] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[231]),
        .Q(data4[39]),
        .R(1'b0));
  FDRE \res_mem_reg[232] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[232]),
        .Q(data4[40]),
        .R(1'b0));
  FDRE \res_mem_reg[233] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[233]),
        .Q(data4[41]),
        .R(1'b0));
  FDRE \res_mem_reg[234] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[234]),
        .Q(data4[42]),
        .R(1'b0));
  FDRE \res_mem_reg[235] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[235]),
        .Q(data4[43]),
        .R(1'b0));
  FDRE \res_mem_reg[236] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[236]),
        .Q(data4[44]),
        .R(1'b0));
  FDRE \res_mem_reg[237] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[237]),
        .Q(data4[45]),
        .R(1'b0));
  FDRE \res_mem_reg[238] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[238]),
        .Q(data4[46]),
        .R(1'b0));
  FDRE \res_mem_reg[239] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[239]),
        .Q(data4[47]),
        .R(1'b0));
  FDRE \res_mem_reg[23] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[23]),
        .Q(\res_mem_reg_n_0_[23] ),
        .R(1'b0));
  FDRE \res_mem_reg[240] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[240]),
        .Q(data5[0]),
        .R(1'b0));
  FDRE \res_mem_reg[241] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[241]),
        .Q(data5[1]),
        .R(1'b0));
  FDRE \res_mem_reg[242] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[242]),
        .Q(data5[2]),
        .R(1'b0));
  FDRE \res_mem_reg[243] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[243]),
        .Q(data5[3]),
        .R(1'b0));
  FDRE \res_mem_reg[244] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[244]),
        .Q(data5[4]),
        .R(1'b0));
  FDRE \res_mem_reg[245] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[245]),
        .Q(data5[5]),
        .R(1'b0));
  FDRE \res_mem_reg[246] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[246]),
        .Q(data5[6]),
        .R(1'b0));
  FDRE \res_mem_reg[247] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[247]),
        .Q(data5[7]),
        .R(1'b0));
  FDRE \res_mem_reg[248] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[248]),
        .Q(data5[8]),
        .R(1'b0));
  FDRE \res_mem_reg[249] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[249]),
        .Q(data5[9]),
        .R(1'b0));
  FDRE \res_mem_reg[24] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[24]),
        .Q(\res_mem_reg_n_0_[24] ),
        .R(1'b0));
  FDRE \res_mem_reg[250] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[250]),
        .Q(data5[10]),
        .R(1'b0));
  FDRE \res_mem_reg[251] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[251]),
        .Q(data5[11]),
        .R(1'b0));
  FDRE \res_mem_reg[252] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[252]),
        .Q(data5[12]),
        .R(1'b0));
  FDRE \res_mem_reg[253] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[253]),
        .Q(data5[13]),
        .R(1'b0));
  FDRE \res_mem_reg[254] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[254]),
        .Q(data5[14]),
        .R(1'b0));
  FDRE \res_mem_reg[255] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[255]),
        .Q(data5[15]),
        .R(1'b0));
  FDRE \res_mem_reg[256] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[256]),
        .Q(data5[16]),
        .R(1'b0));
  FDRE \res_mem_reg[257] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[257]),
        .Q(data5[17]),
        .R(1'b0));
  FDRE \res_mem_reg[258] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[258]),
        .Q(data5[18]),
        .R(1'b0));
  FDRE \res_mem_reg[259] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[259]),
        .Q(data5[19]),
        .R(1'b0));
  FDRE \res_mem_reg[25] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[25]),
        .Q(\res_mem_reg_n_0_[25] ),
        .R(1'b0));
  FDRE \res_mem_reg[260] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[260]),
        .Q(data5[20]),
        .R(1'b0));
  FDRE \res_mem_reg[261] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[261]),
        .Q(data5[21]),
        .R(1'b0));
  FDRE \res_mem_reg[262] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[262]),
        .Q(data5[22]),
        .R(1'b0));
  FDRE \res_mem_reg[263] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[263]),
        .Q(data5[23]),
        .R(1'b0));
  FDRE \res_mem_reg[264] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[264]),
        .Q(data5[24]),
        .R(1'b0));
  FDRE \res_mem_reg[265] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[265]),
        .Q(data5[25]),
        .R(1'b0));
  FDRE \res_mem_reg[266] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[266]),
        .Q(data5[26]),
        .R(1'b0));
  FDRE \res_mem_reg[267] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[267]),
        .Q(data5[27]),
        .R(1'b0));
  FDRE \res_mem_reg[268] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[268]),
        .Q(data5[28]),
        .R(1'b0));
  FDRE \res_mem_reg[269] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[269]),
        .Q(data5[29]),
        .R(1'b0));
  FDRE \res_mem_reg[26] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[26]),
        .Q(\res_mem_reg_n_0_[26] ),
        .R(1'b0));
  FDRE \res_mem_reg[270] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[270]),
        .Q(data5[30]),
        .R(1'b0));
  FDRE \res_mem_reg[271] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[271]),
        .Q(data5[31]),
        .R(1'b0));
  FDRE \res_mem_reg[272] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[272]),
        .Q(data5[32]),
        .R(1'b0));
  FDRE \res_mem_reg[273] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[273]),
        .Q(data5[33]),
        .R(1'b0));
  FDRE \res_mem_reg[274] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[274]),
        .Q(data5[34]),
        .R(1'b0));
  FDRE \res_mem_reg[275] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[275]),
        .Q(data5[35]),
        .R(1'b0));
  FDRE \res_mem_reg[276] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[276]),
        .Q(data5[36]),
        .R(1'b0));
  FDRE \res_mem_reg[277] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[277]),
        .Q(data5[37]),
        .R(1'b0));
  FDRE \res_mem_reg[278] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[278]),
        .Q(data5[38]),
        .R(1'b0));
  FDRE \res_mem_reg[279] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[279]),
        .Q(data5[39]),
        .R(1'b0));
  FDRE \res_mem_reg[27] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[27]),
        .Q(\res_mem_reg_n_0_[27] ),
        .R(1'b0));
  FDRE \res_mem_reg[280] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[280]),
        .Q(data5[40]),
        .R(1'b0));
  FDRE \res_mem_reg[281] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[281]),
        .Q(data5[41]),
        .R(1'b0));
  FDRE \res_mem_reg[282] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[282]),
        .Q(data5[42]),
        .R(1'b0));
  FDRE \res_mem_reg[283] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[283]),
        .Q(data5[43]),
        .R(1'b0));
  FDRE \res_mem_reg[284] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[284]),
        .Q(data5[44]),
        .R(1'b0));
  FDRE \res_mem_reg[285] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[285]),
        .Q(data5[45]),
        .R(1'b0));
  FDRE \res_mem_reg[286] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[286]),
        .Q(data5[46]),
        .R(1'b0));
  FDRE \res_mem_reg[287] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[287]),
        .Q(data5[47]),
        .R(1'b0));
  FDRE \res_mem_reg[288] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[288]),
        .Q(data6[0]),
        .R(1'b0));
  FDRE \res_mem_reg[289] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[289]),
        .Q(data6[1]),
        .R(1'b0));
  FDRE \res_mem_reg[28] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[28]),
        .Q(\res_mem_reg_n_0_[28] ),
        .R(1'b0));
  FDRE \res_mem_reg[290] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[290]),
        .Q(data6[2]),
        .R(1'b0));
  FDRE \res_mem_reg[291] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[291]),
        .Q(data6[3]),
        .R(1'b0));
  FDRE \res_mem_reg[292] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[292]),
        .Q(data6[4]),
        .R(1'b0));
  FDRE \res_mem_reg[293] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[293]),
        .Q(data6[5]),
        .R(1'b0));
  FDRE \res_mem_reg[294] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[294]),
        .Q(data6[6]),
        .R(1'b0));
  FDRE \res_mem_reg[295] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[295]),
        .Q(data6[7]),
        .R(1'b0));
  FDRE \res_mem_reg[296] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[296]),
        .Q(data6[8]),
        .R(1'b0));
  FDRE \res_mem_reg[297] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[297]),
        .Q(data6[9]),
        .R(1'b0));
  FDRE \res_mem_reg[298] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[298]),
        .Q(data6[10]),
        .R(1'b0));
  FDRE \res_mem_reg[299] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[299]),
        .Q(data6[11]),
        .R(1'b0));
  FDRE \res_mem_reg[29] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[29]),
        .Q(\res_mem_reg_n_0_[29] ),
        .R(1'b0));
  FDRE \res_mem_reg[2] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[2]),
        .Q(\res_mem_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \res_mem_reg[300] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[300]),
        .Q(data6[12]),
        .R(1'b0));
  FDRE \res_mem_reg[301] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[301]),
        .Q(data6[13]),
        .R(1'b0));
  FDRE \res_mem_reg[302] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[302]),
        .Q(data6[14]),
        .R(1'b0));
  FDRE \res_mem_reg[303] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[303]),
        .Q(data6[15]),
        .R(1'b0));
  FDRE \res_mem_reg[304] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[304]),
        .Q(data6[16]),
        .R(1'b0));
  FDRE \res_mem_reg[305] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[305]),
        .Q(data6[17]),
        .R(1'b0));
  FDRE \res_mem_reg[306] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[306]),
        .Q(data6[18]),
        .R(1'b0));
  FDRE \res_mem_reg[307] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[307]),
        .Q(data6[19]),
        .R(1'b0));
  FDRE \res_mem_reg[308] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[308]),
        .Q(data6[20]),
        .R(1'b0));
  FDRE \res_mem_reg[309] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[309]),
        .Q(data6[21]),
        .R(1'b0));
  FDRE \res_mem_reg[30] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[30]),
        .Q(\res_mem_reg_n_0_[30] ),
        .R(1'b0));
  FDRE \res_mem_reg[310] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[310]),
        .Q(data6[22]),
        .R(1'b0));
  FDRE \res_mem_reg[311] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[311]),
        .Q(data6[23]),
        .R(1'b0));
  FDRE \res_mem_reg[312] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[312]),
        .Q(data6[24]),
        .R(1'b0));
  FDRE \res_mem_reg[313] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[313]),
        .Q(data6[25]),
        .R(1'b0));
  FDRE \res_mem_reg[314] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[314]),
        .Q(data6[26]),
        .R(1'b0));
  FDRE \res_mem_reg[315] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[315]),
        .Q(data6[27]),
        .R(1'b0));
  FDRE \res_mem_reg[316] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[316]),
        .Q(data6[28]),
        .R(1'b0));
  FDRE \res_mem_reg[317] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[317]),
        .Q(data6[29]),
        .R(1'b0));
  FDRE \res_mem_reg[318] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[318]),
        .Q(data6[30]),
        .R(1'b0));
  FDRE \res_mem_reg[319] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[319]),
        .Q(data6[31]),
        .R(1'b0));
  FDRE \res_mem_reg[31] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[31]),
        .Q(\res_mem_reg_n_0_[31] ),
        .R(1'b0));
  FDRE \res_mem_reg[320] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[320]),
        .Q(data6[32]),
        .R(1'b0));
  FDRE \res_mem_reg[321] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[321]),
        .Q(data6[33]),
        .R(1'b0));
  FDRE \res_mem_reg[322] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[322]),
        .Q(data6[34]),
        .R(1'b0));
  FDRE \res_mem_reg[323] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[323]),
        .Q(data6[35]),
        .R(1'b0));
  FDRE \res_mem_reg[324] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[324]),
        .Q(data6[36]),
        .R(1'b0));
  FDRE \res_mem_reg[325] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[325]),
        .Q(data6[37]),
        .R(1'b0));
  FDRE \res_mem_reg[326] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[326]),
        .Q(data6[38]),
        .R(1'b0));
  FDRE \res_mem_reg[327] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[327]),
        .Q(data6[39]),
        .R(1'b0));
  FDRE \res_mem_reg[328] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[328]),
        .Q(data6[40]),
        .R(1'b0));
  FDRE \res_mem_reg[329] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[329]),
        .Q(data6[41]),
        .R(1'b0));
  FDRE \res_mem_reg[32] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[32]),
        .Q(\res_mem_reg_n_0_[32] ),
        .R(1'b0));
  FDRE \res_mem_reg[330] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[330]),
        .Q(data6[42]),
        .R(1'b0));
  FDRE \res_mem_reg[331] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[331]),
        .Q(data6[43]),
        .R(1'b0));
  FDRE \res_mem_reg[332] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[332]),
        .Q(data6[44]),
        .R(1'b0));
  FDRE \res_mem_reg[333] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[333]),
        .Q(data6[45]),
        .R(1'b0));
  FDRE \res_mem_reg[334] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[334]),
        .Q(data6[46]),
        .R(1'b0));
  FDRE \res_mem_reg[335] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[335]),
        .Q(data6[47]),
        .R(1'b0));
  FDRE \res_mem_reg[336] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[336]),
        .Q(data7[0]),
        .R(1'b0));
  FDRE \res_mem_reg[337] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[337]),
        .Q(data7[1]),
        .R(1'b0));
  FDRE \res_mem_reg[338] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[338]),
        .Q(data7[2]),
        .R(1'b0));
  FDRE \res_mem_reg[339] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[339]),
        .Q(data7[3]),
        .R(1'b0));
  FDRE \res_mem_reg[33] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[33]),
        .Q(\res_mem_reg_n_0_[33] ),
        .R(1'b0));
  FDRE \res_mem_reg[340] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[340]),
        .Q(data7[4]),
        .R(1'b0));
  FDRE \res_mem_reg[341] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[341]),
        .Q(data7[5]),
        .R(1'b0));
  FDRE \res_mem_reg[342] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[342]),
        .Q(data7[6]),
        .R(1'b0));
  FDRE \res_mem_reg[343] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[343]),
        .Q(data7[7]),
        .R(1'b0));
  FDRE \res_mem_reg[344] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[344]),
        .Q(data7[8]),
        .R(1'b0));
  FDRE \res_mem_reg[345] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[345]),
        .Q(data7[9]),
        .R(1'b0));
  FDRE \res_mem_reg[346] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[346]),
        .Q(data7[10]),
        .R(1'b0));
  FDRE \res_mem_reg[347] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[347]),
        .Q(data7[11]),
        .R(1'b0));
  FDRE \res_mem_reg[348] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[348]),
        .Q(data7[12]),
        .R(1'b0));
  FDRE \res_mem_reg[349] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[349]),
        .Q(data7[13]),
        .R(1'b0));
  FDRE \res_mem_reg[34] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[34]),
        .Q(\res_mem_reg_n_0_[34] ),
        .R(1'b0));
  FDRE \res_mem_reg[350] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[350]),
        .Q(data7[14]),
        .R(1'b0));
  FDRE \res_mem_reg[351] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[351]),
        .Q(data7[15]),
        .R(1'b0));
  FDRE \res_mem_reg[352] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[352]),
        .Q(data7[16]),
        .R(1'b0));
  FDRE \res_mem_reg[353] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[353]),
        .Q(data7[17]),
        .R(1'b0));
  FDRE \res_mem_reg[354] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[354]),
        .Q(data7[18]),
        .R(1'b0));
  FDRE \res_mem_reg[355] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[355]),
        .Q(data7[19]),
        .R(1'b0));
  FDRE \res_mem_reg[356] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[356]),
        .Q(data7[20]),
        .R(1'b0));
  FDRE \res_mem_reg[357] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[357]),
        .Q(data7[21]),
        .R(1'b0));
  FDRE \res_mem_reg[358] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[358]),
        .Q(data7[22]),
        .R(1'b0));
  FDRE \res_mem_reg[359] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[359]),
        .Q(data7[23]),
        .R(1'b0));
  FDRE \res_mem_reg[35] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[35]),
        .Q(\res_mem_reg_n_0_[35] ),
        .R(1'b0));
  FDRE \res_mem_reg[360] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[360]),
        .Q(data7[24]),
        .R(1'b0));
  FDRE \res_mem_reg[361] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[361]),
        .Q(data7[25]),
        .R(1'b0));
  FDRE \res_mem_reg[362] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[362]),
        .Q(data7[26]),
        .R(1'b0));
  FDRE \res_mem_reg[363] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[363]),
        .Q(data7[27]),
        .R(1'b0));
  FDRE \res_mem_reg[364] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[364]),
        .Q(data7[28]),
        .R(1'b0));
  FDRE \res_mem_reg[365] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[365]),
        .Q(data7[29]),
        .R(1'b0));
  FDRE \res_mem_reg[366] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[366]),
        .Q(data7[30]),
        .R(1'b0));
  FDRE \res_mem_reg[367] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[367]),
        .Q(data7[31]),
        .R(1'b0));
  FDRE \res_mem_reg[368] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[368]),
        .Q(data7[32]),
        .R(1'b0));
  FDRE \res_mem_reg[369] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[369]),
        .Q(data7[33]),
        .R(1'b0));
  FDRE \res_mem_reg[36] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[36]),
        .Q(\res_mem_reg_n_0_[36] ),
        .R(1'b0));
  FDRE \res_mem_reg[370] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[370]),
        .Q(data7[34]),
        .R(1'b0));
  FDRE \res_mem_reg[371] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[371]),
        .Q(data7[35]),
        .R(1'b0));
  FDRE \res_mem_reg[372] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[372]),
        .Q(data7[36]),
        .R(1'b0));
  FDRE \res_mem_reg[373] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[373]),
        .Q(data7[37]),
        .R(1'b0));
  FDRE \res_mem_reg[374] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[374]),
        .Q(data7[38]),
        .R(1'b0));
  FDRE \res_mem_reg[375] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[375]),
        .Q(data7[39]),
        .R(1'b0));
  FDRE \res_mem_reg[376] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[376]),
        .Q(data7[40]),
        .R(1'b0));
  FDRE \res_mem_reg[377] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[377]),
        .Q(data7[41]),
        .R(1'b0));
  FDRE \res_mem_reg[378] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[378]),
        .Q(data7[42]),
        .R(1'b0));
  FDRE \res_mem_reg[379] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[379]),
        .Q(data7[43]),
        .R(1'b0));
  FDRE \res_mem_reg[37] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[37]),
        .Q(\res_mem_reg_n_0_[37] ),
        .R(1'b0));
  FDRE \res_mem_reg[380] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[380]),
        .Q(data7[44]),
        .R(1'b0));
  FDRE \res_mem_reg[381] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[381]),
        .Q(data7[45]),
        .R(1'b0));
  FDRE \res_mem_reg[382] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[382]),
        .Q(data7[46]),
        .R(1'b0));
  FDRE \res_mem_reg[383] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[383]),
        .Q(data7[47]),
        .R(1'b0));
  FDRE \res_mem_reg[38] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[38]),
        .Q(\res_mem_reg_n_0_[38] ),
        .R(1'b0));
  FDRE \res_mem_reg[39] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[39]),
        .Q(\res_mem_reg_n_0_[39] ),
        .R(1'b0));
  FDRE \res_mem_reg[3] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[3]),
        .Q(\res_mem_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \res_mem_reg[40] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[40]),
        .Q(\res_mem_reg_n_0_[40] ),
        .R(1'b0));
  FDRE \res_mem_reg[41] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[41]),
        .Q(\res_mem_reg_n_0_[41] ),
        .R(1'b0));
  FDRE \res_mem_reg[42] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[42]),
        .Q(\res_mem_reg_n_0_[42] ),
        .R(1'b0));
  FDRE \res_mem_reg[43] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[43]),
        .Q(\res_mem_reg_n_0_[43] ),
        .R(1'b0));
  FDRE \res_mem_reg[44] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[44]),
        .Q(\res_mem_reg_n_0_[44] ),
        .R(1'b0));
  FDRE \res_mem_reg[45] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[45]),
        .Q(\res_mem_reg_n_0_[45] ),
        .R(1'b0));
  FDRE \res_mem_reg[46] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[46]),
        .Q(\res_mem_reg_n_0_[46] ),
        .R(1'b0));
  FDRE \res_mem_reg[47] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[47]),
        .Q(\res_mem_reg_n_0_[47] ),
        .R(1'b0));
  FDRE \res_mem_reg[48] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[48]),
        .Q(data1[0]),
        .R(1'b0));
  FDRE \res_mem_reg[49] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[49]),
        .Q(data1[1]),
        .R(1'b0));
  FDRE \res_mem_reg[4] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[4]),
        .Q(\res_mem_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \res_mem_reg[50] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[50]),
        .Q(data1[2]),
        .R(1'b0));
  FDRE \res_mem_reg[51] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[51]),
        .Q(data1[3]),
        .R(1'b0));
  FDRE \res_mem_reg[52] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[52]),
        .Q(data1[4]),
        .R(1'b0));
  FDRE \res_mem_reg[53] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[53]),
        .Q(data1[5]),
        .R(1'b0));
  FDRE \res_mem_reg[54] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[54]),
        .Q(data1[6]),
        .R(1'b0));
  FDRE \res_mem_reg[55] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[55]),
        .Q(data1[7]),
        .R(1'b0));
  FDRE \res_mem_reg[56] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[56]),
        .Q(data1[8]),
        .R(1'b0));
  FDRE \res_mem_reg[57] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[57]),
        .Q(data1[9]),
        .R(1'b0));
  FDRE \res_mem_reg[58] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[58]),
        .Q(data1[10]),
        .R(1'b0));
  FDRE \res_mem_reg[59] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[59]),
        .Q(data1[11]),
        .R(1'b0));
  FDRE \res_mem_reg[5] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[5]),
        .Q(\res_mem_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \res_mem_reg[60] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[60]),
        .Q(data1[12]),
        .R(1'b0));
  FDRE \res_mem_reg[61] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[61]),
        .Q(data1[13]),
        .R(1'b0));
  FDRE \res_mem_reg[62] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[62]),
        .Q(data1[14]),
        .R(1'b0));
  FDRE \res_mem_reg[63] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[63]),
        .Q(data1[15]),
        .R(1'b0));
  FDRE \res_mem_reg[64] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[64]),
        .Q(data1[16]),
        .R(1'b0));
  FDRE \res_mem_reg[65] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[65]),
        .Q(data1[17]),
        .R(1'b0));
  FDRE \res_mem_reg[66] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[66]),
        .Q(data1[18]),
        .R(1'b0));
  FDRE \res_mem_reg[67] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[67]),
        .Q(data1[19]),
        .R(1'b0));
  FDRE \res_mem_reg[68] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[68]),
        .Q(data1[20]),
        .R(1'b0));
  FDRE \res_mem_reg[69] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[69]),
        .Q(data1[21]),
        .R(1'b0));
  FDRE \res_mem_reg[6] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[6]),
        .Q(\res_mem_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \res_mem_reg[70] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[70]),
        .Q(data1[22]),
        .R(1'b0));
  FDRE \res_mem_reg[71] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[71]),
        .Q(data1[23]),
        .R(1'b0));
  FDRE \res_mem_reg[72] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[72]),
        .Q(data1[24]),
        .R(1'b0));
  FDRE \res_mem_reg[73] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[73]),
        .Q(data1[25]),
        .R(1'b0));
  FDRE \res_mem_reg[74] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[74]),
        .Q(data1[26]),
        .R(1'b0));
  FDRE \res_mem_reg[75] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[75]),
        .Q(data1[27]),
        .R(1'b0));
  FDRE \res_mem_reg[76] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[76]),
        .Q(data1[28]),
        .R(1'b0));
  FDRE \res_mem_reg[77] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[77]),
        .Q(data1[29]),
        .R(1'b0));
  FDRE \res_mem_reg[78] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[78]),
        .Q(data1[30]),
        .R(1'b0));
  FDRE \res_mem_reg[79] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[79]),
        .Q(data1[31]),
        .R(1'b0));
  FDRE \res_mem_reg[7] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[7]),
        .Q(\res_mem_reg_n_0_[7] ),
        .R(1'b0));
  FDRE \res_mem_reg[80] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[80]),
        .Q(data1[32]),
        .R(1'b0));
  FDRE \res_mem_reg[81] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[81]),
        .Q(data1[33]),
        .R(1'b0));
  FDRE \res_mem_reg[82] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[82]),
        .Q(data1[34]),
        .R(1'b0));
  FDRE \res_mem_reg[83] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[83]),
        .Q(data1[35]),
        .R(1'b0));
  FDRE \res_mem_reg[84] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[84]),
        .Q(data1[36]),
        .R(1'b0));
  FDRE \res_mem_reg[85] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[85]),
        .Q(data1[37]),
        .R(1'b0));
  FDRE \res_mem_reg[86] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[86]),
        .Q(data1[38]),
        .R(1'b0));
  FDRE \res_mem_reg[87] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[87]),
        .Q(data1[39]),
        .R(1'b0));
  FDRE \res_mem_reg[88] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[88]),
        .Q(data1[40]),
        .R(1'b0));
  FDRE \res_mem_reg[89] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[89]),
        .Q(data1[41]),
        .R(1'b0));
  FDRE \res_mem_reg[8] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[8]),
        .Q(\res_mem_reg_n_0_[8] ),
        .R(1'b0));
  FDRE \res_mem_reg[90] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[90]),
        .Q(data1[42]),
        .R(1'b0));
  FDRE \res_mem_reg[91] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[91]),
        .Q(data1[43]),
        .R(1'b0));
  FDRE \res_mem_reg[92] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[92]),
        .Q(data1[44]),
        .R(1'b0));
  FDRE \res_mem_reg[93] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[93]),
        .Q(data1[45]),
        .R(1'b0));
  FDRE \res_mem_reg[94] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[94]),
        .Q(data1[46]),
        .R(1'b0));
  FDRE \res_mem_reg[95] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[95]),
        .Q(data1[47]),
        .R(1'b0));
  FDRE \res_mem_reg[96] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[96]),
        .Q(data2[0]),
        .R(1'b0));
  FDRE \res_mem_reg[97] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[97]),
        .Q(data2[1]),
        .R(1'b0));
  FDRE \res_mem_reg[98] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[98]),
        .Q(data2[2]),
        .R(1'b0));
  FDRE \res_mem_reg[99] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[99]),
        .Q(data2[3]),
        .R(1'b0));
  FDRE \res_mem_reg[9] 
       (.C(clk),
        .CE(\slv_regs_reg[0][12] ),
        .D(D[9]),
        .Q(\res_mem_reg_n_0_[9] ),
        .R(1'b0));
  FDRE start_reg
       (.C(clk),
        .CE(1'b1),
        .D(start_reg_0),
        .Q(start),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "b_ram_bank" *) 
module design_1_top_0_0_b_ram_bank
   (s_axi_ctrl_status_awready,
    p_0_in__0,
    s_axi_ctrl_status_wready,
    valid_input,
    s_axi_ctrl_status_arready,
    b_ram_out,
    s_axi_ctrl_status_bvalid,
    s_axi_ctrl_status_rvalid,
    initialized,
    \counter_reg[4]_0 ,
    \count_i_reg[6] ,
    Q,
    \counter_reg[4]_1 ,
    SR,
    dp_enable,
    \counter_reg[8] ,
    \counter_reg[8]_0 ,
    \counter_reg[8]_1 ,
    \counter_reg[8]_2 ,
    \counter_reg[8]_3 ,
    \counter_reg[8]_4 ,
    \counter_reg[8]_5 ,
    S,
    \counter_reg[1]_0 ,
    \counter_reg[1]_1 ,
    \counter_reg[1]_2 ,
    \counter_reg[1]_3 ,
    \counter_reg[1]_4 ,
    \counter_reg[1]_5 ,
    \counter_reg[1]_6 ,
    \counter_reg[1]_7 ,
    O,
    in_stream_ready_reg,
    s_axi_ctrl_status_rdata,
    \counter_reg[4]_2 ,
    \read_address_reg[2]_0 ,
    clk,
    axi_wready_reg,
    axi_arready_reg,
    \b_ram_sel_reg[7]_0 ,
    s_axi_ctrl_status_wvalid,
    s_axi_ctrl_status_awvalid,
    s_axi_ctrl_status_arvalid,
    aresetn,
    s_axis_tvalid,
    CO,
    \slv_regs_reg[0][11] ,
    \slv_regs_reg[0][11]_0 ,
    \slv_regs_reg[0][11]_1 ,
    \slv_regs_reg[0][11]_2 ,
    \slv_regs_reg[0][11]_3 ,
    \slv_regs_reg[0][11]_4 ,
    \slv_regs_reg[0][11]_5 ,
    s_axis_tready,
    dp_extend_end,
    s_axi_ctrl_status_awaddr,
    s_axi_ctrl_status_wdata,
    s_axi_ctrl_status_araddr,
    s_axi_ctrl_status_wstrb,
    \slv_regs_reg[0][11]_6 );
  output s_axi_ctrl_status_awready;
  output p_0_in__0;
  output s_axi_ctrl_status_wready;
  output valid_input;
  output s_axi_ctrl_status_arready;
  output [255:0]b_ram_out;
  output s_axi_ctrl_status_bvalid;
  output s_axi_ctrl_status_rvalid;
  output initialized;
  output \counter_reg[4]_0 ;
  output \count_i_reg[6] ;
  output [0:0]Q;
  output [0:0]\counter_reg[4]_1 ;
  output [0:0]SR;
  output dp_enable;
  output [0:0]\counter_reg[8] ;
  output [0:0]\counter_reg[8]_0 ;
  output [0:0]\counter_reg[8]_1 ;
  output [0:0]\counter_reg[8]_2 ;
  output [0:0]\counter_reg[8]_3 ;
  output [0:0]\counter_reg[8]_4 ;
  output [0:0]\counter_reg[8]_5 ;
  output [0:0]S;
  output [7:0]\counter_reg[1]_0 ;
  output [0:0]\counter_reg[1]_1 ;
  output [0:0]\counter_reg[1]_2 ;
  output [0:0]\counter_reg[1]_3 ;
  output [0:0]\counter_reg[1]_4 ;
  output [0:0]\counter_reg[1]_5 ;
  output [0:0]\counter_reg[1]_6 ;
  output [0:0]\counter_reg[1]_7 ;
  output [0:0]O;
  output in_stream_ready_reg;
  output [31:0]s_axi_ctrl_status_rdata;
  output [0:0]\counter_reg[4]_2 ;
  output [3:0]\read_address_reg[2]_0 ;
  input clk;
  input axi_wready_reg;
  input axi_arready_reg;
  input \b_ram_sel_reg[7]_0 ;
  input s_axi_ctrl_status_wvalid;
  input s_axi_ctrl_status_awvalid;
  input s_axi_ctrl_status_arvalid;
  input aresetn;
  input s_axis_tvalid;
  input [0:0]CO;
  input [0:0]\slv_regs_reg[0][11] ;
  input [0:0]\slv_regs_reg[0][11]_0 ;
  input [0:0]\slv_regs_reg[0][11]_1 ;
  input [0:0]\slv_regs_reg[0][11]_2 ;
  input [0:0]\slv_regs_reg[0][11]_3 ;
  input [0:0]\slv_regs_reg[0][11]_4 ;
  input [0:0]\slv_regs_reg[0][11]_5 ;
  input s_axis_tready;
  input dp_extend_end;
  input [3:0]s_axi_ctrl_status_awaddr;
  input [31:0]s_axi_ctrl_status_wdata;
  input [3:0]s_axi_ctrl_status_araddr;
  input [3:0]s_axi_ctrl_status_wstrb;
  input [1:0]\slv_regs_reg[0][11]_6 ;

  wire [0:0]CO;
  wire [0:0]O;
  wire [0:0]Q;
  wire [0:0]S;
  wire [0:0]SR;
  wire aresetn;
  wire axi_arready_reg;
  wire axi_wready_reg;
  wire [255:0]b_ram_out;
  wire [6:0]b_ram_sel;
  wire \b_ram_sel_reg[7]_0 ;
  wire clk;
  wire \count_i_reg[6] ;
  wire [6:0]counter;
  wire [6:4]counter2;
  wire \counter[6]_i_1__7_n_0 ;
  wire \counter[6]_i_9_n_0 ;
  wire [7:0]\counter_reg[1]_0 ;
  wire [0:0]\counter_reg[1]_1 ;
  wire [0:0]\counter_reg[1]_2 ;
  wire [0:0]\counter_reg[1]_3 ;
  wire [0:0]\counter_reg[1]_4 ;
  wire [0:0]\counter_reg[1]_5 ;
  wire [0:0]\counter_reg[1]_6 ;
  wire [0:0]\counter_reg[1]_7 ;
  wire \counter_reg[4]_0 ;
  wire [0:0]\counter_reg[4]_1 ;
  wire [0:0]\counter_reg[4]_2 ;
  wire [0:0]\counter_reg[8] ;
  wire [0:0]\counter_reg[8]_0 ;
  wire [0:0]\counter_reg[8]_1 ;
  wire [0:0]\counter_reg[8]_2 ;
  wire [0:0]\counter_reg[8]_3 ;
  wire [0:0]\counter_reg[8]_4 ;
  wire [0:0]\counter_reg[8]_5 ;
  wire dp_enable;
  wire dp_extend_end;
  wire in_stream_ready_reg;
  wire initialized;
  wire [6:0]p_0_in;
  wire p_0_in__0;
  wire p_1_in;
  wire [7:0]p_2_in;
  wire [5:4]read_address0;
  wire \read_address[6]_i_11_n_0 ;
  wire \read_address[6]_i_15_n_0 ;
  wire \read_address[6]_i_5_n_0 ;
  wire [3:0]\read_address_reg[2]_0 ;
  wire \read_address_reg_n_0_[0] ;
  wire \read_address_reg_n_0_[1] ;
  wire \read_address_reg_n_0_[2] ;
  wire \read_address_reg_n_0_[3] ;
  wire \read_address_reg_n_0_[4] ;
  wire \read_address_reg_n_0_[5] ;
  wire \read_address_reg_n_0_[6] ;
  wire register_interface_n_10;
  wire register_interface_n_11;
  wire register_interface_n_12;
  wire register_interface_n_13;
  wire register_interface_n_14;
  wire register_interface_n_15;
  wire register_interface_n_16;
  wire register_interface_n_17;
  wire register_interface_n_18;
  wire register_interface_n_19;
  wire register_interface_n_20;
  wire register_interface_n_21;
  wire register_interface_n_22;
  wire register_interface_n_23;
  wire register_interface_n_24;
  wire register_interface_n_25;
  wire register_interface_n_26;
  wire register_interface_n_27;
  wire register_interface_n_28;
  wire register_interface_n_29;
  wire register_interface_n_30;
  wire register_interface_n_31;
  wire register_interface_n_32;
  wire register_interface_n_33;
  wire register_interface_n_34;
  wire register_interface_n_35;
  wire register_interface_n_36;
  wire register_interface_n_37;
  wire register_interface_n_38;
  wire register_interface_n_39;
  wire register_interface_n_40;
  wire register_interface_n_41;
  wire register_interface_n_42;
  wire register_interface_n_43;
  wire register_interface_n_44;
  wire register_interface_n_45;
  wire register_interface_n_46;
  wire register_interface_n_47;
  wire register_interface_n_48;
  wire register_interface_n_7;
  wire register_interface_n_77;
  wire register_interface_n_79;
  wire register_interface_n_8;
  wire register_interface_n_80;
  wire register_interface_n_85;
  wire register_interface_n_86;
  wire register_interface_n_87;
  wire register_interface_n_89;
  wire register_interface_n_9;
  wire register_interface_n_90;
  wire register_interface_n_91;
  wire register_interface_n_92;
  wire [3:0]s_axi_ctrl_status_araddr;
  wire s_axi_ctrl_status_arready;
  wire s_axi_ctrl_status_arvalid;
  wire [3:0]s_axi_ctrl_status_awaddr;
  wire s_axi_ctrl_status_awready;
  wire s_axi_ctrl_status_awvalid;
  wire s_axi_ctrl_status_bvalid;
  wire [31:0]s_axi_ctrl_status_rdata;
  wire s_axi_ctrl_status_rvalid;
  wire [31:0]s_axi_ctrl_status_wdata;
  wire s_axi_ctrl_status_wready;
  wire [3:0]s_axi_ctrl_status_wstrb;
  wire s_axi_ctrl_status_wvalid;
  wire s_axis_tready;
  wire s_axis_tvalid;
  wire [0:0]\slv_regs_reg[0][11] ;
  wire [0:0]\slv_regs_reg[0][11]_0 ;
  wire [0:0]\slv_regs_reg[0][11]_1 ;
  wire [0:0]\slv_regs_reg[0][11]_2 ;
  wire [0:0]\slv_regs_reg[0][11]_3 ;
  wire [0:0]\slv_regs_reg[0][11]_4 ;
  wire [0:0]\slv_regs_reg[0][11]_5 ;
  wire [1:0]\slv_regs_reg[0][11]_6 ;
  wire state0;
  wire [7:6]state1;
  wire valid_input;

  design_1_top_0_0_block_ram \b_ram[0].DUT 
       (.DIADI({register_interface_n_17,register_interface_n_18,register_interface_n_19,register_interface_n_20,register_interface_n_21,register_interface_n_22,register_interface_n_23,register_interface_n_24,register_interface_n_25,register_interface_n_26,register_interface_n_27,register_interface_n_28,register_interface_n_29,register_interface_n_30,register_interface_n_31,register_interface_n_32}),
        .DIBDI({register_interface_n_33,register_interface_n_34,register_interface_n_35,register_interface_n_36,register_interface_n_37,register_interface_n_38,register_interface_n_39,register_interface_n_40,register_interface_n_41,register_interface_n_42,register_interface_n_43,register_interface_n_44,register_interface_n_45,register_interface_n_46,register_interface_n_47,register_interface_n_48}),
        .Q({\read_address_reg_n_0_[6] ,\read_address_reg_n_0_[5] ,\read_address_reg_n_0_[4] ,\read_address_reg_n_0_[3] ,\read_address_reg_n_0_[2] ,\read_address_reg_n_0_[1] ,\read_address_reg_n_0_[0] }),
        .WEBWE(register_interface_n_89),
        .aresetn(p_0_in__0),
        .b_ram_out(b_ram_out[31:0]),
        .clk(clk),
        .\slv_regs_reg[0][12] (register_interface_n_16));
  design_1_top_0_0_block_ram_7 \b_ram[1].DUT 
       (.DIADI({register_interface_n_17,register_interface_n_18,register_interface_n_19,register_interface_n_20,register_interface_n_21,register_interface_n_22,register_interface_n_23,register_interface_n_24,register_interface_n_25,register_interface_n_26,register_interface_n_27,register_interface_n_28,register_interface_n_29,register_interface_n_30,register_interface_n_31,register_interface_n_32}),
        .DIBDI({register_interface_n_33,register_interface_n_34,register_interface_n_35,register_interface_n_36,register_interface_n_37,register_interface_n_38,register_interface_n_39,register_interface_n_40,register_interface_n_41,register_interface_n_42,register_interface_n_43,register_interface_n_44,register_interface_n_45,register_interface_n_46,register_interface_n_47,register_interface_n_48}),
        .E(register_interface_n_15),
        .Q({\read_address_reg_n_0_[6] ,\read_address_reg_n_0_[5] ,\read_address_reg_n_0_[4] ,\read_address_reg_n_0_[3] ,\read_address_reg_n_0_[2] ,\read_address_reg_n_0_[1] ,\read_address_reg_n_0_[0] }),
        .aresetn(p_0_in__0),
        .b_ram_out(b_ram_out[63:32]),
        .clk(clk),
        .\slv_regs_reg[0][12] (register_interface_n_16));
  design_1_top_0_0_block_ram_8 \b_ram[2].DUT 
       (.DIADI({register_interface_n_17,register_interface_n_18,register_interface_n_19,register_interface_n_20,register_interface_n_21,register_interface_n_22,register_interface_n_23,register_interface_n_24,register_interface_n_25,register_interface_n_26,register_interface_n_27,register_interface_n_28,register_interface_n_29,register_interface_n_30,register_interface_n_31,register_interface_n_32}),
        .DIBDI({register_interface_n_33,register_interface_n_34,register_interface_n_35,register_interface_n_36,register_interface_n_37,register_interface_n_38,register_interface_n_39,register_interface_n_40,register_interface_n_41,register_interface_n_42,register_interface_n_43,register_interface_n_44,register_interface_n_45,register_interface_n_46,register_interface_n_47,register_interface_n_48}),
        .E(register_interface_n_14),
        .Q({\read_address_reg_n_0_[6] ,\read_address_reg_n_0_[5] ,\read_address_reg_n_0_[4] ,\read_address_reg_n_0_[3] ,\read_address_reg_n_0_[2] ,\read_address_reg_n_0_[1] ,\read_address_reg_n_0_[0] }),
        .aresetn(p_0_in__0),
        .b_ram_out(b_ram_out[95:64]),
        .clk(clk),
        .\slv_regs_reg[0][12] (register_interface_n_16));
  design_1_top_0_0_block_ram_9 \b_ram[3].DUT 
       (.DIADI({register_interface_n_17,register_interface_n_18,register_interface_n_19,register_interface_n_20,register_interface_n_21,register_interface_n_22,register_interface_n_23,register_interface_n_24,register_interface_n_25,register_interface_n_26,register_interface_n_27,register_interface_n_28,register_interface_n_29,register_interface_n_30,register_interface_n_31,register_interface_n_32}),
        .DIBDI({register_interface_n_33,register_interface_n_34,register_interface_n_35,register_interface_n_36,register_interface_n_37,register_interface_n_38,register_interface_n_39,register_interface_n_40,register_interface_n_41,register_interface_n_42,register_interface_n_43,register_interface_n_44,register_interface_n_45,register_interface_n_46,register_interface_n_47,register_interface_n_48}),
        .E(register_interface_n_13),
        .Q({\read_address_reg_n_0_[6] ,\read_address_reg_n_0_[5] ,\read_address_reg_n_0_[4] ,\read_address_reg_n_0_[3] ,\read_address_reg_n_0_[2] ,\read_address_reg_n_0_[1] ,\read_address_reg_n_0_[0] }),
        .aresetn(p_0_in__0),
        .b_ram_out(b_ram_out[127:96]),
        .clk(clk),
        .\slv_regs_reg[0][12] (register_interface_n_16));
  design_1_top_0_0_block_ram_10 \b_ram[4].DUT 
       (.DIADI({register_interface_n_17,register_interface_n_18,register_interface_n_19,register_interface_n_20,register_interface_n_21,register_interface_n_22,register_interface_n_23,register_interface_n_24,register_interface_n_25,register_interface_n_26,register_interface_n_27,register_interface_n_28,register_interface_n_29,register_interface_n_30,register_interface_n_31,register_interface_n_32}),
        .DIBDI({register_interface_n_33,register_interface_n_34,register_interface_n_35,register_interface_n_36,register_interface_n_37,register_interface_n_38,register_interface_n_39,register_interface_n_40,register_interface_n_41,register_interface_n_42,register_interface_n_43,register_interface_n_44,register_interface_n_45,register_interface_n_46,register_interface_n_47,register_interface_n_48}),
        .E(register_interface_n_12),
        .Q({\read_address_reg_n_0_[6] ,\read_address_reg_n_0_[5] ,\read_address_reg_n_0_[4] ,\read_address_reg_n_0_[3] ,\read_address_reg_n_0_[2] ,\read_address_reg_n_0_[1] ,\read_address_reg_n_0_[0] }),
        .aresetn(p_0_in__0),
        .b_ram_out(b_ram_out[159:128]),
        .clk(clk),
        .\slv_regs_reg[0][12] (register_interface_n_16));
  design_1_top_0_0_block_ram_11 \b_ram[5].DUT 
       (.DIADI({register_interface_n_17,register_interface_n_18,register_interface_n_19,register_interface_n_20,register_interface_n_21,register_interface_n_22,register_interface_n_23,register_interface_n_24,register_interface_n_25,register_interface_n_26,register_interface_n_27,register_interface_n_28,register_interface_n_29,register_interface_n_30,register_interface_n_31,register_interface_n_32}),
        .DIBDI({register_interface_n_33,register_interface_n_34,register_interface_n_35,register_interface_n_36,register_interface_n_37,register_interface_n_38,register_interface_n_39,register_interface_n_40,register_interface_n_41,register_interface_n_42,register_interface_n_43,register_interface_n_44,register_interface_n_45,register_interface_n_46,register_interface_n_47,register_interface_n_48}),
        .E(register_interface_n_11),
        .Q({\read_address_reg_n_0_[6] ,\read_address_reg_n_0_[5] ,\read_address_reg_n_0_[4] ,\read_address_reg_n_0_[3] ,\read_address_reg_n_0_[2] ,\read_address_reg_n_0_[1] ,\read_address_reg_n_0_[0] }),
        .aresetn(p_0_in__0),
        .b_ram_out(b_ram_out[191:160]),
        .clk(clk),
        .\slv_regs_reg[0][12] (register_interface_n_16));
  design_1_top_0_0_block_ram_12 \b_ram[6].DUT 
       (.DIADI({register_interface_n_17,register_interface_n_18,register_interface_n_19,register_interface_n_20,register_interface_n_21,register_interface_n_22,register_interface_n_23,register_interface_n_24,register_interface_n_25,register_interface_n_26,register_interface_n_27,register_interface_n_28,register_interface_n_29,register_interface_n_30,register_interface_n_31,register_interface_n_32}),
        .DIBDI({register_interface_n_33,register_interface_n_34,register_interface_n_35,register_interface_n_36,register_interface_n_37,register_interface_n_38,register_interface_n_39,register_interface_n_40,register_interface_n_41,register_interface_n_42,register_interface_n_43,register_interface_n_44,register_interface_n_45,register_interface_n_46,register_interface_n_47,register_interface_n_48}),
        .E(register_interface_n_10),
        .Q({\read_address_reg_n_0_[6] ,\read_address_reg_n_0_[5] ,\read_address_reg_n_0_[4] ,\read_address_reg_n_0_[3] ,\read_address_reg_n_0_[2] ,\read_address_reg_n_0_[1] ,\read_address_reg_n_0_[0] }),
        .aresetn(p_0_in__0),
        .b_ram_out(b_ram_out[223:192]),
        .clk(clk),
        .\slv_regs_reg[0][12] (register_interface_n_16));
  design_1_top_0_0_block_ram_13 \b_ram[7].DUT 
       (.DIADI({register_interface_n_17,register_interface_n_18,register_interface_n_19,register_interface_n_20,register_interface_n_21,register_interface_n_22,register_interface_n_23,register_interface_n_24,register_interface_n_25,register_interface_n_26,register_interface_n_27,register_interface_n_28,register_interface_n_29,register_interface_n_30,register_interface_n_31,register_interface_n_32}),
        .DIBDI({register_interface_n_33,register_interface_n_34,register_interface_n_35,register_interface_n_36,register_interface_n_37,register_interface_n_38,register_interface_n_39,register_interface_n_40,register_interface_n_41,register_interface_n_42,register_interface_n_43,register_interface_n_44,register_interface_n_45,register_interface_n_46,register_interface_n_47,register_interface_n_48}),
        .Q({\read_address_reg_n_0_[6] ,\read_address_reg_n_0_[5] ,\read_address_reg_n_0_[4] ,\read_address_reg_n_0_[3] ,\read_address_reg_n_0_[2] ,\read_address_reg_n_0_[1] ,\read_address_reg_n_0_[0] }),
        .aresetn(p_0_in__0),
        .b_ram_out(b_ram_out[255:224]),
        .clk(clk),
        .\slv_regs_reg[0][12] (register_interface_n_16),
        .\state_reg[1] (register_interface_n_9));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \b_ram_sel[0]_i_1 
       (.I0(\counter_reg[4]_0 ),
        .O(p_2_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \b_ram_sel[1]_i_1 
       (.I0(\counter_reg[4]_0 ),
        .I1(b_ram_sel[0]),
        .O(p_2_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \b_ram_sel[2]_i_1 
       (.I0(\counter_reg[4]_0 ),
        .I1(b_ram_sel[1]),
        .O(p_2_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \b_ram_sel[3]_i_1 
       (.I0(\counter_reg[4]_0 ),
        .I1(b_ram_sel[2]),
        .O(p_2_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \b_ram_sel[4]_i_1 
       (.I0(\counter_reg[4]_0 ),
        .I1(b_ram_sel[3]),
        .O(p_2_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \b_ram_sel[5]_i_1 
       (.I0(\counter_reg[4]_0 ),
        .I1(b_ram_sel[4]),
        .O(p_2_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \b_ram_sel[6]_i_1 
       (.I0(\counter_reg[4]_0 ),
        .I1(b_ram_sel[5]),
        .O(p_2_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \b_ram_sel[7]_i_2 
       (.I0(\counter_reg[4]_0 ),
        .I1(b_ram_sel[6]),
        .O(p_2_in[7]));
  FDCE \b_ram_sel_reg[0] 
       (.C(clk),
        .CE(p_1_in),
        .CLR(p_0_in__0),
        .D(p_2_in[0]),
        .Q(b_ram_sel[0]));
  FDCE \b_ram_sel_reg[1] 
       (.C(clk),
        .CE(p_1_in),
        .CLR(p_0_in__0),
        .D(p_2_in[1]),
        .Q(b_ram_sel[1]));
  FDCE \b_ram_sel_reg[2] 
       (.C(clk),
        .CE(p_1_in),
        .CLR(p_0_in__0),
        .D(p_2_in[2]),
        .Q(b_ram_sel[2]));
  FDCE \b_ram_sel_reg[3] 
       (.C(clk),
        .CE(p_1_in),
        .CLR(p_0_in__0),
        .D(p_2_in[3]),
        .Q(b_ram_sel[3]));
  FDCE \b_ram_sel_reg[4] 
       (.C(clk),
        .CE(p_1_in),
        .CLR(p_0_in__0),
        .D(p_2_in[4]),
        .Q(b_ram_sel[4]));
  FDCE \b_ram_sel_reg[5] 
       (.C(clk),
        .CE(p_1_in),
        .CLR(p_0_in__0),
        .D(p_2_in[5]),
        .Q(b_ram_sel[5]));
  FDCE \b_ram_sel_reg[6] 
       (.C(clk),
        .CE(p_1_in),
        .CLR(p_0_in__0),
        .D(p_2_in[6]),
        .Q(b_ram_sel[6]));
  FDCE \b_ram_sel_reg[7] 
       (.C(clk),
        .CE(p_1_in),
        .CLR(p_0_in__0),
        .D(p_2_in[7]),
        .Q(\counter_reg[4]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \counter[4]_i_2 
       (.I0(counter[2]),
        .I1(counter[0]),
        .I2(counter[1]),
        .I3(counter[3]),
        .I4(counter[4]),
        .O(counter2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \counter[5]_i_2 
       (.I0(counter[3]),
        .I1(counter[1]),
        .I2(counter[0]),
        .I3(counter[2]),
        .I4(counter[4]),
        .I5(counter[5]),
        .O(counter2[5]));
  LUT3 #(
    .INIT(8'h04)) 
    \counter[6]_i_1__7 
       (.I0(\count_i_reg[6] ),
        .I1(aresetn),
        .I2(\counter_reg[4]_0 ),
        .O(\counter[6]_i_1__7_n_0 ));
  LUT3 #(
    .INIT(8'h78)) 
    \counter[6]_i_5 
       (.I0(\counter[6]_i_9_n_0 ),
        .I1(counter[5]),
        .I2(counter[6]),
        .O(counter2[6]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \counter[6]_i_9 
       (.I0(counter[4]),
        .I1(counter[2]),
        .I2(counter[0]),
        .I3(counter[1]),
        .I4(counter[3]),
        .O(\counter[6]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(register_interface_n_77),
        .Q(counter[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[1] 
       (.C(clk),
        .CE(register_interface_n_8),
        .D(register_interface_n_85),
        .Q(counter[1]),
        .R(\counter[6]_i_1__7_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[2] 
       (.C(clk),
        .CE(register_interface_n_8),
        .D(register_interface_n_86),
        .Q(counter[2]),
        .R(\counter[6]_i_1__7_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[3] 
       (.C(clk),
        .CE(register_interface_n_8),
        .D(register_interface_n_87),
        .Q(counter[3]),
        .R(\counter[6]_i_1__7_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[4] 
       (.C(clk),
        .CE(register_interface_n_8),
        .D(register_interface_n_92),
        .Q(counter[4]),
        .R(\counter[6]_i_1__7_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[5] 
       (.C(clk),
        .CE(register_interface_n_8),
        .D(register_interface_n_91),
        .Q(counter[5]),
        .R(\counter[6]_i_1__7_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[6] 
       (.C(clk),
        .CE(register_interface_n_8),
        .D(register_interface_n_90),
        .Q(counter[6]),
        .R(\counter[6]_i_1__7_n_0 ));
  FDCE initialized_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(p_0_in__0),
        .D(\b_ram_sel_reg[7]_0 ),
        .Q(initialized));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'h07)) 
    \read_address[0]_i_1 
       (.I0(state0),
        .I1(\count_i_reg[6] ),
        .I2(\read_address_reg_n_0_[0] ),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT4 #(
    .INIT(16'h0770)) 
    \read_address[1]_i_1 
       (.I0(state0),
        .I1(\count_i_reg[6] ),
        .I2(\read_address_reg_n_0_[0] ),
        .I3(\read_address_reg_n_0_[1] ),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT5 #(
    .INIT(32'h07777000)) 
    \read_address[2]_i_1 
       (.I0(state0),
        .I1(\count_i_reg[6] ),
        .I2(\read_address_reg_n_0_[0] ),
        .I3(\read_address_reg_n_0_[1] ),
        .I4(\read_address_reg_n_0_[2] ),
        .O(p_0_in[2]));
  LUT6 #(
    .INIT(64'h0777777770000000)) 
    \read_address[3]_i_1 
       (.I0(state0),
        .I1(\count_i_reg[6] ),
        .I2(\read_address_reg_n_0_[1] ),
        .I3(\read_address_reg_n_0_[0] ),
        .I4(\read_address_reg_n_0_[2] ),
        .I5(\read_address_reg_n_0_[3] ),
        .O(p_0_in[3]));
  LUT3 #(
    .INIT(8'h70)) 
    \read_address[4]_i_1 
       (.I0(state0),
        .I1(\count_i_reg[6] ),
        .I2(read_address0[4]),
        .O(p_0_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \read_address[4]_i_2 
       (.I0(\read_address_reg_n_0_[2] ),
        .I1(\read_address_reg_n_0_[0] ),
        .I2(\read_address_reg_n_0_[1] ),
        .I3(\read_address_reg_n_0_[3] ),
        .I4(\read_address_reg_n_0_[4] ),
        .O(read_address0[4]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'h70)) 
    \read_address[5]_i_1 
       (.I0(state0),
        .I1(\count_i_reg[6] ),
        .I2(read_address0[5]),
        .O(p_0_in[5]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \read_address[5]_i_2 
       (.I0(\read_address_reg_n_0_[3] ),
        .I1(\read_address_reg_n_0_[1] ),
        .I2(\read_address_reg_n_0_[0] ),
        .I3(\read_address_reg_n_0_[2] ),
        .I4(\read_address_reg_n_0_[4] ),
        .I5(\read_address_reg_n_0_[5] ),
        .O(read_address0[5]));
  LUT3 #(
    .INIT(8'h04)) 
    \read_address[6]_i_11 
       (.I0(state1[6]),
        .I1(\read_address_reg_n_0_[6] ),
        .I2(state1[7]),
        .O(\read_address[6]_i_11_n_0 ));
  LUT3 #(
    .INIT(8'h09)) 
    \read_address[6]_i_15 
       (.I0(\read_address_reg_n_0_[6] ),
        .I1(state1[6]),
        .I2(state1[7]),
        .O(\read_address[6]_i_15_n_0 ));
  LUT5 #(
    .INIT(32'h07777000)) 
    \read_address[6]_i_2 
       (.I0(state0),
        .I1(\count_i_reg[6] ),
        .I2(\read_address[6]_i_5_n_0 ),
        .I3(\read_address_reg_n_0_[5] ),
        .I4(\read_address_reg_n_0_[6] ),
        .O(p_0_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \read_address[6]_i_5 
       (.I0(\read_address_reg_n_0_[4] ),
        .I1(\read_address_reg_n_0_[2] ),
        .I2(\read_address_reg_n_0_[0] ),
        .I3(\read_address_reg_n_0_[1] ),
        .I4(\read_address_reg_n_0_[3] ),
        .O(\read_address[6]_i_5_n_0 ));
  FDRE \read_address_reg[0] 
       (.C(clk),
        .CE(register_interface_n_7),
        .D(p_0_in[0]),
        .Q(\read_address_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \read_address_reg[1] 
       (.C(clk),
        .CE(register_interface_n_7),
        .D(p_0_in[1]),
        .Q(\read_address_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \read_address_reg[2] 
       (.C(clk),
        .CE(register_interface_n_7),
        .D(p_0_in[2]),
        .Q(\read_address_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \read_address_reg[3] 
       (.C(clk),
        .CE(register_interface_n_7),
        .D(p_0_in[3]),
        .Q(\read_address_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \read_address_reg[4] 
       (.C(clk),
        .CE(register_interface_n_7),
        .D(p_0_in[4]),
        .Q(\read_address_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \read_address_reg[5] 
       (.C(clk),
        .CE(register_interface_n_7),
        .D(p_0_in[5]),
        .Q(\read_address_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \read_address_reg[6] 
       (.C(clk),
        .CE(register_interface_n_7),
        .D(p_0_in[6]),
        .Q(\read_address_reg_n_0_[6] ),
        .R(1'b0));
  design_1_top_0_0_register_interface register_interface
       (.CO(CO),
        .DI(\read_address[6]_i_11_n_0 ),
        .DIADI({register_interface_n_17,register_interface_n_18,register_interface_n_19,register_interface_n_20,register_interface_n_21,register_interface_n_22,register_interface_n_23,register_interface_n_24,register_interface_n_25,register_interface_n_26,register_interface_n_27,register_interface_n_28,register_interface_n_29,register_interface_n_30,register_interface_n_31,register_interface_n_32}),
        .DIBDI({register_interface_n_33,register_interface_n_34,register_interface_n_35,register_interface_n_36,register_interface_n_37,register_interface_n_38,register_interface_n_39,register_interface_n_40,register_interface_n_41,register_interface_n_42,register_interface_n_43,register_interface_n_44,register_interface_n_45,register_interface_n_46,register_interface_n_47,register_interface_n_48}),
        .E(register_interface_n_7),
        .O(O),
        .Q({\counter_reg[4]_1 ,b_ram_sel}),
        .S(S),
        .SR(SR),
        .WEBWE(register_interface_n_89),
        .\add_r_reg[47] (dp_enable),
        .aresetn(aresetn),
        .axi_arready_reg_0(axi_arready_reg),
        .\axi_rdata_reg[0]_0 (p_0_in__0),
        .\axi_rdata_reg[0]_1 (Q),
        .axi_wready_reg_0(axi_wready_reg),
        .b_ram_data_reg(register_interface_n_10),
        .b_ram_data_reg_0(register_interface_n_11),
        .b_ram_data_reg_1(register_interface_n_12),
        .b_ram_data_reg_2(register_interface_n_13),
        .b_ram_data_reg_3(register_interface_n_14),
        .b_ram_data_reg_4(register_interface_n_15),
        .b_ram_data_reg_5(register_interface_n_16),
        .\b_ram_sel_reg[0] (p_1_in),
        .clk(clk),
        .\count_i_reg[6] (register_interface_n_9),
        .counter(counter[3:0]),
        .counter2(counter2),
        .\counter_reg[0] (register_interface_n_77),
        .\counter_reg[1] (\counter_reg[1]_1 ),
        .\counter_reg[1]_0 (\counter_reg[1]_2 ),
        .\counter_reg[1]_1 (\counter_reg[1]_3 ),
        .\counter_reg[1]_2 (\counter_reg[1]_4 ),
        .\counter_reg[1]_3 (\counter_reg[1]_5 ),
        .\counter_reg[1]_4 (\counter_reg[1]_6 ),
        .\counter_reg[1]_5 (\counter_reg[1]_7 ),
        .\counter_reg[1]_6 (\counter_reg[1]_0 ),
        .\counter_reg[1]_7 (register_interface_n_85),
        .\counter_reg[2] (register_interface_n_86),
        .\counter_reg[3] (register_interface_n_87),
        .\counter_reg[4] (valid_input),
        .\counter_reg[4]_0 (\counter_reg[4]_2 ),
        .\counter_reg[4]_1 (register_interface_n_92),
        .\counter_reg[5] (register_interface_n_91),
        .\counter_reg[6] (register_interface_n_8),
        .\counter_reg[6]_0 (register_interface_n_90),
        .\counter_reg[8] (\counter_reg[8] ),
        .\counter_reg[8]_0 (\counter_reg[8]_0 ),
        .\counter_reg[8]_1 (\counter_reg[8]_1 ),
        .\counter_reg[8]_2 (\counter_reg[8]_2 ),
        .\counter_reg[8]_3 (\counter_reg[8]_3 ),
        .\counter_reg[8]_4 (\counter_reg[8]_4 ),
        .\counter_reg[8]_5 (\counter_reg[8]_5 ),
        .dp_extend_end(dp_extend_end),
        .in_stream_ready_reg(in_stream_ready_reg),
        .initialized_reg(initialized),
        .\read_address_reg[2] ({\read_address_reg[2]_0 [0],state1}),
        .\read_address_reg[2]_0 (state0),
        .\read_address_reg[2]_1 (\read_address_reg[2]_0 [3:1]),
        .\read_address_reg[5] ({\read_address_reg_n_0_[5] ,\read_address_reg_n_0_[4] ,\read_address_reg_n_0_[3] ,\read_address_reg_n_0_[2] ,\read_address_reg_n_0_[1] ,\read_address_reg_n_0_[0] }),
        .\read_address_reg[6] (\read_address[6]_i_15_n_0 ),
        .s_axi_ctrl_status_araddr(s_axi_ctrl_status_araddr),
        .s_axi_ctrl_status_arready(s_axi_ctrl_status_arready),
        .s_axi_ctrl_status_arvalid(s_axi_ctrl_status_arvalid),
        .s_axi_ctrl_status_awaddr(s_axi_ctrl_status_awaddr),
        .s_axi_ctrl_status_awready(s_axi_ctrl_status_awready),
        .s_axi_ctrl_status_awvalid(s_axi_ctrl_status_awvalid),
        .s_axi_ctrl_status_bvalid(s_axi_ctrl_status_bvalid),
        .s_axi_ctrl_status_rdata(s_axi_ctrl_status_rdata),
        .s_axi_ctrl_status_rvalid(s_axi_ctrl_status_rvalid),
        .s_axi_ctrl_status_wdata(s_axi_ctrl_status_wdata),
        .s_axi_ctrl_status_wready(s_axi_ctrl_status_wready),
        .s_axi_ctrl_status_wstrb(s_axi_ctrl_status_wstrb),
        .s_axi_ctrl_status_wvalid(s_axi_ctrl_status_wvalid),
        .s_axis_tready(s_axis_tready),
        .s_axis_tvalid(s_axis_tvalid),
        .\slv_regs_reg[0][11]_0 (\slv_regs_reg[0][11] ),
        .\slv_regs_reg[0][11]_1 (\slv_regs_reg[0][11]_0 ),
        .\slv_regs_reg[0][11]_2 (\slv_regs_reg[0][11]_1 ),
        .\slv_regs_reg[0][11]_3 (\slv_regs_reg[0][11]_2 ),
        .\slv_regs_reg[0][11]_4 (\slv_regs_reg[0][11]_3 ),
        .\slv_regs_reg[0][11]_5 (\slv_regs_reg[0][11]_4 ),
        .\slv_regs_reg[0][11]_6 (\slv_regs_reg[0][11]_5 ),
        .\slv_regs_reg[0][11]_7 (\slv_regs_reg[0][11]_6 ),
        .\state_reg[0] (register_interface_n_80),
        .\state_reg[0]_0 (\counter_reg[4]_0 ),
        .\state_reg[1] (register_interface_n_79),
        .\state_reg[1]_0 (\count_i_reg[6] ));
  FDCE \state_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .CLR(p_0_in__0),
        .D(register_interface_n_80),
        .Q(\counter_reg[4]_0 ));
  FDCE \state_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .CLR(p_0_in__0),
        .D(register_interface_n_79),
        .Q(\count_i_reg[6] ));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_top_0_0_block_ram
   (b_ram_out,
    clk,
    \slv_regs_reg[0][12] ,
    aresetn,
    Q,
    DIADI,
    DIBDI,
    WEBWE);
  output [31:0]b_ram_out;
  input clk;
  input \slv_regs_reg[0][12] ;
  input aresetn;
  input [6:0]Q;
  input [15:0]DIADI;
  input [15:0]DIBDI;
  input [0:0]WEBWE;

  wire [15:0]DIADI;
  wire [15:0]DIBDI;
  wire [6:0]Q;
  wire [0:0]WEBWE;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \count_i[6]_i_2_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [6:0]p_0_in__0_0;
  wire \slv_regs_reg[0][12] ;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,Q,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(DIADI),
        .DIBDI(DIBDI),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(b_ram_out[15:0]),
        .DOBDO(b_ram_out[31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(\slv_regs_reg[0][12] ),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(aresetn),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({WEBWE,WEBWE,WEBWE,WEBWE}));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in__0_0[0]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in__0_0[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[2]_i_1 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[2]),
        .O(p_0_in__0_0[2]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in__0_0[3]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \count_i[4]_i_1 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .I3(count_i_reg__0[3]),
        .I4(count_i_reg__0[4]),
        .O(p_0_in__0_0[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \count_i[5]_i_1 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[4]),
        .I5(count_i_reg__0[5]),
        .O(p_0_in__0_0[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[6]_i_1 
       (.I0(\count_i[6]_i_2_n_0 ),
        .I1(count_i_reg__0[5]),
        .I2(count_i_reg__0[6]),
        .O(p_0_in__0_0[6]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \count_i[6]_i_2 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .I4(count_i_reg__0[3]),
        .O(\count_i[6]_i_2_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__0_0[0]),
        .Q(count_i_reg__0[0]),
        .R(aresetn));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__0_0[1]),
        .Q(count_i_reg__0[1]),
        .R(aresetn));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__0_0[2]),
        .Q(count_i_reg__0[2]),
        .R(aresetn));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__0_0[3]),
        .Q(count_i_reg__0[3]),
        .R(aresetn));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__0_0[4]),
        .Q(count_i_reg__0[4]),
        .R(aresetn));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__0_0[5]),
        .Q(count_i_reg__0[5]),
        .R(aresetn));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__0_0[6]),
        .Q(count_i_reg__0[6]),
        .R(aresetn));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_top_0_0_block_ram_10
   (b_ram_out,
    clk,
    \slv_regs_reg[0][12] ,
    aresetn,
    Q,
    DIADI,
    DIBDI,
    E);
  output [31:0]b_ram_out;
  input clk;
  input \slv_regs_reg[0][12] ;
  input aresetn;
  input [6:0]Q;
  input [15:0]DIADI;
  input [15:0]DIBDI;
  input [0:0]E;

  wire [15:0]DIADI;
  wire [15:0]DIBDI;
  wire [0:0]E;
  wire [6:0]Q;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \count_i[6]_i_2__3_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [6:0]p_0_in__4;
  wire \slv_regs_reg[0][12] ;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,Q,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(DIADI),
        .DIBDI(DIBDI),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(b_ram_out[15:0]),
        .DOBDO(b_ram_out[31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(\slv_regs_reg[0][12] ),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(aresetn),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({E,E,E,E}));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1__3 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in__4[0]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1__3 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in__4[1]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[2]_i_1__3 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[2]),
        .O(p_0_in__4[2]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1__3 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in__4[3]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \count_i[4]_i_1__3 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .I3(count_i_reg__0[3]),
        .I4(count_i_reg__0[4]),
        .O(p_0_in__4[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \count_i[5]_i_1__3 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[4]),
        .I5(count_i_reg__0[5]),
        .O(p_0_in__4[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[6]_i_1__3 
       (.I0(\count_i[6]_i_2__3_n_0 ),
        .I1(count_i_reg__0[5]),
        .I2(count_i_reg__0[6]),
        .O(p_0_in__4[6]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \count_i[6]_i_2__3 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .I4(count_i_reg__0[3]),
        .O(\count_i[6]_i_2__3_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__4[0]),
        .Q(count_i_reg__0[0]),
        .R(aresetn));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__4[1]),
        .Q(count_i_reg__0[1]),
        .R(aresetn));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__4[2]),
        .Q(count_i_reg__0[2]),
        .R(aresetn));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__4[3]),
        .Q(count_i_reg__0[3]),
        .R(aresetn));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__4[4]),
        .Q(count_i_reg__0[4]),
        .R(aresetn));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__4[5]),
        .Q(count_i_reg__0[5]),
        .R(aresetn));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__4[6]),
        .Q(count_i_reg__0[6]),
        .R(aresetn));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_top_0_0_block_ram_11
   (b_ram_out,
    clk,
    \slv_regs_reg[0][12] ,
    aresetn,
    Q,
    DIADI,
    DIBDI,
    E);
  output [31:0]b_ram_out;
  input clk;
  input \slv_regs_reg[0][12] ;
  input aresetn;
  input [6:0]Q;
  input [15:0]DIADI;
  input [15:0]DIBDI;
  input [0:0]E;

  wire [15:0]DIADI;
  wire [15:0]DIBDI;
  wire [0:0]E;
  wire [6:0]Q;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \count_i[6]_i_2__4_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [6:0]p_0_in__5;
  wire \slv_regs_reg[0][12] ;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,Q,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(DIADI),
        .DIBDI(DIBDI),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(b_ram_out[15:0]),
        .DOBDO(b_ram_out[31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(\slv_regs_reg[0][12] ),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(aresetn),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({E,E,E,E}));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1__4 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in__5[0]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1__4 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in__5[1]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[2]_i_1__4 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[2]),
        .O(p_0_in__5[2]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1__4 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in__5[3]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \count_i[4]_i_1__4 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .I3(count_i_reg__0[3]),
        .I4(count_i_reg__0[4]),
        .O(p_0_in__5[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \count_i[5]_i_1__4 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[4]),
        .I5(count_i_reg__0[5]),
        .O(p_0_in__5[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[6]_i_1__4 
       (.I0(\count_i[6]_i_2__4_n_0 ),
        .I1(count_i_reg__0[5]),
        .I2(count_i_reg__0[6]),
        .O(p_0_in__5[6]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \count_i[6]_i_2__4 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .I4(count_i_reg__0[3]),
        .O(\count_i[6]_i_2__4_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__5[0]),
        .Q(count_i_reg__0[0]),
        .R(aresetn));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__5[1]),
        .Q(count_i_reg__0[1]),
        .R(aresetn));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__5[2]),
        .Q(count_i_reg__0[2]),
        .R(aresetn));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__5[3]),
        .Q(count_i_reg__0[3]),
        .R(aresetn));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__5[4]),
        .Q(count_i_reg__0[4]),
        .R(aresetn));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__5[5]),
        .Q(count_i_reg__0[5]),
        .R(aresetn));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__5[6]),
        .Q(count_i_reg__0[6]),
        .R(aresetn));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_top_0_0_block_ram_12
   (b_ram_out,
    clk,
    \slv_regs_reg[0][12] ,
    aresetn,
    Q,
    DIADI,
    DIBDI,
    E);
  output [31:0]b_ram_out;
  input clk;
  input \slv_regs_reg[0][12] ;
  input aresetn;
  input [6:0]Q;
  input [15:0]DIADI;
  input [15:0]DIBDI;
  input [0:0]E;

  wire [15:0]DIADI;
  wire [15:0]DIBDI;
  wire [0:0]E;
  wire [6:0]Q;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \count_i[6]_i_2__5_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [6:0]p_0_in__6;
  wire \slv_regs_reg[0][12] ;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,Q,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(DIADI),
        .DIBDI(DIBDI),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(b_ram_out[15:0]),
        .DOBDO(b_ram_out[31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(\slv_regs_reg[0][12] ),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(aresetn),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({E,E,E,E}));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1__5 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in__6[0]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1__5 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in__6[1]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[2]_i_1__5 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[2]),
        .O(p_0_in__6[2]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1__5 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in__6[3]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \count_i[4]_i_1__5 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .I3(count_i_reg__0[3]),
        .I4(count_i_reg__0[4]),
        .O(p_0_in__6[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \count_i[5]_i_1__5 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[4]),
        .I5(count_i_reg__0[5]),
        .O(p_0_in__6[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[6]_i_1__5 
       (.I0(\count_i[6]_i_2__5_n_0 ),
        .I1(count_i_reg__0[5]),
        .I2(count_i_reg__0[6]),
        .O(p_0_in__6[6]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \count_i[6]_i_2__5 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .I4(count_i_reg__0[3]),
        .O(\count_i[6]_i_2__5_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__6[0]),
        .Q(count_i_reg__0[0]),
        .R(aresetn));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__6[1]),
        .Q(count_i_reg__0[1]),
        .R(aresetn));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__6[2]),
        .Q(count_i_reg__0[2]),
        .R(aresetn));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__6[3]),
        .Q(count_i_reg__0[3]),
        .R(aresetn));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__6[4]),
        .Q(count_i_reg__0[4]),
        .R(aresetn));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__6[5]),
        .Q(count_i_reg__0[5]),
        .R(aresetn));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__6[6]),
        .Q(count_i_reg__0[6]),
        .R(aresetn));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_top_0_0_block_ram_13
   (b_ram_out,
    clk,
    \slv_regs_reg[0][12] ,
    aresetn,
    Q,
    DIADI,
    DIBDI,
    \state_reg[1] );
  output [31:0]b_ram_out;
  input clk;
  input \slv_regs_reg[0][12] ;
  input aresetn;
  input [6:0]Q;
  input [15:0]DIADI;
  input [15:0]DIBDI;
  input \state_reg[1] ;

  wire [15:0]DIADI;
  wire [15:0]DIBDI;
  wire [6:0]Q;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \count_i[6]_i_2__6_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [6:0]p_0_in__7;
  wire \slv_regs_reg[0][12] ;
  wire \state_reg[1] ;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,Q,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(DIADI),
        .DIBDI(DIBDI),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(b_ram_out[15:0]),
        .DOBDO(b_ram_out[31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(\slv_regs_reg[0][12] ),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(aresetn),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({\state_reg[1] ,\state_reg[1] ,\state_reg[1] ,\state_reg[1] }));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1__6 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in__7[0]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1__6 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in__7[1]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[2]_i_1__6 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[2]),
        .O(p_0_in__7[2]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1__6 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in__7[3]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \count_i[4]_i_1__6 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .I3(count_i_reg__0[3]),
        .I4(count_i_reg__0[4]),
        .O(p_0_in__7[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \count_i[5]_i_1__6 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[4]),
        .I5(count_i_reg__0[5]),
        .O(p_0_in__7[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[6]_i_1__6 
       (.I0(\count_i[6]_i_2__6_n_0 ),
        .I1(count_i_reg__0[5]),
        .I2(count_i_reg__0[6]),
        .O(p_0_in__7[6]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \count_i[6]_i_2__6 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .I4(count_i_reg__0[3]),
        .O(\count_i[6]_i_2__6_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(\state_reg[1] ),
        .D(p_0_in__7[0]),
        .Q(count_i_reg__0[0]),
        .R(aresetn));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(\state_reg[1] ),
        .D(p_0_in__7[1]),
        .Q(count_i_reg__0[1]),
        .R(aresetn));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(\state_reg[1] ),
        .D(p_0_in__7[2]),
        .Q(count_i_reg__0[2]),
        .R(aresetn));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(\state_reg[1] ),
        .D(p_0_in__7[3]),
        .Q(count_i_reg__0[3]),
        .R(aresetn));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(\state_reg[1] ),
        .D(p_0_in__7[4]),
        .Q(count_i_reg__0[4]),
        .R(aresetn));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(\state_reg[1] ),
        .D(p_0_in__7[5]),
        .Q(count_i_reg__0[5]),
        .R(aresetn));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(\state_reg[1] ),
        .D(p_0_in__7[6]),
        .Q(count_i_reg__0[6]),
        .R(aresetn));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_top_0_0_block_ram_7
   (b_ram_out,
    clk,
    \slv_regs_reg[0][12] ,
    aresetn,
    Q,
    DIADI,
    DIBDI,
    E);
  output [31:0]b_ram_out;
  input clk;
  input \slv_regs_reg[0][12] ;
  input aresetn;
  input [6:0]Q;
  input [15:0]DIADI;
  input [15:0]DIBDI;
  input [0:0]E;

  wire [15:0]DIADI;
  wire [15:0]DIBDI;
  wire [0:0]E;
  wire [6:0]Q;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \count_i[6]_i_2__0_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [6:0]p_0_in__1;
  wire \slv_regs_reg[0][12] ;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,Q,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(DIADI),
        .DIBDI(DIBDI),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(b_ram_out[15:0]),
        .DOBDO(b_ram_out[31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(\slv_regs_reg[0][12] ),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(aresetn),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({E,E,E,E}));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1__0 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1__0 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[2]_i_1__0 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[2]),
        .O(p_0_in__1[2]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1__0 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in__1[3]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \count_i[4]_i_1__0 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .I3(count_i_reg__0[3]),
        .I4(count_i_reg__0[4]),
        .O(p_0_in__1[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \count_i[5]_i_1__0 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[4]),
        .I5(count_i_reg__0[5]),
        .O(p_0_in__1[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[6]_i_1__0 
       (.I0(\count_i[6]_i_2__0_n_0 ),
        .I1(count_i_reg__0[5]),
        .I2(count_i_reg__0[6]),
        .O(p_0_in__1[6]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \count_i[6]_i_2__0 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .I4(count_i_reg__0[3]),
        .O(\count_i[6]_i_2__0_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__1[0]),
        .Q(count_i_reg__0[0]),
        .R(aresetn));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__1[1]),
        .Q(count_i_reg__0[1]),
        .R(aresetn));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__1[2]),
        .Q(count_i_reg__0[2]),
        .R(aresetn));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__1[3]),
        .Q(count_i_reg__0[3]),
        .R(aresetn));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__1[4]),
        .Q(count_i_reg__0[4]),
        .R(aresetn));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__1[5]),
        .Q(count_i_reg__0[5]),
        .R(aresetn));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__1[6]),
        .Q(count_i_reg__0[6]),
        .R(aresetn));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_top_0_0_block_ram_8
   (b_ram_out,
    clk,
    \slv_regs_reg[0][12] ,
    aresetn,
    Q,
    DIADI,
    DIBDI,
    E);
  output [31:0]b_ram_out;
  input clk;
  input \slv_regs_reg[0][12] ;
  input aresetn;
  input [6:0]Q;
  input [15:0]DIADI;
  input [15:0]DIBDI;
  input [0:0]E;

  wire [15:0]DIADI;
  wire [15:0]DIBDI;
  wire [0:0]E;
  wire [6:0]Q;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \count_i[6]_i_2__1_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [6:0]p_0_in__2;
  wire \slv_regs_reg[0][12] ;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,Q,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(DIADI),
        .DIBDI(DIBDI),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(b_ram_out[15:0]),
        .DOBDO(b_ram_out[31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(\slv_regs_reg[0][12] ),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(aresetn),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({E,E,E,E}));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1__1 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in__2[0]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1__1 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in__2[1]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[2]_i_1__1 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[2]),
        .O(p_0_in__2[2]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1__1 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in__2[3]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \count_i[4]_i_1__1 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .I3(count_i_reg__0[3]),
        .I4(count_i_reg__0[4]),
        .O(p_0_in__2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \count_i[5]_i_1__1 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[4]),
        .I5(count_i_reg__0[5]),
        .O(p_0_in__2[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[6]_i_1__1 
       (.I0(\count_i[6]_i_2__1_n_0 ),
        .I1(count_i_reg__0[5]),
        .I2(count_i_reg__0[6]),
        .O(p_0_in__2[6]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \count_i[6]_i_2__1 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .I4(count_i_reg__0[3]),
        .O(\count_i[6]_i_2__1_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__2[0]),
        .Q(count_i_reg__0[0]),
        .R(aresetn));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__2[1]),
        .Q(count_i_reg__0[1]),
        .R(aresetn));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__2[2]),
        .Q(count_i_reg__0[2]),
        .R(aresetn));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__2[3]),
        .Q(count_i_reg__0[3]),
        .R(aresetn));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__2[4]),
        .Q(count_i_reg__0[4]),
        .R(aresetn));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__2[5]),
        .Q(count_i_reg__0[5]),
        .R(aresetn));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__2[6]),
        .Q(count_i_reg__0[6]),
        .R(aresetn));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_top_0_0_block_ram_9
   (b_ram_out,
    clk,
    \slv_regs_reg[0][12] ,
    aresetn,
    Q,
    DIADI,
    DIBDI,
    E);
  output [31:0]b_ram_out;
  input clk;
  input \slv_regs_reg[0][12] ;
  input aresetn;
  input [6:0]Q;
  input [15:0]DIADI;
  input [15:0]DIBDI;
  input [0:0]E;

  wire [15:0]DIADI;
  wire [15:0]DIBDI;
  wire [0:0]E;
  wire [6:0]Q;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \count_i[6]_i_2__2_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [6:0]p_0_in__3;
  wire \slv_regs_reg[0][12] ;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,Q,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(DIADI),
        .DIBDI(DIBDI),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(b_ram_out[15:0]),
        .DOBDO(b_ram_out[31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(\slv_regs_reg[0][12] ),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(aresetn),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({E,E,E,E}));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1__2 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in__3[0]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1__2 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in__3[1]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[2]_i_1__2 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[2]),
        .O(p_0_in__3[2]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1__2 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in__3[3]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \count_i[4]_i_1__2 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .I3(count_i_reg__0[3]),
        .I4(count_i_reg__0[4]),
        .O(p_0_in__3[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \count_i[5]_i_1__2 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[4]),
        .I5(count_i_reg__0[5]),
        .O(p_0_in__3[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \count_i[6]_i_1__2 
       (.I0(\count_i[6]_i_2__2_n_0 ),
        .I1(count_i_reg__0[5]),
        .I2(count_i_reg__0[6]),
        .O(p_0_in__3[6]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \count_i[6]_i_2__2 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .I4(count_i_reg__0[3]),
        .O(\count_i[6]_i_2__2_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__3[0]),
        .Q(count_i_reg__0[0]),
        .R(aresetn));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__3[1]),
        .Q(count_i_reg__0[1]),
        .R(aresetn));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__3[2]),
        .Q(count_i_reg__0[2]),
        .R(aresetn));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__3[3]),
        .Q(count_i_reg__0[3]),
        .R(aresetn));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__3[4]),
        .Q(count_i_reg__0[4]),
        .R(aresetn));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__3[5]),
        .Q(count_i_reg__0[5]),
        .R(aresetn));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__3[6]),
        .Q(count_i_reg__0[6]),
        .R(aresetn));
endmodule

(* ORIG_REF_NAME = "dot_product" *) 
module design_1_top_0_0_dot_product
   (D,
    CO,
    start_reg,
    E,
    A,
    b_ram_out,
    dp_enable,
    clk,
    p_0_in__0,
    \slv_regs_reg[0][11] ,
    O,
    \slv_regs_reg[0][11]_0 ,
    aresetn,
    start,
    Q,
    \slv_regs_reg[0][8] ,
    \slv_regs_reg[0][0] ,
    \slv_regs_reg[0][11]_1 ,
    \slv_regs_reg[0][11]_2 ,
    \slv_regs_reg[0][11]_3 ,
    SR);
  output [47:0]D;
  output [0:0]CO;
  output start_reg;
  output [0:0]E;
  input [15:0]A;
  input [31:0]b_ram_out;
  input dp_enable;
  input clk;
  input p_0_in__0;
  input [0:0]\slv_regs_reg[0][11] ;
  input [0:0]O;
  input \slv_regs_reg[0][11]_0 ;
  input aresetn;
  input start;
  input [0:0]Q;
  input [7:0]\slv_regs_reg[0][8] ;
  input [0:0]\slv_regs_reg[0][0] ;
  input [0:0]\slv_regs_reg[0][11]_1 ;
  input [0:0]\slv_regs_reg[0][11]_2 ;
  input [0:0]\slv_regs_reg[0][11]_3 ;
  input [0:0]SR;

  wire [15:0]A;
  wire [0:0]CO;
  wire [47:0]D;
  wire [0:0]E;
  wire [0:0]O;
  wire [0:0]Q;
  wire [0:0]SR;
  wire add_r1_carry_i_2_n_0;
  wire add_r1_carry_i_3_n_0;
  wire add_r1_carry_i_4_n_0;
  wire add_r1_carry_n_0;
  wire add_r1_carry_n_1;
  wire add_r1_carry_n_2;
  wire add_r1_carry_n_3;
  wire \add_r[0]_i_2_n_0 ;
  wire \add_r[0]_i_3_n_0 ;
  wire \add_r[0]_i_4_n_0 ;
  wire \add_r[0]_i_5_n_0 ;
  wire \add_r[12]_i_2_n_0 ;
  wire \add_r[12]_i_3_n_0 ;
  wire \add_r[12]_i_4_n_0 ;
  wire \add_r[12]_i_5_n_0 ;
  wire \add_r[16]_i_2_n_0 ;
  wire \add_r[16]_i_3_n_0 ;
  wire \add_r[16]_i_4_n_0 ;
  wire \add_r[16]_i_5_n_0 ;
  wire \add_r[20]_i_2_n_0 ;
  wire \add_r[20]_i_3_n_0 ;
  wire \add_r[20]_i_4_n_0 ;
  wire \add_r[20]_i_5_n_0 ;
  wire \add_r[24]_i_2_n_0 ;
  wire \add_r[24]_i_3_n_0 ;
  wire \add_r[24]_i_4_n_0 ;
  wire \add_r[24]_i_5_n_0 ;
  wire \add_r[28]_i_2_n_0 ;
  wire \add_r[28]_i_3_n_0 ;
  wire \add_r[28]_i_4_n_0 ;
  wire \add_r[28]_i_5_n_0 ;
  wire \add_r[32]_i_2_n_0 ;
  wire \add_r[32]_i_3_n_0 ;
  wire \add_r[32]_i_4_n_0 ;
  wire \add_r[32]_i_5_n_0 ;
  wire \add_r[36]_i_2_n_0 ;
  wire \add_r[36]_i_3_n_0 ;
  wire \add_r[36]_i_4_n_0 ;
  wire \add_r[36]_i_5_n_0 ;
  wire \add_r[40]_i_2_n_0 ;
  wire \add_r[40]_i_3_n_0 ;
  wire \add_r[40]_i_4_n_0 ;
  wire \add_r[40]_i_5_n_0 ;
  wire \add_r[44]_i_2_n_0 ;
  wire \add_r[44]_i_3_n_0 ;
  wire \add_r[44]_i_4_n_0 ;
  wire \add_r[44]_i_5_n_0 ;
  wire \add_r[4]_i_2_n_0 ;
  wire \add_r[4]_i_3_n_0 ;
  wire \add_r[4]_i_4_n_0 ;
  wire \add_r[4]_i_5_n_0 ;
  wire \add_r[8]_i_2_n_0 ;
  wire \add_r[8]_i_3_n_0 ;
  wire \add_r[8]_i_4_n_0 ;
  wire \add_r[8]_i_5_n_0 ;
  wire \add_r_reg[0]_i_1_n_0 ;
  wire \add_r_reg[0]_i_1_n_1 ;
  wire \add_r_reg[0]_i_1_n_2 ;
  wire \add_r_reg[0]_i_1_n_3 ;
  wire \add_r_reg[0]_i_1_n_4 ;
  wire \add_r_reg[0]_i_1_n_5 ;
  wire \add_r_reg[0]_i_1_n_6 ;
  wire \add_r_reg[0]_i_1_n_7 ;
  wire \add_r_reg[12]_i_1_n_0 ;
  wire \add_r_reg[12]_i_1_n_1 ;
  wire \add_r_reg[12]_i_1_n_2 ;
  wire \add_r_reg[12]_i_1_n_3 ;
  wire \add_r_reg[12]_i_1_n_4 ;
  wire \add_r_reg[12]_i_1_n_5 ;
  wire \add_r_reg[12]_i_1_n_6 ;
  wire \add_r_reg[12]_i_1_n_7 ;
  wire \add_r_reg[16]_i_1_n_0 ;
  wire \add_r_reg[16]_i_1_n_1 ;
  wire \add_r_reg[16]_i_1_n_2 ;
  wire \add_r_reg[16]_i_1_n_3 ;
  wire \add_r_reg[16]_i_1_n_4 ;
  wire \add_r_reg[16]_i_1_n_5 ;
  wire \add_r_reg[16]_i_1_n_6 ;
  wire \add_r_reg[16]_i_1_n_7 ;
  wire \add_r_reg[20]_i_1_n_0 ;
  wire \add_r_reg[20]_i_1_n_1 ;
  wire \add_r_reg[20]_i_1_n_2 ;
  wire \add_r_reg[20]_i_1_n_3 ;
  wire \add_r_reg[20]_i_1_n_4 ;
  wire \add_r_reg[20]_i_1_n_5 ;
  wire \add_r_reg[20]_i_1_n_6 ;
  wire \add_r_reg[20]_i_1_n_7 ;
  wire \add_r_reg[24]_i_1_n_0 ;
  wire \add_r_reg[24]_i_1_n_1 ;
  wire \add_r_reg[24]_i_1_n_2 ;
  wire \add_r_reg[24]_i_1_n_3 ;
  wire \add_r_reg[24]_i_1_n_4 ;
  wire \add_r_reg[24]_i_1_n_5 ;
  wire \add_r_reg[24]_i_1_n_6 ;
  wire \add_r_reg[24]_i_1_n_7 ;
  wire \add_r_reg[28]_i_1_n_0 ;
  wire \add_r_reg[28]_i_1_n_1 ;
  wire \add_r_reg[28]_i_1_n_2 ;
  wire \add_r_reg[28]_i_1_n_3 ;
  wire \add_r_reg[28]_i_1_n_4 ;
  wire \add_r_reg[28]_i_1_n_5 ;
  wire \add_r_reg[28]_i_1_n_6 ;
  wire \add_r_reg[28]_i_1_n_7 ;
  wire \add_r_reg[32]_i_1_n_0 ;
  wire \add_r_reg[32]_i_1_n_1 ;
  wire \add_r_reg[32]_i_1_n_2 ;
  wire \add_r_reg[32]_i_1_n_3 ;
  wire \add_r_reg[32]_i_1_n_4 ;
  wire \add_r_reg[32]_i_1_n_5 ;
  wire \add_r_reg[32]_i_1_n_6 ;
  wire \add_r_reg[32]_i_1_n_7 ;
  wire \add_r_reg[36]_i_1_n_0 ;
  wire \add_r_reg[36]_i_1_n_1 ;
  wire \add_r_reg[36]_i_1_n_2 ;
  wire \add_r_reg[36]_i_1_n_3 ;
  wire \add_r_reg[36]_i_1_n_4 ;
  wire \add_r_reg[36]_i_1_n_5 ;
  wire \add_r_reg[36]_i_1_n_6 ;
  wire \add_r_reg[36]_i_1_n_7 ;
  wire \add_r_reg[40]_i_1_n_0 ;
  wire \add_r_reg[40]_i_1_n_1 ;
  wire \add_r_reg[40]_i_1_n_2 ;
  wire \add_r_reg[40]_i_1_n_3 ;
  wire \add_r_reg[40]_i_1_n_4 ;
  wire \add_r_reg[40]_i_1_n_5 ;
  wire \add_r_reg[40]_i_1_n_6 ;
  wire \add_r_reg[40]_i_1_n_7 ;
  wire \add_r_reg[44]_i_1_n_1 ;
  wire \add_r_reg[44]_i_1_n_2 ;
  wire \add_r_reg[44]_i_1_n_3 ;
  wire \add_r_reg[44]_i_1_n_4 ;
  wire \add_r_reg[44]_i_1_n_5 ;
  wire \add_r_reg[44]_i_1_n_6 ;
  wire \add_r_reg[44]_i_1_n_7 ;
  wire \add_r_reg[4]_i_1_n_0 ;
  wire \add_r_reg[4]_i_1_n_1 ;
  wire \add_r_reg[4]_i_1_n_2 ;
  wire \add_r_reg[4]_i_1_n_3 ;
  wire \add_r_reg[4]_i_1_n_4 ;
  wire \add_r_reg[4]_i_1_n_5 ;
  wire \add_r_reg[4]_i_1_n_6 ;
  wire \add_r_reg[4]_i_1_n_7 ;
  wire \add_r_reg[8]_i_1_n_0 ;
  wire \add_r_reg[8]_i_1_n_1 ;
  wire \add_r_reg[8]_i_1_n_2 ;
  wire \add_r_reg[8]_i_1_n_3 ;
  wire \add_r_reg[8]_i_1_n_4 ;
  wire \add_r_reg[8]_i_1_n_5 ;
  wire \add_r_reg[8]_i_1_n_6 ;
  wire \add_r_reg[8]_i_1_n_7 ;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \counter[1]_i_1_n_0 ;
  wire \counter[8]_i_4__0_n_0 ;
  wire \counter_reg_n_0_[0] ;
  wire \counter_reg_n_0_[1] ;
  wire \counter_reg_n_0_[2] ;
  wire \counter_reg_n_0_[3] ;
  wire \counter_reg_n_0_[4] ;
  wire \counter_reg_n_0_[5] ;
  wire \counter_reg_n_0_[6] ;
  wire \counter_reg_n_0_[7] ;
  wire \counter_reg_n_0_[8] ;
  wire dp_enable;
  wire mul_r0_n_100;
  wire mul_r0_n_101;
  wire mul_r0_n_102;
  wire mul_r0_n_103;
  wire mul_r0_n_104;
  wire mul_r0_n_105;
  wire mul_r0_n_106;
  wire mul_r0_n_107;
  wire mul_r0_n_108;
  wire mul_r0_n_109;
  wire mul_r0_n_110;
  wire mul_r0_n_111;
  wire mul_r0_n_112;
  wire mul_r0_n_113;
  wire mul_r0_n_114;
  wire mul_r0_n_115;
  wire mul_r0_n_116;
  wire mul_r0_n_117;
  wire mul_r0_n_118;
  wire mul_r0_n_119;
  wire mul_r0_n_120;
  wire mul_r0_n_121;
  wire mul_r0_n_122;
  wire mul_r0_n_123;
  wire mul_r0_n_124;
  wire mul_r0_n_125;
  wire mul_r0_n_126;
  wire mul_r0_n_127;
  wire mul_r0_n_128;
  wire mul_r0_n_129;
  wire mul_r0_n_130;
  wire mul_r0_n_131;
  wire mul_r0_n_132;
  wire mul_r0_n_133;
  wire mul_r0_n_134;
  wire mul_r0_n_135;
  wire mul_r0_n_136;
  wire mul_r0_n_137;
  wire mul_r0_n_138;
  wire mul_r0_n_139;
  wire mul_r0_n_140;
  wire mul_r0_n_141;
  wire mul_r0_n_142;
  wire mul_r0_n_143;
  wire mul_r0_n_144;
  wire mul_r0_n_145;
  wire mul_r0_n_146;
  wire mul_r0_n_147;
  wire mul_r0_n_148;
  wire mul_r0_n_149;
  wire mul_r0_n_150;
  wire mul_r0_n_151;
  wire mul_r0_n_152;
  wire mul_r0_n_153;
  wire mul_r0_n_58;
  wire mul_r0_n_59;
  wire mul_r0_n_60;
  wire mul_r0_n_61;
  wire mul_r0_n_62;
  wire mul_r0_n_63;
  wire mul_r0_n_64;
  wire mul_r0_n_65;
  wire mul_r0_n_66;
  wire mul_r0_n_67;
  wire mul_r0_n_68;
  wire mul_r0_n_69;
  wire mul_r0_n_70;
  wire mul_r0_n_71;
  wire mul_r0_n_72;
  wire mul_r0_n_73;
  wire mul_r0_n_74;
  wire mul_r0_n_75;
  wire mul_r0_n_76;
  wire mul_r0_n_77;
  wire mul_r0_n_78;
  wire mul_r0_n_79;
  wire mul_r0_n_80;
  wire mul_r0_n_81;
  wire mul_r0_n_82;
  wire mul_r0_n_83;
  wire mul_r0_n_84;
  wire mul_r0_n_85;
  wire mul_r0_n_86;
  wire mul_r0_n_87;
  wire mul_r0_n_88;
  wire mul_r0_n_89;
  wire mul_r0_n_90;
  wire mul_r0_n_91;
  wire mul_r0_n_92;
  wire mul_r0_n_93;
  wire mul_r0_n_94;
  wire mul_r0_n_95;
  wire mul_r0_n_96;
  wire mul_r0_n_97;
  wire mul_r0_n_98;
  wire mul_r0_n_99;
  wire mul_r_reg__0_n_58;
  wire mul_r_reg__0_n_59;
  wire mul_r_reg__0_n_60;
  wire mul_r_reg__0_n_61;
  wire mul_r_reg__0_n_62;
  wire mul_r_reg__0_n_63;
  wire mul_r_reg__0_n_64;
  wire mul_r_reg__0_n_65;
  wire mul_r_reg__0_n_66;
  wire mul_r_reg__0_n_67;
  wire mul_r_reg__0_n_68;
  wire mul_r_reg__0_n_69;
  wire mul_r_reg__0_n_70;
  wire mul_r_reg__0_n_71;
  wire mul_r_reg__0_n_72;
  wire mul_r_reg__0_n_73;
  wire mul_r_reg__0_n_74;
  wire [47:0]mul_r_reg__1;
  wire p_0_in__0;
  wire [8:0]p_1_in;
  wire \res_mem[383]_i_2_n_0 ;
  wire [0:0]\slv_regs_reg[0][0] ;
  wire [0:0]\slv_regs_reg[0][11] ;
  wire \slv_regs_reg[0][11]_0 ;
  wire [0:0]\slv_regs_reg[0][11]_1 ;
  wire [0:0]\slv_regs_reg[0][11]_2 ;
  wire [0:0]\slv_regs_reg[0][11]_3 ;
  wire [7:0]\slv_regs_reg[0][8] ;
  wire start;
  wire start_reg;
  wire [3:0]NLW_add_r1_carry_O_UNCONNECTED;
  wire [3:1]NLW_add_r1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_add_r1_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_add_r_reg[44]_i_1_CO_UNCONNECTED ;
  wire NLW_mul_r0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r0_CARRYOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r_reg__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r_reg__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_r_reg__0_PCOUT_UNCONNECTED;

  CARRY4 add_r1_carry
       (.CI(1'b0),
        .CO({add_r1_carry_n_0,add_r1_carry_n_1,add_r1_carry_n_2,add_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry_O_UNCONNECTED[3:0]),
        .S({\slv_regs_reg[0][11] ,add_r1_carry_i_2_n_0,add_r1_carry_i_3_n_0,add_r1_carry_i_4_n_0}));
  CARRY4 add_r1_carry__0
       (.CI(add_r1_carry_n_0),
        .CO({NLW_add_r1_carry__0_CO_UNCONNECTED[3:1],CO}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,O}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_2
       (.I0(\counter_reg_n_0_[7] ),
        .I1(\slv_regs_reg[0][8] [6]),
        .I2(\counter_reg_n_0_[6] ),
        .I3(\slv_regs_reg[0][8] [5]),
        .I4(\slv_regs_reg[0][8] [7]),
        .I5(\counter_reg_n_0_[8] ),
        .O(add_r1_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_3
       (.I0(\counter_reg_n_0_[4] ),
        .I1(\slv_regs_reg[0][8] [3]),
        .I2(\counter_reg_n_0_[3] ),
        .I3(\slv_regs_reg[0][8] [2]),
        .I4(\slv_regs_reg[0][8] [4]),
        .I5(\counter_reg_n_0_[5] ),
        .O(add_r1_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h0990000000000990)) 
    add_r1_carry_i_4
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\slv_regs_reg[0][8] [0]),
        .I2(\slv_regs_reg[0][0] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\slv_regs_reg[0][8] [1]),
        .I5(\counter_reg_n_0_[2] ),
        .O(add_r1_carry_i_4_n_0));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_2 
       (.I0(mul_r_reg__1[3]),
        .I1(CO),
        .I2(D[3]),
        .O(\add_r[0]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_3 
       (.I0(mul_r_reg__1[2]),
        .I1(CO),
        .I2(D[2]),
        .O(\add_r[0]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_4 
       (.I0(mul_r_reg__1[1]),
        .I1(CO),
        .I2(D[1]),
        .O(\add_r[0]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_5 
       (.I0(mul_r_reg__1[0]),
        .I1(CO),
        .I2(D[0]),
        .O(\add_r[0]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_2 
       (.I0(mul_r_reg__1[15]),
        .I1(CO),
        .I2(D[15]),
        .O(\add_r[12]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_3 
       (.I0(mul_r_reg__1[14]),
        .I1(CO),
        .I2(D[14]),
        .O(\add_r[12]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_4 
       (.I0(mul_r_reg__1[13]),
        .I1(CO),
        .I2(D[13]),
        .O(\add_r[12]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_5 
       (.I0(mul_r_reg__1[12]),
        .I1(CO),
        .I2(D[12]),
        .O(\add_r[12]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_2 
       (.I0(mul_r_reg__1[19]),
        .I1(CO),
        .I2(D[19]),
        .O(\add_r[16]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_3 
       (.I0(mul_r_reg__1[18]),
        .I1(CO),
        .I2(D[18]),
        .O(\add_r[16]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_4 
       (.I0(mul_r_reg__1[17]),
        .I1(CO),
        .I2(D[17]),
        .O(\add_r[16]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_5 
       (.I0(mul_r_reg__1[16]),
        .I1(CO),
        .I2(D[16]),
        .O(\add_r[16]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_2 
       (.I0(mul_r_reg__1[23]),
        .I1(CO),
        .I2(D[23]),
        .O(\add_r[20]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_3 
       (.I0(mul_r_reg__1[22]),
        .I1(CO),
        .I2(D[22]),
        .O(\add_r[20]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_4 
       (.I0(mul_r_reg__1[21]),
        .I1(CO),
        .I2(D[21]),
        .O(\add_r[20]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_5 
       (.I0(mul_r_reg__1[20]),
        .I1(CO),
        .I2(D[20]),
        .O(\add_r[20]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_2 
       (.I0(mul_r_reg__1[27]),
        .I1(CO),
        .I2(D[27]),
        .O(\add_r[24]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_3 
       (.I0(mul_r_reg__1[26]),
        .I1(CO),
        .I2(D[26]),
        .O(\add_r[24]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_4 
       (.I0(mul_r_reg__1[25]),
        .I1(CO),
        .I2(D[25]),
        .O(\add_r[24]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_5 
       (.I0(mul_r_reg__1[24]),
        .I1(CO),
        .I2(D[24]),
        .O(\add_r[24]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_2 
       (.I0(mul_r_reg__1[31]),
        .I1(CO),
        .I2(D[31]),
        .O(\add_r[28]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_3 
       (.I0(mul_r_reg__1[30]),
        .I1(CO),
        .I2(D[30]),
        .O(\add_r[28]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_4 
       (.I0(mul_r_reg__1[29]),
        .I1(CO),
        .I2(D[29]),
        .O(\add_r[28]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_5 
       (.I0(mul_r_reg__1[28]),
        .I1(CO),
        .I2(D[28]),
        .O(\add_r[28]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_2 
       (.I0(mul_r_reg__1[35]),
        .I1(CO),
        .I2(D[35]),
        .O(\add_r[32]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_3 
       (.I0(mul_r_reg__1[34]),
        .I1(CO),
        .I2(D[34]),
        .O(\add_r[32]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_4 
       (.I0(mul_r_reg__1[33]),
        .I1(CO),
        .I2(D[33]),
        .O(\add_r[32]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_5 
       (.I0(mul_r_reg__1[32]),
        .I1(CO),
        .I2(D[32]),
        .O(\add_r[32]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_2 
       (.I0(mul_r_reg__1[39]),
        .I1(CO),
        .I2(D[39]),
        .O(\add_r[36]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_3 
       (.I0(mul_r_reg__1[38]),
        .I1(CO),
        .I2(D[38]),
        .O(\add_r[36]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_4 
       (.I0(mul_r_reg__1[37]),
        .I1(CO),
        .I2(D[37]),
        .O(\add_r[36]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_5 
       (.I0(mul_r_reg__1[36]),
        .I1(CO),
        .I2(D[36]),
        .O(\add_r[36]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_2 
       (.I0(mul_r_reg__1[43]),
        .I1(CO),
        .I2(D[43]),
        .O(\add_r[40]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_3 
       (.I0(mul_r_reg__1[42]),
        .I1(CO),
        .I2(D[42]),
        .O(\add_r[40]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_4 
       (.I0(mul_r_reg__1[41]),
        .I1(CO),
        .I2(D[41]),
        .O(\add_r[40]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_5 
       (.I0(mul_r_reg__1[40]),
        .I1(CO),
        .I2(D[40]),
        .O(\add_r[40]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_2 
       (.I0(D[47]),
        .I1(CO),
        .I2(mul_r_reg__1[47]),
        .O(\add_r[44]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_3 
       (.I0(mul_r_reg__1[46]),
        .I1(CO),
        .I2(D[46]),
        .O(\add_r[44]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_4 
       (.I0(mul_r_reg__1[45]),
        .I1(CO),
        .I2(D[45]),
        .O(\add_r[44]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_5 
       (.I0(mul_r_reg__1[44]),
        .I1(CO),
        .I2(D[44]),
        .O(\add_r[44]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_2 
       (.I0(mul_r_reg__1[7]),
        .I1(CO),
        .I2(D[7]),
        .O(\add_r[4]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_3 
       (.I0(mul_r_reg__1[6]),
        .I1(CO),
        .I2(D[6]),
        .O(\add_r[4]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_4 
       (.I0(mul_r_reg__1[5]),
        .I1(CO),
        .I2(D[5]),
        .O(\add_r[4]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_5 
       (.I0(mul_r_reg__1[4]),
        .I1(CO),
        .I2(D[4]),
        .O(\add_r[4]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_2 
       (.I0(mul_r_reg__1[11]),
        .I1(CO),
        .I2(D[11]),
        .O(\add_r[8]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_3 
       (.I0(mul_r_reg__1[10]),
        .I1(CO),
        .I2(D[10]),
        .O(\add_r[8]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_4 
       (.I0(mul_r_reg__1[9]),
        .I1(CO),
        .I2(D[9]),
        .O(\add_r[8]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_5 
       (.I0(mul_r_reg__1[8]),
        .I1(CO),
        .I2(D[8]),
        .O(\add_r[8]_i_5_n_0 ));
  FDRE \add_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1_n_7 ),
        .Q(D[0]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\add_r_reg[0]_i_1_n_0 ,\add_r_reg[0]_i_1_n_1 ,\add_r_reg[0]_i_1_n_2 ,\add_r_reg[0]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[3:0]),
        .O({\add_r_reg[0]_i_1_n_4 ,\add_r_reg[0]_i_1_n_5 ,\add_r_reg[0]_i_1_n_6 ,\add_r_reg[0]_i_1_n_7 }),
        .S({\add_r[0]_i_2_n_0 ,\add_r[0]_i_3_n_0 ,\add_r[0]_i_4_n_0 ,\add_r[0]_i_5_n_0 }));
  FDRE \add_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1_n_5 ),
        .Q(D[10]),
        .R(p_0_in__0));
  FDRE \add_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1_n_4 ),
        .Q(D[11]),
        .R(p_0_in__0));
  FDRE \add_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1_n_7 ),
        .Q(D[12]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[12]_i_1 
       (.CI(\add_r_reg[8]_i_1_n_0 ),
        .CO({\add_r_reg[12]_i_1_n_0 ,\add_r_reg[12]_i_1_n_1 ,\add_r_reg[12]_i_1_n_2 ,\add_r_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[15:12]),
        .O({\add_r_reg[12]_i_1_n_4 ,\add_r_reg[12]_i_1_n_5 ,\add_r_reg[12]_i_1_n_6 ,\add_r_reg[12]_i_1_n_7 }),
        .S({\add_r[12]_i_2_n_0 ,\add_r[12]_i_3_n_0 ,\add_r[12]_i_4_n_0 ,\add_r[12]_i_5_n_0 }));
  FDRE \add_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1_n_6 ),
        .Q(D[13]),
        .R(p_0_in__0));
  FDRE \add_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1_n_5 ),
        .Q(D[14]),
        .R(p_0_in__0));
  FDRE \add_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1_n_4 ),
        .Q(D[15]),
        .R(p_0_in__0));
  FDRE \add_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1_n_7 ),
        .Q(D[16]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[16]_i_1 
       (.CI(\add_r_reg[12]_i_1_n_0 ),
        .CO({\add_r_reg[16]_i_1_n_0 ,\add_r_reg[16]_i_1_n_1 ,\add_r_reg[16]_i_1_n_2 ,\add_r_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[19:16]),
        .O({\add_r_reg[16]_i_1_n_4 ,\add_r_reg[16]_i_1_n_5 ,\add_r_reg[16]_i_1_n_6 ,\add_r_reg[16]_i_1_n_7 }),
        .S({\add_r[16]_i_2_n_0 ,\add_r[16]_i_3_n_0 ,\add_r[16]_i_4_n_0 ,\add_r[16]_i_5_n_0 }));
  FDRE \add_r_reg[17] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1_n_6 ),
        .Q(D[17]),
        .R(p_0_in__0));
  FDRE \add_r_reg[18] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1_n_5 ),
        .Q(D[18]),
        .R(p_0_in__0));
  FDRE \add_r_reg[19] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1_n_4 ),
        .Q(D[19]),
        .R(p_0_in__0));
  FDRE \add_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1_n_6 ),
        .Q(D[1]),
        .R(p_0_in__0));
  FDRE \add_r_reg[20] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1_n_7 ),
        .Q(D[20]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[20]_i_1 
       (.CI(\add_r_reg[16]_i_1_n_0 ),
        .CO({\add_r_reg[20]_i_1_n_0 ,\add_r_reg[20]_i_1_n_1 ,\add_r_reg[20]_i_1_n_2 ,\add_r_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[23:20]),
        .O({\add_r_reg[20]_i_1_n_4 ,\add_r_reg[20]_i_1_n_5 ,\add_r_reg[20]_i_1_n_6 ,\add_r_reg[20]_i_1_n_7 }),
        .S({\add_r[20]_i_2_n_0 ,\add_r[20]_i_3_n_0 ,\add_r[20]_i_4_n_0 ,\add_r[20]_i_5_n_0 }));
  FDRE \add_r_reg[21] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1_n_6 ),
        .Q(D[21]),
        .R(p_0_in__0));
  FDRE \add_r_reg[22] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1_n_5 ),
        .Q(D[22]),
        .R(p_0_in__0));
  FDRE \add_r_reg[23] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1_n_4 ),
        .Q(D[23]),
        .R(p_0_in__0));
  FDRE \add_r_reg[24] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1_n_7 ),
        .Q(D[24]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[24]_i_1 
       (.CI(\add_r_reg[20]_i_1_n_0 ),
        .CO({\add_r_reg[24]_i_1_n_0 ,\add_r_reg[24]_i_1_n_1 ,\add_r_reg[24]_i_1_n_2 ,\add_r_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[27:24]),
        .O({\add_r_reg[24]_i_1_n_4 ,\add_r_reg[24]_i_1_n_5 ,\add_r_reg[24]_i_1_n_6 ,\add_r_reg[24]_i_1_n_7 }),
        .S({\add_r[24]_i_2_n_0 ,\add_r[24]_i_3_n_0 ,\add_r[24]_i_4_n_0 ,\add_r[24]_i_5_n_0 }));
  FDRE \add_r_reg[25] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1_n_6 ),
        .Q(D[25]),
        .R(p_0_in__0));
  FDRE \add_r_reg[26] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1_n_5 ),
        .Q(D[26]),
        .R(p_0_in__0));
  FDRE \add_r_reg[27] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1_n_4 ),
        .Q(D[27]),
        .R(p_0_in__0));
  FDRE \add_r_reg[28] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1_n_7 ),
        .Q(D[28]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[28]_i_1 
       (.CI(\add_r_reg[24]_i_1_n_0 ),
        .CO({\add_r_reg[28]_i_1_n_0 ,\add_r_reg[28]_i_1_n_1 ,\add_r_reg[28]_i_1_n_2 ,\add_r_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[31:28]),
        .O({\add_r_reg[28]_i_1_n_4 ,\add_r_reg[28]_i_1_n_5 ,\add_r_reg[28]_i_1_n_6 ,\add_r_reg[28]_i_1_n_7 }),
        .S({\add_r[28]_i_2_n_0 ,\add_r[28]_i_3_n_0 ,\add_r[28]_i_4_n_0 ,\add_r[28]_i_5_n_0 }));
  FDRE \add_r_reg[29] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1_n_6 ),
        .Q(D[29]),
        .R(p_0_in__0));
  FDRE \add_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1_n_5 ),
        .Q(D[2]),
        .R(p_0_in__0));
  FDRE \add_r_reg[30] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1_n_5 ),
        .Q(D[30]),
        .R(p_0_in__0));
  FDRE \add_r_reg[31] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1_n_4 ),
        .Q(D[31]),
        .R(p_0_in__0));
  FDRE \add_r_reg[32] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1_n_7 ),
        .Q(D[32]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[32]_i_1 
       (.CI(\add_r_reg[28]_i_1_n_0 ),
        .CO({\add_r_reg[32]_i_1_n_0 ,\add_r_reg[32]_i_1_n_1 ,\add_r_reg[32]_i_1_n_2 ,\add_r_reg[32]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[35:32]),
        .O({\add_r_reg[32]_i_1_n_4 ,\add_r_reg[32]_i_1_n_5 ,\add_r_reg[32]_i_1_n_6 ,\add_r_reg[32]_i_1_n_7 }),
        .S({\add_r[32]_i_2_n_0 ,\add_r[32]_i_3_n_0 ,\add_r[32]_i_4_n_0 ,\add_r[32]_i_5_n_0 }));
  FDRE \add_r_reg[33] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1_n_6 ),
        .Q(D[33]),
        .R(p_0_in__0));
  FDRE \add_r_reg[34] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1_n_5 ),
        .Q(D[34]),
        .R(p_0_in__0));
  FDRE \add_r_reg[35] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1_n_4 ),
        .Q(D[35]),
        .R(p_0_in__0));
  FDRE \add_r_reg[36] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1_n_7 ),
        .Q(D[36]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[36]_i_1 
       (.CI(\add_r_reg[32]_i_1_n_0 ),
        .CO({\add_r_reg[36]_i_1_n_0 ,\add_r_reg[36]_i_1_n_1 ,\add_r_reg[36]_i_1_n_2 ,\add_r_reg[36]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[39:36]),
        .O({\add_r_reg[36]_i_1_n_4 ,\add_r_reg[36]_i_1_n_5 ,\add_r_reg[36]_i_1_n_6 ,\add_r_reg[36]_i_1_n_7 }),
        .S({\add_r[36]_i_2_n_0 ,\add_r[36]_i_3_n_0 ,\add_r[36]_i_4_n_0 ,\add_r[36]_i_5_n_0 }));
  FDRE \add_r_reg[37] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1_n_6 ),
        .Q(D[37]),
        .R(p_0_in__0));
  FDRE \add_r_reg[38] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1_n_5 ),
        .Q(D[38]),
        .R(p_0_in__0));
  FDRE \add_r_reg[39] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1_n_4 ),
        .Q(D[39]),
        .R(p_0_in__0));
  FDRE \add_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1_n_4 ),
        .Q(D[3]),
        .R(p_0_in__0));
  FDRE \add_r_reg[40] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1_n_7 ),
        .Q(D[40]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[40]_i_1 
       (.CI(\add_r_reg[36]_i_1_n_0 ),
        .CO({\add_r_reg[40]_i_1_n_0 ,\add_r_reg[40]_i_1_n_1 ,\add_r_reg[40]_i_1_n_2 ,\add_r_reg[40]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[43:40]),
        .O({\add_r_reg[40]_i_1_n_4 ,\add_r_reg[40]_i_1_n_5 ,\add_r_reg[40]_i_1_n_6 ,\add_r_reg[40]_i_1_n_7 }),
        .S({\add_r[40]_i_2_n_0 ,\add_r[40]_i_3_n_0 ,\add_r[40]_i_4_n_0 ,\add_r[40]_i_5_n_0 }));
  FDRE \add_r_reg[41] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1_n_6 ),
        .Q(D[41]),
        .R(p_0_in__0));
  FDRE \add_r_reg[42] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1_n_5 ),
        .Q(D[42]),
        .R(p_0_in__0));
  FDRE \add_r_reg[43] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1_n_4 ),
        .Q(D[43]),
        .R(p_0_in__0));
  FDRE \add_r_reg[44] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1_n_7 ),
        .Q(D[44]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[44]_i_1 
       (.CI(\add_r_reg[40]_i_1_n_0 ),
        .CO({\NLW_add_r_reg[44]_i_1_CO_UNCONNECTED [3],\add_r_reg[44]_i_1_n_1 ,\add_r_reg[44]_i_1_n_2 ,\add_r_reg[44]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,mul_r_reg__1[46:44]}),
        .O({\add_r_reg[44]_i_1_n_4 ,\add_r_reg[44]_i_1_n_5 ,\add_r_reg[44]_i_1_n_6 ,\add_r_reg[44]_i_1_n_7 }),
        .S({\add_r[44]_i_2_n_0 ,\add_r[44]_i_3_n_0 ,\add_r[44]_i_4_n_0 ,\add_r[44]_i_5_n_0 }));
  FDRE \add_r_reg[45] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1_n_6 ),
        .Q(D[45]),
        .R(p_0_in__0));
  FDRE \add_r_reg[46] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1_n_5 ),
        .Q(D[46]),
        .R(p_0_in__0));
  FDRE \add_r_reg[47] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1_n_4 ),
        .Q(D[47]),
        .R(p_0_in__0));
  FDRE \add_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1_n_7 ),
        .Q(D[4]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[4]_i_1 
       (.CI(\add_r_reg[0]_i_1_n_0 ),
        .CO({\add_r_reg[4]_i_1_n_0 ,\add_r_reg[4]_i_1_n_1 ,\add_r_reg[4]_i_1_n_2 ,\add_r_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[7:4]),
        .O({\add_r_reg[4]_i_1_n_4 ,\add_r_reg[4]_i_1_n_5 ,\add_r_reg[4]_i_1_n_6 ,\add_r_reg[4]_i_1_n_7 }),
        .S({\add_r[4]_i_2_n_0 ,\add_r[4]_i_3_n_0 ,\add_r[4]_i_4_n_0 ,\add_r[4]_i_5_n_0 }));
  FDRE \add_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1_n_6 ),
        .Q(D[5]),
        .R(p_0_in__0));
  FDRE \add_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1_n_5 ),
        .Q(D[6]),
        .R(p_0_in__0));
  FDRE \add_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1_n_4 ),
        .Q(D[7]),
        .R(p_0_in__0));
  FDRE \add_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1_n_7 ),
        .Q(D[8]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[8]_i_1 
       (.CI(\add_r_reg[4]_i_1_n_0 ),
        .CO({\add_r_reg[8]_i_1_n_0 ,\add_r_reg[8]_i_1_n_1 ,\add_r_reg[8]_i_1_n_2 ,\add_r_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[11:8]),
        .O({\add_r_reg[8]_i_1_n_4 ,\add_r_reg[8]_i_1_n_5 ,\add_r_reg[8]_i_1_n_6 ,\add_r_reg[8]_i_1_n_7 }),
        .S({\add_r[8]_i_2_n_0 ,\add_r[8]_i_3_n_0 ,\add_r[8]_i_4_n_0 ,\add_r[8]_i_5_n_0 }));
  FDRE \add_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1_n_6 ),
        .Q(D[9]),
        .R(p_0_in__0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter[0]_i_1__2 
       (.I0(\counter_reg_n_0_[0] ),
        .O(p_1_in[0]));
  LUT4 #(
    .INIT(16'hF7C8)) 
    \counter[1]_i_1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(dp_enable),
        .I2(CO),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \counter[2]_i_1__1 
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[2] ),
        .O(p_1_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \counter[3]_i_1__1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .O(p_1_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \counter[4]_i_1__0 
       (.I0(\counter_reg_n_0_[3] ),
        .I1(\counter_reg_n_0_[2] ),
        .I2(\counter_reg_n_0_[1] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[4] ),
        .O(p_1_in[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \counter[5]_i_1__0 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .I4(\counter_reg_n_0_[4] ),
        .I5(\counter_reg_n_0_[5] ),
        .O(p_1_in[5]));
  LUT2 #(
    .INIT(4'h9)) 
    \counter[6]_i_1 
       (.I0(\counter[8]_i_4__0_n_0 ),
        .I1(\counter_reg_n_0_[6] ),
        .O(p_1_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hB4)) 
    \counter[7]_i_1 
       (.I0(\counter[8]_i_4__0_n_0 ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(\counter_reg_n_0_[7] ),
        .O(p_1_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'hF708)) 
    \counter[8]_i_3__0 
       (.I0(\counter_reg_n_0_[7] ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(\counter[8]_i_4__0_n_0 ),
        .I3(\counter_reg_n_0_[8] ),
        .O(p_1_in[8]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \counter[8]_i_4__0 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .I4(\counter_reg_n_0_[4] ),
        .I5(\counter_reg_n_0_[5] ),
        .O(\counter[8]_i_4__0_n_0 ));
  FDRE \counter_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(p_1_in[0]),
        .Q(\counter_reg_n_0_[0] ),
        .R(SR));
  FDRE \counter_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\counter[1]_i_1_n_0 ),
        .Q(\counter_reg_n_0_[1] ),
        .R(p_0_in__0));
  FDRE \counter_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(p_1_in[2]),
        .Q(\counter_reg_n_0_[2] ),
        .R(SR));
  FDRE \counter_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(p_1_in[3]),
        .Q(\counter_reg_n_0_[3] ),
        .R(SR));
  FDRE \counter_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(p_1_in[4]),
        .Q(\counter_reg_n_0_[4] ),
        .R(SR));
  FDRE \counter_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(p_1_in[5]),
        .Q(\counter_reg_n_0_[5] ),
        .R(SR));
  FDRE \counter_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(p_1_in[6]),
        .Q(\counter_reg_n_0_[6] ),
        .R(SR));
  FDRE \counter_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(p_1_in[7]),
        .Q(\counter_reg_n_0_[7] ),
        .R(SR));
  FDRE \counter_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(p_1_in[8]),
        .Q(\counter_reg_n_0_[8] ),
        .R(SR));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,b_ram_out[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({A[15],A[15],A}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r0_OVERFLOW_UNCONNECTED),
        .P({mul_r0_n_58,mul_r0_n_59,mul_r0_n_60,mul_r0_n_61,mul_r0_n_62,mul_r0_n_63,mul_r0_n_64,mul_r0_n_65,mul_r0_n_66,mul_r0_n_67,mul_r0_n_68,mul_r0_n_69,mul_r0_n_70,mul_r0_n_71,mul_r0_n_72,mul_r0_n_73,mul_r0_n_74,mul_r0_n_75,mul_r0_n_76,mul_r0_n_77,mul_r0_n_78,mul_r0_n_79,mul_r0_n_80,mul_r0_n_81,mul_r0_n_82,mul_r0_n_83,mul_r0_n_84,mul_r0_n_85,mul_r0_n_86,mul_r0_n_87,mul_r0_n_88,mul_r0_n_89,mul_r0_n_90,mul_r0_n_91,mul_r0_n_92,mul_r0_n_93,mul_r0_n_94,mul_r0_n_95,mul_r0_n_96,mul_r0_n_97,mul_r0_n_98,mul_r0_n_99,mul_r0_n_100,mul_r0_n_101,mul_r0_n_102,mul_r0_n_103,mul_r0_n_104,mul_r0_n_105}),
        .PATTERNBDETECT(NLW_mul_r0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_r0_UNDERFLOW_UNCONNECTED));
  FDRE \mul_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_105),
        .Q(mul_r_reg__1[0]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_95),
        .Q(mul_r_reg__1[10]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_94),
        .Q(mul_r_reg__1[11]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_93),
        .Q(mul_r_reg__1[12]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_92),
        .Q(mul_r_reg__1[13]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_91),
        .Q(mul_r_reg__1[14]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_90),
        .Q(mul_r_reg__1[15]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_89),
        .Q(mul_r_reg__1[16]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_104),
        .Q(mul_r_reg__1[1]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_103),
        .Q(mul_r_reg__1[2]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_102),
        .Q(mul_r_reg__1[3]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_101),
        .Q(mul_r_reg__1[4]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_100),
        .Q(mul_r_reg__1[5]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_99),
        .Q(mul_r_reg__1[6]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_98),
        .Q(mul_r_reg__1[7]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_97),
        .Q(mul_r_reg__1[8]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_96),
        .Q(mul_r_reg__1[9]),
        .R(p_0_in__0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-12 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r_reg__0
       (.A({A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r_reg__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({b_ram_out[31],b_ram_out[31],b_ram_out[31],b_ram_out[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r_reg__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(dp_enable),
        .CLK(clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED),
        .P({mul_r_reg__0_n_58,mul_r_reg__0_n_59,mul_r_reg__0_n_60,mul_r_reg__0_n_61,mul_r_reg__0_n_62,mul_r_reg__0_n_63,mul_r_reg__0_n_64,mul_r_reg__0_n_65,mul_r_reg__0_n_66,mul_r_reg__0_n_67,mul_r_reg__0_n_68,mul_r_reg__0_n_69,mul_r_reg__0_n_70,mul_r_reg__0_n_71,mul_r_reg__0_n_72,mul_r_reg__0_n_73,mul_r_reg__0_n_74,mul_r_reg__1[47:17]}),
        .PATTERNBDETECT(NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .PCOUT(NLW_mul_r_reg__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(p_0_in__0),
        .UNDERFLOW(NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED));
  LUT4 #(
    .INIT(16'h8000)) 
    \res_mem[383]_i_1 
       (.I0(dp_enable),
        .I1(\res_mem[383]_i_2_n_0 ),
        .I2(\slv_regs_reg[0][11]_0 ),
        .I3(aresetn),
        .O(E));
  LUT4 #(
    .INIT(16'h8000)) 
    \res_mem[383]_i_2 
       (.I0(CO),
        .I1(\slv_regs_reg[0][11]_1 ),
        .I2(\slv_regs_reg[0][11]_2 ),
        .I3(\slv_regs_reg[0][11]_3 ),
        .O(\res_mem[383]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h80FF8000FFFF8000)) 
    start_i_1
       (.I0(dp_enable),
        .I1(\res_mem[383]_i_2_n_0 ),
        .I2(\slv_regs_reg[0][11]_0 ),
        .I3(aresetn),
        .I4(start),
        .I5(Q),
        .O(start_reg));
endmodule

(* ORIG_REF_NAME = "dot_product" *) 
module design_1_top_0_0_dot_product_0
   (D,
    \counter_reg[1]_0 ,
    A,
    b_ram_out,
    dp_enable,
    clk,
    p_0_in__0,
    \slv_regs_reg[0][11] ,
    O,
    \slv_regs_reg[0][8] ,
    \slv_regs_reg[0][0] ,
    \slv_regs_reg[0][12] );
  output [47:0]D;
  output [0:0]\counter_reg[1]_0 ;
  input [15:0]A;
  input [31:0]b_ram_out;
  input dp_enable;
  input clk;
  input p_0_in__0;
  input [0:0]\slv_regs_reg[0][11] ;
  input [0:0]O;
  input [7:0]\slv_regs_reg[0][8] ;
  input [0:0]\slv_regs_reg[0][0] ;
  input [0:0]\slv_regs_reg[0][12] ;

  wire [15:0]A;
  wire [47:0]D;
  wire [0:0]O;
  wire add_r1_carry_i_2__0_n_0;
  wire add_r1_carry_i_3__0_n_0;
  wire add_r1_carry_i_4__0_n_0;
  wire add_r1_carry_n_0;
  wire add_r1_carry_n_1;
  wire add_r1_carry_n_2;
  wire add_r1_carry_n_3;
  wire \add_r[0]_i_2__0_n_0 ;
  wire \add_r[0]_i_3__0_n_0 ;
  wire \add_r[0]_i_4__0_n_0 ;
  wire \add_r[0]_i_5__0_n_0 ;
  wire \add_r[12]_i_2__0_n_0 ;
  wire \add_r[12]_i_3__0_n_0 ;
  wire \add_r[12]_i_4__0_n_0 ;
  wire \add_r[12]_i_5__0_n_0 ;
  wire \add_r[16]_i_2__0_n_0 ;
  wire \add_r[16]_i_3__0_n_0 ;
  wire \add_r[16]_i_4__0_n_0 ;
  wire \add_r[16]_i_5__0_n_0 ;
  wire \add_r[20]_i_2__0_n_0 ;
  wire \add_r[20]_i_3__0_n_0 ;
  wire \add_r[20]_i_4__0_n_0 ;
  wire \add_r[20]_i_5__0_n_0 ;
  wire \add_r[24]_i_2__0_n_0 ;
  wire \add_r[24]_i_3__0_n_0 ;
  wire \add_r[24]_i_4__0_n_0 ;
  wire \add_r[24]_i_5__0_n_0 ;
  wire \add_r[28]_i_2__0_n_0 ;
  wire \add_r[28]_i_3__0_n_0 ;
  wire \add_r[28]_i_4__0_n_0 ;
  wire \add_r[28]_i_5__0_n_0 ;
  wire \add_r[32]_i_2__0_n_0 ;
  wire \add_r[32]_i_3__0_n_0 ;
  wire \add_r[32]_i_4__0_n_0 ;
  wire \add_r[32]_i_5__0_n_0 ;
  wire \add_r[36]_i_2__0_n_0 ;
  wire \add_r[36]_i_3__0_n_0 ;
  wire \add_r[36]_i_4__0_n_0 ;
  wire \add_r[36]_i_5__0_n_0 ;
  wire \add_r[40]_i_2__0_n_0 ;
  wire \add_r[40]_i_3__0_n_0 ;
  wire \add_r[40]_i_4__0_n_0 ;
  wire \add_r[40]_i_5__0_n_0 ;
  wire \add_r[44]_i_2__0_n_0 ;
  wire \add_r[44]_i_3__0_n_0 ;
  wire \add_r[44]_i_4__0_n_0 ;
  wire \add_r[44]_i_5__0_n_0 ;
  wire \add_r[4]_i_2__0_n_0 ;
  wire \add_r[4]_i_3__0_n_0 ;
  wire \add_r[4]_i_4__0_n_0 ;
  wire \add_r[4]_i_5__0_n_0 ;
  wire \add_r[8]_i_2__0_n_0 ;
  wire \add_r[8]_i_3__0_n_0 ;
  wire \add_r[8]_i_4__0_n_0 ;
  wire \add_r[8]_i_5__0_n_0 ;
  wire \add_r_reg[0]_i_1__0_n_0 ;
  wire \add_r_reg[0]_i_1__0_n_1 ;
  wire \add_r_reg[0]_i_1__0_n_2 ;
  wire \add_r_reg[0]_i_1__0_n_3 ;
  wire \add_r_reg[0]_i_1__0_n_4 ;
  wire \add_r_reg[0]_i_1__0_n_5 ;
  wire \add_r_reg[0]_i_1__0_n_6 ;
  wire \add_r_reg[0]_i_1__0_n_7 ;
  wire \add_r_reg[12]_i_1__0_n_0 ;
  wire \add_r_reg[12]_i_1__0_n_1 ;
  wire \add_r_reg[12]_i_1__0_n_2 ;
  wire \add_r_reg[12]_i_1__0_n_3 ;
  wire \add_r_reg[12]_i_1__0_n_4 ;
  wire \add_r_reg[12]_i_1__0_n_5 ;
  wire \add_r_reg[12]_i_1__0_n_6 ;
  wire \add_r_reg[12]_i_1__0_n_7 ;
  wire \add_r_reg[16]_i_1__0_n_0 ;
  wire \add_r_reg[16]_i_1__0_n_1 ;
  wire \add_r_reg[16]_i_1__0_n_2 ;
  wire \add_r_reg[16]_i_1__0_n_3 ;
  wire \add_r_reg[16]_i_1__0_n_4 ;
  wire \add_r_reg[16]_i_1__0_n_5 ;
  wire \add_r_reg[16]_i_1__0_n_6 ;
  wire \add_r_reg[16]_i_1__0_n_7 ;
  wire \add_r_reg[20]_i_1__0_n_0 ;
  wire \add_r_reg[20]_i_1__0_n_1 ;
  wire \add_r_reg[20]_i_1__0_n_2 ;
  wire \add_r_reg[20]_i_1__0_n_3 ;
  wire \add_r_reg[20]_i_1__0_n_4 ;
  wire \add_r_reg[20]_i_1__0_n_5 ;
  wire \add_r_reg[20]_i_1__0_n_6 ;
  wire \add_r_reg[20]_i_1__0_n_7 ;
  wire \add_r_reg[24]_i_1__0_n_0 ;
  wire \add_r_reg[24]_i_1__0_n_1 ;
  wire \add_r_reg[24]_i_1__0_n_2 ;
  wire \add_r_reg[24]_i_1__0_n_3 ;
  wire \add_r_reg[24]_i_1__0_n_4 ;
  wire \add_r_reg[24]_i_1__0_n_5 ;
  wire \add_r_reg[24]_i_1__0_n_6 ;
  wire \add_r_reg[24]_i_1__0_n_7 ;
  wire \add_r_reg[28]_i_1__0_n_0 ;
  wire \add_r_reg[28]_i_1__0_n_1 ;
  wire \add_r_reg[28]_i_1__0_n_2 ;
  wire \add_r_reg[28]_i_1__0_n_3 ;
  wire \add_r_reg[28]_i_1__0_n_4 ;
  wire \add_r_reg[28]_i_1__0_n_5 ;
  wire \add_r_reg[28]_i_1__0_n_6 ;
  wire \add_r_reg[28]_i_1__0_n_7 ;
  wire \add_r_reg[32]_i_1__0_n_0 ;
  wire \add_r_reg[32]_i_1__0_n_1 ;
  wire \add_r_reg[32]_i_1__0_n_2 ;
  wire \add_r_reg[32]_i_1__0_n_3 ;
  wire \add_r_reg[32]_i_1__0_n_4 ;
  wire \add_r_reg[32]_i_1__0_n_5 ;
  wire \add_r_reg[32]_i_1__0_n_6 ;
  wire \add_r_reg[32]_i_1__0_n_7 ;
  wire \add_r_reg[36]_i_1__0_n_0 ;
  wire \add_r_reg[36]_i_1__0_n_1 ;
  wire \add_r_reg[36]_i_1__0_n_2 ;
  wire \add_r_reg[36]_i_1__0_n_3 ;
  wire \add_r_reg[36]_i_1__0_n_4 ;
  wire \add_r_reg[36]_i_1__0_n_5 ;
  wire \add_r_reg[36]_i_1__0_n_6 ;
  wire \add_r_reg[36]_i_1__0_n_7 ;
  wire \add_r_reg[40]_i_1__0_n_0 ;
  wire \add_r_reg[40]_i_1__0_n_1 ;
  wire \add_r_reg[40]_i_1__0_n_2 ;
  wire \add_r_reg[40]_i_1__0_n_3 ;
  wire \add_r_reg[40]_i_1__0_n_4 ;
  wire \add_r_reg[40]_i_1__0_n_5 ;
  wire \add_r_reg[40]_i_1__0_n_6 ;
  wire \add_r_reg[40]_i_1__0_n_7 ;
  wire \add_r_reg[44]_i_1__0_n_1 ;
  wire \add_r_reg[44]_i_1__0_n_2 ;
  wire \add_r_reg[44]_i_1__0_n_3 ;
  wire \add_r_reg[44]_i_1__0_n_4 ;
  wire \add_r_reg[44]_i_1__0_n_5 ;
  wire \add_r_reg[44]_i_1__0_n_6 ;
  wire \add_r_reg[44]_i_1__0_n_7 ;
  wire \add_r_reg[4]_i_1__0_n_0 ;
  wire \add_r_reg[4]_i_1__0_n_1 ;
  wire \add_r_reg[4]_i_1__0_n_2 ;
  wire \add_r_reg[4]_i_1__0_n_3 ;
  wire \add_r_reg[4]_i_1__0_n_4 ;
  wire \add_r_reg[4]_i_1__0_n_5 ;
  wire \add_r_reg[4]_i_1__0_n_6 ;
  wire \add_r_reg[4]_i_1__0_n_7 ;
  wire \add_r_reg[8]_i_1__0_n_0 ;
  wire \add_r_reg[8]_i_1__0_n_1 ;
  wire \add_r_reg[8]_i_1__0_n_2 ;
  wire \add_r_reg[8]_i_1__0_n_3 ;
  wire \add_r_reg[8]_i_1__0_n_4 ;
  wire \add_r_reg[8]_i_1__0_n_5 ;
  wire \add_r_reg[8]_i_1__0_n_6 ;
  wire \add_r_reg[8]_i_1__0_n_7 ;
  wire [31:0]b_ram_out;
  wire clk;
  wire \counter[0]_i_1__3_n_0 ;
  wire \counter[1]_i_1_n_0 ;
  wire \counter[2]_i_1__2_n_0 ;
  wire \counter[3]_i_1__2_n_0 ;
  wire \counter[4]_i_1__1_n_0 ;
  wire \counter[5]_i_1__1_n_0 ;
  wire \counter[6]_i_1__0_n_0 ;
  wire \counter[7]_i_1__0_n_0 ;
  wire \counter[8]_i_2__1_n_0 ;
  wire \counter[8]_i_3__1_n_0 ;
  wire [0:0]\counter_reg[1]_0 ;
  wire \counter_reg_n_0_[0] ;
  wire \counter_reg_n_0_[1] ;
  wire \counter_reg_n_0_[2] ;
  wire \counter_reg_n_0_[3] ;
  wire \counter_reg_n_0_[4] ;
  wire \counter_reg_n_0_[5] ;
  wire \counter_reg_n_0_[6] ;
  wire \counter_reg_n_0_[7] ;
  wire \counter_reg_n_0_[8] ;
  wire dp_enable;
  wire mul_r0_n_100;
  wire mul_r0_n_101;
  wire mul_r0_n_102;
  wire mul_r0_n_103;
  wire mul_r0_n_104;
  wire mul_r0_n_105;
  wire mul_r0_n_106;
  wire mul_r0_n_107;
  wire mul_r0_n_108;
  wire mul_r0_n_109;
  wire mul_r0_n_110;
  wire mul_r0_n_111;
  wire mul_r0_n_112;
  wire mul_r0_n_113;
  wire mul_r0_n_114;
  wire mul_r0_n_115;
  wire mul_r0_n_116;
  wire mul_r0_n_117;
  wire mul_r0_n_118;
  wire mul_r0_n_119;
  wire mul_r0_n_120;
  wire mul_r0_n_121;
  wire mul_r0_n_122;
  wire mul_r0_n_123;
  wire mul_r0_n_124;
  wire mul_r0_n_125;
  wire mul_r0_n_126;
  wire mul_r0_n_127;
  wire mul_r0_n_128;
  wire mul_r0_n_129;
  wire mul_r0_n_130;
  wire mul_r0_n_131;
  wire mul_r0_n_132;
  wire mul_r0_n_133;
  wire mul_r0_n_134;
  wire mul_r0_n_135;
  wire mul_r0_n_136;
  wire mul_r0_n_137;
  wire mul_r0_n_138;
  wire mul_r0_n_139;
  wire mul_r0_n_140;
  wire mul_r0_n_141;
  wire mul_r0_n_142;
  wire mul_r0_n_143;
  wire mul_r0_n_144;
  wire mul_r0_n_145;
  wire mul_r0_n_146;
  wire mul_r0_n_147;
  wire mul_r0_n_148;
  wire mul_r0_n_149;
  wire mul_r0_n_150;
  wire mul_r0_n_151;
  wire mul_r0_n_152;
  wire mul_r0_n_153;
  wire mul_r0_n_58;
  wire mul_r0_n_59;
  wire mul_r0_n_60;
  wire mul_r0_n_61;
  wire mul_r0_n_62;
  wire mul_r0_n_63;
  wire mul_r0_n_64;
  wire mul_r0_n_65;
  wire mul_r0_n_66;
  wire mul_r0_n_67;
  wire mul_r0_n_68;
  wire mul_r0_n_69;
  wire mul_r0_n_70;
  wire mul_r0_n_71;
  wire mul_r0_n_72;
  wire mul_r0_n_73;
  wire mul_r0_n_74;
  wire mul_r0_n_75;
  wire mul_r0_n_76;
  wire mul_r0_n_77;
  wire mul_r0_n_78;
  wire mul_r0_n_79;
  wire mul_r0_n_80;
  wire mul_r0_n_81;
  wire mul_r0_n_82;
  wire mul_r0_n_83;
  wire mul_r0_n_84;
  wire mul_r0_n_85;
  wire mul_r0_n_86;
  wire mul_r0_n_87;
  wire mul_r0_n_88;
  wire mul_r0_n_89;
  wire mul_r0_n_90;
  wire mul_r0_n_91;
  wire mul_r0_n_92;
  wire mul_r0_n_93;
  wire mul_r0_n_94;
  wire mul_r0_n_95;
  wire mul_r0_n_96;
  wire mul_r0_n_97;
  wire mul_r0_n_98;
  wire mul_r0_n_99;
  wire mul_r_reg__0_n_58;
  wire mul_r_reg__0_n_59;
  wire mul_r_reg__0_n_60;
  wire mul_r_reg__0_n_61;
  wire mul_r_reg__0_n_62;
  wire mul_r_reg__0_n_63;
  wire mul_r_reg__0_n_64;
  wire mul_r_reg__0_n_65;
  wire mul_r_reg__0_n_66;
  wire mul_r_reg__0_n_67;
  wire mul_r_reg__0_n_68;
  wire mul_r_reg__0_n_69;
  wire mul_r_reg__0_n_70;
  wire mul_r_reg__0_n_71;
  wire mul_r_reg__0_n_72;
  wire mul_r_reg__0_n_73;
  wire mul_r_reg__0_n_74;
  wire [47:0]mul_r_reg__1;
  wire p_0_in__0;
  wire [0:0]\slv_regs_reg[0][0] ;
  wire [0:0]\slv_regs_reg[0][11] ;
  wire [0:0]\slv_regs_reg[0][12] ;
  wire [7:0]\slv_regs_reg[0][8] ;
  wire [3:0]NLW_add_r1_carry_O_UNCONNECTED;
  wire [3:1]NLW_add_r1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_add_r1_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_add_r_reg[44]_i_1__0_CO_UNCONNECTED ;
  wire NLW_mul_r0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r0_CARRYOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r_reg__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r_reg__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_r_reg__0_PCOUT_UNCONNECTED;

  CARRY4 add_r1_carry
       (.CI(1'b0),
        .CO({add_r1_carry_n_0,add_r1_carry_n_1,add_r1_carry_n_2,add_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry_O_UNCONNECTED[3:0]),
        .S({\slv_regs_reg[0][11] ,add_r1_carry_i_2__0_n_0,add_r1_carry_i_3__0_n_0,add_r1_carry_i_4__0_n_0}));
  CARRY4 add_r1_carry__0
       (.CI(add_r1_carry_n_0),
        .CO({NLW_add_r1_carry__0_CO_UNCONNECTED[3:1],\counter_reg[1]_0 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,O}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_2__0
       (.I0(\counter_reg_n_0_[7] ),
        .I1(\slv_regs_reg[0][8] [6]),
        .I2(\counter_reg_n_0_[6] ),
        .I3(\slv_regs_reg[0][8] [5]),
        .I4(\slv_regs_reg[0][8] [7]),
        .I5(\counter_reg_n_0_[8] ),
        .O(add_r1_carry_i_2__0_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_3__0
       (.I0(\counter_reg_n_0_[4] ),
        .I1(\slv_regs_reg[0][8] [3]),
        .I2(\counter_reg_n_0_[3] ),
        .I3(\slv_regs_reg[0][8] [2]),
        .I4(\slv_regs_reg[0][8] [4]),
        .I5(\counter_reg_n_0_[5] ),
        .O(add_r1_carry_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h0990000000000990)) 
    add_r1_carry_i_4__0
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\slv_regs_reg[0][8] [0]),
        .I2(\slv_regs_reg[0][0] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\slv_regs_reg[0][8] [1]),
        .I5(\counter_reg_n_0_[2] ),
        .O(add_r1_carry_i_4__0_n_0));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_2__0 
       (.I0(mul_r_reg__1[3]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[3]),
        .O(\add_r[0]_i_2__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_3__0 
       (.I0(mul_r_reg__1[2]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[2]),
        .O(\add_r[0]_i_3__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_4__0 
       (.I0(mul_r_reg__1[1]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[1]),
        .O(\add_r[0]_i_4__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_5__0 
       (.I0(mul_r_reg__1[0]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[0]),
        .O(\add_r[0]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_2__0 
       (.I0(mul_r_reg__1[15]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[15]),
        .O(\add_r[12]_i_2__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_3__0 
       (.I0(mul_r_reg__1[14]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[14]),
        .O(\add_r[12]_i_3__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_4__0 
       (.I0(mul_r_reg__1[13]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[13]),
        .O(\add_r[12]_i_4__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_5__0 
       (.I0(mul_r_reg__1[12]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[12]),
        .O(\add_r[12]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_2__0 
       (.I0(mul_r_reg__1[19]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[19]),
        .O(\add_r[16]_i_2__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_3__0 
       (.I0(mul_r_reg__1[18]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[18]),
        .O(\add_r[16]_i_3__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_4__0 
       (.I0(mul_r_reg__1[17]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[17]),
        .O(\add_r[16]_i_4__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_5__0 
       (.I0(mul_r_reg__1[16]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[16]),
        .O(\add_r[16]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_2__0 
       (.I0(mul_r_reg__1[23]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[23]),
        .O(\add_r[20]_i_2__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_3__0 
       (.I0(mul_r_reg__1[22]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[22]),
        .O(\add_r[20]_i_3__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_4__0 
       (.I0(mul_r_reg__1[21]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[21]),
        .O(\add_r[20]_i_4__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_5__0 
       (.I0(mul_r_reg__1[20]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[20]),
        .O(\add_r[20]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_2__0 
       (.I0(mul_r_reg__1[27]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[27]),
        .O(\add_r[24]_i_2__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_3__0 
       (.I0(mul_r_reg__1[26]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[26]),
        .O(\add_r[24]_i_3__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_4__0 
       (.I0(mul_r_reg__1[25]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[25]),
        .O(\add_r[24]_i_4__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_5__0 
       (.I0(mul_r_reg__1[24]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[24]),
        .O(\add_r[24]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_2__0 
       (.I0(mul_r_reg__1[31]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[31]),
        .O(\add_r[28]_i_2__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_3__0 
       (.I0(mul_r_reg__1[30]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[30]),
        .O(\add_r[28]_i_3__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_4__0 
       (.I0(mul_r_reg__1[29]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[29]),
        .O(\add_r[28]_i_4__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_5__0 
       (.I0(mul_r_reg__1[28]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[28]),
        .O(\add_r[28]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_2__0 
       (.I0(mul_r_reg__1[35]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[35]),
        .O(\add_r[32]_i_2__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_3__0 
       (.I0(mul_r_reg__1[34]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[34]),
        .O(\add_r[32]_i_3__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_4__0 
       (.I0(mul_r_reg__1[33]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[33]),
        .O(\add_r[32]_i_4__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_5__0 
       (.I0(mul_r_reg__1[32]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[32]),
        .O(\add_r[32]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_2__0 
       (.I0(mul_r_reg__1[39]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[39]),
        .O(\add_r[36]_i_2__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_3__0 
       (.I0(mul_r_reg__1[38]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[38]),
        .O(\add_r[36]_i_3__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_4__0 
       (.I0(mul_r_reg__1[37]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[37]),
        .O(\add_r[36]_i_4__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_5__0 
       (.I0(mul_r_reg__1[36]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[36]),
        .O(\add_r[36]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_2__0 
       (.I0(mul_r_reg__1[43]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[43]),
        .O(\add_r[40]_i_2__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_3__0 
       (.I0(mul_r_reg__1[42]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[42]),
        .O(\add_r[40]_i_3__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_4__0 
       (.I0(mul_r_reg__1[41]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[41]),
        .O(\add_r[40]_i_4__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_5__0 
       (.I0(mul_r_reg__1[40]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[40]),
        .O(\add_r[40]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_2__0 
       (.I0(D[47]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[47]),
        .O(\add_r[44]_i_2__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_3__0 
       (.I0(mul_r_reg__1[46]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[46]),
        .O(\add_r[44]_i_3__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_4__0 
       (.I0(mul_r_reg__1[45]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[45]),
        .O(\add_r[44]_i_4__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_5__0 
       (.I0(mul_r_reg__1[44]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[44]),
        .O(\add_r[44]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_2__0 
       (.I0(mul_r_reg__1[7]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[7]),
        .O(\add_r[4]_i_2__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_3__0 
       (.I0(mul_r_reg__1[6]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[6]),
        .O(\add_r[4]_i_3__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_4__0 
       (.I0(mul_r_reg__1[5]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[5]),
        .O(\add_r[4]_i_4__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_5__0 
       (.I0(mul_r_reg__1[4]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[4]),
        .O(\add_r[4]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_2__0 
       (.I0(mul_r_reg__1[11]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[11]),
        .O(\add_r[8]_i_2__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_3__0 
       (.I0(mul_r_reg__1[10]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[10]),
        .O(\add_r[8]_i_3__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_4__0 
       (.I0(mul_r_reg__1[9]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[9]),
        .O(\add_r[8]_i_4__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_5__0 
       (.I0(mul_r_reg__1[8]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[8]),
        .O(\add_r[8]_i_5__0_n_0 ));
  FDRE \add_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__0_n_7 ),
        .Q(D[0]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[0]_i_1__0 
       (.CI(1'b0),
        .CO({\add_r_reg[0]_i_1__0_n_0 ,\add_r_reg[0]_i_1__0_n_1 ,\add_r_reg[0]_i_1__0_n_2 ,\add_r_reg[0]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[3:0]),
        .O({\add_r_reg[0]_i_1__0_n_4 ,\add_r_reg[0]_i_1__0_n_5 ,\add_r_reg[0]_i_1__0_n_6 ,\add_r_reg[0]_i_1__0_n_7 }),
        .S({\add_r[0]_i_2__0_n_0 ,\add_r[0]_i_3__0_n_0 ,\add_r[0]_i_4__0_n_0 ,\add_r[0]_i_5__0_n_0 }));
  FDRE \add_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__0_n_5 ),
        .Q(D[10]),
        .R(p_0_in__0));
  FDRE \add_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__0_n_4 ),
        .Q(D[11]),
        .R(p_0_in__0));
  FDRE \add_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__0_n_7 ),
        .Q(D[12]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[12]_i_1__0 
       (.CI(\add_r_reg[8]_i_1__0_n_0 ),
        .CO({\add_r_reg[12]_i_1__0_n_0 ,\add_r_reg[12]_i_1__0_n_1 ,\add_r_reg[12]_i_1__0_n_2 ,\add_r_reg[12]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[15:12]),
        .O({\add_r_reg[12]_i_1__0_n_4 ,\add_r_reg[12]_i_1__0_n_5 ,\add_r_reg[12]_i_1__0_n_6 ,\add_r_reg[12]_i_1__0_n_7 }),
        .S({\add_r[12]_i_2__0_n_0 ,\add_r[12]_i_3__0_n_0 ,\add_r[12]_i_4__0_n_0 ,\add_r[12]_i_5__0_n_0 }));
  FDRE \add_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__0_n_6 ),
        .Q(D[13]),
        .R(p_0_in__0));
  FDRE \add_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__0_n_5 ),
        .Q(D[14]),
        .R(p_0_in__0));
  FDRE \add_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__0_n_4 ),
        .Q(D[15]),
        .R(p_0_in__0));
  FDRE \add_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__0_n_7 ),
        .Q(D[16]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[16]_i_1__0 
       (.CI(\add_r_reg[12]_i_1__0_n_0 ),
        .CO({\add_r_reg[16]_i_1__0_n_0 ,\add_r_reg[16]_i_1__0_n_1 ,\add_r_reg[16]_i_1__0_n_2 ,\add_r_reg[16]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[19:16]),
        .O({\add_r_reg[16]_i_1__0_n_4 ,\add_r_reg[16]_i_1__0_n_5 ,\add_r_reg[16]_i_1__0_n_6 ,\add_r_reg[16]_i_1__0_n_7 }),
        .S({\add_r[16]_i_2__0_n_0 ,\add_r[16]_i_3__0_n_0 ,\add_r[16]_i_4__0_n_0 ,\add_r[16]_i_5__0_n_0 }));
  FDRE \add_r_reg[17] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__0_n_6 ),
        .Q(D[17]),
        .R(p_0_in__0));
  FDRE \add_r_reg[18] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__0_n_5 ),
        .Q(D[18]),
        .R(p_0_in__0));
  FDRE \add_r_reg[19] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__0_n_4 ),
        .Q(D[19]),
        .R(p_0_in__0));
  FDRE \add_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__0_n_6 ),
        .Q(D[1]),
        .R(p_0_in__0));
  FDRE \add_r_reg[20] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__0_n_7 ),
        .Q(D[20]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[20]_i_1__0 
       (.CI(\add_r_reg[16]_i_1__0_n_0 ),
        .CO({\add_r_reg[20]_i_1__0_n_0 ,\add_r_reg[20]_i_1__0_n_1 ,\add_r_reg[20]_i_1__0_n_2 ,\add_r_reg[20]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[23:20]),
        .O({\add_r_reg[20]_i_1__0_n_4 ,\add_r_reg[20]_i_1__0_n_5 ,\add_r_reg[20]_i_1__0_n_6 ,\add_r_reg[20]_i_1__0_n_7 }),
        .S({\add_r[20]_i_2__0_n_0 ,\add_r[20]_i_3__0_n_0 ,\add_r[20]_i_4__0_n_0 ,\add_r[20]_i_5__0_n_0 }));
  FDRE \add_r_reg[21] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__0_n_6 ),
        .Q(D[21]),
        .R(p_0_in__0));
  FDRE \add_r_reg[22] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__0_n_5 ),
        .Q(D[22]),
        .R(p_0_in__0));
  FDRE \add_r_reg[23] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__0_n_4 ),
        .Q(D[23]),
        .R(p_0_in__0));
  FDRE \add_r_reg[24] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__0_n_7 ),
        .Q(D[24]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[24]_i_1__0 
       (.CI(\add_r_reg[20]_i_1__0_n_0 ),
        .CO({\add_r_reg[24]_i_1__0_n_0 ,\add_r_reg[24]_i_1__0_n_1 ,\add_r_reg[24]_i_1__0_n_2 ,\add_r_reg[24]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[27:24]),
        .O({\add_r_reg[24]_i_1__0_n_4 ,\add_r_reg[24]_i_1__0_n_5 ,\add_r_reg[24]_i_1__0_n_6 ,\add_r_reg[24]_i_1__0_n_7 }),
        .S({\add_r[24]_i_2__0_n_0 ,\add_r[24]_i_3__0_n_0 ,\add_r[24]_i_4__0_n_0 ,\add_r[24]_i_5__0_n_0 }));
  FDRE \add_r_reg[25] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__0_n_6 ),
        .Q(D[25]),
        .R(p_0_in__0));
  FDRE \add_r_reg[26] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__0_n_5 ),
        .Q(D[26]),
        .R(p_0_in__0));
  FDRE \add_r_reg[27] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__0_n_4 ),
        .Q(D[27]),
        .R(p_0_in__0));
  FDRE \add_r_reg[28] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__0_n_7 ),
        .Q(D[28]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[28]_i_1__0 
       (.CI(\add_r_reg[24]_i_1__0_n_0 ),
        .CO({\add_r_reg[28]_i_1__0_n_0 ,\add_r_reg[28]_i_1__0_n_1 ,\add_r_reg[28]_i_1__0_n_2 ,\add_r_reg[28]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[31:28]),
        .O({\add_r_reg[28]_i_1__0_n_4 ,\add_r_reg[28]_i_1__0_n_5 ,\add_r_reg[28]_i_1__0_n_6 ,\add_r_reg[28]_i_1__0_n_7 }),
        .S({\add_r[28]_i_2__0_n_0 ,\add_r[28]_i_3__0_n_0 ,\add_r[28]_i_4__0_n_0 ,\add_r[28]_i_5__0_n_0 }));
  FDRE \add_r_reg[29] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__0_n_6 ),
        .Q(D[29]),
        .R(p_0_in__0));
  FDRE \add_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__0_n_5 ),
        .Q(D[2]),
        .R(p_0_in__0));
  FDRE \add_r_reg[30] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__0_n_5 ),
        .Q(D[30]),
        .R(p_0_in__0));
  FDRE \add_r_reg[31] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__0_n_4 ),
        .Q(D[31]),
        .R(p_0_in__0));
  FDRE \add_r_reg[32] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__0_n_7 ),
        .Q(D[32]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[32]_i_1__0 
       (.CI(\add_r_reg[28]_i_1__0_n_0 ),
        .CO({\add_r_reg[32]_i_1__0_n_0 ,\add_r_reg[32]_i_1__0_n_1 ,\add_r_reg[32]_i_1__0_n_2 ,\add_r_reg[32]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[35:32]),
        .O({\add_r_reg[32]_i_1__0_n_4 ,\add_r_reg[32]_i_1__0_n_5 ,\add_r_reg[32]_i_1__0_n_6 ,\add_r_reg[32]_i_1__0_n_7 }),
        .S({\add_r[32]_i_2__0_n_0 ,\add_r[32]_i_3__0_n_0 ,\add_r[32]_i_4__0_n_0 ,\add_r[32]_i_5__0_n_0 }));
  FDRE \add_r_reg[33] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__0_n_6 ),
        .Q(D[33]),
        .R(p_0_in__0));
  FDRE \add_r_reg[34] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__0_n_5 ),
        .Q(D[34]),
        .R(p_0_in__0));
  FDRE \add_r_reg[35] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__0_n_4 ),
        .Q(D[35]),
        .R(p_0_in__0));
  FDRE \add_r_reg[36] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__0_n_7 ),
        .Q(D[36]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[36]_i_1__0 
       (.CI(\add_r_reg[32]_i_1__0_n_0 ),
        .CO({\add_r_reg[36]_i_1__0_n_0 ,\add_r_reg[36]_i_1__0_n_1 ,\add_r_reg[36]_i_1__0_n_2 ,\add_r_reg[36]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[39:36]),
        .O({\add_r_reg[36]_i_1__0_n_4 ,\add_r_reg[36]_i_1__0_n_5 ,\add_r_reg[36]_i_1__0_n_6 ,\add_r_reg[36]_i_1__0_n_7 }),
        .S({\add_r[36]_i_2__0_n_0 ,\add_r[36]_i_3__0_n_0 ,\add_r[36]_i_4__0_n_0 ,\add_r[36]_i_5__0_n_0 }));
  FDRE \add_r_reg[37] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__0_n_6 ),
        .Q(D[37]),
        .R(p_0_in__0));
  FDRE \add_r_reg[38] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__0_n_5 ),
        .Q(D[38]),
        .R(p_0_in__0));
  FDRE \add_r_reg[39] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__0_n_4 ),
        .Q(D[39]),
        .R(p_0_in__0));
  FDRE \add_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__0_n_4 ),
        .Q(D[3]),
        .R(p_0_in__0));
  FDRE \add_r_reg[40] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__0_n_7 ),
        .Q(D[40]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[40]_i_1__0 
       (.CI(\add_r_reg[36]_i_1__0_n_0 ),
        .CO({\add_r_reg[40]_i_1__0_n_0 ,\add_r_reg[40]_i_1__0_n_1 ,\add_r_reg[40]_i_1__0_n_2 ,\add_r_reg[40]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[43:40]),
        .O({\add_r_reg[40]_i_1__0_n_4 ,\add_r_reg[40]_i_1__0_n_5 ,\add_r_reg[40]_i_1__0_n_6 ,\add_r_reg[40]_i_1__0_n_7 }),
        .S({\add_r[40]_i_2__0_n_0 ,\add_r[40]_i_3__0_n_0 ,\add_r[40]_i_4__0_n_0 ,\add_r[40]_i_5__0_n_0 }));
  FDRE \add_r_reg[41] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__0_n_6 ),
        .Q(D[41]),
        .R(p_0_in__0));
  FDRE \add_r_reg[42] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__0_n_5 ),
        .Q(D[42]),
        .R(p_0_in__0));
  FDRE \add_r_reg[43] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__0_n_4 ),
        .Q(D[43]),
        .R(p_0_in__0));
  FDRE \add_r_reg[44] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__0_n_7 ),
        .Q(D[44]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[44]_i_1__0 
       (.CI(\add_r_reg[40]_i_1__0_n_0 ),
        .CO({\NLW_add_r_reg[44]_i_1__0_CO_UNCONNECTED [3],\add_r_reg[44]_i_1__0_n_1 ,\add_r_reg[44]_i_1__0_n_2 ,\add_r_reg[44]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,mul_r_reg__1[46:44]}),
        .O({\add_r_reg[44]_i_1__0_n_4 ,\add_r_reg[44]_i_1__0_n_5 ,\add_r_reg[44]_i_1__0_n_6 ,\add_r_reg[44]_i_1__0_n_7 }),
        .S({\add_r[44]_i_2__0_n_0 ,\add_r[44]_i_3__0_n_0 ,\add_r[44]_i_4__0_n_0 ,\add_r[44]_i_5__0_n_0 }));
  FDRE \add_r_reg[45] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__0_n_6 ),
        .Q(D[45]),
        .R(p_0_in__0));
  FDRE \add_r_reg[46] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__0_n_5 ),
        .Q(D[46]),
        .R(p_0_in__0));
  FDRE \add_r_reg[47] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__0_n_4 ),
        .Q(D[47]),
        .R(p_0_in__0));
  FDRE \add_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__0_n_7 ),
        .Q(D[4]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[4]_i_1__0 
       (.CI(\add_r_reg[0]_i_1__0_n_0 ),
        .CO({\add_r_reg[4]_i_1__0_n_0 ,\add_r_reg[4]_i_1__0_n_1 ,\add_r_reg[4]_i_1__0_n_2 ,\add_r_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[7:4]),
        .O({\add_r_reg[4]_i_1__0_n_4 ,\add_r_reg[4]_i_1__0_n_5 ,\add_r_reg[4]_i_1__0_n_6 ,\add_r_reg[4]_i_1__0_n_7 }),
        .S({\add_r[4]_i_2__0_n_0 ,\add_r[4]_i_3__0_n_0 ,\add_r[4]_i_4__0_n_0 ,\add_r[4]_i_5__0_n_0 }));
  FDRE \add_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__0_n_6 ),
        .Q(D[5]),
        .R(p_0_in__0));
  FDRE \add_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__0_n_5 ),
        .Q(D[6]),
        .R(p_0_in__0));
  FDRE \add_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__0_n_4 ),
        .Q(D[7]),
        .R(p_0_in__0));
  FDRE \add_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__0_n_7 ),
        .Q(D[8]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[8]_i_1__0 
       (.CI(\add_r_reg[4]_i_1__0_n_0 ),
        .CO({\add_r_reg[8]_i_1__0_n_0 ,\add_r_reg[8]_i_1__0_n_1 ,\add_r_reg[8]_i_1__0_n_2 ,\add_r_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[11:8]),
        .O({\add_r_reg[8]_i_1__0_n_4 ,\add_r_reg[8]_i_1__0_n_5 ,\add_r_reg[8]_i_1__0_n_6 ,\add_r_reg[8]_i_1__0_n_7 }),
        .S({\add_r[8]_i_2__0_n_0 ,\add_r[8]_i_3__0_n_0 ,\add_r[8]_i_4__0_n_0 ,\add_r[8]_i_5__0_n_0 }));
  FDRE \add_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__0_n_6 ),
        .Q(D[9]),
        .R(p_0_in__0));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter[0]_i_1__3 
       (.I0(\counter_reg_n_0_[0] ),
        .O(\counter[0]_i_1__3_n_0 ));
  LUT4 #(
    .INIT(16'hF7C8)) 
    \counter[1]_i_1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(dp_enable),
        .I2(\counter_reg[1]_0 ),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \counter[2]_i_1__2 
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[2] ),
        .O(\counter[2]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \counter[3]_i_1__2 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .O(\counter[3]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \counter[4]_i_1__1 
       (.I0(\counter_reg_n_0_[3] ),
        .I1(\counter_reg_n_0_[2] ),
        .I2(\counter_reg_n_0_[1] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[4] ),
        .O(\counter[4]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \counter[5]_i_1__1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .I4(\counter_reg_n_0_[4] ),
        .I5(\counter_reg_n_0_[5] ),
        .O(\counter[5]_i_1__1_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \counter[6]_i_1__0 
       (.I0(\counter[8]_i_3__1_n_0 ),
        .I1(\counter_reg_n_0_[6] ),
        .O(\counter[6]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hB4)) 
    \counter[7]_i_1__0 
       (.I0(\counter[8]_i_3__1_n_0 ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(\counter_reg_n_0_[7] ),
        .O(\counter[7]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT4 #(
    .INIT(16'hF708)) 
    \counter[8]_i_2__1 
       (.I0(\counter_reg_n_0_[7] ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(\counter[8]_i_3__1_n_0 ),
        .I3(\counter_reg_n_0_[8] ),
        .O(\counter[8]_i_2__1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \counter[8]_i_3__1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .I4(\counter_reg_n_0_[4] ),
        .I5(\counter_reg_n_0_[5] ),
        .O(\counter[8]_i_3__1_n_0 ));
  FDRE \counter_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[0]_i_1__3_n_0 ),
        .Q(\counter_reg_n_0_[0] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\counter[1]_i_1_n_0 ),
        .Q(\counter_reg_n_0_[1] ),
        .R(p_0_in__0));
  FDRE \counter_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[2]_i_1__2_n_0 ),
        .Q(\counter_reg_n_0_[2] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[3]_i_1__2_n_0 ),
        .Q(\counter_reg_n_0_[3] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[4]_i_1__1_n_0 ),
        .Q(\counter_reg_n_0_[4] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[5]_i_1__1_n_0 ),
        .Q(\counter_reg_n_0_[5] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[6]_i_1__0_n_0 ),
        .Q(\counter_reg_n_0_[6] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[7]_i_1__0_n_0 ),
        .Q(\counter_reg_n_0_[7] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[8]_i_2__1_n_0 ),
        .Q(\counter_reg_n_0_[8] ),
        .R(\slv_regs_reg[0][12] ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,b_ram_out[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({A[15],A[15],A}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r0_OVERFLOW_UNCONNECTED),
        .P({mul_r0_n_58,mul_r0_n_59,mul_r0_n_60,mul_r0_n_61,mul_r0_n_62,mul_r0_n_63,mul_r0_n_64,mul_r0_n_65,mul_r0_n_66,mul_r0_n_67,mul_r0_n_68,mul_r0_n_69,mul_r0_n_70,mul_r0_n_71,mul_r0_n_72,mul_r0_n_73,mul_r0_n_74,mul_r0_n_75,mul_r0_n_76,mul_r0_n_77,mul_r0_n_78,mul_r0_n_79,mul_r0_n_80,mul_r0_n_81,mul_r0_n_82,mul_r0_n_83,mul_r0_n_84,mul_r0_n_85,mul_r0_n_86,mul_r0_n_87,mul_r0_n_88,mul_r0_n_89,mul_r0_n_90,mul_r0_n_91,mul_r0_n_92,mul_r0_n_93,mul_r0_n_94,mul_r0_n_95,mul_r0_n_96,mul_r0_n_97,mul_r0_n_98,mul_r0_n_99,mul_r0_n_100,mul_r0_n_101,mul_r0_n_102,mul_r0_n_103,mul_r0_n_104,mul_r0_n_105}),
        .PATTERNBDETECT(NLW_mul_r0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_r0_UNDERFLOW_UNCONNECTED));
  FDRE \mul_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_105),
        .Q(mul_r_reg__1[0]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_95),
        .Q(mul_r_reg__1[10]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_94),
        .Q(mul_r_reg__1[11]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_93),
        .Q(mul_r_reg__1[12]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_92),
        .Q(mul_r_reg__1[13]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_91),
        .Q(mul_r_reg__1[14]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_90),
        .Q(mul_r_reg__1[15]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_89),
        .Q(mul_r_reg__1[16]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_104),
        .Q(mul_r_reg__1[1]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_103),
        .Q(mul_r_reg__1[2]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_102),
        .Q(mul_r_reg__1[3]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_101),
        .Q(mul_r_reg__1[4]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_100),
        .Q(mul_r_reg__1[5]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_99),
        .Q(mul_r_reg__1[6]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_98),
        .Q(mul_r_reg__1[7]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_97),
        .Q(mul_r_reg__1[8]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_96),
        .Q(mul_r_reg__1[9]),
        .R(p_0_in__0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-12 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r_reg__0
       (.A({A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r_reg__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({b_ram_out[31],b_ram_out[31],b_ram_out[31],b_ram_out[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r_reg__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(dp_enable),
        .CLK(clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED),
        .P({mul_r_reg__0_n_58,mul_r_reg__0_n_59,mul_r_reg__0_n_60,mul_r_reg__0_n_61,mul_r_reg__0_n_62,mul_r_reg__0_n_63,mul_r_reg__0_n_64,mul_r_reg__0_n_65,mul_r_reg__0_n_66,mul_r_reg__0_n_67,mul_r_reg__0_n_68,mul_r_reg__0_n_69,mul_r_reg__0_n_70,mul_r_reg__0_n_71,mul_r_reg__0_n_72,mul_r_reg__0_n_73,mul_r_reg__0_n_74,mul_r_reg__1[47:17]}),
        .PATTERNBDETECT(NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .PCOUT(NLW_mul_r_reg__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(p_0_in__0),
        .UNDERFLOW(NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "dot_product" *) 
module design_1_top_0_0_dot_product_1
   (D,
    \counter_reg[1]_0 ,
    A,
    b_ram_out,
    dp_enable,
    clk,
    p_0_in__0,
    \slv_regs_reg[0][11] ,
    O,
    \slv_regs_reg[0][8] ,
    \slv_regs_reg[0][0] ,
    \slv_regs_reg[0][12] );
  output [47:0]D;
  output [0:0]\counter_reg[1]_0 ;
  input [16:0]A;
  input [31:0]b_ram_out;
  input dp_enable;
  input clk;
  input p_0_in__0;
  input [0:0]\slv_regs_reg[0][11] ;
  input [0:0]O;
  input [7:0]\slv_regs_reg[0][8] ;
  input [0:0]\slv_regs_reg[0][0] ;
  input [0:0]\slv_regs_reg[0][12] ;

  wire [16:0]A;
  wire [47:0]D;
  wire [0:0]O;
  wire add_r1_carry_i_2__1_n_0;
  wire add_r1_carry_i_3__1_n_0;
  wire add_r1_carry_i_4__1_n_0;
  wire add_r1_carry_n_0;
  wire add_r1_carry_n_1;
  wire add_r1_carry_n_2;
  wire add_r1_carry_n_3;
  wire \add_r[0]_i_2__1_n_0 ;
  wire \add_r[0]_i_3__1_n_0 ;
  wire \add_r[0]_i_4__1_n_0 ;
  wire \add_r[0]_i_5__1_n_0 ;
  wire \add_r[12]_i_2__1_n_0 ;
  wire \add_r[12]_i_3__1_n_0 ;
  wire \add_r[12]_i_4__1_n_0 ;
  wire \add_r[12]_i_5__1_n_0 ;
  wire \add_r[16]_i_2__1_n_0 ;
  wire \add_r[16]_i_3__1_n_0 ;
  wire \add_r[16]_i_4__1_n_0 ;
  wire \add_r[16]_i_5__1_n_0 ;
  wire \add_r[20]_i_2__1_n_0 ;
  wire \add_r[20]_i_3__1_n_0 ;
  wire \add_r[20]_i_4__1_n_0 ;
  wire \add_r[20]_i_5__1_n_0 ;
  wire \add_r[24]_i_2__1_n_0 ;
  wire \add_r[24]_i_3__1_n_0 ;
  wire \add_r[24]_i_4__1_n_0 ;
  wire \add_r[24]_i_5__1_n_0 ;
  wire \add_r[28]_i_2__1_n_0 ;
  wire \add_r[28]_i_3__1_n_0 ;
  wire \add_r[28]_i_4__1_n_0 ;
  wire \add_r[28]_i_5__1_n_0 ;
  wire \add_r[32]_i_2__1_n_0 ;
  wire \add_r[32]_i_3__1_n_0 ;
  wire \add_r[32]_i_4__1_n_0 ;
  wire \add_r[32]_i_5__1_n_0 ;
  wire \add_r[36]_i_2__1_n_0 ;
  wire \add_r[36]_i_3__1_n_0 ;
  wire \add_r[36]_i_4__1_n_0 ;
  wire \add_r[36]_i_5__1_n_0 ;
  wire \add_r[40]_i_2__1_n_0 ;
  wire \add_r[40]_i_3__1_n_0 ;
  wire \add_r[40]_i_4__1_n_0 ;
  wire \add_r[40]_i_5__1_n_0 ;
  wire \add_r[44]_i_2__1_n_0 ;
  wire \add_r[44]_i_3__1_n_0 ;
  wire \add_r[44]_i_4__1_n_0 ;
  wire \add_r[44]_i_5__1_n_0 ;
  wire \add_r[4]_i_2__1_n_0 ;
  wire \add_r[4]_i_3__1_n_0 ;
  wire \add_r[4]_i_4__1_n_0 ;
  wire \add_r[4]_i_5__1_n_0 ;
  wire \add_r[8]_i_2__1_n_0 ;
  wire \add_r[8]_i_3__1_n_0 ;
  wire \add_r[8]_i_4__1_n_0 ;
  wire \add_r[8]_i_5__1_n_0 ;
  wire \add_r_reg[0]_i_1__1_n_0 ;
  wire \add_r_reg[0]_i_1__1_n_1 ;
  wire \add_r_reg[0]_i_1__1_n_2 ;
  wire \add_r_reg[0]_i_1__1_n_3 ;
  wire \add_r_reg[0]_i_1__1_n_4 ;
  wire \add_r_reg[0]_i_1__1_n_5 ;
  wire \add_r_reg[0]_i_1__1_n_6 ;
  wire \add_r_reg[0]_i_1__1_n_7 ;
  wire \add_r_reg[12]_i_1__1_n_0 ;
  wire \add_r_reg[12]_i_1__1_n_1 ;
  wire \add_r_reg[12]_i_1__1_n_2 ;
  wire \add_r_reg[12]_i_1__1_n_3 ;
  wire \add_r_reg[12]_i_1__1_n_4 ;
  wire \add_r_reg[12]_i_1__1_n_5 ;
  wire \add_r_reg[12]_i_1__1_n_6 ;
  wire \add_r_reg[12]_i_1__1_n_7 ;
  wire \add_r_reg[16]_i_1__1_n_0 ;
  wire \add_r_reg[16]_i_1__1_n_1 ;
  wire \add_r_reg[16]_i_1__1_n_2 ;
  wire \add_r_reg[16]_i_1__1_n_3 ;
  wire \add_r_reg[16]_i_1__1_n_4 ;
  wire \add_r_reg[16]_i_1__1_n_5 ;
  wire \add_r_reg[16]_i_1__1_n_6 ;
  wire \add_r_reg[16]_i_1__1_n_7 ;
  wire \add_r_reg[20]_i_1__1_n_0 ;
  wire \add_r_reg[20]_i_1__1_n_1 ;
  wire \add_r_reg[20]_i_1__1_n_2 ;
  wire \add_r_reg[20]_i_1__1_n_3 ;
  wire \add_r_reg[20]_i_1__1_n_4 ;
  wire \add_r_reg[20]_i_1__1_n_5 ;
  wire \add_r_reg[20]_i_1__1_n_6 ;
  wire \add_r_reg[20]_i_1__1_n_7 ;
  wire \add_r_reg[24]_i_1__1_n_0 ;
  wire \add_r_reg[24]_i_1__1_n_1 ;
  wire \add_r_reg[24]_i_1__1_n_2 ;
  wire \add_r_reg[24]_i_1__1_n_3 ;
  wire \add_r_reg[24]_i_1__1_n_4 ;
  wire \add_r_reg[24]_i_1__1_n_5 ;
  wire \add_r_reg[24]_i_1__1_n_6 ;
  wire \add_r_reg[24]_i_1__1_n_7 ;
  wire \add_r_reg[28]_i_1__1_n_0 ;
  wire \add_r_reg[28]_i_1__1_n_1 ;
  wire \add_r_reg[28]_i_1__1_n_2 ;
  wire \add_r_reg[28]_i_1__1_n_3 ;
  wire \add_r_reg[28]_i_1__1_n_4 ;
  wire \add_r_reg[28]_i_1__1_n_5 ;
  wire \add_r_reg[28]_i_1__1_n_6 ;
  wire \add_r_reg[28]_i_1__1_n_7 ;
  wire \add_r_reg[32]_i_1__1_n_0 ;
  wire \add_r_reg[32]_i_1__1_n_1 ;
  wire \add_r_reg[32]_i_1__1_n_2 ;
  wire \add_r_reg[32]_i_1__1_n_3 ;
  wire \add_r_reg[32]_i_1__1_n_4 ;
  wire \add_r_reg[32]_i_1__1_n_5 ;
  wire \add_r_reg[32]_i_1__1_n_6 ;
  wire \add_r_reg[32]_i_1__1_n_7 ;
  wire \add_r_reg[36]_i_1__1_n_0 ;
  wire \add_r_reg[36]_i_1__1_n_1 ;
  wire \add_r_reg[36]_i_1__1_n_2 ;
  wire \add_r_reg[36]_i_1__1_n_3 ;
  wire \add_r_reg[36]_i_1__1_n_4 ;
  wire \add_r_reg[36]_i_1__1_n_5 ;
  wire \add_r_reg[36]_i_1__1_n_6 ;
  wire \add_r_reg[36]_i_1__1_n_7 ;
  wire \add_r_reg[40]_i_1__1_n_0 ;
  wire \add_r_reg[40]_i_1__1_n_1 ;
  wire \add_r_reg[40]_i_1__1_n_2 ;
  wire \add_r_reg[40]_i_1__1_n_3 ;
  wire \add_r_reg[40]_i_1__1_n_4 ;
  wire \add_r_reg[40]_i_1__1_n_5 ;
  wire \add_r_reg[40]_i_1__1_n_6 ;
  wire \add_r_reg[40]_i_1__1_n_7 ;
  wire \add_r_reg[44]_i_1__1_n_1 ;
  wire \add_r_reg[44]_i_1__1_n_2 ;
  wire \add_r_reg[44]_i_1__1_n_3 ;
  wire \add_r_reg[44]_i_1__1_n_4 ;
  wire \add_r_reg[44]_i_1__1_n_5 ;
  wire \add_r_reg[44]_i_1__1_n_6 ;
  wire \add_r_reg[44]_i_1__1_n_7 ;
  wire \add_r_reg[4]_i_1__1_n_0 ;
  wire \add_r_reg[4]_i_1__1_n_1 ;
  wire \add_r_reg[4]_i_1__1_n_2 ;
  wire \add_r_reg[4]_i_1__1_n_3 ;
  wire \add_r_reg[4]_i_1__1_n_4 ;
  wire \add_r_reg[4]_i_1__1_n_5 ;
  wire \add_r_reg[4]_i_1__1_n_6 ;
  wire \add_r_reg[4]_i_1__1_n_7 ;
  wire \add_r_reg[8]_i_1__1_n_0 ;
  wire \add_r_reg[8]_i_1__1_n_1 ;
  wire \add_r_reg[8]_i_1__1_n_2 ;
  wire \add_r_reg[8]_i_1__1_n_3 ;
  wire \add_r_reg[8]_i_1__1_n_4 ;
  wire \add_r_reg[8]_i_1__1_n_5 ;
  wire \add_r_reg[8]_i_1__1_n_6 ;
  wire \add_r_reg[8]_i_1__1_n_7 ;
  wire [31:0]b_ram_out;
  wire clk;
  wire \counter[0]_i_1__4_n_0 ;
  wire \counter[1]_i_1_n_0 ;
  wire \counter[2]_i_1__3_n_0 ;
  wire \counter[3]_i_1__3_n_0 ;
  wire \counter[4]_i_1__2_n_0 ;
  wire \counter[5]_i_1__2_n_0 ;
  wire \counter[6]_i_1__1_n_0 ;
  wire \counter[7]_i_1__1_n_0 ;
  wire \counter[8]_i_2__2_n_0 ;
  wire \counter[8]_i_3__2_n_0 ;
  wire [0:0]\counter_reg[1]_0 ;
  wire \counter_reg_n_0_[0] ;
  wire \counter_reg_n_0_[1] ;
  wire \counter_reg_n_0_[2] ;
  wire \counter_reg_n_0_[3] ;
  wire \counter_reg_n_0_[4] ;
  wire \counter_reg_n_0_[5] ;
  wire \counter_reg_n_0_[6] ;
  wire \counter_reg_n_0_[7] ;
  wire \counter_reg_n_0_[8] ;
  wire dp_enable;
  wire mul_r0_n_100;
  wire mul_r0_n_101;
  wire mul_r0_n_102;
  wire mul_r0_n_103;
  wire mul_r0_n_104;
  wire mul_r0_n_105;
  wire mul_r0_n_106;
  wire mul_r0_n_107;
  wire mul_r0_n_108;
  wire mul_r0_n_109;
  wire mul_r0_n_110;
  wire mul_r0_n_111;
  wire mul_r0_n_112;
  wire mul_r0_n_113;
  wire mul_r0_n_114;
  wire mul_r0_n_115;
  wire mul_r0_n_116;
  wire mul_r0_n_117;
  wire mul_r0_n_118;
  wire mul_r0_n_119;
  wire mul_r0_n_120;
  wire mul_r0_n_121;
  wire mul_r0_n_122;
  wire mul_r0_n_123;
  wire mul_r0_n_124;
  wire mul_r0_n_125;
  wire mul_r0_n_126;
  wire mul_r0_n_127;
  wire mul_r0_n_128;
  wire mul_r0_n_129;
  wire mul_r0_n_130;
  wire mul_r0_n_131;
  wire mul_r0_n_132;
  wire mul_r0_n_133;
  wire mul_r0_n_134;
  wire mul_r0_n_135;
  wire mul_r0_n_136;
  wire mul_r0_n_137;
  wire mul_r0_n_138;
  wire mul_r0_n_139;
  wire mul_r0_n_140;
  wire mul_r0_n_141;
  wire mul_r0_n_142;
  wire mul_r0_n_143;
  wire mul_r0_n_144;
  wire mul_r0_n_145;
  wire mul_r0_n_146;
  wire mul_r0_n_147;
  wire mul_r0_n_148;
  wire mul_r0_n_149;
  wire mul_r0_n_150;
  wire mul_r0_n_151;
  wire mul_r0_n_152;
  wire mul_r0_n_153;
  wire mul_r0_n_58;
  wire mul_r0_n_59;
  wire mul_r0_n_60;
  wire mul_r0_n_61;
  wire mul_r0_n_62;
  wire mul_r0_n_63;
  wire mul_r0_n_64;
  wire mul_r0_n_65;
  wire mul_r0_n_66;
  wire mul_r0_n_67;
  wire mul_r0_n_68;
  wire mul_r0_n_69;
  wire mul_r0_n_70;
  wire mul_r0_n_71;
  wire mul_r0_n_72;
  wire mul_r0_n_73;
  wire mul_r0_n_74;
  wire mul_r0_n_75;
  wire mul_r0_n_76;
  wire mul_r0_n_77;
  wire mul_r0_n_78;
  wire mul_r0_n_79;
  wire mul_r0_n_80;
  wire mul_r0_n_81;
  wire mul_r0_n_82;
  wire mul_r0_n_83;
  wire mul_r0_n_84;
  wire mul_r0_n_85;
  wire mul_r0_n_86;
  wire mul_r0_n_87;
  wire mul_r0_n_88;
  wire mul_r0_n_89;
  wire mul_r0_n_90;
  wire mul_r0_n_91;
  wire mul_r0_n_92;
  wire mul_r0_n_93;
  wire mul_r0_n_94;
  wire mul_r0_n_95;
  wire mul_r0_n_96;
  wire mul_r0_n_97;
  wire mul_r0_n_98;
  wire mul_r0_n_99;
  wire mul_r_reg__0_n_58;
  wire mul_r_reg__0_n_59;
  wire mul_r_reg__0_n_60;
  wire mul_r_reg__0_n_61;
  wire mul_r_reg__0_n_62;
  wire mul_r_reg__0_n_63;
  wire mul_r_reg__0_n_64;
  wire mul_r_reg__0_n_65;
  wire mul_r_reg__0_n_66;
  wire mul_r_reg__0_n_67;
  wire mul_r_reg__0_n_68;
  wire mul_r_reg__0_n_69;
  wire mul_r_reg__0_n_70;
  wire mul_r_reg__0_n_71;
  wire mul_r_reg__0_n_72;
  wire mul_r_reg__0_n_73;
  wire mul_r_reg__0_n_74;
  wire [47:0]mul_r_reg__1;
  wire p_0_in__0;
  wire [0:0]\slv_regs_reg[0][0] ;
  wire [0:0]\slv_regs_reg[0][11] ;
  wire [0:0]\slv_regs_reg[0][12] ;
  wire [7:0]\slv_regs_reg[0][8] ;
  wire [3:0]NLW_add_r1_carry_O_UNCONNECTED;
  wire [3:1]NLW_add_r1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_add_r1_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_add_r_reg[44]_i_1__1_CO_UNCONNECTED ;
  wire NLW_mul_r0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r0_CARRYOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r_reg__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r_reg__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_r_reg__0_PCOUT_UNCONNECTED;

  CARRY4 add_r1_carry
       (.CI(1'b0),
        .CO({add_r1_carry_n_0,add_r1_carry_n_1,add_r1_carry_n_2,add_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry_O_UNCONNECTED[3:0]),
        .S({\slv_regs_reg[0][11] ,add_r1_carry_i_2__1_n_0,add_r1_carry_i_3__1_n_0,add_r1_carry_i_4__1_n_0}));
  CARRY4 add_r1_carry__0
       (.CI(add_r1_carry_n_0),
        .CO({NLW_add_r1_carry__0_CO_UNCONNECTED[3:1],\counter_reg[1]_0 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,O}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_2__1
       (.I0(\counter_reg_n_0_[7] ),
        .I1(\slv_regs_reg[0][8] [6]),
        .I2(\counter_reg_n_0_[6] ),
        .I3(\slv_regs_reg[0][8] [5]),
        .I4(\slv_regs_reg[0][8] [7]),
        .I5(\counter_reg_n_0_[8] ),
        .O(add_r1_carry_i_2__1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_3__1
       (.I0(\counter_reg_n_0_[4] ),
        .I1(\slv_regs_reg[0][8] [3]),
        .I2(\counter_reg_n_0_[3] ),
        .I3(\slv_regs_reg[0][8] [2]),
        .I4(\slv_regs_reg[0][8] [4]),
        .I5(\counter_reg_n_0_[5] ),
        .O(add_r1_carry_i_3__1_n_0));
  LUT6 #(
    .INIT(64'h0990000000000990)) 
    add_r1_carry_i_4__1
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\slv_regs_reg[0][8] [0]),
        .I2(\slv_regs_reg[0][0] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\slv_regs_reg[0][8] [1]),
        .I5(\counter_reg_n_0_[2] ),
        .O(add_r1_carry_i_4__1_n_0));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_2__1 
       (.I0(mul_r_reg__1[3]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[3]),
        .O(\add_r[0]_i_2__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_3__1 
       (.I0(mul_r_reg__1[2]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[2]),
        .O(\add_r[0]_i_3__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_4__1 
       (.I0(mul_r_reg__1[1]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[1]),
        .O(\add_r[0]_i_4__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_5__1 
       (.I0(mul_r_reg__1[0]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[0]),
        .O(\add_r[0]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_2__1 
       (.I0(mul_r_reg__1[15]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[15]),
        .O(\add_r[12]_i_2__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_3__1 
       (.I0(mul_r_reg__1[14]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[14]),
        .O(\add_r[12]_i_3__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_4__1 
       (.I0(mul_r_reg__1[13]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[13]),
        .O(\add_r[12]_i_4__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_5__1 
       (.I0(mul_r_reg__1[12]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[12]),
        .O(\add_r[12]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_2__1 
       (.I0(mul_r_reg__1[19]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[19]),
        .O(\add_r[16]_i_2__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_3__1 
       (.I0(mul_r_reg__1[18]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[18]),
        .O(\add_r[16]_i_3__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_4__1 
       (.I0(mul_r_reg__1[17]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[17]),
        .O(\add_r[16]_i_4__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_5__1 
       (.I0(mul_r_reg__1[16]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[16]),
        .O(\add_r[16]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_2__1 
       (.I0(mul_r_reg__1[23]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[23]),
        .O(\add_r[20]_i_2__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_3__1 
       (.I0(mul_r_reg__1[22]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[22]),
        .O(\add_r[20]_i_3__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_4__1 
       (.I0(mul_r_reg__1[21]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[21]),
        .O(\add_r[20]_i_4__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_5__1 
       (.I0(mul_r_reg__1[20]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[20]),
        .O(\add_r[20]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_2__1 
       (.I0(mul_r_reg__1[27]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[27]),
        .O(\add_r[24]_i_2__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_3__1 
       (.I0(mul_r_reg__1[26]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[26]),
        .O(\add_r[24]_i_3__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_4__1 
       (.I0(mul_r_reg__1[25]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[25]),
        .O(\add_r[24]_i_4__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_5__1 
       (.I0(mul_r_reg__1[24]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[24]),
        .O(\add_r[24]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_2__1 
       (.I0(mul_r_reg__1[31]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[31]),
        .O(\add_r[28]_i_2__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_3__1 
       (.I0(mul_r_reg__1[30]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[30]),
        .O(\add_r[28]_i_3__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_4__1 
       (.I0(mul_r_reg__1[29]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[29]),
        .O(\add_r[28]_i_4__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_5__1 
       (.I0(mul_r_reg__1[28]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[28]),
        .O(\add_r[28]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_2__1 
       (.I0(mul_r_reg__1[35]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[35]),
        .O(\add_r[32]_i_2__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_3__1 
       (.I0(mul_r_reg__1[34]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[34]),
        .O(\add_r[32]_i_3__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_4__1 
       (.I0(mul_r_reg__1[33]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[33]),
        .O(\add_r[32]_i_4__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_5__1 
       (.I0(mul_r_reg__1[32]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[32]),
        .O(\add_r[32]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_2__1 
       (.I0(mul_r_reg__1[39]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[39]),
        .O(\add_r[36]_i_2__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_3__1 
       (.I0(mul_r_reg__1[38]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[38]),
        .O(\add_r[36]_i_3__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_4__1 
       (.I0(mul_r_reg__1[37]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[37]),
        .O(\add_r[36]_i_4__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_5__1 
       (.I0(mul_r_reg__1[36]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[36]),
        .O(\add_r[36]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_2__1 
       (.I0(mul_r_reg__1[43]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[43]),
        .O(\add_r[40]_i_2__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_3__1 
       (.I0(mul_r_reg__1[42]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[42]),
        .O(\add_r[40]_i_3__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_4__1 
       (.I0(mul_r_reg__1[41]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[41]),
        .O(\add_r[40]_i_4__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_5__1 
       (.I0(mul_r_reg__1[40]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[40]),
        .O(\add_r[40]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_2__1 
       (.I0(D[47]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[47]),
        .O(\add_r[44]_i_2__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_3__1 
       (.I0(mul_r_reg__1[46]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[46]),
        .O(\add_r[44]_i_3__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_4__1 
       (.I0(mul_r_reg__1[45]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[45]),
        .O(\add_r[44]_i_4__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_5__1 
       (.I0(mul_r_reg__1[44]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[44]),
        .O(\add_r[44]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_2__1 
       (.I0(mul_r_reg__1[7]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[7]),
        .O(\add_r[4]_i_2__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_3__1 
       (.I0(mul_r_reg__1[6]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[6]),
        .O(\add_r[4]_i_3__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_4__1 
       (.I0(mul_r_reg__1[5]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[5]),
        .O(\add_r[4]_i_4__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_5__1 
       (.I0(mul_r_reg__1[4]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[4]),
        .O(\add_r[4]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_2__1 
       (.I0(mul_r_reg__1[11]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[11]),
        .O(\add_r[8]_i_2__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_3__1 
       (.I0(mul_r_reg__1[10]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[10]),
        .O(\add_r[8]_i_3__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_4__1 
       (.I0(mul_r_reg__1[9]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[9]),
        .O(\add_r[8]_i_4__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_5__1 
       (.I0(mul_r_reg__1[8]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[8]),
        .O(\add_r[8]_i_5__1_n_0 ));
  FDRE \add_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__1_n_7 ),
        .Q(D[0]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[0]_i_1__1 
       (.CI(1'b0),
        .CO({\add_r_reg[0]_i_1__1_n_0 ,\add_r_reg[0]_i_1__1_n_1 ,\add_r_reg[0]_i_1__1_n_2 ,\add_r_reg[0]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[3:0]),
        .O({\add_r_reg[0]_i_1__1_n_4 ,\add_r_reg[0]_i_1__1_n_5 ,\add_r_reg[0]_i_1__1_n_6 ,\add_r_reg[0]_i_1__1_n_7 }),
        .S({\add_r[0]_i_2__1_n_0 ,\add_r[0]_i_3__1_n_0 ,\add_r[0]_i_4__1_n_0 ,\add_r[0]_i_5__1_n_0 }));
  FDRE \add_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__1_n_5 ),
        .Q(D[10]),
        .R(p_0_in__0));
  FDRE \add_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__1_n_4 ),
        .Q(D[11]),
        .R(p_0_in__0));
  FDRE \add_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__1_n_7 ),
        .Q(D[12]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[12]_i_1__1 
       (.CI(\add_r_reg[8]_i_1__1_n_0 ),
        .CO({\add_r_reg[12]_i_1__1_n_0 ,\add_r_reg[12]_i_1__1_n_1 ,\add_r_reg[12]_i_1__1_n_2 ,\add_r_reg[12]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[15:12]),
        .O({\add_r_reg[12]_i_1__1_n_4 ,\add_r_reg[12]_i_1__1_n_5 ,\add_r_reg[12]_i_1__1_n_6 ,\add_r_reg[12]_i_1__1_n_7 }),
        .S({\add_r[12]_i_2__1_n_0 ,\add_r[12]_i_3__1_n_0 ,\add_r[12]_i_4__1_n_0 ,\add_r[12]_i_5__1_n_0 }));
  FDRE \add_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__1_n_6 ),
        .Q(D[13]),
        .R(p_0_in__0));
  FDRE \add_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__1_n_5 ),
        .Q(D[14]),
        .R(p_0_in__0));
  FDRE \add_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__1_n_4 ),
        .Q(D[15]),
        .R(p_0_in__0));
  FDRE \add_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__1_n_7 ),
        .Q(D[16]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[16]_i_1__1 
       (.CI(\add_r_reg[12]_i_1__1_n_0 ),
        .CO({\add_r_reg[16]_i_1__1_n_0 ,\add_r_reg[16]_i_1__1_n_1 ,\add_r_reg[16]_i_1__1_n_2 ,\add_r_reg[16]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[19:16]),
        .O({\add_r_reg[16]_i_1__1_n_4 ,\add_r_reg[16]_i_1__1_n_5 ,\add_r_reg[16]_i_1__1_n_6 ,\add_r_reg[16]_i_1__1_n_7 }),
        .S({\add_r[16]_i_2__1_n_0 ,\add_r[16]_i_3__1_n_0 ,\add_r[16]_i_4__1_n_0 ,\add_r[16]_i_5__1_n_0 }));
  FDRE \add_r_reg[17] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__1_n_6 ),
        .Q(D[17]),
        .R(p_0_in__0));
  FDRE \add_r_reg[18] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__1_n_5 ),
        .Q(D[18]),
        .R(p_0_in__0));
  FDRE \add_r_reg[19] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__1_n_4 ),
        .Q(D[19]),
        .R(p_0_in__0));
  FDRE \add_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__1_n_6 ),
        .Q(D[1]),
        .R(p_0_in__0));
  FDRE \add_r_reg[20] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__1_n_7 ),
        .Q(D[20]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[20]_i_1__1 
       (.CI(\add_r_reg[16]_i_1__1_n_0 ),
        .CO({\add_r_reg[20]_i_1__1_n_0 ,\add_r_reg[20]_i_1__1_n_1 ,\add_r_reg[20]_i_1__1_n_2 ,\add_r_reg[20]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[23:20]),
        .O({\add_r_reg[20]_i_1__1_n_4 ,\add_r_reg[20]_i_1__1_n_5 ,\add_r_reg[20]_i_1__1_n_6 ,\add_r_reg[20]_i_1__1_n_7 }),
        .S({\add_r[20]_i_2__1_n_0 ,\add_r[20]_i_3__1_n_0 ,\add_r[20]_i_4__1_n_0 ,\add_r[20]_i_5__1_n_0 }));
  FDRE \add_r_reg[21] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__1_n_6 ),
        .Q(D[21]),
        .R(p_0_in__0));
  FDRE \add_r_reg[22] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__1_n_5 ),
        .Q(D[22]),
        .R(p_0_in__0));
  FDRE \add_r_reg[23] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__1_n_4 ),
        .Q(D[23]),
        .R(p_0_in__0));
  FDRE \add_r_reg[24] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__1_n_7 ),
        .Q(D[24]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[24]_i_1__1 
       (.CI(\add_r_reg[20]_i_1__1_n_0 ),
        .CO({\add_r_reg[24]_i_1__1_n_0 ,\add_r_reg[24]_i_1__1_n_1 ,\add_r_reg[24]_i_1__1_n_2 ,\add_r_reg[24]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[27:24]),
        .O({\add_r_reg[24]_i_1__1_n_4 ,\add_r_reg[24]_i_1__1_n_5 ,\add_r_reg[24]_i_1__1_n_6 ,\add_r_reg[24]_i_1__1_n_7 }),
        .S({\add_r[24]_i_2__1_n_0 ,\add_r[24]_i_3__1_n_0 ,\add_r[24]_i_4__1_n_0 ,\add_r[24]_i_5__1_n_0 }));
  FDRE \add_r_reg[25] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__1_n_6 ),
        .Q(D[25]),
        .R(p_0_in__0));
  FDRE \add_r_reg[26] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__1_n_5 ),
        .Q(D[26]),
        .R(p_0_in__0));
  FDRE \add_r_reg[27] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__1_n_4 ),
        .Q(D[27]),
        .R(p_0_in__0));
  FDRE \add_r_reg[28] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__1_n_7 ),
        .Q(D[28]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[28]_i_1__1 
       (.CI(\add_r_reg[24]_i_1__1_n_0 ),
        .CO({\add_r_reg[28]_i_1__1_n_0 ,\add_r_reg[28]_i_1__1_n_1 ,\add_r_reg[28]_i_1__1_n_2 ,\add_r_reg[28]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[31:28]),
        .O({\add_r_reg[28]_i_1__1_n_4 ,\add_r_reg[28]_i_1__1_n_5 ,\add_r_reg[28]_i_1__1_n_6 ,\add_r_reg[28]_i_1__1_n_7 }),
        .S({\add_r[28]_i_2__1_n_0 ,\add_r[28]_i_3__1_n_0 ,\add_r[28]_i_4__1_n_0 ,\add_r[28]_i_5__1_n_0 }));
  FDRE \add_r_reg[29] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__1_n_6 ),
        .Q(D[29]),
        .R(p_0_in__0));
  FDRE \add_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__1_n_5 ),
        .Q(D[2]),
        .R(p_0_in__0));
  FDRE \add_r_reg[30] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__1_n_5 ),
        .Q(D[30]),
        .R(p_0_in__0));
  FDRE \add_r_reg[31] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__1_n_4 ),
        .Q(D[31]),
        .R(p_0_in__0));
  FDRE \add_r_reg[32] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__1_n_7 ),
        .Q(D[32]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[32]_i_1__1 
       (.CI(\add_r_reg[28]_i_1__1_n_0 ),
        .CO({\add_r_reg[32]_i_1__1_n_0 ,\add_r_reg[32]_i_1__1_n_1 ,\add_r_reg[32]_i_1__1_n_2 ,\add_r_reg[32]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[35:32]),
        .O({\add_r_reg[32]_i_1__1_n_4 ,\add_r_reg[32]_i_1__1_n_5 ,\add_r_reg[32]_i_1__1_n_6 ,\add_r_reg[32]_i_1__1_n_7 }),
        .S({\add_r[32]_i_2__1_n_0 ,\add_r[32]_i_3__1_n_0 ,\add_r[32]_i_4__1_n_0 ,\add_r[32]_i_5__1_n_0 }));
  FDRE \add_r_reg[33] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__1_n_6 ),
        .Q(D[33]),
        .R(p_0_in__0));
  FDRE \add_r_reg[34] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__1_n_5 ),
        .Q(D[34]),
        .R(p_0_in__0));
  FDRE \add_r_reg[35] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__1_n_4 ),
        .Q(D[35]),
        .R(p_0_in__0));
  FDRE \add_r_reg[36] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__1_n_7 ),
        .Q(D[36]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[36]_i_1__1 
       (.CI(\add_r_reg[32]_i_1__1_n_0 ),
        .CO({\add_r_reg[36]_i_1__1_n_0 ,\add_r_reg[36]_i_1__1_n_1 ,\add_r_reg[36]_i_1__1_n_2 ,\add_r_reg[36]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[39:36]),
        .O({\add_r_reg[36]_i_1__1_n_4 ,\add_r_reg[36]_i_1__1_n_5 ,\add_r_reg[36]_i_1__1_n_6 ,\add_r_reg[36]_i_1__1_n_7 }),
        .S({\add_r[36]_i_2__1_n_0 ,\add_r[36]_i_3__1_n_0 ,\add_r[36]_i_4__1_n_0 ,\add_r[36]_i_5__1_n_0 }));
  FDRE \add_r_reg[37] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__1_n_6 ),
        .Q(D[37]),
        .R(p_0_in__0));
  FDRE \add_r_reg[38] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__1_n_5 ),
        .Q(D[38]),
        .R(p_0_in__0));
  FDRE \add_r_reg[39] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__1_n_4 ),
        .Q(D[39]),
        .R(p_0_in__0));
  FDRE \add_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__1_n_4 ),
        .Q(D[3]),
        .R(p_0_in__0));
  FDRE \add_r_reg[40] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__1_n_7 ),
        .Q(D[40]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[40]_i_1__1 
       (.CI(\add_r_reg[36]_i_1__1_n_0 ),
        .CO({\add_r_reg[40]_i_1__1_n_0 ,\add_r_reg[40]_i_1__1_n_1 ,\add_r_reg[40]_i_1__1_n_2 ,\add_r_reg[40]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[43:40]),
        .O({\add_r_reg[40]_i_1__1_n_4 ,\add_r_reg[40]_i_1__1_n_5 ,\add_r_reg[40]_i_1__1_n_6 ,\add_r_reg[40]_i_1__1_n_7 }),
        .S({\add_r[40]_i_2__1_n_0 ,\add_r[40]_i_3__1_n_0 ,\add_r[40]_i_4__1_n_0 ,\add_r[40]_i_5__1_n_0 }));
  FDRE \add_r_reg[41] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__1_n_6 ),
        .Q(D[41]),
        .R(p_0_in__0));
  FDRE \add_r_reg[42] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__1_n_5 ),
        .Q(D[42]),
        .R(p_0_in__0));
  FDRE \add_r_reg[43] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__1_n_4 ),
        .Q(D[43]),
        .R(p_0_in__0));
  FDRE \add_r_reg[44] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__1_n_7 ),
        .Q(D[44]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[44]_i_1__1 
       (.CI(\add_r_reg[40]_i_1__1_n_0 ),
        .CO({\NLW_add_r_reg[44]_i_1__1_CO_UNCONNECTED [3],\add_r_reg[44]_i_1__1_n_1 ,\add_r_reg[44]_i_1__1_n_2 ,\add_r_reg[44]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,mul_r_reg__1[46:44]}),
        .O({\add_r_reg[44]_i_1__1_n_4 ,\add_r_reg[44]_i_1__1_n_5 ,\add_r_reg[44]_i_1__1_n_6 ,\add_r_reg[44]_i_1__1_n_7 }),
        .S({\add_r[44]_i_2__1_n_0 ,\add_r[44]_i_3__1_n_0 ,\add_r[44]_i_4__1_n_0 ,\add_r[44]_i_5__1_n_0 }));
  FDRE \add_r_reg[45] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__1_n_6 ),
        .Q(D[45]),
        .R(p_0_in__0));
  FDRE \add_r_reg[46] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__1_n_5 ),
        .Q(D[46]),
        .R(p_0_in__0));
  FDRE \add_r_reg[47] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__1_n_4 ),
        .Q(D[47]),
        .R(p_0_in__0));
  FDRE \add_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__1_n_7 ),
        .Q(D[4]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[4]_i_1__1 
       (.CI(\add_r_reg[0]_i_1__1_n_0 ),
        .CO({\add_r_reg[4]_i_1__1_n_0 ,\add_r_reg[4]_i_1__1_n_1 ,\add_r_reg[4]_i_1__1_n_2 ,\add_r_reg[4]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[7:4]),
        .O({\add_r_reg[4]_i_1__1_n_4 ,\add_r_reg[4]_i_1__1_n_5 ,\add_r_reg[4]_i_1__1_n_6 ,\add_r_reg[4]_i_1__1_n_7 }),
        .S({\add_r[4]_i_2__1_n_0 ,\add_r[4]_i_3__1_n_0 ,\add_r[4]_i_4__1_n_0 ,\add_r[4]_i_5__1_n_0 }));
  FDRE \add_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__1_n_6 ),
        .Q(D[5]),
        .R(p_0_in__0));
  FDRE \add_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__1_n_5 ),
        .Q(D[6]),
        .R(p_0_in__0));
  FDRE \add_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__1_n_4 ),
        .Q(D[7]),
        .R(p_0_in__0));
  FDRE \add_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__1_n_7 ),
        .Q(D[8]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[8]_i_1__1 
       (.CI(\add_r_reg[4]_i_1__1_n_0 ),
        .CO({\add_r_reg[8]_i_1__1_n_0 ,\add_r_reg[8]_i_1__1_n_1 ,\add_r_reg[8]_i_1__1_n_2 ,\add_r_reg[8]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[11:8]),
        .O({\add_r_reg[8]_i_1__1_n_4 ,\add_r_reg[8]_i_1__1_n_5 ,\add_r_reg[8]_i_1__1_n_6 ,\add_r_reg[8]_i_1__1_n_7 }),
        .S({\add_r[8]_i_2__1_n_0 ,\add_r[8]_i_3__1_n_0 ,\add_r[8]_i_4__1_n_0 ,\add_r[8]_i_5__1_n_0 }));
  FDRE \add_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__1_n_6 ),
        .Q(D[9]),
        .R(p_0_in__0));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter[0]_i_1__4 
       (.I0(\counter_reg_n_0_[0] ),
        .O(\counter[0]_i_1__4_n_0 ));
  LUT4 #(
    .INIT(16'hF7C8)) 
    \counter[1]_i_1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(dp_enable),
        .I2(\counter_reg[1]_0 ),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \counter[2]_i_1__3 
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[2] ),
        .O(\counter[2]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \counter[3]_i_1__3 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .O(\counter[3]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \counter[4]_i_1__2 
       (.I0(\counter_reg_n_0_[3] ),
        .I1(\counter_reg_n_0_[2] ),
        .I2(\counter_reg_n_0_[1] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[4] ),
        .O(\counter[4]_i_1__2_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \counter[5]_i_1__2 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .I4(\counter_reg_n_0_[4] ),
        .I5(\counter_reg_n_0_[5] ),
        .O(\counter[5]_i_1__2_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \counter[6]_i_1__1 
       (.I0(\counter[8]_i_3__2_n_0 ),
        .I1(\counter_reg_n_0_[6] ),
        .O(\counter[6]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hB4)) 
    \counter[7]_i_1__1 
       (.I0(\counter[8]_i_3__2_n_0 ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(\counter_reg_n_0_[7] ),
        .O(\counter[7]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'hF708)) 
    \counter[8]_i_2__2 
       (.I0(\counter_reg_n_0_[7] ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(\counter[8]_i_3__2_n_0 ),
        .I3(\counter_reg_n_0_[8] ),
        .O(\counter[8]_i_2__2_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \counter[8]_i_3__2 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .I4(\counter_reg_n_0_[4] ),
        .I5(\counter_reg_n_0_[5] ),
        .O(\counter[8]_i_3__2_n_0 ));
  FDRE \counter_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[0]_i_1__4_n_0 ),
        .Q(\counter_reg_n_0_[0] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\counter[1]_i_1_n_0 ),
        .Q(\counter_reg_n_0_[1] ),
        .R(p_0_in__0));
  FDRE \counter_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[2]_i_1__3_n_0 ),
        .Q(\counter_reg_n_0_[2] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[3]_i_1__3_n_0 ),
        .Q(\counter_reg_n_0_[3] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[4]_i_1__2_n_0 ),
        .Q(\counter_reg_n_0_[4] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[5]_i_1__2_n_0 ),
        .Q(\counter_reg_n_0_[5] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[6]_i_1__1_n_0 ),
        .Q(\counter_reg_n_0_[6] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[7]_i_1__1_n_0 ),
        .Q(\counter_reg_n_0_[7] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[8]_i_2__2_n_0 ),
        .Q(\counter_reg_n_0_[8] ),
        .R(\slv_regs_reg[0][12] ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,b_ram_out[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({A[16],A[16],A[16],A[14:0]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r0_OVERFLOW_UNCONNECTED),
        .P({mul_r0_n_58,mul_r0_n_59,mul_r0_n_60,mul_r0_n_61,mul_r0_n_62,mul_r0_n_63,mul_r0_n_64,mul_r0_n_65,mul_r0_n_66,mul_r0_n_67,mul_r0_n_68,mul_r0_n_69,mul_r0_n_70,mul_r0_n_71,mul_r0_n_72,mul_r0_n_73,mul_r0_n_74,mul_r0_n_75,mul_r0_n_76,mul_r0_n_77,mul_r0_n_78,mul_r0_n_79,mul_r0_n_80,mul_r0_n_81,mul_r0_n_82,mul_r0_n_83,mul_r0_n_84,mul_r0_n_85,mul_r0_n_86,mul_r0_n_87,mul_r0_n_88,mul_r0_n_89,mul_r0_n_90,mul_r0_n_91,mul_r0_n_92,mul_r0_n_93,mul_r0_n_94,mul_r0_n_95,mul_r0_n_96,mul_r0_n_97,mul_r0_n_98,mul_r0_n_99,mul_r0_n_100,mul_r0_n_101,mul_r0_n_102,mul_r0_n_103,mul_r0_n_104,mul_r0_n_105}),
        .PATTERNBDETECT(NLW_mul_r0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_r0_UNDERFLOW_UNCONNECTED));
  FDRE \mul_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_105),
        .Q(mul_r_reg__1[0]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_95),
        .Q(mul_r_reg__1[10]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_94),
        .Q(mul_r_reg__1[11]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_93),
        .Q(mul_r_reg__1[12]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_92),
        .Q(mul_r_reg__1[13]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_91),
        .Q(mul_r_reg__1[14]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_90),
        .Q(mul_r_reg__1[15]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_89),
        .Q(mul_r_reg__1[16]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_104),
        .Q(mul_r_reg__1[1]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_103),
        .Q(mul_r_reg__1[2]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_102),
        .Q(mul_r_reg__1[3]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_101),
        .Q(mul_r_reg__1[4]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_100),
        .Q(mul_r_reg__1[5]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_99),
        .Q(mul_r_reg__1[6]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_98),
        .Q(mul_r_reg__1[7]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_97),
        .Q(mul_r_reg__1[8]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_96),
        .Q(mul_r_reg__1[9]),
        .R(p_0_in__0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-12 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r_reg__0
       (.A({A[16],A[16],A[16],A[16],A[16:15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r_reg__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({b_ram_out[31],b_ram_out[31],b_ram_out[31],b_ram_out[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r_reg__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(dp_enable),
        .CLK(clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED),
        .P({mul_r_reg__0_n_58,mul_r_reg__0_n_59,mul_r_reg__0_n_60,mul_r_reg__0_n_61,mul_r_reg__0_n_62,mul_r_reg__0_n_63,mul_r_reg__0_n_64,mul_r_reg__0_n_65,mul_r_reg__0_n_66,mul_r_reg__0_n_67,mul_r_reg__0_n_68,mul_r_reg__0_n_69,mul_r_reg__0_n_70,mul_r_reg__0_n_71,mul_r_reg__0_n_72,mul_r_reg__0_n_73,mul_r_reg__0_n_74,mul_r_reg__1[47:17]}),
        .PATTERNBDETECT(NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .PCOUT(NLW_mul_r_reg__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(p_0_in__0),
        .UNDERFLOW(NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "dot_product" *) 
module design_1_top_0_0_dot_product_2
   (D,
    \counter_reg[1]_0 ,
    A,
    b_ram_out,
    dp_enable,
    clk,
    p_0_in__0,
    \slv_regs_reg[0][11] ,
    O,
    \slv_regs_reg[0][8] ,
    \slv_regs_reg[0][0] ,
    \slv_regs_reg[0][12] );
  output [47:0]D;
  output [0:0]\counter_reg[1]_0 ;
  input [16:0]A;
  input [31:0]b_ram_out;
  input dp_enable;
  input clk;
  input p_0_in__0;
  input [0:0]\slv_regs_reg[0][11] ;
  input [0:0]O;
  input [7:0]\slv_regs_reg[0][8] ;
  input [0:0]\slv_regs_reg[0][0] ;
  input [0:0]\slv_regs_reg[0][12] ;

  wire [16:0]A;
  wire [47:0]D;
  wire [0:0]O;
  wire add_r1_carry_i_2__2_n_0;
  wire add_r1_carry_i_3__2_n_0;
  wire add_r1_carry_i_4__2_n_0;
  wire add_r1_carry_n_0;
  wire add_r1_carry_n_1;
  wire add_r1_carry_n_2;
  wire add_r1_carry_n_3;
  wire \add_r[0]_i_2__2_n_0 ;
  wire \add_r[0]_i_3__2_n_0 ;
  wire \add_r[0]_i_4__2_n_0 ;
  wire \add_r[0]_i_5__2_n_0 ;
  wire \add_r[12]_i_2__2_n_0 ;
  wire \add_r[12]_i_3__2_n_0 ;
  wire \add_r[12]_i_4__2_n_0 ;
  wire \add_r[12]_i_5__2_n_0 ;
  wire \add_r[16]_i_2__2_n_0 ;
  wire \add_r[16]_i_3__2_n_0 ;
  wire \add_r[16]_i_4__2_n_0 ;
  wire \add_r[16]_i_5__2_n_0 ;
  wire \add_r[20]_i_2__2_n_0 ;
  wire \add_r[20]_i_3__2_n_0 ;
  wire \add_r[20]_i_4__2_n_0 ;
  wire \add_r[20]_i_5__2_n_0 ;
  wire \add_r[24]_i_2__2_n_0 ;
  wire \add_r[24]_i_3__2_n_0 ;
  wire \add_r[24]_i_4__2_n_0 ;
  wire \add_r[24]_i_5__2_n_0 ;
  wire \add_r[28]_i_2__2_n_0 ;
  wire \add_r[28]_i_3__2_n_0 ;
  wire \add_r[28]_i_4__2_n_0 ;
  wire \add_r[28]_i_5__2_n_0 ;
  wire \add_r[32]_i_2__2_n_0 ;
  wire \add_r[32]_i_3__2_n_0 ;
  wire \add_r[32]_i_4__2_n_0 ;
  wire \add_r[32]_i_5__2_n_0 ;
  wire \add_r[36]_i_2__2_n_0 ;
  wire \add_r[36]_i_3__2_n_0 ;
  wire \add_r[36]_i_4__2_n_0 ;
  wire \add_r[36]_i_5__2_n_0 ;
  wire \add_r[40]_i_2__2_n_0 ;
  wire \add_r[40]_i_3__2_n_0 ;
  wire \add_r[40]_i_4__2_n_0 ;
  wire \add_r[40]_i_5__2_n_0 ;
  wire \add_r[44]_i_2__2_n_0 ;
  wire \add_r[44]_i_3__2_n_0 ;
  wire \add_r[44]_i_4__2_n_0 ;
  wire \add_r[44]_i_5__2_n_0 ;
  wire \add_r[4]_i_2__2_n_0 ;
  wire \add_r[4]_i_3__2_n_0 ;
  wire \add_r[4]_i_4__2_n_0 ;
  wire \add_r[4]_i_5__2_n_0 ;
  wire \add_r[8]_i_2__2_n_0 ;
  wire \add_r[8]_i_3__2_n_0 ;
  wire \add_r[8]_i_4__2_n_0 ;
  wire \add_r[8]_i_5__2_n_0 ;
  wire \add_r_reg[0]_i_1__2_n_0 ;
  wire \add_r_reg[0]_i_1__2_n_1 ;
  wire \add_r_reg[0]_i_1__2_n_2 ;
  wire \add_r_reg[0]_i_1__2_n_3 ;
  wire \add_r_reg[0]_i_1__2_n_4 ;
  wire \add_r_reg[0]_i_1__2_n_5 ;
  wire \add_r_reg[0]_i_1__2_n_6 ;
  wire \add_r_reg[0]_i_1__2_n_7 ;
  wire \add_r_reg[12]_i_1__2_n_0 ;
  wire \add_r_reg[12]_i_1__2_n_1 ;
  wire \add_r_reg[12]_i_1__2_n_2 ;
  wire \add_r_reg[12]_i_1__2_n_3 ;
  wire \add_r_reg[12]_i_1__2_n_4 ;
  wire \add_r_reg[12]_i_1__2_n_5 ;
  wire \add_r_reg[12]_i_1__2_n_6 ;
  wire \add_r_reg[12]_i_1__2_n_7 ;
  wire \add_r_reg[16]_i_1__2_n_0 ;
  wire \add_r_reg[16]_i_1__2_n_1 ;
  wire \add_r_reg[16]_i_1__2_n_2 ;
  wire \add_r_reg[16]_i_1__2_n_3 ;
  wire \add_r_reg[16]_i_1__2_n_4 ;
  wire \add_r_reg[16]_i_1__2_n_5 ;
  wire \add_r_reg[16]_i_1__2_n_6 ;
  wire \add_r_reg[16]_i_1__2_n_7 ;
  wire \add_r_reg[20]_i_1__2_n_0 ;
  wire \add_r_reg[20]_i_1__2_n_1 ;
  wire \add_r_reg[20]_i_1__2_n_2 ;
  wire \add_r_reg[20]_i_1__2_n_3 ;
  wire \add_r_reg[20]_i_1__2_n_4 ;
  wire \add_r_reg[20]_i_1__2_n_5 ;
  wire \add_r_reg[20]_i_1__2_n_6 ;
  wire \add_r_reg[20]_i_1__2_n_7 ;
  wire \add_r_reg[24]_i_1__2_n_0 ;
  wire \add_r_reg[24]_i_1__2_n_1 ;
  wire \add_r_reg[24]_i_1__2_n_2 ;
  wire \add_r_reg[24]_i_1__2_n_3 ;
  wire \add_r_reg[24]_i_1__2_n_4 ;
  wire \add_r_reg[24]_i_1__2_n_5 ;
  wire \add_r_reg[24]_i_1__2_n_6 ;
  wire \add_r_reg[24]_i_1__2_n_7 ;
  wire \add_r_reg[28]_i_1__2_n_0 ;
  wire \add_r_reg[28]_i_1__2_n_1 ;
  wire \add_r_reg[28]_i_1__2_n_2 ;
  wire \add_r_reg[28]_i_1__2_n_3 ;
  wire \add_r_reg[28]_i_1__2_n_4 ;
  wire \add_r_reg[28]_i_1__2_n_5 ;
  wire \add_r_reg[28]_i_1__2_n_6 ;
  wire \add_r_reg[28]_i_1__2_n_7 ;
  wire \add_r_reg[32]_i_1__2_n_0 ;
  wire \add_r_reg[32]_i_1__2_n_1 ;
  wire \add_r_reg[32]_i_1__2_n_2 ;
  wire \add_r_reg[32]_i_1__2_n_3 ;
  wire \add_r_reg[32]_i_1__2_n_4 ;
  wire \add_r_reg[32]_i_1__2_n_5 ;
  wire \add_r_reg[32]_i_1__2_n_6 ;
  wire \add_r_reg[32]_i_1__2_n_7 ;
  wire \add_r_reg[36]_i_1__2_n_0 ;
  wire \add_r_reg[36]_i_1__2_n_1 ;
  wire \add_r_reg[36]_i_1__2_n_2 ;
  wire \add_r_reg[36]_i_1__2_n_3 ;
  wire \add_r_reg[36]_i_1__2_n_4 ;
  wire \add_r_reg[36]_i_1__2_n_5 ;
  wire \add_r_reg[36]_i_1__2_n_6 ;
  wire \add_r_reg[36]_i_1__2_n_7 ;
  wire \add_r_reg[40]_i_1__2_n_0 ;
  wire \add_r_reg[40]_i_1__2_n_1 ;
  wire \add_r_reg[40]_i_1__2_n_2 ;
  wire \add_r_reg[40]_i_1__2_n_3 ;
  wire \add_r_reg[40]_i_1__2_n_4 ;
  wire \add_r_reg[40]_i_1__2_n_5 ;
  wire \add_r_reg[40]_i_1__2_n_6 ;
  wire \add_r_reg[40]_i_1__2_n_7 ;
  wire \add_r_reg[44]_i_1__2_n_1 ;
  wire \add_r_reg[44]_i_1__2_n_2 ;
  wire \add_r_reg[44]_i_1__2_n_3 ;
  wire \add_r_reg[44]_i_1__2_n_4 ;
  wire \add_r_reg[44]_i_1__2_n_5 ;
  wire \add_r_reg[44]_i_1__2_n_6 ;
  wire \add_r_reg[44]_i_1__2_n_7 ;
  wire \add_r_reg[4]_i_1__2_n_0 ;
  wire \add_r_reg[4]_i_1__2_n_1 ;
  wire \add_r_reg[4]_i_1__2_n_2 ;
  wire \add_r_reg[4]_i_1__2_n_3 ;
  wire \add_r_reg[4]_i_1__2_n_4 ;
  wire \add_r_reg[4]_i_1__2_n_5 ;
  wire \add_r_reg[4]_i_1__2_n_6 ;
  wire \add_r_reg[4]_i_1__2_n_7 ;
  wire \add_r_reg[8]_i_1__2_n_0 ;
  wire \add_r_reg[8]_i_1__2_n_1 ;
  wire \add_r_reg[8]_i_1__2_n_2 ;
  wire \add_r_reg[8]_i_1__2_n_3 ;
  wire \add_r_reg[8]_i_1__2_n_4 ;
  wire \add_r_reg[8]_i_1__2_n_5 ;
  wire \add_r_reg[8]_i_1__2_n_6 ;
  wire \add_r_reg[8]_i_1__2_n_7 ;
  wire [31:0]b_ram_out;
  wire clk;
  wire \counter[0]_i_1__5_n_0 ;
  wire \counter[1]_i_1_n_0 ;
  wire \counter[2]_i_1__4_n_0 ;
  wire \counter[3]_i_1__4_n_0 ;
  wire \counter[4]_i_1__3_n_0 ;
  wire \counter[5]_i_1__3_n_0 ;
  wire \counter[6]_i_1__2_n_0 ;
  wire \counter[7]_i_1__2_n_0 ;
  wire \counter[8]_i_2__3_n_0 ;
  wire \counter[8]_i_3__3_n_0 ;
  wire [0:0]\counter_reg[1]_0 ;
  wire \counter_reg_n_0_[0] ;
  wire \counter_reg_n_0_[1] ;
  wire \counter_reg_n_0_[2] ;
  wire \counter_reg_n_0_[3] ;
  wire \counter_reg_n_0_[4] ;
  wire \counter_reg_n_0_[5] ;
  wire \counter_reg_n_0_[6] ;
  wire \counter_reg_n_0_[7] ;
  wire \counter_reg_n_0_[8] ;
  wire dp_enable;
  wire mul_r0_n_100;
  wire mul_r0_n_101;
  wire mul_r0_n_102;
  wire mul_r0_n_103;
  wire mul_r0_n_104;
  wire mul_r0_n_105;
  wire mul_r0_n_106;
  wire mul_r0_n_107;
  wire mul_r0_n_108;
  wire mul_r0_n_109;
  wire mul_r0_n_110;
  wire mul_r0_n_111;
  wire mul_r0_n_112;
  wire mul_r0_n_113;
  wire mul_r0_n_114;
  wire mul_r0_n_115;
  wire mul_r0_n_116;
  wire mul_r0_n_117;
  wire mul_r0_n_118;
  wire mul_r0_n_119;
  wire mul_r0_n_120;
  wire mul_r0_n_121;
  wire mul_r0_n_122;
  wire mul_r0_n_123;
  wire mul_r0_n_124;
  wire mul_r0_n_125;
  wire mul_r0_n_126;
  wire mul_r0_n_127;
  wire mul_r0_n_128;
  wire mul_r0_n_129;
  wire mul_r0_n_130;
  wire mul_r0_n_131;
  wire mul_r0_n_132;
  wire mul_r0_n_133;
  wire mul_r0_n_134;
  wire mul_r0_n_135;
  wire mul_r0_n_136;
  wire mul_r0_n_137;
  wire mul_r0_n_138;
  wire mul_r0_n_139;
  wire mul_r0_n_140;
  wire mul_r0_n_141;
  wire mul_r0_n_142;
  wire mul_r0_n_143;
  wire mul_r0_n_144;
  wire mul_r0_n_145;
  wire mul_r0_n_146;
  wire mul_r0_n_147;
  wire mul_r0_n_148;
  wire mul_r0_n_149;
  wire mul_r0_n_150;
  wire mul_r0_n_151;
  wire mul_r0_n_152;
  wire mul_r0_n_153;
  wire mul_r0_n_58;
  wire mul_r0_n_59;
  wire mul_r0_n_60;
  wire mul_r0_n_61;
  wire mul_r0_n_62;
  wire mul_r0_n_63;
  wire mul_r0_n_64;
  wire mul_r0_n_65;
  wire mul_r0_n_66;
  wire mul_r0_n_67;
  wire mul_r0_n_68;
  wire mul_r0_n_69;
  wire mul_r0_n_70;
  wire mul_r0_n_71;
  wire mul_r0_n_72;
  wire mul_r0_n_73;
  wire mul_r0_n_74;
  wire mul_r0_n_75;
  wire mul_r0_n_76;
  wire mul_r0_n_77;
  wire mul_r0_n_78;
  wire mul_r0_n_79;
  wire mul_r0_n_80;
  wire mul_r0_n_81;
  wire mul_r0_n_82;
  wire mul_r0_n_83;
  wire mul_r0_n_84;
  wire mul_r0_n_85;
  wire mul_r0_n_86;
  wire mul_r0_n_87;
  wire mul_r0_n_88;
  wire mul_r0_n_89;
  wire mul_r0_n_90;
  wire mul_r0_n_91;
  wire mul_r0_n_92;
  wire mul_r0_n_93;
  wire mul_r0_n_94;
  wire mul_r0_n_95;
  wire mul_r0_n_96;
  wire mul_r0_n_97;
  wire mul_r0_n_98;
  wire mul_r0_n_99;
  wire mul_r_reg__0_n_58;
  wire mul_r_reg__0_n_59;
  wire mul_r_reg__0_n_60;
  wire mul_r_reg__0_n_61;
  wire mul_r_reg__0_n_62;
  wire mul_r_reg__0_n_63;
  wire mul_r_reg__0_n_64;
  wire mul_r_reg__0_n_65;
  wire mul_r_reg__0_n_66;
  wire mul_r_reg__0_n_67;
  wire mul_r_reg__0_n_68;
  wire mul_r_reg__0_n_69;
  wire mul_r_reg__0_n_70;
  wire mul_r_reg__0_n_71;
  wire mul_r_reg__0_n_72;
  wire mul_r_reg__0_n_73;
  wire mul_r_reg__0_n_74;
  wire [47:0]mul_r_reg__1;
  wire p_0_in__0;
  wire [0:0]\slv_regs_reg[0][0] ;
  wire [0:0]\slv_regs_reg[0][11] ;
  wire [0:0]\slv_regs_reg[0][12] ;
  wire [7:0]\slv_regs_reg[0][8] ;
  wire [3:0]NLW_add_r1_carry_O_UNCONNECTED;
  wire [3:1]NLW_add_r1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_add_r1_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_add_r_reg[44]_i_1__2_CO_UNCONNECTED ;
  wire NLW_mul_r0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r0_CARRYOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r_reg__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r_reg__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_r_reg__0_PCOUT_UNCONNECTED;

  CARRY4 add_r1_carry
       (.CI(1'b0),
        .CO({add_r1_carry_n_0,add_r1_carry_n_1,add_r1_carry_n_2,add_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry_O_UNCONNECTED[3:0]),
        .S({\slv_regs_reg[0][11] ,add_r1_carry_i_2__2_n_0,add_r1_carry_i_3__2_n_0,add_r1_carry_i_4__2_n_0}));
  CARRY4 add_r1_carry__0
       (.CI(add_r1_carry_n_0),
        .CO({NLW_add_r1_carry__0_CO_UNCONNECTED[3:1],\counter_reg[1]_0 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,O}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_2__2
       (.I0(\counter_reg_n_0_[7] ),
        .I1(\slv_regs_reg[0][8] [6]),
        .I2(\counter_reg_n_0_[6] ),
        .I3(\slv_regs_reg[0][8] [5]),
        .I4(\slv_regs_reg[0][8] [7]),
        .I5(\counter_reg_n_0_[8] ),
        .O(add_r1_carry_i_2__2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_3__2
       (.I0(\counter_reg_n_0_[4] ),
        .I1(\slv_regs_reg[0][8] [3]),
        .I2(\counter_reg_n_0_[3] ),
        .I3(\slv_regs_reg[0][8] [2]),
        .I4(\slv_regs_reg[0][8] [4]),
        .I5(\counter_reg_n_0_[5] ),
        .O(add_r1_carry_i_3__2_n_0));
  LUT6 #(
    .INIT(64'h0990000000000990)) 
    add_r1_carry_i_4__2
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\slv_regs_reg[0][8] [0]),
        .I2(\slv_regs_reg[0][0] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\slv_regs_reg[0][8] [1]),
        .I5(\counter_reg_n_0_[2] ),
        .O(add_r1_carry_i_4__2_n_0));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_2__2 
       (.I0(mul_r_reg__1[3]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[3]),
        .O(\add_r[0]_i_2__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_3__2 
       (.I0(mul_r_reg__1[2]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[2]),
        .O(\add_r[0]_i_3__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_4__2 
       (.I0(mul_r_reg__1[1]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[1]),
        .O(\add_r[0]_i_4__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_5__2 
       (.I0(mul_r_reg__1[0]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[0]),
        .O(\add_r[0]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_2__2 
       (.I0(mul_r_reg__1[15]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[15]),
        .O(\add_r[12]_i_2__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_3__2 
       (.I0(mul_r_reg__1[14]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[14]),
        .O(\add_r[12]_i_3__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_4__2 
       (.I0(mul_r_reg__1[13]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[13]),
        .O(\add_r[12]_i_4__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_5__2 
       (.I0(mul_r_reg__1[12]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[12]),
        .O(\add_r[12]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_2__2 
       (.I0(mul_r_reg__1[19]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[19]),
        .O(\add_r[16]_i_2__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_3__2 
       (.I0(mul_r_reg__1[18]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[18]),
        .O(\add_r[16]_i_3__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_4__2 
       (.I0(mul_r_reg__1[17]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[17]),
        .O(\add_r[16]_i_4__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_5__2 
       (.I0(mul_r_reg__1[16]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[16]),
        .O(\add_r[16]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_2__2 
       (.I0(mul_r_reg__1[23]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[23]),
        .O(\add_r[20]_i_2__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_3__2 
       (.I0(mul_r_reg__1[22]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[22]),
        .O(\add_r[20]_i_3__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_4__2 
       (.I0(mul_r_reg__1[21]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[21]),
        .O(\add_r[20]_i_4__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_5__2 
       (.I0(mul_r_reg__1[20]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[20]),
        .O(\add_r[20]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_2__2 
       (.I0(mul_r_reg__1[27]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[27]),
        .O(\add_r[24]_i_2__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_3__2 
       (.I0(mul_r_reg__1[26]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[26]),
        .O(\add_r[24]_i_3__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_4__2 
       (.I0(mul_r_reg__1[25]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[25]),
        .O(\add_r[24]_i_4__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_5__2 
       (.I0(mul_r_reg__1[24]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[24]),
        .O(\add_r[24]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_2__2 
       (.I0(mul_r_reg__1[31]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[31]),
        .O(\add_r[28]_i_2__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_3__2 
       (.I0(mul_r_reg__1[30]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[30]),
        .O(\add_r[28]_i_3__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_4__2 
       (.I0(mul_r_reg__1[29]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[29]),
        .O(\add_r[28]_i_4__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_5__2 
       (.I0(mul_r_reg__1[28]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[28]),
        .O(\add_r[28]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_2__2 
       (.I0(mul_r_reg__1[35]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[35]),
        .O(\add_r[32]_i_2__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_3__2 
       (.I0(mul_r_reg__1[34]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[34]),
        .O(\add_r[32]_i_3__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_4__2 
       (.I0(mul_r_reg__1[33]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[33]),
        .O(\add_r[32]_i_4__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_5__2 
       (.I0(mul_r_reg__1[32]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[32]),
        .O(\add_r[32]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_2__2 
       (.I0(mul_r_reg__1[39]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[39]),
        .O(\add_r[36]_i_2__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_3__2 
       (.I0(mul_r_reg__1[38]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[38]),
        .O(\add_r[36]_i_3__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_4__2 
       (.I0(mul_r_reg__1[37]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[37]),
        .O(\add_r[36]_i_4__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_5__2 
       (.I0(mul_r_reg__1[36]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[36]),
        .O(\add_r[36]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_2__2 
       (.I0(mul_r_reg__1[43]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[43]),
        .O(\add_r[40]_i_2__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_3__2 
       (.I0(mul_r_reg__1[42]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[42]),
        .O(\add_r[40]_i_3__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_4__2 
       (.I0(mul_r_reg__1[41]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[41]),
        .O(\add_r[40]_i_4__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_5__2 
       (.I0(mul_r_reg__1[40]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[40]),
        .O(\add_r[40]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_2__2 
       (.I0(D[47]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[47]),
        .O(\add_r[44]_i_2__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_3__2 
       (.I0(mul_r_reg__1[46]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[46]),
        .O(\add_r[44]_i_3__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_4__2 
       (.I0(mul_r_reg__1[45]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[45]),
        .O(\add_r[44]_i_4__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_5__2 
       (.I0(mul_r_reg__1[44]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[44]),
        .O(\add_r[44]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_2__2 
       (.I0(mul_r_reg__1[7]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[7]),
        .O(\add_r[4]_i_2__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_3__2 
       (.I0(mul_r_reg__1[6]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[6]),
        .O(\add_r[4]_i_3__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_4__2 
       (.I0(mul_r_reg__1[5]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[5]),
        .O(\add_r[4]_i_4__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_5__2 
       (.I0(mul_r_reg__1[4]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[4]),
        .O(\add_r[4]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_2__2 
       (.I0(mul_r_reg__1[11]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[11]),
        .O(\add_r[8]_i_2__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_3__2 
       (.I0(mul_r_reg__1[10]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[10]),
        .O(\add_r[8]_i_3__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_4__2 
       (.I0(mul_r_reg__1[9]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[9]),
        .O(\add_r[8]_i_4__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_5__2 
       (.I0(mul_r_reg__1[8]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[8]),
        .O(\add_r[8]_i_5__2_n_0 ));
  FDRE \add_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__2_n_7 ),
        .Q(D[0]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[0]_i_1__2 
       (.CI(1'b0),
        .CO({\add_r_reg[0]_i_1__2_n_0 ,\add_r_reg[0]_i_1__2_n_1 ,\add_r_reg[0]_i_1__2_n_2 ,\add_r_reg[0]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[3:0]),
        .O({\add_r_reg[0]_i_1__2_n_4 ,\add_r_reg[0]_i_1__2_n_5 ,\add_r_reg[0]_i_1__2_n_6 ,\add_r_reg[0]_i_1__2_n_7 }),
        .S({\add_r[0]_i_2__2_n_0 ,\add_r[0]_i_3__2_n_0 ,\add_r[0]_i_4__2_n_0 ,\add_r[0]_i_5__2_n_0 }));
  FDRE \add_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__2_n_5 ),
        .Q(D[10]),
        .R(p_0_in__0));
  FDRE \add_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__2_n_4 ),
        .Q(D[11]),
        .R(p_0_in__0));
  FDRE \add_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__2_n_7 ),
        .Q(D[12]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[12]_i_1__2 
       (.CI(\add_r_reg[8]_i_1__2_n_0 ),
        .CO({\add_r_reg[12]_i_1__2_n_0 ,\add_r_reg[12]_i_1__2_n_1 ,\add_r_reg[12]_i_1__2_n_2 ,\add_r_reg[12]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[15:12]),
        .O({\add_r_reg[12]_i_1__2_n_4 ,\add_r_reg[12]_i_1__2_n_5 ,\add_r_reg[12]_i_1__2_n_6 ,\add_r_reg[12]_i_1__2_n_7 }),
        .S({\add_r[12]_i_2__2_n_0 ,\add_r[12]_i_3__2_n_0 ,\add_r[12]_i_4__2_n_0 ,\add_r[12]_i_5__2_n_0 }));
  FDRE \add_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__2_n_6 ),
        .Q(D[13]),
        .R(p_0_in__0));
  FDRE \add_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__2_n_5 ),
        .Q(D[14]),
        .R(p_0_in__0));
  FDRE \add_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__2_n_4 ),
        .Q(D[15]),
        .R(p_0_in__0));
  FDRE \add_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__2_n_7 ),
        .Q(D[16]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[16]_i_1__2 
       (.CI(\add_r_reg[12]_i_1__2_n_0 ),
        .CO({\add_r_reg[16]_i_1__2_n_0 ,\add_r_reg[16]_i_1__2_n_1 ,\add_r_reg[16]_i_1__2_n_2 ,\add_r_reg[16]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[19:16]),
        .O({\add_r_reg[16]_i_1__2_n_4 ,\add_r_reg[16]_i_1__2_n_5 ,\add_r_reg[16]_i_1__2_n_6 ,\add_r_reg[16]_i_1__2_n_7 }),
        .S({\add_r[16]_i_2__2_n_0 ,\add_r[16]_i_3__2_n_0 ,\add_r[16]_i_4__2_n_0 ,\add_r[16]_i_5__2_n_0 }));
  FDRE \add_r_reg[17] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__2_n_6 ),
        .Q(D[17]),
        .R(p_0_in__0));
  FDRE \add_r_reg[18] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__2_n_5 ),
        .Q(D[18]),
        .R(p_0_in__0));
  FDRE \add_r_reg[19] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__2_n_4 ),
        .Q(D[19]),
        .R(p_0_in__0));
  FDRE \add_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__2_n_6 ),
        .Q(D[1]),
        .R(p_0_in__0));
  FDRE \add_r_reg[20] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__2_n_7 ),
        .Q(D[20]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[20]_i_1__2 
       (.CI(\add_r_reg[16]_i_1__2_n_0 ),
        .CO({\add_r_reg[20]_i_1__2_n_0 ,\add_r_reg[20]_i_1__2_n_1 ,\add_r_reg[20]_i_1__2_n_2 ,\add_r_reg[20]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[23:20]),
        .O({\add_r_reg[20]_i_1__2_n_4 ,\add_r_reg[20]_i_1__2_n_5 ,\add_r_reg[20]_i_1__2_n_6 ,\add_r_reg[20]_i_1__2_n_7 }),
        .S({\add_r[20]_i_2__2_n_0 ,\add_r[20]_i_3__2_n_0 ,\add_r[20]_i_4__2_n_0 ,\add_r[20]_i_5__2_n_0 }));
  FDRE \add_r_reg[21] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__2_n_6 ),
        .Q(D[21]),
        .R(p_0_in__0));
  FDRE \add_r_reg[22] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__2_n_5 ),
        .Q(D[22]),
        .R(p_0_in__0));
  FDRE \add_r_reg[23] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__2_n_4 ),
        .Q(D[23]),
        .R(p_0_in__0));
  FDRE \add_r_reg[24] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__2_n_7 ),
        .Q(D[24]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[24]_i_1__2 
       (.CI(\add_r_reg[20]_i_1__2_n_0 ),
        .CO({\add_r_reg[24]_i_1__2_n_0 ,\add_r_reg[24]_i_1__2_n_1 ,\add_r_reg[24]_i_1__2_n_2 ,\add_r_reg[24]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[27:24]),
        .O({\add_r_reg[24]_i_1__2_n_4 ,\add_r_reg[24]_i_1__2_n_5 ,\add_r_reg[24]_i_1__2_n_6 ,\add_r_reg[24]_i_1__2_n_7 }),
        .S({\add_r[24]_i_2__2_n_0 ,\add_r[24]_i_3__2_n_0 ,\add_r[24]_i_4__2_n_0 ,\add_r[24]_i_5__2_n_0 }));
  FDRE \add_r_reg[25] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__2_n_6 ),
        .Q(D[25]),
        .R(p_0_in__0));
  FDRE \add_r_reg[26] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__2_n_5 ),
        .Q(D[26]),
        .R(p_0_in__0));
  FDRE \add_r_reg[27] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__2_n_4 ),
        .Q(D[27]),
        .R(p_0_in__0));
  FDRE \add_r_reg[28] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__2_n_7 ),
        .Q(D[28]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[28]_i_1__2 
       (.CI(\add_r_reg[24]_i_1__2_n_0 ),
        .CO({\add_r_reg[28]_i_1__2_n_0 ,\add_r_reg[28]_i_1__2_n_1 ,\add_r_reg[28]_i_1__2_n_2 ,\add_r_reg[28]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[31:28]),
        .O({\add_r_reg[28]_i_1__2_n_4 ,\add_r_reg[28]_i_1__2_n_5 ,\add_r_reg[28]_i_1__2_n_6 ,\add_r_reg[28]_i_1__2_n_7 }),
        .S({\add_r[28]_i_2__2_n_0 ,\add_r[28]_i_3__2_n_0 ,\add_r[28]_i_4__2_n_0 ,\add_r[28]_i_5__2_n_0 }));
  FDRE \add_r_reg[29] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__2_n_6 ),
        .Q(D[29]),
        .R(p_0_in__0));
  FDRE \add_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__2_n_5 ),
        .Q(D[2]),
        .R(p_0_in__0));
  FDRE \add_r_reg[30] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__2_n_5 ),
        .Q(D[30]),
        .R(p_0_in__0));
  FDRE \add_r_reg[31] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__2_n_4 ),
        .Q(D[31]),
        .R(p_0_in__0));
  FDRE \add_r_reg[32] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__2_n_7 ),
        .Q(D[32]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[32]_i_1__2 
       (.CI(\add_r_reg[28]_i_1__2_n_0 ),
        .CO({\add_r_reg[32]_i_1__2_n_0 ,\add_r_reg[32]_i_1__2_n_1 ,\add_r_reg[32]_i_1__2_n_2 ,\add_r_reg[32]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[35:32]),
        .O({\add_r_reg[32]_i_1__2_n_4 ,\add_r_reg[32]_i_1__2_n_5 ,\add_r_reg[32]_i_1__2_n_6 ,\add_r_reg[32]_i_1__2_n_7 }),
        .S({\add_r[32]_i_2__2_n_0 ,\add_r[32]_i_3__2_n_0 ,\add_r[32]_i_4__2_n_0 ,\add_r[32]_i_5__2_n_0 }));
  FDRE \add_r_reg[33] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__2_n_6 ),
        .Q(D[33]),
        .R(p_0_in__0));
  FDRE \add_r_reg[34] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__2_n_5 ),
        .Q(D[34]),
        .R(p_0_in__0));
  FDRE \add_r_reg[35] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__2_n_4 ),
        .Q(D[35]),
        .R(p_0_in__0));
  FDRE \add_r_reg[36] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__2_n_7 ),
        .Q(D[36]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[36]_i_1__2 
       (.CI(\add_r_reg[32]_i_1__2_n_0 ),
        .CO({\add_r_reg[36]_i_1__2_n_0 ,\add_r_reg[36]_i_1__2_n_1 ,\add_r_reg[36]_i_1__2_n_2 ,\add_r_reg[36]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[39:36]),
        .O({\add_r_reg[36]_i_1__2_n_4 ,\add_r_reg[36]_i_1__2_n_5 ,\add_r_reg[36]_i_1__2_n_6 ,\add_r_reg[36]_i_1__2_n_7 }),
        .S({\add_r[36]_i_2__2_n_0 ,\add_r[36]_i_3__2_n_0 ,\add_r[36]_i_4__2_n_0 ,\add_r[36]_i_5__2_n_0 }));
  FDRE \add_r_reg[37] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__2_n_6 ),
        .Q(D[37]),
        .R(p_0_in__0));
  FDRE \add_r_reg[38] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__2_n_5 ),
        .Q(D[38]),
        .R(p_0_in__0));
  FDRE \add_r_reg[39] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__2_n_4 ),
        .Q(D[39]),
        .R(p_0_in__0));
  FDRE \add_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__2_n_4 ),
        .Q(D[3]),
        .R(p_0_in__0));
  FDRE \add_r_reg[40] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__2_n_7 ),
        .Q(D[40]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[40]_i_1__2 
       (.CI(\add_r_reg[36]_i_1__2_n_0 ),
        .CO({\add_r_reg[40]_i_1__2_n_0 ,\add_r_reg[40]_i_1__2_n_1 ,\add_r_reg[40]_i_1__2_n_2 ,\add_r_reg[40]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[43:40]),
        .O({\add_r_reg[40]_i_1__2_n_4 ,\add_r_reg[40]_i_1__2_n_5 ,\add_r_reg[40]_i_1__2_n_6 ,\add_r_reg[40]_i_1__2_n_7 }),
        .S({\add_r[40]_i_2__2_n_0 ,\add_r[40]_i_3__2_n_0 ,\add_r[40]_i_4__2_n_0 ,\add_r[40]_i_5__2_n_0 }));
  FDRE \add_r_reg[41] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__2_n_6 ),
        .Q(D[41]),
        .R(p_0_in__0));
  FDRE \add_r_reg[42] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__2_n_5 ),
        .Q(D[42]),
        .R(p_0_in__0));
  FDRE \add_r_reg[43] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__2_n_4 ),
        .Q(D[43]),
        .R(p_0_in__0));
  FDRE \add_r_reg[44] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__2_n_7 ),
        .Q(D[44]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[44]_i_1__2 
       (.CI(\add_r_reg[40]_i_1__2_n_0 ),
        .CO({\NLW_add_r_reg[44]_i_1__2_CO_UNCONNECTED [3],\add_r_reg[44]_i_1__2_n_1 ,\add_r_reg[44]_i_1__2_n_2 ,\add_r_reg[44]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,mul_r_reg__1[46:44]}),
        .O({\add_r_reg[44]_i_1__2_n_4 ,\add_r_reg[44]_i_1__2_n_5 ,\add_r_reg[44]_i_1__2_n_6 ,\add_r_reg[44]_i_1__2_n_7 }),
        .S({\add_r[44]_i_2__2_n_0 ,\add_r[44]_i_3__2_n_0 ,\add_r[44]_i_4__2_n_0 ,\add_r[44]_i_5__2_n_0 }));
  FDRE \add_r_reg[45] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__2_n_6 ),
        .Q(D[45]),
        .R(p_0_in__0));
  FDRE \add_r_reg[46] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__2_n_5 ),
        .Q(D[46]),
        .R(p_0_in__0));
  FDRE \add_r_reg[47] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__2_n_4 ),
        .Q(D[47]),
        .R(p_0_in__0));
  FDRE \add_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__2_n_7 ),
        .Q(D[4]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[4]_i_1__2 
       (.CI(\add_r_reg[0]_i_1__2_n_0 ),
        .CO({\add_r_reg[4]_i_1__2_n_0 ,\add_r_reg[4]_i_1__2_n_1 ,\add_r_reg[4]_i_1__2_n_2 ,\add_r_reg[4]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[7:4]),
        .O({\add_r_reg[4]_i_1__2_n_4 ,\add_r_reg[4]_i_1__2_n_5 ,\add_r_reg[4]_i_1__2_n_6 ,\add_r_reg[4]_i_1__2_n_7 }),
        .S({\add_r[4]_i_2__2_n_0 ,\add_r[4]_i_3__2_n_0 ,\add_r[4]_i_4__2_n_0 ,\add_r[4]_i_5__2_n_0 }));
  FDRE \add_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__2_n_6 ),
        .Q(D[5]),
        .R(p_0_in__0));
  FDRE \add_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__2_n_5 ),
        .Q(D[6]),
        .R(p_0_in__0));
  FDRE \add_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__2_n_4 ),
        .Q(D[7]),
        .R(p_0_in__0));
  FDRE \add_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__2_n_7 ),
        .Q(D[8]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[8]_i_1__2 
       (.CI(\add_r_reg[4]_i_1__2_n_0 ),
        .CO({\add_r_reg[8]_i_1__2_n_0 ,\add_r_reg[8]_i_1__2_n_1 ,\add_r_reg[8]_i_1__2_n_2 ,\add_r_reg[8]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[11:8]),
        .O({\add_r_reg[8]_i_1__2_n_4 ,\add_r_reg[8]_i_1__2_n_5 ,\add_r_reg[8]_i_1__2_n_6 ,\add_r_reg[8]_i_1__2_n_7 }),
        .S({\add_r[8]_i_2__2_n_0 ,\add_r[8]_i_3__2_n_0 ,\add_r[8]_i_4__2_n_0 ,\add_r[8]_i_5__2_n_0 }));
  FDRE \add_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__2_n_6 ),
        .Q(D[9]),
        .R(p_0_in__0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter[0]_i_1__5 
       (.I0(\counter_reg_n_0_[0] ),
        .O(\counter[0]_i_1__5_n_0 ));
  LUT4 #(
    .INIT(16'hF7C8)) 
    \counter[1]_i_1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(dp_enable),
        .I2(\counter_reg[1]_0 ),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \counter[2]_i_1__4 
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[2] ),
        .O(\counter[2]_i_1__4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \counter[3]_i_1__4 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .O(\counter[3]_i_1__4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \counter[4]_i_1__3 
       (.I0(\counter_reg_n_0_[3] ),
        .I1(\counter_reg_n_0_[2] ),
        .I2(\counter_reg_n_0_[1] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[4] ),
        .O(\counter[4]_i_1__3_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \counter[5]_i_1__3 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .I4(\counter_reg_n_0_[4] ),
        .I5(\counter_reg_n_0_[5] ),
        .O(\counter[5]_i_1__3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \counter[6]_i_1__2 
       (.I0(\counter[8]_i_3__3_n_0 ),
        .I1(\counter_reg_n_0_[6] ),
        .O(\counter[6]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hB4)) 
    \counter[7]_i_1__2 
       (.I0(\counter[8]_i_3__3_n_0 ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(\counter_reg_n_0_[7] ),
        .O(\counter[7]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT4 #(
    .INIT(16'hF708)) 
    \counter[8]_i_2__3 
       (.I0(\counter_reg_n_0_[7] ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(\counter[8]_i_3__3_n_0 ),
        .I3(\counter_reg_n_0_[8] ),
        .O(\counter[8]_i_2__3_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \counter[8]_i_3__3 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .I4(\counter_reg_n_0_[4] ),
        .I5(\counter_reg_n_0_[5] ),
        .O(\counter[8]_i_3__3_n_0 ));
  FDRE \counter_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[0]_i_1__5_n_0 ),
        .Q(\counter_reg_n_0_[0] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\counter[1]_i_1_n_0 ),
        .Q(\counter_reg_n_0_[1] ),
        .R(p_0_in__0));
  FDRE \counter_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[2]_i_1__4_n_0 ),
        .Q(\counter_reg_n_0_[2] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[3]_i_1__4_n_0 ),
        .Q(\counter_reg_n_0_[3] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[4]_i_1__3_n_0 ),
        .Q(\counter_reg_n_0_[4] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[5]_i_1__3_n_0 ),
        .Q(\counter_reg_n_0_[5] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[6]_i_1__2_n_0 ),
        .Q(\counter_reg_n_0_[6] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[7]_i_1__2_n_0 ),
        .Q(\counter_reg_n_0_[7] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[8]_i_2__3_n_0 ),
        .Q(\counter_reg_n_0_[8] ),
        .R(\slv_regs_reg[0][12] ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,b_ram_out[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({A[16],A[16],A[16],A[14:0]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r0_OVERFLOW_UNCONNECTED),
        .P({mul_r0_n_58,mul_r0_n_59,mul_r0_n_60,mul_r0_n_61,mul_r0_n_62,mul_r0_n_63,mul_r0_n_64,mul_r0_n_65,mul_r0_n_66,mul_r0_n_67,mul_r0_n_68,mul_r0_n_69,mul_r0_n_70,mul_r0_n_71,mul_r0_n_72,mul_r0_n_73,mul_r0_n_74,mul_r0_n_75,mul_r0_n_76,mul_r0_n_77,mul_r0_n_78,mul_r0_n_79,mul_r0_n_80,mul_r0_n_81,mul_r0_n_82,mul_r0_n_83,mul_r0_n_84,mul_r0_n_85,mul_r0_n_86,mul_r0_n_87,mul_r0_n_88,mul_r0_n_89,mul_r0_n_90,mul_r0_n_91,mul_r0_n_92,mul_r0_n_93,mul_r0_n_94,mul_r0_n_95,mul_r0_n_96,mul_r0_n_97,mul_r0_n_98,mul_r0_n_99,mul_r0_n_100,mul_r0_n_101,mul_r0_n_102,mul_r0_n_103,mul_r0_n_104,mul_r0_n_105}),
        .PATTERNBDETECT(NLW_mul_r0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_r0_UNDERFLOW_UNCONNECTED));
  FDRE \mul_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_105),
        .Q(mul_r_reg__1[0]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_95),
        .Q(mul_r_reg__1[10]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_94),
        .Q(mul_r_reg__1[11]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_93),
        .Q(mul_r_reg__1[12]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_92),
        .Q(mul_r_reg__1[13]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_91),
        .Q(mul_r_reg__1[14]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_90),
        .Q(mul_r_reg__1[15]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_89),
        .Q(mul_r_reg__1[16]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_104),
        .Q(mul_r_reg__1[1]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_103),
        .Q(mul_r_reg__1[2]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_102),
        .Q(mul_r_reg__1[3]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_101),
        .Q(mul_r_reg__1[4]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_100),
        .Q(mul_r_reg__1[5]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_99),
        .Q(mul_r_reg__1[6]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_98),
        .Q(mul_r_reg__1[7]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_97),
        .Q(mul_r_reg__1[8]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_96),
        .Q(mul_r_reg__1[9]),
        .R(p_0_in__0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-12 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r_reg__0
       (.A({A[16],A[16],A[16],A[16],A[16],A[16],A[16:15],A[15],A[15],A[15],A[15],A[15],A[15],A[15:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r_reg__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({b_ram_out[31],b_ram_out[31],b_ram_out[31],b_ram_out[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r_reg__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(dp_enable),
        .CLK(clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED),
        .P({mul_r_reg__0_n_58,mul_r_reg__0_n_59,mul_r_reg__0_n_60,mul_r_reg__0_n_61,mul_r_reg__0_n_62,mul_r_reg__0_n_63,mul_r_reg__0_n_64,mul_r_reg__0_n_65,mul_r_reg__0_n_66,mul_r_reg__0_n_67,mul_r_reg__0_n_68,mul_r_reg__0_n_69,mul_r_reg__0_n_70,mul_r_reg__0_n_71,mul_r_reg__0_n_72,mul_r_reg__0_n_73,mul_r_reg__0_n_74,mul_r_reg__1[47:17]}),
        .PATTERNBDETECT(NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .PCOUT(NLW_mul_r_reg__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(p_0_in__0),
        .UNDERFLOW(NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "dot_product" *) 
module design_1_top_0_0_dot_product_3
   (D,
    CO,
    B,
    b_ram_out,
    dp_enable,
    clk,
    p_0_in__0,
    \in_raw_delay_reg[15]_rep__5 ,
    \slv_regs_reg[0][11] ,
    O,
    \slv_regs_reg[0][8] ,
    \slv_regs_reg[0][0] ,
    \slv_regs_reg[0][12] );
  output [47:0]D;
  output [0:0]CO;
  input [16:0]B;
  input [31:0]b_ram_out;
  input dp_enable;
  input clk;
  input p_0_in__0;
  input [0:0]\in_raw_delay_reg[15]_rep__5 ;
  input [0:0]\slv_regs_reg[0][11] ;
  input [0:0]O;
  input [7:0]\slv_regs_reg[0][8] ;
  input [0:0]\slv_regs_reg[0][0] ;
  input [0:0]\slv_regs_reg[0][12] ;

  wire [16:0]B;
  wire [0:0]CO;
  wire [47:0]D;
  wire [0:0]O;
  wire add_r1_carry_i_2__3_n_0;
  wire add_r1_carry_i_3__3_n_0;
  wire add_r1_carry_i_4__3_n_0;
  wire add_r1_carry_n_0;
  wire add_r1_carry_n_1;
  wire add_r1_carry_n_2;
  wire add_r1_carry_n_3;
  wire \add_r[0]_i_2__3_n_0 ;
  wire \add_r[0]_i_3__3_n_0 ;
  wire \add_r[0]_i_4__3_n_0 ;
  wire \add_r[0]_i_5__3_n_0 ;
  wire \add_r[12]_i_2__3_n_0 ;
  wire \add_r[12]_i_3__3_n_0 ;
  wire \add_r[12]_i_4__3_n_0 ;
  wire \add_r[12]_i_5__3_n_0 ;
  wire \add_r[16]_i_2__3_n_0 ;
  wire \add_r[16]_i_3__3_n_0 ;
  wire \add_r[16]_i_4__3_n_0 ;
  wire \add_r[16]_i_5__3_n_0 ;
  wire \add_r[20]_i_2__3_n_0 ;
  wire \add_r[20]_i_3__3_n_0 ;
  wire \add_r[20]_i_4__3_n_0 ;
  wire \add_r[20]_i_5__3_n_0 ;
  wire \add_r[24]_i_2__3_n_0 ;
  wire \add_r[24]_i_3__3_n_0 ;
  wire \add_r[24]_i_4__3_n_0 ;
  wire \add_r[24]_i_5__3_n_0 ;
  wire \add_r[28]_i_2__3_n_0 ;
  wire \add_r[28]_i_3__3_n_0 ;
  wire \add_r[28]_i_4__3_n_0 ;
  wire \add_r[28]_i_5__3_n_0 ;
  wire \add_r[32]_i_2__3_n_0 ;
  wire \add_r[32]_i_3__3_n_0 ;
  wire \add_r[32]_i_4__3_n_0 ;
  wire \add_r[32]_i_5__3_n_0 ;
  wire \add_r[36]_i_2__3_n_0 ;
  wire \add_r[36]_i_3__3_n_0 ;
  wire \add_r[36]_i_4__3_n_0 ;
  wire \add_r[36]_i_5__3_n_0 ;
  wire \add_r[40]_i_2__3_n_0 ;
  wire \add_r[40]_i_3__3_n_0 ;
  wire \add_r[40]_i_4__3_n_0 ;
  wire \add_r[40]_i_5__3_n_0 ;
  wire \add_r[44]_i_2__3_n_0 ;
  wire \add_r[44]_i_3__3_n_0 ;
  wire \add_r[44]_i_4__3_n_0 ;
  wire \add_r[44]_i_5__3_n_0 ;
  wire \add_r[4]_i_2__3_n_0 ;
  wire \add_r[4]_i_3__3_n_0 ;
  wire \add_r[4]_i_4__3_n_0 ;
  wire \add_r[4]_i_5__3_n_0 ;
  wire \add_r[8]_i_2__3_n_0 ;
  wire \add_r[8]_i_3__3_n_0 ;
  wire \add_r[8]_i_4__3_n_0 ;
  wire \add_r[8]_i_5__3_n_0 ;
  wire \add_r_reg[0]_i_1__3_n_0 ;
  wire \add_r_reg[0]_i_1__3_n_1 ;
  wire \add_r_reg[0]_i_1__3_n_2 ;
  wire \add_r_reg[0]_i_1__3_n_3 ;
  wire \add_r_reg[0]_i_1__3_n_4 ;
  wire \add_r_reg[0]_i_1__3_n_5 ;
  wire \add_r_reg[0]_i_1__3_n_6 ;
  wire \add_r_reg[0]_i_1__3_n_7 ;
  wire \add_r_reg[12]_i_1__3_n_0 ;
  wire \add_r_reg[12]_i_1__3_n_1 ;
  wire \add_r_reg[12]_i_1__3_n_2 ;
  wire \add_r_reg[12]_i_1__3_n_3 ;
  wire \add_r_reg[12]_i_1__3_n_4 ;
  wire \add_r_reg[12]_i_1__3_n_5 ;
  wire \add_r_reg[12]_i_1__3_n_6 ;
  wire \add_r_reg[12]_i_1__3_n_7 ;
  wire \add_r_reg[16]_i_1__3_n_0 ;
  wire \add_r_reg[16]_i_1__3_n_1 ;
  wire \add_r_reg[16]_i_1__3_n_2 ;
  wire \add_r_reg[16]_i_1__3_n_3 ;
  wire \add_r_reg[16]_i_1__3_n_4 ;
  wire \add_r_reg[16]_i_1__3_n_5 ;
  wire \add_r_reg[16]_i_1__3_n_6 ;
  wire \add_r_reg[16]_i_1__3_n_7 ;
  wire \add_r_reg[20]_i_1__3_n_0 ;
  wire \add_r_reg[20]_i_1__3_n_1 ;
  wire \add_r_reg[20]_i_1__3_n_2 ;
  wire \add_r_reg[20]_i_1__3_n_3 ;
  wire \add_r_reg[20]_i_1__3_n_4 ;
  wire \add_r_reg[20]_i_1__3_n_5 ;
  wire \add_r_reg[20]_i_1__3_n_6 ;
  wire \add_r_reg[20]_i_1__3_n_7 ;
  wire \add_r_reg[24]_i_1__3_n_0 ;
  wire \add_r_reg[24]_i_1__3_n_1 ;
  wire \add_r_reg[24]_i_1__3_n_2 ;
  wire \add_r_reg[24]_i_1__3_n_3 ;
  wire \add_r_reg[24]_i_1__3_n_4 ;
  wire \add_r_reg[24]_i_1__3_n_5 ;
  wire \add_r_reg[24]_i_1__3_n_6 ;
  wire \add_r_reg[24]_i_1__3_n_7 ;
  wire \add_r_reg[28]_i_1__3_n_0 ;
  wire \add_r_reg[28]_i_1__3_n_1 ;
  wire \add_r_reg[28]_i_1__3_n_2 ;
  wire \add_r_reg[28]_i_1__3_n_3 ;
  wire \add_r_reg[28]_i_1__3_n_4 ;
  wire \add_r_reg[28]_i_1__3_n_5 ;
  wire \add_r_reg[28]_i_1__3_n_6 ;
  wire \add_r_reg[28]_i_1__3_n_7 ;
  wire \add_r_reg[32]_i_1__3_n_0 ;
  wire \add_r_reg[32]_i_1__3_n_1 ;
  wire \add_r_reg[32]_i_1__3_n_2 ;
  wire \add_r_reg[32]_i_1__3_n_3 ;
  wire \add_r_reg[32]_i_1__3_n_4 ;
  wire \add_r_reg[32]_i_1__3_n_5 ;
  wire \add_r_reg[32]_i_1__3_n_6 ;
  wire \add_r_reg[32]_i_1__3_n_7 ;
  wire \add_r_reg[36]_i_1__3_n_0 ;
  wire \add_r_reg[36]_i_1__3_n_1 ;
  wire \add_r_reg[36]_i_1__3_n_2 ;
  wire \add_r_reg[36]_i_1__3_n_3 ;
  wire \add_r_reg[36]_i_1__3_n_4 ;
  wire \add_r_reg[36]_i_1__3_n_5 ;
  wire \add_r_reg[36]_i_1__3_n_6 ;
  wire \add_r_reg[36]_i_1__3_n_7 ;
  wire \add_r_reg[40]_i_1__3_n_0 ;
  wire \add_r_reg[40]_i_1__3_n_1 ;
  wire \add_r_reg[40]_i_1__3_n_2 ;
  wire \add_r_reg[40]_i_1__3_n_3 ;
  wire \add_r_reg[40]_i_1__3_n_4 ;
  wire \add_r_reg[40]_i_1__3_n_5 ;
  wire \add_r_reg[40]_i_1__3_n_6 ;
  wire \add_r_reg[40]_i_1__3_n_7 ;
  wire \add_r_reg[44]_i_1__3_n_1 ;
  wire \add_r_reg[44]_i_1__3_n_2 ;
  wire \add_r_reg[44]_i_1__3_n_3 ;
  wire \add_r_reg[44]_i_1__3_n_4 ;
  wire \add_r_reg[44]_i_1__3_n_5 ;
  wire \add_r_reg[44]_i_1__3_n_6 ;
  wire \add_r_reg[44]_i_1__3_n_7 ;
  wire \add_r_reg[4]_i_1__3_n_0 ;
  wire \add_r_reg[4]_i_1__3_n_1 ;
  wire \add_r_reg[4]_i_1__3_n_2 ;
  wire \add_r_reg[4]_i_1__3_n_3 ;
  wire \add_r_reg[4]_i_1__3_n_4 ;
  wire \add_r_reg[4]_i_1__3_n_5 ;
  wire \add_r_reg[4]_i_1__3_n_6 ;
  wire \add_r_reg[4]_i_1__3_n_7 ;
  wire \add_r_reg[8]_i_1__3_n_0 ;
  wire \add_r_reg[8]_i_1__3_n_1 ;
  wire \add_r_reg[8]_i_1__3_n_2 ;
  wire \add_r_reg[8]_i_1__3_n_3 ;
  wire \add_r_reg[8]_i_1__3_n_4 ;
  wire \add_r_reg[8]_i_1__3_n_5 ;
  wire \add_r_reg[8]_i_1__3_n_6 ;
  wire \add_r_reg[8]_i_1__3_n_7 ;
  wire [31:0]b_ram_out;
  wire clk;
  wire \counter[0]_i_1__6_n_0 ;
  wire \counter[1]_i_1_n_0 ;
  wire \counter[2]_i_1__5_n_0 ;
  wire \counter[3]_i_1__5_n_0 ;
  wire \counter[4]_i_1__4_n_0 ;
  wire \counter[5]_i_1__4_n_0 ;
  wire \counter[6]_i_1__3_n_0 ;
  wire \counter[7]_i_1__3_n_0 ;
  wire \counter[8]_i_2__4_n_0 ;
  wire \counter[8]_i_3__4_n_0 ;
  wire \counter_reg_n_0_[0] ;
  wire \counter_reg_n_0_[1] ;
  wire \counter_reg_n_0_[2] ;
  wire \counter_reg_n_0_[3] ;
  wire \counter_reg_n_0_[4] ;
  wire \counter_reg_n_0_[5] ;
  wire \counter_reg_n_0_[6] ;
  wire \counter_reg_n_0_[7] ;
  wire \counter_reg_n_0_[8] ;
  wire dp_enable;
  wire [0:0]\in_raw_delay_reg[15]_rep__5 ;
  wire mul_r0_n_100;
  wire mul_r0_n_101;
  wire mul_r0_n_102;
  wire mul_r0_n_103;
  wire mul_r0_n_104;
  wire mul_r0_n_105;
  wire mul_r0_n_106;
  wire mul_r0_n_107;
  wire mul_r0_n_108;
  wire mul_r0_n_109;
  wire mul_r0_n_110;
  wire mul_r0_n_111;
  wire mul_r0_n_112;
  wire mul_r0_n_113;
  wire mul_r0_n_114;
  wire mul_r0_n_115;
  wire mul_r0_n_116;
  wire mul_r0_n_117;
  wire mul_r0_n_118;
  wire mul_r0_n_119;
  wire mul_r0_n_120;
  wire mul_r0_n_121;
  wire mul_r0_n_122;
  wire mul_r0_n_123;
  wire mul_r0_n_124;
  wire mul_r0_n_125;
  wire mul_r0_n_126;
  wire mul_r0_n_127;
  wire mul_r0_n_128;
  wire mul_r0_n_129;
  wire mul_r0_n_130;
  wire mul_r0_n_131;
  wire mul_r0_n_132;
  wire mul_r0_n_133;
  wire mul_r0_n_134;
  wire mul_r0_n_135;
  wire mul_r0_n_136;
  wire mul_r0_n_137;
  wire mul_r0_n_138;
  wire mul_r0_n_139;
  wire mul_r0_n_140;
  wire mul_r0_n_141;
  wire mul_r0_n_142;
  wire mul_r0_n_143;
  wire mul_r0_n_144;
  wire mul_r0_n_145;
  wire mul_r0_n_146;
  wire mul_r0_n_147;
  wire mul_r0_n_148;
  wire mul_r0_n_149;
  wire mul_r0_n_150;
  wire mul_r0_n_151;
  wire mul_r0_n_152;
  wire mul_r0_n_153;
  wire mul_r0_n_58;
  wire mul_r0_n_59;
  wire mul_r0_n_60;
  wire mul_r0_n_61;
  wire mul_r0_n_62;
  wire mul_r0_n_63;
  wire mul_r0_n_64;
  wire mul_r0_n_65;
  wire mul_r0_n_66;
  wire mul_r0_n_67;
  wire mul_r0_n_68;
  wire mul_r0_n_69;
  wire mul_r0_n_70;
  wire mul_r0_n_71;
  wire mul_r0_n_72;
  wire mul_r0_n_73;
  wire mul_r0_n_74;
  wire mul_r0_n_75;
  wire mul_r0_n_76;
  wire mul_r0_n_77;
  wire mul_r0_n_78;
  wire mul_r0_n_79;
  wire mul_r0_n_80;
  wire mul_r0_n_81;
  wire mul_r0_n_82;
  wire mul_r0_n_83;
  wire mul_r0_n_84;
  wire mul_r0_n_85;
  wire mul_r0_n_86;
  wire mul_r0_n_87;
  wire mul_r0_n_88;
  wire mul_r0_n_89;
  wire mul_r0_n_90;
  wire mul_r0_n_91;
  wire mul_r0_n_92;
  wire mul_r0_n_93;
  wire mul_r0_n_94;
  wire mul_r0_n_95;
  wire mul_r0_n_96;
  wire mul_r0_n_97;
  wire mul_r0_n_98;
  wire mul_r0_n_99;
  wire mul_r_reg__0_n_58;
  wire mul_r_reg__0_n_59;
  wire mul_r_reg__0_n_60;
  wire mul_r_reg__0_n_61;
  wire mul_r_reg__0_n_62;
  wire mul_r_reg__0_n_63;
  wire mul_r_reg__0_n_64;
  wire mul_r_reg__0_n_65;
  wire mul_r_reg__0_n_66;
  wire mul_r_reg__0_n_67;
  wire mul_r_reg__0_n_68;
  wire mul_r_reg__0_n_69;
  wire mul_r_reg__0_n_70;
  wire mul_r_reg__0_n_71;
  wire mul_r_reg__0_n_72;
  wire mul_r_reg__0_n_73;
  wire mul_r_reg__0_n_74;
  wire [47:0]mul_r_reg__1;
  wire p_0_in__0;
  wire [0:0]\slv_regs_reg[0][0] ;
  wire [0:0]\slv_regs_reg[0][11] ;
  wire [0:0]\slv_regs_reg[0][12] ;
  wire [7:0]\slv_regs_reg[0][8] ;
  wire [3:0]NLW_add_r1_carry_O_UNCONNECTED;
  wire [3:1]NLW_add_r1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_add_r1_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_add_r_reg[44]_i_1__3_CO_UNCONNECTED ;
  wire NLW_mul_r0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r0_CARRYOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r_reg__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r_reg__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_r_reg__0_PCOUT_UNCONNECTED;

  CARRY4 add_r1_carry
       (.CI(1'b0),
        .CO({add_r1_carry_n_0,add_r1_carry_n_1,add_r1_carry_n_2,add_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry_O_UNCONNECTED[3:0]),
        .S({\slv_regs_reg[0][11] ,add_r1_carry_i_2__3_n_0,add_r1_carry_i_3__3_n_0,add_r1_carry_i_4__3_n_0}));
  CARRY4 add_r1_carry__0
       (.CI(add_r1_carry_n_0),
        .CO({NLW_add_r1_carry__0_CO_UNCONNECTED[3:1],CO}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,O}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_2__3
       (.I0(\counter_reg_n_0_[7] ),
        .I1(\slv_regs_reg[0][8] [6]),
        .I2(\counter_reg_n_0_[6] ),
        .I3(\slv_regs_reg[0][8] [5]),
        .I4(\slv_regs_reg[0][8] [7]),
        .I5(\counter_reg_n_0_[8] ),
        .O(add_r1_carry_i_2__3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_3__3
       (.I0(\counter_reg_n_0_[4] ),
        .I1(\slv_regs_reg[0][8] [3]),
        .I2(\counter_reg_n_0_[3] ),
        .I3(\slv_regs_reg[0][8] [2]),
        .I4(\slv_regs_reg[0][8] [4]),
        .I5(\counter_reg_n_0_[5] ),
        .O(add_r1_carry_i_3__3_n_0));
  LUT6 #(
    .INIT(64'h0990000000000990)) 
    add_r1_carry_i_4__3
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\slv_regs_reg[0][8] [0]),
        .I2(\slv_regs_reg[0][0] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\slv_regs_reg[0][8] [1]),
        .I5(\counter_reg_n_0_[2] ),
        .O(add_r1_carry_i_4__3_n_0));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_2__3 
       (.I0(mul_r_reg__1[3]),
        .I1(CO),
        .I2(D[3]),
        .O(\add_r[0]_i_2__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_3__3 
       (.I0(mul_r_reg__1[2]),
        .I1(CO),
        .I2(D[2]),
        .O(\add_r[0]_i_3__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_4__3 
       (.I0(mul_r_reg__1[1]),
        .I1(CO),
        .I2(D[1]),
        .O(\add_r[0]_i_4__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_5__3 
       (.I0(mul_r_reg__1[0]),
        .I1(CO),
        .I2(D[0]),
        .O(\add_r[0]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_2__3 
       (.I0(mul_r_reg__1[15]),
        .I1(CO),
        .I2(D[15]),
        .O(\add_r[12]_i_2__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_3__3 
       (.I0(mul_r_reg__1[14]),
        .I1(CO),
        .I2(D[14]),
        .O(\add_r[12]_i_3__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_4__3 
       (.I0(mul_r_reg__1[13]),
        .I1(CO),
        .I2(D[13]),
        .O(\add_r[12]_i_4__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_5__3 
       (.I0(mul_r_reg__1[12]),
        .I1(CO),
        .I2(D[12]),
        .O(\add_r[12]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_2__3 
       (.I0(mul_r_reg__1[19]),
        .I1(CO),
        .I2(D[19]),
        .O(\add_r[16]_i_2__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_3__3 
       (.I0(mul_r_reg__1[18]),
        .I1(CO),
        .I2(D[18]),
        .O(\add_r[16]_i_3__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_4__3 
       (.I0(mul_r_reg__1[17]),
        .I1(CO),
        .I2(D[17]),
        .O(\add_r[16]_i_4__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_5__3 
       (.I0(mul_r_reg__1[16]),
        .I1(CO),
        .I2(D[16]),
        .O(\add_r[16]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_2__3 
       (.I0(mul_r_reg__1[23]),
        .I1(CO),
        .I2(D[23]),
        .O(\add_r[20]_i_2__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_3__3 
       (.I0(mul_r_reg__1[22]),
        .I1(CO),
        .I2(D[22]),
        .O(\add_r[20]_i_3__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_4__3 
       (.I0(mul_r_reg__1[21]),
        .I1(CO),
        .I2(D[21]),
        .O(\add_r[20]_i_4__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_5__3 
       (.I0(mul_r_reg__1[20]),
        .I1(CO),
        .I2(D[20]),
        .O(\add_r[20]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_2__3 
       (.I0(mul_r_reg__1[27]),
        .I1(CO),
        .I2(D[27]),
        .O(\add_r[24]_i_2__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_3__3 
       (.I0(mul_r_reg__1[26]),
        .I1(CO),
        .I2(D[26]),
        .O(\add_r[24]_i_3__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_4__3 
       (.I0(mul_r_reg__1[25]),
        .I1(CO),
        .I2(D[25]),
        .O(\add_r[24]_i_4__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_5__3 
       (.I0(mul_r_reg__1[24]),
        .I1(CO),
        .I2(D[24]),
        .O(\add_r[24]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_2__3 
       (.I0(mul_r_reg__1[31]),
        .I1(CO),
        .I2(D[31]),
        .O(\add_r[28]_i_2__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_3__3 
       (.I0(mul_r_reg__1[30]),
        .I1(CO),
        .I2(D[30]),
        .O(\add_r[28]_i_3__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_4__3 
       (.I0(mul_r_reg__1[29]),
        .I1(CO),
        .I2(D[29]),
        .O(\add_r[28]_i_4__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_5__3 
       (.I0(mul_r_reg__1[28]),
        .I1(CO),
        .I2(D[28]),
        .O(\add_r[28]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_2__3 
       (.I0(mul_r_reg__1[35]),
        .I1(CO),
        .I2(D[35]),
        .O(\add_r[32]_i_2__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_3__3 
       (.I0(mul_r_reg__1[34]),
        .I1(CO),
        .I2(D[34]),
        .O(\add_r[32]_i_3__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_4__3 
       (.I0(mul_r_reg__1[33]),
        .I1(CO),
        .I2(D[33]),
        .O(\add_r[32]_i_4__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_5__3 
       (.I0(mul_r_reg__1[32]),
        .I1(CO),
        .I2(D[32]),
        .O(\add_r[32]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_2__3 
       (.I0(mul_r_reg__1[39]),
        .I1(CO),
        .I2(D[39]),
        .O(\add_r[36]_i_2__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_3__3 
       (.I0(mul_r_reg__1[38]),
        .I1(CO),
        .I2(D[38]),
        .O(\add_r[36]_i_3__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_4__3 
       (.I0(mul_r_reg__1[37]),
        .I1(CO),
        .I2(D[37]),
        .O(\add_r[36]_i_4__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_5__3 
       (.I0(mul_r_reg__1[36]),
        .I1(CO),
        .I2(D[36]),
        .O(\add_r[36]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_2__3 
       (.I0(mul_r_reg__1[43]),
        .I1(CO),
        .I2(D[43]),
        .O(\add_r[40]_i_2__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_3__3 
       (.I0(mul_r_reg__1[42]),
        .I1(CO),
        .I2(D[42]),
        .O(\add_r[40]_i_3__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_4__3 
       (.I0(mul_r_reg__1[41]),
        .I1(CO),
        .I2(D[41]),
        .O(\add_r[40]_i_4__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_5__3 
       (.I0(mul_r_reg__1[40]),
        .I1(CO),
        .I2(D[40]),
        .O(\add_r[40]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_2__3 
       (.I0(D[47]),
        .I1(CO),
        .I2(mul_r_reg__1[47]),
        .O(\add_r[44]_i_2__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_3__3 
       (.I0(mul_r_reg__1[46]),
        .I1(CO),
        .I2(D[46]),
        .O(\add_r[44]_i_3__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_4__3 
       (.I0(mul_r_reg__1[45]),
        .I1(CO),
        .I2(D[45]),
        .O(\add_r[44]_i_4__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_5__3 
       (.I0(mul_r_reg__1[44]),
        .I1(CO),
        .I2(D[44]),
        .O(\add_r[44]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_2__3 
       (.I0(mul_r_reg__1[7]),
        .I1(CO),
        .I2(D[7]),
        .O(\add_r[4]_i_2__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_3__3 
       (.I0(mul_r_reg__1[6]),
        .I1(CO),
        .I2(D[6]),
        .O(\add_r[4]_i_3__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_4__3 
       (.I0(mul_r_reg__1[5]),
        .I1(CO),
        .I2(D[5]),
        .O(\add_r[4]_i_4__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_5__3 
       (.I0(mul_r_reg__1[4]),
        .I1(CO),
        .I2(D[4]),
        .O(\add_r[4]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_2__3 
       (.I0(mul_r_reg__1[11]),
        .I1(CO),
        .I2(D[11]),
        .O(\add_r[8]_i_2__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_3__3 
       (.I0(mul_r_reg__1[10]),
        .I1(CO),
        .I2(D[10]),
        .O(\add_r[8]_i_3__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_4__3 
       (.I0(mul_r_reg__1[9]),
        .I1(CO),
        .I2(D[9]),
        .O(\add_r[8]_i_4__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_5__3 
       (.I0(mul_r_reg__1[8]),
        .I1(CO),
        .I2(D[8]),
        .O(\add_r[8]_i_5__3_n_0 ));
  FDRE \add_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__3_n_7 ),
        .Q(D[0]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[0]_i_1__3 
       (.CI(1'b0),
        .CO({\add_r_reg[0]_i_1__3_n_0 ,\add_r_reg[0]_i_1__3_n_1 ,\add_r_reg[0]_i_1__3_n_2 ,\add_r_reg[0]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[3:0]),
        .O({\add_r_reg[0]_i_1__3_n_4 ,\add_r_reg[0]_i_1__3_n_5 ,\add_r_reg[0]_i_1__3_n_6 ,\add_r_reg[0]_i_1__3_n_7 }),
        .S({\add_r[0]_i_2__3_n_0 ,\add_r[0]_i_3__3_n_0 ,\add_r[0]_i_4__3_n_0 ,\add_r[0]_i_5__3_n_0 }));
  FDRE \add_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__3_n_5 ),
        .Q(D[10]),
        .R(p_0_in__0));
  FDRE \add_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__3_n_4 ),
        .Q(D[11]),
        .R(p_0_in__0));
  FDRE \add_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__3_n_7 ),
        .Q(D[12]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[12]_i_1__3 
       (.CI(\add_r_reg[8]_i_1__3_n_0 ),
        .CO({\add_r_reg[12]_i_1__3_n_0 ,\add_r_reg[12]_i_1__3_n_1 ,\add_r_reg[12]_i_1__3_n_2 ,\add_r_reg[12]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[15:12]),
        .O({\add_r_reg[12]_i_1__3_n_4 ,\add_r_reg[12]_i_1__3_n_5 ,\add_r_reg[12]_i_1__3_n_6 ,\add_r_reg[12]_i_1__3_n_7 }),
        .S({\add_r[12]_i_2__3_n_0 ,\add_r[12]_i_3__3_n_0 ,\add_r[12]_i_4__3_n_0 ,\add_r[12]_i_5__3_n_0 }));
  FDRE \add_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__3_n_6 ),
        .Q(D[13]),
        .R(p_0_in__0));
  FDRE \add_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__3_n_5 ),
        .Q(D[14]),
        .R(p_0_in__0));
  FDRE \add_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__3_n_4 ),
        .Q(D[15]),
        .R(p_0_in__0));
  FDRE \add_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__3_n_7 ),
        .Q(D[16]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[16]_i_1__3 
       (.CI(\add_r_reg[12]_i_1__3_n_0 ),
        .CO({\add_r_reg[16]_i_1__3_n_0 ,\add_r_reg[16]_i_1__3_n_1 ,\add_r_reg[16]_i_1__3_n_2 ,\add_r_reg[16]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[19:16]),
        .O({\add_r_reg[16]_i_1__3_n_4 ,\add_r_reg[16]_i_1__3_n_5 ,\add_r_reg[16]_i_1__3_n_6 ,\add_r_reg[16]_i_1__3_n_7 }),
        .S({\add_r[16]_i_2__3_n_0 ,\add_r[16]_i_3__3_n_0 ,\add_r[16]_i_4__3_n_0 ,\add_r[16]_i_5__3_n_0 }));
  FDRE \add_r_reg[17] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__3_n_6 ),
        .Q(D[17]),
        .R(p_0_in__0));
  FDRE \add_r_reg[18] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__3_n_5 ),
        .Q(D[18]),
        .R(p_0_in__0));
  FDRE \add_r_reg[19] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__3_n_4 ),
        .Q(D[19]),
        .R(p_0_in__0));
  FDRE \add_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__3_n_6 ),
        .Q(D[1]),
        .R(p_0_in__0));
  FDRE \add_r_reg[20] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__3_n_7 ),
        .Q(D[20]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[20]_i_1__3 
       (.CI(\add_r_reg[16]_i_1__3_n_0 ),
        .CO({\add_r_reg[20]_i_1__3_n_0 ,\add_r_reg[20]_i_1__3_n_1 ,\add_r_reg[20]_i_1__3_n_2 ,\add_r_reg[20]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[23:20]),
        .O({\add_r_reg[20]_i_1__3_n_4 ,\add_r_reg[20]_i_1__3_n_5 ,\add_r_reg[20]_i_1__3_n_6 ,\add_r_reg[20]_i_1__3_n_7 }),
        .S({\add_r[20]_i_2__3_n_0 ,\add_r[20]_i_3__3_n_0 ,\add_r[20]_i_4__3_n_0 ,\add_r[20]_i_5__3_n_0 }));
  FDRE \add_r_reg[21] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__3_n_6 ),
        .Q(D[21]),
        .R(p_0_in__0));
  FDRE \add_r_reg[22] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__3_n_5 ),
        .Q(D[22]),
        .R(p_0_in__0));
  FDRE \add_r_reg[23] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__3_n_4 ),
        .Q(D[23]),
        .R(p_0_in__0));
  FDRE \add_r_reg[24] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__3_n_7 ),
        .Q(D[24]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[24]_i_1__3 
       (.CI(\add_r_reg[20]_i_1__3_n_0 ),
        .CO({\add_r_reg[24]_i_1__3_n_0 ,\add_r_reg[24]_i_1__3_n_1 ,\add_r_reg[24]_i_1__3_n_2 ,\add_r_reg[24]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[27:24]),
        .O({\add_r_reg[24]_i_1__3_n_4 ,\add_r_reg[24]_i_1__3_n_5 ,\add_r_reg[24]_i_1__3_n_6 ,\add_r_reg[24]_i_1__3_n_7 }),
        .S({\add_r[24]_i_2__3_n_0 ,\add_r[24]_i_3__3_n_0 ,\add_r[24]_i_4__3_n_0 ,\add_r[24]_i_5__3_n_0 }));
  FDRE \add_r_reg[25] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__3_n_6 ),
        .Q(D[25]),
        .R(p_0_in__0));
  FDRE \add_r_reg[26] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__3_n_5 ),
        .Q(D[26]),
        .R(p_0_in__0));
  FDRE \add_r_reg[27] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__3_n_4 ),
        .Q(D[27]),
        .R(p_0_in__0));
  FDRE \add_r_reg[28] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__3_n_7 ),
        .Q(D[28]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[28]_i_1__3 
       (.CI(\add_r_reg[24]_i_1__3_n_0 ),
        .CO({\add_r_reg[28]_i_1__3_n_0 ,\add_r_reg[28]_i_1__3_n_1 ,\add_r_reg[28]_i_1__3_n_2 ,\add_r_reg[28]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[31:28]),
        .O({\add_r_reg[28]_i_1__3_n_4 ,\add_r_reg[28]_i_1__3_n_5 ,\add_r_reg[28]_i_1__3_n_6 ,\add_r_reg[28]_i_1__3_n_7 }),
        .S({\add_r[28]_i_2__3_n_0 ,\add_r[28]_i_3__3_n_0 ,\add_r[28]_i_4__3_n_0 ,\add_r[28]_i_5__3_n_0 }));
  FDRE \add_r_reg[29] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__3_n_6 ),
        .Q(D[29]),
        .R(p_0_in__0));
  FDRE \add_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__3_n_5 ),
        .Q(D[2]),
        .R(p_0_in__0));
  FDRE \add_r_reg[30] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__3_n_5 ),
        .Q(D[30]),
        .R(p_0_in__0));
  FDRE \add_r_reg[31] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__3_n_4 ),
        .Q(D[31]),
        .R(p_0_in__0));
  FDRE \add_r_reg[32] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__3_n_7 ),
        .Q(D[32]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[32]_i_1__3 
       (.CI(\add_r_reg[28]_i_1__3_n_0 ),
        .CO({\add_r_reg[32]_i_1__3_n_0 ,\add_r_reg[32]_i_1__3_n_1 ,\add_r_reg[32]_i_1__3_n_2 ,\add_r_reg[32]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[35:32]),
        .O({\add_r_reg[32]_i_1__3_n_4 ,\add_r_reg[32]_i_1__3_n_5 ,\add_r_reg[32]_i_1__3_n_6 ,\add_r_reg[32]_i_1__3_n_7 }),
        .S({\add_r[32]_i_2__3_n_0 ,\add_r[32]_i_3__3_n_0 ,\add_r[32]_i_4__3_n_0 ,\add_r[32]_i_5__3_n_0 }));
  FDRE \add_r_reg[33] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__3_n_6 ),
        .Q(D[33]),
        .R(p_0_in__0));
  FDRE \add_r_reg[34] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__3_n_5 ),
        .Q(D[34]),
        .R(p_0_in__0));
  FDRE \add_r_reg[35] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__3_n_4 ),
        .Q(D[35]),
        .R(p_0_in__0));
  FDRE \add_r_reg[36] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__3_n_7 ),
        .Q(D[36]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[36]_i_1__3 
       (.CI(\add_r_reg[32]_i_1__3_n_0 ),
        .CO({\add_r_reg[36]_i_1__3_n_0 ,\add_r_reg[36]_i_1__3_n_1 ,\add_r_reg[36]_i_1__3_n_2 ,\add_r_reg[36]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[39:36]),
        .O({\add_r_reg[36]_i_1__3_n_4 ,\add_r_reg[36]_i_1__3_n_5 ,\add_r_reg[36]_i_1__3_n_6 ,\add_r_reg[36]_i_1__3_n_7 }),
        .S({\add_r[36]_i_2__3_n_0 ,\add_r[36]_i_3__3_n_0 ,\add_r[36]_i_4__3_n_0 ,\add_r[36]_i_5__3_n_0 }));
  FDRE \add_r_reg[37] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__3_n_6 ),
        .Q(D[37]),
        .R(p_0_in__0));
  FDRE \add_r_reg[38] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__3_n_5 ),
        .Q(D[38]),
        .R(p_0_in__0));
  FDRE \add_r_reg[39] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__3_n_4 ),
        .Q(D[39]),
        .R(p_0_in__0));
  FDRE \add_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__3_n_4 ),
        .Q(D[3]),
        .R(p_0_in__0));
  FDRE \add_r_reg[40] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__3_n_7 ),
        .Q(D[40]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[40]_i_1__3 
       (.CI(\add_r_reg[36]_i_1__3_n_0 ),
        .CO({\add_r_reg[40]_i_1__3_n_0 ,\add_r_reg[40]_i_1__3_n_1 ,\add_r_reg[40]_i_1__3_n_2 ,\add_r_reg[40]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[43:40]),
        .O({\add_r_reg[40]_i_1__3_n_4 ,\add_r_reg[40]_i_1__3_n_5 ,\add_r_reg[40]_i_1__3_n_6 ,\add_r_reg[40]_i_1__3_n_7 }),
        .S({\add_r[40]_i_2__3_n_0 ,\add_r[40]_i_3__3_n_0 ,\add_r[40]_i_4__3_n_0 ,\add_r[40]_i_5__3_n_0 }));
  FDRE \add_r_reg[41] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__3_n_6 ),
        .Q(D[41]),
        .R(p_0_in__0));
  FDRE \add_r_reg[42] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__3_n_5 ),
        .Q(D[42]),
        .R(p_0_in__0));
  FDRE \add_r_reg[43] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__3_n_4 ),
        .Q(D[43]),
        .R(p_0_in__0));
  FDRE \add_r_reg[44] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__3_n_7 ),
        .Q(D[44]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[44]_i_1__3 
       (.CI(\add_r_reg[40]_i_1__3_n_0 ),
        .CO({\NLW_add_r_reg[44]_i_1__3_CO_UNCONNECTED [3],\add_r_reg[44]_i_1__3_n_1 ,\add_r_reg[44]_i_1__3_n_2 ,\add_r_reg[44]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,mul_r_reg__1[46:44]}),
        .O({\add_r_reg[44]_i_1__3_n_4 ,\add_r_reg[44]_i_1__3_n_5 ,\add_r_reg[44]_i_1__3_n_6 ,\add_r_reg[44]_i_1__3_n_7 }),
        .S({\add_r[44]_i_2__3_n_0 ,\add_r[44]_i_3__3_n_0 ,\add_r[44]_i_4__3_n_0 ,\add_r[44]_i_5__3_n_0 }));
  FDRE \add_r_reg[45] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__3_n_6 ),
        .Q(D[45]),
        .R(p_0_in__0));
  FDRE \add_r_reg[46] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__3_n_5 ),
        .Q(D[46]),
        .R(p_0_in__0));
  FDRE \add_r_reg[47] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__3_n_4 ),
        .Q(D[47]),
        .R(p_0_in__0));
  FDRE \add_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__3_n_7 ),
        .Q(D[4]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[4]_i_1__3 
       (.CI(\add_r_reg[0]_i_1__3_n_0 ),
        .CO({\add_r_reg[4]_i_1__3_n_0 ,\add_r_reg[4]_i_1__3_n_1 ,\add_r_reg[4]_i_1__3_n_2 ,\add_r_reg[4]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[7:4]),
        .O({\add_r_reg[4]_i_1__3_n_4 ,\add_r_reg[4]_i_1__3_n_5 ,\add_r_reg[4]_i_1__3_n_6 ,\add_r_reg[4]_i_1__3_n_7 }),
        .S({\add_r[4]_i_2__3_n_0 ,\add_r[4]_i_3__3_n_0 ,\add_r[4]_i_4__3_n_0 ,\add_r[4]_i_5__3_n_0 }));
  FDRE \add_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__3_n_6 ),
        .Q(D[5]),
        .R(p_0_in__0));
  FDRE \add_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__3_n_5 ),
        .Q(D[6]),
        .R(p_0_in__0));
  FDRE \add_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__3_n_4 ),
        .Q(D[7]),
        .R(p_0_in__0));
  FDRE \add_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__3_n_7 ),
        .Q(D[8]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[8]_i_1__3 
       (.CI(\add_r_reg[4]_i_1__3_n_0 ),
        .CO({\add_r_reg[8]_i_1__3_n_0 ,\add_r_reg[8]_i_1__3_n_1 ,\add_r_reg[8]_i_1__3_n_2 ,\add_r_reg[8]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[11:8]),
        .O({\add_r_reg[8]_i_1__3_n_4 ,\add_r_reg[8]_i_1__3_n_5 ,\add_r_reg[8]_i_1__3_n_6 ,\add_r_reg[8]_i_1__3_n_7 }),
        .S({\add_r[8]_i_2__3_n_0 ,\add_r[8]_i_3__3_n_0 ,\add_r[8]_i_4__3_n_0 ,\add_r[8]_i_5__3_n_0 }));
  FDRE \add_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__3_n_6 ),
        .Q(D[9]),
        .R(p_0_in__0));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter[0]_i_1__6 
       (.I0(\counter_reg_n_0_[0] ),
        .O(\counter[0]_i_1__6_n_0 ));
  LUT4 #(
    .INIT(16'hF7C8)) 
    \counter[1]_i_1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(dp_enable),
        .I2(CO),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \counter[2]_i_1__5 
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[2] ),
        .O(\counter[2]_i_1__5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \counter[3]_i_1__5 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .O(\counter[3]_i_1__5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \counter[4]_i_1__4 
       (.I0(\counter_reg_n_0_[3] ),
        .I1(\counter_reg_n_0_[2] ),
        .I2(\counter_reg_n_0_[1] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[4] ),
        .O(\counter[4]_i_1__4_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \counter[5]_i_1__4 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .I4(\counter_reg_n_0_[4] ),
        .I5(\counter_reg_n_0_[5] ),
        .O(\counter[5]_i_1__4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \counter[6]_i_1__3 
       (.I0(\counter[8]_i_3__4_n_0 ),
        .I1(\counter_reg_n_0_[6] ),
        .O(\counter[6]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hB4)) 
    \counter[7]_i_1__3 
       (.I0(\counter[8]_i_3__4_n_0 ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(\counter_reg_n_0_[7] ),
        .O(\counter[7]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT4 #(
    .INIT(16'hF708)) 
    \counter[8]_i_2__4 
       (.I0(\counter_reg_n_0_[7] ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(\counter[8]_i_3__4_n_0 ),
        .I3(\counter_reg_n_0_[8] ),
        .O(\counter[8]_i_2__4_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \counter[8]_i_3__4 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .I4(\counter_reg_n_0_[4] ),
        .I5(\counter_reg_n_0_[5] ),
        .O(\counter[8]_i_3__4_n_0 ));
  FDRE \counter_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[0]_i_1__6_n_0 ),
        .Q(\counter_reg_n_0_[0] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\counter[1]_i_1_n_0 ),
        .Q(\counter_reg_n_0_[1] ),
        .R(p_0_in__0));
  FDRE \counter_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[2]_i_1__5_n_0 ),
        .Q(\counter_reg_n_0_[2] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[3]_i_1__5_n_0 ),
        .Q(\counter_reg_n_0_[3] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[4]_i_1__4_n_0 ),
        .Q(\counter_reg_n_0_[4] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[5]_i_1__4_n_0 ),
        .Q(\counter_reg_n_0_[5] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[6]_i_1__3_n_0 ),
        .Q(\counter_reg_n_0_[6] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[7]_i_1__3_n_0 ),
        .Q(\counter_reg_n_0_[7] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[8]_i_2__4_n_0 ),
        .Q(\counter_reg_n_0_[8] ),
        .R(\slv_regs_reg[0][12] ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,b_ram_out[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({B[16],B}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r0_OVERFLOW_UNCONNECTED),
        .P({mul_r0_n_58,mul_r0_n_59,mul_r0_n_60,mul_r0_n_61,mul_r0_n_62,mul_r0_n_63,mul_r0_n_64,mul_r0_n_65,mul_r0_n_66,mul_r0_n_67,mul_r0_n_68,mul_r0_n_69,mul_r0_n_70,mul_r0_n_71,mul_r0_n_72,mul_r0_n_73,mul_r0_n_74,mul_r0_n_75,mul_r0_n_76,mul_r0_n_77,mul_r0_n_78,mul_r0_n_79,mul_r0_n_80,mul_r0_n_81,mul_r0_n_82,mul_r0_n_83,mul_r0_n_84,mul_r0_n_85,mul_r0_n_86,mul_r0_n_87,mul_r0_n_88,mul_r0_n_89,mul_r0_n_90,mul_r0_n_91,mul_r0_n_92,mul_r0_n_93,mul_r0_n_94,mul_r0_n_95,mul_r0_n_96,mul_r0_n_97,mul_r0_n_98,mul_r0_n_99,mul_r0_n_100,mul_r0_n_101,mul_r0_n_102,mul_r0_n_103,mul_r0_n_104,mul_r0_n_105}),
        .PATTERNBDETECT(NLW_mul_r0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_r0_UNDERFLOW_UNCONNECTED));
  FDRE \mul_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_105),
        .Q(mul_r_reg__1[0]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_95),
        .Q(mul_r_reg__1[10]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_94),
        .Q(mul_r_reg__1[11]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_93),
        .Q(mul_r_reg__1[12]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_92),
        .Q(mul_r_reg__1[13]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_91),
        .Q(mul_r_reg__1[14]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_90),
        .Q(mul_r_reg__1[15]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_89),
        .Q(mul_r_reg__1[16]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_104),
        .Q(mul_r_reg__1[1]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_103),
        .Q(mul_r_reg__1[2]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_102),
        .Q(mul_r_reg__1[3]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_101),
        .Q(mul_r_reg__1[4]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_100),
        .Q(mul_r_reg__1[5]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_99),
        .Q(mul_r_reg__1[6]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_98),
        .Q(mul_r_reg__1[7]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_97),
        .Q(mul_r_reg__1[8]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_96),
        .Q(mul_r_reg__1[9]),
        .R(p_0_in__0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-12 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r_reg__0
       (.A({B[15],B[15],B[15],B[15],B[15],B[15],B[15],B[15],B[15],\in_raw_delay_reg[15]_rep__5 ,\in_raw_delay_reg[15]_rep__5 ,\in_raw_delay_reg[15]_rep__5 ,\in_raw_delay_reg[15]_rep__5 ,\in_raw_delay_reg[15]_rep__5 ,\in_raw_delay_reg[15]_rep__5 ,B[14:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r_reg__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({b_ram_out[31],b_ram_out[31],b_ram_out[31],b_ram_out[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r_reg__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(dp_enable),
        .CLK(clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED),
        .P({mul_r_reg__0_n_58,mul_r_reg__0_n_59,mul_r_reg__0_n_60,mul_r_reg__0_n_61,mul_r_reg__0_n_62,mul_r_reg__0_n_63,mul_r_reg__0_n_64,mul_r_reg__0_n_65,mul_r_reg__0_n_66,mul_r_reg__0_n_67,mul_r_reg__0_n_68,mul_r_reg__0_n_69,mul_r_reg__0_n_70,mul_r_reg__0_n_71,mul_r_reg__0_n_72,mul_r_reg__0_n_73,mul_r_reg__0_n_74,mul_r_reg__1[47:17]}),
        .PATTERNBDETECT(NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .PCOUT(NLW_mul_r_reg__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(p_0_in__0),
        .UNDERFLOW(NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "dot_product" *) 
module design_1_top_0_0_dot_product_4
   (D,
    CO,
    A,
    b_ram_out,
    dp_enable,
    clk,
    p_0_in__0,
    \slv_regs_reg[0][11] ,
    O,
    \slv_regs_reg[0][8] ,
    \slv_regs_reg[0][0] ,
    \slv_regs_reg[0][12] );
  output [47:0]D;
  output [0:0]CO;
  input [17:0]A;
  input [31:0]b_ram_out;
  input dp_enable;
  input clk;
  input p_0_in__0;
  input [0:0]\slv_regs_reg[0][11] ;
  input [0:0]O;
  input [7:0]\slv_regs_reg[0][8] ;
  input [0:0]\slv_regs_reg[0][0] ;
  input [0:0]\slv_regs_reg[0][12] ;

  wire [17:0]A;
  wire [0:0]CO;
  wire [47:0]D;
  wire [0:0]O;
  wire add_r1_carry_i_2__4_n_0;
  wire add_r1_carry_i_3__4_n_0;
  wire add_r1_carry_i_4__4_n_0;
  wire add_r1_carry_n_0;
  wire add_r1_carry_n_1;
  wire add_r1_carry_n_2;
  wire add_r1_carry_n_3;
  wire \add_r[0]_i_2__4_n_0 ;
  wire \add_r[0]_i_3__4_n_0 ;
  wire \add_r[0]_i_4__4_n_0 ;
  wire \add_r[0]_i_5__4_n_0 ;
  wire \add_r[12]_i_2__4_n_0 ;
  wire \add_r[12]_i_3__4_n_0 ;
  wire \add_r[12]_i_4__4_n_0 ;
  wire \add_r[12]_i_5__4_n_0 ;
  wire \add_r[16]_i_2__4_n_0 ;
  wire \add_r[16]_i_3__4_n_0 ;
  wire \add_r[16]_i_4__4_n_0 ;
  wire \add_r[16]_i_5__4_n_0 ;
  wire \add_r[20]_i_2__4_n_0 ;
  wire \add_r[20]_i_3__4_n_0 ;
  wire \add_r[20]_i_4__4_n_0 ;
  wire \add_r[20]_i_5__4_n_0 ;
  wire \add_r[24]_i_2__4_n_0 ;
  wire \add_r[24]_i_3__4_n_0 ;
  wire \add_r[24]_i_4__4_n_0 ;
  wire \add_r[24]_i_5__4_n_0 ;
  wire \add_r[28]_i_2__4_n_0 ;
  wire \add_r[28]_i_3__4_n_0 ;
  wire \add_r[28]_i_4__4_n_0 ;
  wire \add_r[28]_i_5__4_n_0 ;
  wire \add_r[32]_i_2__4_n_0 ;
  wire \add_r[32]_i_3__4_n_0 ;
  wire \add_r[32]_i_4__4_n_0 ;
  wire \add_r[32]_i_5__4_n_0 ;
  wire \add_r[36]_i_2__4_n_0 ;
  wire \add_r[36]_i_3__4_n_0 ;
  wire \add_r[36]_i_4__4_n_0 ;
  wire \add_r[36]_i_5__4_n_0 ;
  wire \add_r[40]_i_2__4_n_0 ;
  wire \add_r[40]_i_3__4_n_0 ;
  wire \add_r[40]_i_4__4_n_0 ;
  wire \add_r[40]_i_5__4_n_0 ;
  wire \add_r[44]_i_2__4_n_0 ;
  wire \add_r[44]_i_3__4_n_0 ;
  wire \add_r[44]_i_4__4_n_0 ;
  wire \add_r[44]_i_5__4_n_0 ;
  wire \add_r[4]_i_2__4_n_0 ;
  wire \add_r[4]_i_3__4_n_0 ;
  wire \add_r[4]_i_4__4_n_0 ;
  wire \add_r[4]_i_5__4_n_0 ;
  wire \add_r[8]_i_2__4_n_0 ;
  wire \add_r[8]_i_3__4_n_0 ;
  wire \add_r[8]_i_4__4_n_0 ;
  wire \add_r[8]_i_5__4_n_0 ;
  wire \add_r_reg[0]_i_1__4_n_0 ;
  wire \add_r_reg[0]_i_1__4_n_1 ;
  wire \add_r_reg[0]_i_1__4_n_2 ;
  wire \add_r_reg[0]_i_1__4_n_3 ;
  wire \add_r_reg[0]_i_1__4_n_4 ;
  wire \add_r_reg[0]_i_1__4_n_5 ;
  wire \add_r_reg[0]_i_1__4_n_6 ;
  wire \add_r_reg[0]_i_1__4_n_7 ;
  wire \add_r_reg[12]_i_1__4_n_0 ;
  wire \add_r_reg[12]_i_1__4_n_1 ;
  wire \add_r_reg[12]_i_1__4_n_2 ;
  wire \add_r_reg[12]_i_1__4_n_3 ;
  wire \add_r_reg[12]_i_1__4_n_4 ;
  wire \add_r_reg[12]_i_1__4_n_5 ;
  wire \add_r_reg[12]_i_1__4_n_6 ;
  wire \add_r_reg[12]_i_1__4_n_7 ;
  wire \add_r_reg[16]_i_1__4_n_0 ;
  wire \add_r_reg[16]_i_1__4_n_1 ;
  wire \add_r_reg[16]_i_1__4_n_2 ;
  wire \add_r_reg[16]_i_1__4_n_3 ;
  wire \add_r_reg[16]_i_1__4_n_4 ;
  wire \add_r_reg[16]_i_1__4_n_5 ;
  wire \add_r_reg[16]_i_1__4_n_6 ;
  wire \add_r_reg[16]_i_1__4_n_7 ;
  wire \add_r_reg[20]_i_1__4_n_0 ;
  wire \add_r_reg[20]_i_1__4_n_1 ;
  wire \add_r_reg[20]_i_1__4_n_2 ;
  wire \add_r_reg[20]_i_1__4_n_3 ;
  wire \add_r_reg[20]_i_1__4_n_4 ;
  wire \add_r_reg[20]_i_1__4_n_5 ;
  wire \add_r_reg[20]_i_1__4_n_6 ;
  wire \add_r_reg[20]_i_1__4_n_7 ;
  wire \add_r_reg[24]_i_1__4_n_0 ;
  wire \add_r_reg[24]_i_1__4_n_1 ;
  wire \add_r_reg[24]_i_1__4_n_2 ;
  wire \add_r_reg[24]_i_1__4_n_3 ;
  wire \add_r_reg[24]_i_1__4_n_4 ;
  wire \add_r_reg[24]_i_1__4_n_5 ;
  wire \add_r_reg[24]_i_1__4_n_6 ;
  wire \add_r_reg[24]_i_1__4_n_7 ;
  wire \add_r_reg[28]_i_1__4_n_0 ;
  wire \add_r_reg[28]_i_1__4_n_1 ;
  wire \add_r_reg[28]_i_1__4_n_2 ;
  wire \add_r_reg[28]_i_1__4_n_3 ;
  wire \add_r_reg[28]_i_1__4_n_4 ;
  wire \add_r_reg[28]_i_1__4_n_5 ;
  wire \add_r_reg[28]_i_1__4_n_6 ;
  wire \add_r_reg[28]_i_1__4_n_7 ;
  wire \add_r_reg[32]_i_1__4_n_0 ;
  wire \add_r_reg[32]_i_1__4_n_1 ;
  wire \add_r_reg[32]_i_1__4_n_2 ;
  wire \add_r_reg[32]_i_1__4_n_3 ;
  wire \add_r_reg[32]_i_1__4_n_4 ;
  wire \add_r_reg[32]_i_1__4_n_5 ;
  wire \add_r_reg[32]_i_1__4_n_6 ;
  wire \add_r_reg[32]_i_1__4_n_7 ;
  wire \add_r_reg[36]_i_1__4_n_0 ;
  wire \add_r_reg[36]_i_1__4_n_1 ;
  wire \add_r_reg[36]_i_1__4_n_2 ;
  wire \add_r_reg[36]_i_1__4_n_3 ;
  wire \add_r_reg[36]_i_1__4_n_4 ;
  wire \add_r_reg[36]_i_1__4_n_5 ;
  wire \add_r_reg[36]_i_1__4_n_6 ;
  wire \add_r_reg[36]_i_1__4_n_7 ;
  wire \add_r_reg[40]_i_1__4_n_0 ;
  wire \add_r_reg[40]_i_1__4_n_1 ;
  wire \add_r_reg[40]_i_1__4_n_2 ;
  wire \add_r_reg[40]_i_1__4_n_3 ;
  wire \add_r_reg[40]_i_1__4_n_4 ;
  wire \add_r_reg[40]_i_1__4_n_5 ;
  wire \add_r_reg[40]_i_1__4_n_6 ;
  wire \add_r_reg[40]_i_1__4_n_7 ;
  wire \add_r_reg[44]_i_1__4_n_1 ;
  wire \add_r_reg[44]_i_1__4_n_2 ;
  wire \add_r_reg[44]_i_1__4_n_3 ;
  wire \add_r_reg[44]_i_1__4_n_4 ;
  wire \add_r_reg[44]_i_1__4_n_5 ;
  wire \add_r_reg[44]_i_1__4_n_6 ;
  wire \add_r_reg[44]_i_1__4_n_7 ;
  wire \add_r_reg[4]_i_1__4_n_0 ;
  wire \add_r_reg[4]_i_1__4_n_1 ;
  wire \add_r_reg[4]_i_1__4_n_2 ;
  wire \add_r_reg[4]_i_1__4_n_3 ;
  wire \add_r_reg[4]_i_1__4_n_4 ;
  wire \add_r_reg[4]_i_1__4_n_5 ;
  wire \add_r_reg[4]_i_1__4_n_6 ;
  wire \add_r_reg[4]_i_1__4_n_7 ;
  wire \add_r_reg[8]_i_1__4_n_0 ;
  wire \add_r_reg[8]_i_1__4_n_1 ;
  wire \add_r_reg[8]_i_1__4_n_2 ;
  wire \add_r_reg[8]_i_1__4_n_3 ;
  wire \add_r_reg[8]_i_1__4_n_4 ;
  wire \add_r_reg[8]_i_1__4_n_5 ;
  wire \add_r_reg[8]_i_1__4_n_6 ;
  wire \add_r_reg[8]_i_1__4_n_7 ;
  wire [31:0]b_ram_out;
  wire clk;
  wire \counter[0]_i_1__7_n_0 ;
  wire \counter[1]_i_1_n_0 ;
  wire \counter[2]_i_1__6_n_0 ;
  wire \counter[3]_i_1__6_n_0 ;
  wire \counter[4]_i_1__5_n_0 ;
  wire \counter[5]_i_1__5_n_0 ;
  wire \counter[6]_i_1__4_n_0 ;
  wire \counter[7]_i_1__4_n_0 ;
  wire \counter[8]_i_2__5_n_0 ;
  wire \counter[8]_i_3__5_n_0 ;
  wire \counter_reg_n_0_[0] ;
  wire \counter_reg_n_0_[1] ;
  wire \counter_reg_n_0_[2] ;
  wire \counter_reg_n_0_[3] ;
  wire \counter_reg_n_0_[4] ;
  wire \counter_reg_n_0_[5] ;
  wire \counter_reg_n_0_[6] ;
  wire \counter_reg_n_0_[7] ;
  wire \counter_reg_n_0_[8] ;
  wire dp_enable;
  wire mul_r0_n_100;
  wire mul_r0_n_101;
  wire mul_r0_n_102;
  wire mul_r0_n_103;
  wire mul_r0_n_104;
  wire mul_r0_n_105;
  wire mul_r0_n_106;
  wire mul_r0_n_107;
  wire mul_r0_n_108;
  wire mul_r0_n_109;
  wire mul_r0_n_110;
  wire mul_r0_n_111;
  wire mul_r0_n_112;
  wire mul_r0_n_113;
  wire mul_r0_n_114;
  wire mul_r0_n_115;
  wire mul_r0_n_116;
  wire mul_r0_n_117;
  wire mul_r0_n_118;
  wire mul_r0_n_119;
  wire mul_r0_n_120;
  wire mul_r0_n_121;
  wire mul_r0_n_122;
  wire mul_r0_n_123;
  wire mul_r0_n_124;
  wire mul_r0_n_125;
  wire mul_r0_n_126;
  wire mul_r0_n_127;
  wire mul_r0_n_128;
  wire mul_r0_n_129;
  wire mul_r0_n_130;
  wire mul_r0_n_131;
  wire mul_r0_n_132;
  wire mul_r0_n_133;
  wire mul_r0_n_134;
  wire mul_r0_n_135;
  wire mul_r0_n_136;
  wire mul_r0_n_137;
  wire mul_r0_n_138;
  wire mul_r0_n_139;
  wire mul_r0_n_140;
  wire mul_r0_n_141;
  wire mul_r0_n_142;
  wire mul_r0_n_143;
  wire mul_r0_n_144;
  wire mul_r0_n_145;
  wire mul_r0_n_146;
  wire mul_r0_n_147;
  wire mul_r0_n_148;
  wire mul_r0_n_149;
  wire mul_r0_n_150;
  wire mul_r0_n_151;
  wire mul_r0_n_152;
  wire mul_r0_n_153;
  wire mul_r0_n_58;
  wire mul_r0_n_59;
  wire mul_r0_n_60;
  wire mul_r0_n_61;
  wire mul_r0_n_62;
  wire mul_r0_n_63;
  wire mul_r0_n_64;
  wire mul_r0_n_65;
  wire mul_r0_n_66;
  wire mul_r0_n_67;
  wire mul_r0_n_68;
  wire mul_r0_n_69;
  wire mul_r0_n_70;
  wire mul_r0_n_71;
  wire mul_r0_n_72;
  wire mul_r0_n_73;
  wire mul_r0_n_74;
  wire mul_r0_n_75;
  wire mul_r0_n_76;
  wire mul_r0_n_77;
  wire mul_r0_n_78;
  wire mul_r0_n_79;
  wire mul_r0_n_80;
  wire mul_r0_n_81;
  wire mul_r0_n_82;
  wire mul_r0_n_83;
  wire mul_r0_n_84;
  wire mul_r0_n_85;
  wire mul_r0_n_86;
  wire mul_r0_n_87;
  wire mul_r0_n_88;
  wire mul_r0_n_89;
  wire mul_r0_n_90;
  wire mul_r0_n_91;
  wire mul_r0_n_92;
  wire mul_r0_n_93;
  wire mul_r0_n_94;
  wire mul_r0_n_95;
  wire mul_r0_n_96;
  wire mul_r0_n_97;
  wire mul_r0_n_98;
  wire mul_r0_n_99;
  wire mul_r_reg__0_n_58;
  wire mul_r_reg__0_n_59;
  wire mul_r_reg__0_n_60;
  wire mul_r_reg__0_n_61;
  wire mul_r_reg__0_n_62;
  wire mul_r_reg__0_n_63;
  wire mul_r_reg__0_n_64;
  wire mul_r_reg__0_n_65;
  wire mul_r_reg__0_n_66;
  wire mul_r_reg__0_n_67;
  wire mul_r_reg__0_n_68;
  wire mul_r_reg__0_n_69;
  wire mul_r_reg__0_n_70;
  wire mul_r_reg__0_n_71;
  wire mul_r_reg__0_n_72;
  wire mul_r_reg__0_n_73;
  wire mul_r_reg__0_n_74;
  wire [47:0]mul_r_reg__1;
  wire p_0_in__0;
  wire [0:0]\slv_regs_reg[0][0] ;
  wire [0:0]\slv_regs_reg[0][11] ;
  wire [0:0]\slv_regs_reg[0][12] ;
  wire [7:0]\slv_regs_reg[0][8] ;
  wire [3:0]NLW_add_r1_carry_O_UNCONNECTED;
  wire [3:1]NLW_add_r1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_add_r1_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_add_r_reg[44]_i_1__4_CO_UNCONNECTED ;
  wire NLW_mul_r0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r0_CARRYOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r_reg__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r_reg__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_r_reg__0_PCOUT_UNCONNECTED;

  CARRY4 add_r1_carry
       (.CI(1'b0),
        .CO({add_r1_carry_n_0,add_r1_carry_n_1,add_r1_carry_n_2,add_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry_O_UNCONNECTED[3:0]),
        .S({\slv_regs_reg[0][11] ,add_r1_carry_i_2__4_n_0,add_r1_carry_i_3__4_n_0,add_r1_carry_i_4__4_n_0}));
  CARRY4 add_r1_carry__0
       (.CI(add_r1_carry_n_0),
        .CO({NLW_add_r1_carry__0_CO_UNCONNECTED[3:1],CO}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,O}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_2__4
       (.I0(\counter_reg_n_0_[7] ),
        .I1(\slv_regs_reg[0][8] [6]),
        .I2(\counter_reg_n_0_[6] ),
        .I3(\slv_regs_reg[0][8] [5]),
        .I4(\slv_regs_reg[0][8] [7]),
        .I5(\counter_reg_n_0_[8] ),
        .O(add_r1_carry_i_2__4_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_3__4
       (.I0(\counter_reg_n_0_[4] ),
        .I1(\slv_regs_reg[0][8] [3]),
        .I2(\counter_reg_n_0_[3] ),
        .I3(\slv_regs_reg[0][8] [2]),
        .I4(\slv_regs_reg[0][8] [4]),
        .I5(\counter_reg_n_0_[5] ),
        .O(add_r1_carry_i_3__4_n_0));
  LUT6 #(
    .INIT(64'h0990000000000990)) 
    add_r1_carry_i_4__4
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\slv_regs_reg[0][8] [0]),
        .I2(\slv_regs_reg[0][0] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\slv_regs_reg[0][8] [1]),
        .I5(\counter_reg_n_0_[2] ),
        .O(add_r1_carry_i_4__4_n_0));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_2__4 
       (.I0(mul_r_reg__1[3]),
        .I1(CO),
        .I2(D[3]),
        .O(\add_r[0]_i_2__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_3__4 
       (.I0(mul_r_reg__1[2]),
        .I1(CO),
        .I2(D[2]),
        .O(\add_r[0]_i_3__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_4__4 
       (.I0(mul_r_reg__1[1]),
        .I1(CO),
        .I2(D[1]),
        .O(\add_r[0]_i_4__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_5__4 
       (.I0(mul_r_reg__1[0]),
        .I1(CO),
        .I2(D[0]),
        .O(\add_r[0]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_2__4 
       (.I0(mul_r_reg__1[15]),
        .I1(CO),
        .I2(D[15]),
        .O(\add_r[12]_i_2__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_3__4 
       (.I0(mul_r_reg__1[14]),
        .I1(CO),
        .I2(D[14]),
        .O(\add_r[12]_i_3__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_4__4 
       (.I0(mul_r_reg__1[13]),
        .I1(CO),
        .I2(D[13]),
        .O(\add_r[12]_i_4__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_5__4 
       (.I0(mul_r_reg__1[12]),
        .I1(CO),
        .I2(D[12]),
        .O(\add_r[12]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_2__4 
       (.I0(mul_r_reg__1[19]),
        .I1(CO),
        .I2(D[19]),
        .O(\add_r[16]_i_2__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_3__4 
       (.I0(mul_r_reg__1[18]),
        .I1(CO),
        .I2(D[18]),
        .O(\add_r[16]_i_3__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_4__4 
       (.I0(mul_r_reg__1[17]),
        .I1(CO),
        .I2(D[17]),
        .O(\add_r[16]_i_4__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_5__4 
       (.I0(mul_r_reg__1[16]),
        .I1(CO),
        .I2(D[16]),
        .O(\add_r[16]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_2__4 
       (.I0(mul_r_reg__1[23]),
        .I1(CO),
        .I2(D[23]),
        .O(\add_r[20]_i_2__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_3__4 
       (.I0(mul_r_reg__1[22]),
        .I1(CO),
        .I2(D[22]),
        .O(\add_r[20]_i_3__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_4__4 
       (.I0(mul_r_reg__1[21]),
        .I1(CO),
        .I2(D[21]),
        .O(\add_r[20]_i_4__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_5__4 
       (.I0(mul_r_reg__1[20]),
        .I1(CO),
        .I2(D[20]),
        .O(\add_r[20]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_2__4 
       (.I0(mul_r_reg__1[27]),
        .I1(CO),
        .I2(D[27]),
        .O(\add_r[24]_i_2__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_3__4 
       (.I0(mul_r_reg__1[26]),
        .I1(CO),
        .I2(D[26]),
        .O(\add_r[24]_i_3__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_4__4 
       (.I0(mul_r_reg__1[25]),
        .I1(CO),
        .I2(D[25]),
        .O(\add_r[24]_i_4__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_5__4 
       (.I0(mul_r_reg__1[24]),
        .I1(CO),
        .I2(D[24]),
        .O(\add_r[24]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_2__4 
       (.I0(mul_r_reg__1[31]),
        .I1(CO),
        .I2(D[31]),
        .O(\add_r[28]_i_2__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_3__4 
       (.I0(mul_r_reg__1[30]),
        .I1(CO),
        .I2(D[30]),
        .O(\add_r[28]_i_3__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_4__4 
       (.I0(mul_r_reg__1[29]),
        .I1(CO),
        .I2(D[29]),
        .O(\add_r[28]_i_4__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_5__4 
       (.I0(mul_r_reg__1[28]),
        .I1(CO),
        .I2(D[28]),
        .O(\add_r[28]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_2__4 
       (.I0(mul_r_reg__1[35]),
        .I1(CO),
        .I2(D[35]),
        .O(\add_r[32]_i_2__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_3__4 
       (.I0(mul_r_reg__1[34]),
        .I1(CO),
        .I2(D[34]),
        .O(\add_r[32]_i_3__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_4__4 
       (.I0(mul_r_reg__1[33]),
        .I1(CO),
        .I2(D[33]),
        .O(\add_r[32]_i_4__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_5__4 
       (.I0(mul_r_reg__1[32]),
        .I1(CO),
        .I2(D[32]),
        .O(\add_r[32]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_2__4 
       (.I0(mul_r_reg__1[39]),
        .I1(CO),
        .I2(D[39]),
        .O(\add_r[36]_i_2__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_3__4 
       (.I0(mul_r_reg__1[38]),
        .I1(CO),
        .I2(D[38]),
        .O(\add_r[36]_i_3__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_4__4 
       (.I0(mul_r_reg__1[37]),
        .I1(CO),
        .I2(D[37]),
        .O(\add_r[36]_i_4__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_5__4 
       (.I0(mul_r_reg__1[36]),
        .I1(CO),
        .I2(D[36]),
        .O(\add_r[36]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_2__4 
       (.I0(mul_r_reg__1[43]),
        .I1(CO),
        .I2(D[43]),
        .O(\add_r[40]_i_2__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_3__4 
       (.I0(mul_r_reg__1[42]),
        .I1(CO),
        .I2(D[42]),
        .O(\add_r[40]_i_3__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_4__4 
       (.I0(mul_r_reg__1[41]),
        .I1(CO),
        .I2(D[41]),
        .O(\add_r[40]_i_4__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_5__4 
       (.I0(mul_r_reg__1[40]),
        .I1(CO),
        .I2(D[40]),
        .O(\add_r[40]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_2__4 
       (.I0(D[47]),
        .I1(CO),
        .I2(mul_r_reg__1[47]),
        .O(\add_r[44]_i_2__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_3__4 
       (.I0(mul_r_reg__1[46]),
        .I1(CO),
        .I2(D[46]),
        .O(\add_r[44]_i_3__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_4__4 
       (.I0(mul_r_reg__1[45]),
        .I1(CO),
        .I2(D[45]),
        .O(\add_r[44]_i_4__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_5__4 
       (.I0(mul_r_reg__1[44]),
        .I1(CO),
        .I2(D[44]),
        .O(\add_r[44]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_2__4 
       (.I0(mul_r_reg__1[7]),
        .I1(CO),
        .I2(D[7]),
        .O(\add_r[4]_i_2__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_3__4 
       (.I0(mul_r_reg__1[6]),
        .I1(CO),
        .I2(D[6]),
        .O(\add_r[4]_i_3__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_4__4 
       (.I0(mul_r_reg__1[5]),
        .I1(CO),
        .I2(D[5]),
        .O(\add_r[4]_i_4__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_5__4 
       (.I0(mul_r_reg__1[4]),
        .I1(CO),
        .I2(D[4]),
        .O(\add_r[4]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_2__4 
       (.I0(mul_r_reg__1[11]),
        .I1(CO),
        .I2(D[11]),
        .O(\add_r[8]_i_2__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_3__4 
       (.I0(mul_r_reg__1[10]),
        .I1(CO),
        .I2(D[10]),
        .O(\add_r[8]_i_3__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_4__4 
       (.I0(mul_r_reg__1[9]),
        .I1(CO),
        .I2(D[9]),
        .O(\add_r[8]_i_4__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_5__4 
       (.I0(mul_r_reg__1[8]),
        .I1(CO),
        .I2(D[8]),
        .O(\add_r[8]_i_5__4_n_0 ));
  FDRE \add_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__4_n_7 ),
        .Q(D[0]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[0]_i_1__4 
       (.CI(1'b0),
        .CO({\add_r_reg[0]_i_1__4_n_0 ,\add_r_reg[0]_i_1__4_n_1 ,\add_r_reg[0]_i_1__4_n_2 ,\add_r_reg[0]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[3:0]),
        .O({\add_r_reg[0]_i_1__4_n_4 ,\add_r_reg[0]_i_1__4_n_5 ,\add_r_reg[0]_i_1__4_n_6 ,\add_r_reg[0]_i_1__4_n_7 }),
        .S({\add_r[0]_i_2__4_n_0 ,\add_r[0]_i_3__4_n_0 ,\add_r[0]_i_4__4_n_0 ,\add_r[0]_i_5__4_n_0 }));
  FDRE \add_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__4_n_5 ),
        .Q(D[10]),
        .R(p_0_in__0));
  FDRE \add_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__4_n_4 ),
        .Q(D[11]),
        .R(p_0_in__0));
  FDRE \add_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__4_n_7 ),
        .Q(D[12]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[12]_i_1__4 
       (.CI(\add_r_reg[8]_i_1__4_n_0 ),
        .CO({\add_r_reg[12]_i_1__4_n_0 ,\add_r_reg[12]_i_1__4_n_1 ,\add_r_reg[12]_i_1__4_n_2 ,\add_r_reg[12]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[15:12]),
        .O({\add_r_reg[12]_i_1__4_n_4 ,\add_r_reg[12]_i_1__4_n_5 ,\add_r_reg[12]_i_1__4_n_6 ,\add_r_reg[12]_i_1__4_n_7 }),
        .S({\add_r[12]_i_2__4_n_0 ,\add_r[12]_i_3__4_n_0 ,\add_r[12]_i_4__4_n_0 ,\add_r[12]_i_5__4_n_0 }));
  FDRE \add_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__4_n_6 ),
        .Q(D[13]),
        .R(p_0_in__0));
  FDRE \add_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__4_n_5 ),
        .Q(D[14]),
        .R(p_0_in__0));
  FDRE \add_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__4_n_4 ),
        .Q(D[15]),
        .R(p_0_in__0));
  FDRE \add_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__4_n_7 ),
        .Q(D[16]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[16]_i_1__4 
       (.CI(\add_r_reg[12]_i_1__4_n_0 ),
        .CO({\add_r_reg[16]_i_1__4_n_0 ,\add_r_reg[16]_i_1__4_n_1 ,\add_r_reg[16]_i_1__4_n_2 ,\add_r_reg[16]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[19:16]),
        .O({\add_r_reg[16]_i_1__4_n_4 ,\add_r_reg[16]_i_1__4_n_5 ,\add_r_reg[16]_i_1__4_n_6 ,\add_r_reg[16]_i_1__4_n_7 }),
        .S({\add_r[16]_i_2__4_n_0 ,\add_r[16]_i_3__4_n_0 ,\add_r[16]_i_4__4_n_0 ,\add_r[16]_i_5__4_n_0 }));
  FDRE \add_r_reg[17] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__4_n_6 ),
        .Q(D[17]),
        .R(p_0_in__0));
  FDRE \add_r_reg[18] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__4_n_5 ),
        .Q(D[18]),
        .R(p_0_in__0));
  FDRE \add_r_reg[19] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__4_n_4 ),
        .Q(D[19]),
        .R(p_0_in__0));
  FDRE \add_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__4_n_6 ),
        .Q(D[1]),
        .R(p_0_in__0));
  FDRE \add_r_reg[20] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__4_n_7 ),
        .Q(D[20]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[20]_i_1__4 
       (.CI(\add_r_reg[16]_i_1__4_n_0 ),
        .CO({\add_r_reg[20]_i_1__4_n_0 ,\add_r_reg[20]_i_1__4_n_1 ,\add_r_reg[20]_i_1__4_n_2 ,\add_r_reg[20]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[23:20]),
        .O({\add_r_reg[20]_i_1__4_n_4 ,\add_r_reg[20]_i_1__4_n_5 ,\add_r_reg[20]_i_1__4_n_6 ,\add_r_reg[20]_i_1__4_n_7 }),
        .S({\add_r[20]_i_2__4_n_0 ,\add_r[20]_i_3__4_n_0 ,\add_r[20]_i_4__4_n_0 ,\add_r[20]_i_5__4_n_0 }));
  FDRE \add_r_reg[21] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__4_n_6 ),
        .Q(D[21]),
        .R(p_0_in__0));
  FDRE \add_r_reg[22] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__4_n_5 ),
        .Q(D[22]),
        .R(p_0_in__0));
  FDRE \add_r_reg[23] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__4_n_4 ),
        .Q(D[23]),
        .R(p_0_in__0));
  FDRE \add_r_reg[24] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__4_n_7 ),
        .Q(D[24]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[24]_i_1__4 
       (.CI(\add_r_reg[20]_i_1__4_n_0 ),
        .CO({\add_r_reg[24]_i_1__4_n_0 ,\add_r_reg[24]_i_1__4_n_1 ,\add_r_reg[24]_i_1__4_n_2 ,\add_r_reg[24]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[27:24]),
        .O({\add_r_reg[24]_i_1__4_n_4 ,\add_r_reg[24]_i_1__4_n_5 ,\add_r_reg[24]_i_1__4_n_6 ,\add_r_reg[24]_i_1__4_n_7 }),
        .S({\add_r[24]_i_2__4_n_0 ,\add_r[24]_i_3__4_n_0 ,\add_r[24]_i_4__4_n_0 ,\add_r[24]_i_5__4_n_0 }));
  FDRE \add_r_reg[25] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__4_n_6 ),
        .Q(D[25]),
        .R(p_0_in__0));
  FDRE \add_r_reg[26] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__4_n_5 ),
        .Q(D[26]),
        .R(p_0_in__0));
  FDRE \add_r_reg[27] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__4_n_4 ),
        .Q(D[27]),
        .R(p_0_in__0));
  FDRE \add_r_reg[28] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__4_n_7 ),
        .Q(D[28]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[28]_i_1__4 
       (.CI(\add_r_reg[24]_i_1__4_n_0 ),
        .CO({\add_r_reg[28]_i_1__4_n_0 ,\add_r_reg[28]_i_1__4_n_1 ,\add_r_reg[28]_i_1__4_n_2 ,\add_r_reg[28]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[31:28]),
        .O({\add_r_reg[28]_i_1__4_n_4 ,\add_r_reg[28]_i_1__4_n_5 ,\add_r_reg[28]_i_1__4_n_6 ,\add_r_reg[28]_i_1__4_n_7 }),
        .S({\add_r[28]_i_2__4_n_0 ,\add_r[28]_i_3__4_n_0 ,\add_r[28]_i_4__4_n_0 ,\add_r[28]_i_5__4_n_0 }));
  FDRE \add_r_reg[29] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__4_n_6 ),
        .Q(D[29]),
        .R(p_0_in__0));
  FDRE \add_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__4_n_5 ),
        .Q(D[2]),
        .R(p_0_in__0));
  FDRE \add_r_reg[30] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__4_n_5 ),
        .Q(D[30]),
        .R(p_0_in__0));
  FDRE \add_r_reg[31] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__4_n_4 ),
        .Q(D[31]),
        .R(p_0_in__0));
  FDRE \add_r_reg[32] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__4_n_7 ),
        .Q(D[32]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[32]_i_1__4 
       (.CI(\add_r_reg[28]_i_1__4_n_0 ),
        .CO({\add_r_reg[32]_i_1__4_n_0 ,\add_r_reg[32]_i_1__4_n_1 ,\add_r_reg[32]_i_1__4_n_2 ,\add_r_reg[32]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[35:32]),
        .O({\add_r_reg[32]_i_1__4_n_4 ,\add_r_reg[32]_i_1__4_n_5 ,\add_r_reg[32]_i_1__4_n_6 ,\add_r_reg[32]_i_1__4_n_7 }),
        .S({\add_r[32]_i_2__4_n_0 ,\add_r[32]_i_3__4_n_0 ,\add_r[32]_i_4__4_n_0 ,\add_r[32]_i_5__4_n_0 }));
  FDRE \add_r_reg[33] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__4_n_6 ),
        .Q(D[33]),
        .R(p_0_in__0));
  FDRE \add_r_reg[34] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__4_n_5 ),
        .Q(D[34]),
        .R(p_0_in__0));
  FDRE \add_r_reg[35] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__4_n_4 ),
        .Q(D[35]),
        .R(p_0_in__0));
  FDRE \add_r_reg[36] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__4_n_7 ),
        .Q(D[36]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[36]_i_1__4 
       (.CI(\add_r_reg[32]_i_1__4_n_0 ),
        .CO({\add_r_reg[36]_i_1__4_n_0 ,\add_r_reg[36]_i_1__4_n_1 ,\add_r_reg[36]_i_1__4_n_2 ,\add_r_reg[36]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[39:36]),
        .O({\add_r_reg[36]_i_1__4_n_4 ,\add_r_reg[36]_i_1__4_n_5 ,\add_r_reg[36]_i_1__4_n_6 ,\add_r_reg[36]_i_1__4_n_7 }),
        .S({\add_r[36]_i_2__4_n_0 ,\add_r[36]_i_3__4_n_0 ,\add_r[36]_i_4__4_n_0 ,\add_r[36]_i_5__4_n_0 }));
  FDRE \add_r_reg[37] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__4_n_6 ),
        .Q(D[37]),
        .R(p_0_in__0));
  FDRE \add_r_reg[38] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__4_n_5 ),
        .Q(D[38]),
        .R(p_0_in__0));
  FDRE \add_r_reg[39] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__4_n_4 ),
        .Q(D[39]),
        .R(p_0_in__0));
  FDRE \add_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__4_n_4 ),
        .Q(D[3]),
        .R(p_0_in__0));
  FDRE \add_r_reg[40] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__4_n_7 ),
        .Q(D[40]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[40]_i_1__4 
       (.CI(\add_r_reg[36]_i_1__4_n_0 ),
        .CO({\add_r_reg[40]_i_1__4_n_0 ,\add_r_reg[40]_i_1__4_n_1 ,\add_r_reg[40]_i_1__4_n_2 ,\add_r_reg[40]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[43:40]),
        .O({\add_r_reg[40]_i_1__4_n_4 ,\add_r_reg[40]_i_1__4_n_5 ,\add_r_reg[40]_i_1__4_n_6 ,\add_r_reg[40]_i_1__4_n_7 }),
        .S({\add_r[40]_i_2__4_n_0 ,\add_r[40]_i_3__4_n_0 ,\add_r[40]_i_4__4_n_0 ,\add_r[40]_i_5__4_n_0 }));
  FDRE \add_r_reg[41] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__4_n_6 ),
        .Q(D[41]),
        .R(p_0_in__0));
  FDRE \add_r_reg[42] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__4_n_5 ),
        .Q(D[42]),
        .R(p_0_in__0));
  FDRE \add_r_reg[43] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__4_n_4 ),
        .Q(D[43]),
        .R(p_0_in__0));
  FDRE \add_r_reg[44] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__4_n_7 ),
        .Q(D[44]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[44]_i_1__4 
       (.CI(\add_r_reg[40]_i_1__4_n_0 ),
        .CO({\NLW_add_r_reg[44]_i_1__4_CO_UNCONNECTED [3],\add_r_reg[44]_i_1__4_n_1 ,\add_r_reg[44]_i_1__4_n_2 ,\add_r_reg[44]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,mul_r_reg__1[46:44]}),
        .O({\add_r_reg[44]_i_1__4_n_4 ,\add_r_reg[44]_i_1__4_n_5 ,\add_r_reg[44]_i_1__4_n_6 ,\add_r_reg[44]_i_1__4_n_7 }),
        .S({\add_r[44]_i_2__4_n_0 ,\add_r[44]_i_3__4_n_0 ,\add_r[44]_i_4__4_n_0 ,\add_r[44]_i_5__4_n_0 }));
  FDRE \add_r_reg[45] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__4_n_6 ),
        .Q(D[45]),
        .R(p_0_in__0));
  FDRE \add_r_reg[46] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__4_n_5 ),
        .Q(D[46]),
        .R(p_0_in__0));
  FDRE \add_r_reg[47] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__4_n_4 ),
        .Q(D[47]),
        .R(p_0_in__0));
  FDRE \add_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__4_n_7 ),
        .Q(D[4]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[4]_i_1__4 
       (.CI(\add_r_reg[0]_i_1__4_n_0 ),
        .CO({\add_r_reg[4]_i_1__4_n_0 ,\add_r_reg[4]_i_1__4_n_1 ,\add_r_reg[4]_i_1__4_n_2 ,\add_r_reg[4]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[7:4]),
        .O({\add_r_reg[4]_i_1__4_n_4 ,\add_r_reg[4]_i_1__4_n_5 ,\add_r_reg[4]_i_1__4_n_6 ,\add_r_reg[4]_i_1__4_n_7 }),
        .S({\add_r[4]_i_2__4_n_0 ,\add_r[4]_i_3__4_n_0 ,\add_r[4]_i_4__4_n_0 ,\add_r[4]_i_5__4_n_0 }));
  FDRE \add_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__4_n_6 ),
        .Q(D[5]),
        .R(p_0_in__0));
  FDRE \add_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__4_n_5 ),
        .Q(D[6]),
        .R(p_0_in__0));
  FDRE \add_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__4_n_4 ),
        .Q(D[7]),
        .R(p_0_in__0));
  FDRE \add_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__4_n_7 ),
        .Q(D[8]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[8]_i_1__4 
       (.CI(\add_r_reg[4]_i_1__4_n_0 ),
        .CO({\add_r_reg[8]_i_1__4_n_0 ,\add_r_reg[8]_i_1__4_n_1 ,\add_r_reg[8]_i_1__4_n_2 ,\add_r_reg[8]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[11:8]),
        .O({\add_r_reg[8]_i_1__4_n_4 ,\add_r_reg[8]_i_1__4_n_5 ,\add_r_reg[8]_i_1__4_n_6 ,\add_r_reg[8]_i_1__4_n_7 }),
        .S({\add_r[8]_i_2__4_n_0 ,\add_r[8]_i_3__4_n_0 ,\add_r[8]_i_4__4_n_0 ,\add_r[8]_i_5__4_n_0 }));
  FDRE \add_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__4_n_6 ),
        .Q(D[9]),
        .R(p_0_in__0));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter[0]_i_1__7 
       (.I0(\counter_reg_n_0_[0] ),
        .O(\counter[0]_i_1__7_n_0 ));
  LUT4 #(
    .INIT(16'hF7C8)) 
    \counter[1]_i_1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(dp_enable),
        .I2(CO),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \counter[2]_i_1__6 
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[2] ),
        .O(\counter[2]_i_1__6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \counter[3]_i_1__6 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .O(\counter[3]_i_1__6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \counter[4]_i_1__5 
       (.I0(\counter_reg_n_0_[3] ),
        .I1(\counter_reg_n_0_[2] ),
        .I2(\counter_reg_n_0_[1] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[4] ),
        .O(\counter[4]_i_1__5_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \counter[5]_i_1__5 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .I4(\counter_reg_n_0_[4] ),
        .I5(\counter_reg_n_0_[5] ),
        .O(\counter[5]_i_1__5_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \counter[6]_i_1__4 
       (.I0(\counter[8]_i_3__5_n_0 ),
        .I1(\counter_reg_n_0_[6] ),
        .O(\counter[6]_i_1__4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'hB4)) 
    \counter[7]_i_1__4 
       (.I0(\counter[8]_i_3__5_n_0 ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(\counter_reg_n_0_[7] ),
        .O(\counter[7]_i_1__4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT4 #(
    .INIT(16'hF708)) 
    \counter[8]_i_2__5 
       (.I0(\counter_reg_n_0_[7] ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(\counter[8]_i_3__5_n_0 ),
        .I3(\counter_reg_n_0_[8] ),
        .O(\counter[8]_i_2__5_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \counter[8]_i_3__5 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .I4(\counter_reg_n_0_[4] ),
        .I5(\counter_reg_n_0_[5] ),
        .O(\counter[8]_i_3__5_n_0 ));
  FDRE \counter_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[0]_i_1__7_n_0 ),
        .Q(\counter_reg_n_0_[0] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\counter[1]_i_1_n_0 ),
        .Q(\counter_reg_n_0_[1] ),
        .R(p_0_in__0));
  FDRE \counter_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[2]_i_1__6_n_0 ),
        .Q(\counter_reg_n_0_[2] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[3]_i_1__6_n_0 ),
        .Q(\counter_reg_n_0_[3] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[4]_i_1__5_n_0 ),
        .Q(\counter_reg_n_0_[4] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[5]_i_1__5_n_0 ),
        .Q(\counter_reg_n_0_[5] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[6]_i_1__4_n_0 ),
        .Q(\counter_reg_n_0_[6] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[7]_i_1__4_n_0 ),
        .Q(\counter_reg_n_0_[7] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[8]_i_2__5_n_0 ),
        .Q(\counter_reg_n_0_[8] ),
        .R(\slv_regs_reg[0][12] ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,b_ram_out[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({A[17],A[17],A[17],A[14:0]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r0_OVERFLOW_UNCONNECTED),
        .P({mul_r0_n_58,mul_r0_n_59,mul_r0_n_60,mul_r0_n_61,mul_r0_n_62,mul_r0_n_63,mul_r0_n_64,mul_r0_n_65,mul_r0_n_66,mul_r0_n_67,mul_r0_n_68,mul_r0_n_69,mul_r0_n_70,mul_r0_n_71,mul_r0_n_72,mul_r0_n_73,mul_r0_n_74,mul_r0_n_75,mul_r0_n_76,mul_r0_n_77,mul_r0_n_78,mul_r0_n_79,mul_r0_n_80,mul_r0_n_81,mul_r0_n_82,mul_r0_n_83,mul_r0_n_84,mul_r0_n_85,mul_r0_n_86,mul_r0_n_87,mul_r0_n_88,mul_r0_n_89,mul_r0_n_90,mul_r0_n_91,mul_r0_n_92,mul_r0_n_93,mul_r0_n_94,mul_r0_n_95,mul_r0_n_96,mul_r0_n_97,mul_r0_n_98,mul_r0_n_99,mul_r0_n_100,mul_r0_n_101,mul_r0_n_102,mul_r0_n_103,mul_r0_n_104,mul_r0_n_105}),
        .PATTERNBDETECT(NLW_mul_r0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_r0_UNDERFLOW_UNCONNECTED));
  FDRE \mul_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_105),
        .Q(mul_r_reg__1[0]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_95),
        .Q(mul_r_reg__1[10]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_94),
        .Q(mul_r_reg__1[11]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_93),
        .Q(mul_r_reg__1[12]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_92),
        .Q(mul_r_reg__1[13]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_91),
        .Q(mul_r_reg__1[14]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_90),
        .Q(mul_r_reg__1[15]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_89),
        .Q(mul_r_reg__1[16]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_104),
        .Q(mul_r_reg__1[1]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_103),
        .Q(mul_r_reg__1[2]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_102),
        .Q(mul_r_reg__1[3]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_101),
        .Q(mul_r_reg__1[4]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_100),
        .Q(mul_r_reg__1[5]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_99),
        .Q(mul_r_reg__1[6]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_98),
        .Q(mul_r_reg__1[7]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_97),
        .Q(mul_r_reg__1[8]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_96),
        .Q(mul_r_reg__1[9]),
        .R(p_0_in__0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-12 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r_reg__0
       (.A({A[17:16],A[16],A[16],A[16],A[16],A[16],A[16],A[16],A[16],A[16:15],A[15],A[15],A[15:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r_reg__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({b_ram_out[31],b_ram_out[31],b_ram_out[31],b_ram_out[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r_reg__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(dp_enable),
        .CLK(clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED),
        .P({mul_r_reg__0_n_58,mul_r_reg__0_n_59,mul_r_reg__0_n_60,mul_r_reg__0_n_61,mul_r_reg__0_n_62,mul_r_reg__0_n_63,mul_r_reg__0_n_64,mul_r_reg__0_n_65,mul_r_reg__0_n_66,mul_r_reg__0_n_67,mul_r_reg__0_n_68,mul_r_reg__0_n_69,mul_r_reg__0_n_70,mul_r_reg__0_n_71,mul_r_reg__0_n_72,mul_r_reg__0_n_73,mul_r_reg__0_n_74,mul_r_reg__1[47:17]}),
        .PATTERNBDETECT(NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .PCOUT(NLW_mul_r_reg__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(p_0_in__0),
        .UNDERFLOW(NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "dot_product" *) 
module design_1_top_0_0_dot_product_5
   (D,
    CO,
    \counter_reg[3]_0 ,
    p_rdy,
    \res_mem_reg[383] ,
    A,
    b_ram_out,
    dp_enable,
    clk,
    p_0_in__0,
    \slv_regs_reg[0][11] ,
    O,
    \slv_regs_reg[0][11]_0 ,
    \slv_regs_reg[0][11]_1 ,
    \slv_regs_reg[0][11]_2 ,
    \slv_regs_reg[0][11]_3 ,
    \slv_regs_reg[0][8] ,
    \slv_regs_reg[0][0] ,
    \slv_regs_reg[0][11]_4 ,
    \slv_regs_reg[0][11]_5 ,
    \slv_regs_reg[0][11]_6 ,
    \slv_regs_reg[0][12] );
  output [47:0]D;
  output [0:0]CO;
  output [0:0]\counter_reg[3]_0 ;
  output p_rdy;
  output \res_mem_reg[383] ;
  input [17:0]A;
  input [31:0]b_ram_out;
  input dp_enable;
  input clk;
  input p_0_in__0;
  input [0:0]\slv_regs_reg[0][11] ;
  input [0:0]O;
  input [0:0]\slv_regs_reg[0][11]_0 ;
  input [0:0]\slv_regs_reg[0][11]_1 ;
  input [0:0]\slv_regs_reg[0][11]_2 ;
  input [0:0]\slv_regs_reg[0][11]_3 ;
  input [7:0]\slv_regs_reg[0][8] ;
  input [0:0]\slv_regs_reg[0][0] ;
  input [0:0]\slv_regs_reg[0][11]_4 ;
  input [0:0]\slv_regs_reg[0][11]_5 ;
  input [0:0]\slv_regs_reg[0][11]_6 ;
  input [0:0]\slv_regs_reg[0][12] ;

  wire [17:0]A;
  wire [0:0]CO;
  wire [47:0]D;
  wire [0:0]O;
  wire add_r1_carry_i_2__5_n_0;
  wire add_r1_carry_i_3__5_n_0;
  wire add_r1_carry_i_4__5_n_0;
  wire add_r1_carry_n_0;
  wire add_r1_carry_n_1;
  wire add_r1_carry_n_2;
  wire add_r1_carry_n_3;
  wire \add_r[0]_i_2__5_n_0 ;
  wire \add_r[0]_i_3__5_n_0 ;
  wire \add_r[0]_i_4__5_n_0 ;
  wire \add_r[0]_i_5__5_n_0 ;
  wire \add_r[12]_i_2__5_n_0 ;
  wire \add_r[12]_i_3__5_n_0 ;
  wire \add_r[12]_i_4__5_n_0 ;
  wire \add_r[12]_i_5__5_n_0 ;
  wire \add_r[16]_i_2__5_n_0 ;
  wire \add_r[16]_i_3__5_n_0 ;
  wire \add_r[16]_i_4__5_n_0 ;
  wire \add_r[16]_i_5__5_n_0 ;
  wire \add_r[20]_i_2__5_n_0 ;
  wire \add_r[20]_i_3__5_n_0 ;
  wire \add_r[20]_i_4__5_n_0 ;
  wire \add_r[20]_i_5__5_n_0 ;
  wire \add_r[24]_i_2__5_n_0 ;
  wire \add_r[24]_i_3__5_n_0 ;
  wire \add_r[24]_i_4__5_n_0 ;
  wire \add_r[24]_i_5__5_n_0 ;
  wire \add_r[28]_i_2__5_n_0 ;
  wire \add_r[28]_i_3__5_n_0 ;
  wire \add_r[28]_i_4__5_n_0 ;
  wire \add_r[28]_i_5__5_n_0 ;
  wire \add_r[32]_i_2__5_n_0 ;
  wire \add_r[32]_i_3__5_n_0 ;
  wire \add_r[32]_i_4__5_n_0 ;
  wire \add_r[32]_i_5__5_n_0 ;
  wire \add_r[36]_i_2__5_n_0 ;
  wire \add_r[36]_i_3__5_n_0 ;
  wire \add_r[36]_i_4__5_n_0 ;
  wire \add_r[36]_i_5__5_n_0 ;
  wire \add_r[40]_i_2__5_n_0 ;
  wire \add_r[40]_i_3__5_n_0 ;
  wire \add_r[40]_i_4__5_n_0 ;
  wire \add_r[40]_i_5__5_n_0 ;
  wire \add_r[44]_i_2__5_n_0 ;
  wire \add_r[44]_i_3__5_n_0 ;
  wire \add_r[44]_i_4__5_n_0 ;
  wire \add_r[44]_i_5__5_n_0 ;
  wire \add_r[4]_i_2__5_n_0 ;
  wire \add_r[4]_i_3__5_n_0 ;
  wire \add_r[4]_i_4__5_n_0 ;
  wire \add_r[4]_i_5__5_n_0 ;
  wire \add_r[8]_i_2__5_n_0 ;
  wire \add_r[8]_i_3__5_n_0 ;
  wire \add_r[8]_i_4__5_n_0 ;
  wire \add_r[8]_i_5__5_n_0 ;
  wire \add_r_reg[0]_i_1__5_n_0 ;
  wire \add_r_reg[0]_i_1__5_n_1 ;
  wire \add_r_reg[0]_i_1__5_n_2 ;
  wire \add_r_reg[0]_i_1__5_n_3 ;
  wire \add_r_reg[0]_i_1__5_n_4 ;
  wire \add_r_reg[0]_i_1__5_n_5 ;
  wire \add_r_reg[0]_i_1__5_n_6 ;
  wire \add_r_reg[0]_i_1__5_n_7 ;
  wire \add_r_reg[12]_i_1__5_n_0 ;
  wire \add_r_reg[12]_i_1__5_n_1 ;
  wire \add_r_reg[12]_i_1__5_n_2 ;
  wire \add_r_reg[12]_i_1__5_n_3 ;
  wire \add_r_reg[12]_i_1__5_n_4 ;
  wire \add_r_reg[12]_i_1__5_n_5 ;
  wire \add_r_reg[12]_i_1__5_n_6 ;
  wire \add_r_reg[12]_i_1__5_n_7 ;
  wire \add_r_reg[16]_i_1__5_n_0 ;
  wire \add_r_reg[16]_i_1__5_n_1 ;
  wire \add_r_reg[16]_i_1__5_n_2 ;
  wire \add_r_reg[16]_i_1__5_n_3 ;
  wire \add_r_reg[16]_i_1__5_n_4 ;
  wire \add_r_reg[16]_i_1__5_n_5 ;
  wire \add_r_reg[16]_i_1__5_n_6 ;
  wire \add_r_reg[16]_i_1__5_n_7 ;
  wire \add_r_reg[20]_i_1__5_n_0 ;
  wire \add_r_reg[20]_i_1__5_n_1 ;
  wire \add_r_reg[20]_i_1__5_n_2 ;
  wire \add_r_reg[20]_i_1__5_n_3 ;
  wire \add_r_reg[20]_i_1__5_n_4 ;
  wire \add_r_reg[20]_i_1__5_n_5 ;
  wire \add_r_reg[20]_i_1__5_n_6 ;
  wire \add_r_reg[20]_i_1__5_n_7 ;
  wire \add_r_reg[24]_i_1__5_n_0 ;
  wire \add_r_reg[24]_i_1__5_n_1 ;
  wire \add_r_reg[24]_i_1__5_n_2 ;
  wire \add_r_reg[24]_i_1__5_n_3 ;
  wire \add_r_reg[24]_i_1__5_n_4 ;
  wire \add_r_reg[24]_i_1__5_n_5 ;
  wire \add_r_reg[24]_i_1__5_n_6 ;
  wire \add_r_reg[24]_i_1__5_n_7 ;
  wire \add_r_reg[28]_i_1__5_n_0 ;
  wire \add_r_reg[28]_i_1__5_n_1 ;
  wire \add_r_reg[28]_i_1__5_n_2 ;
  wire \add_r_reg[28]_i_1__5_n_3 ;
  wire \add_r_reg[28]_i_1__5_n_4 ;
  wire \add_r_reg[28]_i_1__5_n_5 ;
  wire \add_r_reg[28]_i_1__5_n_6 ;
  wire \add_r_reg[28]_i_1__5_n_7 ;
  wire \add_r_reg[32]_i_1__5_n_0 ;
  wire \add_r_reg[32]_i_1__5_n_1 ;
  wire \add_r_reg[32]_i_1__5_n_2 ;
  wire \add_r_reg[32]_i_1__5_n_3 ;
  wire \add_r_reg[32]_i_1__5_n_4 ;
  wire \add_r_reg[32]_i_1__5_n_5 ;
  wire \add_r_reg[32]_i_1__5_n_6 ;
  wire \add_r_reg[32]_i_1__5_n_7 ;
  wire \add_r_reg[36]_i_1__5_n_0 ;
  wire \add_r_reg[36]_i_1__5_n_1 ;
  wire \add_r_reg[36]_i_1__5_n_2 ;
  wire \add_r_reg[36]_i_1__5_n_3 ;
  wire \add_r_reg[36]_i_1__5_n_4 ;
  wire \add_r_reg[36]_i_1__5_n_5 ;
  wire \add_r_reg[36]_i_1__5_n_6 ;
  wire \add_r_reg[36]_i_1__5_n_7 ;
  wire \add_r_reg[40]_i_1__5_n_0 ;
  wire \add_r_reg[40]_i_1__5_n_1 ;
  wire \add_r_reg[40]_i_1__5_n_2 ;
  wire \add_r_reg[40]_i_1__5_n_3 ;
  wire \add_r_reg[40]_i_1__5_n_4 ;
  wire \add_r_reg[40]_i_1__5_n_5 ;
  wire \add_r_reg[40]_i_1__5_n_6 ;
  wire \add_r_reg[40]_i_1__5_n_7 ;
  wire \add_r_reg[44]_i_1__5_n_1 ;
  wire \add_r_reg[44]_i_1__5_n_2 ;
  wire \add_r_reg[44]_i_1__5_n_3 ;
  wire \add_r_reg[44]_i_1__5_n_4 ;
  wire \add_r_reg[44]_i_1__5_n_5 ;
  wire \add_r_reg[44]_i_1__5_n_6 ;
  wire \add_r_reg[44]_i_1__5_n_7 ;
  wire \add_r_reg[4]_i_1__5_n_0 ;
  wire \add_r_reg[4]_i_1__5_n_1 ;
  wire \add_r_reg[4]_i_1__5_n_2 ;
  wire \add_r_reg[4]_i_1__5_n_3 ;
  wire \add_r_reg[4]_i_1__5_n_4 ;
  wire \add_r_reg[4]_i_1__5_n_5 ;
  wire \add_r_reg[4]_i_1__5_n_6 ;
  wire \add_r_reg[4]_i_1__5_n_7 ;
  wire \add_r_reg[8]_i_1__5_n_0 ;
  wire \add_r_reg[8]_i_1__5_n_1 ;
  wire \add_r_reg[8]_i_1__5_n_2 ;
  wire \add_r_reg[8]_i_1__5_n_3 ;
  wire \add_r_reg[8]_i_1__5_n_4 ;
  wire \add_r_reg[8]_i_1__5_n_5 ;
  wire \add_r_reg[8]_i_1__5_n_6 ;
  wire \add_r_reg[8]_i_1__5_n_7 ;
  wire [31:0]b_ram_out;
  wire clk;
  wire \counter[0]_i_1__8_n_0 ;
  wire \counter[1]_i_1_n_0 ;
  wire \counter[2]_i_1__7_n_0 ;
  wire \counter[3]_i_1__7_n_0 ;
  wire \counter[4]_i_1__6_n_0 ;
  wire \counter[5]_i_1__6_n_0 ;
  wire \counter[6]_i_1__5_n_0 ;
  wire \counter[7]_i_1__5_n_0 ;
  wire \counter[8]_i_2__6_n_0 ;
  wire \counter[8]_i_3__6_n_0 ;
  wire [0:0]\counter_reg[3]_0 ;
  wire \counter_reg_n_0_[0] ;
  wire \counter_reg_n_0_[1] ;
  wire \counter_reg_n_0_[2] ;
  wire \counter_reg_n_0_[3] ;
  wire \counter_reg_n_0_[4] ;
  wire \counter_reg_n_0_[5] ;
  wire \counter_reg_n_0_[6] ;
  wire \counter_reg_n_0_[7] ;
  wire \counter_reg_n_0_[8] ;
  wire dp_enable;
  wire mul_r0_n_100;
  wire mul_r0_n_101;
  wire mul_r0_n_102;
  wire mul_r0_n_103;
  wire mul_r0_n_104;
  wire mul_r0_n_105;
  wire mul_r0_n_106;
  wire mul_r0_n_107;
  wire mul_r0_n_108;
  wire mul_r0_n_109;
  wire mul_r0_n_110;
  wire mul_r0_n_111;
  wire mul_r0_n_112;
  wire mul_r0_n_113;
  wire mul_r0_n_114;
  wire mul_r0_n_115;
  wire mul_r0_n_116;
  wire mul_r0_n_117;
  wire mul_r0_n_118;
  wire mul_r0_n_119;
  wire mul_r0_n_120;
  wire mul_r0_n_121;
  wire mul_r0_n_122;
  wire mul_r0_n_123;
  wire mul_r0_n_124;
  wire mul_r0_n_125;
  wire mul_r0_n_126;
  wire mul_r0_n_127;
  wire mul_r0_n_128;
  wire mul_r0_n_129;
  wire mul_r0_n_130;
  wire mul_r0_n_131;
  wire mul_r0_n_132;
  wire mul_r0_n_133;
  wire mul_r0_n_134;
  wire mul_r0_n_135;
  wire mul_r0_n_136;
  wire mul_r0_n_137;
  wire mul_r0_n_138;
  wire mul_r0_n_139;
  wire mul_r0_n_140;
  wire mul_r0_n_141;
  wire mul_r0_n_142;
  wire mul_r0_n_143;
  wire mul_r0_n_144;
  wire mul_r0_n_145;
  wire mul_r0_n_146;
  wire mul_r0_n_147;
  wire mul_r0_n_148;
  wire mul_r0_n_149;
  wire mul_r0_n_150;
  wire mul_r0_n_151;
  wire mul_r0_n_152;
  wire mul_r0_n_153;
  wire mul_r0_n_58;
  wire mul_r0_n_59;
  wire mul_r0_n_60;
  wire mul_r0_n_61;
  wire mul_r0_n_62;
  wire mul_r0_n_63;
  wire mul_r0_n_64;
  wire mul_r0_n_65;
  wire mul_r0_n_66;
  wire mul_r0_n_67;
  wire mul_r0_n_68;
  wire mul_r0_n_69;
  wire mul_r0_n_70;
  wire mul_r0_n_71;
  wire mul_r0_n_72;
  wire mul_r0_n_73;
  wire mul_r0_n_74;
  wire mul_r0_n_75;
  wire mul_r0_n_76;
  wire mul_r0_n_77;
  wire mul_r0_n_78;
  wire mul_r0_n_79;
  wire mul_r0_n_80;
  wire mul_r0_n_81;
  wire mul_r0_n_82;
  wire mul_r0_n_83;
  wire mul_r0_n_84;
  wire mul_r0_n_85;
  wire mul_r0_n_86;
  wire mul_r0_n_87;
  wire mul_r0_n_88;
  wire mul_r0_n_89;
  wire mul_r0_n_90;
  wire mul_r0_n_91;
  wire mul_r0_n_92;
  wire mul_r0_n_93;
  wire mul_r0_n_94;
  wire mul_r0_n_95;
  wire mul_r0_n_96;
  wire mul_r0_n_97;
  wire mul_r0_n_98;
  wire mul_r0_n_99;
  wire mul_r_reg__0_n_58;
  wire mul_r_reg__0_n_59;
  wire mul_r_reg__0_n_60;
  wire mul_r_reg__0_n_61;
  wire mul_r_reg__0_n_62;
  wire mul_r_reg__0_n_63;
  wire mul_r_reg__0_n_64;
  wire mul_r_reg__0_n_65;
  wire mul_r_reg__0_n_66;
  wire mul_r_reg__0_n_67;
  wire mul_r_reg__0_n_68;
  wire mul_r_reg__0_n_69;
  wire mul_r_reg__0_n_70;
  wire mul_r_reg__0_n_71;
  wire mul_r_reg__0_n_72;
  wire mul_r_reg__0_n_73;
  wire mul_r_reg__0_n_74;
  wire [47:0]mul_r_reg__1;
  wire p_0_in__0;
  wire p_rdy;
  wire \res_mem_reg[383] ;
  wire [0:0]\slv_regs_reg[0][0] ;
  wire [0:0]\slv_regs_reg[0][11] ;
  wire [0:0]\slv_regs_reg[0][11]_0 ;
  wire [0:0]\slv_regs_reg[0][11]_1 ;
  wire [0:0]\slv_regs_reg[0][11]_2 ;
  wire [0:0]\slv_regs_reg[0][11]_3 ;
  wire [0:0]\slv_regs_reg[0][11]_4 ;
  wire [0:0]\slv_regs_reg[0][11]_5 ;
  wire [0:0]\slv_regs_reg[0][11]_6 ;
  wire [0:0]\slv_regs_reg[0][12] ;
  wire [7:0]\slv_regs_reg[0][8] ;
  wire [3:0]NLW_add_r1_carry_O_UNCONNECTED;
  wire [3:1]NLW_add_r1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_add_r1_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_add_r_reg[44]_i_1__5_CO_UNCONNECTED ;
  wire NLW_mul_r0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r0_CARRYOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r_reg__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r_reg__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_r_reg__0_PCOUT_UNCONNECTED;

  CARRY4 add_r1_carry
       (.CI(1'b0),
        .CO({add_r1_carry_n_0,add_r1_carry_n_1,add_r1_carry_n_2,add_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry_O_UNCONNECTED[3:0]),
        .S({\slv_regs_reg[0][11] ,add_r1_carry_i_2__5_n_0,add_r1_carry_i_3__5_n_0,add_r1_carry_i_4__5_n_0}));
  CARRY4 add_r1_carry__0
       (.CI(add_r1_carry_n_0),
        .CO({NLW_add_r1_carry__0_CO_UNCONNECTED[3:1],CO}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,O}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_2__5
       (.I0(\counter_reg_n_0_[7] ),
        .I1(\slv_regs_reg[0][8] [6]),
        .I2(\counter_reg_n_0_[6] ),
        .I3(\slv_regs_reg[0][8] [5]),
        .I4(\slv_regs_reg[0][8] [7]),
        .I5(\counter_reg_n_0_[8] ),
        .O(add_r1_carry_i_2__5_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_3__5
       (.I0(\counter_reg_n_0_[4] ),
        .I1(\slv_regs_reg[0][8] [3]),
        .I2(\counter_reg_n_0_[3] ),
        .I3(\slv_regs_reg[0][8] [2]),
        .I4(\slv_regs_reg[0][8] [4]),
        .I5(\counter_reg_n_0_[5] ),
        .O(add_r1_carry_i_3__5_n_0));
  LUT6 #(
    .INIT(64'h0990000000000990)) 
    add_r1_carry_i_4__5
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\slv_regs_reg[0][8] [0]),
        .I2(\slv_regs_reg[0][0] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\slv_regs_reg[0][8] [1]),
        .I5(\counter_reg_n_0_[2] ),
        .O(add_r1_carry_i_4__5_n_0));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_2__5 
       (.I0(mul_r_reg__1[3]),
        .I1(CO),
        .I2(D[3]),
        .O(\add_r[0]_i_2__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_3__5 
       (.I0(mul_r_reg__1[2]),
        .I1(CO),
        .I2(D[2]),
        .O(\add_r[0]_i_3__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_4__5 
       (.I0(mul_r_reg__1[1]),
        .I1(CO),
        .I2(D[1]),
        .O(\add_r[0]_i_4__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_5__5 
       (.I0(mul_r_reg__1[0]),
        .I1(CO),
        .I2(D[0]),
        .O(\add_r[0]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_2__5 
       (.I0(mul_r_reg__1[15]),
        .I1(CO),
        .I2(D[15]),
        .O(\add_r[12]_i_2__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_3__5 
       (.I0(mul_r_reg__1[14]),
        .I1(CO),
        .I2(D[14]),
        .O(\add_r[12]_i_3__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_4__5 
       (.I0(mul_r_reg__1[13]),
        .I1(CO),
        .I2(D[13]),
        .O(\add_r[12]_i_4__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_5__5 
       (.I0(mul_r_reg__1[12]),
        .I1(CO),
        .I2(D[12]),
        .O(\add_r[12]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_2__5 
       (.I0(mul_r_reg__1[19]),
        .I1(CO),
        .I2(D[19]),
        .O(\add_r[16]_i_2__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_3__5 
       (.I0(mul_r_reg__1[18]),
        .I1(CO),
        .I2(D[18]),
        .O(\add_r[16]_i_3__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_4__5 
       (.I0(mul_r_reg__1[17]),
        .I1(CO),
        .I2(D[17]),
        .O(\add_r[16]_i_4__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_5__5 
       (.I0(mul_r_reg__1[16]),
        .I1(CO),
        .I2(D[16]),
        .O(\add_r[16]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_2__5 
       (.I0(mul_r_reg__1[23]),
        .I1(CO),
        .I2(D[23]),
        .O(\add_r[20]_i_2__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_3__5 
       (.I0(mul_r_reg__1[22]),
        .I1(CO),
        .I2(D[22]),
        .O(\add_r[20]_i_3__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_4__5 
       (.I0(mul_r_reg__1[21]),
        .I1(CO),
        .I2(D[21]),
        .O(\add_r[20]_i_4__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_5__5 
       (.I0(mul_r_reg__1[20]),
        .I1(CO),
        .I2(D[20]),
        .O(\add_r[20]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_2__5 
       (.I0(mul_r_reg__1[27]),
        .I1(CO),
        .I2(D[27]),
        .O(\add_r[24]_i_2__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_3__5 
       (.I0(mul_r_reg__1[26]),
        .I1(CO),
        .I2(D[26]),
        .O(\add_r[24]_i_3__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_4__5 
       (.I0(mul_r_reg__1[25]),
        .I1(CO),
        .I2(D[25]),
        .O(\add_r[24]_i_4__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_5__5 
       (.I0(mul_r_reg__1[24]),
        .I1(CO),
        .I2(D[24]),
        .O(\add_r[24]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_2__5 
       (.I0(mul_r_reg__1[31]),
        .I1(CO),
        .I2(D[31]),
        .O(\add_r[28]_i_2__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_3__5 
       (.I0(mul_r_reg__1[30]),
        .I1(CO),
        .I2(D[30]),
        .O(\add_r[28]_i_3__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_4__5 
       (.I0(mul_r_reg__1[29]),
        .I1(CO),
        .I2(D[29]),
        .O(\add_r[28]_i_4__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_5__5 
       (.I0(mul_r_reg__1[28]),
        .I1(CO),
        .I2(D[28]),
        .O(\add_r[28]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_2__5 
       (.I0(mul_r_reg__1[35]),
        .I1(CO),
        .I2(D[35]),
        .O(\add_r[32]_i_2__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_3__5 
       (.I0(mul_r_reg__1[34]),
        .I1(CO),
        .I2(D[34]),
        .O(\add_r[32]_i_3__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_4__5 
       (.I0(mul_r_reg__1[33]),
        .I1(CO),
        .I2(D[33]),
        .O(\add_r[32]_i_4__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_5__5 
       (.I0(mul_r_reg__1[32]),
        .I1(CO),
        .I2(D[32]),
        .O(\add_r[32]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_2__5 
       (.I0(mul_r_reg__1[39]),
        .I1(CO),
        .I2(D[39]),
        .O(\add_r[36]_i_2__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_3__5 
       (.I0(mul_r_reg__1[38]),
        .I1(CO),
        .I2(D[38]),
        .O(\add_r[36]_i_3__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_4__5 
       (.I0(mul_r_reg__1[37]),
        .I1(CO),
        .I2(D[37]),
        .O(\add_r[36]_i_4__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_5__5 
       (.I0(mul_r_reg__1[36]),
        .I1(CO),
        .I2(D[36]),
        .O(\add_r[36]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_2__5 
       (.I0(mul_r_reg__1[43]),
        .I1(CO),
        .I2(D[43]),
        .O(\add_r[40]_i_2__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_3__5 
       (.I0(mul_r_reg__1[42]),
        .I1(CO),
        .I2(D[42]),
        .O(\add_r[40]_i_3__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_4__5 
       (.I0(mul_r_reg__1[41]),
        .I1(CO),
        .I2(D[41]),
        .O(\add_r[40]_i_4__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_5__5 
       (.I0(mul_r_reg__1[40]),
        .I1(CO),
        .I2(D[40]),
        .O(\add_r[40]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_2__5 
       (.I0(D[47]),
        .I1(CO),
        .I2(mul_r_reg__1[47]),
        .O(\add_r[44]_i_2__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_3__5 
       (.I0(mul_r_reg__1[46]),
        .I1(CO),
        .I2(D[46]),
        .O(\add_r[44]_i_3__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_4__5 
       (.I0(mul_r_reg__1[45]),
        .I1(CO),
        .I2(D[45]),
        .O(\add_r[44]_i_4__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_5__5 
       (.I0(mul_r_reg__1[44]),
        .I1(CO),
        .I2(D[44]),
        .O(\add_r[44]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_2__5 
       (.I0(mul_r_reg__1[7]),
        .I1(CO),
        .I2(D[7]),
        .O(\add_r[4]_i_2__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_3__5 
       (.I0(mul_r_reg__1[6]),
        .I1(CO),
        .I2(D[6]),
        .O(\add_r[4]_i_3__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_4__5 
       (.I0(mul_r_reg__1[5]),
        .I1(CO),
        .I2(D[5]),
        .O(\add_r[4]_i_4__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_5__5 
       (.I0(mul_r_reg__1[4]),
        .I1(CO),
        .I2(D[4]),
        .O(\add_r[4]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_2__5 
       (.I0(mul_r_reg__1[11]),
        .I1(CO),
        .I2(D[11]),
        .O(\add_r[8]_i_2__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_3__5 
       (.I0(mul_r_reg__1[10]),
        .I1(CO),
        .I2(D[10]),
        .O(\add_r[8]_i_3__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_4__5 
       (.I0(mul_r_reg__1[9]),
        .I1(CO),
        .I2(D[9]),
        .O(\add_r[8]_i_4__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_5__5 
       (.I0(mul_r_reg__1[8]),
        .I1(CO),
        .I2(D[8]),
        .O(\add_r[8]_i_5__5_n_0 ));
  FDRE \add_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__5_n_7 ),
        .Q(D[0]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[0]_i_1__5 
       (.CI(1'b0),
        .CO({\add_r_reg[0]_i_1__5_n_0 ,\add_r_reg[0]_i_1__5_n_1 ,\add_r_reg[0]_i_1__5_n_2 ,\add_r_reg[0]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[3:0]),
        .O({\add_r_reg[0]_i_1__5_n_4 ,\add_r_reg[0]_i_1__5_n_5 ,\add_r_reg[0]_i_1__5_n_6 ,\add_r_reg[0]_i_1__5_n_7 }),
        .S({\add_r[0]_i_2__5_n_0 ,\add_r[0]_i_3__5_n_0 ,\add_r[0]_i_4__5_n_0 ,\add_r[0]_i_5__5_n_0 }));
  FDRE \add_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__5_n_5 ),
        .Q(D[10]),
        .R(p_0_in__0));
  FDRE \add_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__5_n_4 ),
        .Q(D[11]),
        .R(p_0_in__0));
  FDRE \add_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__5_n_7 ),
        .Q(D[12]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[12]_i_1__5 
       (.CI(\add_r_reg[8]_i_1__5_n_0 ),
        .CO({\add_r_reg[12]_i_1__5_n_0 ,\add_r_reg[12]_i_1__5_n_1 ,\add_r_reg[12]_i_1__5_n_2 ,\add_r_reg[12]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[15:12]),
        .O({\add_r_reg[12]_i_1__5_n_4 ,\add_r_reg[12]_i_1__5_n_5 ,\add_r_reg[12]_i_1__5_n_6 ,\add_r_reg[12]_i_1__5_n_7 }),
        .S({\add_r[12]_i_2__5_n_0 ,\add_r[12]_i_3__5_n_0 ,\add_r[12]_i_4__5_n_0 ,\add_r[12]_i_5__5_n_0 }));
  FDRE \add_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__5_n_6 ),
        .Q(D[13]),
        .R(p_0_in__0));
  FDRE \add_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__5_n_5 ),
        .Q(D[14]),
        .R(p_0_in__0));
  FDRE \add_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__5_n_4 ),
        .Q(D[15]),
        .R(p_0_in__0));
  FDRE \add_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__5_n_7 ),
        .Q(D[16]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[16]_i_1__5 
       (.CI(\add_r_reg[12]_i_1__5_n_0 ),
        .CO({\add_r_reg[16]_i_1__5_n_0 ,\add_r_reg[16]_i_1__5_n_1 ,\add_r_reg[16]_i_1__5_n_2 ,\add_r_reg[16]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[19:16]),
        .O({\add_r_reg[16]_i_1__5_n_4 ,\add_r_reg[16]_i_1__5_n_5 ,\add_r_reg[16]_i_1__5_n_6 ,\add_r_reg[16]_i_1__5_n_7 }),
        .S({\add_r[16]_i_2__5_n_0 ,\add_r[16]_i_3__5_n_0 ,\add_r[16]_i_4__5_n_0 ,\add_r[16]_i_5__5_n_0 }));
  FDRE \add_r_reg[17] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__5_n_6 ),
        .Q(D[17]),
        .R(p_0_in__0));
  FDRE \add_r_reg[18] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__5_n_5 ),
        .Q(D[18]),
        .R(p_0_in__0));
  FDRE \add_r_reg[19] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__5_n_4 ),
        .Q(D[19]),
        .R(p_0_in__0));
  FDRE \add_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__5_n_6 ),
        .Q(D[1]),
        .R(p_0_in__0));
  FDRE \add_r_reg[20] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__5_n_7 ),
        .Q(D[20]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[20]_i_1__5 
       (.CI(\add_r_reg[16]_i_1__5_n_0 ),
        .CO({\add_r_reg[20]_i_1__5_n_0 ,\add_r_reg[20]_i_1__5_n_1 ,\add_r_reg[20]_i_1__5_n_2 ,\add_r_reg[20]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[23:20]),
        .O({\add_r_reg[20]_i_1__5_n_4 ,\add_r_reg[20]_i_1__5_n_5 ,\add_r_reg[20]_i_1__5_n_6 ,\add_r_reg[20]_i_1__5_n_7 }),
        .S({\add_r[20]_i_2__5_n_0 ,\add_r[20]_i_3__5_n_0 ,\add_r[20]_i_4__5_n_0 ,\add_r[20]_i_5__5_n_0 }));
  FDRE \add_r_reg[21] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__5_n_6 ),
        .Q(D[21]),
        .R(p_0_in__0));
  FDRE \add_r_reg[22] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__5_n_5 ),
        .Q(D[22]),
        .R(p_0_in__0));
  FDRE \add_r_reg[23] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__5_n_4 ),
        .Q(D[23]),
        .R(p_0_in__0));
  FDRE \add_r_reg[24] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__5_n_7 ),
        .Q(D[24]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[24]_i_1__5 
       (.CI(\add_r_reg[20]_i_1__5_n_0 ),
        .CO({\add_r_reg[24]_i_1__5_n_0 ,\add_r_reg[24]_i_1__5_n_1 ,\add_r_reg[24]_i_1__5_n_2 ,\add_r_reg[24]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[27:24]),
        .O({\add_r_reg[24]_i_1__5_n_4 ,\add_r_reg[24]_i_1__5_n_5 ,\add_r_reg[24]_i_1__5_n_6 ,\add_r_reg[24]_i_1__5_n_7 }),
        .S({\add_r[24]_i_2__5_n_0 ,\add_r[24]_i_3__5_n_0 ,\add_r[24]_i_4__5_n_0 ,\add_r[24]_i_5__5_n_0 }));
  FDRE \add_r_reg[25] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__5_n_6 ),
        .Q(D[25]),
        .R(p_0_in__0));
  FDRE \add_r_reg[26] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__5_n_5 ),
        .Q(D[26]),
        .R(p_0_in__0));
  FDRE \add_r_reg[27] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__5_n_4 ),
        .Q(D[27]),
        .R(p_0_in__0));
  FDRE \add_r_reg[28] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__5_n_7 ),
        .Q(D[28]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[28]_i_1__5 
       (.CI(\add_r_reg[24]_i_1__5_n_0 ),
        .CO({\add_r_reg[28]_i_1__5_n_0 ,\add_r_reg[28]_i_1__5_n_1 ,\add_r_reg[28]_i_1__5_n_2 ,\add_r_reg[28]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[31:28]),
        .O({\add_r_reg[28]_i_1__5_n_4 ,\add_r_reg[28]_i_1__5_n_5 ,\add_r_reg[28]_i_1__5_n_6 ,\add_r_reg[28]_i_1__5_n_7 }),
        .S({\add_r[28]_i_2__5_n_0 ,\add_r[28]_i_3__5_n_0 ,\add_r[28]_i_4__5_n_0 ,\add_r[28]_i_5__5_n_0 }));
  FDRE \add_r_reg[29] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__5_n_6 ),
        .Q(D[29]),
        .R(p_0_in__0));
  FDRE \add_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__5_n_5 ),
        .Q(D[2]),
        .R(p_0_in__0));
  FDRE \add_r_reg[30] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__5_n_5 ),
        .Q(D[30]),
        .R(p_0_in__0));
  FDRE \add_r_reg[31] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__5_n_4 ),
        .Q(D[31]),
        .R(p_0_in__0));
  FDRE \add_r_reg[32] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__5_n_7 ),
        .Q(D[32]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[32]_i_1__5 
       (.CI(\add_r_reg[28]_i_1__5_n_0 ),
        .CO({\add_r_reg[32]_i_1__5_n_0 ,\add_r_reg[32]_i_1__5_n_1 ,\add_r_reg[32]_i_1__5_n_2 ,\add_r_reg[32]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[35:32]),
        .O({\add_r_reg[32]_i_1__5_n_4 ,\add_r_reg[32]_i_1__5_n_5 ,\add_r_reg[32]_i_1__5_n_6 ,\add_r_reg[32]_i_1__5_n_7 }),
        .S({\add_r[32]_i_2__5_n_0 ,\add_r[32]_i_3__5_n_0 ,\add_r[32]_i_4__5_n_0 ,\add_r[32]_i_5__5_n_0 }));
  FDRE \add_r_reg[33] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__5_n_6 ),
        .Q(D[33]),
        .R(p_0_in__0));
  FDRE \add_r_reg[34] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__5_n_5 ),
        .Q(D[34]),
        .R(p_0_in__0));
  FDRE \add_r_reg[35] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__5_n_4 ),
        .Q(D[35]),
        .R(p_0_in__0));
  FDRE \add_r_reg[36] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__5_n_7 ),
        .Q(D[36]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[36]_i_1__5 
       (.CI(\add_r_reg[32]_i_1__5_n_0 ),
        .CO({\add_r_reg[36]_i_1__5_n_0 ,\add_r_reg[36]_i_1__5_n_1 ,\add_r_reg[36]_i_1__5_n_2 ,\add_r_reg[36]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[39:36]),
        .O({\add_r_reg[36]_i_1__5_n_4 ,\add_r_reg[36]_i_1__5_n_5 ,\add_r_reg[36]_i_1__5_n_6 ,\add_r_reg[36]_i_1__5_n_7 }),
        .S({\add_r[36]_i_2__5_n_0 ,\add_r[36]_i_3__5_n_0 ,\add_r[36]_i_4__5_n_0 ,\add_r[36]_i_5__5_n_0 }));
  FDRE \add_r_reg[37] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__5_n_6 ),
        .Q(D[37]),
        .R(p_0_in__0));
  FDRE \add_r_reg[38] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__5_n_5 ),
        .Q(D[38]),
        .R(p_0_in__0));
  FDRE \add_r_reg[39] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__5_n_4 ),
        .Q(D[39]),
        .R(p_0_in__0));
  FDRE \add_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__5_n_4 ),
        .Q(D[3]),
        .R(p_0_in__0));
  FDRE \add_r_reg[40] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__5_n_7 ),
        .Q(D[40]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[40]_i_1__5 
       (.CI(\add_r_reg[36]_i_1__5_n_0 ),
        .CO({\add_r_reg[40]_i_1__5_n_0 ,\add_r_reg[40]_i_1__5_n_1 ,\add_r_reg[40]_i_1__5_n_2 ,\add_r_reg[40]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[43:40]),
        .O({\add_r_reg[40]_i_1__5_n_4 ,\add_r_reg[40]_i_1__5_n_5 ,\add_r_reg[40]_i_1__5_n_6 ,\add_r_reg[40]_i_1__5_n_7 }),
        .S({\add_r[40]_i_2__5_n_0 ,\add_r[40]_i_3__5_n_0 ,\add_r[40]_i_4__5_n_0 ,\add_r[40]_i_5__5_n_0 }));
  FDRE \add_r_reg[41] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__5_n_6 ),
        .Q(D[41]),
        .R(p_0_in__0));
  FDRE \add_r_reg[42] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__5_n_5 ),
        .Q(D[42]),
        .R(p_0_in__0));
  FDRE \add_r_reg[43] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__5_n_4 ),
        .Q(D[43]),
        .R(p_0_in__0));
  FDRE \add_r_reg[44] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__5_n_7 ),
        .Q(D[44]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[44]_i_1__5 
       (.CI(\add_r_reg[40]_i_1__5_n_0 ),
        .CO({\NLW_add_r_reg[44]_i_1__5_CO_UNCONNECTED [3],\add_r_reg[44]_i_1__5_n_1 ,\add_r_reg[44]_i_1__5_n_2 ,\add_r_reg[44]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,mul_r_reg__1[46:44]}),
        .O({\add_r_reg[44]_i_1__5_n_4 ,\add_r_reg[44]_i_1__5_n_5 ,\add_r_reg[44]_i_1__5_n_6 ,\add_r_reg[44]_i_1__5_n_7 }),
        .S({\add_r[44]_i_2__5_n_0 ,\add_r[44]_i_3__5_n_0 ,\add_r[44]_i_4__5_n_0 ,\add_r[44]_i_5__5_n_0 }));
  FDRE \add_r_reg[45] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__5_n_6 ),
        .Q(D[45]),
        .R(p_0_in__0));
  FDRE \add_r_reg[46] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__5_n_5 ),
        .Q(D[46]),
        .R(p_0_in__0));
  FDRE \add_r_reg[47] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__5_n_4 ),
        .Q(D[47]),
        .R(p_0_in__0));
  FDRE \add_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__5_n_7 ),
        .Q(D[4]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[4]_i_1__5 
       (.CI(\add_r_reg[0]_i_1__5_n_0 ),
        .CO({\add_r_reg[4]_i_1__5_n_0 ,\add_r_reg[4]_i_1__5_n_1 ,\add_r_reg[4]_i_1__5_n_2 ,\add_r_reg[4]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[7:4]),
        .O({\add_r_reg[4]_i_1__5_n_4 ,\add_r_reg[4]_i_1__5_n_5 ,\add_r_reg[4]_i_1__5_n_6 ,\add_r_reg[4]_i_1__5_n_7 }),
        .S({\add_r[4]_i_2__5_n_0 ,\add_r[4]_i_3__5_n_0 ,\add_r[4]_i_4__5_n_0 ,\add_r[4]_i_5__5_n_0 }));
  FDRE \add_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__5_n_6 ),
        .Q(D[5]),
        .R(p_0_in__0));
  FDRE \add_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__5_n_5 ),
        .Q(D[6]),
        .R(p_0_in__0));
  FDRE \add_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__5_n_4 ),
        .Q(D[7]),
        .R(p_0_in__0));
  FDRE \add_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__5_n_7 ),
        .Q(D[8]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[8]_i_1__5 
       (.CI(\add_r_reg[4]_i_1__5_n_0 ),
        .CO({\add_r_reg[8]_i_1__5_n_0 ,\add_r_reg[8]_i_1__5_n_1 ,\add_r_reg[8]_i_1__5_n_2 ,\add_r_reg[8]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[11:8]),
        .O({\add_r_reg[8]_i_1__5_n_4 ,\add_r_reg[8]_i_1__5_n_5 ,\add_r_reg[8]_i_1__5_n_6 ,\add_r_reg[8]_i_1__5_n_7 }),
        .S({\add_r[8]_i_2__5_n_0 ,\add_r[8]_i_3__5_n_0 ,\add_r[8]_i_4__5_n_0 ,\add_r[8]_i_5__5_n_0 }));
  FDRE \add_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__5_n_6 ),
        .Q(D[9]),
        .R(p_0_in__0));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter[0]_i_1__8 
       (.I0(\counter_reg_n_0_[0] ),
        .O(\counter[0]_i_1__8_n_0 ));
  LUT4 #(
    .INIT(16'hF7C8)) 
    \counter[1]_i_1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(dp_enable),
        .I2(CO),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \counter[2]_i_1__7 
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[2] ),
        .O(\counter[2]_i_1__7_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \counter[3]_i_1__0 
       (.I0(p_rdy),
        .O(\counter_reg[3]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \counter[3]_i_1__7 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .O(\counter[3]_i_1__7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \counter[4]_i_1__6 
       (.I0(\counter_reg_n_0_[3] ),
        .I1(\counter_reg_n_0_[2] ),
        .I2(\counter_reg_n_0_[1] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[4] ),
        .O(\counter[4]_i_1__6_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \counter[5]_i_1__6 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .I4(\counter_reg_n_0_[4] ),
        .I5(\counter_reg_n_0_[5] ),
        .O(\counter[5]_i_1__6_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \counter[6]_i_1__5 
       (.I0(\counter[8]_i_3__6_n_0 ),
        .I1(\counter_reg_n_0_[6] ),
        .O(\counter[6]_i_1__5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'hB4)) 
    \counter[7]_i_1__5 
       (.I0(\counter[8]_i_3__6_n_0 ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(\counter_reg_n_0_[7] ),
        .O(\counter[7]_i_1__5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'hF708)) 
    \counter[8]_i_2__6 
       (.I0(\counter_reg_n_0_[7] ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(\counter[8]_i_3__6_n_0 ),
        .I3(\counter_reg_n_0_[8] ),
        .O(\counter[8]_i_2__6_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \counter[8]_i_3__6 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .I4(\counter_reg_n_0_[4] ),
        .I5(\counter_reg_n_0_[5] ),
        .O(\counter[8]_i_3__6_n_0 ));
  FDRE \counter_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[0]_i_1__8_n_0 ),
        .Q(\counter_reg_n_0_[0] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\counter[1]_i_1_n_0 ),
        .Q(\counter_reg_n_0_[1] ),
        .R(p_0_in__0));
  FDRE \counter_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[2]_i_1__7_n_0 ),
        .Q(\counter_reg_n_0_[2] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[3]_i_1__7_n_0 ),
        .Q(\counter_reg_n_0_[3] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[4]_i_1__6_n_0 ),
        .Q(\counter_reg_n_0_[4] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[5]_i_1__6_n_0 ),
        .Q(\counter_reg_n_0_[5] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[6]_i_1__5_n_0 ),
        .Q(\counter_reg_n_0_[6] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[7]_i_1__5_n_0 ),
        .Q(\counter_reg_n_0_[7] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[8]_i_2__6_n_0 ),
        .Q(\counter_reg_n_0_[8] ),
        .R(\slv_regs_reg[0][12] ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \m_axis_tdata[47]_i_3 
       (.I0(\res_mem_reg[383] ),
        .I1(\slv_regs_reg[0][11]_0 ),
        .I2(\slv_regs_reg[0][11]_1 ),
        .I3(\slv_regs_reg[0][11]_2 ),
        .I4(\slv_regs_reg[0][11]_3 ),
        .I5(dp_enable),
        .O(p_rdy));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,b_ram_out[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({A[17],A[17],A[17],A[14:0]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r0_OVERFLOW_UNCONNECTED),
        .P({mul_r0_n_58,mul_r0_n_59,mul_r0_n_60,mul_r0_n_61,mul_r0_n_62,mul_r0_n_63,mul_r0_n_64,mul_r0_n_65,mul_r0_n_66,mul_r0_n_67,mul_r0_n_68,mul_r0_n_69,mul_r0_n_70,mul_r0_n_71,mul_r0_n_72,mul_r0_n_73,mul_r0_n_74,mul_r0_n_75,mul_r0_n_76,mul_r0_n_77,mul_r0_n_78,mul_r0_n_79,mul_r0_n_80,mul_r0_n_81,mul_r0_n_82,mul_r0_n_83,mul_r0_n_84,mul_r0_n_85,mul_r0_n_86,mul_r0_n_87,mul_r0_n_88,mul_r0_n_89,mul_r0_n_90,mul_r0_n_91,mul_r0_n_92,mul_r0_n_93,mul_r0_n_94,mul_r0_n_95,mul_r0_n_96,mul_r0_n_97,mul_r0_n_98,mul_r0_n_99,mul_r0_n_100,mul_r0_n_101,mul_r0_n_102,mul_r0_n_103,mul_r0_n_104,mul_r0_n_105}),
        .PATTERNBDETECT(NLW_mul_r0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_r0_UNDERFLOW_UNCONNECTED));
  FDRE \mul_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_105),
        .Q(mul_r_reg__1[0]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_95),
        .Q(mul_r_reg__1[10]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_94),
        .Q(mul_r_reg__1[11]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_93),
        .Q(mul_r_reg__1[12]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_92),
        .Q(mul_r_reg__1[13]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_91),
        .Q(mul_r_reg__1[14]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_90),
        .Q(mul_r_reg__1[15]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_89),
        .Q(mul_r_reg__1[16]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_104),
        .Q(mul_r_reg__1[1]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_103),
        .Q(mul_r_reg__1[2]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_102),
        .Q(mul_r_reg__1[3]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_101),
        .Q(mul_r_reg__1[4]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_100),
        .Q(mul_r_reg__1[5]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_99),
        .Q(mul_r_reg__1[6]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_98),
        .Q(mul_r_reg__1[7]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_97),
        .Q(mul_r_reg__1[8]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_96),
        .Q(mul_r_reg__1[9]),
        .R(p_0_in__0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-12 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r_reg__0
       (.A({A[17],A[17],A[17:16],A[16],A[16],A[16],A[16],A[16],A[16],A[16],A[16],A[16:15],A[15:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r_reg__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({b_ram_out[31],b_ram_out[31],b_ram_out[31],b_ram_out[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r_reg__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(dp_enable),
        .CLK(clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED),
        .P({mul_r_reg__0_n_58,mul_r_reg__0_n_59,mul_r_reg__0_n_60,mul_r_reg__0_n_61,mul_r_reg__0_n_62,mul_r_reg__0_n_63,mul_r_reg__0_n_64,mul_r_reg__0_n_65,mul_r_reg__0_n_66,mul_r_reg__0_n_67,mul_r_reg__0_n_68,mul_r_reg__0_n_69,mul_r_reg__0_n_70,mul_r_reg__0_n_71,mul_r_reg__0_n_72,mul_r_reg__0_n_73,mul_r_reg__0_n_74,mul_r_reg__1[47:17]}),
        .PATTERNBDETECT(NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .PCOUT(NLW_mul_r_reg__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(p_0_in__0),
        .UNDERFLOW(NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED));
  LUT4 #(
    .INIT(16'h8000)) 
    \res_mem[383]_i_3 
       (.I0(CO),
        .I1(\slv_regs_reg[0][11]_4 ),
        .I2(\slv_regs_reg[0][11]_5 ),
        .I3(\slv_regs_reg[0][11]_6 ),
        .O(\res_mem_reg[383] ));
endmodule

(* ORIG_REF_NAME = "dot_product" *) 
module design_1_top_0_0_dot_product_6
   (D,
    \counter_reg[1]_0 ,
    A,
    b_ram_out,
    dp_enable,
    clk,
    p_0_in__0,
    S,
    O,
    \slv_regs_reg[0][8] ,
    \slv_regs_reg[0][0] ,
    \slv_regs_reg[0][12] );
  output [47:0]D;
  output [0:0]\counter_reg[1]_0 ;
  input [16:0]A;
  input [31:0]b_ram_out;
  input dp_enable;
  input clk;
  input p_0_in__0;
  input [0:0]S;
  input [0:0]O;
  input [7:0]\slv_regs_reg[0][8] ;
  input [0:0]\slv_regs_reg[0][0] ;
  input [0:0]\slv_regs_reg[0][12] ;

  wire [16:0]A;
  wire [47:0]D;
  wire [0:0]O;
  wire [0:0]S;
  wire add_r1_carry_i_2__6_n_0;
  wire add_r1_carry_i_3__6_n_0;
  wire add_r1_carry_i_4__6_n_0;
  wire add_r1_carry_n_0;
  wire add_r1_carry_n_1;
  wire add_r1_carry_n_2;
  wire add_r1_carry_n_3;
  wire \add_r[0]_i_2__6_n_0 ;
  wire \add_r[0]_i_3__6_n_0 ;
  wire \add_r[0]_i_4__6_n_0 ;
  wire \add_r[0]_i_5__6_n_0 ;
  wire \add_r[12]_i_2__6_n_0 ;
  wire \add_r[12]_i_3__6_n_0 ;
  wire \add_r[12]_i_4__6_n_0 ;
  wire \add_r[12]_i_5__6_n_0 ;
  wire \add_r[16]_i_2__6_n_0 ;
  wire \add_r[16]_i_3__6_n_0 ;
  wire \add_r[16]_i_4__6_n_0 ;
  wire \add_r[16]_i_5__6_n_0 ;
  wire \add_r[20]_i_2__6_n_0 ;
  wire \add_r[20]_i_3__6_n_0 ;
  wire \add_r[20]_i_4__6_n_0 ;
  wire \add_r[20]_i_5__6_n_0 ;
  wire \add_r[24]_i_2__6_n_0 ;
  wire \add_r[24]_i_3__6_n_0 ;
  wire \add_r[24]_i_4__6_n_0 ;
  wire \add_r[24]_i_5__6_n_0 ;
  wire \add_r[28]_i_2__6_n_0 ;
  wire \add_r[28]_i_3__6_n_0 ;
  wire \add_r[28]_i_4__6_n_0 ;
  wire \add_r[28]_i_5__6_n_0 ;
  wire \add_r[32]_i_2__6_n_0 ;
  wire \add_r[32]_i_3__6_n_0 ;
  wire \add_r[32]_i_4__6_n_0 ;
  wire \add_r[32]_i_5__6_n_0 ;
  wire \add_r[36]_i_2__6_n_0 ;
  wire \add_r[36]_i_3__6_n_0 ;
  wire \add_r[36]_i_4__6_n_0 ;
  wire \add_r[36]_i_5__6_n_0 ;
  wire \add_r[40]_i_2__6_n_0 ;
  wire \add_r[40]_i_3__6_n_0 ;
  wire \add_r[40]_i_4__6_n_0 ;
  wire \add_r[40]_i_5__6_n_0 ;
  wire \add_r[44]_i_2__6_n_0 ;
  wire \add_r[44]_i_3__6_n_0 ;
  wire \add_r[44]_i_4__6_n_0 ;
  wire \add_r[44]_i_5__6_n_0 ;
  wire \add_r[4]_i_2__6_n_0 ;
  wire \add_r[4]_i_3__6_n_0 ;
  wire \add_r[4]_i_4__6_n_0 ;
  wire \add_r[4]_i_5__6_n_0 ;
  wire \add_r[8]_i_2__6_n_0 ;
  wire \add_r[8]_i_3__6_n_0 ;
  wire \add_r[8]_i_4__6_n_0 ;
  wire \add_r[8]_i_5__6_n_0 ;
  wire \add_r_reg[0]_i_1__6_n_0 ;
  wire \add_r_reg[0]_i_1__6_n_1 ;
  wire \add_r_reg[0]_i_1__6_n_2 ;
  wire \add_r_reg[0]_i_1__6_n_3 ;
  wire \add_r_reg[0]_i_1__6_n_4 ;
  wire \add_r_reg[0]_i_1__6_n_5 ;
  wire \add_r_reg[0]_i_1__6_n_6 ;
  wire \add_r_reg[0]_i_1__6_n_7 ;
  wire \add_r_reg[12]_i_1__6_n_0 ;
  wire \add_r_reg[12]_i_1__6_n_1 ;
  wire \add_r_reg[12]_i_1__6_n_2 ;
  wire \add_r_reg[12]_i_1__6_n_3 ;
  wire \add_r_reg[12]_i_1__6_n_4 ;
  wire \add_r_reg[12]_i_1__6_n_5 ;
  wire \add_r_reg[12]_i_1__6_n_6 ;
  wire \add_r_reg[12]_i_1__6_n_7 ;
  wire \add_r_reg[16]_i_1__6_n_0 ;
  wire \add_r_reg[16]_i_1__6_n_1 ;
  wire \add_r_reg[16]_i_1__6_n_2 ;
  wire \add_r_reg[16]_i_1__6_n_3 ;
  wire \add_r_reg[16]_i_1__6_n_4 ;
  wire \add_r_reg[16]_i_1__6_n_5 ;
  wire \add_r_reg[16]_i_1__6_n_6 ;
  wire \add_r_reg[16]_i_1__6_n_7 ;
  wire \add_r_reg[20]_i_1__6_n_0 ;
  wire \add_r_reg[20]_i_1__6_n_1 ;
  wire \add_r_reg[20]_i_1__6_n_2 ;
  wire \add_r_reg[20]_i_1__6_n_3 ;
  wire \add_r_reg[20]_i_1__6_n_4 ;
  wire \add_r_reg[20]_i_1__6_n_5 ;
  wire \add_r_reg[20]_i_1__6_n_6 ;
  wire \add_r_reg[20]_i_1__6_n_7 ;
  wire \add_r_reg[24]_i_1__6_n_0 ;
  wire \add_r_reg[24]_i_1__6_n_1 ;
  wire \add_r_reg[24]_i_1__6_n_2 ;
  wire \add_r_reg[24]_i_1__6_n_3 ;
  wire \add_r_reg[24]_i_1__6_n_4 ;
  wire \add_r_reg[24]_i_1__6_n_5 ;
  wire \add_r_reg[24]_i_1__6_n_6 ;
  wire \add_r_reg[24]_i_1__6_n_7 ;
  wire \add_r_reg[28]_i_1__6_n_0 ;
  wire \add_r_reg[28]_i_1__6_n_1 ;
  wire \add_r_reg[28]_i_1__6_n_2 ;
  wire \add_r_reg[28]_i_1__6_n_3 ;
  wire \add_r_reg[28]_i_1__6_n_4 ;
  wire \add_r_reg[28]_i_1__6_n_5 ;
  wire \add_r_reg[28]_i_1__6_n_6 ;
  wire \add_r_reg[28]_i_1__6_n_7 ;
  wire \add_r_reg[32]_i_1__6_n_0 ;
  wire \add_r_reg[32]_i_1__6_n_1 ;
  wire \add_r_reg[32]_i_1__6_n_2 ;
  wire \add_r_reg[32]_i_1__6_n_3 ;
  wire \add_r_reg[32]_i_1__6_n_4 ;
  wire \add_r_reg[32]_i_1__6_n_5 ;
  wire \add_r_reg[32]_i_1__6_n_6 ;
  wire \add_r_reg[32]_i_1__6_n_7 ;
  wire \add_r_reg[36]_i_1__6_n_0 ;
  wire \add_r_reg[36]_i_1__6_n_1 ;
  wire \add_r_reg[36]_i_1__6_n_2 ;
  wire \add_r_reg[36]_i_1__6_n_3 ;
  wire \add_r_reg[36]_i_1__6_n_4 ;
  wire \add_r_reg[36]_i_1__6_n_5 ;
  wire \add_r_reg[36]_i_1__6_n_6 ;
  wire \add_r_reg[36]_i_1__6_n_7 ;
  wire \add_r_reg[40]_i_1__6_n_0 ;
  wire \add_r_reg[40]_i_1__6_n_1 ;
  wire \add_r_reg[40]_i_1__6_n_2 ;
  wire \add_r_reg[40]_i_1__6_n_3 ;
  wire \add_r_reg[40]_i_1__6_n_4 ;
  wire \add_r_reg[40]_i_1__6_n_5 ;
  wire \add_r_reg[40]_i_1__6_n_6 ;
  wire \add_r_reg[40]_i_1__6_n_7 ;
  wire \add_r_reg[44]_i_1__6_n_1 ;
  wire \add_r_reg[44]_i_1__6_n_2 ;
  wire \add_r_reg[44]_i_1__6_n_3 ;
  wire \add_r_reg[44]_i_1__6_n_4 ;
  wire \add_r_reg[44]_i_1__6_n_5 ;
  wire \add_r_reg[44]_i_1__6_n_6 ;
  wire \add_r_reg[44]_i_1__6_n_7 ;
  wire \add_r_reg[4]_i_1__6_n_0 ;
  wire \add_r_reg[4]_i_1__6_n_1 ;
  wire \add_r_reg[4]_i_1__6_n_2 ;
  wire \add_r_reg[4]_i_1__6_n_3 ;
  wire \add_r_reg[4]_i_1__6_n_4 ;
  wire \add_r_reg[4]_i_1__6_n_5 ;
  wire \add_r_reg[4]_i_1__6_n_6 ;
  wire \add_r_reg[4]_i_1__6_n_7 ;
  wire \add_r_reg[8]_i_1__6_n_0 ;
  wire \add_r_reg[8]_i_1__6_n_1 ;
  wire \add_r_reg[8]_i_1__6_n_2 ;
  wire \add_r_reg[8]_i_1__6_n_3 ;
  wire \add_r_reg[8]_i_1__6_n_4 ;
  wire \add_r_reg[8]_i_1__6_n_5 ;
  wire \add_r_reg[8]_i_1__6_n_6 ;
  wire \add_r_reg[8]_i_1__6_n_7 ;
  wire [31:0]b_ram_out;
  wire clk;
  wire \counter[0]_i_1__9_n_0 ;
  wire \counter[1]_i_1_n_0 ;
  wire \counter[2]_i_1__8_n_0 ;
  wire \counter[3]_i_1__8_n_0 ;
  wire \counter[4]_i_1__7_n_0 ;
  wire \counter[5]_i_1__7_n_0 ;
  wire \counter[6]_i_1__6_n_0 ;
  wire \counter[7]_i_1__6_n_0 ;
  wire \counter[8]_i_2__7_n_0 ;
  wire \counter[8]_i_3__7_n_0 ;
  wire [0:0]\counter_reg[1]_0 ;
  wire \counter_reg_n_0_[0] ;
  wire \counter_reg_n_0_[1] ;
  wire \counter_reg_n_0_[2] ;
  wire \counter_reg_n_0_[3] ;
  wire \counter_reg_n_0_[4] ;
  wire \counter_reg_n_0_[5] ;
  wire \counter_reg_n_0_[6] ;
  wire \counter_reg_n_0_[7] ;
  wire \counter_reg_n_0_[8] ;
  wire dp_enable;
  wire mul_r0_n_100;
  wire mul_r0_n_101;
  wire mul_r0_n_102;
  wire mul_r0_n_103;
  wire mul_r0_n_104;
  wire mul_r0_n_105;
  wire mul_r0_n_106;
  wire mul_r0_n_107;
  wire mul_r0_n_108;
  wire mul_r0_n_109;
  wire mul_r0_n_110;
  wire mul_r0_n_111;
  wire mul_r0_n_112;
  wire mul_r0_n_113;
  wire mul_r0_n_114;
  wire mul_r0_n_115;
  wire mul_r0_n_116;
  wire mul_r0_n_117;
  wire mul_r0_n_118;
  wire mul_r0_n_119;
  wire mul_r0_n_120;
  wire mul_r0_n_121;
  wire mul_r0_n_122;
  wire mul_r0_n_123;
  wire mul_r0_n_124;
  wire mul_r0_n_125;
  wire mul_r0_n_126;
  wire mul_r0_n_127;
  wire mul_r0_n_128;
  wire mul_r0_n_129;
  wire mul_r0_n_130;
  wire mul_r0_n_131;
  wire mul_r0_n_132;
  wire mul_r0_n_133;
  wire mul_r0_n_134;
  wire mul_r0_n_135;
  wire mul_r0_n_136;
  wire mul_r0_n_137;
  wire mul_r0_n_138;
  wire mul_r0_n_139;
  wire mul_r0_n_140;
  wire mul_r0_n_141;
  wire mul_r0_n_142;
  wire mul_r0_n_143;
  wire mul_r0_n_144;
  wire mul_r0_n_145;
  wire mul_r0_n_146;
  wire mul_r0_n_147;
  wire mul_r0_n_148;
  wire mul_r0_n_149;
  wire mul_r0_n_150;
  wire mul_r0_n_151;
  wire mul_r0_n_152;
  wire mul_r0_n_153;
  wire mul_r0_n_58;
  wire mul_r0_n_59;
  wire mul_r0_n_60;
  wire mul_r0_n_61;
  wire mul_r0_n_62;
  wire mul_r0_n_63;
  wire mul_r0_n_64;
  wire mul_r0_n_65;
  wire mul_r0_n_66;
  wire mul_r0_n_67;
  wire mul_r0_n_68;
  wire mul_r0_n_69;
  wire mul_r0_n_70;
  wire mul_r0_n_71;
  wire mul_r0_n_72;
  wire mul_r0_n_73;
  wire mul_r0_n_74;
  wire mul_r0_n_75;
  wire mul_r0_n_76;
  wire mul_r0_n_77;
  wire mul_r0_n_78;
  wire mul_r0_n_79;
  wire mul_r0_n_80;
  wire mul_r0_n_81;
  wire mul_r0_n_82;
  wire mul_r0_n_83;
  wire mul_r0_n_84;
  wire mul_r0_n_85;
  wire mul_r0_n_86;
  wire mul_r0_n_87;
  wire mul_r0_n_88;
  wire mul_r0_n_89;
  wire mul_r0_n_90;
  wire mul_r0_n_91;
  wire mul_r0_n_92;
  wire mul_r0_n_93;
  wire mul_r0_n_94;
  wire mul_r0_n_95;
  wire mul_r0_n_96;
  wire mul_r0_n_97;
  wire mul_r0_n_98;
  wire mul_r0_n_99;
  wire mul_r_reg__0_n_58;
  wire mul_r_reg__0_n_59;
  wire mul_r_reg__0_n_60;
  wire mul_r_reg__0_n_61;
  wire mul_r_reg__0_n_62;
  wire mul_r_reg__0_n_63;
  wire mul_r_reg__0_n_64;
  wire mul_r_reg__0_n_65;
  wire mul_r_reg__0_n_66;
  wire mul_r_reg__0_n_67;
  wire mul_r_reg__0_n_68;
  wire mul_r_reg__0_n_69;
  wire mul_r_reg__0_n_70;
  wire mul_r_reg__0_n_71;
  wire mul_r_reg__0_n_72;
  wire mul_r_reg__0_n_73;
  wire mul_r_reg__0_n_74;
  wire [47:0]mul_r_reg__1;
  wire p_0_in__0;
  wire [0:0]\slv_regs_reg[0][0] ;
  wire [0:0]\slv_regs_reg[0][12] ;
  wire [7:0]\slv_regs_reg[0][8] ;
  wire [3:0]NLW_add_r1_carry_O_UNCONNECTED;
  wire [3:1]NLW_add_r1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_add_r1_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_add_r_reg[44]_i_1__6_CO_UNCONNECTED ;
  wire NLW_mul_r0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r0_CARRYOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r_reg__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r_reg__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_r_reg__0_PCOUT_UNCONNECTED;

  CARRY4 add_r1_carry
       (.CI(1'b0),
        .CO({add_r1_carry_n_0,add_r1_carry_n_1,add_r1_carry_n_2,add_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry_O_UNCONNECTED[3:0]),
        .S({S,add_r1_carry_i_2__6_n_0,add_r1_carry_i_3__6_n_0,add_r1_carry_i_4__6_n_0}));
  CARRY4 add_r1_carry__0
       (.CI(add_r1_carry_n_0),
        .CO({NLW_add_r1_carry__0_CO_UNCONNECTED[3:1],\counter_reg[1]_0 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,O}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_2__6
       (.I0(\counter_reg_n_0_[7] ),
        .I1(\slv_regs_reg[0][8] [6]),
        .I2(\counter_reg_n_0_[6] ),
        .I3(\slv_regs_reg[0][8] [5]),
        .I4(\slv_regs_reg[0][8] [7]),
        .I5(\counter_reg_n_0_[8] ),
        .O(add_r1_carry_i_2__6_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_3__6
       (.I0(\counter_reg_n_0_[4] ),
        .I1(\slv_regs_reg[0][8] [3]),
        .I2(\counter_reg_n_0_[3] ),
        .I3(\slv_regs_reg[0][8] [2]),
        .I4(\slv_regs_reg[0][8] [4]),
        .I5(\counter_reg_n_0_[5] ),
        .O(add_r1_carry_i_3__6_n_0));
  LUT6 #(
    .INIT(64'h0990000000000990)) 
    add_r1_carry_i_4__6
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\slv_regs_reg[0][8] [0]),
        .I2(\slv_regs_reg[0][0] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\slv_regs_reg[0][8] [1]),
        .I5(\counter_reg_n_0_[2] ),
        .O(add_r1_carry_i_4__6_n_0));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_2__6 
       (.I0(mul_r_reg__1[3]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[3]),
        .O(\add_r[0]_i_2__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_3__6 
       (.I0(mul_r_reg__1[2]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[2]),
        .O(\add_r[0]_i_3__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_4__6 
       (.I0(mul_r_reg__1[1]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[1]),
        .O(\add_r[0]_i_4__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[0]_i_5__6 
       (.I0(mul_r_reg__1[0]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[0]),
        .O(\add_r[0]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_2__6 
       (.I0(mul_r_reg__1[15]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[15]),
        .O(\add_r[12]_i_2__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_3__6 
       (.I0(mul_r_reg__1[14]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[14]),
        .O(\add_r[12]_i_3__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_4__6 
       (.I0(mul_r_reg__1[13]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[13]),
        .O(\add_r[12]_i_4__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[12]_i_5__6 
       (.I0(mul_r_reg__1[12]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[12]),
        .O(\add_r[12]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_2__6 
       (.I0(mul_r_reg__1[19]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[19]),
        .O(\add_r[16]_i_2__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_3__6 
       (.I0(mul_r_reg__1[18]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[18]),
        .O(\add_r[16]_i_3__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_4__6 
       (.I0(mul_r_reg__1[17]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[17]),
        .O(\add_r[16]_i_4__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[16]_i_5__6 
       (.I0(mul_r_reg__1[16]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[16]),
        .O(\add_r[16]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_2__6 
       (.I0(mul_r_reg__1[23]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[23]),
        .O(\add_r[20]_i_2__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_3__6 
       (.I0(mul_r_reg__1[22]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[22]),
        .O(\add_r[20]_i_3__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_4__6 
       (.I0(mul_r_reg__1[21]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[21]),
        .O(\add_r[20]_i_4__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[20]_i_5__6 
       (.I0(mul_r_reg__1[20]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[20]),
        .O(\add_r[20]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_2__6 
       (.I0(mul_r_reg__1[27]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[27]),
        .O(\add_r[24]_i_2__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_3__6 
       (.I0(mul_r_reg__1[26]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[26]),
        .O(\add_r[24]_i_3__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_4__6 
       (.I0(mul_r_reg__1[25]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[25]),
        .O(\add_r[24]_i_4__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[24]_i_5__6 
       (.I0(mul_r_reg__1[24]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[24]),
        .O(\add_r[24]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_2__6 
       (.I0(mul_r_reg__1[31]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[31]),
        .O(\add_r[28]_i_2__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_3__6 
       (.I0(mul_r_reg__1[30]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[30]),
        .O(\add_r[28]_i_3__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_4__6 
       (.I0(mul_r_reg__1[29]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[29]),
        .O(\add_r[28]_i_4__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[28]_i_5__6 
       (.I0(mul_r_reg__1[28]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[28]),
        .O(\add_r[28]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_2__6 
       (.I0(mul_r_reg__1[35]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[35]),
        .O(\add_r[32]_i_2__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_3__6 
       (.I0(mul_r_reg__1[34]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[34]),
        .O(\add_r[32]_i_3__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_4__6 
       (.I0(mul_r_reg__1[33]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[33]),
        .O(\add_r[32]_i_4__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[32]_i_5__6 
       (.I0(mul_r_reg__1[32]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[32]),
        .O(\add_r[32]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_2__6 
       (.I0(mul_r_reg__1[39]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[39]),
        .O(\add_r[36]_i_2__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_3__6 
       (.I0(mul_r_reg__1[38]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[38]),
        .O(\add_r[36]_i_3__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_4__6 
       (.I0(mul_r_reg__1[37]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[37]),
        .O(\add_r[36]_i_4__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[36]_i_5__6 
       (.I0(mul_r_reg__1[36]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[36]),
        .O(\add_r[36]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_2__6 
       (.I0(mul_r_reg__1[43]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[43]),
        .O(\add_r[40]_i_2__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_3__6 
       (.I0(mul_r_reg__1[42]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[42]),
        .O(\add_r[40]_i_3__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_4__6 
       (.I0(mul_r_reg__1[41]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[41]),
        .O(\add_r[40]_i_4__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[40]_i_5__6 
       (.I0(mul_r_reg__1[40]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[40]),
        .O(\add_r[40]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_2__6 
       (.I0(D[47]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[47]),
        .O(\add_r[44]_i_2__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_3__6 
       (.I0(mul_r_reg__1[46]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[46]),
        .O(\add_r[44]_i_3__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_4__6 
       (.I0(mul_r_reg__1[45]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[45]),
        .O(\add_r[44]_i_4__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_5__6 
       (.I0(mul_r_reg__1[44]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[44]),
        .O(\add_r[44]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_2__6 
       (.I0(mul_r_reg__1[7]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[7]),
        .O(\add_r[4]_i_2__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_3__6 
       (.I0(mul_r_reg__1[6]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[6]),
        .O(\add_r[4]_i_3__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_4__6 
       (.I0(mul_r_reg__1[5]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[5]),
        .O(\add_r[4]_i_4__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[4]_i_5__6 
       (.I0(mul_r_reg__1[4]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[4]),
        .O(\add_r[4]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_2__6 
       (.I0(mul_r_reg__1[11]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[11]),
        .O(\add_r[8]_i_2__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_3__6 
       (.I0(mul_r_reg__1[10]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[10]),
        .O(\add_r[8]_i_3__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_4__6 
       (.I0(mul_r_reg__1[9]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[9]),
        .O(\add_r[8]_i_4__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[8]_i_5__6 
       (.I0(mul_r_reg__1[8]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[8]),
        .O(\add_r[8]_i_5__6_n_0 ));
  FDRE \add_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__6_n_7 ),
        .Q(D[0]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[0]_i_1__6 
       (.CI(1'b0),
        .CO({\add_r_reg[0]_i_1__6_n_0 ,\add_r_reg[0]_i_1__6_n_1 ,\add_r_reg[0]_i_1__6_n_2 ,\add_r_reg[0]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[3:0]),
        .O({\add_r_reg[0]_i_1__6_n_4 ,\add_r_reg[0]_i_1__6_n_5 ,\add_r_reg[0]_i_1__6_n_6 ,\add_r_reg[0]_i_1__6_n_7 }),
        .S({\add_r[0]_i_2__6_n_0 ,\add_r[0]_i_3__6_n_0 ,\add_r[0]_i_4__6_n_0 ,\add_r[0]_i_5__6_n_0 }));
  FDRE \add_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__6_n_5 ),
        .Q(D[10]),
        .R(p_0_in__0));
  FDRE \add_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__6_n_4 ),
        .Q(D[11]),
        .R(p_0_in__0));
  FDRE \add_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__6_n_7 ),
        .Q(D[12]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[12]_i_1__6 
       (.CI(\add_r_reg[8]_i_1__6_n_0 ),
        .CO({\add_r_reg[12]_i_1__6_n_0 ,\add_r_reg[12]_i_1__6_n_1 ,\add_r_reg[12]_i_1__6_n_2 ,\add_r_reg[12]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[15:12]),
        .O({\add_r_reg[12]_i_1__6_n_4 ,\add_r_reg[12]_i_1__6_n_5 ,\add_r_reg[12]_i_1__6_n_6 ,\add_r_reg[12]_i_1__6_n_7 }),
        .S({\add_r[12]_i_2__6_n_0 ,\add_r[12]_i_3__6_n_0 ,\add_r[12]_i_4__6_n_0 ,\add_r[12]_i_5__6_n_0 }));
  FDRE \add_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__6_n_6 ),
        .Q(D[13]),
        .R(p_0_in__0));
  FDRE \add_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__6_n_5 ),
        .Q(D[14]),
        .R(p_0_in__0));
  FDRE \add_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__6_n_4 ),
        .Q(D[15]),
        .R(p_0_in__0));
  FDRE \add_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__6_n_7 ),
        .Q(D[16]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[16]_i_1__6 
       (.CI(\add_r_reg[12]_i_1__6_n_0 ),
        .CO({\add_r_reg[16]_i_1__6_n_0 ,\add_r_reg[16]_i_1__6_n_1 ,\add_r_reg[16]_i_1__6_n_2 ,\add_r_reg[16]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[19:16]),
        .O({\add_r_reg[16]_i_1__6_n_4 ,\add_r_reg[16]_i_1__6_n_5 ,\add_r_reg[16]_i_1__6_n_6 ,\add_r_reg[16]_i_1__6_n_7 }),
        .S({\add_r[16]_i_2__6_n_0 ,\add_r[16]_i_3__6_n_0 ,\add_r[16]_i_4__6_n_0 ,\add_r[16]_i_5__6_n_0 }));
  FDRE \add_r_reg[17] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__6_n_6 ),
        .Q(D[17]),
        .R(p_0_in__0));
  FDRE \add_r_reg[18] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__6_n_5 ),
        .Q(D[18]),
        .R(p_0_in__0));
  FDRE \add_r_reg[19] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__6_n_4 ),
        .Q(D[19]),
        .R(p_0_in__0));
  FDRE \add_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__6_n_6 ),
        .Q(D[1]),
        .R(p_0_in__0));
  FDRE \add_r_reg[20] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__6_n_7 ),
        .Q(D[20]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[20]_i_1__6 
       (.CI(\add_r_reg[16]_i_1__6_n_0 ),
        .CO({\add_r_reg[20]_i_1__6_n_0 ,\add_r_reg[20]_i_1__6_n_1 ,\add_r_reg[20]_i_1__6_n_2 ,\add_r_reg[20]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[23:20]),
        .O({\add_r_reg[20]_i_1__6_n_4 ,\add_r_reg[20]_i_1__6_n_5 ,\add_r_reg[20]_i_1__6_n_6 ,\add_r_reg[20]_i_1__6_n_7 }),
        .S({\add_r[20]_i_2__6_n_0 ,\add_r[20]_i_3__6_n_0 ,\add_r[20]_i_4__6_n_0 ,\add_r[20]_i_5__6_n_0 }));
  FDRE \add_r_reg[21] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__6_n_6 ),
        .Q(D[21]),
        .R(p_0_in__0));
  FDRE \add_r_reg[22] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__6_n_5 ),
        .Q(D[22]),
        .R(p_0_in__0));
  FDRE \add_r_reg[23] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__6_n_4 ),
        .Q(D[23]),
        .R(p_0_in__0));
  FDRE \add_r_reg[24] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__6_n_7 ),
        .Q(D[24]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[24]_i_1__6 
       (.CI(\add_r_reg[20]_i_1__6_n_0 ),
        .CO({\add_r_reg[24]_i_1__6_n_0 ,\add_r_reg[24]_i_1__6_n_1 ,\add_r_reg[24]_i_1__6_n_2 ,\add_r_reg[24]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[27:24]),
        .O({\add_r_reg[24]_i_1__6_n_4 ,\add_r_reg[24]_i_1__6_n_5 ,\add_r_reg[24]_i_1__6_n_6 ,\add_r_reg[24]_i_1__6_n_7 }),
        .S({\add_r[24]_i_2__6_n_0 ,\add_r[24]_i_3__6_n_0 ,\add_r[24]_i_4__6_n_0 ,\add_r[24]_i_5__6_n_0 }));
  FDRE \add_r_reg[25] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__6_n_6 ),
        .Q(D[25]),
        .R(p_0_in__0));
  FDRE \add_r_reg[26] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__6_n_5 ),
        .Q(D[26]),
        .R(p_0_in__0));
  FDRE \add_r_reg[27] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__6_n_4 ),
        .Q(D[27]),
        .R(p_0_in__0));
  FDRE \add_r_reg[28] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__6_n_7 ),
        .Q(D[28]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[28]_i_1__6 
       (.CI(\add_r_reg[24]_i_1__6_n_0 ),
        .CO({\add_r_reg[28]_i_1__6_n_0 ,\add_r_reg[28]_i_1__6_n_1 ,\add_r_reg[28]_i_1__6_n_2 ,\add_r_reg[28]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[31:28]),
        .O({\add_r_reg[28]_i_1__6_n_4 ,\add_r_reg[28]_i_1__6_n_5 ,\add_r_reg[28]_i_1__6_n_6 ,\add_r_reg[28]_i_1__6_n_7 }),
        .S({\add_r[28]_i_2__6_n_0 ,\add_r[28]_i_3__6_n_0 ,\add_r[28]_i_4__6_n_0 ,\add_r[28]_i_5__6_n_0 }));
  FDRE \add_r_reg[29] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__6_n_6 ),
        .Q(D[29]),
        .R(p_0_in__0));
  FDRE \add_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__6_n_5 ),
        .Q(D[2]),
        .R(p_0_in__0));
  FDRE \add_r_reg[30] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__6_n_5 ),
        .Q(D[30]),
        .R(p_0_in__0));
  FDRE \add_r_reg[31] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__6_n_4 ),
        .Q(D[31]),
        .R(p_0_in__0));
  FDRE \add_r_reg[32] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__6_n_7 ),
        .Q(D[32]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[32]_i_1__6 
       (.CI(\add_r_reg[28]_i_1__6_n_0 ),
        .CO({\add_r_reg[32]_i_1__6_n_0 ,\add_r_reg[32]_i_1__6_n_1 ,\add_r_reg[32]_i_1__6_n_2 ,\add_r_reg[32]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[35:32]),
        .O({\add_r_reg[32]_i_1__6_n_4 ,\add_r_reg[32]_i_1__6_n_5 ,\add_r_reg[32]_i_1__6_n_6 ,\add_r_reg[32]_i_1__6_n_7 }),
        .S({\add_r[32]_i_2__6_n_0 ,\add_r[32]_i_3__6_n_0 ,\add_r[32]_i_4__6_n_0 ,\add_r[32]_i_5__6_n_0 }));
  FDRE \add_r_reg[33] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__6_n_6 ),
        .Q(D[33]),
        .R(p_0_in__0));
  FDRE \add_r_reg[34] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__6_n_5 ),
        .Q(D[34]),
        .R(p_0_in__0));
  FDRE \add_r_reg[35] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__6_n_4 ),
        .Q(D[35]),
        .R(p_0_in__0));
  FDRE \add_r_reg[36] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__6_n_7 ),
        .Q(D[36]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[36]_i_1__6 
       (.CI(\add_r_reg[32]_i_1__6_n_0 ),
        .CO({\add_r_reg[36]_i_1__6_n_0 ,\add_r_reg[36]_i_1__6_n_1 ,\add_r_reg[36]_i_1__6_n_2 ,\add_r_reg[36]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[39:36]),
        .O({\add_r_reg[36]_i_1__6_n_4 ,\add_r_reg[36]_i_1__6_n_5 ,\add_r_reg[36]_i_1__6_n_6 ,\add_r_reg[36]_i_1__6_n_7 }),
        .S({\add_r[36]_i_2__6_n_0 ,\add_r[36]_i_3__6_n_0 ,\add_r[36]_i_4__6_n_0 ,\add_r[36]_i_5__6_n_0 }));
  FDRE \add_r_reg[37] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__6_n_6 ),
        .Q(D[37]),
        .R(p_0_in__0));
  FDRE \add_r_reg[38] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__6_n_5 ),
        .Q(D[38]),
        .R(p_0_in__0));
  FDRE \add_r_reg[39] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__6_n_4 ),
        .Q(D[39]),
        .R(p_0_in__0));
  FDRE \add_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__6_n_4 ),
        .Q(D[3]),
        .R(p_0_in__0));
  FDRE \add_r_reg[40] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__6_n_7 ),
        .Q(D[40]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[40]_i_1__6 
       (.CI(\add_r_reg[36]_i_1__6_n_0 ),
        .CO({\add_r_reg[40]_i_1__6_n_0 ,\add_r_reg[40]_i_1__6_n_1 ,\add_r_reg[40]_i_1__6_n_2 ,\add_r_reg[40]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[43:40]),
        .O({\add_r_reg[40]_i_1__6_n_4 ,\add_r_reg[40]_i_1__6_n_5 ,\add_r_reg[40]_i_1__6_n_6 ,\add_r_reg[40]_i_1__6_n_7 }),
        .S({\add_r[40]_i_2__6_n_0 ,\add_r[40]_i_3__6_n_0 ,\add_r[40]_i_4__6_n_0 ,\add_r[40]_i_5__6_n_0 }));
  FDRE \add_r_reg[41] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__6_n_6 ),
        .Q(D[41]),
        .R(p_0_in__0));
  FDRE \add_r_reg[42] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__6_n_5 ),
        .Q(D[42]),
        .R(p_0_in__0));
  FDRE \add_r_reg[43] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__6_n_4 ),
        .Q(D[43]),
        .R(p_0_in__0));
  FDRE \add_r_reg[44] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__6_n_7 ),
        .Q(D[44]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[44]_i_1__6 
       (.CI(\add_r_reg[40]_i_1__6_n_0 ),
        .CO({\NLW_add_r_reg[44]_i_1__6_CO_UNCONNECTED [3],\add_r_reg[44]_i_1__6_n_1 ,\add_r_reg[44]_i_1__6_n_2 ,\add_r_reg[44]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,mul_r_reg__1[46:44]}),
        .O({\add_r_reg[44]_i_1__6_n_4 ,\add_r_reg[44]_i_1__6_n_5 ,\add_r_reg[44]_i_1__6_n_6 ,\add_r_reg[44]_i_1__6_n_7 }),
        .S({\add_r[44]_i_2__6_n_0 ,\add_r[44]_i_3__6_n_0 ,\add_r[44]_i_4__6_n_0 ,\add_r[44]_i_5__6_n_0 }));
  FDRE \add_r_reg[45] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__6_n_6 ),
        .Q(D[45]),
        .R(p_0_in__0));
  FDRE \add_r_reg[46] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__6_n_5 ),
        .Q(D[46]),
        .R(p_0_in__0));
  FDRE \add_r_reg[47] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__6_n_4 ),
        .Q(D[47]),
        .R(p_0_in__0));
  FDRE \add_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__6_n_7 ),
        .Q(D[4]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[4]_i_1__6 
       (.CI(\add_r_reg[0]_i_1__6_n_0 ),
        .CO({\add_r_reg[4]_i_1__6_n_0 ,\add_r_reg[4]_i_1__6_n_1 ,\add_r_reg[4]_i_1__6_n_2 ,\add_r_reg[4]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[7:4]),
        .O({\add_r_reg[4]_i_1__6_n_4 ,\add_r_reg[4]_i_1__6_n_5 ,\add_r_reg[4]_i_1__6_n_6 ,\add_r_reg[4]_i_1__6_n_7 }),
        .S({\add_r[4]_i_2__6_n_0 ,\add_r[4]_i_3__6_n_0 ,\add_r[4]_i_4__6_n_0 ,\add_r[4]_i_5__6_n_0 }));
  FDRE \add_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__6_n_6 ),
        .Q(D[5]),
        .R(p_0_in__0));
  FDRE \add_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__6_n_5 ),
        .Q(D[6]),
        .R(p_0_in__0));
  FDRE \add_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__6_n_4 ),
        .Q(D[7]),
        .R(p_0_in__0));
  FDRE \add_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__6_n_7 ),
        .Q(D[8]),
        .R(p_0_in__0));
  CARRY4 \add_r_reg[8]_i_1__6 
       (.CI(\add_r_reg[4]_i_1__6_n_0 ),
        .CO({\add_r_reg[8]_i_1__6_n_0 ,\add_r_reg[8]_i_1__6_n_1 ,\add_r_reg[8]_i_1__6_n_2 ,\add_r_reg[8]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI(mul_r_reg__1[11:8]),
        .O({\add_r_reg[8]_i_1__6_n_4 ,\add_r_reg[8]_i_1__6_n_5 ,\add_r_reg[8]_i_1__6_n_6 ,\add_r_reg[8]_i_1__6_n_7 }),
        .S({\add_r[8]_i_2__6_n_0 ,\add_r[8]_i_3__6_n_0 ,\add_r[8]_i_4__6_n_0 ,\add_r[8]_i_5__6_n_0 }));
  FDRE \add_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__6_n_6 ),
        .Q(D[9]),
        .R(p_0_in__0));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter[0]_i_1__9 
       (.I0(\counter_reg_n_0_[0] ),
        .O(\counter[0]_i_1__9_n_0 ));
  LUT4 #(
    .INIT(16'hF7C8)) 
    \counter[1]_i_1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(dp_enable),
        .I2(\counter_reg[1]_0 ),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \counter[2]_i_1__8 
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[2] ),
        .O(\counter[2]_i_1__8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \counter[3]_i_1__8 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .O(\counter[3]_i_1__8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \counter[4]_i_1__7 
       (.I0(\counter_reg_n_0_[3] ),
        .I1(\counter_reg_n_0_[2] ),
        .I2(\counter_reg_n_0_[1] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[4] ),
        .O(\counter[4]_i_1__7_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \counter[5]_i_1__7 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .I4(\counter_reg_n_0_[4] ),
        .I5(\counter_reg_n_0_[5] ),
        .O(\counter[5]_i_1__7_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \counter[6]_i_1__6 
       (.I0(\counter[8]_i_3__7_n_0 ),
        .I1(\counter_reg_n_0_[6] ),
        .O(\counter[6]_i_1__6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'hB4)) 
    \counter[7]_i_1__6 
       (.I0(\counter[8]_i_3__7_n_0 ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(\counter_reg_n_0_[7] ),
        .O(\counter[7]_i_1__6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT4 #(
    .INIT(16'hF708)) 
    \counter[8]_i_2__7 
       (.I0(\counter_reg_n_0_[7] ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(\counter[8]_i_3__7_n_0 ),
        .I3(\counter_reg_n_0_[8] ),
        .O(\counter[8]_i_2__7_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \counter[8]_i_3__7 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[3] ),
        .I4(\counter_reg_n_0_[4] ),
        .I5(\counter_reg_n_0_[5] ),
        .O(\counter[8]_i_3__7_n_0 ));
  FDRE \counter_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[0]_i_1__9_n_0 ),
        .Q(\counter_reg_n_0_[0] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\counter[1]_i_1_n_0 ),
        .Q(\counter_reg_n_0_[1] ),
        .R(p_0_in__0));
  FDRE \counter_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[2]_i_1__8_n_0 ),
        .Q(\counter_reg_n_0_[2] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[3]_i_1__8_n_0 ),
        .Q(\counter_reg_n_0_[3] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[4]_i_1__7_n_0 ),
        .Q(\counter_reg_n_0_[4] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[5]_i_1__7_n_0 ),
        .Q(\counter_reg_n_0_[5] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[6]_i_1__6_n_0 ),
        .Q(\counter_reg_n_0_[6] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[7]_i_1__6_n_0 ),
        .Q(\counter_reg_n_0_[7] ),
        .R(\slv_regs_reg[0][12] ));
  FDRE \counter_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[8]_i_2__7_n_0 ),
        .Q(\counter_reg_n_0_[8] ),
        .R(\slv_regs_reg[0][12] ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,b_ram_out[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({A[16],A[16],A[16],A[14:0]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r0_OVERFLOW_UNCONNECTED),
        .P({mul_r0_n_58,mul_r0_n_59,mul_r0_n_60,mul_r0_n_61,mul_r0_n_62,mul_r0_n_63,mul_r0_n_64,mul_r0_n_65,mul_r0_n_66,mul_r0_n_67,mul_r0_n_68,mul_r0_n_69,mul_r0_n_70,mul_r0_n_71,mul_r0_n_72,mul_r0_n_73,mul_r0_n_74,mul_r0_n_75,mul_r0_n_76,mul_r0_n_77,mul_r0_n_78,mul_r0_n_79,mul_r0_n_80,mul_r0_n_81,mul_r0_n_82,mul_r0_n_83,mul_r0_n_84,mul_r0_n_85,mul_r0_n_86,mul_r0_n_87,mul_r0_n_88,mul_r0_n_89,mul_r0_n_90,mul_r0_n_91,mul_r0_n_92,mul_r0_n_93,mul_r0_n_94,mul_r0_n_95,mul_r0_n_96,mul_r0_n_97,mul_r0_n_98,mul_r0_n_99,mul_r0_n_100,mul_r0_n_101,mul_r0_n_102,mul_r0_n_103,mul_r0_n_104,mul_r0_n_105}),
        .PATTERNBDETECT(NLW_mul_r0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_r0_UNDERFLOW_UNCONNECTED));
  FDRE \mul_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_105),
        .Q(mul_r_reg__1[0]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_95),
        .Q(mul_r_reg__1[10]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_94),
        .Q(mul_r_reg__1[11]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_93),
        .Q(mul_r_reg__1[12]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_92),
        .Q(mul_r_reg__1[13]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_91),
        .Q(mul_r_reg__1[14]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_90),
        .Q(mul_r_reg__1[15]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_89),
        .Q(mul_r_reg__1[16]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_104),
        .Q(mul_r_reg__1[1]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_103),
        .Q(mul_r_reg__1[2]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_102),
        .Q(mul_r_reg__1[3]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_101),
        .Q(mul_r_reg__1[4]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_100),
        .Q(mul_r_reg__1[5]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_99),
        .Q(mul_r_reg__1[6]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_98),
        .Q(mul_r_reg__1[7]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_97),
        .Q(mul_r_reg__1[8]),
        .R(p_0_in__0));
  FDRE \mul_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_96),
        .Q(mul_r_reg__1[9]),
        .R(p_0_in__0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-12 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r_reg__0
       (.A({A[16],A[16],A[16],A[16],A[16:15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r_reg__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({b_ram_out[31],b_ram_out[31],b_ram_out[31],b_ram_out[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r_reg__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(dp_enable),
        .CLK(clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED),
        .P({mul_r_reg__0_n_58,mul_r_reg__0_n_59,mul_r_reg__0_n_60,mul_r_reg__0_n_61,mul_r_reg__0_n_62,mul_r_reg__0_n_63,mul_r_reg__0_n_64,mul_r_reg__0_n_65,mul_r_reg__0_n_66,mul_r_reg__0_n_67,mul_r_reg__0_n_68,mul_r_reg__0_n_69,mul_r_reg__0_n_70,mul_r_reg__0_n_71,mul_r_reg__0_n_72,mul_r_reg__0_n_73,mul_r_reg__0_n_74,mul_r_reg__1[47:17]}),
        .PATTERNBDETECT(NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .PCOUT(NLW_mul_r_reg__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(p_0_in__0),
        .UNDERFLOW(NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "dot_product_module" *) 
module design_1_top_0_0_dot_product_module
   (D,
    CO,
    \counter_reg[1] ,
    \counter_reg[1]_0 ,
    \counter_reg[1]_1 ,
    \counter_reg[1]_2 ,
    \counter_reg[1]_3 ,
    \counter_reg[1]_4 ,
    \counter_reg[1]_5 ,
    start_reg,
    E,
    \counter_reg[3] ,
    p_rdy,
    A,
    b_ram_out,
    dp_enable,
    clk,
    p_0_in__0,
    B,
    \in_raw_delay_reg[15]_rep__9 ,
    \in_raw_delay_reg[15]_rep__8 ,
    \in_raw_delay_reg[15]_rep__7 ,
    \in_raw_delay_reg[15]_rep__6 ,
    \in_raw_delay_reg[15]_rep__4 ,
    \in_raw_delay_reg[15]_rep__2 ,
    \in_raw_delay_reg[15]_rep__0 ,
    \slv_regs_reg[0][11] ,
    O,
    \slv_regs_reg[0][11]_0 ,
    \slv_regs_reg[0][11]_1 ,
    \slv_regs_reg[0][11]_2 ,
    \slv_regs_reg[0][11]_3 ,
    \slv_regs_reg[0][11]_4 ,
    \slv_regs_reg[0][11]_5 ,
    S,
    aresetn,
    start,
    Q,
    \slv_regs_reg[0][8] ,
    \slv_regs_reg[0][0] ,
    SR,
    \slv_regs_reg[0][12] ,
    \slv_regs_reg[0][12]_0 ,
    \slv_regs_reg[0][12]_1 ,
    \slv_regs_reg[0][12]_2 ,
    \slv_regs_reg[0][12]_3 ,
    \slv_regs_reg[0][12]_4 ,
    \slv_regs_reg[0][12]_5 );
  output [383:0]D;
  output [0:0]CO;
  output [0:0]\counter_reg[1] ;
  output [0:0]\counter_reg[1]_0 ;
  output [0:0]\counter_reg[1]_1 ;
  output [0:0]\counter_reg[1]_2 ;
  output [0:0]\counter_reg[1]_3 ;
  output [0:0]\counter_reg[1]_4 ;
  output [0:0]\counter_reg[1]_5 ;
  output start_reg;
  output [0:0]E;
  output [0:0]\counter_reg[3] ;
  output p_rdy;
  input [15:0]A;
  input [255:0]b_ram_out;
  input dp_enable;
  input clk;
  input p_0_in__0;
  input [0:0]B;
  input [0:0]\in_raw_delay_reg[15]_rep__9 ;
  input [0:0]\in_raw_delay_reg[15]_rep__8 ;
  input [0:0]\in_raw_delay_reg[15]_rep__7 ;
  input [1:0]\in_raw_delay_reg[15]_rep__6 ;
  input [1:0]\in_raw_delay_reg[15]_rep__4 ;
  input [1:0]\in_raw_delay_reg[15]_rep__2 ;
  input [0:0]\in_raw_delay_reg[15]_rep__0 ;
  input [0:0]\slv_regs_reg[0][11] ;
  input [0:0]O;
  input [0:0]\slv_regs_reg[0][11]_0 ;
  input [0:0]\slv_regs_reg[0][11]_1 ;
  input [0:0]\slv_regs_reg[0][11]_2 ;
  input [0:0]\slv_regs_reg[0][11]_3 ;
  input [0:0]\slv_regs_reg[0][11]_4 ;
  input [0:0]\slv_regs_reg[0][11]_5 ;
  input [0:0]S;
  input aresetn;
  input start;
  input [0:0]Q;
  input [7:0]\slv_regs_reg[0][8] ;
  input [0:0]\slv_regs_reg[0][0] ;
  input [0:0]SR;
  input [0:0]\slv_regs_reg[0][12] ;
  input [0:0]\slv_regs_reg[0][12]_0 ;
  input [0:0]\slv_regs_reg[0][12]_1 ;
  input [0:0]\slv_regs_reg[0][12]_2 ;
  input [0:0]\slv_regs_reg[0][12]_3 ;
  input [0:0]\slv_regs_reg[0][12]_4 ;
  input [0:0]\slv_regs_reg[0][12]_5 ;

  wire [15:0]A;
  wire [0:0]B;
  wire [0:0]CO;
  wire [383:0]D;
  wire [0:0]E;
  wire [0:0]O;
  wire [0:0]Q;
  wire [0:0]S;
  wire [0:0]SR;
  wire aresetn;
  wire [255:0]b_ram_out;
  wire clk;
  wire [0:0]\counter_reg[1] ;
  wire [0:0]\counter_reg[1]_0 ;
  wire [0:0]\counter_reg[1]_1 ;
  wire [0:0]\counter_reg[1]_2 ;
  wire [0:0]\counter_reg[1]_3 ;
  wire [0:0]\counter_reg[1]_4 ;
  wire [0:0]\counter_reg[1]_5 ;
  wire [0:0]\counter_reg[3] ;
  wire \dot_prod[6].dp_n_51 ;
  wire dp_enable;
  wire [0:0]\in_raw_delay_reg[15]_rep__0 ;
  wire [1:0]\in_raw_delay_reg[15]_rep__2 ;
  wire [1:0]\in_raw_delay_reg[15]_rep__4 ;
  wire [1:0]\in_raw_delay_reg[15]_rep__6 ;
  wire [0:0]\in_raw_delay_reg[15]_rep__7 ;
  wire [0:0]\in_raw_delay_reg[15]_rep__8 ;
  wire [0:0]\in_raw_delay_reg[15]_rep__9 ;
  wire p_0_in__0;
  wire p_rdy;
  wire [0:0]\slv_regs_reg[0][0] ;
  wire [0:0]\slv_regs_reg[0][11] ;
  wire [0:0]\slv_regs_reg[0][11]_0 ;
  wire [0:0]\slv_regs_reg[0][11]_1 ;
  wire [0:0]\slv_regs_reg[0][11]_2 ;
  wire [0:0]\slv_regs_reg[0][11]_3 ;
  wire [0:0]\slv_regs_reg[0][11]_4 ;
  wire [0:0]\slv_regs_reg[0][11]_5 ;
  wire [0:0]\slv_regs_reg[0][12] ;
  wire [0:0]\slv_regs_reg[0][12]_0 ;
  wire [0:0]\slv_regs_reg[0][12]_1 ;
  wire [0:0]\slv_regs_reg[0][12]_2 ;
  wire [0:0]\slv_regs_reg[0][12]_3 ;
  wire [0:0]\slv_regs_reg[0][12]_4 ;
  wire [0:0]\slv_regs_reg[0][12]_5 ;
  wire [7:0]\slv_regs_reg[0][8] ;
  wire start;
  wire start_reg;

  design_1_top_0_0_dot_product \dot_prod[0].dp 
       (.A(A),
        .CO(CO),
        .D(D[47:0]),
        .E(E),
        .O(O),
        .Q(Q),
        .SR(SR),
        .aresetn(aresetn),
        .b_ram_out(b_ram_out[31:0]),
        .clk(clk),
        .dp_enable(dp_enable),
        .p_0_in__0(p_0_in__0),
        .\slv_regs_reg[0][0] (\slv_regs_reg[0][0] ),
        .\slv_regs_reg[0][11] (\slv_regs_reg[0][11] ),
        .\slv_regs_reg[0][11]_0 (\dot_prod[6].dp_n_51 ),
        .\slv_regs_reg[0][11]_1 (\counter_reg[1] ),
        .\slv_regs_reg[0][11]_2 (\counter_reg[1]_0 ),
        .\slv_regs_reg[0][11]_3 (\counter_reg[1]_1 ),
        .\slv_regs_reg[0][8] (\slv_regs_reg[0][8] ),
        .start(start),
        .start_reg(start_reg));
  design_1_top_0_0_dot_product_0 \dot_prod[1].dp 
       (.A(A),
        .D(D[95:48]),
        .O(O),
        .b_ram_out(b_ram_out[63:32]),
        .clk(clk),
        .\counter_reg[1]_0 (\counter_reg[1] ),
        .dp_enable(dp_enable),
        .p_0_in__0(p_0_in__0),
        .\slv_regs_reg[0][0] (\slv_regs_reg[0][0] ),
        .\slv_regs_reg[0][11] (\slv_regs_reg[0][11]_0 ),
        .\slv_regs_reg[0][12] (\slv_regs_reg[0][12] ),
        .\slv_regs_reg[0][8] (\slv_regs_reg[0][8] ));
  design_1_top_0_0_dot_product_1 \dot_prod[2].dp 
       (.A({B,\in_raw_delay_reg[15]_rep__9 ,A[14:0]}),
        .D(D[143:96]),
        .O(O),
        .b_ram_out(b_ram_out[95:64]),
        .clk(clk),
        .\counter_reg[1]_0 (\counter_reg[1]_0 ),
        .dp_enable(dp_enable),
        .p_0_in__0(p_0_in__0),
        .\slv_regs_reg[0][0] (\slv_regs_reg[0][0] ),
        .\slv_regs_reg[0][11] (\slv_regs_reg[0][11]_1 ),
        .\slv_regs_reg[0][12] (\slv_regs_reg[0][12]_0 ),
        .\slv_regs_reg[0][8] (\slv_regs_reg[0][8] ));
  design_1_top_0_0_dot_product_2 \dot_prod[3].dp 
       (.A({\in_raw_delay_reg[15]_rep__8 ,\in_raw_delay_reg[15]_rep__7 ,A[14:0]}),
        .D(D[191:144]),
        .O(O),
        .b_ram_out(b_ram_out[127:96]),
        .clk(clk),
        .\counter_reg[1]_0 (\counter_reg[1]_1 ),
        .dp_enable(dp_enable),
        .p_0_in__0(p_0_in__0),
        .\slv_regs_reg[0][0] (\slv_regs_reg[0][0] ),
        .\slv_regs_reg[0][11] (\slv_regs_reg[0][11]_2 ),
        .\slv_regs_reg[0][12] (\slv_regs_reg[0][12]_1 ),
        .\slv_regs_reg[0][8] (\slv_regs_reg[0][8] ));
  design_1_top_0_0_dot_product_3 \dot_prod[4].dp 
       (.B({\in_raw_delay_reg[15]_rep__7 ,\in_raw_delay_reg[15]_rep__6 [1],A[14:0]}),
        .CO(\counter_reg[1]_2 ),
        .D(D[239:192]),
        .O(O),
        .b_ram_out(b_ram_out[159:128]),
        .clk(clk),
        .dp_enable(dp_enable),
        .\in_raw_delay_reg[15]_rep__5 (\in_raw_delay_reg[15]_rep__6 [0]),
        .p_0_in__0(p_0_in__0),
        .\slv_regs_reg[0][0] (\slv_regs_reg[0][0] ),
        .\slv_regs_reg[0][11] (\slv_regs_reg[0][11]_3 ),
        .\slv_regs_reg[0][12] (\slv_regs_reg[0][12]_2 ),
        .\slv_regs_reg[0][8] (\slv_regs_reg[0][8] ));
  design_1_top_0_0_dot_product_4 \dot_prod[5].dp 
       (.A({\in_raw_delay_reg[15]_rep__6 [0],\in_raw_delay_reg[15]_rep__4 ,A[14:0]}),
        .CO(\counter_reg[1]_3 ),
        .D(D[287:240]),
        .O(O),
        .b_ram_out(b_ram_out[191:160]),
        .clk(clk),
        .dp_enable(dp_enable),
        .p_0_in__0(p_0_in__0),
        .\slv_regs_reg[0][0] (\slv_regs_reg[0][0] ),
        .\slv_regs_reg[0][11] (\slv_regs_reg[0][11]_4 ),
        .\slv_regs_reg[0][12] (\slv_regs_reg[0][12]_3 ),
        .\slv_regs_reg[0][8] (\slv_regs_reg[0][8] ));
  design_1_top_0_0_dot_product_5 \dot_prod[6].dp 
       (.A({\in_raw_delay_reg[15]_rep__4 [0],\in_raw_delay_reg[15]_rep__2 ,A[14:0]}),
        .CO(\counter_reg[1]_4 ),
        .D(D[335:288]),
        .O(O),
        .b_ram_out(b_ram_out[223:192]),
        .clk(clk),
        .\counter_reg[3]_0 (\counter_reg[3] ),
        .dp_enable(dp_enable),
        .p_0_in__0(p_0_in__0),
        .p_rdy(p_rdy),
        .\res_mem_reg[383] (\dot_prod[6].dp_n_51 ),
        .\slv_regs_reg[0][0] (\slv_regs_reg[0][0] ),
        .\slv_regs_reg[0][11] (\slv_regs_reg[0][11]_5 ),
        .\slv_regs_reg[0][11]_0 (CO),
        .\slv_regs_reg[0][11]_1 (\counter_reg[1] ),
        .\slv_regs_reg[0][11]_2 (\counter_reg[1]_0 ),
        .\slv_regs_reg[0][11]_3 (\counter_reg[1]_1 ),
        .\slv_regs_reg[0][11]_4 (\counter_reg[1]_5 ),
        .\slv_regs_reg[0][11]_5 (\counter_reg[1]_2 ),
        .\slv_regs_reg[0][11]_6 (\counter_reg[1]_3 ),
        .\slv_regs_reg[0][12] (\slv_regs_reg[0][12]_4 ),
        .\slv_regs_reg[0][8] (\slv_regs_reg[0][8] ));
  design_1_top_0_0_dot_product_6 \dot_prod[7].dp 
       (.A({\in_raw_delay_reg[15]_rep__2 [0],\in_raw_delay_reg[15]_rep__0 ,A[14:0]}),
        .D(D[383:336]),
        .O(O),
        .S(S),
        .b_ram_out(b_ram_out[255:224]),
        .clk(clk),
        .\counter_reg[1]_0 (\counter_reg[1]_5 ),
        .dp_enable(dp_enable),
        .p_0_in__0(p_0_in__0),
        .\slv_regs_reg[0][0] (\slv_regs_reg[0][0] ),
        .\slv_regs_reg[0][12] (\slv_regs_reg[0][12]_5 ),
        .\slv_regs_reg[0][8] (\slv_regs_reg[0][8] ));
endmodule

(* ORIG_REF_NAME = "register_interface" *) 
module design_1_top_0_0_register_interface
   (s_axi_ctrl_status_awready,
    \axi_rdata_reg[0]_0 ,
    s_axi_ctrl_status_wready,
    \counter_reg[4] ,
    s_axi_ctrl_status_arready,
    s_axi_ctrl_status_bvalid,
    s_axi_ctrl_status_rvalid,
    E,
    \counter_reg[6] ,
    \count_i_reg[6] ,
    b_ram_data_reg,
    b_ram_data_reg_0,
    b_ram_data_reg_1,
    b_ram_data_reg_2,
    b_ram_data_reg_3,
    b_ram_data_reg_4,
    b_ram_data_reg_5,
    DIADI,
    DIBDI,
    SR,
    \add_r_reg[47] ,
    \counter_reg[8] ,
    \counter_reg[8]_0 ,
    \counter_reg[8]_1 ,
    \counter_reg[8]_2 ,
    \counter_reg[8]_3 ,
    \counter_reg[8]_4 ,
    \counter_reg[8]_5 ,
    S,
    \counter_reg[1] ,
    \counter_reg[1]_0 ,
    \counter_reg[1]_1 ,
    \counter_reg[1]_2 ,
    \counter_reg[1]_3 ,
    \counter_reg[1]_4 ,
    \counter_reg[1]_5 ,
    \counter_reg[1]_6 ,
    \axi_rdata_reg[0]_1 ,
    O,
    in_stream_ready_reg,
    \counter_reg[0] ,
    \counter_reg[4]_0 ,
    \state_reg[1] ,
    \state_reg[0] ,
    \read_address_reg[2] ,
    \read_address_reg[2]_0 ,
    \counter_reg[1]_7 ,
    \counter_reg[2] ,
    \counter_reg[3] ,
    \b_ram_sel_reg[0] ,
    WEBWE,
    \counter_reg[6]_0 ,
    \counter_reg[5] ,
    \counter_reg[4]_1 ,
    \read_address_reg[2]_1 ,
    s_axi_ctrl_status_rdata,
    clk,
    axi_wready_reg_0,
    axi_arready_reg_0,
    s_axi_ctrl_status_wvalid,
    s_axi_ctrl_status_awvalid,
    s_axi_ctrl_status_arvalid,
    aresetn,
    \state_reg[0]_0 ,
    initialized_reg,
    \state_reg[1]_0 ,
    counter2,
    Q,
    s_axis_tvalid,
    CO,
    \slv_regs_reg[0][11]_0 ,
    \slv_regs_reg[0][11]_1 ,
    \slv_regs_reg[0][11]_2 ,
    \slv_regs_reg[0][11]_3 ,
    \slv_regs_reg[0][11]_4 ,
    \slv_regs_reg[0][11]_5 ,
    \slv_regs_reg[0][11]_6 ,
    s_axis_tready,
    dp_extend_end,
    counter,
    \read_address_reg[5] ,
    DI,
    \read_address_reg[6] ,
    \slv_regs_reg[0][11]_7 ,
    s_axi_ctrl_status_awaddr,
    s_axi_ctrl_status_wdata,
    s_axi_ctrl_status_araddr,
    s_axi_ctrl_status_wstrb);
  output s_axi_ctrl_status_awready;
  output \axi_rdata_reg[0]_0 ;
  output s_axi_ctrl_status_wready;
  output \counter_reg[4] ;
  output s_axi_ctrl_status_arready;
  output s_axi_ctrl_status_bvalid;
  output s_axi_ctrl_status_rvalid;
  output [0:0]E;
  output \counter_reg[6] ;
  output \count_i_reg[6] ;
  output [0:0]b_ram_data_reg;
  output [0:0]b_ram_data_reg_0;
  output [0:0]b_ram_data_reg_1;
  output [0:0]b_ram_data_reg_2;
  output [0:0]b_ram_data_reg_3;
  output [0:0]b_ram_data_reg_4;
  output b_ram_data_reg_5;
  output [15:0]DIADI;
  output [15:0]DIBDI;
  output [0:0]SR;
  output \add_r_reg[47] ;
  output [0:0]\counter_reg[8] ;
  output [0:0]\counter_reg[8]_0 ;
  output [0:0]\counter_reg[8]_1 ;
  output [0:0]\counter_reg[8]_2 ;
  output [0:0]\counter_reg[8]_3 ;
  output [0:0]\counter_reg[8]_4 ;
  output [0:0]\counter_reg[8]_5 ;
  output [0:0]S;
  output [0:0]\counter_reg[1] ;
  output [0:0]\counter_reg[1]_0 ;
  output [0:0]\counter_reg[1]_1 ;
  output [0:0]\counter_reg[1]_2 ;
  output [0:0]\counter_reg[1]_3 ;
  output [0:0]\counter_reg[1]_4 ;
  output [0:0]\counter_reg[1]_5 ;
  output [7:0]\counter_reg[1]_6 ;
  output [0:0]\axi_rdata_reg[0]_1 ;
  output [0:0]O;
  output in_stream_ready_reg;
  output \counter_reg[0] ;
  output [0:0]\counter_reg[4]_0 ;
  output \state_reg[1] ;
  output \state_reg[0] ;
  output [2:0]\read_address_reg[2] ;
  output [0:0]\read_address_reg[2]_0 ;
  output \counter_reg[1]_7 ;
  output \counter_reg[2] ;
  output \counter_reg[3] ;
  output [0:0]\b_ram_sel_reg[0] ;
  output [0:0]WEBWE;
  output \counter_reg[6]_0 ;
  output \counter_reg[5] ;
  output \counter_reg[4]_1 ;
  output [2:0]\read_address_reg[2]_1 ;
  output [31:0]s_axi_ctrl_status_rdata;
  input clk;
  input axi_wready_reg_0;
  input axi_arready_reg_0;
  input s_axi_ctrl_status_wvalid;
  input s_axi_ctrl_status_awvalid;
  input s_axi_ctrl_status_arvalid;
  input aresetn;
  input \state_reg[0]_0 ;
  input initialized_reg;
  input \state_reg[1]_0 ;
  input [2:0]counter2;
  input [7:0]Q;
  input s_axis_tvalid;
  input [0:0]CO;
  input [0:0]\slv_regs_reg[0][11]_0 ;
  input [0:0]\slv_regs_reg[0][11]_1 ;
  input [0:0]\slv_regs_reg[0][11]_2 ;
  input [0:0]\slv_regs_reg[0][11]_3 ;
  input [0:0]\slv_regs_reg[0][11]_4 ;
  input [0:0]\slv_regs_reg[0][11]_5 ;
  input [0:0]\slv_regs_reg[0][11]_6 ;
  input s_axis_tready;
  input dp_extend_end;
  input [3:0]counter;
  input [5:0]\read_address_reg[5] ;
  input [0:0]DI;
  input [0:0]\read_address_reg[6] ;
  input [1:0]\slv_regs_reg[0][11]_7 ;
  input [3:0]s_axi_ctrl_status_awaddr;
  input [31:0]s_axi_ctrl_status_wdata;
  input [3:0]s_axi_ctrl_status_araddr;
  input [3:0]s_axi_ctrl_status_wstrb;

  wire [0:0]CO;
  wire [0:0]DI;
  wire [15:0]DIADI;
  wire [15:0]DIBDI;
  wire [0:0]E;
  wire [0:0]O;
  wire [7:0]Q;
  wire [0:0]S;
  wire [0:0]SR;
  wire [0:0]WEBWE;
  wire add_r1_carry__0_i_1_n_2;
  wire add_r1_carry__0_i_1_n_3;
  wire add_r1_carry_i_5_n_0;
  wire add_r1_carry_i_5_n_1;
  wire add_r1_carry_i_5_n_2;
  wire add_r1_carry_i_5_n_3;
  wire add_r1_carry_i_6_n_0;
  wire add_r1_carry_i_6_n_1;
  wire add_r1_carry_i_6_n_2;
  wire add_r1_carry_i_6_n_3;
  wire [12:9]add_r2;
  wire \add_r_reg[47] ;
  wire aresetn;
  wire axi_arready_i_1_n_0;
  wire axi_arready_reg_0;
  wire axi_awready_i_1_n_0;
  wire \axi_rdata[0]_i_4_n_0 ;
  wire \axi_rdata[0]_i_5_n_0 ;
  wire \axi_rdata[0]_i_6_n_0 ;
  wire \axi_rdata[0]_i_7_n_0 ;
  wire \axi_rdata[10]_i_4_n_0 ;
  wire \axi_rdata[10]_i_5_n_0 ;
  wire \axi_rdata[10]_i_6_n_0 ;
  wire \axi_rdata[10]_i_7_n_0 ;
  wire \axi_rdata[11]_i_4_n_0 ;
  wire \axi_rdata[11]_i_5_n_0 ;
  wire \axi_rdata[11]_i_6_n_0 ;
  wire \axi_rdata[11]_i_7_n_0 ;
  wire \axi_rdata[12]_i_4_n_0 ;
  wire \axi_rdata[12]_i_5_n_0 ;
  wire \axi_rdata[12]_i_6_n_0 ;
  wire \axi_rdata[12]_i_7_n_0 ;
  wire \axi_rdata[13]_i_4_n_0 ;
  wire \axi_rdata[13]_i_5_n_0 ;
  wire \axi_rdata[13]_i_6_n_0 ;
  wire \axi_rdata[13]_i_7_n_0 ;
  wire \axi_rdata[14]_i_4_n_0 ;
  wire \axi_rdata[14]_i_5_n_0 ;
  wire \axi_rdata[14]_i_6_n_0 ;
  wire \axi_rdata[14]_i_7_n_0 ;
  wire \axi_rdata[15]_i_4_n_0 ;
  wire \axi_rdata[15]_i_5_n_0 ;
  wire \axi_rdata[15]_i_6_n_0 ;
  wire \axi_rdata[15]_i_7_n_0 ;
  wire \axi_rdata[16]_i_4_n_0 ;
  wire \axi_rdata[16]_i_5_n_0 ;
  wire \axi_rdata[16]_i_6_n_0 ;
  wire \axi_rdata[16]_i_7_n_0 ;
  wire \axi_rdata[17]_i_4_n_0 ;
  wire \axi_rdata[17]_i_5_n_0 ;
  wire \axi_rdata[17]_i_6_n_0 ;
  wire \axi_rdata[17]_i_7_n_0 ;
  wire \axi_rdata[18]_i_4_n_0 ;
  wire \axi_rdata[18]_i_5_n_0 ;
  wire \axi_rdata[18]_i_6_n_0 ;
  wire \axi_rdata[18]_i_7_n_0 ;
  wire \axi_rdata[19]_i_4_n_0 ;
  wire \axi_rdata[19]_i_5_n_0 ;
  wire \axi_rdata[19]_i_6_n_0 ;
  wire \axi_rdata[19]_i_7_n_0 ;
  wire \axi_rdata[1]_i_4_n_0 ;
  wire \axi_rdata[1]_i_5_n_0 ;
  wire \axi_rdata[1]_i_6_n_0 ;
  wire \axi_rdata[1]_i_7_n_0 ;
  wire \axi_rdata[20]_i_4_n_0 ;
  wire \axi_rdata[20]_i_5_n_0 ;
  wire \axi_rdata[20]_i_6_n_0 ;
  wire \axi_rdata[20]_i_7_n_0 ;
  wire \axi_rdata[21]_i_4_n_0 ;
  wire \axi_rdata[21]_i_5_n_0 ;
  wire \axi_rdata[21]_i_6_n_0 ;
  wire \axi_rdata[21]_i_7_n_0 ;
  wire \axi_rdata[22]_i_4_n_0 ;
  wire \axi_rdata[22]_i_5_n_0 ;
  wire \axi_rdata[22]_i_6_n_0 ;
  wire \axi_rdata[22]_i_7_n_0 ;
  wire \axi_rdata[23]_i_4_n_0 ;
  wire \axi_rdata[23]_i_5_n_0 ;
  wire \axi_rdata[23]_i_6_n_0 ;
  wire \axi_rdata[23]_i_7_n_0 ;
  wire \axi_rdata[24]_i_4_n_0 ;
  wire \axi_rdata[24]_i_5_n_0 ;
  wire \axi_rdata[24]_i_6_n_0 ;
  wire \axi_rdata[24]_i_7_n_0 ;
  wire \axi_rdata[25]_i_4_n_0 ;
  wire \axi_rdata[25]_i_5_n_0 ;
  wire \axi_rdata[25]_i_6_n_0 ;
  wire \axi_rdata[25]_i_7_n_0 ;
  wire \axi_rdata[26]_i_4_n_0 ;
  wire \axi_rdata[26]_i_5_n_0 ;
  wire \axi_rdata[26]_i_6_n_0 ;
  wire \axi_rdata[26]_i_7_n_0 ;
  wire \axi_rdata[27]_i_4_n_0 ;
  wire \axi_rdata[27]_i_5_n_0 ;
  wire \axi_rdata[27]_i_6_n_0 ;
  wire \axi_rdata[27]_i_7_n_0 ;
  wire \axi_rdata[28]_i_4_n_0 ;
  wire \axi_rdata[28]_i_5_n_0 ;
  wire \axi_rdata[28]_i_6_n_0 ;
  wire \axi_rdata[28]_i_7_n_0 ;
  wire \axi_rdata[29]_i_4_n_0 ;
  wire \axi_rdata[29]_i_5_n_0 ;
  wire \axi_rdata[29]_i_6_n_0 ;
  wire \axi_rdata[29]_i_7_n_0 ;
  wire \axi_rdata[2]_i_4_n_0 ;
  wire \axi_rdata[2]_i_5_n_0 ;
  wire \axi_rdata[2]_i_6_n_0 ;
  wire \axi_rdata[2]_i_7_n_0 ;
  wire \axi_rdata[30]_i_4_n_0 ;
  wire \axi_rdata[30]_i_5_n_0 ;
  wire \axi_rdata[30]_i_6_n_0 ;
  wire \axi_rdata[30]_i_7_n_0 ;
  wire \axi_rdata[31]_i_1_n_0 ;
  wire \axi_rdata[31]_i_5_n_0 ;
  wire \axi_rdata[31]_i_6_n_0 ;
  wire \axi_rdata[31]_i_7_n_0 ;
  wire \axi_rdata[31]_i_8_n_0 ;
  wire \axi_rdata[3]_i_4_n_0 ;
  wire \axi_rdata[3]_i_5_n_0 ;
  wire \axi_rdata[3]_i_6_n_0 ;
  wire \axi_rdata[3]_i_7_n_0 ;
  wire \axi_rdata[4]_i_4_n_0 ;
  wire \axi_rdata[4]_i_5_n_0 ;
  wire \axi_rdata[4]_i_6_n_0 ;
  wire \axi_rdata[4]_i_7_n_0 ;
  wire \axi_rdata[5]_i_4_n_0 ;
  wire \axi_rdata[5]_i_5_n_0 ;
  wire \axi_rdata[5]_i_6_n_0 ;
  wire \axi_rdata[5]_i_7_n_0 ;
  wire \axi_rdata[6]_i_4_n_0 ;
  wire \axi_rdata[6]_i_5_n_0 ;
  wire \axi_rdata[6]_i_6_n_0 ;
  wire \axi_rdata[6]_i_7_n_0 ;
  wire \axi_rdata[7]_i_4_n_0 ;
  wire \axi_rdata[7]_i_5_n_0 ;
  wire \axi_rdata[7]_i_6_n_0 ;
  wire \axi_rdata[7]_i_7_n_0 ;
  wire \axi_rdata[8]_i_4_n_0 ;
  wire \axi_rdata[8]_i_5_n_0 ;
  wire \axi_rdata[8]_i_6_n_0 ;
  wire \axi_rdata[8]_i_7_n_0 ;
  wire \axi_rdata[9]_i_4_n_0 ;
  wire \axi_rdata[9]_i_5_n_0 ;
  wire \axi_rdata[9]_i_6_n_0 ;
  wire \axi_rdata[9]_i_7_n_0 ;
  wire \axi_rdata_reg[0]_0 ;
  wire [0:0]\axi_rdata_reg[0]_1 ;
  wire \axi_rdata_reg[0]_i_2_n_0 ;
  wire \axi_rdata_reg[0]_i_3_n_0 ;
  wire \axi_rdata_reg[10]_i_2_n_0 ;
  wire \axi_rdata_reg[10]_i_3_n_0 ;
  wire \axi_rdata_reg[11]_i_2_n_0 ;
  wire \axi_rdata_reg[11]_i_3_n_0 ;
  wire \axi_rdata_reg[12]_i_2_n_0 ;
  wire \axi_rdata_reg[12]_i_3_n_0 ;
  wire \axi_rdata_reg[13]_i_2_n_0 ;
  wire \axi_rdata_reg[13]_i_3_n_0 ;
  wire \axi_rdata_reg[14]_i_2_n_0 ;
  wire \axi_rdata_reg[14]_i_3_n_0 ;
  wire \axi_rdata_reg[15]_i_2_n_0 ;
  wire \axi_rdata_reg[15]_i_3_n_0 ;
  wire \axi_rdata_reg[16]_i_2_n_0 ;
  wire \axi_rdata_reg[16]_i_3_n_0 ;
  wire \axi_rdata_reg[17]_i_2_n_0 ;
  wire \axi_rdata_reg[17]_i_3_n_0 ;
  wire \axi_rdata_reg[18]_i_2_n_0 ;
  wire \axi_rdata_reg[18]_i_3_n_0 ;
  wire \axi_rdata_reg[19]_i_2_n_0 ;
  wire \axi_rdata_reg[19]_i_3_n_0 ;
  wire \axi_rdata_reg[1]_i_2_n_0 ;
  wire \axi_rdata_reg[1]_i_3_n_0 ;
  wire \axi_rdata_reg[20]_i_2_n_0 ;
  wire \axi_rdata_reg[20]_i_3_n_0 ;
  wire \axi_rdata_reg[21]_i_2_n_0 ;
  wire \axi_rdata_reg[21]_i_3_n_0 ;
  wire \axi_rdata_reg[22]_i_2_n_0 ;
  wire \axi_rdata_reg[22]_i_3_n_0 ;
  wire \axi_rdata_reg[23]_i_2_n_0 ;
  wire \axi_rdata_reg[23]_i_3_n_0 ;
  wire \axi_rdata_reg[24]_i_2_n_0 ;
  wire \axi_rdata_reg[24]_i_3_n_0 ;
  wire \axi_rdata_reg[25]_i_2_n_0 ;
  wire \axi_rdata_reg[25]_i_3_n_0 ;
  wire \axi_rdata_reg[26]_i_2_n_0 ;
  wire \axi_rdata_reg[26]_i_3_n_0 ;
  wire \axi_rdata_reg[27]_i_2_n_0 ;
  wire \axi_rdata_reg[27]_i_3_n_0 ;
  wire \axi_rdata_reg[28]_i_2_n_0 ;
  wire \axi_rdata_reg[28]_i_3_n_0 ;
  wire \axi_rdata_reg[29]_i_2_n_0 ;
  wire \axi_rdata_reg[29]_i_3_n_0 ;
  wire \axi_rdata_reg[2]_i_2_n_0 ;
  wire \axi_rdata_reg[2]_i_3_n_0 ;
  wire \axi_rdata_reg[30]_i_2_n_0 ;
  wire \axi_rdata_reg[30]_i_3_n_0 ;
  wire \axi_rdata_reg[31]_i_3_n_0 ;
  wire \axi_rdata_reg[31]_i_4_n_0 ;
  wire \axi_rdata_reg[3]_i_2_n_0 ;
  wire \axi_rdata_reg[3]_i_3_n_0 ;
  wire \axi_rdata_reg[4]_i_2_n_0 ;
  wire \axi_rdata_reg[4]_i_3_n_0 ;
  wire \axi_rdata_reg[5]_i_2_n_0 ;
  wire \axi_rdata_reg[5]_i_3_n_0 ;
  wire \axi_rdata_reg[6]_i_2_n_0 ;
  wire \axi_rdata_reg[6]_i_3_n_0 ;
  wire \axi_rdata_reg[7]_i_2_n_0 ;
  wire \axi_rdata_reg[7]_i_3_n_0 ;
  wire \axi_rdata_reg[8]_i_2_n_0 ;
  wire \axi_rdata_reg[8]_i_3_n_0 ;
  wire \axi_rdata_reg[9]_i_2_n_0 ;
  wire \axi_rdata_reg[9]_i_3_n_0 ;
  wire axi_wready_i_1_n_0;
  wire axi_wready_reg_0;
  wire [0:0]b_ram_data_reg;
  wire [0:0]b_ram_data_reg_0;
  wire [0:0]b_ram_data_reg_1;
  wire [0:0]b_ram_data_reg_2;
  wire [0:0]b_ram_data_reg_3;
  wire [0:0]b_ram_data_reg_4;
  wire b_ram_data_reg_5;
  wire [0:0]\b_ram_sel_reg[0] ;
  wire clk;
  wire \count_i_reg[6] ;
  wire [3:0]counter;
  wire [2:0]counter2;
  wire \counter[0]_i_2_n_0 ;
  wire \counter[6]_i_10_n_0 ;
  wire \counter[6]_i_11_n_0 ;
  wire \counter[6]_i_12_n_0 ;
  wire \counter[6]_i_13_n_0 ;
  wire \counter[6]_i_14_n_0 ;
  wire \counter[6]_i_15_n_0 ;
  wire \counter[6]_i_16_n_0 ;
  wire \counter[6]_i_17_n_0 ;
  wire \counter[6]_i_7_n_0 ;
  wire \counter[6]_i_8_n_0 ;
  wire \counter_reg[0] ;
  wire [0:0]\counter_reg[1] ;
  wire [0:0]\counter_reg[1]_0 ;
  wire [0:0]\counter_reg[1]_1 ;
  wire [0:0]\counter_reg[1]_2 ;
  wire [0:0]\counter_reg[1]_3 ;
  wire [0:0]\counter_reg[1]_4 ;
  wire [0:0]\counter_reg[1]_5 ;
  wire [7:0]\counter_reg[1]_6 ;
  wire \counter_reg[1]_7 ;
  wire \counter_reg[2] ;
  wire \counter_reg[3] ;
  wire \counter_reg[4] ;
  wire [0:0]\counter_reg[4]_0 ;
  wire \counter_reg[4]_1 ;
  wire \counter_reg[5] ;
  wire \counter_reg[6] ;
  wire \counter_reg[6]_0 ;
  wire \counter_reg[6]_i_4_n_3 ;
  wire \counter_reg[6]_i_6_n_0 ;
  wire \counter_reg[6]_i_6_n_1 ;
  wire \counter_reg[6]_i_6_n_2 ;
  wire \counter_reg[6]_i_6_n_3 ;
  wire [0:0]\counter_reg[8] ;
  wire [0:0]\counter_reg[8]_0 ;
  wire [0:0]\counter_reg[8]_1 ;
  wire [0:0]\counter_reg[8]_2 ;
  wire [0:0]\counter_reg[8]_3 ;
  wire [0:0]\counter_reg[8]_4 ;
  wire [0:0]\counter_reg[8]_5 ;
  wire [13:12]cpu2emsc_register;
  wire [31:0]data_in;
  wire dp_extend_end;
  wire in_stream_ready_reg;
  wire initialized_reg;
  wire [3:0]p_0_in;
  wire [31:7]p_1_in;
  wire \read_address[6]_i_12_n_0 ;
  wire \read_address[6]_i_13_n_0 ;
  wire \read_address[6]_i_14_n_0 ;
  wire \read_address[6]_i_16_n_0 ;
  wire \read_address[6]_i_17_n_0 ;
  wire \read_address[6]_i_18_n_0 ;
  wire \read_address[6]_i_20_n_0 ;
  wire \read_address[6]_i_21_n_0 ;
  wire \read_address[6]_i_22_n_0 ;
  wire \read_address[6]_i_24_n_0 ;
  wire \read_address[6]_i_25_n_0 ;
  wire \read_address[6]_i_26_n_0 ;
  wire \read_address[6]_i_27_n_0 ;
  wire \read_address[6]_i_28_n_0 ;
  wire \read_address[6]_i_29_n_0 ;
  wire \read_address[6]_i_30_n_0 ;
  wire \read_address[6]_i_31_n_0 ;
  wire [2:0]\read_address_reg[2] ;
  wire [0:0]\read_address_reg[2]_0 ;
  wire [2:0]\read_address_reg[2]_1 ;
  wire [5:0]\read_address_reg[5] ;
  wire [0:0]\read_address_reg[6] ;
  wire \read_address_reg[6]_i_19_n_0 ;
  wire \read_address_reg[6]_i_19_n_1 ;
  wire \read_address_reg[6]_i_19_n_2 ;
  wire \read_address_reg[6]_i_19_n_3 ;
  wire \read_address_reg[6]_i_23_n_0 ;
  wire \read_address_reg[6]_i_23_n_1 ;
  wire \read_address_reg[6]_i_23_n_2 ;
  wire \read_address_reg[6]_i_23_n_3 ;
  wire \read_address_reg[6]_i_4_n_2 ;
  wire \read_address_reg[6]_i_4_n_3 ;
  wire \read_address_reg[6]_i_6_n_0 ;
  wire \read_address_reg[6]_i_6_n_1 ;
  wire \read_address_reg[6]_i_6_n_2 ;
  wire \read_address_reg[6]_i_6_n_3 ;
  wire \read_address_reg[6]_i_8_n_0 ;
  wire \read_address_reg[6]_i_8_n_2 ;
  wire \read_address_reg[6]_i_8_n_3 ;
  wire [31:0]\read_data[0]_14 ;
  wire read_enable;
  wire [3:0]s_axi_ctrl_status_araddr;
  wire s_axi_ctrl_status_arready;
  wire s_axi_ctrl_status_arvalid;
  wire [3:0]s_axi_ctrl_status_awaddr;
  wire s_axi_ctrl_status_awready;
  wire s_axi_ctrl_status_awvalid;
  wire s_axi_ctrl_status_bvalid;
  wire [31:0]s_axi_ctrl_status_rdata;
  wire s_axi_ctrl_status_rvalid;
  wire [31:0]s_axi_ctrl_status_wdata;
  wire s_axi_ctrl_status_wready;
  wire [3:0]s_axi_ctrl_status_wstrb;
  wire s_axi_ctrl_status_wvalid;
  wire s_axis_tready;
  wire s_axis_tvalid;
  wire [3:0]sel0;
  wire slv_reg_wren__2;
  wire \slv_regs[10][15]_i_1_n_0 ;
  wire \slv_regs[10][23]_i_1_n_0 ;
  wire \slv_regs[10][31]_i_1_n_0 ;
  wire \slv_regs[10][7]_i_1_n_0 ;
  wire \slv_regs[11][15]_i_1_n_0 ;
  wire \slv_regs[11][23]_i_1_n_0 ;
  wire \slv_regs[11][31]_i_1_n_0 ;
  wire \slv_regs[11][7]_i_1_n_0 ;
  wire \slv_regs[12][15]_i_1_n_0 ;
  wire \slv_regs[12][23]_i_1_n_0 ;
  wire \slv_regs[12][31]_i_1_n_0 ;
  wire \slv_regs[12][7]_i_1_n_0 ;
  wire \slv_regs[13][15]_i_1_n_0 ;
  wire \slv_regs[13][23]_i_1_n_0 ;
  wire \slv_regs[13][31]_i_1_n_0 ;
  wire \slv_regs[13][7]_i_1_n_0 ;
  wire \slv_regs[14][15]_i_1_n_0 ;
  wire \slv_regs[14][23]_i_1_n_0 ;
  wire \slv_regs[14][31]_i_1_n_0 ;
  wire \slv_regs[14][7]_i_1_n_0 ;
  wire \slv_regs[15][15]_i_1_n_0 ;
  wire \slv_regs[15][23]_i_1_n_0 ;
  wire \slv_regs[15][31]_i_1_n_0 ;
  wire \slv_regs[15][7]_i_1_n_0 ;
  wire \slv_regs[1][15]_i_1_n_0 ;
  wire \slv_regs[1][23]_i_1_n_0 ;
  wire \slv_regs[1][31]_i_1_n_0 ;
  wire \slv_regs[1][7]_i_1_n_0 ;
  wire \slv_regs[2][15]_i_1_n_0 ;
  wire \slv_regs[2][23]_i_1_n_0 ;
  wire \slv_regs[2][31]_i_1_n_0 ;
  wire \slv_regs[2][7]_i_1_n_0 ;
  wire \slv_regs[3][15]_i_1_n_0 ;
  wire \slv_regs[3][23]_i_1_n_0 ;
  wire \slv_regs[3][31]_i_1_n_0 ;
  wire \slv_regs[3][7]_i_1_n_0 ;
  wire \slv_regs[4][15]_i_1_n_0 ;
  wire \slv_regs[4][23]_i_1_n_0 ;
  wire \slv_regs[4][31]_i_1_n_0 ;
  wire \slv_regs[4][7]_i_1_n_0 ;
  wire \slv_regs[5][15]_i_1_n_0 ;
  wire \slv_regs[5][23]_i_1_n_0 ;
  wire \slv_regs[5][31]_i_1_n_0 ;
  wire \slv_regs[5][7]_i_1_n_0 ;
  wire \slv_regs[6][15]_i_1_n_0 ;
  wire \slv_regs[6][23]_i_1_n_0 ;
  wire \slv_regs[6][31]_i_1_n_0 ;
  wire \slv_regs[6][7]_i_1_n_0 ;
  wire \slv_regs[7][15]_i_1_n_0 ;
  wire \slv_regs[7][23]_i_1_n_0 ;
  wire \slv_regs[7][31]_i_1_n_0 ;
  wire \slv_regs[7][7]_i_1_n_0 ;
  wire \slv_regs[8][15]_i_1_n_0 ;
  wire \slv_regs[8][23]_i_1_n_0 ;
  wire \slv_regs[8][31]_i_1_n_0 ;
  wire \slv_regs[8][7]_i_1_n_0 ;
  wire \slv_regs[9][15]_i_1_n_0 ;
  wire \slv_regs[9][23]_i_1_n_0 ;
  wire \slv_regs[9][31]_i_1_n_0 ;
  wire \slv_regs[9][7]_i_1_n_0 ;
  wire [0:0]\slv_regs_reg[0][11]_0 ;
  wire [0:0]\slv_regs_reg[0][11]_1 ;
  wire [0:0]\slv_regs_reg[0][11]_2 ;
  wire [0:0]\slv_regs_reg[0][11]_3 ;
  wire [0:0]\slv_regs_reg[0][11]_4 ;
  wire [0:0]\slv_regs_reg[0][11]_5 ;
  wire [0:0]\slv_regs_reg[0][11]_6 ;
  wire [1:0]\slv_regs_reg[0][11]_7 ;
  wire [31:0]\slv_regs_reg[10]_8 ;
  wire [31:0]\slv_regs_reg[11]_9 ;
  wire [31:0]\slv_regs_reg[12]_10 ;
  wire [31:0]\slv_regs_reg[13]_11 ;
  wire [31:0]\slv_regs_reg[14]_12 ;
  wire [31:0]\slv_regs_reg[15]_13 ;
  wire [31:0]\slv_regs_reg[2]_0 ;
  wire [31:0]\slv_regs_reg[3]_1 ;
  wire [31:0]\slv_regs_reg[4]_2 ;
  wire [31:0]\slv_regs_reg[5]_3 ;
  wire [31:0]\slv_regs_reg[6]_4 ;
  wire [31:0]\slv_regs_reg[7]_5 ;
  wire [31:0]\slv_regs_reg[8]_6 ;
  wire [31:0]\slv_regs_reg[9]_7 ;
  wire \slv_regs_reg_n_0_[0][14] ;
  wire \slv_regs_reg_n_0_[0][15] ;
  wire \slv_regs_reg_n_0_[0][16] ;
  wire \slv_regs_reg_n_0_[0][17] ;
  wire \slv_regs_reg_n_0_[0][18] ;
  wire \slv_regs_reg_n_0_[0][19] ;
  wire \slv_regs_reg_n_0_[0][20] ;
  wire \slv_regs_reg_n_0_[0][21] ;
  wire \slv_regs_reg_n_0_[0][22] ;
  wire \slv_regs_reg_n_0_[0][23] ;
  wire \slv_regs_reg_n_0_[0][24] ;
  wire \slv_regs_reg_n_0_[0][25] ;
  wire \slv_regs_reg_n_0_[0][26] ;
  wire \slv_regs_reg_n_0_[0][27] ;
  wire \slv_regs_reg_n_0_[0][28] ;
  wire \slv_regs_reg_n_0_[0][29] ;
  wire \slv_regs_reg_n_0_[0][30] ;
  wire \slv_regs_reg_n_0_[0][31] ;
  wire [12:1]state1;
  wire \state[1]_i_2_n_0 ;
  wire \state[1]_i_3_n_0 ;
  wire \state[1]_i_4_n_0 ;
  wire \state_reg[0] ;
  wire \state_reg[0]_0 ;
  wire \state_reg[1] ;
  wire \state_reg[1]_0 ;
  wire [11:1]v_len;
  wire valid_input1_out;
  wire [3:3]NLW_add_r1_carry__0_i_1_CO_UNCONNECTED;
  wire [3:2]\NLW_counter_reg[6]_i_4_CO_UNCONNECTED ;
  wire [3:0]\NLW_counter_reg[6]_i_4_O_UNCONNECTED ;
  wire [3:0]\NLW_counter_reg[6]_i_6_O_UNCONNECTED ;
  wire [3:3]\NLW_read_address_reg[6]_i_4_CO_UNCONNECTED ;
  wire [3:0]\NLW_read_address_reg[6]_i_4_O_UNCONNECTED ;
  wire [3:0]\NLW_read_address_reg[6]_i_6_O_UNCONNECTED ;
  wire [2:2]\NLW_read_address_reg[6]_i_8_CO_UNCONNECTED ;
  wire [3:3]\NLW_read_address_reg[6]_i_8_O_UNCONNECTED ;

  CARRY4 add_r1_carry__0_i_1
       (.CI(add_r1_carry_i_5_n_0),
        .CO({NLW_add_r1_carry__0_i_1_CO_UNCONNECTED[3],add_r2[12],add_r1_carry__0_i_1_n_2,add_r1_carry__0_i_1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({O,add_r2[11:9]}),
        .S({1'b1,v_len[11:9]}));
  LUT3 #(
    .INIT(8'h01)) 
    add_r1_carry_i_1
       (.I0(add_r2[11]),
        .I1(add_r2[10]),
        .I2(add_r2[9]),
        .O(S));
  LUT3 #(
    .INIT(8'h01)) 
    add_r1_carry_i_1__0
       (.I0(add_r2[11]),
        .I1(add_r2[10]),
        .I2(add_r2[9]),
        .O(\counter_reg[1] ));
  LUT3 #(
    .INIT(8'h01)) 
    add_r1_carry_i_1__1
       (.I0(add_r2[11]),
        .I1(add_r2[10]),
        .I2(add_r2[9]),
        .O(\counter_reg[1]_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    add_r1_carry_i_1__2
       (.I0(add_r2[11]),
        .I1(add_r2[10]),
        .I2(add_r2[9]),
        .O(\counter_reg[1]_1 ));
  LUT3 #(
    .INIT(8'h01)) 
    add_r1_carry_i_1__3
       (.I0(add_r2[11]),
        .I1(add_r2[10]),
        .I2(add_r2[9]),
        .O(\counter_reg[1]_2 ));
  LUT3 #(
    .INIT(8'h01)) 
    add_r1_carry_i_1__4
       (.I0(add_r2[11]),
        .I1(add_r2[10]),
        .I2(add_r2[9]),
        .O(\counter_reg[1]_3 ));
  LUT3 #(
    .INIT(8'h01)) 
    add_r1_carry_i_1__5
       (.I0(add_r2[11]),
        .I1(add_r2[10]),
        .I2(add_r2[9]),
        .O(\counter_reg[1]_4 ));
  LUT3 #(
    .INIT(8'h01)) 
    add_r1_carry_i_1__6
       (.I0(add_r2[11]),
        .I1(add_r2[10]),
        .I2(add_r2[9]),
        .O(\counter_reg[1]_5 ));
  CARRY4 add_r1_carry_i_5
       (.CI(add_r1_carry_i_6_n_0),
        .CO({add_r1_carry_i_5_n_0,add_r1_carry_i_5_n_1,add_r1_carry_i_5_n_2,add_r1_carry_i_5_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\counter_reg[1]_6 [7:4]),
        .S(v_len[8:5]));
  CARRY4 add_r1_carry_i_6
       (.CI(1'b0),
        .CO({add_r1_carry_i_6_n_0,add_r1_carry_i_6_n_1,add_r1_carry_i_6_n_2,add_r1_carry_i_6_n_3}),
        .CYINIT(\axi_rdata_reg[0]_1 ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\counter_reg[1]_6 [3:0]),
        .S(v_len[4:1]));
  FDSE \axi_araddr_reg[2] 
       (.C(clk),
        .CE(axi_arready_i_1_n_0),
        .D(s_axi_ctrl_status_araddr[0]),
        .Q(sel0[0]),
        .S(\axi_rdata_reg[0]_0 ));
  FDSE \axi_araddr_reg[3] 
       (.C(clk),
        .CE(axi_arready_i_1_n_0),
        .D(s_axi_ctrl_status_araddr[1]),
        .Q(sel0[1]),
        .S(\axi_rdata_reg[0]_0 ));
  FDSE \axi_araddr_reg[4] 
       (.C(clk),
        .CE(axi_arready_i_1_n_0),
        .D(s_axi_ctrl_status_araddr[2]),
        .Q(sel0[2]),
        .S(\axi_rdata_reg[0]_0 ));
  FDSE \axi_araddr_reg[5] 
       (.C(clk),
        .CE(axi_arready_i_1_n_0),
        .D(s_axi_ctrl_status_araddr[3]),
        .Q(sel0[3]),
        .S(\axi_rdata_reg[0]_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s_axi_ctrl_status_arvalid),
        .I1(s_axi_ctrl_status_arready),
        .O(axi_arready_i_1_n_0));
  FDRE axi_arready_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_arready_i_1_n_0),
        .Q(s_axi_ctrl_status_arready),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(clk),
        .CE(axi_awready_i_1_n_0),
        .D(s_axi_ctrl_status_awaddr[0]),
        .Q(p_0_in[0]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \axi_awaddr_reg[3] 
       (.C(clk),
        .CE(axi_awready_i_1_n_0),
        .D(s_axi_ctrl_status_awaddr[1]),
        .Q(p_0_in[1]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \axi_awaddr_reg[4] 
       (.C(clk),
        .CE(axi_awready_i_1_n_0),
        .D(s_axi_ctrl_status_awaddr[2]),
        .Q(p_0_in[2]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \axi_awaddr_reg[5] 
       (.C(clk),
        .CE(axi_awready_i_1_n_0),
        .D(s_axi_ctrl_status_awaddr[3]),
        .Q(p_0_in[3]),
        .R(\axi_rdata_reg[0]_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    axi_awready_i_1
       (.I0(s_axi_ctrl_status_wvalid),
        .I1(s_axi_ctrl_status_awvalid),
        .I2(s_axi_ctrl_status_awready),
        .O(axi_awready_i_1_n_0));
  FDRE axi_awready_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_awready_i_1_n_0),
        .Q(s_axi_ctrl_status_awready),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE axi_bvalid_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_wready_reg_0),
        .Q(s_axi_ctrl_status_bvalid),
        .R(\axi_rdata_reg[0]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_4 
       (.I0(\slv_regs_reg[3]_1 [0]),
        .I1(\slv_regs_reg[2]_0 [0]),
        .I2(sel0[1]),
        .I3(initialized_reg),
        .I4(sel0[0]),
        .I5(\axi_rdata_reg[0]_1 ),
        .O(\axi_rdata[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_5 
       (.I0(\slv_regs_reg[7]_5 [0]),
        .I1(\slv_regs_reg[6]_4 [0]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [0]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [0]),
        .O(\axi_rdata[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_6 
       (.I0(\slv_regs_reg[11]_9 [0]),
        .I1(\slv_regs_reg[10]_8 [0]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [0]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [0]),
        .O(\axi_rdata[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_7 
       (.I0(\slv_regs_reg[15]_13 [0]),
        .I1(\slv_regs_reg[14]_12 [0]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [0]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [0]),
        .O(\axi_rdata[0]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[10]_i_4 
       (.I0(\slv_regs_reg[3]_1 [10]),
        .I1(\slv_regs_reg[2]_0 [10]),
        .I2(sel0[1]),
        .I3(v_len[10]),
        .I4(sel0[0]),
        .O(\axi_rdata[10]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_5 
       (.I0(\slv_regs_reg[7]_5 [10]),
        .I1(\slv_regs_reg[6]_4 [10]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [10]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [10]),
        .O(\axi_rdata[10]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_6 
       (.I0(\slv_regs_reg[11]_9 [10]),
        .I1(\slv_regs_reg[10]_8 [10]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [10]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [10]),
        .O(\axi_rdata[10]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_7 
       (.I0(\slv_regs_reg[15]_13 [10]),
        .I1(\slv_regs_reg[14]_12 [10]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [10]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [10]),
        .O(\axi_rdata[10]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[11]_i_4 
       (.I0(\slv_regs_reg[3]_1 [11]),
        .I1(\slv_regs_reg[2]_0 [11]),
        .I2(sel0[1]),
        .I3(v_len[11]),
        .I4(sel0[0]),
        .O(\axi_rdata[11]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_5 
       (.I0(\slv_regs_reg[7]_5 [11]),
        .I1(\slv_regs_reg[6]_4 [11]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [11]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [11]),
        .O(\axi_rdata[11]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_6 
       (.I0(\slv_regs_reg[11]_9 [11]),
        .I1(\slv_regs_reg[10]_8 [11]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [11]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [11]),
        .O(\axi_rdata[11]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_7 
       (.I0(\slv_regs_reg[15]_13 [11]),
        .I1(\slv_regs_reg[14]_12 [11]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [11]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [11]),
        .O(\axi_rdata[11]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[12]_i_4 
       (.I0(\slv_regs_reg[3]_1 [12]),
        .I1(\slv_regs_reg[2]_0 [12]),
        .I2(sel0[1]),
        .I3(cpu2emsc_register[12]),
        .I4(sel0[0]),
        .O(\axi_rdata[12]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_5 
       (.I0(\slv_regs_reg[7]_5 [12]),
        .I1(\slv_regs_reg[6]_4 [12]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [12]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [12]),
        .O(\axi_rdata[12]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_6 
       (.I0(\slv_regs_reg[11]_9 [12]),
        .I1(\slv_regs_reg[10]_8 [12]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [12]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [12]),
        .O(\axi_rdata[12]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_7 
       (.I0(\slv_regs_reg[15]_13 [12]),
        .I1(\slv_regs_reg[14]_12 [12]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [12]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [12]),
        .O(\axi_rdata[12]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[13]_i_4 
       (.I0(\slv_regs_reg[3]_1 [13]),
        .I1(\slv_regs_reg[2]_0 [13]),
        .I2(sel0[1]),
        .I3(cpu2emsc_register[13]),
        .I4(sel0[0]),
        .O(\axi_rdata[13]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_5 
       (.I0(\slv_regs_reg[7]_5 [13]),
        .I1(\slv_regs_reg[6]_4 [13]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [13]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [13]),
        .O(\axi_rdata[13]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_6 
       (.I0(\slv_regs_reg[11]_9 [13]),
        .I1(\slv_regs_reg[10]_8 [13]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [13]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [13]),
        .O(\axi_rdata[13]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_7 
       (.I0(\slv_regs_reg[15]_13 [13]),
        .I1(\slv_regs_reg[14]_12 [13]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [13]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [13]),
        .O(\axi_rdata[13]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[14]_i_4 
       (.I0(\slv_regs_reg[3]_1 [14]),
        .I1(\slv_regs_reg[2]_0 [14]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][14] ),
        .I4(sel0[0]),
        .O(\axi_rdata[14]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_5 
       (.I0(\slv_regs_reg[7]_5 [14]),
        .I1(\slv_regs_reg[6]_4 [14]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [14]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [14]),
        .O(\axi_rdata[14]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_6 
       (.I0(\slv_regs_reg[11]_9 [14]),
        .I1(\slv_regs_reg[10]_8 [14]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [14]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [14]),
        .O(\axi_rdata[14]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_7 
       (.I0(\slv_regs_reg[15]_13 [14]),
        .I1(\slv_regs_reg[14]_12 [14]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [14]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [14]),
        .O(\axi_rdata[14]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[15]_i_4 
       (.I0(\slv_regs_reg[3]_1 [15]),
        .I1(\slv_regs_reg[2]_0 [15]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][15] ),
        .I4(sel0[0]),
        .O(\axi_rdata[15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_5 
       (.I0(\slv_regs_reg[7]_5 [15]),
        .I1(\slv_regs_reg[6]_4 [15]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [15]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [15]),
        .O(\axi_rdata[15]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_6 
       (.I0(\slv_regs_reg[11]_9 [15]),
        .I1(\slv_regs_reg[10]_8 [15]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [15]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [15]),
        .O(\axi_rdata[15]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_7 
       (.I0(\slv_regs_reg[15]_13 [15]),
        .I1(\slv_regs_reg[14]_12 [15]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [15]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [15]),
        .O(\axi_rdata[15]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[16]_i_4 
       (.I0(\slv_regs_reg[3]_1 [16]),
        .I1(\slv_regs_reg[2]_0 [16]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][16] ),
        .I4(sel0[0]),
        .O(\axi_rdata[16]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_5 
       (.I0(\slv_regs_reg[7]_5 [16]),
        .I1(\slv_regs_reg[6]_4 [16]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [16]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [16]),
        .O(\axi_rdata[16]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_6 
       (.I0(\slv_regs_reg[11]_9 [16]),
        .I1(\slv_regs_reg[10]_8 [16]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [16]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [16]),
        .O(\axi_rdata[16]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_7 
       (.I0(\slv_regs_reg[15]_13 [16]),
        .I1(\slv_regs_reg[14]_12 [16]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [16]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [16]),
        .O(\axi_rdata[16]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[17]_i_4 
       (.I0(\slv_regs_reg[3]_1 [17]),
        .I1(\slv_regs_reg[2]_0 [17]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][17] ),
        .I4(sel0[0]),
        .O(\axi_rdata[17]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_5 
       (.I0(\slv_regs_reg[7]_5 [17]),
        .I1(\slv_regs_reg[6]_4 [17]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [17]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [17]),
        .O(\axi_rdata[17]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_6 
       (.I0(\slv_regs_reg[11]_9 [17]),
        .I1(\slv_regs_reg[10]_8 [17]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [17]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [17]),
        .O(\axi_rdata[17]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_7 
       (.I0(\slv_regs_reg[15]_13 [17]),
        .I1(\slv_regs_reg[14]_12 [17]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [17]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [17]),
        .O(\axi_rdata[17]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[18]_i_4 
       (.I0(\slv_regs_reg[3]_1 [18]),
        .I1(\slv_regs_reg[2]_0 [18]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][18] ),
        .I4(sel0[0]),
        .O(\axi_rdata[18]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_5 
       (.I0(\slv_regs_reg[7]_5 [18]),
        .I1(\slv_regs_reg[6]_4 [18]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [18]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [18]),
        .O(\axi_rdata[18]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_6 
       (.I0(\slv_regs_reg[11]_9 [18]),
        .I1(\slv_regs_reg[10]_8 [18]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [18]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [18]),
        .O(\axi_rdata[18]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_7 
       (.I0(\slv_regs_reg[15]_13 [18]),
        .I1(\slv_regs_reg[14]_12 [18]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [18]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [18]),
        .O(\axi_rdata[18]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[19]_i_4 
       (.I0(\slv_regs_reg[3]_1 [19]),
        .I1(\slv_regs_reg[2]_0 [19]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][19] ),
        .I4(sel0[0]),
        .O(\axi_rdata[19]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_5 
       (.I0(\slv_regs_reg[7]_5 [19]),
        .I1(\slv_regs_reg[6]_4 [19]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [19]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [19]),
        .O(\axi_rdata[19]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_6 
       (.I0(\slv_regs_reg[11]_9 [19]),
        .I1(\slv_regs_reg[10]_8 [19]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [19]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [19]),
        .O(\axi_rdata[19]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_7 
       (.I0(\slv_regs_reg[15]_13 [19]),
        .I1(\slv_regs_reg[14]_12 [19]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [19]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [19]),
        .O(\axi_rdata[19]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[1]_i_4 
       (.I0(\slv_regs_reg[3]_1 [1]),
        .I1(\slv_regs_reg[2]_0 [1]),
        .I2(sel0[1]),
        .I3(v_len[1]),
        .I4(sel0[0]),
        .O(\axi_rdata[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_5 
       (.I0(\slv_regs_reg[7]_5 [1]),
        .I1(\slv_regs_reg[6]_4 [1]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [1]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [1]),
        .O(\axi_rdata[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_6 
       (.I0(\slv_regs_reg[11]_9 [1]),
        .I1(\slv_regs_reg[10]_8 [1]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [1]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [1]),
        .O(\axi_rdata[1]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_7 
       (.I0(\slv_regs_reg[15]_13 [1]),
        .I1(\slv_regs_reg[14]_12 [1]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [1]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [1]),
        .O(\axi_rdata[1]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[20]_i_4 
       (.I0(\slv_regs_reg[3]_1 [20]),
        .I1(\slv_regs_reg[2]_0 [20]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][20] ),
        .I4(sel0[0]),
        .O(\axi_rdata[20]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_5 
       (.I0(\slv_regs_reg[7]_5 [20]),
        .I1(\slv_regs_reg[6]_4 [20]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [20]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [20]),
        .O(\axi_rdata[20]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_6 
       (.I0(\slv_regs_reg[11]_9 [20]),
        .I1(\slv_regs_reg[10]_8 [20]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [20]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [20]),
        .O(\axi_rdata[20]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_7 
       (.I0(\slv_regs_reg[15]_13 [20]),
        .I1(\slv_regs_reg[14]_12 [20]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [20]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [20]),
        .O(\axi_rdata[20]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[21]_i_4 
       (.I0(\slv_regs_reg[3]_1 [21]),
        .I1(\slv_regs_reg[2]_0 [21]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][21] ),
        .I4(sel0[0]),
        .O(\axi_rdata[21]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_5 
       (.I0(\slv_regs_reg[7]_5 [21]),
        .I1(\slv_regs_reg[6]_4 [21]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [21]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [21]),
        .O(\axi_rdata[21]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_6 
       (.I0(\slv_regs_reg[11]_9 [21]),
        .I1(\slv_regs_reg[10]_8 [21]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [21]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [21]),
        .O(\axi_rdata[21]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_7 
       (.I0(\slv_regs_reg[15]_13 [21]),
        .I1(\slv_regs_reg[14]_12 [21]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [21]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [21]),
        .O(\axi_rdata[21]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[22]_i_4 
       (.I0(\slv_regs_reg[3]_1 [22]),
        .I1(\slv_regs_reg[2]_0 [22]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][22] ),
        .I4(sel0[0]),
        .O(\axi_rdata[22]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_5 
       (.I0(\slv_regs_reg[7]_5 [22]),
        .I1(\slv_regs_reg[6]_4 [22]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [22]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [22]),
        .O(\axi_rdata[22]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_6 
       (.I0(\slv_regs_reg[11]_9 [22]),
        .I1(\slv_regs_reg[10]_8 [22]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [22]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [22]),
        .O(\axi_rdata[22]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_7 
       (.I0(\slv_regs_reg[15]_13 [22]),
        .I1(\slv_regs_reg[14]_12 [22]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [22]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [22]),
        .O(\axi_rdata[22]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[23]_i_4 
       (.I0(\slv_regs_reg[3]_1 [23]),
        .I1(\slv_regs_reg[2]_0 [23]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][23] ),
        .I4(sel0[0]),
        .O(\axi_rdata[23]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_5 
       (.I0(\slv_regs_reg[7]_5 [23]),
        .I1(\slv_regs_reg[6]_4 [23]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [23]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [23]),
        .O(\axi_rdata[23]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_6 
       (.I0(\slv_regs_reg[11]_9 [23]),
        .I1(\slv_regs_reg[10]_8 [23]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [23]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [23]),
        .O(\axi_rdata[23]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_7 
       (.I0(\slv_regs_reg[15]_13 [23]),
        .I1(\slv_regs_reg[14]_12 [23]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [23]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [23]),
        .O(\axi_rdata[23]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[24]_i_4 
       (.I0(\slv_regs_reg[3]_1 [24]),
        .I1(\slv_regs_reg[2]_0 [24]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][24] ),
        .I4(sel0[0]),
        .O(\axi_rdata[24]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_5 
       (.I0(\slv_regs_reg[7]_5 [24]),
        .I1(\slv_regs_reg[6]_4 [24]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [24]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [24]),
        .O(\axi_rdata[24]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_6 
       (.I0(\slv_regs_reg[11]_9 [24]),
        .I1(\slv_regs_reg[10]_8 [24]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [24]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [24]),
        .O(\axi_rdata[24]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_7 
       (.I0(\slv_regs_reg[15]_13 [24]),
        .I1(\slv_regs_reg[14]_12 [24]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [24]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [24]),
        .O(\axi_rdata[24]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[25]_i_4 
       (.I0(\slv_regs_reg[3]_1 [25]),
        .I1(\slv_regs_reg[2]_0 [25]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][25] ),
        .I4(sel0[0]),
        .O(\axi_rdata[25]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_5 
       (.I0(\slv_regs_reg[7]_5 [25]),
        .I1(\slv_regs_reg[6]_4 [25]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [25]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [25]),
        .O(\axi_rdata[25]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_6 
       (.I0(\slv_regs_reg[11]_9 [25]),
        .I1(\slv_regs_reg[10]_8 [25]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [25]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [25]),
        .O(\axi_rdata[25]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_7 
       (.I0(\slv_regs_reg[15]_13 [25]),
        .I1(\slv_regs_reg[14]_12 [25]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [25]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [25]),
        .O(\axi_rdata[25]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[26]_i_4 
       (.I0(\slv_regs_reg[3]_1 [26]),
        .I1(\slv_regs_reg[2]_0 [26]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][26] ),
        .I4(sel0[0]),
        .O(\axi_rdata[26]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_5 
       (.I0(\slv_regs_reg[7]_5 [26]),
        .I1(\slv_regs_reg[6]_4 [26]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [26]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [26]),
        .O(\axi_rdata[26]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_6 
       (.I0(\slv_regs_reg[11]_9 [26]),
        .I1(\slv_regs_reg[10]_8 [26]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [26]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [26]),
        .O(\axi_rdata[26]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_7 
       (.I0(\slv_regs_reg[15]_13 [26]),
        .I1(\slv_regs_reg[14]_12 [26]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [26]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [26]),
        .O(\axi_rdata[26]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[27]_i_4 
       (.I0(\slv_regs_reg[3]_1 [27]),
        .I1(\slv_regs_reg[2]_0 [27]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][27] ),
        .I4(sel0[0]),
        .O(\axi_rdata[27]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_5 
       (.I0(\slv_regs_reg[7]_5 [27]),
        .I1(\slv_regs_reg[6]_4 [27]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [27]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [27]),
        .O(\axi_rdata[27]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_6 
       (.I0(\slv_regs_reg[11]_9 [27]),
        .I1(\slv_regs_reg[10]_8 [27]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [27]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [27]),
        .O(\axi_rdata[27]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_7 
       (.I0(\slv_regs_reg[15]_13 [27]),
        .I1(\slv_regs_reg[14]_12 [27]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [27]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [27]),
        .O(\axi_rdata[27]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[28]_i_4 
       (.I0(\slv_regs_reg[3]_1 [28]),
        .I1(\slv_regs_reg[2]_0 [28]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][28] ),
        .I4(sel0[0]),
        .O(\axi_rdata[28]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_5 
       (.I0(\slv_regs_reg[7]_5 [28]),
        .I1(\slv_regs_reg[6]_4 [28]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [28]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [28]),
        .O(\axi_rdata[28]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_6 
       (.I0(\slv_regs_reg[11]_9 [28]),
        .I1(\slv_regs_reg[10]_8 [28]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [28]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [28]),
        .O(\axi_rdata[28]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_7 
       (.I0(\slv_regs_reg[15]_13 [28]),
        .I1(\slv_regs_reg[14]_12 [28]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [28]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [28]),
        .O(\axi_rdata[28]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[29]_i_4 
       (.I0(\slv_regs_reg[3]_1 [29]),
        .I1(\slv_regs_reg[2]_0 [29]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][29] ),
        .I4(sel0[0]),
        .O(\axi_rdata[29]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_5 
       (.I0(\slv_regs_reg[7]_5 [29]),
        .I1(\slv_regs_reg[6]_4 [29]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [29]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [29]),
        .O(\axi_rdata[29]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_6 
       (.I0(\slv_regs_reg[11]_9 [29]),
        .I1(\slv_regs_reg[10]_8 [29]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [29]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [29]),
        .O(\axi_rdata[29]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_7 
       (.I0(\slv_regs_reg[15]_13 [29]),
        .I1(\slv_regs_reg[14]_12 [29]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [29]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [29]),
        .O(\axi_rdata[29]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[2]_i_4 
       (.I0(\slv_regs_reg[3]_1 [2]),
        .I1(\slv_regs_reg[2]_0 [2]),
        .I2(sel0[1]),
        .I3(v_len[2]),
        .I4(sel0[0]),
        .O(\axi_rdata[2]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_5 
       (.I0(\slv_regs_reg[7]_5 [2]),
        .I1(\slv_regs_reg[6]_4 [2]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [2]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [2]),
        .O(\axi_rdata[2]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_6 
       (.I0(\slv_regs_reg[11]_9 [2]),
        .I1(\slv_regs_reg[10]_8 [2]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [2]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [2]),
        .O(\axi_rdata[2]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_7 
       (.I0(\slv_regs_reg[15]_13 [2]),
        .I1(\slv_regs_reg[14]_12 [2]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [2]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [2]),
        .O(\axi_rdata[2]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[30]_i_4 
       (.I0(\slv_regs_reg[3]_1 [30]),
        .I1(\slv_regs_reg[2]_0 [30]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][30] ),
        .I4(sel0[0]),
        .O(\axi_rdata[30]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_5 
       (.I0(\slv_regs_reg[7]_5 [30]),
        .I1(\slv_regs_reg[6]_4 [30]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [30]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [30]),
        .O(\axi_rdata[30]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_6 
       (.I0(\slv_regs_reg[11]_9 [30]),
        .I1(\slv_regs_reg[10]_8 [30]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [30]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [30]),
        .O(\axi_rdata[30]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_7 
       (.I0(\slv_regs_reg[15]_13 [30]),
        .I1(\slv_regs_reg[14]_12 [30]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [30]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [30]),
        .O(\axi_rdata[30]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(s_axi_ctrl_status_arready),
        .I1(s_axi_ctrl_status_arvalid),
        .I2(s_axi_ctrl_status_rvalid),
        .O(\axi_rdata[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[31]_i_5 
       (.I0(\slv_regs_reg[3]_1 [31]),
        .I1(\slv_regs_reg[2]_0 [31]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][31] ),
        .I4(sel0[0]),
        .O(\axi_rdata[31]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_6 
       (.I0(\slv_regs_reg[7]_5 [31]),
        .I1(\slv_regs_reg[6]_4 [31]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [31]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [31]),
        .O(\axi_rdata[31]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_7 
       (.I0(\slv_regs_reg[11]_9 [31]),
        .I1(\slv_regs_reg[10]_8 [31]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [31]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [31]),
        .O(\axi_rdata[31]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_8 
       (.I0(\slv_regs_reg[15]_13 [31]),
        .I1(\slv_regs_reg[14]_12 [31]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [31]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [31]),
        .O(\axi_rdata[31]_i_8_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[3]_i_4 
       (.I0(\slv_regs_reg[3]_1 [3]),
        .I1(\slv_regs_reg[2]_0 [3]),
        .I2(sel0[1]),
        .I3(v_len[3]),
        .I4(sel0[0]),
        .O(\axi_rdata[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_5 
       (.I0(\slv_regs_reg[7]_5 [3]),
        .I1(\slv_regs_reg[6]_4 [3]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [3]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [3]),
        .O(\axi_rdata[3]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_6 
       (.I0(\slv_regs_reg[11]_9 [3]),
        .I1(\slv_regs_reg[10]_8 [3]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [3]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [3]),
        .O(\axi_rdata[3]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_7 
       (.I0(\slv_regs_reg[15]_13 [3]),
        .I1(\slv_regs_reg[14]_12 [3]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [3]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [3]),
        .O(\axi_rdata[3]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[4]_i_4 
       (.I0(\slv_regs_reg[3]_1 [4]),
        .I1(\slv_regs_reg[2]_0 [4]),
        .I2(sel0[1]),
        .I3(v_len[4]),
        .I4(sel0[0]),
        .O(\axi_rdata[4]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_5 
       (.I0(\slv_regs_reg[7]_5 [4]),
        .I1(\slv_regs_reg[6]_4 [4]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [4]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [4]),
        .O(\axi_rdata[4]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_6 
       (.I0(\slv_regs_reg[11]_9 [4]),
        .I1(\slv_regs_reg[10]_8 [4]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [4]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [4]),
        .O(\axi_rdata[4]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_7 
       (.I0(\slv_regs_reg[15]_13 [4]),
        .I1(\slv_regs_reg[14]_12 [4]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [4]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [4]),
        .O(\axi_rdata[4]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[5]_i_4 
       (.I0(\slv_regs_reg[3]_1 [5]),
        .I1(\slv_regs_reg[2]_0 [5]),
        .I2(sel0[1]),
        .I3(v_len[5]),
        .I4(sel0[0]),
        .O(\axi_rdata[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_5 
       (.I0(\slv_regs_reg[7]_5 [5]),
        .I1(\slv_regs_reg[6]_4 [5]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [5]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [5]),
        .O(\axi_rdata[5]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_6 
       (.I0(\slv_regs_reg[11]_9 [5]),
        .I1(\slv_regs_reg[10]_8 [5]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [5]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [5]),
        .O(\axi_rdata[5]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_7 
       (.I0(\slv_regs_reg[15]_13 [5]),
        .I1(\slv_regs_reg[14]_12 [5]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [5]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [5]),
        .O(\axi_rdata[5]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[6]_i_4 
       (.I0(\slv_regs_reg[3]_1 [6]),
        .I1(\slv_regs_reg[2]_0 [6]),
        .I2(sel0[1]),
        .I3(v_len[6]),
        .I4(sel0[0]),
        .O(\axi_rdata[6]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_5 
       (.I0(\slv_regs_reg[7]_5 [6]),
        .I1(\slv_regs_reg[6]_4 [6]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [6]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [6]),
        .O(\axi_rdata[6]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_6 
       (.I0(\slv_regs_reg[11]_9 [6]),
        .I1(\slv_regs_reg[10]_8 [6]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [6]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [6]),
        .O(\axi_rdata[6]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_7 
       (.I0(\slv_regs_reg[15]_13 [6]),
        .I1(\slv_regs_reg[14]_12 [6]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [6]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [6]),
        .O(\axi_rdata[6]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[7]_i_4 
       (.I0(\slv_regs_reg[3]_1 [7]),
        .I1(\slv_regs_reg[2]_0 [7]),
        .I2(sel0[1]),
        .I3(v_len[7]),
        .I4(sel0[0]),
        .O(\axi_rdata[7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_5 
       (.I0(\slv_regs_reg[7]_5 [7]),
        .I1(\slv_regs_reg[6]_4 [7]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [7]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [7]),
        .O(\axi_rdata[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_6 
       (.I0(\slv_regs_reg[11]_9 [7]),
        .I1(\slv_regs_reg[10]_8 [7]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [7]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [7]),
        .O(\axi_rdata[7]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_7 
       (.I0(\slv_regs_reg[15]_13 [7]),
        .I1(\slv_regs_reg[14]_12 [7]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [7]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [7]),
        .O(\axi_rdata[7]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[8]_i_4 
       (.I0(\slv_regs_reg[3]_1 [8]),
        .I1(\slv_regs_reg[2]_0 [8]),
        .I2(sel0[1]),
        .I3(v_len[8]),
        .I4(sel0[0]),
        .O(\axi_rdata[8]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_5 
       (.I0(\slv_regs_reg[7]_5 [8]),
        .I1(\slv_regs_reg[6]_4 [8]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [8]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [8]),
        .O(\axi_rdata[8]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_6 
       (.I0(\slv_regs_reg[11]_9 [8]),
        .I1(\slv_regs_reg[10]_8 [8]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [8]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [8]),
        .O(\axi_rdata[8]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_7 
       (.I0(\slv_regs_reg[15]_13 [8]),
        .I1(\slv_regs_reg[14]_12 [8]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [8]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [8]),
        .O(\axi_rdata[8]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[9]_i_4 
       (.I0(\slv_regs_reg[3]_1 [9]),
        .I1(\slv_regs_reg[2]_0 [9]),
        .I2(sel0[1]),
        .I3(v_len[9]),
        .I4(sel0[0]),
        .O(\axi_rdata[9]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_5 
       (.I0(\slv_regs_reg[7]_5 [9]),
        .I1(\slv_regs_reg[6]_4 [9]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [9]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [9]),
        .O(\axi_rdata[9]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_6 
       (.I0(\slv_regs_reg[11]_9 [9]),
        .I1(\slv_regs_reg[10]_8 [9]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [9]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [9]),
        .O(\axi_rdata[9]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_7 
       (.I0(\slv_regs_reg[15]_13 [9]),
        .I1(\slv_regs_reg[14]_12 [9]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [9]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [9]),
        .O(\axi_rdata[9]_i_7_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [0]),
        .Q(s_axi_ctrl_status_rdata[0]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[0]_i_1 
       (.I0(\axi_rdata_reg[0]_i_2_n_0 ),
        .I1(\axi_rdata_reg[0]_i_3_n_0 ),
        .O(\read_data[0]_14 [0]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[0]_i_2 
       (.I0(\axi_rdata[0]_i_4_n_0 ),
        .I1(\axi_rdata[0]_i_5_n_0 ),
        .O(\axi_rdata_reg[0]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[0]_i_3 
       (.I0(\axi_rdata[0]_i_6_n_0 ),
        .I1(\axi_rdata[0]_i_7_n_0 ),
        .O(\axi_rdata_reg[0]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[10] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [10]),
        .Q(s_axi_ctrl_status_rdata[10]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[10]_i_1 
       (.I0(\axi_rdata_reg[10]_i_2_n_0 ),
        .I1(\axi_rdata_reg[10]_i_3_n_0 ),
        .O(\read_data[0]_14 [10]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[10]_i_2 
       (.I0(\axi_rdata[10]_i_4_n_0 ),
        .I1(\axi_rdata[10]_i_5_n_0 ),
        .O(\axi_rdata_reg[10]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[10]_i_3 
       (.I0(\axi_rdata[10]_i_6_n_0 ),
        .I1(\axi_rdata[10]_i_7_n_0 ),
        .O(\axi_rdata_reg[10]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[11] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [11]),
        .Q(s_axi_ctrl_status_rdata[11]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[11]_i_1 
       (.I0(\axi_rdata_reg[11]_i_2_n_0 ),
        .I1(\axi_rdata_reg[11]_i_3_n_0 ),
        .O(\read_data[0]_14 [11]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[11]_i_2 
       (.I0(\axi_rdata[11]_i_4_n_0 ),
        .I1(\axi_rdata[11]_i_5_n_0 ),
        .O(\axi_rdata_reg[11]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[11]_i_3 
       (.I0(\axi_rdata[11]_i_6_n_0 ),
        .I1(\axi_rdata[11]_i_7_n_0 ),
        .O(\axi_rdata_reg[11]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[12] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [12]),
        .Q(s_axi_ctrl_status_rdata[12]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[12]_i_1 
       (.I0(\axi_rdata_reg[12]_i_2_n_0 ),
        .I1(\axi_rdata_reg[12]_i_3_n_0 ),
        .O(\read_data[0]_14 [12]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[12]_i_2 
       (.I0(\axi_rdata[12]_i_4_n_0 ),
        .I1(\axi_rdata[12]_i_5_n_0 ),
        .O(\axi_rdata_reg[12]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[12]_i_3 
       (.I0(\axi_rdata[12]_i_6_n_0 ),
        .I1(\axi_rdata[12]_i_7_n_0 ),
        .O(\axi_rdata_reg[12]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[13] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [13]),
        .Q(s_axi_ctrl_status_rdata[13]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[13]_i_1 
       (.I0(\axi_rdata_reg[13]_i_2_n_0 ),
        .I1(\axi_rdata_reg[13]_i_3_n_0 ),
        .O(\read_data[0]_14 [13]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[13]_i_2 
       (.I0(\axi_rdata[13]_i_4_n_0 ),
        .I1(\axi_rdata[13]_i_5_n_0 ),
        .O(\axi_rdata_reg[13]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[13]_i_3 
       (.I0(\axi_rdata[13]_i_6_n_0 ),
        .I1(\axi_rdata[13]_i_7_n_0 ),
        .O(\axi_rdata_reg[13]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[14] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [14]),
        .Q(s_axi_ctrl_status_rdata[14]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[14]_i_1 
       (.I0(\axi_rdata_reg[14]_i_2_n_0 ),
        .I1(\axi_rdata_reg[14]_i_3_n_0 ),
        .O(\read_data[0]_14 [14]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[14]_i_2 
       (.I0(\axi_rdata[14]_i_4_n_0 ),
        .I1(\axi_rdata[14]_i_5_n_0 ),
        .O(\axi_rdata_reg[14]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[14]_i_3 
       (.I0(\axi_rdata[14]_i_6_n_0 ),
        .I1(\axi_rdata[14]_i_7_n_0 ),
        .O(\axi_rdata_reg[14]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[15] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [15]),
        .Q(s_axi_ctrl_status_rdata[15]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[15]_i_1 
       (.I0(\axi_rdata_reg[15]_i_2_n_0 ),
        .I1(\axi_rdata_reg[15]_i_3_n_0 ),
        .O(\read_data[0]_14 [15]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[15]_i_2 
       (.I0(\axi_rdata[15]_i_4_n_0 ),
        .I1(\axi_rdata[15]_i_5_n_0 ),
        .O(\axi_rdata_reg[15]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[15]_i_3 
       (.I0(\axi_rdata[15]_i_6_n_0 ),
        .I1(\axi_rdata[15]_i_7_n_0 ),
        .O(\axi_rdata_reg[15]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[16] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [16]),
        .Q(s_axi_ctrl_status_rdata[16]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[16]_i_1 
       (.I0(\axi_rdata_reg[16]_i_2_n_0 ),
        .I1(\axi_rdata_reg[16]_i_3_n_0 ),
        .O(\read_data[0]_14 [16]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[16]_i_2 
       (.I0(\axi_rdata[16]_i_4_n_0 ),
        .I1(\axi_rdata[16]_i_5_n_0 ),
        .O(\axi_rdata_reg[16]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[16]_i_3 
       (.I0(\axi_rdata[16]_i_6_n_0 ),
        .I1(\axi_rdata[16]_i_7_n_0 ),
        .O(\axi_rdata_reg[16]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[17] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [17]),
        .Q(s_axi_ctrl_status_rdata[17]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[17]_i_1 
       (.I0(\axi_rdata_reg[17]_i_2_n_0 ),
        .I1(\axi_rdata_reg[17]_i_3_n_0 ),
        .O(\read_data[0]_14 [17]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[17]_i_2 
       (.I0(\axi_rdata[17]_i_4_n_0 ),
        .I1(\axi_rdata[17]_i_5_n_0 ),
        .O(\axi_rdata_reg[17]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[17]_i_3 
       (.I0(\axi_rdata[17]_i_6_n_0 ),
        .I1(\axi_rdata[17]_i_7_n_0 ),
        .O(\axi_rdata_reg[17]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[18] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [18]),
        .Q(s_axi_ctrl_status_rdata[18]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[18]_i_1 
       (.I0(\axi_rdata_reg[18]_i_2_n_0 ),
        .I1(\axi_rdata_reg[18]_i_3_n_0 ),
        .O(\read_data[0]_14 [18]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[18]_i_2 
       (.I0(\axi_rdata[18]_i_4_n_0 ),
        .I1(\axi_rdata[18]_i_5_n_0 ),
        .O(\axi_rdata_reg[18]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[18]_i_3 
       (.I0(\axi_rdata[18]_i_6_n_0 ),
        .I1(\axi_rdata[18]_i_7_n_0 ),
        .O(\axi_rdata_reg[18]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[19] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [19]),
        .Q(s_axi_ctrl_status_rdata[19]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[19]_i_1 
       (.I0(\axi_rdata_reg[19]_i_2_n_0 ),
        .I1(\axi_rdata_reg[19]_i_3_n_0 ),
        .O(\read_data[0]_14 [19]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[19]_i_2 
       (.I0(\axi_rdata[19]_i_4_n_0 ),
        .I1(\axi_rdata[19]_i_5_n_0 ),
        .O(\axi_rdata_reg[19]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[19]_i_3 
       (.I0(\axi_rdata[19]_i_6_n_0 ),
        .I1(\axi_rdata[19]_i_7_n_0 ),
        .O(\axi_rdata_reg[19]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[1] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [1]),
        .Q(s_axi_ctrl_status_rdata[1]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[1]_i_1 
       (.I0(\axi_rdata_reg[1]_i_2_n_0 ),
        .I1(\axi_rdata_reg[1]_i_3_n_0 ),
        .O(\read_data[0]_14 [1]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[1]_i_2 
       (.I0(\axi_rdata[1]_i_4_n_0 ),
        .I1(\axi_rdata[1]_i_5_n_0 ),
        .O(\axi_rdata_reg[1]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[1]_i_3 
       (.I0(\axi_rdata[1]_i_6_n_0 ),
        .I1(\axi_rdata[1]_i_7_n_0 ),
        .O(\axi_rdata_reg[1]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[20] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [20]),
        .Q(s_axi_ctrl_status_rdata[20]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[20]_i_1 
       (.I0(\axi_rdata_reg[20]_i_2_n_0 ),
        .I1(\axi_rdata_reg[20]_i_3_n_0 ),
        .O(\read_data[0]_14 [20]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[20]_i_2 
       (.I0(\axi_rdata[20]_i_4_n_0 ),
        .I1(\axi_rdata[20]_i_5_n_0 ),
        .O(\axi_rdata_reg[20]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[20]_i_3 
       (.I0(\axi_rdata[20]_i_6_n_0 ),
        .I1(\axi_rdata[20]_i_7_n_0 ),
        .O(\axi_rdata_reg[20]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[21] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [21]),
        .Q(s_axi_ctrl_status_rdata[21]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[21]_i_1 
       (.I0(\axi_rdata_reg[21]_i_2_n_0 ),
        .I1(\axi_rdata_reg[21]_i_3_n_0 ),
        .O(\read_data[0]_14 [21]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[21]_i_2 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(\axi_rdata[21]_i_5_n_0 ),
        .O(\axi_rdata_reg[21]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[21]_i_3 
       (.I0(\axi_rdata[21]_i_6_n_0 ),
        .I1(\axi_rdata[21]_i_7_n_0 ),
        .O(\axi_rdata_reg[21]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[22] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [22]),
        .Q(s_axi_ctrl_status_rdata[22]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[22]_i_1 
       (.I0(\axi_rdata_reg[22]_i_2_n_0 ),
        .I1(\axi_rdata_reg[22]_i_3_n_0 ),
        .O(\read_data[0]_14 [22]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[22]_i_2 
       (.I0(\axi_rdata[22]_i_4_n_0 ),
        .I1(\axi_rdata[22]_i_5_n_0 ),
        .O(\axi_rdata_reg[22]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[22]_i_3 
       (.I0(\axi_rdata[22]_i_6_n_0 ),
        .I1(\axi_rdata[22]_i_7_n_0 ),
        .O(\axi_rdata_reg[22]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[23] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [23]),
        .Q(s_axi_ctrl_status_rdata[23]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[23]_i_1 
       (.I0(\axi_rdata_reg[23]_i_2_n_0 ),
        .I1(\axi_rdata_reg[23]_i_3_n_0 ),
        .O(\read_data[0]_14 [23]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[23]_i_2 
       (.I0(\axi_rdata[23]_i_4_n_0 ),
        .I1(\axi_rdata[23]_i_5_n_0 ),
        .O(\axi_rdata_reg[23]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[23]_i_3 
       (.I0(\axi_rdata[23]_i_6_n_0 ),
        .I1(\axi_rdata[23]_i_7_n_0 ),
        .O(\axi_rdata_reg[23]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[24] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [24]),
        .Q(s_axi_ctrl_status_rdata[24]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[24]_i_1 
       (.I0(\axi_rdata_reg[24]_i_2_n_0 ),
        .I1(\axi_rdata_reg[24]_i_3_n_0 ),
        .O(\read_data[0]_14 [24]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[24]_i_2 
       (.I0(\axi_rdata[24]_i_4_n_0 ),
        .I1(\axi_rdata[24]_i_5_n_0 ),
        .O(\axi_rdata_reg[24]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[24]_i_3 
       (.I0(\axi_rdata[24]_i_6_n_0 ),
        .I1(\axi_rdata[24]_i_7_n_0 ),
        .O(\axi_rdata_reg[24]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[25] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [25]),
        .Q(s_axi_ctrl_status_rdata[25]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[25]_i_1 
       (.I0(\axi_rdata_reg[25]_i_2_n_0 ),
        .I1(\axi_rdata_reg[25]_i_3_n_0 ),
        .O(\read_data[0]_14 [25]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[25]_i_2 
       (.I0(\axi_rdata[25]_i_4_n_0 ),
        .I1(\axi_rdata[25]_i_5_n_0 ),
        .O(\axi_rdata_reg[25]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[25]_i_3 
       (.I0(\axi_rdata[25]_i_6_n_0 ),
        .I1(\axi_rdata[25]_i_7_n_0 ),
        .O(\axi_rdata_reg[25]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[26] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [26]),
        .Q(s_axi_ctrl_status_rdata[26]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[26]_i_1 
       (.I0(\axi_rdata_reg[26]_i_2_n_0 ),
        .I1(\axi_rdata_reg[26]_i_3_n_0 ),
        .O(\read_data[0]_14 [26]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[26]_i_2 
       (.I0(\axi_rdata[26]_i_4_n_0 ),
        .I1(\axi_rdata[26]_i_5_n_0 ),
        .O(\axi_rdata_reg[26]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[26]_i_3 
       (.I0(\axi_rdata[26]_i_6_n_0 ),
        .I1(\axi_rdata[26]_i_7_n_0 ),
        .O(\axi_rdata_reg[26]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[27] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [27]),
        .Q(s_axi_ctrl_status_rdata[27]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[27]_i_1 
       (.I0(\axi_rdata_reg[27]_i_2_n_0 ),
        .I1(\axi_rdata_reg[27]_i_3_n_0 ),
        .O(\read_data[0]_14 [27]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[27]_i_2 
       (.I0(\axi_rdata[27]_i_4_n_0 ),
        .I1(\axi_rdata[27]_i_5_n_0 ),
        .O(\axi_rdata_reg[27]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[27]_i_3 
       (.I0(\axi_rdata[27]_i_6_n_0 ),
        .I1(\axi_rdata[27]_i_7_n_0 ),
        .O(\axi_rdata_reg[27]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[28] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [28]),
        .Q(s_axi_ctrl_status_rdata[28]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[28]_i_1 
       (.I0(\axi_rdata_reg[28]_i_2_n_0 ),
        .I1(\axi_rdata_reg[28]_i_3_n_0 ),
        .O(\read_data[0]_14 [28]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[28]_i_2 
       (.I0(\axi_rdata[28]_i_4_n_0 ),
        .I1(\axi_rdata[28]_i_5_n_0 ),
        .O(\axi_rdata_reg[28]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[28]_i_3 
       (.I0(\axi_rdata[28]_i_6_n_0 ),
        .I1(\axi_rdata[28]_i_7_n_0 ),
        .O(\axi_rdata_reg[28]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[29] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [29]),
        .Q(s_axi_ctrl_status_rdata[29]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[29]_i_1 
       (.I0(\axi_rdata_reg[29]_i_2_n_0 ),
        .I1(\axi_rdata_reg[29]_i_3_n_0 ),
        .O(\read_data[0]_14 [29]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[29]_i_2 
       (.I0(\axi_rdata[29]_i_4_n_0 ),
        .I1(\axi_rdata[29]_i_5_n_0 ),
        .O(\axi_rdata_reg[29]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[29]_i_3 
       (.I0(\axi_rdata[29]_i_6_n_0 ),
        .I1(\axi_rdata[29]_i_7_n_0 ),
        .O(\axi_rdata_reg[29]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[2] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [2]),
        .Q(s_axi_ctrl_status_rdata[2]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[2]_i_1 
       (.I0(\axi_rdata_reg[2]_i_2_n_0 ),
        .I1(\axi_rdata_reg[2]_i_3_n_0 ),
        .O(\read_data[0]_14 [2]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[2]_i_2 
       (.I0(\axi_rdata[2]_i_4_n_0 ),
        .I1(\axi_rdata[2]_i_5_n_0 ),
        .O(\axi_rdata_reg[2]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[2]_i_3 
       (.I0(\axi_rdata[2]_i_6_n_0 ),
        .I1(\axi_rdata[2]_i_7_n_0 ),
        .O(\axi_rdata_reg[2]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[30] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [30]),
        .Q(s_axi_ctrl_status_rdata[30]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[30]_i_1 
       (.I0(\axi_rdata_reg[30]_i_2_n_0 ),
        .I1(\axi_rdata_reg[30]_i_3_n_0 ),
        .O(\read_data[0]_14 [30]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[30]_i_2 
       (.I0(\axi_rdata[30]_i_4_n_0 ),
        .I1(\axi_rdata[30]_i_5_n_0 ),
        .O(\axi_rdata_reg[30]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[30]_i_3 
       (.I0(\axi_rdata[30]_i_6_n_0 ),
        .I1(\axi_rdata[30]_i_7_n_0 ),
        .O(\axi_rdata_reg[30]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[31] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [31]),
        .Q(s_axi_ctrl_status_rdata[31]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[31]_i_2 
       (.I0(\axi_rdata_reg[31]_i_3_n_0 ),
        .I1(\axi_rdata_reg[31]_i_4_n_0 ),
        .O(\read_data[0]_14 [31]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[31]_i_3 
       (.I0(\axi_rdata[31]_i_5_n_0 ),
        .I1(\axi_rdata[31]_i_6_n_0 ),
        .O(\axi_rdata_reg[31]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[31]_i_4 
       (.I0(\axi_rdata[31]_i_7_n_0 ),
        .I1(\axi_rdata[31]_i_8_n_0 ),
        .O(\axi_rdata_reg[31]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[3] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [3]),
        .Q(s_axi_ctrl_status_rdata[3]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[3]_i_1 
       (.I0(\axi_rdata_reg[3]_i_2_n_0 ),
        .I1(\axi_rdata_reg[3]_i_3_n_0 ),
        .O(\read_data[0]_14 [3]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[3]_i_2 
       (.I0(\axi_rdata[3]_i_4_n_0 ),
        .I1(\axi_rdata[3]_i_5_n_0 ),
        .O(\axi_rdata_reg[3]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[3]_i_3 
       (.I0(\axi_rdata[3]_i_6_n_0 ),
        .I1(\axi_rdata[3]_i_7_n_0 ),
        .O(\axi_rdata_reg[3]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[4] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [4]),
        .Q(s_axi_ctrl_status_rdata[4]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[4]_i_1 
       (.I0(\axi_rdata_reg[4]_i_2_n_0 ),
        .I1(\axi_rdata_reg[4]_i_3_n_0 ),
        .O(\read_data[0]_14 [4]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[4]_i_2 
       (.I0(\axi_rdata[4]_i_4_n_0 ),
        .I1(\axi_rdata[4]_i_5_n_0 ),
        .O(\axi_rdata_reg[4]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[4]_i_3 
       (.I0(\axi_rdata[4]_i_6_n_0 ),
        .I1(\axi_rdata[4]_i_7_n_0 ),
        .O(\axi_rdata_reg[4]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[5] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [5]),
        .Q(s_axi_ctrl_status_rdata[5]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[5]_i_1 
       (.I0(\axi_rdata_reg[5]_i_2_n_0 ),
        .I1(\axi_rdata_reg[5]_i_3_n_0 ),
        .O(\read_data[0]_14 [5]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[5]_i_2 
       (.I0(\axi_rdata[5]_i_4_n_0 ),
        .I1(\axi_rdata[5]_i_5_n_0 ),
        .O(\axi_rdata_reg[5]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[5]_i_3 
       (.I0(\axi_rdata[5]_i_6_n_0 ),
        .I1(\axi_rdata[5]_i_7_n_0 ),
        .O(\axi_rdata_reg[5]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[6] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [6]),
        .Q(s_axi_ctrl_status_rdata[6]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[6]_i_1 
       (.I0(\axi_rdata_reg[6]_i_2_n_0 ),
        .I1(\axi_rdata_reg[6]_i_3_n_0 ),
        .O(\read_data[0]_14 [6]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[6]_i_2 
       (.I0(\axi_rdata[6]_i_4_n_0 ),
        .I1(\axi_rdata[6]_i_5_n_0 ),
        .O(\axi_rdata_reg[6]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[6]_i_3 
       (.I0(\axi_rdata[6]_i_6_n_0 ),
        .I1(\axi_rdata[6]_i_7_n_0 ),
        .O(\axi_rdata_reg[6]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[7] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [7]),
        .Q(s_axi_ctrl_status_rdata[7]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[7]_i_1 
       (.I0(\axi_rdata_reg[7]_i_2_n_0 ),
        .I1(\axi_rdata_reg[7]_i_3_n_0 ),
        .O(\read_data[0]_14 [7]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[7]_i_2 
       (.I0(\axi_rdata[7]_i_4_n_0 ),
        .I1(\axi_rdata[7]_i_5_n_0 ),
        .O(\axi_rdata_reg[7]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[7]_i_3 
       (.I0(\axi_rdata[7]_i_6_n_0 ),
        .I1(\axi_rdata[7]_i_7_n_0 ),
        .O(\axi_rdata_reg[7]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[8] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [8]),
        .Q(s_axi_ctrl_status_rdata[8]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[8]_i_1 
       (.I0(\axi_rdata_reg[8]_i_2_n_0 ),
        .I1(\axi_rdata_reg[8]_i_3_n_0 ),
        .O(\read_data[0]_14 [8]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[8]_i_2 
       (.I0(\axi_rdata[8]_i_4_n_0 ),
        .I1(\axi_rdata[8]_i_5_n_0 ),
        .O(\axi_rdata_reg[8]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[8]_i_3 
       (.I0(\axi_rdata[8]_i_6_n_0 ),
        .I1(\axi_rdata[8]_i_7_n_0 ),
        .O(\axi_rdata_reg[8]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[9] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [9]),
        .Q(s_axi_ctrl_status_rdata[9]),
        .R(\axi_rdata_reg[0]_0 ));
  MUXF8 \axi_rdata_reg[9]_i_1 
       (.I0(\axi_rdata_reg[9]_i_2_n_0 ),
        .I1(\axi_rdata_reg[9]_i_3_n_0 ),
        .O(\read_data[0]_14 [9]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[9]_i_2 
       (.I0(\axi_rdata[9]_i_4_n_0 ),
        .I1(\axi_rdata[9]_i_5_n_0 ),
        .O(\axi_rdata_reg[9]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[9]_i_3 
       (.I0(\axi_rdata[9]_i_6_n_0 ),
        .I1(\axi_rdata[9]_i_7_n_0 ),
        .O(\axi_rdata_reg[9]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE axi_rvalid_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_arready_reg_0),
        .Q(s_axi_ctrl_status_rvalid),
        .R(\axi_rdata_reg[0]_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    axi_wready_i_1
       (.I0(s_axi_ctrl_status_wvalid),
        .I1(s_axi_ctrl_status_awvalid),
        .I2(s_axi_ctrl_status_wready),
        .O(axi_wready_i_1_n_0));
  FDRE axi_wready_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_wready_i_1_n_0),
        .Q(s_axi_ctrl_status_wready),
        .R(\axi_rdata_reg[0]_0 ));
  LUT4 #(
    .INIT(16'h4000)) 
    b_ram_data_reg_i_1
       (.I0(\state_reg[1]_0 ),
        .I1(\state_reg[0]_0 ),
        .I2(\counter_reg[4] ),
        .I3(Q[7]),
        .O(\count_i_reg[6] ));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_10
       (.I0(aresetn),
        .I1(data_in[7]),
        .O(DIADI[7]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_11
       (.I0(aresetn),
        .I1(data_in[6]),
        .O(DIADI[6]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_12
       (.I0(aresetn),
        .I1(data_in[5]),
        .O(DIADI[5]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_13
       (.I0(aresetn),
        .I1(data_in[4]),
        .O(DIADI[4]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_14
       (.I0(aresetn),
        .I1(data_in[3]),
        .O(DIADI[3]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_15
       (.I0(aresetn),
        .I1(data_in[2]),
        .O(DIADI[2]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_16
       (.I0(aresetn),
        .I1(data_in[1]),
        .O(DIADI[1]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_17
       (.I0(aresetn),
        .I1(data_in[0]),
        .O(DIADI[0]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_18
       (.I0(aresetn),
        .I1(data_in[31]),
        .O(DIBDI[15]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_19
       (.I0(aresetn),
        .I1(data_in[30]),
        .O(DIBDI[14]));
  LUT4 #(
    .INIT(16'h4000)) 
    b_ram_data_reg_i_1__0
       (.I0(\state_reg[1]_0 ),
        .I1(\state_reg[0]_0 ),
        .I2(\counter_reg[4] ),
        .I3(Q[6]),
        .O(b_ram_data_reg));
  LUT4 #(
    .INIT(16'h4000)) 
    b_ram_data_reg_i_1__1
       (.I0(\state_reg[1]_0 ),
        .I1(\state_reg[0]_0 ),
        .I2(\counter_reg[4] ),
        .I3(Q[5]),
        .O(b_ram_data_reg_0));
  LUT4 #(
    .INIT(16'h4000)) 
    b_ram_data_reg_i_1__2
       (.I0(\state_reg[1]_0 ),
        .I1(\state_reg[0]_0 ),
        .I2(\counter_reg[4] ),
        .I3(Q[4]),
        .O(b_ram_data_reg_1));
  LUT4 #(
    .INIT(16'h4000)) 
    b_ram_data_reg_i_1__3
       (.I0(\state_reg[1]_0 ),
        .I1(\state_reg[0]_0 ),
        .I2(\counter_reg[4] ),
        .I3(Q[3]),
        .O(b_ram_data_reg_2));
  LUT4 #(
    .INIT(16'h4000)) 
    b_ram_data_reg_i_1__4
       (.I0(\state_reg[1]_0 ),
        .I1(\state_reg[0]_0 ),
        .I2(\counter_reg[4] ),
        .I3(Q[2]),
        .O(b_ram_data_reg_3));
  LUT4 #(
    .INIT(16'h4000)) 
    b_ram_data_reg_i_1__5
       (.I0(\state_reg[1]_0 ),
        .I1(\state_reg[0]_0 ),
        .I2(\counter_reg[4] ),
        .I3(Q[1]),
        .O(b_ram_data_reg_4));
  LUT4 #(
    .INIT(16'h80FF)) 
    b_ram_data_reg_i_1__6
       (.I0(s_axis_tvalid),
        .I1(cpu2emsc_register[12]),
        .I2(initialized_reg),
        .I3(aresetn),
        .O(b_ram_data_reg_5));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_2
       (.I0(aresetn),
        .I1(data_in[15]),
        .O(DIADI[15]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_20
       (.I0(aresetn),
        .I1(data_in[29]),
        .O(DIBDI[13]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_21
       (.I0(aresetn),
        .I1(data_in[28]),
        .O(DIBDI[12]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_22
       (.I0(aresetn),
        .I1(data_in[27]),
        .O(DIBDI[11]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_23
       (.I0(aresetn),
        .I1(data_in[26]),
        .O(DIBDI[10]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_24
       (.I0(aresetn),
        .I1(data_in[25]),
        .O(DIBDI[9]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_25
       (.I0(aresetn),
        .I1(data_in[24]),
        .O(DIBDI[8]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_26
       (.I0(aresetn),
        .I1(data_in[23]),
        .O(DIBDI[7]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_27
       (.I0(aresetn),
        .I1(data_in[22]),
        .O(DIBDI[6]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_28
       (.I0(aresetn),
        .I1(data_in[21]),
        .O(DIBDI[5]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_29
       (.I0(aresetn),
        .I1(data_in[20]),
        .O(DIBDI[4]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_3
       (.I0(aresetn),
        .I1(data_in[14]),
        .O(DIADI[14]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_30
       (.I0(aresetn),
        .I1(data_in[19]),
        .O(DIBDI[3]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_31
       (.I0(aresetn),
        .I1(data_in[18]),
        .O(DIBDI[2]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_32
       (.I0(aresetn),
        .I1(data_in[17]),
        .O(DIBDI[1]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_33
       (.I0(aresetn),
        .I1(data_in[16]),
        .O(DIBDI[0]));
  LUT5 #(
    .INIT(32'h20203000)) 
    b_ram_data_reg_i_34
       (.I0(Q[0]),
        .I1(\state_reg[1]_0 ),
        .I2(\counter_reg[4] ),
        .I3(cpu2emsc_register[13]),
        .I4(\state_reg[0]_0 ),
        .O(WEBWE));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_4
       (.I0(aresetn),
        .I1(data_in[13]),
        .O(DIADI[13]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_5
       (.I0(aresetn),
        .I1(data_in[12]),
        .O(DIADI[12]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_6
       (.I0(aresetn),
        .I1(data_in[11]),
        .O(DIADI[11]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_7
       (.I0(aresetn),
        .I1(data_in[10]),
        .O(DIADI[10]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_8
       (.I0(aresetn),
        .I1(data_in[9]),
        .O(DIADI[9]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_9
       (.I0(aresetn),
        .I1(data_in[8]),
        .O(DIADI[8]));
  LUT6 #(
    .INIT(64'h0000000040E04040)) 
    \b_ram_sel[7]_i_1 
       (.I0(\state_reg[0]_0 ),
        .I1(cpu2emsc_register[13]),
        .I2(\counter_reg[4] ),
        .I3(Q[7]),
        .I4(\counter_reg[4]_0 ),
        .I5(\state_reg[1]_0 ),
        .O(\b_ram_sel_reg[0] ));
  LUT6 #(
    .INIT(64'h0F00FFFFBFB00000)) 
    \counter[0]_i_1 
       (.I0(\count_i_reg[6] ),
        .I1(\counter_reg[4]_0 ),
        .I2(\state_reg[0]_0 ),
        .I3(\counter[0]_i_2_n_0 ),
        .I4(\counter_reg[6] ),
        .I5(counter[0]),
        .O(\counter_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \counter[0]_i_2 
       (.I0(\counter_reg[4] ),
        .I1(cpu2emsc_register[13]),
        .O(\counter[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'h0BB0)) 
    \counter[1]_i_1 
       (.I0(\count_i_reg[6] ),
        .I1(\counter_reg[4]_0 ),
        .I2(counter[0]),
        .I3(counter[1]),
        .O(\counter_reg[1]_7 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'h0BBBB000)) 
    \counter[2]_i_1 
       (.I0(\count_i_reg[6] ),
        .I1(\counter_reg[4]_0 ),
        .I2(counter[0]),
        .I3(counter[1]),
        .I4(counter[2]),
        .O(\counter_reg[2] ));
  LUT6 #(
    .INIT(64'h0BBBBBBBB0000000)) 
    \counter[3]_i_1 
       (.I0(\count_i_reg[6] ),
        .I1(\counter_reg[4]_0 ),
        .I2(counter[1]),
        .I3(counter[0]),
        .I4(counter[2]),
        .I5(counter[3]),
        .O(\counter_reg[3] ));
  LUT5 #(
    .INIT(32'h80FF0000)) 
    \counter[4]_i_1 
       (.I0(Q[7]),
        .I1(\counter_reg[4] ),
        .I2(\state_reg[0]_0 ),
        .I3(\counter_reg[4]_0 ),
        .I4(counter2[0]),
        .O(\counter_reg[4]_1 ));
  LUT5 #(
    .INIT(32'h80FF0000)) 
    \counter[5]_i_1 
       (.I0(Q[7]),
        .I1(\counter_reg[4] ),
        .I2(\state_reg[0]_0 ),
        .I3(\counter_reg[4]_0 ),
        .I4(counter2[1]),
        .O(\counter_reg[5] ));
  LUT3 #(
    .INIT(8'h04)) 
    \counter[6]_i_10 
       (.I0(v_len[6]),
        .I1(counter2[2]),
        .I2(v_len[7]),
        .O(\counter[6]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \counter[6]_i_11 
       (.I0(counter2[0]),
        .I1(v_len[4]),
        .I2(v_len[5]),
        .I3(counter2[1]),
        .O(\counter[6]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h0777733331111000)) 
    \counter[6]_i_12 
       (.I0(v_len[2]),
        .I1(v_len[3]),
        .I2(counter[1]),
        .I3(counter[0]),
        .I4(counter[2]),
        .I5(counter[3]),
        .O(\counter[6]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'h0731)) 
    \counter[6]_i_13 
       (.I0(\axi_rdata_reg[0]_1 ),
        .I1(v_len[1]),
        .I2(counter[0]),
        .I3(counter[1]),
        .O(\counter[6]_i_13_n_0 ));
  LUT3 #(
    .INIT(8'h09)) 
    \counter[6]_i_14 
       (.I0(counter2[2]),
        .I1(v_len[6]),
        .I2(v_len[7]),
        .O(\counter[6]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \counter[6]_i_15 
       (.I0(counter2[0]),
        .I1(v_len[4]),
        .I2(counter2[1]),
        .I3(v_len[5]),
        .O(\counter[6]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'h2A95400040002A95)) 
    \counter[6]_i_16 
       (.I0(v_len[2]),
        .I1(counter[1]),
        .I2(counter[0]),
        .I3(counter[2]),
        .I4(counter[3]),
        .I5(v_len[3]),
        .O(\counter[6]_i_16_n_0 ));
  LUT4 #(
    .INIT(16'h2442)) 
    \counter[6]_i_17 
       (.I0(\axi_rdata_reg[0]_1 ),
        .I1(counter[0]),
        .I2(counter[1]),
        .I3(v_len[1]),
        .O(\counter[6]_i_17_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'h2022)) 
    \counter[6]_i_2 
       (.I0(aresetn),
        .I1(\state_reg[1]_0 ),
        .I2(\counter_reg[4] ),
        .I3(\state_reg[0]_0 ),
        .O(\counter_reg[6] ));
  LUT5 #(
    .INIT(32'h80FF0000)) 
    \counter[6]_i_3 
       (.I0(Q[7]),
        .I1(\counter_reg[4] ),
        .I2(\state_reg[0]_0 ),
        .I3(\counter_reg[4]_0 ),
        .I4(counter2[2]),
        .O(\counter_reg[6]_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \counter[6]_i_7 
       (.I0(v_len[10]),
        .I1(v_len[11]),
        .O(\counter[6]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \counter[6]_i_8 
       (.I0(v_len[8]),
        .I1(v_len[9]),
        .O(\counter[6]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \counter[8]_i_1 
       (.I0(\add_r_reg[47] ),
        .I1(CO),
        .I2(aresetn),
        .O(SR));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \counter[8]_i_1__0 
       (.I0(\add_r_reg[47] ),
        .I1(\slv_regs_reg[0][11]_0 ),
        .I2(aresetn),
        .O(\counter_reg[8] ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \counter[8]_i_1__1 
       (.I0(\add_r_reg[47] ),
        .I1(\slv_regs_reg[0][11]_1 ),
        .I2(aresetn),
        .O(\counter_reg[8]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \counter[8]_i_1__2 
       (.I0(\add_r_reg[47] ),
        .I1(\slv_regs_reg[0][11]_2 ),
        .I2(aresetn),
        .O(\counter_reg[8]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \counter[8]_i_1__3 
       (.I0(\add_r_reg[47] ),
        .I1(\slv_regs_reg[0][11]_3 ),
        .I2(aresetn),
        .O(\counter_reg[8]_2 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \counter[8]_i_1__4 
       (.I0(\add_r_reg[47] ),
        .I1(\slv_regs_reg[0][11]_4 ),
        .I2(aresetn),
        .O(\counter_reg[8]_3 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \counter[8]_i_1__5 
       (.I0(\add_r_reg[47] ),
        .I1(\slv_regs_reg[0][11]_5 ),
        .I2(aresetn),
        .O(\counter_reg[8]_4 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \counter[8]_i_1__6 
       (.I0(\add_r_reg[47] ),
        .I1(\slv_regs_reg[0][11]_6 ),
        .I2(aresetn),
        .O(\counter_reg[8]_5 ));
  LUT5 #(
    .INIT(32'hFFFF8000)) 
    \counter[8]_i_2 
       (.I0(cpu2emsc_register[12]),
        .I1(initialized_reg),
        .I2(s_axis_tready),
        .I3(s_axis_tvalid),
        .I4(dp_extend_end),
        .O(\add_r_reg[47] ));
  CARRY4 \counter_reg[6]_i_4 
       (.CI(\counter_reg[6]_i_6_n_0 ),
        .CO({\NLW_counter_reg[6]_i_4_CO_UNCONNECTED [3:2],\counter_reg[4]_0 ,\counter_reg[6]_i_4_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_counter_reg[6]_i_4_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,\counter[6]_i_7_n_0 ,\counter[6]_i_8_n_0 }));
  CARRY4 \counter_reg[6]_i_6 
       (.CI(1'b0),
        .CO({\counter_reg[6]_i_6_n_0 ,\counter_reg[6]_i_6_n_1 ,\counter_reg[6]_i_6_n_2 ,\counter_reg[6]_i_6_n_3 }),
        .CYINIT(1'b1),
        .DI({\counter[6]_i_10_n_0 ,\counter[6]_i_11_n_0 ,\counter[6]_i_12_n_0 ,\counter[6]_i_13_n_0 }),
        .O(\NLW_counter_reg[6]_i_6_O_UNCONNECTED [3:0]),
        .S({\counter[6]_i_14_n_0 ,\counter[6]_i_15_n_0 ,\counter[6]_i_16_n_0 ,\counter[6]_i_17_n_0 }));
  LUT3 #(
    .INIT(8'hF8)) 
    in_stream_ready_i_1
       (.I0(initialized_reg),
        .I1(cpu2emsc_register[12]),
        .I2(s_axis_tready),
        .O(in_stream_ready_reg));
  LUT1 #(
    .INIT(2'h1)) 
    in_stream_ready_i_2
       (.I0(aresetn),
        .O(\axi_rdata_reg[0]_0 ));
  LUT6 #(
    .INIT(64'h2200220000002000)) 
    \read_address[6]_i_1 
       (.I0(aresetn),
        .I1(\state_reg[0]_0 ),
        .I2(initialized_reg),
        .I3(read_enable),
        .I4(\counter[0]_i_2_n_0 ),
        .I5(\state_reg[1]_0 ),
        .O(E));
  LUT4 #(
    .INIT(16'h2F02)) 
    \read_address[6]_i_12 
       (.I0(\read_address_reg[5] [4]),
        .I1(state1[4]),
        .I2(state1[5]),
        .I3(\read_address_reg[5] [5]),
        .O(\read_address[6]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'h2F02)) 
    \read_address[6]_i_13 
       (.I0(\read_address_reg[5] [2]),
        .I1(state1[2]),
        .I2(state1[3]),
        .I3(\read_address_reg[5] [3]),
        .O(\read_address[6]_i_13_n_0 ));
  LUT4 #(
    .INIT(16'h8F08)) 
    \read_address[6]_i_14 
       (.I0(\axi_rdata_reg[0]_1 ),
        .I1(\read_address_reg[5] [0]),
        .I2(state1[1]),
        .I3(\read_address_reg[5] [1]),
        .O(\read_address[6]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \read_address[6]_i_16 
       (.I0(\read_address_reg[5] [4]),
        .I1(state1[4]),
        .I2(\read_address_reg[5] [5]),
        .I3(state1[5]),
        .O(\read_address[6]_i_16_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \read_address[6]_i_17 
       (.I0(\read_address_reg[5] [2]),
        .I1(state1[2]),
        .I2(\read_address_reg[5] [3]),
        .I3(state1[3]),
        .O(\read_address[6]_i_17_n_0 ));
  LUT4 #(
    .INIT(16'h6006)) 
    \read_address[6]_i_18 
       (.I0(\read_address_reg[5] [0]),
        .I1(\axi_rdata_reg[0]_1 ),
        .I2(\read_address_reg[5] [1]),
        .I3(state1[1]),
        .O(\read_address[6]_i_18_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \read_address[6]_i_20 
       (.I0(v_len[11]),
        .O(\read_address[6]_i_20_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \read_address[6]_i_21 
       (.I0(v_len[10]),
        .O(\read_address[6]_i_21_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \read_address[6]_i_22 
       (.I0(v_len[9]),
        .O(\read_address[6]_i_22_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \read_address[6]_i_24 
       (.I0(v_len[8]),
        .O(\read_address[6]_i_24_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \read_address[6]_i_25 
       (.I0(v_len[7]),
        .O(\read_address[6]_i_25_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \read_address[6]_i_26 
       (.I0(v_len[6]),
        .O(\read_address[6]_i_26_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \read_address[6]_i_27 
       (.I0(v_len[5]),
        .O(\read_address[6]_i_27_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \read_address[6]_i_28 
       (.I0(v_len[4]),
        .O(\read_address[6]_i_28_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \read_address[6]_i_29 
       (.I0(v_len[3]),
        .O(\read_address[6]_i_29_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \read_address[6]_i_3 
       (.I0(initialized_reg),
        .I1(cpu2emsc_register[12]),
        .I2(s_axis_tvalid),
        .O(read_enable));
  LUT1 #(
    .INIT(2'h1)) 
    \read_address[6]_i_30 
       (.I0(v_len[2]),
        .O(\read_address[6]_i_30_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \read_address[6]_i_31 
       (.I0(v_len[1]),
        .O(\read_address[6]_i_31_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \read_address[6]_i_7 
       (.I0(\read_address_reg[6]_i_8_n_0 ),
        .O(state1[12]));
  CARRY4 \read_address_reg[6]_i_19 
       (.CI(\read_address_reg[6]_i_23_n_0 ),
        .CO({\read_address_reg[6]_i_19_n_0 ,\read_address_reg[6]_i_19_n_1 ,\read_address_reg[6]_i_19_n_2 ,\read_address_reg[6]_i_19_n_3 }),
        .CYINIT(1'b0),
        .DI(v_len[8:5]),
        .O({\read_address_reg[2] ,state1[5]}),
        .S({\read_address[6]_i_24_n_0 ,\read_address[6]_i_25_n_0 ,\read_address[6]_i_26_n_0 ,\read_address[6]_i_27_n_0 }));
  CARRY4 \read_address_reg[6]_i_23 
       (.CI(1'b0),
        .CO({\read_address_reg[6]_i_23_n_0 ,\read_address_reg[6]_i_23_n_1 ,\read_address_reg[6]_i_23_n_2 ,\read_address_reg[6]_i_23_n_3 }),
        .CYINIT(\axi_rdata_reg[0]_1 ),
        .DI(v_len[4:1]),
        .O(state1[4:1]),
        .S({\read_address[6]_i_28_n_0 ,\read_address[6]_i_29_n_0 ,\read_address[6]_i_30_n_0 ,\read_address[6]_i_31_n_0 }));
  CARRY4 \read_address_reg[6]_i_4 
       (.CI(\read_address_reg[6]_i_6_n_0 ),
        .CO({\NLW_read_address_reg[6]_i_4_CO_UNCONNECTED [3],\read_address_reg[2]_0 ,\read_address_reg[6]_i_4_n_2 ,\read_address_reg[6]_i_4_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,state1[12],1'b0,1'b0}),
        .O(\NLW_read_address_reg[6]_i_4_O_UNCONNECTED [3:0]),
        .S({1'b0,\read_address_reg[6]_i_8_n_0 ,\slv_regs_reg[0][11]_7 }));
  CARRY4 \read_address_reg[6]_i_6 
       (.CI(1'b0),
        .CO({\read_address_reg[6]_i_6_n_0 ,\read_address_reg[6]_i_6_n_1 ,\read_address_reg[6]_i_6_n_2 ,\read_address_reg[6]_i_6_n_3 }),
        .CYINIT(1'b1),
        .DI({DI,\read_address[6]_i_12_n_0 ,\read_address[6]_i_13_n_0 ,\read_address[6]_i_14_n_0 }),
        .O(\NLW_read_address_reg[6]_i_6_O_UNCONNECTED [3:0]),
        .S({\read_address_reg[6] ,\read_address[6]_i_16_n_0 ,\read_address[6]_i_17_n_0 ,\read_address[6]_i_18_n_0 }));
  CARRY4 \read_address_reg[6]_i_8 
       (.CI(\read_address_reg[6]_i_19_n_0 ),
        .CO({\read_address_reg[6]_i_8_n_0 ,\NLW_read_address_reg[6]_i_8_CO_UNCONNECTED [2],\read_address_reg[6]_i_8_n_2 ,\read_address_reg[6]_i_8_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,v_len[11:9]}),
        .O({\NLW_read_address_reg[6]_i_8_O_UNCONNECTED [3],\read_address_reg[2]_1 }),
        .S({1'b1,\read_address[6]_i_20_n_0 ,\read_address[6]_i_21_n_0 ,\read_address[6]_i_22_n_0 }));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_regs[0][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(p_1_in[12]));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_regs[0][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(p_1_in[23]));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_regs[0][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(p_1_in[31]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_regs[0][31]_i_2 
       (.I0(s_axi_ctrl_status_wready),
        .I1(s_axi_ctrl_status_wvalid),
        .I2(s_axi_ctrl_status_awready),
        .I3(s_axi_ctrl_status_awvalid),
        .O(slv_reg_wren__2));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_regs[0][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(p_1_in[7]));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[10][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\slv_regs[10][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[10][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\slv_regs[10][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[10][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\slv_regs[10][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[10][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\slv_regs[10][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[11][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\slv_regs[11][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[11][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\slv_regs[11][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[11][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\slv_regs[11][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[11][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\slv_regs[11][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[12][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\slv_regs[12][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[12][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\slv_regs[12][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[12][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\slv_regs[12][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[12][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\slv_regs[12][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[13][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\slv_regs[13][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[13][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\slv_regs[13][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[13][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\slv_regs[13][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[13][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\slv_regs[13][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[14][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\slv_regs[14][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[14][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\slv_regs[14][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[14][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\slv_regs[14][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[14][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\slv_regs[14][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_regs[15][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\slv_regs[15][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_regs[15][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\slv_regs[15][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_regs[15][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\slv_regs[15][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \slv_regs[15][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\slv_regs[15][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[1][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\slv_regs[1][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[1][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\slv_regs[1][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[1][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\slv_regs[1][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[1][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\slv_regs[1][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[2][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\slv_regs[2][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[2][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\slv_regs[2][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[2][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\slv_regs[2][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[2][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\slv_regs[2][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[3][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(p_0_in[3]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\slv_regs[3][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[3][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(p_0_in[3]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\slv_regs[3][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[3][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(p_0_in[3]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\slv_regs[3][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[3][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(p_0_in[3]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\slv_regs[3][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[4][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\slv_regs[4][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[4][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\slv_regs[4][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[4][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\slv_regs[4][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[4][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\slv_regs[4][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[5][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\slv_regs[5][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[5][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\slv_regs[5][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[5][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\slv_regs[5][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[5][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\slv_regs[5][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[6][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\slv_regs[6][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[6][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\slv_regs[6][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[6][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\slv_regs[6][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[6][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\slv_regs[6][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[7][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\slv_regs[7][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[7][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\slv_regs[7][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[7][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\slv_regs[7][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \slv_regs[7][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\slv_regs[7][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[8][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\slv_regs[8][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[8][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\slv_regs[8][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[8][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\slv_regs[8][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_regs[8][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\slv_regs[8][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[9][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\slv_regs[9][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[9][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\slv_regs[9][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[9][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\slv_regs[9][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \slv_regs[9][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\slv_regs[9][7]_i_1_n_0 ));
  FDRE \slv_regs_reg[0][0] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\axi_rdata_reg[0]_1 ),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][10] 
       (.C(clk),
        .CE(p_1_in[12]),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(v_len[10]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][11] 
       (.C(clk),
        .CE(p_1_in[12]),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(v_len[11]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][12] 
       (.C(clk),
        .CE(p_1_in[12]),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(cpu2emsc_register[12]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][13] 
       (.C(clk),
        .CE(p_1_in[12]),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(cpu2emsc_register[13]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][14] 
       (.C(clk),
        .CE(p_1_in[12]),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg_n_0_[0][14] ),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][15] 
       (.C(clk),
        .CE(p_1_in[12]),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg_n_0_[0][15] ),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][16] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg_n_0_[0][16] ),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][17] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg_n_0_[0][17] ),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][18] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg_n_0_[0][18] ),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][19] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg_n_0_[0][19] ),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][1] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(v_len[1]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][20] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg_n_0_[0][20] ),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][21] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg_n_0_[0][21] ),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][22] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg_n_0_[0][22] ),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][23] 
       (.C(clk),
        .CE(p_1_in[23]),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg_n_0_[0][23] ),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][24] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg_n_0_[0][24] ),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][25] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg_n_0_[0][25] ),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][26] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg_n_0_[0][26] ),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][27] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg_n_0_[0][27] ),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][28] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg_n_0_[0][28] ),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][29] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg_n_0_[0][29] ),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][2] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(v_len[2]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][30] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg_n_0_[0][30] ),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][31] 
       (.C(clk),
        .CE(p_1_in[31]),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg_n_0_[0][31] ),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][3] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(v_len[3]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][4] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(v_len[4]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][5] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(v_len[5]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][6] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(v_len[6]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][7] 
       (.C(clk),
        .CE(p_1_in[7]),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(v_len[7]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][8] 
       (.C(clk),
        .CE(p_1_in[12]),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(v_len[8]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[0][9] 
       (.C(clk),
        .CE(p_1_in[12]),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(v_len[9]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][0] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[10]_8 [0]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][10] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[10]_8 [10]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][11] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[10]_8 [11]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][12] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[10]_8 [12]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][13] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[10]_8 [13]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][14] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[10]_8 [14]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][15] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[10]_8 [15]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][16] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[10]_8 [16]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][17] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[10]_8 [17]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][18] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[10]_8 [18]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][19] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[10]_8 [19]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][1] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[10]_8 [1]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][20] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[10]_8 [20]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][21] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[10]_8 [21]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][22] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[10]_8 [22]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][23] 
       (.C(clk),
        .CE(\slv_regs[10][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[10]_8 [23]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][24] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[10]_8 [24]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][25] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[10]_8 [25]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][26] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[10]_8 [26]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][27] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[10]_8 [27]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][28] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[10]_8 [28]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][29] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[10]_8 [29]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][2] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[10]_8 [2]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][30] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[10]_8 [30]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][31] 
       (.C(clk),
        .CE(\slv_regs[10][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[10]_8 [31]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][3] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[10]_8 [3]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][4] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[10]_8 [4]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][5] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[10]_8 [5]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][6] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[10]_8 [6]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][7] 
       (.C(clk),
        .CE(\slv_regs[10][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[10]_8 [7]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][8] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[10]_8 [8]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[10][9] 
       (.C(clk),
        .CE(\slv_regs[10][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[10]_8 [9]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][0] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[11]_9 [0]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][10] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[11]_9 [10]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][11] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[11]_9 [11]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][12] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[11]_9 [12]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][13] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[11]_9 [13]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][14] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[11]_9 [14]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][15] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[11]_9 [15]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][16] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[11]_9 [16]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][17] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[11]_9 [17]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][18] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[11]_9 [18]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][19] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[11]_9 [19]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][1] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[11]_9 [1]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][20] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[11]_9 [20]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][21] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[11]_9 [21]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][22] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[11]_9 [22]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][23] 
       (.C(clk),
        .CE(\slv_regs[11][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[11]_9 [23]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][24] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[11]_9 [24]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][25] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[11]_9 [25]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][26] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[11]_9 [26]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][27] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[11]_9 [27]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][28] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[11]_9 [28]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][29] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[11]_9 [29]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][2] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[11]_9 [2]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][30] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[11]_9 [30]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][31] 
       (.C(clk),
        .CE(\slv_regs[11][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[11]_9 [31]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][3] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[11]_9 [3]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][4] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[11]_9 [4]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][5] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[11]_9 [5]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][6] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[11]_9 [6]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][7] 
       (.C(clk),
        .CE(\slv_regs[11][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[11]_9 [7]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][8] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[11]_9 [8]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[11][9] 
       (.C(clk),
        .CE(\slv_regs[11][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[11]_9 [9]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][0] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[12]_10 [0]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][10] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[12]_10 [10]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][11] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[12]_10 [11]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][12] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[12]_10 [12]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][13] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[12]_10 [13]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][14] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[12]_10 [14]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][15] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[12]_10 [15]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][16] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[12]_10 [16]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][17] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[12]_10 [17]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][18] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[12]_10 [18]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][19] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[12]_10 [19]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][1] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[12]_10 [1]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][20] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[12]_10 [20]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][21] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[12]_10 [21]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][22] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[12]_10 [22]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][23] 
       (.C(clk),
        .CE(\slv_regs[12][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[12]_10 [23]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][24] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[12]_10 [24]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][25] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[12]_10 [25]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][26] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[12]_10 [26]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][27] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[12]_10 [27]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][28] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[12]_10 [28]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][29] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[12]_10 [29]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][2] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[12]_10 [2]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][30] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[12]_10 [30]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][31] 
       (.C(clk),
        .CE(\slv_regs[12][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[12]_10 [31]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][3] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[12]_10 [3]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][4] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[12]_10 [4]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][5] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[12]_10 [5]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][6] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[12]_10 [6]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][7] 
       (.C(clk),
        .CE(\slv_regs[12][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[12]_10 [7]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][8] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[12]_10 [8]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[12][9] 
       (.C(clk),
        .CE(\slv_regs[12][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[12]_10 [9]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][0] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[13]_11 [0]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][10] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[13]_11 [10]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][11] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[13]_11 [11]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][12] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[13]_11 [12]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][13] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[13]_11 [13]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][14] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[13]_11 [14]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][15] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[13]_11 [15]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][16] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[13]_11 [16]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][17] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[13]_11 [17]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][18] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[13]_11 [18]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][19] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[13]_11 [19]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][1] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[13]_11 [1]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][20] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[13]_11 [20]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][21] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[13]_11 [21]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][22] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[13]_11 [22]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][23] 
       (.C(clk),
        .CE(\slv_regs[13][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[13]_11 [23]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][24] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[13]_11 [24]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][25] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[13]_11 [25]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][26] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[13]_11 [26]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][27] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[13]_11 [27]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][28] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[13]_11 [28]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][29] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[13]_11 [29]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][2] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[13]_11 [2]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][30] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[13]_11 [30]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][31] 
       (.C(clk),
        .CE(\slv_regs[13][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[13]_11 [31]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][3] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[13]_11 [3]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][4] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[13]_11 [4]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][5] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[13]_11 [5]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][6] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[13]_11 [6]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][7] 
       (.C(clk),
        .CE(\slv_regs[13][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[13]_11 [7]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][8] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[13]_11 [8]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[13][9] 
       (.C(clk),
        .CE(\slv_regs[13][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[13]_11 [9]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][0] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[14]_12 [0]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][10] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[14]_12 [10]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][11] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[14]_12 [11]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][12] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[14]_12 [12]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][13] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[14]_12 [13]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][14] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[14]_12 [14]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][15] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[14]_12 [15]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][16] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[14]_12 [16]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][17] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[14]_12 [17]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][18] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[14]_12 [18]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][19] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[14]_12 [19]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][1] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[14]_12 [1]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][20] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[14]_12 [20]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][21] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[14]_12 [21]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][22] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[14]_12 [22]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][23] 
       (.C(clk),
        .CE(\slv_regs[14][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[14]_12 [23]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][24] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[14]_12 [24]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][25] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[14]_12 [25]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][26] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[14]_12 [26]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][27] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[14]_12 [27]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][28] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[14]_12 [28]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][29] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[14]_12 [29]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][2] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[14]_12 [2]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][30] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[14]_12 [30]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][31] 
       (.C(clk),
        .CE(\slv_regs[14][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[14]_12 [31]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][3] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[14]_12 [3]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][4] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[14]_12 [4]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][5] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[14]_12 [5]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][6] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[14]_12 [6]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][7] 
       (.C(clk),
        .CE(\slv_regs[14][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[14]_12 [7]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][8] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[14]_12 [8]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[14][9] 
       (.C(clk),
        .CE(\slv_regs[14][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[14]_12 [9]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][0] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[15]_13 [0]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][10] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[15]_13 [10]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][11] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[15]_13 [11]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][12] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[15]_13 [12]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][13] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[15]_13 [13]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][14] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[15]_13 [14]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][15] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[15]_13 [15]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][16] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[15]_13 [16]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][17] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[15]_13 [17]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][18] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[15]_13 [18]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][19] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[15]_13 [19]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][1] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[15]_13 [1]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][20] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[15]_13 [20]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][21] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[15]_13 [21]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][22] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[15]_13 [22]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][23] 
       (.C(clk),
        .CE(\slv_regs[15][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[15]_13 [23]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][24] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[15]_13 [24]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][25] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[15]_13 [25]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][26] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[15]_13 [26]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][27] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[15]_13 [27]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][28] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[15]_13 [28]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][29] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[15]_13 [29]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][2] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[15]_13 [2]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][30] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[15]_13 [30]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][31] 
       (.C(clk),
        .CE(\slv_regs[15][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[15]_13 [31]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][3] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[15]_13 [3]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][4] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[15]_13 [4]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][5] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[15]_13 [5]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][6] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[15]_13 [6]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][7] 
       (.C(clk),
        .CE(\slv_regs[15][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[15]_13 [7]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][8] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[15]_13 [8]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[15][9] 
       (.C(clk),
        .CE(\slv_regs[15][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[15]_13 [9]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][0] 
       (.C(clk),
        .CE(\slv_regs[1][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(data_in[0]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][10] 
       (.C(clk),
        .CE(\slv_regs[1][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(data_in[10]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][11] 
       (.C(clk),
        .CE(\slv_regs[1][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(data_in[11]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][12] 
       (.C(clk),
        .CE(\slv_regs[1][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(data_in[12]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][13] 
       (.C(clk),
        .CE(\slv_regs[1][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(data_in[13]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][14] 
       (.C(clk),
        .CE(\slv_regs[1][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(data_in[14]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][15] 
       (.C(clk),
        .CE(\slv_regs[1][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(data_in[15]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][16] 
       (.C(clk),
        .CE(\slv_regs[1][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(data_in[16]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][17] 
       (.C(clk),
        .CE(\slv_regs[1][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(data_in[17]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][18] 
       (.C(clk),
        .CE(\slv_regs[1][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(data_in[18]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][19] 
       (.C(clk),
        .CE(\slv_regs[1][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(data_in[19]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][1] 
       (.C(clk),
        .CE(\slv_regs[1][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(data_in[1]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][20] 
       (.C(clk),
        .CE(\slv_regs[1][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(data_in[20]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][21] 
       (.C(clk),
        .CE(\slv_regs[1][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(data_in[21]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][22] 
       (.C(clk),
        .CE(\slv_regs[1][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(data_in[22]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][23] 
       (.C(clk),
        .CE(\slv_regs[1][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(data_in[23]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][24] 
       (.C(clk),
        .CE(\slv_regs[1][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(data_in[24]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][25] 
       (.C(clk),
        .CE(\slv_regs[1][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(data_in[25]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][26] 
       (.C(clk),
        .CE(\slv_regs[1][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(data_in[26]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][27] 
       (.C(clk),
        .CE(\slv_regs[1][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(data_in[27]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][28] 
       (.C(clk),
        .CE(\slv_regs[1][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(data_in[28]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][29] 
       (.C(clk),
        .CE(\slv_regs[1][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(data_in[29]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][2] 
       (.C(clk),
        .CE(\slv_regs[1][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(data_in[2]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][30] 
       (.C(clk),
        .CE(\slv_regs[1][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(data_in[30]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][31] 
       (.C(clk),
        .CE(\slv_regs[1][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(data_in[31]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][3] 
       (.C(clk),
        .CE(\slv_regs[1][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(data_in[3]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][4] 
       (.C(clk),
        .CE(\slv_regs[1][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(data_in[4]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][5] 
       (.C(clk),
        .CE(\slv_regs[1][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(data_in[5]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][6] 
       (.C(clk),
        .CE(\slv_regs[1][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(data_in[6]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][7] 
       (.C(clk),
        .CE(\slv_regs[1][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(data_in[7]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][8] 
       (.C(clk),
        .CE(\slv_regs[1][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(data_in[8]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[1][9] 
       (.C(clk),
        .CE(\slv_regs[1][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(data_in[9]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][0] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[2]_0 [0]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][10] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[2]_0 [10]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][11] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[2]_0 [11]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][12] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[2]_0 [12]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][13] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[2]_0 [13]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][14] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[2]_0 [14]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][15] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[2]_0 [15]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][16] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[2]_0 [16]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][17] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[2]_0 [17]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][18] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[2]_0 [18]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][19] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[2]_0 [19]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][1] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[2]_0 [1]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][20] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[2]_0 [20]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][21] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[2]_0 [21]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][22] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[2]_0 [22]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][23] 
       (.C(clk),
        .CE(\slv_regs[2][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[2]_0 [23]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][24] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[2]_0 [24]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][25] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[2]_0 [25]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][26] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[2]_0 [26]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][27] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[2]_0 [27]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][28] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[2]_0 [28]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][29] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[2]_0 [29]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][2] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[2]_0 [2]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][30] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[2]_0 [30]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][31] 
       (.C(clk),
        .CE(\slv_regs[2][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[2]_0 [31]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][3] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[2]_0 [3]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][4] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[2]_0 [4]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][5] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[2]_0 [5]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][6] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[2]_0 [6]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][7] 
       (.C(clk),
        .CE(\slv_regs[2][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[2]_0 [7]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][8] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[2]_0 [8]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[2][9] 
       (.C(clk),
        .CE(\slv_regs[2][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[2]_0 [9]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][0] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[3]_1 [0]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][10] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[3]_1 [10]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][11] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[3]_1 [11]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][12] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[3]_1 [12]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][13] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[3]_1 [13]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][14] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[3]_1 [14]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][15] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[3]_1 [15]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][16] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[3]_1 [16]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][17] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[3]_1 [17]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][18] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[3]_1 [18]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][19] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[3]_1 [19]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][1] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[3]_1 [1]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][20] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[3]_1 [20]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][21] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[3]_1 [21]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][22] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[3]_1 [22]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][23] 
       (.C(clk),
        .CE(\slv_regs[3][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[3]_1 [23]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][24] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[3]_1 [24]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][25] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[3]_1 [25]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][26] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[3]_1 [26]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][27] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[3]_1 [27]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][28] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[3]_1 [28]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][29] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[3]_1 [29]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][2] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[3]_1 [2]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][30] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[3]_1 [30]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][31] 
       (.C(clk),
        .CE(\slv_regs[3][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[3]_1 [31]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][3] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[3]_1 [3]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][4] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[3]_1 [4]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][5] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[3]_1 [5]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][6] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[3]_1 [6]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][7] 
       (.C(clk),
        .CE(\slv_regs[3][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[3]_1 [7]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][8] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[3]_1 [8]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[3][9] 
       (.C(clk),
        .CE(\slv_regs[3][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[3]_1 [9]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][0] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[4]_2 [0]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][10] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[4]_2 [10]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][11] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[4]_2 [11]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][12] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[4]_2 [12]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][13] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[4]_2 [13]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][14] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[4]_2 [14]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][15] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[4]_2 [15]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][16] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[4]_2 [16]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][17] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[4]_2 [17]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][18] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[4]_2 [18]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][19] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[4]_2 [19]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][1] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[4]_2 [1]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][20] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[4]_2 [20]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][21] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[4]_2 [21]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][22] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[4]_2 [22]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][23] 
       (.C(clk),
        .CE(\slv_regs[4][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[4]_2 [23]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][24] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[4]_2 [24]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][25] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[4]_2 [25]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][26] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[4]_2 [26]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][27] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[4]_2 [27]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][28] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[4]_2 [28]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][29] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[4]_2 [29]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][2] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[4]_2 [2]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][30] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[4]_2 [30]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][31] 
       (.C(clk),
        .CE(\slv_regs[4][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[4]_2 [31]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][3] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[4]_2 [3]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][4] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[4]_2 [4]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][5] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[4]_2 [5]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][6] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[4]_2 [6]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][7] 
       (.C(clk),
        .CE(\slv_regs[4][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[4]_2 [7]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][8] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[4]_2 [8]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[4][9] 
       (.C(clk),
        .CE(\slv_regs[4][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[4]_2 [9]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][0] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[5]_3 [0]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][10] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[5]_3 [10]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][11] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[5]_3 [11]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][12] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[5]_3 [12]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][13] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[5]_3 [13]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][14] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[5]_3 [14]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][15] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[5]_3 [15]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][16] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[5]_3 [16]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][17] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[5]_3 [17]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][18] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[5]_3 [18]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][19] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[5]_3 [19]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][1] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[5]_3 [1]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][20] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[5]_3 [20]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][21] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[5]_3 [21]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][22] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[5]_3 [22]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][23] 
       (.C(clk),
        .CE(\slv_regs[5][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[5]_3 [23]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][24] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[5]_3 [24]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][25] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[5]_3 [25]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][26] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[5]_3 [26]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][27] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[5]_3 [27]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][28] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[5]_3 [28]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][29] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[5]_3 [29]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][2] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[5]_3 [2]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][30] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[5]_3 [30]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][31] 
       (.C(clk),
        .CE(\slv_regs[5][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[5]_3 [31]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][3] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[5]_3 [3]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][4] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[5]_3 [4]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][5] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[5]_3 [5]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][6] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[5]_3 [6]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][7] 
       (.C(clk),
        .CE(\slv_regs[5][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[5]_3 [7]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][8] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[5]_3 [8]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[5][9] 
       (.C(clk),
        .CE(\slv_regs[5][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[5]_3 [9]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][0] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[6]_4 [0]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][10] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[6]_4 [10]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][11] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[6]_4 [11]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][12] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[6]_4 [12]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][13] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[6]_4 [13]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][14] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[6]_4 [14]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][15] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[6]_4 [15]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][16] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[6]_4 [16]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][17] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[6]_4 [17]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][18] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[6]_4 [18]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][19] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[6]_4 [19]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][1] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[6]_4 [1]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][20] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[6]_4 [20]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][21] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[6]_4 [21]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][22] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[6]_4 [22]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][23] 
       (.C(clk),
        .CE(\slv_regs[6][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[6]_4 [23]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][24] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[6]_4 [24]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][25] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[6]_4 [25]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][26] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[6]_4 [26]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][27] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[6]_4 [27]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][28] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[6]_4 [28]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][29] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[6]_4 [29]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][2] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[6]_4 [2]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][30] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[6]_4 [30]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][31] 
       (.C(clk),
        .CE(\slv_regs[6][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[6]_4 [31]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][3] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[6]_4 [3]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][4] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[6]_4 [4]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][5] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[6]_4 [5]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][6] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[6]_4 [6]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][7] 
       (.C(clk),
        .CE(\slv_regs[6][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[6]_4 [7]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][8] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[6]_4 [8]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[6][9] 
       (.C(clk),
        .CE(\slv_regs[6][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[6]_4 [9]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][0] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[7]_5 [0]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][10] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[7]_5 [10]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][11] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[7]_5 [11]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][12] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[7]_5 [12]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][13] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[7]_5 [13]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][14] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[7]_5 [14]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][15] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[7]_5 [15]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][16] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[7]_5 [16]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][17] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[7]_5 [17]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][18] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[7]_5 [18]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][19] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[7]_5 [19]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][1] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[7]_5 [1]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][20] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[7]_5 [20]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][21] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[7]_5 [21]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][22] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[7]_5 [22]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][23] 
       (.C(clk),
        .CE(\slv_regs[7][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[7]_5 [23]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][24] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[7]_5 [24]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][25] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[7]_5 [25]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][26] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[7]_5 [26]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][27] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[7]_5 [27]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][28] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[7]_5 [28]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][29] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[7]_5 [29]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][2] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[7]_5 [2]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][30] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[7]_5 [30]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][31] 
       (.C(clk),
        .CE(\slv_regs[7][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[7]_5 [31]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][3] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[7]_5 [3]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][4] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[7]_5 [4]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][5] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[7]_5 [5]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][6] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[7]_5 [6]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][7] 
       (.C(clk),
        .CE(\slv_regs[7][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[7]_5 [7]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][8] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[7]_5 [8]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[7][9] 
       (.C(clk),
        .CE(\slv_regs[7][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[7]_5 [9]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][0] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[8]_6 [0]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][10] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[8]_6 [10]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][11] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[8]_6 [11]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][12] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[8]_6 [12]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][13] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[8]_6 [13]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][14] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[8]_6 [14]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][15] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[8]_6 [15]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][16] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[8]_6 [16]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][17] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[8]_6 [17]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][18] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[8]_6 [18]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][19] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[8]_6 [19]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][1] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[8]_6 [1]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][20] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[8]_6 [20]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][21] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[8]_6 [21]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][22] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[8]_6 [22]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][23] 
       (.C(clk),
        .CE(\slv_regs[8][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[8]_6 [23]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][24] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[8]_6 [24]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][25] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[8]_6 [25]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][26] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[8]_6 [26]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][27] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[8]_6 [27]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][28] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[8]_6 [28]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][29] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[8]_6 [29]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][2] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[8]_6 [2]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][30] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[8]_6 [30]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][31] 
       (.C(clk),
        .CE(\slv_regs[8][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[8]_6 [31]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][3] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[8]_6 [3]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][4] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[8]_6 [4]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][5] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[8]_6 [5]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][6] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[8]_6 [6]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][7] 
       (.C(clk),
        .CE(\slv_regs[8][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[8]_6 [7]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][8] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[8]_6 [8]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[8][9] 
       (.C(clk),
        .CE(\slv_regs[8][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[8]_6 [9]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][0] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[9]_7 [0]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][10] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[9]_7 [10]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][11] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[9]_7 [11]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][12] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[9]_7 [12]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][13] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[9]_7 [13]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][14] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[9]_7 [14]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][15] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[9]_7 [15]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][16] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[9]_7 [16]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][17] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[9]_7 [17]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][18] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[9]_7 [18]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][19] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[9]_7 [19]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][1] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[9]_7 [1]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][20] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[9]_7 [20]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][21] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[9]_7 [21]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][22] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[9]_7 [22]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][23] 
       (.C(clk),
        .CE(\slv_regs[9][23]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[9]_7 [23]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][24] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[9]_7 [24]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][25] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[9]_7 [25]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][26] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[9]_7 [26]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][27] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[9]_7 [27]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][28] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[9]_7 [28]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][29] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[9]_7 [29]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][2] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[9]_7 [2]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][30] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[9]_7 [30]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][31] 
       (.C(clk),
        .CE(\slv_regs[9][31]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[9]_7 [31]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][3] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[9]_7 [3]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][4] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[9]_7 [4]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][5] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[9]_7 [5]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][6] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[9]_7 [6]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][7] 
       (.C(clk),
        .CE(\slv_regs[9][7]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[9]_7 [7]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][8] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[9]_7 [8]),
        .R(\axi_rdata_reg[0]_0 ));
  FDRE \slv_regs_reg[9][9] 
       (.C(clk),
        .CE(\slv_regs[9][15]_i_1_n_0 ),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[9]_7 [9]),
        .R(\axi_rdata_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'h00FF0800)) 
    \state[0]_i_1 
       (.I0(cpu2emsc_register[13]),
        .I1(\counter_reg[4] ),
        .I2(\state_reg[1]_0 ),
        .I3(\state[1]_i_2_n_0 ),
        .I4(\state_reg[0]_0 ),
        .O(\state_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'h00FF1500)) 
    \state[1]_i_1 
       (.I0(\state_reg[0]_0 ),
        .I1(\counter_reg[4] ),
        .I2(cpu2emsc_register[13]),
        .I3(\state[1]_i_2_n_0 ),
        .I4(\state_reg[1]_0 ),
        .O(\state_reg[1] ));
  LUT6 #(
    .INIT(64'h8000FFFF80000000)) 
    \state[1]_i_2 
       (.I0(s_axis_tvalid),
        .I1(cpu2emsc_register[12]),
        .I2(initialized_reg),
        .I3(\read_address_reg[2]_0 ),
        .I4(\state_reg[1]_0 ),
        .I5(\state[1]_i_3_n_0 ),
        .O(\state[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h8F8F0F0F8F800000)) 
    \state[1]_i_3 
       (.I0(\counter_reg[4]_0 ),
        .I1(\count_i_reg[6] ),
        .I2(\state_reg[0]_0 ),
        .I3(cpu2emsc_register[13]),
        .I4(\counter_reg[4] ),
        .I5(\state[1]_i_4_n_0 ),
        .O(\state[1]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \state[1]_i_4 
       (.I0(s_axis_tvalid),
        .I1(cpu2emsc_register[12]),
        .I2(initialized_reg),
        .O(\state[1]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h00040000)) 
    valid_input_i_1
       (.I0(p_0_in[1]),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(slv_reg_wren__2),
        .O(valid_input1_out));
  FDRE valid_input_reg
       (.C(clk),
        .CE(1'b1),
        .D(valid_input1_out),
        .Q(\counter_reg[4] ),
        .R(\axi_rdata_reg[0]_0 ));
endmodule

(* ORIG_REF_NAME = "top" *) 
module design_1_top_0_0_top
   (s_axi_ctrl_status_awready,
    s_axi_ctrl_status_wready,
    s_axi_ctrl_status_arready,
    s_axi_ctrl_status_rvalid,
    \read_address_reg[2] ,
    s_axi_ctrl_status_rdata,
    s_axi_ctrl_status_bvalid,
    m_axis_tdata,
    m_axis_tvalid,
    s_axis_tready,
    s_axi_ctrl_status_wvalid,
    s_axi_ctrl_status_awvalid,
    s_axi_ctrl_status_arvalid,
    aresetn,
    s_axis_tvalid,
    clk,
    s_axi_ctrl_status_awaddr,
    s_axi_ctrl_status_wdata,
    s_axi_ctrl_status_araddr,
    s_axi_ctrl_status_wstrb,
    S,
    s_axi_ctrl_status_bready,
    s_axi_ctrl_status_rready,
    m_axis_tready,
    s_axis_tdata,
    s_axis_tlast);
  output s_axi_ctrl_status_awready;
  output s_axi_ctrl_status_wready;
  output s_axi_ctrl_status_arready;
  output s_axi_ctrl_status_rvalid;
  output [3:0]\read_address_reg[2] ;
  output [31:0]s_axi_ctrl_status_rdata;
  output s_axi_ctrl_status_bvalid;
  output [47:0]m_axis_tdata;
  output m_axis_tvalid;
  output s_axis_tready;
  input s_axi_ctrl_status_wvalid;
  input s_axi_ctrl_status_awvalid;
  input s_axi_ctrl_status_arvalid;
  input aresetn;
  input s_axis_tvalid;
  input clk;
  input [3:0]s_axi_ctrl_status_awaddr;
  input [31:0]s_axi_ctrl_status_wdata;
  input [3:0]s_axi_ctrl_status_araddr;
  input [3:0]s_axi_ctrl_status_wstrb;
  input [1:0]S;
  input s_axi_ctrl_status_bready;
  input s_axi_ctrl_status_rready;
  input m_axis_tready;
  input [15:0]s_axis_tdata;
  input s_axis_tlast;

  wire [1:0]S;
  wire [8:1]add_r2;
  wire aresetn;
  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire b_ram_n_264;
  wire b_ram_n_265;
  wire b_ram_n_270;
  wire b_ram_n_271;
  wire b_ram_n_272;
  wire b_ram_n_273;
  wire b_ram_n_274;
  wire b_ram_n_275;
  wire b_ram_n_276;
  wire b_ram_n_277;
  wire b_ram_n_286;
  wire b_ram_n_287;
  wire b_ram_n_288;
  wire b_ram_n_289;
  wire b_ram_n_290;
  wire b_ram_n_291;
  wire b_ram_n_292;
  wire b_ram_n_293;
  wire b_ram_n_294;
  wire [255:0]b_ram_out;
  wire [7:7]b_ram_sel;
  wire clk;
  wire counter;
  wire [8:8]counter0_in;
  wire counter1;
  wire \counter[0]_i_3_n_0 ;
  wire \counter[0]_i_4_n_0 ;
  wire \counter[0]_i_5_n_0 ;
  wire \counter[0]_i_6_n_0 ;
  wire \counter[0]_i_7_n_0 ;
  wire \counter[12]_i_2_n_0 ;
  wire \counter[12]_i_3_n_0 ;
  wire \counter[12]_i_4_n_0 ;
  wire \counter[12]_i_5_n_0 ;
  wire \counter[16]_i_2_n_0 ;
  wire \counter[16]_i_3_n_0 ;
  wire \counter[16]_i_4_n_0 ;
  wire \counter[16]_i_5_n_0 ;
  wire \counter[20]_i_2_n_0 ;
  wire \counter[20]_i_3_n_0 ;
  wire \counter[20]_i_4_n_0 ;
  wire \counter[20]_i_5_n_0 ;
  wire \counter[24]_i_2_n_0 ;
  wire \counter[24]_i_3_n_0 ;
  wire \counter[24]_i_4_n_0 ;
  wire \counter[24]_i_5_n_0 ;
  wire \counter[28]_i_2_n_0 ;
  wire \counter[28]_i_3_n_0 ;
  wire \counter[28]_i_4_n_0 ;
  wire \counter[28]_i_5_n_0 ;
  wire \counter[4]_i_2__0_n_0 ;
  wire \counter[4]_i_3_n_0 ;
  wire \counter[4]_i_4_n_0 ;
  wire \counter[4]_i_5_n_0 ;
  wire \counter[8]_i_2__0_n_0 ;
  wire \counter[8]_i_3_n_0 ;
  wire \counter[8]_i_4_n_0 ;
  wire \counter[8]_i_5_n_0 ;
  wire [31:0]counter_reg;
  wire \counter_reg[0]_i_2_n_0 ;
  wire \counter_reg[0]_i_2_n_1 ;
  wire \counter_reg[0]_i_2_n_2 ;
  wire \counter_reg[0]_i_2_n_3 ;
  wire \counter_reg[0]_i_2_n_4 ;
  wire \counter_reg[0]_i_2_n_5 ;
  wire \counter_reg[0]_i_2_n_6 ;
  wire \counter_reg[0]_i_2_n_7 ;
  wire \counter_reg[12]_i_1_n_0 ;
  wire \counter_reg[12]_i_1_n_1 ;
  wire \counter_reg[12]_i_1_n_2 ;
  wire \counter_reg[12]_i_1_n_3 ;
  wire \counter_reg[12]_i_1_n_4 ;
  wire \counter_reg[12]_i_1_n_5 ;
  wire \counter_reg[12]_i_1_n_6 ;
  wire \counter_reg[12]_i_1_n_7 ;
  wire \counter_reg[16]_i_1_n_0 ;
  wire \counter_reg[16]_i_1_n_1 ;
  wire \counter_reg[16]_i_1_n_2 ;
  wire \counter_reg[16]_i_1_n_3 ;
  wire \counter_reg[16]_i_1_n_4 ;
  wire \counter_reg[16]_i_1_n_5 ;
  wire \counter_reg[16]_i_1_n_6 ;
  wire \counter_reg[16]_i_1_n_7 ;
  wire \counter_reg[20]_i_1_n_0 ;
  wire \counter_reg[20]_i_1_n_1 ;
  wire \counter_reg[20]_i_1_n_2 ;
  wire \counter_reg[20]_i_1_n_3 ;
  wire \counter_reg[20]_i_1_n_4 ;
  wire \counter_reg[20]_i_1_n_5 ;
  wire \counter_reg[20]_i_1_n_6 ;
  wire \counter_reg[20]_i_1_n_7 ;
  wire \counter_reg[24]_i_1_n_0 ;
  wire \counter_reg[24]_i_1_n_1 ;
  wire \counter_reg[24]_i_1_n_2 ;
  wire \counter_reg[24]_i_1_n_3 ;
  wire \counter_reg[24]_i_1_n_4 ;
  wire \counter_reg[24]_i_1_n_5 ;
  wire \counter_reg[24]_i_1_n_6 ;
  wire \counter_reg[24]_i_1_n_7 ;
  wire \counter_reg[28]_i_1_n_1 ;
  wire \counter_reg[28]_i_1_n_2 ;
  wire \counter_reg[28]_i_1_n_3 ;
  wire \counter_reg[28]_i_1_n_4 ;
  wire \counter_reg[28]_i_1_n_5 ;
  wire \counter_reg[28]_i_1_n_6 ;
  wire \counter_reg[28]_i_1_n_7 ;
  wire \counter_reg[4]_i_1_n_0 ;
  wire \counter_reg[4]_i_1_n_1 ;
  wire \counter_reg[4]_i_1_n_2 ;
  wire \counter_reg[4]_i_1_n_3 ;
  wire \counter_reg[4]_i_1_n_4 ;
  wire \counter_reg[4]_i_1_n_5 ;
  wire \counter_reg[4]_i_1_n_6 ;
  wire \counter_reg[4]_i_1_n_7 ;
  wire \counter_reg[8]_i_1_n_0 ;
  wire \counter_reg[8]_i_1_n_1 ;
  wire \counter_reg[8]_i_1_n_2 ;
  wire \counter_reg[8]_i_1_n_3 ;
  wire \counter_reg[8]_i_1_n_4 ;
  wire \counter_reg[8]_i_1_n_5 ;
  wire \counter_reg[8]_i_1_n_6 ;
  wire \counter_reg[8]_i_1_n_7 ;
  wire dp_enable;
  wire dp_extend_end;
  wire dp_extend_end0;
  wire dp_extend_end0_carry__0_i_1_n_0;
  wire dp_extend_end0_carry__0_i_2_n_0;
  wire dp_extend_end0_carry__0_i_3_n_0;
  wire dp_extend_end0_carry__0_i_4_n_0;
  wire dp_extend_end0_carry__0_n_0;
  wire dp_extend_end0_carry__0_n_1;
  wire dp_extend_end0_carry__0_n_2;
  wire dp_extend_end0_carry__0_n_3;
  wire dp_extend_end0_carry__1_i_1_n_0;
  wire dp_extend_end0_carry__1_i_2_n_0;
  wire dp_extend_end0_carry__1_i_3_n_0;
  wire dp_extend_end0_carry__1_n_2;
  wire dp_extend_end0_carry__1_n_3;
  wire dp_extend_end0_carry_i_1_n_0;
  wire dp_extend_end0_carry_i_2_n_0;
  wire dp_extend_end0_carry_i_3_n_0;
  wire dp_extend_end0_carry_i_4_n_0;
  wire dp_extend_end0_carry_n_0;
  wire dp_extend_end0_carry_n_1;
  wire dp_extend_end0_carry_n_2;
  wire dp_extend_end0_carry_n_3;
  wire [31:1]dp_extend_end1;
  wire dp_extend_end1_carry__0_n_0;
  wire dp_extend_end1_carry__0_n_1;
  wire dp_extend_end1_carry__0_n_2;
  wire dp_extend_end1_carry__0_n_3;
  wire dp_extend_end1_carry__1_n_0;
  wire dp_extend_end1_carry__1_n_1;
  wire dp_extend_end1_carry__1_n_2;
  wire dp_extend_end1_carry__1_n_3;
  wire dp_extend_end1_carry__2_n_0;
  wire dp_extend_end1_carry__2_n_1;
  wire dp_extend_end1_carry__2_n_2;
  wire dp_extend_end1_carry__2_n_3;
  wire dp_extend_end1_carry__3_n_0;
  wire dp_extend_end1_carry__3_n_1;
  wire dp_extend_end1_carry__3_n_2;
  wire dp_extend_end1_carry__3_n_3;
  wire dp_extend_end1_carry__4_n_0;
  wire dp_extend_end1_carry__4_n_1;
  wire dp_extend_end1_carry__4_n_2;
  wire dp_extend_end1_carry__4_n_3;
  wire dp_extend_end1_carry__5_n_0;
  wire dp_extend_end1_carry__5_n_1;
  wire dp_extend_end1_carry__5_n_2;
  wire dp_extend_end1_carry__5_n_3;
  wire dp_extend_end1_carry__6_n_2;
  wire dp_extend_end1_carry__6_n_3;
  wire dp_extend_end1_carry_n_0;
  wire dp_extend_end1_carry_n_1;
  wire dp_extend_end1_carry_n_2;
  wire dp_extend_end1_carry_n_3;
  wire dp_extend_end_i_1_n_0;
  wire dp_n_384;
  wire dp_n_385;
  wire dp_n_386;
  wire dp_n_387;
  wire dp_n_388;
  wire dp_n_389;
  wire dp_n_390;
  wire dp_n_391;
  wire dp_n_392;
  wire gb_n_2;
  wire [14:0]in_raw_delay;
  wire \in_raw_delay_reg[15]_rep__0_n_0 ;
  wire \in_raw_delay_reg[15]_rep__10_n_0 ;
  wire \in_raw_delay_reg[15]_rep__1_n_0 ;
  wire \in_raw_delay_reg[15]_rep__2_n_0 ;
  wire \in_raw_delay_reg[15]_rep__3_n_0 ;
  wire \in_raw_delay_reg[15]_rep__4_n_0 ;
  wire \in_raw_delay_reg[15]_rep__5_n_0 ;
  wire \in_raw_delay_reg[15]_rep__6_n_0 ;
  wire \in_raw_delay_reg[15]_rep__7_n_0 ;
  wire \in_raw_delay_reg[15]_rep__8_n_0 ;
  wire \in_raw_delay_reg[15]_rep__9_n_0 ;
  wire \in_raw_delay_reg[15]_rep_n_0 ;
  wire initialized;
  wire initialized_i_1_n_0;
  wire [47:0]m_axis_tdata;
  wire m_axis_tready;
  wire m_axis_tvalid;
  wire p_0_in;
  wire [383:0]p_out;
  wire p_rdy;
  wire [3:0]\read_address_reg[2] ;
  wire \register_interface/p_0_in__0 ;
  wire res_mem0;
  wire [3:0]s_axi_ctrl_status_araddr;
  wire s_axi_ctrl_status_arready;
  wire s_axi_ctrl_status_arvalid;
  wire [3:0]s_axi_ctrl_status_awaddr;
  wire s_axi_ctrl_status_awready;
  wire s_axi_ctrl_status_awvalid;
  wire s_axi_ctrl_status_bready;
  wire s_axi_ctrl_status_bvalid;
  wire [31:0]s_axi_ctrl_status_rdata;
  wire s_axi_ctrl_status_rready;
  wire s_axi_ctrl_status_rvalid;
  wire [31:0]s_axi_ctrl_status_wdata;
  wire s_axi_ctrl_status_wready;
  wire [3:0]s_axi_ctrl_status_wstrb;
  wire s_axi_ctrl_status_wvalid;
  wire [15:0]s_axis_tdata;
  wire s_axis_tlast;
  wire s_axis_tready;
  wire s_axis_tvalid;
  wire start;
  wire [0:0]v_len;
  wire valid_input;
  wire [3:3]\NLW_counter_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:0]NLW_dp_extend_end0_carry_O_UNCONNECTED;
  wire [3:0]NLW_dp_extend_end0_carry__0_O_UNCONNECTED;
  wire [3:3]NLW_dp_extend_end0_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_dp_extend_end0_carry__1_O_UNCONNECTED;
  wire [3:2]NLW_dp_extend_end1_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_dp_extend_end1_carry__6_O_UNCONNECTED;

  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s_axi_ctrl_status_wready),
        .I1(s_axi_ctrl_status_wvalid),
        .I2(s_axi_ctrl_status_awready),
        .I3(s_axi_ctrl_status_awvalid),
        .I4(s_axi_ctrl_status_bready),
        .I5(s_axi_ctrl_status_bvalid),
        .O(axi_bvalid_i_1_n_0));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s_axi_ctrl_status_arvalid),
        .I1(s_axi_ctrl_status_arready),
        .I2(s_axi_ctrl_status_rvalid),
        .I3(s_axi_ctrl_status_rready),
        .O(axi_rvalid_i_1_n_0));
  design_1_top_0_0_b_ram_bank b_ram
       (.CO(dp_n_384),
        .O(b_ram_n_293),
        .Q(v_len),
        .S(b_ram_n_277),
        .SR(counter0_in),
        .aresetn(aresetn),
        .axi_arready_reg(axi_rvalid_i_1_n_0),
        .axi_wready_reg(axi_bvalid_i_1_n_0),
        .b_ram_out(b_ram_out),
        .\b_ram_sel_reg[7]_0 (initialized_i_1_n_0),
        .clk(clk),
        .\count_i_reg[6] (b_ram_n_265),
        .\counter_reg[1]_0 (add_r2),
        .\counter_reg[1]_1 (b_ram_n_286),
        .\counter_reg[1]_2 (b_ram_n_287),
        .\counter_reg[1]_3 (b_ram_n_288),
        .\counter_reg[1]_4 (b_ram_n_289),
        .\counter_reg[1]_5 (b_ram_n_290),
        .\counter_reg[1]_6 (b_ram_n_291),
        .\counter_reg[1]_7 (b_ram_n_292),
        .\counter_reg[4]_0 (b_ram_n_264),
        .\counter_reg[4]_1 (b_ram_sel),
        .\counter_reg[4]_2 (counter1),
        .\counter_reg[8] (b_ram_n_270),
        .\counter_reg[8]_0 (b_ram_n_271),
        .\counter_reg[8]_1 (b_ram_n_272),
        .\counter_reg[8]_2 (b_ram_n_273),
        .\counter_reg[8]_3 (b_ram_n_274),
        .\counter_reg[8]_4 (b_ram_n_275),
        .\counter_reg[8]_5 (b_ram_n_276),
        .dp_enable(dp_enable),
        .dp_extend_end(dp_extend_end),
        .in_stream_ready_reg(b_ram_n_294),
        .initialized(initialized),
        .p_0_in__0(\register_interface/p_0_in__0 ),
        .\read_address_reg[2]_0 (\read_address_reg[2] ),
        .s_axi_ctrl_status_araddr(s_axi_ctrl_status_araddr),
        .s_axi_ctrl_status_arready(s_axi_ctrl_status_arready),
        .s_axi_ctrl_status_arvalid(s_axi_ctrl_status_arvalid),
        .s_axi_ctrl_status_awaddr(s_axi_ctrl_status_awaddr),
        .s_axi_ctrl_status_awready(s_axi_ctrl_status_awready),
        .s_axi_ctrl_status_awvalid(s_axi_ctrl_status_awvalid),
        .s_axi_ctrl_status_bvalid(s_axi_ctrl_status_bvalid),
        .s_axi_ctrl_status_rdata(s_axi_ctrl_status_rdata),
        .s_axi_ctrl_status_rvalid(s_axi_ctrl_status_rvalid),
        .s_axi_ctrl_status_wdata(s_axi_ctrl_status_wdata),
        .s_axi_ctrl_status_wready(s_axi_ctrl_status_wready),
        .s_axi_ctrl_status_wstrb(s_axi_ctrl_status_wstrb),
        .s_axi_ctrl_status_wvalid(s_axi_ctrl_status_wvalid),
        .s_axis_tready(s_axis_tready),
        .s_axis_tvalid(s_axis_tvalid),
        .\slv_regs_reg[0][11] (dp_n_385),
        .\slv_regs_reg[0][11]_0 (dp_n_386),
        .\slv_regs_reg[0][11]_1 (dp_n_387),
        .\slv_regs_reg[0][11]_2 (dp_n_388),
        .\slv_regs_reg[0][11]_3 (dp_n_389),
        .\slv_regs_reg[0][11]_4 (dp_n_390),
        .\slv_regs_reg[0][11]_5 (dp_n_391),
        .\slv_regs_reg[0][11]_6 (S),
        .valid_input(valid_input));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[0]_i_1__1 
       (.I0(dp_extend_end),
        .I1(s_axis_tlast),
        .O(counter));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[0]_i_3 
       (.I0(counter_reg[0]),
        .I1(dp_extend_end0),
        .O(\counter[0]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[0]_i_4 
       (.I0(counter_reg[3]),
        .I1(dp_extend_end0),
        .O(\counter[0]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[0]_i_5 
       (.I0(counter_reg[2]),
        .I1(dp_extend_end0),
        .O(\counter[0]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[0]_i_6 
       (.I0(counter_reg[1]),
        .I1(dp_extend_end0),
        .O(\counter[0]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \counter[0]_i_7 
       (.I0(counter_reg[0]),
        .I1(dp_extend_end0),
        .O(\counter[0]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[12]_i_2 
       (.I0(counter_reg[15]),
        .I1(dp_extend_end0),
        .O(\counter[12]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[12]_i_3 
       (.I0(counter_reg[14]),
        .I1(dp_extend_end0),
        .O(\counter[12]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[12]_i_4 
       (.I0(counter_reg[13]),
        .I1(dp_extend_end0),
        .O(\counter[12]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[12]_i_5 
       (.I0(counter_reg[12]),
        .I1(dp_extend_end0),
        .O(\counter[12]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[16]_i_2 
       (.I0(counter_reg[19]),
        .I1(dp_extend_end0),
        .O(\counter[16]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[16]_i_3 
       (.I0(counter_reg[18]),
        .I1(dp_extend_end0),
        .O(\counter[16]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[16]_i_4 
       (.I0(counter_reg[17]),
        .I1(dp_extend_end0),
        .O(\counter[16]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[16]_i_5 
       (.I0(counter_reg[16]),
        .I1(dp_extend_end0),
        .O(\counter[16]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[20]_i_2 
       (.I0(counter_reg[23]),
        .I1(dp_extend_end0),
        .O(\counter[20]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[20]_i_3 
       (.I0(counter_reg[22]),
        .I1(dp_extend_end0),
        .O(\counter[20]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[20]_i_4 
       (.I0(counter_reg[21]),
        .I1(dp_extend_end0),
        .O(\counter[20]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[20]_i_5 
       (.I0(counter_reg[20]),
        .I1(dp_extend_end0),
        .O(\counter[20]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[24]_i_2 
       (.I0(counter_reg[27]),
        .I1(dp_extend_end0),
        .O(\counter[24]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[24]_i_3 
       (.I0(counter_reg[26]),
        .I1(dp_extend_end0),
        .O(\counter[24]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[24]_i_4 
       (.I0(counter_reg[25]),
        .I1(dp_extend_end0),
        .O(\counter[24]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[24]_i_5 
       (.I0(counter_reg[24]),
        .I1(dp_extend_end0),
        .O(\counter[24]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[28]_i_2 
       (.I0(counter_reg[31]),
        .I1(dp_extend_end0),
        .O(\counter[28]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[28]_i_3 
       (.I0(counter_reg[30]),
        .I1(dp_extend_end0),
        .O(\counter[28]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[28]_i_4 
       (.I0(counter_reg[29]),
        .I1(dp_extend_end0),
        .O(\counter[28]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[28]_i_5 
       (.I0(counter_reg[28]),
        .I1(dp_extend_end0),
        .O(\counter[28]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[4]_i_2__0 
       (.I0(counter_reg[7]),
        .I1(dp_extend_end0),
        .O(\counter[4]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[4]_i_3 
       (.I0(counter_reg[6]),
        .I1(dp_extend_end0),
        .O(\counter[4]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[4]_i_4 
       (.I0(counter_reg[5]),
        .I1(dp_extend_end0),
        .O(\counter[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[4]_i_5 
       (.I0(counter_reg[4]),
        .I1(dp_extend_end0),
        .O(\counter[4]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[8]_i_2__0 
       (.I0(counter_reg[11]),
        .I1(dp_extend_end0),
        .O(\counter[8]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[8]_i_3 
       (.I0(counter_reg[10]),
        .I1(dp_extend_end0),
        .O(\counter[8]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[8]_i_4 
       (.I0(counter_reg[9]),
        .I1(dp_extend_end0),
        .O(\counter[8]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[8]_i_5 
       (.I0(counter_reg[8]),
        .I1(dp_extend_end0),
        .O(\counter[8]_i_5_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[0] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[0]_i_2_n_7 ),
        .Q(counter_reg[0]));
  CARRY4 \counter_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\counter_reg[0]_i_2_n_0 ,\counter_reg[0]_i_2_n_1 ,\counter_reg[0]_i_2_n_2 ,\counter_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\counter[0]_i_3_n_0 }),
        .O({\counter_reg[0]_i_2_n_4 ,\counter_reg[0]_i_2_n_5 ,\counter_reg[0]_i_2_n_6 ,\counter_reg[0]_i_2_n_7 }),
        .S({\counter[0]_i_4_n_0 ,\counter[0]_i_5_n_0 ,\counter[0]_i_6_n_0 ,\counter[0]_i_7_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[10] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[8]_i_1_n_5 ),
        .Q(counter_reg[10]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[11] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[8]_i_1_n_4 ),
        .Q(counter_reg[11]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[12] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[12]_i_1_n_7 ),
        .Q(counter_reg[12]));
  CARRY4 \counter_reg[12]_i_1 
       (.CI(\counter_reg[8]_i_1_n_0 ),
        .CO({\counter_reg[12]_i_1_n_0 ,\counter_reg[12]_i_1_n_1 ,\counter_reg[12]_i_1_n_2 ,\counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_reg[12]_i_1_n_4 ,\counter_reg[12]_i_1_n_5 ,\counter_reg[12]_i_1_n_6 ,\counter_reg[12]_i_1_n_7 }),
        .S({\counter[12]_i_2_n_0 ,\counter[12]_i_3_n_0 ,\counter[12]_i_4_n_0 ,\counter[12]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[13] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[12]_i_1_n_6 ),
        .Q(counter_reg[13]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[14] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[12]_i_1_n_5 ),
        .Q(counter_reg[14]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[15] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[12]_i_1_n_4 ),
        .Q(counter_reg[15]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[16] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[16]_i_1_n_7 ),
        .Q(counter_reg[16]));
  CARRY4 \counter_reg[16]_i_1 
       (.CI(\counter_reg[12]_i_1_n_0 ),
        .CO({\counter_reg[16]_i_1_n_0 ,\counter_reg[16]_i_1_n_1 ,\counter_reg[16]_i_1_n_2 ,\counter_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_reg[16]_i_1_n_4 ,\counter_reg[16]_i_1_n_5 ,\counter_reg[16]_i_1_n_6 ,\counter_reg[16]_i_1_n_7 }),
        .S({\counter[16]_i_2_n_0 ,\counter[16]_i_3_n_0 ,\counter[16]_i_4_n_0 ,\counter[16]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[17] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[16]_i_1_n_6 ),
        .Q(counter_reg[17]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[18] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[16]_i_1_n_5 ),
        .Q(counter_reg[18]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[19] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[16]_i_1_n_4 ),
        .Q(counter_reg[19]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[1] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[0]_i_2_n_6 ),
        .Q(counter_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[20] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[20]_i_1_n_7 ),
        .Q(counter_reg[20]));
  CARRY4 \counter_reg[20]_i_1 
       (.CI(\counter_reg[16]_i_1_n_0 ),
        .CO({\counter_reg[20]_i_1_n_0 ,\counter_reg[20]_i_1_n_1 ,\counter_reg[20]_i_1_n_2 ,\counter_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_reg[20]_i_1_n_4 ,\counter_reg[20]_i_1_n_5 ,\counter_reg[20]_i_1_n_6 ,\counter_reg[20]_i_1_n_7 }),
        .S({\counter[20]_i_2_n_0 ,\counter[20]_i_3_n_0 ,\counter[20]_i_4_n_0 ,\counter[20]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[21] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[20]_i_1_n_6 ),
        .Q(counter_reg[21]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[22] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[20]_i_1_n_5 ),
        .Q(counter_reg[22]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[23] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[20]_i_1_n_4 ),
        .Q(counter_reg[23]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[24] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[24]_i_1_n_7 ),
        .Q(counter_reg[24]));
  CARRY4 \counter_reg[24]_i_1 
       (.CI(\counter_reg[20]_i_1_n_0 ),
        .CO({\counter_reg[24]_i_1_n_0 ,\counter_reg[24]_i_1_n_1 ,\counter_reg[24]_i_1_n_2 ,\counter_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_reg[24]_i_1_n_4 ,\counter_reg[24]_i_1_n_5 ,\counter_reg[24]_i_1_n_6 ,\counter_reg[24]_i_1_n_7 }),
        .S({\counter[24]_i_2_n_0 ,\counter[24]_i_3_n_0 ,\counter[24]_i_4_n_0 ,\counter[24]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[25] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[24]_i_1_n_6 ),
        .Q(counter_reg[25]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[26] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[24]_i_1_n_5 ),
        .Q(counter_reg[26]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[27] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[24]_i_1_n_4 ),
        .Q(counter_reg[27]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[28] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[28]_i_1_n_7 ),
        .Q(counter_reg[28]));
  CARRY4 \counter_reg[28]_i_1 
       (.CI(\counter_reg[24]_i_1_n_0 ),
        .CO({\NLW_counter_reg[28]_i_1_CO_UNCONNECTED [3],\counter_reg[28]_i_1_n_1 ,\counter_reg[28]_i_1_n_2 ,\counter_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_reg[28]_i_1_n_4 ,\counter_reg[28]_i_1_n_5 ,\counter_reg[28]_i_1_n_6 ,\counter_reg[28]_i_1_n_7 }),
        .S({\counter[28]_i_2_n_0 ,\counter[28]_i_3_n_0 ,\counter[28]_i_4_n_0 ,\counter[28]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[29] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[28]_i_1_n_6 ),
        .Q(counter_reg[29]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[2] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[0]_i_2_n_5 ),
        .Q(counter_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[30] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[28]_i_1_n_5 ),
        .Q(counter_reg[30]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[31] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[28]_i_1_n_4 ),
        .Q(counter_reg[31]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[3] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[0]_i_2_n_4 ),
        .Q(counter_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[4] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[4]_i_1_n_7 ),
        .Q(counter_reg[4]));
  CARRY4 \counter_reg[4]_i_1 
       (.CI(\counter_reg[0]_i_2_n_0 ),
        .CO({\counter_reg[4]_i_1_n_0 ,\counter_reg[4]_i_1_n_1 ,\counter_reg[4]_i_1_n_2 ,\counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_reg[4]_i_1_n_4 ,\counter_reg[4]_i_1_n_5 ,\counter_reg[4]_i_1_n_6 ,\counter_reg[4]_i_1_n_7 }),
        .S({\counter[4]_i_2__0_n_0 ,\counter[4]_i_3_n_0 ,\counter[4]_i_4_n_0 ,\counter[4]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[5] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[4]_i_1_n_6 ),
        .Q(counter_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[6] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[4]_i_1_n_5 ),
        .Q(counter_reg[6]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[7] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[4]_i_1_n_4 ),
        .Q(counter_reg[7]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[8] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[8]_i_1_n_7 ),
        .Q(counter_reg[8]));
  CARRY4 \counter_reg[8]_i_1 
       (.CI(\counter_reg[4]_i_1_n_0 ),
        .CO({\counter_reg[8]_i_1_n_0 ,\counter_reg[8]_i_1_n_1 ,\counter_reg[8]_i_1_n_2 ,\counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_reg[8]_i_1_n_4 ,\counter_reg[8]_i_1_n_5 ,\counter_reg[8]_i_1_n_6 ,\counter_reg[8]_i_1_n_7 }),
        .S({\counter[8]_i_2__0_n_0 ,\counter[8]_i_3_n_0 ,\counter[8]_i_4_n_0 ,\counter[8]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[9] 
       (.C(clk),
        .CE(counter),
        .CLR(\register_interface/p_0_in__0 ),
        .D(\counter_reg[8]_i_1_n_6 ),
        .Q(counter_reg[9]));
  design_1_top_0_0_dot_product_module dp
       (.A({\in_raw_delay_reg[15]_rep_n_0 ,in_raw_delay}),
        .B(\in_raw_delay_reg[15]_rep__10_n_0 ),
        .CO(dp_n_384),
        .D(p_out),
        .E(res_mem0),
        .O(b_ram_n_293),
        .Q(gb_n_2),
        .S(b_ram_n_277),
        .SR(counter0_in),
        .aresetn(aresetn),
        .b_ram_out(b_ram_out),
        .clk(clk),
        .\counter_reg[1] (dp_n_385),
        .\counter_reg[1]_0 (dp_n_386),
        .\counter_reg[1]_1 (dp_n_387),
        .\counter_reg[1]_2 (dp_n_388),
        .\counter_reg[1]_3 (dp_n_389),
        .\counter_reg[1]_4 (dp_n_390),
        .\counter_reg[1]_5 (dp_n_391),
        .\counter_reg[3] (p_0_in),
        .dp_enable(dp_enable),
        .\in_raw_delay_reg[15]_rep__0 (\in_raw_delay_reg[15]_rep__0_n_0 ),
        .\in_raw_delay_reg[15]_rep__2 ({\in_raw_delay_reg[15]_rep__2_n_0 ,\in_raw_delay_reg[15]_rep__1_n_0 }),
        .\in_raw_delay_reg[15]_rep__4 ({\in_raw_delay_reg[15]_rep__4_n_0 ,\in_raw_delay_reg[15]_rep__3_n_0 }),
        .\in_raw_delay_reg[15]_rep__6 ({\in_raw_delay_reg[15]_rep__6_n_0 ,\in_raw_delay_reg[15]_rep__5_n_0 }),
        .\in_raw_delay_reg[15]_rep__7 (\in_raw_delay_reg[15]_rep__7_n_0 ),
        .\in_raw_delay_reg[15]_rep__8 (\in_raw_delay_reg[15]_rep__8_n_0 ),
        .\in_raw_delay_reg[15]_rep__9 (\in_raw_delay_reg[15]_rep__9_n_0 ),
        .p_0_in__0(\register_interface/p_0_in__0 ),
        .p_rdy(p_rdy),
        .\slv_regs_reg[0][0] (v_len),
        .\slv_regs_reg[0][11] (b_ram_n_286),
        .\slv_regs_reg[0][11]_0 (b_ram_n_287),
        .\slv_regs_reg[0][11]_1 (b_ram_n_288),
        .\slv_regs_reg[0][11]_2 (b_ram_n_289),
        .\slv_regs_reg[0][11]_3 (b_ram_n_290),
        .\slv_regs_reg[0][11]_4 (b_ram_n_291),
        .\slv_regs_reg[0][11]_5 (b_ram_n_292),
        .\slv_regs_reg[0][12] (b_ram_n_270),
        .\slv_regs_reg[0][12]_0 (b_ram_n_271),
        .\slv_regs_reg[0][12]_1 (b_ram_n_272),
        .\slv_regs_reg[0][12]_2 (b_ram_n_273),
        .\slv_regs_reg[0][12]_3 (b_ram_n_274),
        .\slv_regs_reg[0][12]_4 (b_ram_n_275),
        .\slv_regs_reg[0][12]_5 (b_ram_n_276),
        .\slv_regs_reg[0][8] (add_r2),
        .start(start),
        .start_reg(dp_n_392));
  CARRY4 dp_extend_end0_carry
       (.CI(1'b0),
        .CO({dp_extend_end0_carry_n_0,dp_extend_end0_carry_n_1,dp_extend_end0_carry_n_2,dp_extend_end0_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_dp_extend_end0_carry_O_UNCONNECTED[3:0]),
        .S({dp_extend_end0_carry_i_1_n_0,dp_extend_end0_carry_i_2_n_0,dp_extend_end0_carry_i_3_n_0,dp_extend_end0_carry_i_4_n_0}));
  CARRY4 dp_extend_end0_carry__0
       (.CI(dp_extend_end0_carry_n_0),
        .CO({dp_extend_end0_carry__0_n_0,dp_extend_end0_carry__0_n_1,dp_extend_end0_carry__0_n_2,dp_extend_end0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_dp_extend_end0_carry__0_O_UNCONNECTED[3:0]),
        .S({dp_extend_end0_carry__0_i_1_n_0,dp_extend_end0_carry__0_i_2_n_0,dp_extend_end0_carry__0_i_3_n_0,dp_extend_end0_carry__0_i_4_n_0}));
  LUT3 #(
    .INIT(8'h01)) 
    dp_extend_end0_carry__0_i_1
       (.I0(dp_extend_end1[23]),
        .I1(dp_extend_end1[22]),
        .I2(dp_extend_end1[21]),
        .O(dp_extend_end0_carry__0_i_1_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    dp_extend_end0_carry__0_i_2
       (.I0(dp_extend_end1[20]),
        .I1(dp_extend_end1[19]),
        .I2(dp_extend_end1[18]),
        .O(dp_extend_end0_carry__0_i_2_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    dp_extend_end0_carry__0_i_3
       (.I0(dp_extend_end1[17]),
        .I1(dp_extend_end1[16]),
        .I2(dp_extend_end1[15]),
        .O(dp_extend_end0_carry__0_i_3_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    dp_extend_end0_carry__0_i_4
       (.I0(dp_extend_end1[14]),
        .I1(dp_extend_end1[13]),
        .I2(dp_extend_end1[12]),
        .O(dp_extend_end0_carry__0_i_4_n_0));
  CARRY4 dp_extend_end0_carry__1
       (.CI(dp_extend_end0_carry__0_n_0),
        .CO({NLW_dp_extend_end0_carry__1_CO_UNCONNECTED[3],dp_extend_end0,dp_extend_end0_carry__1_n_2,dp_extend_end0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_dp_extend_end0_carry__1_O_UNCONNECTED[3:0]),
        .S({1'b0,dp_extend_end0_carry__1_i_1_n_0,dp_extend_end0_carry__1_i_2_n_0,dp_extend_end0_carry__1_i_3_n_0}));
  LUT2 #(
    .INIT(4'h1)) 
    dp_extend_end0_carry__1_i_1
       (.I0(dp_extend_end1[31]),
        .I1(dp_extend_end1[30]),
        .O(dp_extend_end0_carry__1_i_1_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    dp_extend_end0_carry__1_i_2
       (.I0(dp_extend_end1[29]),
        .I1(dp_extend_end1[28]),
        .I2(dp_extend_end1[27]),
        .O(dp_extend_end0_carry__1_i_2_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    dp_extend_end0_carry__1_i_3
       (.I0(dp_extend_end1[26]),
        .I1(dp_extend_end1[25]),
        .I2(dp_extend_end1[24]),
        .O(dp_extend_end0_carry__1_i_3_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    dp_extend_end0_carry_i_1
       (.I0(dp_extend_end1[11]),
        .I1(dp_extend_end1[10]),
        .I2(dp_extend_end1[9]),
        .O(dp_extend_end0_carry_i_1_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    dp_extend_end0_carry_i_2
       (.I0(dp_extend_end1[8]),
        .I1(dp_extend_end1[7]),
        .I2(dp_extend_end1[6]),
        .O(dp_extend_end0_carry_i_2_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    dp_extend_end0_carry_i_3
       (.I0(dp_extend_end1[5]),
        .I1(dp_extend_end1[4]),
        .I2(dp_extend_end1[3]),
        .O(dp_extend_end0_carry_i_3_n_0));
  LUT3 #(
    .INIT(8'h10)) 
    dp_extend_end0_carry_i_4
       (.I0(counter_reg[0]),
        .I1(dp_extend_end1[2]),
        .I2(dp_extend_end1[1]),
        .O(dp_extend_end0_carry_i_4_n_0));
  CARRY4 dp_extend_end1_carry
       (.CI(1'b0),
        .CO({dp_extend_end1_carry_n_0,dp_extend_end1_carry_n_1,dp_extend_end1_carry_n_2,dp_extend_end1_carry_n_3}),
        .CYINIT(counter_reg[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(dp_extend_end1[4:1]),
        .S(counter_reg[4:1]));
  CARRY4 dp_extend_end1_carry__0
       (.CI(dp_extend_end1_carry_n_0),
        .CO({dp_extend_end1_carry__0_n_0,dp_extend_end1_carry__0_n_1,dp_extend_end1_carry__0_n_2,dp_extend_end1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(dp_extend_end1[8:5]),
        .S(counter_reg[8:5]));
  CARRY4 dp_extend_end1_carry__1
       (.CI(dp_extend_end1_carry__0_n_0),
        .CO({dp_extend_end1_carry__1_n_0,dp_extend_end1_carry__1_n_1,dp_extend_end1_carry__1_n_2,dp_extend_end1_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(dp_extend_end1[12:9]),
        .S(counter_reg[12:9]));
  CARRY4 dp_extend_end1_carry__2
       (.CI(dp_extend_end1_carry__1_n_0),
        .CO({dp_extend_end1_carry__2_n_0,dp_extend_end1_carry__2_n_1,dp_extend_end1_carry__2_n_2,dp_extend_end1_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(dp_extend_end1[16:13]),
        .S(counter_reg[16:13]));
  CARRY4 dp_extend_end1_carry__3
       (.CI(dp_extend_end1_carry__2_n_0),
        .CO({dp_extend_end1_carry__3_n_0,dp_extend_end1_carry__3_n_1,dp_extend_end1_carry__3_n_2,dp_extend_end1_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(dp_extend_end1[20:17]),
        .S(counter_reg[20:17]));
  CARRY4 dp_extend_end1_carry__4
       (.CI(dp_extend_end1_carry__3_n_0),
        .CO({dp_extend_end1_carry__4_n_0,dp_extend_end1_carry__4_n_1,dp_extend_end1_carry__4_n_2,dp_extend_end1_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(dp_extend_end1[24:21]),
        .S(counter_reg[24:21]));
  CARRY4 dp_extend_end1_carry__5
       (.CI(dp_extend_end1_carry__4_n_0),
        .CO({dp_extend_end1_carry__5_n_0,dp_extend_end1_carry__5_n_1,dp_extend_end1_carry__5_n_2,dp_extend_end1_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(dp_extend_end1[28:25]),
        .S(counter_reg[28:25]));
  CARRY4 dp_extend_end1_carry__6
       (.CI(dp_extend_end1_carry__5_n_0),
        .CO({NLW_dp_extend_end1_carry__6_CO_UNCONNECTED[3:2],dp_extend_end1_carry__6_n_2,dp_extend_end1_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_dp_extend_end1_carry__6_O_UNCONNECTED[3],dp_extend_end1[31:29]}),
        .S({1'b0,counter_reg[31:29]}));
  LUT4 #(
    .INIT(16'hAECC)) 
    dp_extend_end_i_1
       (.I0(s_axis_tlast),
        .I1(dp_extend_end),
        .I2(dp_extend_end0),
        .I3(aresetn),
        .O(dp_extend_end_i_1_n_0));
  FDRE dp_extend_end_reg
       (.C(clk),
        .CE(1'b1),
        .D(dp_extend_end_i_1_n_0),
        .Q(dp_extend_end),
        .R(1'b0));
  design_1_top_0_0_axi_gearbox gb
       (.D(p_out),
        .E(p_0_in),
        .Q(gb_n_2),
        .clk(clk),
        .m_axis_tdata(m_axis_tdata),
        .m_axis_tready(m_axis_tready),
        .m_axis_tvalid(m_axis_tvalid),
        .p_0_in__0(\register_interface/p_0_in__0 ),
        .p_rdy(p_rdy),
        .\slv_regs_reg[0][12] (res_mem0),
        .start(start),
        .start_reg_0(dp_n_392));
  FDCE \in_raw_delay_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[0]),
        .Q(in_raw_delay[0]));
  FDCE \in_raw_delay_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[10]),
        .Q(in_raw_delay[10]));
  FDCE \in_raw_delay_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[11]),
        .Q(in_raw_delay[11]));
  FDCE \in_raw_delay_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[12]),
        .Q(in_raw_delay[12]));
  FDCE \in_raw_delay_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[13]),
        .Q(in_raw_delay[13]));
  FDCE \in_raw_delay_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[14]),
        .Q(in_raw_delay[14]));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__0 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__0_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__1 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__1_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__10 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__10_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__2 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__2_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__3 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__3_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__4 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__4_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__5 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__5_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__6 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__6_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__7 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__7_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__8 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__8_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__9 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__9_n_0 ));
  FDCE \in_raw_delay_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[1]),
        .Q(in_raw_delay[1]));
  FDCE \in_raw_delay_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[2]),
        .Q(in_raw_delay[2]));
  FDCE \in_raw_delay_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[3]),
        .Q(in_raw_delay[3]));
  FDCE \in_raw_delay_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[4]),
        .Q(in_raw_delay[4]));
  FDCE \in_raw_delay_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[5]),
        .Q(in_raw_delay[5]));
  FDCE \in_raw_delay_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[6]),
        .Q(in_raw_delay[6]));
  FDCE \in_raw_delay_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[7]),
        .Q(in_raw_delay[7]));
  FDCE \in_raw_delay_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[8]),
        .Q(in_raw_delay[8]));
  FDCE \in_raw_delay_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(s_axis_tdata[9]),
        .Q(in_raw_delay[9]));
  FDCE in_stream_ready_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(\register_interface/p_0_in__0 ),
        .D(b_ram_n_294),
        .Q(s_axis_tready));
  LUT6 #(
    .INIT(64'hFFFFFFFF00800000)) 
    initialized_i_1
       (.I0(b_ram_sel),
        .I1(valid_input),
        .I2(b_ram_n_264),
        .I3(b_ram_n_265),
        .I4(counter1),
        .I5(initialized),
        .O(initialized_i_1_n_0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
