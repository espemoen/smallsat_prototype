library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


entity b_ram_bank is
  Generic(
          B_RAM_SIZE      : integer := 100;
          B_RAM_BIT_WIDTH : integer := 32;
          NUM_B_RAM       : integer := 8;
          P_BIT_WIDTH       : integer := 64;
          C_S_AXI_DATA_WIDTH : integer := 64;
          C_S_AXI_ADDR_WIDTH : integer := 8
       );
  Port ( 
        clk             :       in std_logic;
        aresetn         :       in std_logic;
        read_enable     :       in std_logic;
        enable          :       out std_logic;
--      valid_input     :       in std_logic;

        v_len           :       out std_logic_vector(11 downto 0);
        init_flag       :       out std_logic;
        data_out        :       out std_logic_vector(B_RAM_BIT_WIDTH*NUM_B_RAM-1 downto 0);
        
        p_out_reg       :       in std_logic_vector(NUM_B_RAM*64-1 downto 0);
        last_p          :       in std_logic;
        p_rdy           :       in std_logic;
        -- Register interface
        s_axi_ctrl_status_awaddr  : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
        s_axi_ctrl_status_awprot  : in  std_logic_vector(2 downto 0);
        s_axi_ctrl_status_awvalid : in  std_logic;
        s_axi_ctrl_status_awready : out std_logic;
        s_axi_ctrl_status_wdata   : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
        s_axi_ctrl_status_wstrb   : in  std_logic_vector(7 downto 0);
        s_axi_ctrl_status_wvalid  : in  std_logic;
        s_axi_ctrl_status_wready  : out std_logic;
        s_axi_ctrl_status_bresp   : out std_logic_vector(1 downto 0);
        s_axi_ctrl_status_bvalid  : out std_logic;
        s_axi_ctrl_status_bready  : in  std_logic;
        s_axi_ctrl_status_araddr  : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
        s_axi_ctrl_status_arprot  : in  std_logic_vector(2 downto 0);
        s_axi_ctrl_status_arvalid : in  std_logic;
        s_axi_ctrl_status_arready : out std_logic;
        s_axi_ctrl_status_rdata   : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
        s_axi_ctrl_status_rresp   : out std_logic_vector(1 downto 0);
        s_axi_ctrl_status_rvalid  : out std_logic;
        s_axi_ctrl_status_rready  : in  std_logic
  );
end b_ram_bank;

architecture Behavioral of b_ram_bank is

--Control/status registers
signal emsc2cpu_register : std_logic_vector(31 downto 0);
signal cpu2emsc_register : std_logic_vector(31 downto 0);
signal in_G_register     : std_logic_vector(31 downto 0);

--Flyttet fra porter
signal init, valid_input : std_logic;
signal G_size : std_logic_vector(11 downto 0);
signal initialized : std_logic;

--Register for � forskyve raw signalet 1 sykel

signal data_in_w    : std_logic_vector(B_RAM_BIT_WIDTH-1 downto 0);
signal read_address : integer range 0 to B_RAM_SIZE-1;
signal write_enable : std_logic_vector(NUM_B_RAM-1 downto 0);
signal b_ram_sel   : std_logic_vector(NUM_B_RAM-1 downto 0);
signal data_in     : std_logic_vector(B_RAM_BIT_WIDTH-1 downto 0);

TYPE state_type IS (idle, write, read);
SIGNAL state : state_type;

begin

data_in_w <= data_in when aresetn = '1' else (others => '0');

init_flag <= initialized;
v_len <= G_size;
G_size <= cpu2emsc_register(11 downto 0);
enable <= cpu2emsc_register(12) when initialized = '1' else '0'; 
init <= cpu2emsc_register(13); 
emsc2cpu_register(0) <= initialized;
emsc2cpu_register(1) <= p_rdy;
emsc2cpu_register(2) <= last_p;
emsc2cpu_register(31 downto 3) <= (others => '0');



register_interface: entity work.register_interface
    generic map(
        C_S_AXI_DATA_WIDTH => C_S_AXI_DATA_WIDTH,
        C_S_AXI_ADDR_WIDTH => C_S_AXI_ADDR_WIDTH,
        B_RAM_SIZE         => B_RAM_SIZE,
        B_RAM_BIT_WIDTH    => B_RAM_BIT_WIDTH,
        NUM_B_RAM          => NUM_B_RAM
    )
    port map(
         s_axi_aclk    => clk,
         s_axi_aresetn => aresetn,
   
         s_axi_awaddr  => s_axi_ctrl_status_awaddr,
         s_axi_awprot  => s_axi_ctrl_status_awprot,
         s_axi_awvalid => s_axi_ctrl_status_awvalid,
         s_axi_awready => s_axi_ctrl_status_awready,
   
         s_axi_wdata  => s_axi_ctrl_status_wdata,
         s_axi_wstrb  => s_axi_ctrl_status_wstrb,
         s_axi_wvalid => s_axi_ctrl_status_wvalid,
         s_axi_wready => s_axi_ctrl_status_wready,
   
         s_axi_bresp  => s_axi_ctrl_status_bresp,
         s_axi_bvalid => s_axi_ctrl_status_bvalid,
         s_axi_bready => s_axi_ctrl_status_bready,
   
         s_axi_araddr  => s_axi_ctrl_status_araddr,
         s_axi_arprot  => s_axi_ctrl_status_arprot,
         s_axi_arvalid => s_axi_ctrl_status_arvalid,
         s_axi_arready => s_axi_ctrl_status_arready,
   
         s_axi_rdata  => s_axi_ctrl_status_rdata,
         s_axi_rresp  => s_axi_ctrl_status_rresp,
         s_axi_rvalid => s_axi_ctrl_status_rvalid,
         s_axi_rready => s_axi_ctrl_status_rready,
         
         --Register Outputs
         emsc2cpu_register  => emsc2cpu_register,
         p_out_reg          => p_out_reg,
         --Register Inputs
         cpu2emsc_register => cpu2emsc_register,
         in_G_register     => data_in,
         valid_input       => valid_input
         --read_enable       => read_enable_w
    );


b_ram: for i in 0 to NUM_B_RAM-1 generate
    DUT : entity work.block_ram
    Generic map(
            B_RAM_SIZE => B_RAM_SIZE,
            B_RAM_BIT_WIDTH => B_RAM_BIT_WIDTH
    )
    port map(
        clk             =>         clk,
        aresetn         =>         aresetn,
        data_in         =>         data_in_w,
        write_enable    =>         write_enable(i),
        read_enable     =>         read_enable,
        read_address    =>         read_address,
        data_out        =>         data_out(B_RAM_BIT_WIDTH*i + B_RAM_BIT_WIDTH-1 downto B_RAM_BIT_WIDTH*i) 
    );
end generate b_ram;
    

process(clk, aresetn)
    variable counter : integer range 0 to B_RAM_SIZE-1 := 0;
    variable prev_b_ram_addr : std_logic_vector(NUM_B_RAM-1 downto 0);
    variable valid_prev : std_logic;
begin
    if(aresetn = '0') then
        initialized <= '0';
        b_ram_sel <= (others => '0');
        state <= idle;
    elsif(rising_edge(clk)) then
            case state is
                --Stays in idle until either a init or read should be
                --performed
                when idle =>
                    counter := 0;
                    if(init = '1' and valid_input = '1') then
                        state <= write;
                        b_ram_sel <= (0 => '1', others => '0');
                        counter := counter + 1;
                    elsif(read_enable = '1' and initialized = '1') then
                        read_address <= read_address + 1;
                        state <= read;
                    end if;
               
                --Stays in write until initialization is completed.
                --Has to take care of bubbles in input data
                when write =>
                    if(valid_input = '1') then
                        counter := counter + 1;
                        if(counter >= to_integer(unsigned(G_size))) then
                            if(write_enable(NUM_B_RAM-1) = '1') then
                                state <= idle;
                                initialized <= '1';
                                --write_enable <= (others => '0');
                            else
                                --write_enable <= write_enable(NUM_B_RAM-2 downto 0) & '0';
                                b_ram_sel <= b_ram_sel(NUM_B_RAM-2 downto 0) & '0';
                                counter := 0;
                            end if; 
                        end if;
                    end if;
                
                --The read state should simply read out 1 value from each B_ram
                --each cycle.
                when read =>
                    if(read_enable = '1') then
                        read_address <= read_address + 1;
                        if(read_address >= to_integer(unsigned(G_size))-1) then
                            state <= idle;
                            read_address <= 0;
                        end if;
                    end if;
            end case;
    end if;    
end process;


process(b_ram_sel, state, init, valid_input)
begin
    if(state = write and valid_input = '1') then
        write_enable <= b_ram_sel;
    elsif(state = idle and init = '1' and valid_input = '1') then
        write_enable <= (0 => '1', others =>'0');
    else
        write_enable <= (others => '0');
    end if;
end process;

end Behavioral;
