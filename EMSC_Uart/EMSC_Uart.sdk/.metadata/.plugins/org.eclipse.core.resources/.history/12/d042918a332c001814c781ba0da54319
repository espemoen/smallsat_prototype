/*
 * Empty C++ Application
 */

#include <stdio.h>
#include "xil_printf.h"
#include "Eigen/dense"

#include <stdio.h>
#include <stdlib.h>     /* atof */
#include <math.h>
#include <float.h>
#include "xparameters.h"
#include "xuartps.h"

#include <string.h>
#include "xtmrctr.h"

using Eigen::MatrixXf;

#define TMRCTR_DEVICE_ID  XPAR_TMRCTR_0_DEVICE_ID
#define TIMER_COUNTER_0	 0
XTmrCtr TimerCounter;

#define UART_DEVICE_ID		XPAR_PS7_UART_1_DEVICE_ID
XUartPs Uart_Ps;

/************************** Function Prototypes ******************************/
void calculateSD(double* std, double ** data, int nVars, int refOrder);
void mean(double** ref_spectra, double* mean,  int nVars, int refOrder);
void EMSC(double ** raw, double ** ref_spectra, double ** corrected, int nVars, int nObs, int refOrder);
void execute_EMSC(double ** raw, double ** ref_spectra, double ** corrected, int nVars, int nObs, int refOrder, int bin_enable, int bin_size);
double ** initialize(int rows, int columns);

int init_timer(u16 DeviceId, u8 TmrCtrNumber);
u32 start_timer(u8 TmrCtrNumber);
u32 stop_timer(u8 TmrCtrNumber);


template <class MatT>
Eigen::Matrix<typename MatT::Scalar, MatT::ColsAtCompileTime, MatT::RowsAtCompileTime>
pseudoinverse(const MatT &mat, typename MatT::Scalar tolerance = typename MatT::Scalar{1e-4}) // choose appropriately
{
    typedef typename MatT::Scalar Scalar;
    auto svd = mat.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
    const auto &singularValues = svd.singularValues();
    Eigen::Matrix<Scalar, MatT::ColsAtCompileTime, MatT::RowsAtCompileTime> singularValuesInv(mat.cols(), mat.rows());
    singularValuesInv.setZero();
    for (unsigned int i = 0; i < singularValues.size(); ++i) {
        if (singularValues(i) > tolerance)
        {
            singularValuesInv(i, i) = Scalar{1} / singularValues(i);
        }
        else
        {
            singularValuesInv(i, i) = Scalar{0};
        }
    }
    return svd.matrixV() * singularValuesInv * svd.matrixU().adjoint();
}


double ** initialize(int rows, int columns) {
	double **temp;
	temp = (double **)malloc(rows * sizeof(double*));
	for (int row = 0; row < rows; row++) {
		temp[row] = (double*)malloc(columns * sizeof(double));
	}
	return temp;
}

void mean(double** ref_spectra, double* mean,  int nVars, int refOrder) {
	double sum = 0;
	for (int col = 0; col < nVars; col++) {
		for (int row = 0; row < refOrder; row++) {
			sum += ref_spectra[row][col];
		}
		mean[col] = sum / refOrder;
		sum = 0;
	}
}



void EMSC(double** raw, double** ref_spectra, double* mean_spectra, double** corrected, int nVars, int nObs, int refOrder) {

	//---------------------DECLARATIONS---------------------
	MatrixXf M(refOrder + 4, nVars);
	MatrixXf mult1_res(refOrder+4, refOrder+4);
	double ** G = initialize(nVars, refOrder+4);
	double* p = (double*)malloc((refOrder + 4) * sizeof(double));
	double num = 0;



	//------------------------------------------------------
	xil_printf("Constructing M!\n");
	for (int i = 0; i < nVars; i++) {

		//Add 1 in first row
		M(0,i) = 1;
		//M_trans(i,0) = 1;

		//Add linspace and linspace squared
		M(1,i) = num;
		//M_trans(i,1) = num;

		M(2,i) = pow(num, 2);
		//M_trans(i,2) = pow(num, 2);
		num += (1.0 / (nVars - 1));

		for (int y = 0; y < refOrder; y++) {
			M(y + 3,i) = ref_spectra[y][i];
			//M_trans(i,y + 3) = ref_spectra[y][i];
		}

		//Add mean in last row
		M(refOrder+3,i) = mean_spectra[i];
		//M_trans(i,refOrder+3) = mean_spectra[i];
		
	}




	u32 Value1;
	u32 Value2;
	init_timer(TMRCTR_DEVICE_ID, TIMER_COUNTER_0);
	Value1 = start_timer(TIMER_COUNTER_0);

	MatrixXf p_inv = pseudoinverse(M,1e-4);
	xil_printf("Pseudo-Inverse Completed!\n");
	for(int i = 0; i<nVars; i++){
		for(int y = 0; y<refOrder+4; y++){
			G[i][y] =(double) p_inv(i,y);
		}
	}

	Value2 = stop_timer(TIMER_COUNTER_0); //multiply by 10 nanoseconds 100 mhz
	xil_printf("Timer: %d\n", (Value2-Value1));

	//THIS PART SHOULD BE DONE IN HARDWARE
	xil_printf("Calculating Corrected Starting!\n");
	double sum = 0;
	for (int idx = 0; idx < nObs; idx++) {

		for (int i = 0; i < refOrder + 4; i++) {
			for (int y = 0; y <nVars; y++) {
				sum += raw[idx][y] * G[y][i];
			}
			p[i] = sum;

			
			sum = 0;
		}
		
		for (int t = 0; t < nVars; t++) {
			corrected[idx][t] = (raw[idx][t] - p[0] - p[1] * M(1,t) - p[2] * M(2,t)) / p[refOrder + 3];
		}
	}


}


//Timer
//----------------------------------------------------------------------
int init_timer(u16 DeviceId, u8 TmrCtrNumber){
	int Status;
		XTmrCtr *TmrCtrInstancePtr = &TimerCounter;

		/*
		 * Initialize the timer counter so that it's ready to use,
		 * specify the device ID that is generated in xparameters.h
		 */
		Status = XTmrCtr_Initialize(TmrCtrInstancePtr, DeviceId);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		/*
		 * Perform a self-test to ensure that the hardware was built
		 * correctly, use the 1st timer in the device (0)
		 */
		Status = XTmrCtr_SelfTest(TmrCtrInstancePtr, TmrCtrNumber);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		/*
		* Enable the Autoreload mode of the timer counters.
		*/


		return XST_SUCCESS;
}

u32 start_timer(u8 TmrCtrNumber){
	XTmrCtr *TmrCtrInstancePtr = &TimerCounter;
	XTmrCtr_SetOptions(TmrCtrInstancePtr, TmrCtrNumber,
							XTC_AUTO_RELOAD_OPTION);
	u32 val = XTmrCtr_GetValue(TmrCtrInstancePtr, TmrCtrNumber);
	XTmrCtr_Start(TmrCtrInstancePtr, TmrCtrNumber);
	return val;
}

u32 stop_timer(u8 TmrCtrNumber){
	XTmrCtr *TmrCtrInstancePtr = &TimerCounter;
	u32 val = XTmrCtr_GetValue(TmrCtrInstancePtr, TmrCtrNumber);
	XTmrCtr_SetOptions(TmrCtrInstancePtr, TmrCtrNumber, 0);
	return val;
}
//----------------------------------------------------------------------


int main(){

	//Adding pointer to location of stored cube.
	float * mem_ptr = (float*)0x10000000;
	double d;
	d = (float)mem_ptr[0];
	d = (float)mem_ptr[2];
	int nVars = 52; //number of wavelenghts
	int nObs  = 10000; //total number of pixels
	int refOrder = 2; //numbers of species in spectra


	double ** raw = initialize(nObs,nVars);
	double ** ref_spectra = initialize(refOrder, nVars);
	double ** corrected = initialize(nObs, nVars);
	double * mean_v =  (double*)malloc(nVars * sizeof(double));


	//Fill raw matrix
	int index = 0;
	for(int rows = 0; rows < nObs; rows++){
		for(int cols = 0; cols < nVars; cols++){
			raw[rows][cols] = (double)mem_ptr[index++];
		}
	}

	//Construct some reference spectra
	//Just using some spectras from raw in this case
	//as an example.
	for(int i = 0; i<nVars; i++){
		ref_spectra[0][i] = raw[2][i];
		ref_spectra[1][i] = raw[9938][i];
	}

	//calculate mean of ref_spectra
	mean(ref_spectra, mean_v, nVars, refOrder);


	xil_printf("ESMC Starting!\n");

	//Start the EMSC
	EMSC(raw, ref_spectra, mean_v, corrected, nVars, nObs, refOrder);


	//Point to location for storing data
	mem_ptr = (float*)0x13197508;
		    index = 0;
		    for(int i = 0; i<nObs; i++){
		    	for(int y = 0; y<nVars; y++){
		    		mem_ptr[index++] = (float)corrected[i][y];
		    	}
		    }

	xil_printf("Done");

	    return 0;
}
