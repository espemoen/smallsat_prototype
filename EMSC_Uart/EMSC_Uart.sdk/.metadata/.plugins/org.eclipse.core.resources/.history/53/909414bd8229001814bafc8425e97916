/*
 * Empty C++ Application
 */

#include <stdio.h>
#include "xil_printf.h"
#include "Eigen/dense"

#include <stdio.h>
#include <stdlib.h>     /* atof */
#include <math.h>
#include <float.h>
#include "xparameters.h"
#include "xuartps.h"

#include <string.h>
#include "xtmrctr.h"


#define TMRCTR_DEVICE_ID  XPAR_TMRCTR_0_DEVICE_ID
#define TIMER_COUNTER_0	 0
XTmrCtr TimerCounter;

#define UART_DEVICE_ID		XPAR_PS7_UART_1_DEVICE_ID
XUartPs Uart_Ps;

/************************** Function Prototypes ******************************/
void receiveData(double ** arr, int x, int y);
void sendData(double ** arr, int x, int y);
void calculateSD(double* std, double ** data, int nVars, int refOrder);
double mean(int length, double x[]);
void bsxfun(double **res ,double ** ref_spectra, int nVars, int refOrder);
//void matrixMult(double arrA[5][100], double arrB[100][5], int rowA, int colB, int elem,  double res[5][5]);
void matMult(double **arrA, double **arrB, int rowA, int colB, int elem, double **res);
double determinant(double ** a, int k);
void transpose(double ** num, double ** fac, double r, double ** res);
void cofactor(double ** num, double f, double ** res);
void EMSC(double ** raw, double ** ref_spectra, double ** corrected, int nVars, int nObs, int refOrder);
void execute_EMSC(double ** raw, double ** ref_spectra, double ** corrected, int nVars, int nObs, int refOrder, int bin_enable, int bin_size);
double ** initialize(int rows, int columns);

void init_timer(u16 DeviceId, u8 TmrCtrNumber);
u32 start_timer(u8 TmrCtrNumber);
u32 stop_timer(u8 TmrCtrNumber);

using Eigen::MatrixXf;

template <class MatT>
Eigen::Matrix<typename MatT::Scalar, MatT::ColsAtCompileTime, MatT::RowsAtCompileTime>
pseudoinverse(const MatT &mat, typename MatT::Scalar tolerance = typename MatT::Scalar{1e-4}) // choose appropriately
{
    typedef typename MatT::Scalar Scalar;
    auto svd = mat.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
    const auto &singularValues = svd.singularValues();
    Eigen::Matrix<Scalar, MatT::ColsAtCompileTime, MatT::RowsAtCompileTime> singularValuesInv(mat.cols(), mat.rows());
    singularValuesInv.setZero();
    for (unsigned int i = 0; i < singularValues.size(); ++i) {
        if (singularValues(i) > tolerance)
        {
            singularValuesInv(i, i) = Scalar{1} / singularValues(i);
        }
        else
        {
            singularValuesInv(i, i) = Scalar{0};
        }
    }
    return svd.matrixV() * singularValuesInv * svd.matrixU().adjoint();
}


void print_matrix(MatrixXf mat){
	int rows, cols;
	int whole, thousandths;
	float fval;
	rows = mat.rows();
	cols = mat.cols();
	for(int x = 0; x < rows; x++){
		for(int y = 0; y < cols; y++){
			fval = mat(x,y);
			whole = fval;
			thousandths = (fval - whole) * 1000;
			xil_printf("%d.%3d ", whole, thousandths);
		}
		xil_printf("\n");
	}
}

void print_float(double num){
	int whole, thousandths;
	float fval = (float)num;
	whole = fval;
	thousandths = (fval - whole) * 1000;
	xil_printf("%d.%3d\n", whole, thousandths);
}

double ** initialize(int rows, int columns) {
	double **temp;
	temp = (double **)malloc(rows * sizeof(double*));
	for (int row = 0; row < rows; row++) {
		temp[row] = (double*)malloc(columns * sizeof(double));
	}
	return temp;
}

void mean(double ** ref_spectra, double * mean,  int nVars, int refOrder) {
	double sum = 0;
	for (int col = 0; col < nVars; col++) {
		for (int row = 0; row < refOrder; row++) {
			sum += ref_spectra[row][col];
		}
		mean[col] = sum / refOrder;
		sum = 0;
	}
}

void matMult(double **arrA, double **arrB, int rowA, int colB, int elem, double **res) {
	long double sum = 0;
	double probe1 = 0;
	double probe2 = 0;
	for (int row = 0; row < rowA; row++) {
		for (int col = 0; col < colB; col++) {


			for (int y = 0; y < elem; y++) {
				probe1 = arrA[row][y];
				probe2 = arrB[y][col];
				sum += arrA[row][y] * arrB[y][col];
			}
			res[row][col] = sum;
			sum = 0;
		}
	}
}




void EMSC(double ** raw, double ** ref_spectra, double * mean_spectra, double ** corrected, int nVars, int nObs, int refOrder) {

	//---------------------DECLARATIONS---------------------
	double temp[2] = { 0 };
	MatrixXf M(refOrder + 4, nVars);
	//MatrixXf mult2_res(nObs, refOrder + 4);
	//double ** M = initialize(refOrder + 4, nObs);
	MatrixXf M_trans(nObs, refOrder + 4);
	//double ** mult2_res = initialize(nObs, refOrder + 4);
	MatrixXf mult1_res(refOrder+4, refOrder+4);
	double * std = (double *)malloc(nVars * sizeof(double));
	double * p = (double *)malloc((refOrder + 4) * sizeof(double));
	double mean_t, num = 0;

	//------------------------------------------------------

	//Calculates standard deviation of the ref_spectra
	//calculateSD(std, ref_spectra, nVars, refOrder);


	//M[0] = ones(1,nVars);
	//M[1] = wlens
	//M[2] = wlensSQ
	//M[3] = K;
	//M[4] = m;
	//Construct the M matrix
	for (int i = 0; i < nVars; i++) {

		//Add 1 in first row
		M(0,i) = 1;
		M_trans(i,0) = 1;

		//Add linspace and linspace squared
		M(1,i) = num;
		M_trans(i,1) = num;

		M(2,i) = pow(num, 2);
		M_trans(i,2) = pow(num, 2);
		num += (1.0 / (nVars - 1));

		for (int y = 0; y < refOrder; y++) {
			M(y + 3,i) = ref_spectra[y][i];
			M_trans(i,y + 3) = ref_spectra[y][i];
		}

		//Add mean in last row
		M(refOrder+3,i) = mean_spectra[i];
		M_trans(i,refOrder+3) = mean_spectra[i];
		
	}

	MatrixXf mult2_res = pseudoinverse(M,1e-4);

	double t;
	for(int i = 1; i<nVars; i++){
		for (int y = 0; y<refOrder + 4; y++){
			t = (double)mult2_res(i,y);
		}
	}


	//for (int i = 0; i < 100; i++) {
	//	printf("%f  %f  %f  %f  %f %f\n", M[0][i], M[1][i], M[2][i], M[3][i], M[4][i], M[5][i]);
	//}

	//Multiply M with M transpose, stores the result in mult1_res.

	//matMult(M, M_trans, refOrder+4, refOrder+4, nVars, mult1_res);

	//for (int i = 0; i < 100; i++) {
	//	printf("%f  %f  %f  %f  %f %f\n", M_trans[i][0], M_trans[i][1], M_trans[i][2], M_trans[i][3], M_trans[i][4], M_trans[i][5]);
	//}

	//for (int i = 0; i < 6; i++) {
	//	printf("%f  %f  %f  %f  %f %f\n", mult1_res[i][0], mult1_res[i][1], mult1_res[i][2], mult1_res[i][3], mult1_res[i][4], mult1_res[i][5]);
	//}

	//Calculates inverse
	//double ** inverse = initialize(refOrder + 4, refOrder + 4);
	//cofactor(mult1_res, refOrder + 4, inverse);

	//for (int i = 0; i < 6; i++) {
	//	printf("%f  %f  %f  %f  %f %f\n", inverse[i][0], inverse[i][1], inverse[i][2], inverse[i][3], inverse[i][4], inverse[i][5]);
	//}

	//Multiplies the M transpose with the inverse to achieve: M'*inv(M*M'), stores the result in mult2_res.
	//matMult(M_trans, inverse, nObs, refOrder + 4, refOrder + 4, mult2_res);
	
	//for (int i = 0; i < 100; i++) {
	//	printf("%f  %f  %f  %f  %f %f\n", mult2_res[i][0], mult2_res[i][1], mult2_res[i][2], mult2_res[i][3], mult2_res[i][4], mult2_res[i][5]);
	//}

	//THIS PART SHOULD BE DONE IN HARDWARE
	/*
	double sum = 0;
	for (int idx = 0; idx < nObs; idx++) {

		for (int i = 0; i < refOrder + 4; i++) {
			for (int y = 0; y <nVars; y++) {
				sum += raw[idx][y] * mult2_res[y][i];
			}
			p[i] = sum;
			
			sum = 0;
		}
		
		for (int t = 0; t < nVars; t++) {					 //wlens
			corrected[idx][t] = (raw[idx][t] - p[0] - p[1] * M[1][t] - p[2] * M[2][t]) / p[refOrder + 3];
		}
	}
	*/
}

int main(){
	double * mem_ptr = (double*)0x10000000;
	double b = (double)mem_ptr[2];
	b = mem_ptr[519999];

	int nVars = 52; //wavelenghts
	int nObs  = 10000; //m*n
	int refOrder = 2; //numbers of species in spectra

	double **raw = initialize(nObs,nVars);
	double **ref_spectra = initialize(refOrder, nVars);
	double **corrected = initialize(nObs, nVars);
	double * mean_v = (double *)malloc(nVars * sizeof(double));

	int index = 0;
	for(int rows = 0; rows < nObs; rows++){
		for(int cols = 0; cols < nVars; cols++){
			raw[rows][cols] = (double)mem_ptr[index++];
		}
	}

	for(int i = 0; i<nVars; i++){
		ref_spectra[0][i] = raw[2][i];
		ref_spectra[1][i] = raw[92][i];
	}

	//calculate mean of ref_spectra
	mean(ref_spectra, mean_v, nVars, refOrder);

	EMSC(raw, ref_spectra, mean_v, corrected, nVars, nObs, refOrder);


	/*

	int a = 1;

	for (int i = 0; i < 3; i++) {
		for (int y = 0; y < 3; y++) {
			m(i, y) = a;
			a++;
		}
	}



	Eigen::MatrixXf pinv = m.completeOrthogonalDecomposition().pseudoInverse();


	print_matrix(pinv);
	*/
	system("pause");

	//init_platform();



		/*


		//u32 Value1;
	    //u32 Value2;

	    //init_timer(TMRCTR_DEVICE_ID, TIMER_COUNTER_0);

	    //Value1 = start_timer(TIMER_COUNTER_0);
	    for(int i = 0; i<nVars; i++){
	    	ref_spectra[0][i] = raw[2][i];
	    	ref_spectra[1][i] = raw[92][i];
	    }
	    xil_printf("ESMC Starting!\n");
	    execute_EMSC(raw,ref_spectra, corrected, nVars, nObs, refOrder, bin_enable, bin_size);
	    //Value2 = stop_timer(TIMER_COUNTER_0); //multiply by 10 nanoseconds 100 mhz


	    //sendData(corrected, nVars, nObs);
	    free(raw);
	    free(ref_spectra);
	    //xil_printf("Timer: %d\n", (Value2-Value1));
	    xil_printf("corrected: 0x%x\n",corrected);
	    mem_ptr = (u_int32_t*)0x13197508;
	    index = 0;
	    for(int i = 0; i<nObs; i++){
	    	for(int y = 0; y<nVars; y++){
	    		mem_ptr[index] = (float)corrected[i][y];
	    		index++;
	    	}
	    }
	    xil_printf("DONE!");
	    //cleanup_platform();
		*/
	    return 0;
}
