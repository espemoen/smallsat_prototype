/*
 * Empty C++ Application
 */

#include <stdio.h>
#include "xil_printf.h"
#include "Eigen/dense"

#include <stdio.h>
#include <stdlib.h>     /* atof */
#include <math.h>
#include <float.h>
#include "xparameters.h"
#include "xuartps.h"

#include <string.h>
#include "xtmrctr.h"


#define TMRCTR_DEVICE_ID  XPAR_TMRCTR_0_DEVICE_ID
#define TIMER_COUNTER_0	 0
XTmrCtr TimerCounter;

#define UART_DEVICE_ID		XPAR_PS7_UART_1_DEVICE_ID
XUartPs Uart_Ps;

/************************** Function Prototypes ******************************/
void receiveData(double ** arr, int x, int y);
void sendData(double ** arr, int x, int y);
void calculateSD(double* std, double ** data, int nVars, int refOrder);
double mean(int length, double x[]);
void bsxfun(double **res ,double ** ref_spectra, int nVars, int refOrder);
//void matrixMult(double arrA[5][100], double arrB[100][5], int rowA, int colB, int elem,  double res[5][5]);
void matMult(double **arrA, double **arrB, int rowA, int colB, int elem, double **res);
double determinant(double ** a, int k);
void transpose(double ** num, double ** fac, double r, double ** res);
void cofactor(double ** num, double f, double ** res);
void EMSC(double ** raw, double ** ref_spectra, double ** corrected, int nVars, int nObs, int refOrder);
void execute_EMSC(double ** raw, double ** ref_spectra, double ** corrected, int nVars, int nObs, int refOrder, int bin_enable, int bin_size);
double ** initialize(int rows, int columns);

int init_timer(u16 DeviceId, u8 TmrCtrNumber);
u32 start_timer(u8 TmrCtrNumber);
u32 stop_timer(u8 TmrCtrNumber);

using Eigen::MatrixXf;

template <class MatT>
Eigen::Matrix<typename MatT::Scalar, MatT::ColsAtCompileTime, MatT::RowsAtCompileTime>
pseudoinverse(const MatT &mat, typename MatT::Scalar tolerance = typename MatT::Scalar{1e-4}) // choose appropriately
{
    typedef typename MatT::Scalar Scalar;
    auto svd = mat.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
    const auto &singularValues = svd.singularValues();
    Eigen::Matrix<Scalar, MatT::ColsAtCompileTime, MatT::RowsAtCompileTime> singularValuesInv(mat.cols(), mat.rows());
    singularValuesInv.setZero();
    for (unsigned int i = 0; i < singularValues.size(); ++i) {
        if (singularValues(i) > tolerance)
        {
            singularValuesInv(i, i) = Scalar{1} / singularValues(i);
        }
        else
        {
            singularValuesInv(i, i) = Scalar{0};
        }
    }
    return svd.matrixV() * singularValuesInv * svd.matrixU().adjoint();
}


void print_matrix(MatrixXf mat){
	int rows, cols;
	int whole, thousandths;
	float fval;
	rows = mat.rows();
	cols = mat.cols();
	for(int x = 0; x < rows; x++){
		for(int y = 0; y < cols; y++){
			fval = mat(x,y);
			whole = fval;
			thousandths = (fval - whole) * 1000;
			xil_printf("%d.%3d ", whole, thousandths);
		}
		xil_printf("\n");
	}
}

void print_float(double num){
	int whole, thousandths;
	float fval = (float)num;
	whole = fval;
	thousandths = (fval - whole) * 1000;
	xil_printf("%d.%3d\n", whole, thousandths);
}

double ** initialize(int rows, int columns) {
	double **temp;
	temp = (double **)malloc(rows * sizeof(double*));
	for (int row = 0; row < rows; row++) {
		temp[row] = (double*)malloc(columns * sizeof(double));
	}
	return temp;
}

void mean(double ** ref_spectra, double * mean,  int nVars, int refOrder) {
	double sum = 0;
	for (int col = 0; col < nVars; col++) {
		for (int row = 0; row < refOrder; row++) {
			sum += ref_spectra[row][col];
		}
		mean[col] = sum / refOrder;
		sum = 0;
	}
}

void matMult(double **arrA, double **arrB, int rowA, int colB, int elem, double **res) {
	long double sum = 0;
	double probe1 = 0;
	double probe2 = 0;
	for (int row = 0; row < rowA; row++) {
		for (int col = 0; col < colB; col++) {


			for (int y = 0; y < elem; y++) {
				probe1 = arrA[row][y];
				probe2 = arrB[y][col];
				sum += arrA[row][y] * arrB[y][col];
			}
			res[row][col] = sum;
			sum = 0;
		}
	}
}




void EMSC(double ** raw, double ** ref_spectra, double * mean_spectra, double ** corrected, int nVars, int nObs, int refOrder) {

	//---------------------DECLARATIONS---------------------
	MatrixXf M(refOrder + 4, nVars);
	//MatrixXf mult2_res(nObs, refOrder + 4);
	//double ** M = initialize(refOrder + 4, nObs);
	//MatrixXf M_trans(nObs, refOrder + 4);
	//double ** mult2_res = initialize(nObs, refOrder + 4);
	MatrixXf mult1_res(refOrder+4, refOrder+4);
	double * p = (double *)malloc((refOrder + 4) * sizeof(double));
	double num = 0;

	//------------------------------------------------------
	xil_printf("Constructing M Starting!\n");
	for (int i = 0; i < nVars; i++) {

		//Add 1 in first row
		M(0,i) = 1;
		//M_trans(i,0) = 1;

		//Add linspace and linspace squared
		M(1,i) = num;
		//M_trans(i,1) = num;

		M(2,i) = pow(num, 2);
		//M_trans(i,2) = pow(num, 2);
		num += (1.0 / (nVars - 1));

		for (int y = 0; y < refOrder; y++) {
			M(y + 3,i) = ref_spectra[y][i];
			//M_trans(i,y + 3) = ref_spectra[y][i];
		}

		//Add mean in last row
		M(refOrder+3,i) = mean_spectra[i];
		//M_trans(i,refOrder+3) = mean_spectra[i];
		
	}

	MatrixXf mult2_res = pseudoinverse(M,1e-5);
	xil_printf("Pseudo-Inverse Completed!\n");
	//THIS PART SHOULD BE DONE IN HARDWARE
	xil_printf("Potential HW code Starting!\n");
	double sum = 0;
	for (int idx = 0; idx < nObs; idx++) {

		for (int i = 0; i < refOrder + 4; i++) {
			for (int y = 0; y <nVars; y++) {
				sum += raw[idx][y] * mult2_res(y,i);
			}
			p[i] = sum;
			
			sum = 0;
		}
		
		for (int t = 0; t < nVars; t++) {					 //wlens
			corrected[idx][t] = (raw[idx][t] - p[0] - p[1] * M(1,t) - p[2] * M(2,t)) / p[refOrder + 3];
		}
	}

}


//Timer
//----------------------------------------------------------------------


int init_timer(u16 DeviceId, u8 TmrCtrNumber){
	int Status;
		XTmrCtr *TmrCtrInstancePtr = &TimerCounter;

		/*
		 * Initialize the timer counter so that it's ready to use,
		 * specify the device ID that is generated in xparameters.h
		 */
		Status = XTmrCtr_Initialize(TmrCtrInstancePtr, DeviceId);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		/*
		 * Perform a self-test to ensure that the hardware was built
		 * correctly, use the 1st timer in the device (0)
		 */
		Status = XTmrCtr_SelfTest(TmrCtrInstancePtr, TmrCtrNumber);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		/*
		* Enable the Autoreload mode of the timer counters.
		*/


		return XST_SUCCESS;
}


u32 start_timer(u8 TmrCtrNumber){
	XTmrCtr *TmrCtrInstancePtr = &TimerCounter;
	XTmrCtr_SetOptions(TmrCtrInstancePtr, TmrCtrNumber,
							XTC_AUTO_RELOAD_OPTION);
	u32 val = XTmrCtr_GetValue(TmrCtrInstancePtr, TmrCtrNumber);
	XTmrCtr_Start(TmrCtrInstancePtr, TmrCtrNumber);
	return val;
}

u32 stop_timer(u8 TmrCtrNumber){
	XTmrCtr *TmrCtrInstancePtr = &TimerCounter;
	u32 val = XTmrCtr_GetValue(TmrCtrInstancePtr, TmrCtrNumber);
	XTmrCtr_SetOptions(TmrCtrInstancePtr, TmrCtrNumber, 0);
	return val;
}

//----------------------------------------------------------------------


int main(){

	float * mem_ptr = (float*)0x10000000;
	double b = (float)mem_ptr[2];
	b = mem_ptr[519999];

	int nVars = 52; //wavelenghts
	int nObs  = 10000; //m*n
	int refOrder = 2; //numbers of species in spectra

	MatrixXf raw(nObs,nVars);
	MatrixXf ref_spectra(refOrder, nVars);
	MatrixXf corrected(nObs, nVars);
	//double * mean_v = (double *)malloc(nVars * sizeof(double));
	MatrixXf mean_v(1,nVars);
	int index = 0;
	for(int rows = 0; rows < nObs; rows++){
		for(int cols = 0; cols < nVars; cols++){
			raw[rows][cols] = (float)mem_ptr[index++];
		}
	}

	for(int i = 0; i<nVars; i++){
		ref_spectra[0][i] = raw[2][i];
		ref_spectra[1][i] = raw[9938][i];
	}

	//calculate mean of ref_spectra
	mean(ref_spectra, mean_v, nVars, refOrder);

	u32 Value1;
	u32 Value2;

	init_timer(TMRCTR_DEVICE_ID, TIMER_COUNTER_0);

	Value1 = start_timer(TIMER_COUNTER_0);

	xil_printf("ESMC Starting!\n");
	EMSC(raw, ref_spectra, mean_v, corrected, nVars, nObs, refOrder);

	Value2 = stop_timer(TIMER_COUNTER_0); //multiply by 10 nanoseconds 100 mhz

	free(raw);
	free(ref_spectra);
	xil_printf("Timer: %d\n", (Value2-Value1));
	double p;
	p = corrected[6998][0];
	p = corrected[0][1];
	print_float(corrected[0][0]);
	mem_ptr = (float*)0x13197508;
		    index = 0;
		    for(int i = 0; i<nObs; i++){
		    	for(int y = 0; y<nVars; y++){
		    		mem_ptr[index] = (float)corrected[i][y];
		    		index++;
		    	}
		    }

	xil_printf("Done");

	//init_platform();



		/*


		//u32 Value1;
	    //u32 Value2;

	    //init_timer(TMRCTR_DEVICE_ID, TIMER_COUNTER_0);

	    //Value1 = start_timer(TIMER_COUNTER_0);
	    for(int i = 0; i<nVars; i++){
	    	ref_spectra[0][i] = raw[2][i];
	    	ref_spectra[1][i] = raw[92][i];
	    }
	    xil_printf("ESMC Starting!\n");
	    execute_EMSC(raw,ref_spectra, corrected, nVars, nObs, refOrder, bin_enable, bin_size);
	    //Value2 = stop_timer(TIMER_COUNTER_0); //multiply by 10 nanoseconds 100 mhz


	    //sendData(corrected, nVars, nObs);
	    free(raw);
	    free(ref_spectra);
	    //xil_printf("Timer: %d\n", (Value2-Value1));
	    xil_printf("corrected: 0x%x\n",corrected);
	    mem_ptr = (u_int32_t*)0x13197508;
	    index = 0;
	    for(int i = 0; i<nObs; i++){
	    	for(int y = 0; y<nVars; y++){
	    		mem_ptr[index] = (float)corrected[i][y];
	    		index++;
	    	}
	    }
	    xil_printf("DONE!");
	    //cleanup_platform();
		*/
	    return 0;
}
