/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include <stdlib.h>     /* atof */
#include <math.h>
#include <float.h>
#include "platform.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "xuartps.h"

#include <string.h>


#define UART_DEVICE_ID		XPAR_PS7_UART_1_DEVICE_ID
XUartPs Uart_Ps;

/************************** Function Prototypes ******************************/
void receiveData(double ** arr, int x, int y);
void sendData(double ** arr, int x, int y);
double calculateSD(double data[], int length);
double mean(int length, double x[]);
void bsxfun(double *res ,double *a1, double *a2, int length, double std);
//void matrixMult(double arrA[5][100], double arrB[100][5], int rowA, int colB, int elem,  double res[5][5]);
void matMult(double **arrA, double **arrB, int rowA, int colB, int elem, double **res);
double determinant(double ** a, int k);
void transpose(double ** num, double fac[5][5], double r, double ** res);
void cofactor(double ** num, double f, double ** res);
void EMSC(double ** raw, double ** ref_spectra, double ** corrected, int nVars, int nObs);
double ** initialize(int rows, int columns);

//Receive test_data with UART. Remember to start python script first!
void receiveData(double ** arr, int x, int y){
		int Status;

	    u8 temp[8];

	    XUartPs_Config *Config;

	    Config = XUartPs_LookupConfig(UART_DEVICE_ID);
	    Status = XUartPs_CfgInitialize(&Uart_Ps, Config, Config->BaseAddress);
	    XUartPs_SetBaudRate(&Uart_Ps, 115200);

	    print("Starting");

	    for(int row = 0; row < x; row++){
	    	for(int col = 0; col < y; col++){

	    			for(int i = 0; i < 8; i+= 1){
	    				temp[i] = XUartPs_RecvByte(XPAR_PS7_UART_1_BASEADDR);
	    			}

	    			arr[row][col] = atof(temp);


	    			sprintf(temp,"%f", arr[row][col]);
	    			print("GooooooD");
	    	}
	    }
}

double ** initialize(int rows, int columns) {
	double **temp;
	temp = (double **)malloc(rows * sizeof(double*));
	for (int row = 0; row < rows; row++) {
		temp[row] = (double*)malloc(columns * sizeof(double));
	}
	return temp;
}

double mean(int length, double x[]) {
	double sum = 0;
	for (int i = 0; i < length; i++) {
		sum += x[i];
	}
	return((float)sum / length);
}


double calculateSD(double data[], int length)
{
	double sum = 0.0, mean, standardDeviation = 0.0;

	int i;

	for (i = 0; i<length; ++i)
	{
		sum += data[i];
	}

	mean = sum / 100;

	for (i = 0; i<length; ++i)
		standardDeviation += pow(data[i] - mean, 2);

	return sqrt(standardDeviation / (length -1));
}

void bsxfun(double *res ,double *a1, double *a2, int length, double std) {
	for (int i = 0; i < length; i++) {
		res[i] = (a1[i] - a2[i]) / std;
	}
}

/*void matrixMult(double arrA[5][100], double arrB[100][5], int rowA, int colB, int elem,  double res[5][5]) {
	double sum = 0;
	for (int row = 0; row < rowA; row++) {
		for (int col = 0; col < colB; col++) {

			for (int y = 0; y < elem; y++) {
				sum += arrA[row][y] * arrB[y][col];
			}
			res[row][col] = sum;
			sum = 0;
		}
	}
}*/

void matMult(double **arrA, double **arrB, int rowA, int colB, int elem, double **res) {
	double sum = 0;
	for (int row = 0; row < rowA; row++) {
		for (int col = 0; col < colB; col++) {


			for (int y = 0; y < elem; y++) {
				sum += arrA[row][y] * arrB[y][col];
			}
			res[row][col] = sum;
			sum = 0;
		}
	}
}

void EMSC(double ** raw, double ** ref_spectra, double ** corrected, int nVars, int nObs) {

	//---------------------DECLARATIONS---------------------
	double res[100] = { 0 };					//Used in calculations of ref_spectra
	double temp[2] = { 0 };
	double ** M = initialize(5, nObs);
	double ** M_trans = initialize(nVars, 5);	//Transpose matrix of M
	double ** p_M_Mtrans = initialize(5, 5); 	// Stores result from first matrix multiplication
	double ** mult2_res = initialize(nVars, 5); // Stores result from second matrix multiplication
	double mean_t, num = 0;

	//------------------------------------------------------

	//Calculates standard deviation of the ref_spectra
	double std = calculateSD(ref_spectra[1],100);


	//something
	bsxfun(res, ref_spectra[1], ref_spectra[0], 100, std);


	//Construct the M matrix
	for (int i = 0; i < 100; i++) {

		//Add 1 in first row
		M[0][i] = 1;
		M_trans[i][0] = 1;

		//Add mean in second row
		temp[0] = ref_spectra[0][i];
		temp[1] = ref_spectra[1][i];
		mean_t = mean(2, temp);
		M[1][i] = mean_t;
		M_trans[i][1] = mean_t;


		M[2][i] = res[i];
		M_trans[i][2] = res[i];

		//Add linspace and linspace squared
		M[3][i] = num;
		M_trans[i][3] = num;

		M[4][i] = pow(num, 2);
		M_trans[i][4] = pow(num, 2);
		num += (1.0 / 99.0);
	}


	//Multiply M with M transpose, stores the result in mult1_res.
	matMult(M, M_trans, 5, 5, 100, p_M_Mtrans);


	//Calculates inverse
	double ** inverse = initialize(5, 5);
	cofactor(p_M_Mtrans, 5, inverse);


	//Multiplies the M transpose with the inverse to achieve: M'*inv(M*M'), stores the result in mult2_res.
	matMult(M_trans, inverse, 100, 5, 5, mult2_res);

	//THIS PART SHOULD BE DONE IN HARDWARE
	double sum = 0;
	double p[5] = { 0 };
	for (int idx = 0; idx < 100; idx++) {

		for (int i = 0; i < 5; i++) {
			for (int y = 0; y < 100; y++) {
				sum += raw[idx][y] * mult2_res[y][i];
			}
			p[i] = sum;
			sum = 0;
		}
		for (int t = 0; t < 100; t++) {
			corrected[idx][t] = (raw[idx][t] - p[0] - p[3] * M[3][t] - p[4] * M[4][t]) / p[1];
		}
	}


	//--------------------------------------

	free(M);
	free(M_trans);
	free(mult2_res);
	free(p_M_Mtrans);
}



//---------------------------------------------------------------------
// http://www.sanfoundry.com/c-program-find-inverse-matrix/
//---------------------------------------------------------------------
double determinant(double ** a, int k){
	double ** b = (double **)malloc(5 * sizeof(double*));
	for (int row = 0; row < 5; row++) {
		b[row] = (double*)malloc(5 * sizeof(double));
	}

	double s = 1, det = 0;
	int i, j, m, n, c;
	if (k == 1)
	{
		return (a[0][0]);
	}
	else
	{
		det = 0;
		for (c = 0; c < k; c++)
		{
			m = 0;
			n = 0;
			for (i = 0; i < k; i++)
			{
				for (j = 0; j < k; j++)
				{
					b[i][j] = 0;
					if (i != 0 && j != c)
					{
						b[m][n] = a[i][j];
						if (n < (k - 2))
							n++;
						else
						{
							n = 0;
							m++;
						}
					}
				}
			}
			det = det + s * (a[0][c] * determinant(b, k - 1));
			s = -1 * s;
		}
	}

	return (det);
}

/*Finding transpose of matrix*/
void transpose(double ** num, double fac[5][5], double r, double ** res){
	int i, j;
	double b[5][5], inverse[5][5], d;

	for (i = 0; i < r; i++)
	{
		for (j = 0; j < r; j++)
		{
			b[i][j] = fac[j][i];
		}
	}
	d = determinant(num, r);
	for (i = 0; i < r; i++)
	{
		for (j = 0; j < r; j++)
		{
			inverse[i][j] = b[i][j] / d;
		}
	}

	for (i = 0; i < r; i++)
	{
		for (j = 0; j < r; j++)
		{
			res[i][j] = inverse[i][j];
		}
	}
}

void cofactor(double ** num, double f, double ** res){
	double ** b = (double **)malloc(5 * sizeof(double*));
	for (int row = 0; row < 5; row++) {
		b[row] = (double*)malloc(5 * sizeof(double));
	}
	double fac[5][5];
	int p, q, m, n, i, j;
	for (q = 0; q < f; q++)
	{
		for (p = 0; p < f; p++)
		{
			m = 0;
			n = 0;
			for (i = 0; i < f; i++)
			{
				for (j = 0; j < f; j++)
				{
					if (i != q && j != p)
					{
						b[m][n] = num[i][j];
						if (n < (f - 2))
							n++;
						else
						{
							n = 0;
							m++;
						}
					}
				}
			}
			fac[q][p] = pow(-1, q + p) * determinant(b, f - 1);
		}
	}
	transpose(num, fac, f, res);
}

//----------------------------------------------------------------------

void sendData(double ** arr, int x, int y){
	char temp[8];
	for(int row = 0; row < x; row++){
		 for(int col = 0; col < y; col++){
			 for(int i = 0; i < 8; i+= 1){
				 temp[i] = XUartPs_RecvByte(XPAR_PS7_UART_1_BASEADDR);
			 }

			 sprintf(temp,"%f", arr[row][col]);
			 print(temp);
		}
	}
}

//Timer
//----------------------------------------------------------------------
#include "xtmrctr.h"
#define TMRCTR_DEVICE_ID  XPAR_TMRCTR_0_DEVICE_ID
#define TIMER_COUNTER_0	 0
XTmrCtr TimerCounter;

void init_timer(u16 DeviceId, u8 TmrCtrNumber){
	int Status;
		XTmrCtr *TmrCtrInstancePtr = &TimerCounter;

		/*
		 * Initialize the timer counter so that it's ready to use,
		 * specify the device ID that is generated in xparameters.h
		 */
		Status = XTmrCtr_Initialize(TmrCtrInstancePtr, DeviceId);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		/*
		 * Perform a self-test to ensure that the hardware was built
		 * correctly, use the 1st timer in the device (0)
		 */
		Status = XTmrCtr_SelfTest(TmrCtrInstancePtr, TmrCtrNumber);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		/*
		* Enable the Autoreload mode of the timer counters.
		*/


		return XST_SUCCESS;
}


u32 start_timer(u8 TmrCtrNumber){
	XTmrCtr *TmrCtrInstancePtr = &TimerCounter;
	XTmrCtr_SetOptions(TmrCtrInstancePtr, TmrCtrNumber,
							XTC_AUTO_RELOAD_OPTION);
	u32 val = XTmrCtr_GetValue(TmrCtrInstancePtr, TmrCtrNumber);
	XTmrCtr_Start(TmrCtrInstancePtr, TmrCtrNumber);
	return val;
}

u32 stop_timer(u8 TmrCtrNumber){
	XTmrCtr *TmrCtrInstancePtr = &TimerCounter;
	u32 val = XTmrCtr_GetValue(TmrCtrInstancePtr, TmrCtrNumber);
	XTmrCtr_SetOptions(TmrCtrInstancePtr, TmrCtrNumber, 0);
	return val;
}

//----------------------------------------------------------------------


void delay(int ticks) {
   for(int delay = 0; delay < ticks; delay++){
    	//do nothing;
   }
}

int main()
{
    init_platform();
    int pixels = 100;
    int spectres = 100;
    double **raw = initialize(pixels,spectres);
    double **ref_spectra = initialize(2, spectres);
    double **corrected = initialize(pixels, spectres);


    receiveData(raw, 100, 100);

	u32 Value1;
    u32 Value2;

    init_timer(TMRCTR_DEVICE_ID, TIMER_COUNTER_0);

    Value1 = start_timer(TIMER_COUNTER_0);

    for(int i = 0; i<100; i++){
    	ref_spectra[0][i] = raw[2][i];
    	ref_spectra[1][i] = raw[92][i];
    }

    EMSC(raw, ref_spectra, corrected, pixels, spectres);
    Value2 = stop_timer(TIMER_COUNTER_0);

    sendData(corrected, 100, 100);



    xil_printf("%d\n", (Value2-Value1));
    cleanup_platform();
    return 0;
}
