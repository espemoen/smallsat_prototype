/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include <stdlib.h>     /* atof */
#include <math.h>
#include <float.h>
#include "platform.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "xuartps.h"

#include <string.h>
#include "xtmrctr.h"


#define TMRCTR_DEVICE_ID  XPAR_TMRCTR_0_DEVICE_ID
#define TIMER_COUNTER_0	 0
XTmrCtr TimerCounter;

#define UART_DEVICE_ID		XPAR_PS7_UART_1_DEVICE_ID
XUartPs Uart_Ps;

/************************** Function Prototypes ******************************/
void receiveData(double ** arr, int x, int y);
void sendData(double ** arr, int x, int y);
void calculateSD(double* std, double ** data, int nVars, int refOrder);
double mean(int length, double x[]);
void bsxfun(double **res ,double ** ref_spectra, int nVars, int refOrder);
//void matrixMult(double arrA[5][100], double arrB[100][5], int rowA, int colB, int elem,  double res[5][5]);
void matMult(double **arrA, double **arrB, int rowA, int colB, int elem, double **res);
double determinant(double ** a, int k);
void transpose(double ** num, double ** fac, double r, double ** res);
void cofactor(double ** num, double f, double ** res);
void EMSC(double ** raw, double ** ref_spectra, double ** corrected, int nVars, int nObs, int refOrder);
void execute_EMSC(double ** raw, double ** ref_spectra, double ** corrected, int nVars, int nObs, int refOrder, int bin_enable, int bin_size);
double ** initialize(int rows, int columns);

void init_timer(u16 DeviceId, u8 TmrCtrNumber);
u32 start_timer(u8 TmrCtrNumber);
u32 stop_timer(u8 TmrCtrNumber);





//Receive test_data with UART. Remember to start python script first!
void receiveData(double ** arr, int x, int y){
		int Status;

	    u8 temp[8];

	    XUartPs_Config *Config;

	    Config = XUartPs_LookupConfig(UART_DEVICE_ID);
	    Status = XUartPs_CfgInitialize(&Uart_Ps, Config, Config->BaseAddress);
	    XUartPs_SetBaudRate(&Uart_Ps, 115200);

	    print("Starting");

	    for(int row = 0; row < x; row++){
	    	for(int col = 0; col < y; col++){

	    			for(int i = 0; i < 8; i+= 1){
	    				temp[i] = XUartPs_RecvByte(XPAR_PS7_UART_1_BASEADDR);
	    			}

	    			arr[row][col] = atof(temp);


	    			sprintf(temp,"%f", arr[row][col]);
	    			print("Gooooood");
	    	}
	    }
}

double ** initialize(int rows, int columns) {
	double **temp;
	temp = (double **)malloc(rows * sizeof(double*));
	for (int row = 0; row < rows; row++) {
		temp[row] = (double*)malloc(columns * sizeof(double));
	}
	return temp;
}

double mean(int length, double x[]) {
	double sum = 0;
	for (int i = 0; i < length; i++) {
		sum += x[i];
	}
	return((float)sum / length);
}


void calculateSD(double* std, double ** data, int nVars, int refOrder)
{
	double sum = 0.0;
	double smean = 0.0;
	double * mean = (double *)malloc(nVars * sizeof(double));

	if (refOrder > 1) {
		for (int col = 0; col < nVars; col++) {
			for (int row = 0; row < refOrder; row++) {
				sum += data[row][col];
			}
			mean[col] = (sum / refOrder);
			sum = 0;
		}

		for (int col = 0; col < nVars; col++) {
			for (int row = 0; row < refOrder; row++) {
				sum += pow(data[row][col] - mean[col], 2);
			}
			if (refOrder > 1) {
				std[col] = sqrt(sum / (refOrder - 1));
			}
			sum = 0;
		}
	}
	else {
		for (int i = 0; i < nVars; i++) {
			sum += data[0][i];
		}
		smean = sum / nVars;
		sum = 0.0;
		for (int i = 0; i < nVars; i++) {
			sum += pow(data[0][i] - smean,2);
		}

		std[0] = sqrt(sum / (nVars - 1));
	}
	free(mean);

}

void bsxfun(double **res ,double ** ref_spectra, int nVars, int refOrder) {

	for (int col = 0; col < nVars; col++) {
		for (int row = 1; row < refOrder; row++) {
			res[row-1][col] = (ref_spectra[row][col] - ref_spectra[0][col]);
		}
	}
}



void matMult(double **arrA, double **arrB, int rowA, int colB, int elem, double **res) {
	long double sum = 0;
	for (int row = 0; row < rowA; row++) {
		for (int col = 0; col < colB; col++) {


			for (int y = 0; y < elem; y++) {
				sum += arrA[row][y] * arrB[y][col];
			}
			res[row][col] = sum;
			sum = 0;
		}
	}
}

void EMSC(double ** raw, double ** ref_spectra, double ** corrected, int nVars, int nObs, int refOrder) {

	//---------------------DECLARATIONS---------------------
	double temp[2] = { 0 };
	double ** M = initialize(refOrder + 3, nObs);
	double ** M_trans = initialize(nObs, refOrder + 3);
	double ** mult2_res = initialize(nObs, refOrder + 3);
	double ** mult1_res = initialize(refOrder+3, refOrder+3);
	double * std = (double *)malloc(nVars * sizeof(double));
	double * p = (double *)malloc((refOrder + 3) * sizeof(double));
	double ** K = initialize(refOrder-1, nVars);
	double mean_t, num = 0;

	//------------------------------------------------------

	//Calculates standard deviation of the ref_spectra
	//calculateSD(std, ref_spectra, nVars, refOrder);


	//something
	bsxfun(K,	ref_spectra, nVars, refOrder);
	xil_printf("bsxfun completed!\n");

	//Construct the M matrix
	for (int i = 0; i < nVars; i++) {

		//Add 1 in first row
		M[0][i] = 1;
		M_trans[i][0] = 1;

		//Add mean in second row
		temp[0] = ref_spectra[0][i];
		temp[1] = ref_spectra[1][i];
		mean_t = mean(2, temp);
		M[1][i] = mean_t;
		M_trans[i][1] = mean_t;


		for (int y = 0; y < refOrder-1; y++) {
			M[y + 2][i] = K[y][i];
			M_trans[i][y+2] = K[y][i];
		}


		//Add linspace and linspace squared
		M[refOrder+1][i] = num;
		M_trans[i][refOrder+1] = num;

		M[refOrder + 2][i] = pow(num, 2);
		M_trans[i][refOrder + 2] = pow(num, 2);
		num += (1.0 / (nVars-1));
	}
	xil_printf("Mmatrix completed!\n");
	//for (int i = 0; i < 52; i++) {
	//	printf("%f  %f  %f  %f  %f\n", M[0][i], M[1][i], M[2][i], M[3][i], M[4][i]);
	//}

	//Multiply M with M transpose, stores the result in mult1_res.
	matMult(M, M_trans, refOrder+3, refOrder+3, nVars, mult1_res);
	xil_printf("M'*M completed!\n");
	//for (int i = 0; i < 52; i++) {
	//	printf("%f  %f  %f  %f  %f\n", M_trans[i][0], M_trans[i][1], M_trans[i][2], M_trans[i][3], M_trans[i][4]);
	//}

	//for (int i = 0; i < 5; i++) {
	//	printf("%f  %f  %f  %f  %f\n", mult1_res[i][0], mult1_res[i][1], mult1_res[i][2], mult1_res[i][3], mult1_res[i][4]);
	//}

	//Calculates inverse
	double ** inverse = initialize(refOrder + 3, refOrder + 3);
	cofactor(mult1_res, refOrder + 3, inverse);
	xil_printf("Inverse completed!\n");

	//Multiplies the M transpose with the inverse to achieve: M'*inv(M*M'), stores the result in mult2_res.
	matMult(M_trans, inverse, nObs, refOrder + 3, refOrder + 3, mult2_res);
	xil_printf("M'*inv(M*M') completed!\n");
	xil_printf("Starting HW part\n");
	//THIS PART SHOULD BE DONE IN HARDWARE
	double sum = 0;
	u32 Value1;
	u32 Value2;

	init_timer(TMRCTR_DEVICE_ID, TIMER_COUNTER_0);
	Value1 = start_timer(TIMER_COUNTER_0);

	for (int idx = 0; idx < nObs; idx++) {

		for (int i = 0; i < refOrder + 3; i++) {
			for (int y = 0; y <nVars; y++) {
				sum += raw[idx][y] * mult2_res[y][i];
			}
			p[i] = sum;

			sum = 0;
		}

		for (int t = 0; t < nVars; t++) {
			corrected[idx][t] = (raw[idx][t] - p[0] - p[3] * M[refOrder + 1][t] - p[4] * M[refOrder + 2][t]) / p[1];
		}
	}

	Value2 = stop_timer(TIMER_COUNTER_0); //multiply by 10 nanoseconds 100 mhz
	//--------------------------------------
	xil_printf("Timer: %d\n", (Value2-Value1));
	free(M);
	free(M_trans);
	free(mult2_res);
	free(mult1_res);
	free(K);
	free(std);
	free(p);
	}

void execute_EMSC(double ** raw, double ** ref_spectra, double ** corrected, int nVars, int nObs, int refOrder, int bin_enable, int bin_size){
	double **raw_bin = initialize(bin_size, nVars);
	double **corrected_bin = initialize(bin_size, nVars);
	int it = nObs / bin_size;

	if (bin_enable == 1) {

		for (int round = 0; round < it; round++) {

			for (int x = 0; x < bin_size; x++) {
				for (int y = 0; y < nVars; y++) {
					raw_bin[x][y] = raw[x][y];
				}
			}

		EMSC(raw_bin, ref_spectra, corrected_bin, nVars, bin_size, refOrder);

		for (int row = 0; row < bin_size; row++) {
			for (int col = 0; col < nVars; col++) {
				corrected[row + round * 100][col] = corrected_bin[row][col];
			}
		}

		}
	}
	else {
		EMSC(raw, ref_spectra, corrected, nVars, nObs, refOrder);
	}
	free(raw_bin);
	free(corrected_bin);
}

//---------------------------------------------------------------------
// http://www.sanfoundry.com/c-program-find-inverse-matrix/
//---------------------------------------------------------------------
double determinant(double ** a, int k)
{
	double ** b = (double **)malloc(k * sizeof(double*));
	for (int row = 0; row < k; row++) {
		b[row] = (double*)malloc(k * sizeof(double));
	}

	double s = 1, det = 0;
	int i, j, m, n, c;
	if (k == 1)
	{
		return (a[0][0]);
	}
	else
	{
		det = 0;
		for (c = 0; c < k; c++)
		{
			m = 0;
			n = 0;
			for (i = 0; i < k; i++)
			{
				for (j = 0; j < k; j++)
				{
					b[i][j] = 0;
					if (i != 0 && j != c)
					{
						b[m][n] = a[i][j];
						if (n < (k - 2))
							n++;
						else
						{
							n = 0;
							m++;
						}
					}
				}
			}
			det = det + s * (a[0][c] * determinant(b, k - 1));
			s = -1 * s;
		}
	}

	return (det);
}

/*Finding transpose of matrix*/
void transpose(double ** num, double ** fac, double r, double ** res)
{
	int i, j;
	//double b[5][5], inverse[5][5], d;
	double d;
	double ** b = (double **)malloc(r * sizeof(double*));
	double ** inverse = (double **)malloc(r * sizeof(double*));
	for (int row = 0; row < r; row++) {
		b[row] = (double*)malloc(r * sizeof(double));
		inverse[row] = (double*)malloc(r * sizeof(double));
	}
	for (i = 0; i < r; i++)
	{
		for (j = 0; j < r; j++)
		{
			b[i][j] = fac[j][i];
		}
	}
	d = determinant(num, r);
	for (i = 0; i < r; i++)
	{
		for (j = 0; j < r; j++)
		{
			inverse[i][j] = b[i][j] / d;
		}
	}

	for (i = 0; i < r; i++)
	{
		for (j = 0; j < r; j++)
		{
			res[i][j] = inverse[i][j];
		}
	}
}

void cofactor(double ** num, double f, double ** res)
{
	double ** b = (double **)malloc(f * sizeof(double*));
	double ** fac = (double **)malloc(f * sizeof(double*));
	for (int row = 0; row < f; row++) {
		b[row] = (double*)malloc(f * sizeof(double));
		fac[row] = (double*)malloc(f * sizeof(double));
	}
	//double fac[6][6];
	int p, q, m, n, i, j;
	for (q = 0; q < f; q++)
	{
		for (p = 0; p < f; p++)
		{
			m = 0;
			n = 0;
			for (i = 0; i < f; i++)
			{
				for (j = 0; j < f; j++)
				{
					if (i != q && j != p)
					{
						b[m][n] = num[i][j];
						if (n < (f - 2))
							n++;
						else
						{
							n = 0;
							m++;
						}
					}
				}
			}
			fac[q][p] = pow(-1, q + p) * determinant(b, f - 1);
		}
	}
	transpose(num, fac, f, res);
}

//----------------------------------------------------------------------

void sendData(double ** arr, int x, int y){
	char temp[8];
	for(int row = 0; row < x; row++){
		 for(int col = 0; col < y; col++){
			 for(int i = 0; i < 8; i+= 1){
				 temp[i] = XUartPs_RecvByte(XPAR_PS7_UART_1_BASEADDR);
			 }

			 sprintf(temp,"%f", arr[row][col]);
			 print(temp);
		}
	}
}

//Timer
//----------------------------------------------------------------------


void init_timer(u16 DeviceId, u8 TmrCtrNumber){
	int Status;
		XTmrCtr *TmrCtrInstancePtr = &TimerCounter;

		/*
		 * Initialize the timer counter so that it's ready to use,
		 * specify the device ID that is generated in xparameters.h
		 */
		Status = XTmrCtr_Initialize(TmrCtrInstancePtr, DeviceId);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		/*
		 * Perform a self-test to ensure that the hardware was built
		 * correctly, use the 1st timer in the device (0)
		 */
		Status = XTmrCtr_SelfTest(TmrCtrInstancePtr, TmrCtrNumber);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		/*
		* Enable the Autoreload mode of the timer counters.
		*/


		return XST_SUCCESS;
}


u32 start_timer(u8 TmrCtrNumber){
	XTmrCtr *TmrCtrInstancePtr = &TimerCounter;
	XTmrCtr_SetOptions(TmrCtrInstancePtr, TmrCtrNumber,
							XTC_AUTO_RELOAD_OPTION);
	u32 val = XTmrCtr_GetValue(TmrCtrInstancePtr, TmrCtrNumber);
	XTmrCtr_Start(TmrCtrInstancePtr, TmrCtrNumber);
	return val;
}

u32 stop_timer(u8 TmrCtrNumber){
	XTmrCtr *TmrCtrInstancePtr = &TimerCounter;
	u32 val = XTmrCtr_GetValue(TmrCtrInstancePtr, TmrCtrNumber);
	XTmrCtr_SetOptions(TmrCtrInstancePtr, TmrCtrNumber, 0);
	return val;
}

//----------------------------------------------------------------------


void delay(int ticks) {
   for(int delay = 0; delay < ticks; delay++){
    	//do nothing;
   }
}




int main()
{
    //init_platform();

	float * mem_ptr = (uint32_t*)0x10000000;
	float a = (double)mem_ptr[0];
	a = mem_ptr[9999];

	/*
    //static uint32_t mem_ptr[520000];

    int nVars = 100; //wavelenghts
    int nObs  = 100; //m*n
    int refOrder = 2; //numbers of species in spectra
    int bin_enable = 0; //1 = enabled
    int bin_size = 100; // size of bins

    double **raw = initialize(nObs,nVars);

    double **ref_spectra = initialize(refOrder, nVars);
    double **corrected = initialize(nObs, nVars);
    int index = 0;


    for(int rows = 0; rows < nObs; rows++){
    	for(int cols = 0; cols < nVars; cols++){
    		raw[rows][cols] = (float)mem_ptr[index++];
    	}
    }
    xil_printf("Raw loaded!\n");
    float fval = raw[249999][51];
    int whole, thousandths;
    whole = fval;
    thousandths = (fval - whole) * 1000;
    xil_printf("%d.%3d\n", whole, thousandths);

    //receiveData(raw, nObs, nVars);

	//u32 Value1;
    //u32 Value2;

    //init_timer(TMRCTR_DEVICE_ID, TIMER_COUNTER_0);

    //Value1 = start_timer(TIMER_COUNTER_0);
    for(int i = 0; i<nVars; i++){
    	ref_spectra[0][i] = raw[2][i];
    	ref_spectra[1][i] = raw[92][i];
    }
    xil_printf("ESMC Starting!\n");
    execute_EMSC(raw,ref_spectra, corrected, nVars, nObs, refOrder, bin_enable, bin_size);
    //Value2 = stop_timer(TIMER_COUNTER_0); //multiply by 10 nanoseconds 100 mhz


    //sendData(corrected, nVars, nObs);
    free(raw);
    free(ref_spectra);
    //xil_printf("Timer: %d\n", (Value2-Value1));
    xil_printf("corrected: 0x%x\n",corrected);
    mem_ptr = (u_int32_t*)0x13197508;
    index = 0;
    for(int i = 0; i<nObs; i++){
    	for(int y = 0; y<nVars; y++){
    		mem_ptr[index] = (float)corrected[i][y];
    		index++;
    	}
    }
    xil_printf("DONE!");
    //cleanup_platform();
	*/
    return 0;
}
