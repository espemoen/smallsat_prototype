
import _winreg as winreg
import itertools
import serial
import time

def enumerate_serial_ports():
    """ Uses the Win32 registry to return an
        iterator of serial (COM) ports
        existing on this computer.
    """
    path = 'HARDWARE\\DEVICEMAP\\SERIALCOMM'
    try:
        key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, path)
    except WindowsError:
        raise IterationError

    for i in itertools.count():
        try:
            val = winreg.EnumValue(key, i)
            yield str(val[1])
        except EnvironmentError:
            break


with open("cube_test.txt") as f:
	content = f.readlines()

content = [x.strip() for x in content]


f.close()


print("Ready!\n");

ser = serial.Serial('COM3', 115200, timeout=None);

print("Recieved: " + ser.read(8));

w, h = 52, 10000;
corrected = [[0 for x in range(w)] for y in range(h)] 



for i in range(0, 520000):
		send = content[i];
		ser.write(send);
		#print("Sent: " + content[i]);
		#print("Recieved: " + ser.read(8));
		val = (float(i)/520000.00) * 100.0;
		if(i % 10000 == 0):
			print "%f %%" % val;
		a = ser.read(8);

for i in range(0,10000):
	for y in range(0, 52):
		ser.write("Ready!!!");
		print("Sent: " + "Ready!!!");
		rec = ser.read(8);
		print("Recieved: " + rec);
		corrected[i][y] = rec;

a = ser.read(5);
print(a);

F = open('re_corrected.txt','w');
for x in range(0,10000):
	for y in range(0,52):
		F.write(corrected[x][y]+"\n");

F.close;