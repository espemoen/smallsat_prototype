function [ corrected ] = emsc(raw, ref_spectra)
%EMSC_JF Summary of this function goes here
%   Detailed explanation goes here

K = bsxfun(@minus,ref_spectra(2:end,:), ref_spectra(1,:));
m = mean(ref_spectra);

nVars = size(raw, 2);
nObs = size(raw, 1);

wlens = linspace(0,1,nVars);
wlensSQ = wlens.^2;

M = [ones(1, nVars);
    m;
    K;
    wlens;
    wlensSQ];

corrected = raw;
for idx = 1:nObs
    p = raw(idx,:)*M'*pinv(M*M');
    corrected(idx,:) = (raw(idx,:) - p(1) - p(4)*wlens - p(5)*wlensSQ ) / p(2);
end

end

