close all
clear all

load test_data.mat
test_data_test = randi(4095,100,100);
figure
plot(test_data');
title('Test data(input)');
movegui('west');
axis tight

corrected = emsc(test_data, test_data([3 93],:));

figure
plot(corrected');
title('Plot from MATLAB calculations');
movegui('center');
axis tight
plot_result_from_zynq;

