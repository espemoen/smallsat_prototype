clear all
close all

load('pigments_pc.mat');


disp('This matlab script loads a full bil file into the matlab variable x_bil');
disp('and then makes an RGB image, and plots the spectrum of four pixels chosen');
disp('by the user. The first pixel should be from dark water -- its spectrum is');
disp('subtracted from the other three.');
disp(' '); disp(' ');

[FileName,PathName] = uigetfile('*.bil','Select the HICO *.bil file');

%% Get HDR file

% [FileName,PathName] = uigetfile('*.hdr','Select the HICO *.hdr file');
hdr = readHyperHeader([PathName FileName]);
ISF = 50;  % HICO image_scale_factor FROM HDR FILE

%% Get HICO file
% [FileName,PathName] = uigetfile('*.bil','Select the HICO *.bil file');

disp('          Loading BIL file ... please wait about 30 seconds to complete');
x_bil = multibandread([PathName FileName], [hdr.lines hdr.samples hdr.bands],...
    hdr.data_type, hdr.header_offset, hdr.interleave, hdr.byte_order, ...
    {'Row', 'Range', [651 1150]})./ISF;
%%
dims_3d = size(x_bil);

%% Create RGB Image
R = 42; G = 27; B =11;
% x_rgb = x_bil(321:420,141:240,[R G B]);
x_rgb = x_bil(:,:,[R G B]);

figure
imagesc((normalize(x_rgb)).^(1/2)); axis off; axis image;

%% Query User for Four Input Pixels for Processing
disp('          Click on four pixels:');
disp('          The first pixel should be from dark water.');
disp('          Pixel colors: 1-Black, 2-Blue, 3-Green, 4-Magenta');

[p1y, p1x] = ginput(1); [p2y, p2x] = ginput(1); [p3y, p3x] = ginput(1); [p4y, p4x] = ginput(1);

p1y = round(p1y); p1x = round(p1x); p2y = round(p2y); p2x = round(p2x);
p3y = round(p3y); p3x = round(p3x); p4y = round(p4y); p4x = round(p4x);

hold on;
plot(p1y, p1x, 'o' ,'MarkerSize', 8, 'MarkerEdgeColor','y', 'MarkerFaceColor', 'k' );
plot(p2y, p2x, 'o' ,'MarkerSize', 8, 'MarkerEdgeColor','y', 'MarkerFaceColor', 'b' );
plot(p3y, p3x, 'o' ,'MarkerSize', 8, 'MarkerEdgeColor','y', 'MarkerFaceColor', 'g' );
plot(p4y, p4x, 'o' ,'MarkerSize', 8, 'MarkerEdgeColor','y', 'MarkerFaceColor', 'm' );

% HICO Wavelenght Data (nanometers) --- Column Vector (87 x 1)
f_hico = hdr.wavelength*1000;

% Create arrays for spectra of selected pixels, Column Vector (87 x 1)
p1  = squeeze(squeeze(x_bil(p1x, p1y, :)));
p2  = squeeze(squeeze(x_bil(p2x, p2y, :)));
p3  = squeeze(squeeze(x_bil(p3x, p3y, :)));
p4  = squeeze(squeeze(x_bil(p4x, p4y, :)));

% Plot Visible spectra
figure; hold on;
plot(f_hico, p1, '--k'); plot(f_hico, p2, '--b'); plot(f_hico, p3, '--g'); plot(f_hico, p4, '--m');
plot(f_hico, p1, '.k'); plot(f_hico, p2, '.b'); plot(f_hico, p3, '.g'); plot(f_hico, p4, '.m');
axis([f_hico(1) f_hico(end) 0 80]);
title('Specta at Sampled Pixels'); xlabel('Wavelength (nanometers)'); ylabel('Radiance (W/m^2/micrometer/sr)');

% Plot Visible spectra minus dark pixel
figure; hold on;
plot(f_hico, p1, '--k'); plot(f_hico, p2-p1, '--b'); plot(f_hico, p3-p1, '--g'); plot(f_hico, p4-p1, '--m');
plot(f_hico, p1, '.k'); plot(f_hico, p2-p1, '.b'); plot(f_hico, p3-p1, '.g'); plot(f_hico, p4-p1, '.m');
axis([f_hico(1) f_hico(end) 0 80]);
title('Specta at Sampled Pixels Minus Dark Pixel'); xlabel('Wavelength (nanometers)'); ylabel('Corrected Radiance (W/m^2/micrometer/sr)');

clear R G B RA GA BA;
clear ISF OS FRA;
clear p1x p1y p2x p2y p3x p3y p4x p4y;

%%

hs_data = x_bil;
wRef = p2;
dRef = p1;
wlens = f_hico;

%% Subset

% x_small = x_bil(321:420,141:240,:);
% data_flat = reshape(x_small(:,:,1:52), 100*100, 52);

data_flat = reshape(x_bil(:,:,1:52), 500*500, 52);

data_flat = bsxfun(@rdivide, data_flat, p2(1:52)');

emsc_labels = {'Additive', 'Wlen Linear', 'Wlen Quadratic', 'PC1', 'PC2', 'PC3', 'PC4', 'Pigment mean'};

for j = 1:size(coeff, 2)
    pigm_abs(j,:) = interp1(wavelength, coeff(:,j)', f_hico(1:52));
end

mean_abs = interp1(wavelength, mu, f_hico(1:52))';

pigm_refl = (10.^(-pigm_abs));
mean_refl = (10.^(-mean_abs));

wlens = f_hico(1:52);

%% EMSC R

[corrected, P, M, G, K] = emsc(data_flat, pigm_refl, mean_refl, 2);

%% EMSC R Plots

figure
plot(wlens, corrected(1:10:end,:)')
title('EMSC - Reflectance')
axis tight

emsc2D = reshape(P, [500 500 size(P,2)]);

figure
for k = 1:size(P,2)
    subplot(2, size(P,2)/2, k)
    imagesc(emsc2D(:,:,k))
    axis image off
    colorbar
    title(emsc_labels(k))
end

%% EMSC A
[corrected, P, M, G, K] = emsc(-log10(data_flat), pigm_abs, mean_abs, 2);

%% EMSC A Plots

figure
plot(wlens, corrected(1:10:end,:)')
title('EMSC - Absorbance')
axis tight

emsc2D = reshape(P, [500 500 size(P,2)]);

figure
for k = 1:size(P,2)
    subplot(2, size(P,2)/2, k)
    imagesc(emsc2D(:,:,k))
    axis image off
    colorbar
    title(emsc_labels(k))
end