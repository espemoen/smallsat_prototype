// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Sat May  5 08:48:30 2018
// Host        : DESKTOP-0KA73P0 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               d:/MasterOppgave/smallsat_prototype/project_5/project_5.srcs/sources_1/bd/design_1/ip/design_1_cubedma_top_0_0/design_1_cubedma_top_0_0_stub.v
// Design      : design_1_cubedma_top_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "cubedma_top,Vivado 2017.4" *)
module design_1_cubedma_top_0_0(clk, aresetn, m_axi_mem_araddr, 
  m_axi_mem_arlen, m_axi_mem_arsize, m_axi_mem_arburst, m_axi_mem_arprot, 
  m_axi_mem_arcache, m_axi_mem_arvalid, m_axi_mem_arready, m_axi_mem_rdata, 
  m_axi_mem_rresp, m_axi_mem_rlast, m_axi_mem_rvalid, m_axi_mem_rready, m_axi_mem_awaddr, 
  m_axi_mem_awlen, m_axi_mem_awsize, m_axi_mem_awburst, m_axi_mem_awprot, 
  m_axi_mem_awcache, m_axi_mem_awvalid, m_axi_mem_awready, m_axi_mem_wdata, 
  m_axi_mem_wstrb, m_axi_mem_wlast, m_axi_mem_wvalid, m_axi_mem_wready, m_axi_mem_bresp, 
  m_axi_mem_bvalid, m_axi_mem_bready, m_axis_mm2s_tdata, m_axis_mm2s_tlast, 
  m_axis_mm2s_tvalid, m_axis_mm2s_tready, mm2s_ctrl, s_axis_s2mm_tdata, s_axis_s2mm_tlast, 
  s_axis_s2mm_tvalid, s_axis_s2mm_tready, s_axi_ctrl_status_awaddr, 
  s_axi_ctrl_status_awprot, s_axi_ctrl_status_awvalid, s_axi_ctrl_status_awready, 
  s_axi_ctrl_status_wdata, s_axi_ctrl_status_wstrb, s_axi_ctrl_status_wvalid, 
  s_axi_ctrl_status_wready, s_axi_ctrl_status_bresp, s_axi_ctrl_status_bvalid, 
  s_axi_ctrl_status_bready, s_axi_ctrl_status_araddr, s_axi_ctrl_status_arprot, 
  s_axi_ctrl_status_arvalid, s_axi_ctrl_status_arready, s_axi_ctrl_status_rdata, 
  s_axi_ctrl_status_rresp, s_axi_ctrl_status_rvalid, s_axi_ctrl_status_rready, mm2s_irq, 
  s2mm_irq)
/* synthesis syn_black_box black_box_pad_pin="clk,aresetn,m_axi_mem_araddr[31:0],m_axi_mem_arlen[3:0],m_axi_mem_arsize[2:0],m_axi_mem_arburst[1:0],m_axi_mem_arprot[2:0],m_axi_mem_arcache[3:0],m_axi_mem_arvalid,m_axi_mem_arready,m_axi_mem_rdata[63:0],m_axi_mem_rresp[1:0],m_axi_mem_rlast,m_axi_mem_rvalid,m_axi_mem_rready,m_axi_mem_awaddr[31:0],m_axi_mem_awlen[3:0],m_axi_mem_awsize[2:0],m_axi_mem_awburst[1:0],m_axi_mem_awprot[2:0],m_axi_mem_awcache[3:0],m_axi_mem_awvalid,m_axi_mem_awready,m_axi_mem_wdata[63:0],m_axi_mem_wstrb[7:0],m_axi_mem_wlast,m_axi_mem_wvalid,m_axi_mem_wready,m_axi_mem_bresp[1:0],m_axi_mem_bvalid,m_axi_mem_bready,m_axis_mm2s_tdata[15:0],m_axis_mm2s_tlast,m_axis_mm2s_tvalid,m_axis_mm2s_tready,mm2s_ctrl[1:0],s_axis_s2mm_tdata[63:0],s_axis_s2mm_tlast,s_axis_s2mm_tvalid,s_axis_s2mm_tready,s_axi_ctrl_status_awaddr[5:0],s_axi_ctrl_status_awprot[2:0],s_axi_ctrl_status_awvalid,s_axi_ctrl_status_awready,s_axi_ctrl_status_wdata[31:0],s_axi_ctrl_status_wstrb[3:0],s_axi_ctrl_status_wvalid,s_axi_ctrl_status_wready,s_axi_ctrl_status_bresp[1:0],s_axi_ctrl_status_bvalid,s_axi_ctrl_status_bready,s_axi_ctrl_status_araddr[5:0],s_axi_ctrl_status_arprot[2:0],s_axi_ctrl_status_arvalid,s_axi_ctrl_status_arready,s_axi_ctrl_status_rdata[31:0],s_axi_ctrl_status_rresp[1:0],s_axi_ctrl_status_rvalid,s_axi_ctrl_status_rready,mm2s_irq,s2mm_irq" */;
  input clk;
  input aresetn;
  output [31:0]m_axi_mem_araddr;
  output [3:0]m_axi_mem_arlen;
  output [2:0]m_axi_mem_arsize;
  output [1:0]m_axi_mem_arburst;
  output [2:0]m_axi_mem_arprot;
  output [3:0]m_axi_mem_arcache;
  output m_axi_mem_arvalid;
  input m_axi_mem_arready;
  input [63:0]m_axi_mem_rdata;
  input [1:0]m_axi_mem_rresp;
  input m_axi_mem_rlast;
  input m_axi_mem_rvalid;
  output m_axi_mem_rready;
  output [31:0]m_axi_mem_awaddr;
  output [3:0]m_axi_mem_awlen;
  output [2:0]m_axi_mem_awsize;
  output [1:0]m_axi_mem_awburst;
  output [2:0]m_axi_mem_awprot;
  output [3:0]m_axi_mem_awcache;
  output m_axi_mem_awvalid;
  input m_axi_mem_awready;
  output [63:0]m_axi_mem_wdata;
  output [7:0]m_axi_mem_wstrb;
  output m_axi_mem_wlast;
  output m_axi_mem_wvalid;
  input m_axi_mem_wready;
  input [1:0]m_axi_mem_bresp;
  input m_axi_mem_bvalid;
  output m_axi_mem_bready;
  output [15:0]m_axis_mm2s_tdata;
  output m_axis_mm2s_tlast;
  output m_axis_mm2s_tvalid;
  input m_axis_mm2s_tready;
  output [1:0]mm2s_ctrl;
  input [63:0]s_axis_s2mm_tdata;
  input s_axis_s2mm_tlast;
  input s_axis_s2mm_tvalid;
  output s_axis_s2mm_tready;
  input [5:0]s_axi_ctrl_status_awaddr;
  input [2:0]s_axi_ctrl_status_awprot;
  input s_axi_ctrl_status_awvalid;
  output s_axi_ctrl_status_awready;
  input [31:0]s_axi_ctrl_status_wdata;
  input [3:0]s_axi_ctrl_status_wstrb;
  input s_axi_ctrl_status_wvalid;
  output s_axi_ctrl_status_wready;
  output [1:0]s_axi_ctrl_status_bresp;
  output s_axi_ctrl_status_bvalid;
  input s_axi_ctrl_status_bready;
  input [5:0]s_axi_ctrl_status_araddr;
  input [2:0]s_axi_ctrl_status_arprot;
  input s_axi_ctrl_status_arvalid;
  output s_axi_ctrl_status_arready;
  output [31:0]s_axi_ctrl_status_rdata;
  output [1:0]s_axi_ctrl_status_rresp;
  output s_axi_ctrl_status_rvalid;
  input s_axi_ctrl_status_rready;
  output mm2s_irq;
  output s2mm_irq;
endmodule
