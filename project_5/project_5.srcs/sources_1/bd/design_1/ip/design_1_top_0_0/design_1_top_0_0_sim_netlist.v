// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Sat May  5 08:43:41 2018
// Host        : DESKTOP-0KA73P0 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               d:/MasterOppgave/smallsat_prototype/project_5/project_5.srcs/sources_1/bd/design_1/ip/design_1_top_0_0/design_1_top_0_0_sim_netlist.v
// Design      : design_1_top_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_top_0_0,top,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "top,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module design_1_top_0_0
   (clk,
    aresetn,
    p_irq,
    s_axis_tdata,
    s_axis_tvalid,
    s_axis_tready,
    s_axis_tlast,
    m_axis_tdata,
    m_axis_tvalid,
    m_axis_tready,
    m_axis_tlast,
    s_axi_ctrl_status_awaddr,
    s_axi_ctrl_status_awprot,
    s_axi_ctrl_status_awvalid,
    s_axi_ctrl_status_awready,
    s_axi_ctrl_status_wdata,
    s_axi_ctrl_status_wstrb,
    s_axi_ctrl_status_wvalid,
    s_axi_ctrl_status_wready,
    s_axi_ctrl_status_bresp,
    s_axi_ctrl_status_bvalid,
    s_axi_ctrl_status_bready,
    s_axi_ctrl_status_araddr,
    s_axi_ctrl_status_arprot,
    s_axi_ctrl_status_arvalid,
    s_axi_ctrl_status_arready,
    s_axi_ctrl_status_rdata,
    s_axi_ctrl_status_rresp,
    s_axi_ctrl_status_rvalid,
    s_axi_ctrl_status_rready);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk, ASSOCIATED_BUSIF m_axis:s_axis:s_axi_ctrl_status, ASSOCIATED_RESET aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0" *) input clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 aresetn RST" *) (* x_interface_parameter = "XIL_INTERFACENAME aresetn, POLARITY ACTIVE_LOW" *) input aresetn;
  (* x_interface_info = "xilinx.com:signal:interrupt:1.0 p_irq INTERRUPT" *) (* x_interface_parameter = "XIL_INTERFACENAME p_irq, SENSITIVITY LEVEL_HIGH, PortWidth 1" *) output p_irq;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 s_axis TDATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axis, TDATA_NUM_BYTES 2, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef" *) input [15:0]s_axis_tdata;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 s_axis TVALID" *) input s_axis_tvalid;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 s_axis TREADY" *) output s_axis_tready;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 s_axis TLAST" *) input s_axis_tlast;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 m_axis TDATA" *) (* x_interface_parameter = "XIL_INTERFACENAME m_axis, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef" *) output [63:0]m_axis_tdata;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 m_axis TVALID" *) output m_axis_tvalid;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 m_axis TREADY" *) input m_axis_tready;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 m_axis TLAST" *) output m_axis_tlast;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axi_ctrl_status, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [5:0]s_axi_ctrl_status_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status AWPROT" *) input [2:0]s_axi_ctrl_status_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status AWVALID" *) input s_axi_ctrl_status_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status AWREADY" *) output s_axi_ctrl_status_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status WDATA" *) input [31:0]s_axi_ctrl_status_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status WSTRB" *) input [3:0]s_axi_ctrl_status_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status WVALID" *) input s_axi_ctrl_status_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status WREADY" *) output s_axi_ctrl_status_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status BRESP" *) output [1:0]s_axi_ctrl_status_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status BVALID" *) output s_axi_ctrl_status_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status BREADY" *) input s_axi_ctrl_status_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status ARADDR" *) input [5:0]s_axi_ctrl_status_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status ARPROT" *) input [2:0]s_axi_ctrl_status_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status ARVALID" *) input s_axi_ctrl_status_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status ARREADY" *) output s_axi_ctrl_status_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status RDATA" *) output [31:0]s_axi_ctrl_status_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status RRESP" *) output [1:0]s_axi_ctrl_status_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status RVALID" *) output s_axi_ctrl_status_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_ctrl_status RREADY" *) input s_axi_ctrl_status_rready;

  wire \<const0> ;
  wire aresetn;
  wire clk;
  wire [63:0]\^m_axis_tdata ;
  wire m_axis_tlast;
  wire m_axis_tready;
  wire m_axis_tvalid;
  wire p_irq;
  wire [5:0]s_axi_ctrl_status_araddr;
  wire s_axi_ctrl_status_arready;
  wire s_axi_ctrl_status_arvalid;
  wire [5:0]s_axi_ctrl_status_awaddr;
  wire s_axi_ctrl_status_awready;
  wire s_axi_ctrl_status_awvalid;
  wire s_axi_ctrl_status_bready;
  wire s_axi_ctrl_status_bvalid;
  wire [31:0]s_axi_ctrl_status_rdata;
  wire s_axi_ctrl_status_rready;
  wire s_axi_ctrl_status_rvalid;
  wire [31:0]s_axi_ctrl_status_wdata;
  wire s_axi_ctrl_status_wready;
  wire [3:0]s_axi_ctrl_status_wstrb;
  wire s_axi_ctrl_status_wvalid;
  wire [15:0]s_axis_tdata;
  wire s_axis_tlast;
  wire s_axis_tready;
  wire s_axis_tvalid;

  assign m_axis_tdata[63] = \^m_axis_tdata [63];
  assign m_axis_tdata[62] = \^m_axis_tdata [63];
  assign m_axis_tdata[61] = \^m_axis_tdata [63];
  assign m_axis_tdata[60] = \^m_axis_tdata [63];
  assign m_axis_tdata[59] = \^m_axis_tdata [63];
  assign m_axis_tdata[58] = \^m_axis_tdata [63];
  assign m_axis_tdata[57] = \^m_axis_tdata [63];
  assign m_axis_tdata[56] = \^m_axis_tdata [63];
  assign m_axis_tdata[55] = \^m_axis_tdata [63];
  assign m_axis_tdata[54] = \^m_axis_tdata [63];
  assign m_axis_tdata[53] = \^m_axis_tdata [63];
  assign m_axis_tdata[52] = \^m_axis_tdata [63];
  assign m_axis_tdata[51] = \^m_axis_tdata [63];
  assign m_axis_tdata[50] = \^m_axis_tdata [63];
  assign m_axis_tdata[49] = \^m_axis_tdata [63];
  assign m_axis_tdata[48] = \^m_axis_tdata [63];
  assign m_axis_tdata[47:0] = \^m_axis_tdata [47:0];
  assign s_axi_ctrl_status_bresp[1] = \<const0> ;
  assign s_axi_ctrl_status_bresp[0] = \<const0> ;
  assign s_axi_ctrl_status_rresp[1] = \<const0> ;
  assign s_axi_ctrl_status_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  design_1_top_0_0_top U0
       (.aresetn(aresetn),
        .clk(clk),
        .m_axis_tdata({\^m_axis_tdata [63],\^m_axis_tdata [47:0]}),
        .m_axis_tlast(m_axis_tlast),
        .m_axis_tready(m_axis_tready),
        .m_axis_tvalid(m_axis_tvalid),
        .p_irq(p_irq),
        .s_axi_ctrl_status_araddr(s_axi_ctrl_status_araddr[5:2]),
        .s_axi_ctrl_status_arready(s_axi_ctrl_status_arready),
        .s_axi_ctrl_status_arvalid(s_axi_ctrl_status_arvalid),
        .s_axi_ctrl_status_awaddr(s_axi_ctrl_status_awaddr[5:2]),
        .s_axi_ctrl_status_awready(s_axi_ctrl_status_awready),
        .s_axi_ctrl_status_awvalid(s_axi_ctrl_status_awvalid),
        .s_axi_ctrl_status_bready(s_axi_ctrl_status_bready),
        .s_axi_ctrl_status_bvalid(s_axi_ctrl_status_bvalid),
        .s_axi_ctrl_status_rdata(s_axi_ctrl_status_rdata),
        .s_axi_ctrl_status_rready(s_axi_ctrl_status_rready),
        .s_axi_ctrl_status_rvalid(s_axi_ctrl_status_rvalid),
        .s_axi_ctrl_status_wdata(s_axi_ctrl_status_wdata),
        .s_axi_ctrl_status_wready(s_axi_ctrl_status_wready),
        .s_axi_ctrl_status_wstrb(s_axi_ctrl_status_wstrb),
        .s_axi_ctrl_status_wvalid(s_axi_ctrl_status_wvalid),
        .s_axis_tdata(s_axis_tdata),
        .s_axis_tlast(s_axis_tlast),
        .s_axis_tready(s_axis_tready),
        .s_axis_tvalid(s_axis_tvalid));
endmodule

(* ORIG_REF_NAME = "axi_gearbox" *) 
module design_1_top_0_0_axi_gearbox
   (m_axis_tvalid,
    m_axis_tlast,
    p_irq,
    m_axis_tdata,
    clk,
    aresetn_0,
    m_axis_tready,
    last_p_reg,
    aresetn,
    E,
    p_rdy,
    D);
  output m_axis_tvalid;
  output m_axis_tlast;
  output p_irq;
  output [48:0]m_axis_tdata;
  input clk;
  input aresetn_0;
  input m_axis_tready;
  input last_p_reg;
  input aresetn;
  input [0:0]E;
  input p_rdy;
  input [383:0]D;

  wire [383:0]D;
  wire [0:0]E;
  wire aresetn;
  wire aresetn_0;
  wire clk;
  wire [2:0]counter;
  wire \counter[0]__0_i_1_n_0 ;
  wire \counter[0]_i_1__0_n_0 ;
  wire \counter[1]__0_i_1_n_0 ;
  wire \counter[1]_i_1_n_0 ;
  wire \counter[1]_i_2__0_n_0 ;
  wire \counter[2]__0_i_1_n_0 ;
  wire \counter[2]_i_1_n_0 ;
  wire \counter[2]_i_2_n_0 ;
  wire [2:0]counter_reg;
  wire last_flag;
  wire last_flag_i_1_n_0;
  wire last_p_reg;
  wire last_t_valid;
  wire [48:0]m_axis_tdata;
  wire [8:8]m_axis_tdata3;
  wire \m_axis_tdata[0]_i_1_n_0 ;
  wire \m_axis_tdata[0]_i_2_n_0 ;
  wire \m_axis_tdata[0]_i_3_n_0 ;
  wire \m_axis_tdata[0]_i_4_n_0 ;
  wire \m_axis_tdata[0]_i_5_n_0 ;
  wire \m_axis_tdata[10]_i_1_n_0 ;
  wire \m_axis_tdata[10]_i_2_n_0 ;
  wire \m_axis_tdata[10]_i_3_n_0 ;
  wire \m_axis_tdata[10]_i_4_n_0 ;
  wire \m_axis_tdata[10]_i_5_n_0 ;
  wire \m_axis_tdata[11]_i_1_n_0 ;
  wire \m_axis_tdata[11]_i_2_n_0 ;
  wire \m_axis_tdata[11]_i_3_n_0 ;
  wire \m_axis_tdata[11]_i_4_n_0 ;
  wire \m_axis_tdata[11]_i_5_n_0 ;
  wire \m_axis_tdata[12]_i_1_n_0 ;
  wire \m_axis_tdata[12]_i_2_n_0 ;
  wire \m_axis_tdata[12]_i_3_n_0 ;
  wire \m_axis_tdata[12]_i_4_n_0 ;
  wire \m_axis_tdata[12]_i_5_n_0 ;
  wire \m_axis_tdata[13]_i_1_n_0 ;
  wire \m_axis_tdata[13]_i_2_n_0 ;
  wire \m_axis_tdata[13]_i_3_n_0 ;
  wire \m_axis_tdata[13]_i_4_n_0 ;
  wire \m_axis_tdata[13]_i_5_n_0 ;
  wire \m_axis_tdata[14]_i_1_n_0 ;
  wire \m_axis_tdata[14]_i_2_n_0 ;
  wire \m_axis_tdata[14]_i_3_n_0 ;
  wire \m_axis_tdata[14]_i_4_n_0 ;
  wire \m_axis_tdata[14]_i_5_n_0 ;
  wire \m_axis_tdata[15]_i_1_n_0 ;
  wire \m_axis_tdata[15]_i_2_n_0 ;
  wire \m_axis_tdata[15]_i_3_n_0 ;
  wire \m_axis_tdata[15]_i_4_n_0 ;
  wire \m_axis_tdata[15]_i_5_n_0 ;
  wire \m_axis_tdata[16]_i_1_n_0 ;
  wire \m_axis_tdata[16]_i_2_n_0 ;
  wire \m_axis_tdata[16]_i_3_n_0 ;
  wire \m_axis_tdata[16]_i_4_n_0 ;
  wire \m_axis_tdata[16]_i_5_n_0 ;
  wire \m_axis_tdata[17]_i_1_n_0 ;
  wire \m_axis_tdata[17]_i_2_n_0 ;
  wire \m_axis_tdata[17]_i_3_n_0 ;
  wire \m_axis_tdata[17]_i_4_n_0 ;
  wire \m_axis_tdata[17]_i_5_n_0 ;
  wire \m_axis_tdata[18]_i_1_n_0 ;
  wire \m_axis_tdata[18]_i_2_n_0 ;
  wire \m_axis_tdata[18]_i_3_n_0 ;
  wire \m_axis_tdata[18]_i_4_n_0 ;
  wire \m_axis_tdata[18]_i_5_n_0 ;
  wire \m_axis_tdata[19]_i_1_n_0 ;
  wire \m_axis_tdata[19]_i_2_n_0 ;
  wire \m_axis_tdata[19]_i_3_n_0 ;
  wire \m_axis_tdata[19]_i_4_n_0 ;
  wire \m_axis_tdata[19]_i_5_n_0 ;
  wire \m_axis_tdata[1]_i_1_n_0 ;
  wire \m_axis_tdata[1]_i_2_n_0 ;
  wire \m_axis_tdata[1]_i_3_n_0 ;
  wire \m_axis_tdata[1]_i_4_n_0 ;
  wire \m_axis_tdata[1]_i_5_n_0 ;
  wire \m_axis_tdata[20]_i_1_n_0 ;
  wire \m_axis_tdata[20]_i_2_n_0 ;
  wire \m_axis_tdata[20]_i_3_n_0 ;
  wire \m_axis_tdata[20]_i_4_n_0 ;
  wire \m_axis_tdata[20]_i_5_n_0 ;
  wire \m_axis_tdata[21]_i_1_n_0 ;
  wire \m_axis_tdata[21]_i_2_n_0 ;
  wire \m_axis_tdata[21]_i_3_n_0 ;
  wire \m_axis_tdata[21]_i_4_n_0 ;
  wire \m_axis_tdata[21]_i_5_n_0 ;
  wire \m_axis_tdata[22]_i_1_n_0 ;
  wire \m_axis_tdata[22]_i_2_n_0 ;
  wire \m_axis_tdata[22]_i_3_n_0 ;
  wire \m_axis_tdata[22]_i_4_n_0 ;
  wire \m_axis_tdata[22]_i_5_n_0 ;
  wire \m_axis_tdata[23]_i_1_n_0 ;
  wire \m_axis_tdata[23]_i_2_n_0 ;
  wire \m_axis_tdata[23]_i_3_n_0 ;
  wire \m_axis_tdata[23]_i_4_n_0 ;
  wire \m_axis_tdata[23]_i_5_n_0 ;
  wire \m_axis_tdata[24]_i_1_n_0 ;
  wire \m_axis_tdata[24]_i_2_n_0 ;
  wire \m_axis_tdata[24]_i_3_n_0 ;
  wire \m_axis_tdata[24]_i_4_n_0 ;
  wire \m_axis_tdata[24]_i_5_n_0 ;
  wire \m_axis_tdata[25]_i_1_n_0 ;
  wire \m_axis_tdata[25]_i_2_n_0 ;
  wire \m_axis_tdata[25]_i_3_n_0 ;
  wire \m_axis_tdata[25]_i_4_n_0 ;
  wire \m_axis_tdata[25]_i_5_n_0 ;
  wire \m_axis_tdata[26]_i_1_n_0 ;
  wire \m_axis_tdata[26]_i_2_n_0 ;
  wire \m_axis_tdata[26]_i_3_n_0 ;
  wire \m_axis_tdata[26]_i_4_n_0 ;
  wire \m_axis_tdata[26]_i_5_n_0 ;
  wire \m_axis_tdata[27]_i_1_n_0 ;
  wire \m_axis_tdata[27]_i_2_n_0 ;
  wire \m_axis_tdata[27]_i_3_n_0 ;
  wire \m_axis_tdata[27]_i_4_n_0 ;
  wire \m_axis_tdata[27]_i_5_n_0 ;
  wire \m_axis_tdata[28]_i_1_n_0 ;
  wire \m_axis_tdata[28]_i_2_n_0 ;
  wire \m_axis_tdata[28]_i_3_n_0 ;
  wire \m_axis_tdata[28]_i_4_n_0 ;
  wire \m_axis_tdata[28]_i_5_n_0 ;
  wire \m_axis_tdata[29]_i_1_n_0 ;
  wire \m_axis_tdata[29]_i_2_n_0 ;
  wire \m_axis_tdata[29]_i_3_n_0 ;
  wire \m_axis_tdata[29]_i_4_n_0 ;
  wire \m_axis_tdata[29]_i_5_n_0 ;
  wire \m_axis_tdata[2]_i_1_n_0 ;
  wire \m_axis_tdata[2]_i_2_n_0 ;
  wire \m_axis_tdata[2]_i_3_n_0 ;
  wire \m_axis_tdata[2]_i_4_n_0 ;
  wire \m_axis_tdata[2]_i_5_n_0 ;
  wire \m_axis_tdata[30]_i_1_n_0 ;
  wire \m_axis_tdata[30]_i_2_n_0 ;
  wire \m_axis_tdata[30]_i_3_n_0 ;
  wire \m_axis_tdata[30]_i_4_n_0 ;
  wire \m_axis_tdata[30]_i_5_n_0 ;
  wire \m_axis_tdata[31]_i_1_n_0 ;
  wire \m_axis_tdata[31]_i_2_n_0 ;
  wire \m_axis_tdata[32]_i_1_n_0 ;
  wire \m_axis_tdata[32]_i_2_n_0 ;
  wire \m_axis_tdata[32]_i_3_n_0 ;
  wire \m_axis_tdata[32]_i_4_n_0 ;
  wire \m_axis_tdata[32]_i_5_n_0 ;
  wire \m_axis_tdata[32]_i_6_n_0 ;
  wire \m_axis_tdata[32]_i_7_n_0 ;
  wire \m_axis_tdata[32]_i_8_n_0 ;
  wire \m_axis_tdata[33]_i_1_n_0 ;
  wire \m_axis_tdata[33]_i_2_n_0 ;
  wire \m_axis_tdata[33]_i_3_n_0 ;
  wire \m_axis_tdata[33]_i_4_n_0 ;
  wire \m_axis_tdata[33]_i_5_n_0 ;
  wire \m_axis_tdata[33]_i_6_n_0 ;
  wire \m_axis_tdata[33]_i_7_n_0 ;
  wire \m_axis_tdata[33]_i_8_n_0 ;
  wire \m_axis_tdata[34]_i_1_n_0 ;
  wire \m_axis_tdata[34]_i_2_n_0 ;
  wire \m_axis_tdata[34]_i_3_n_0 ;
  wire \m_axis_tdata[34]_i_4_n_0 ;
  wire \m_axis_tdata[34]_i_5_n_0 ;
  wire \m_axis_tdata[34]_i_6_n_0 ;
  wire \m_axis_tdata[34]_i_7_n_0 ;
  wire \m_axis_tdata[34]_i_8_n_0 ;
  wire \m_axis_tdata[35]_i_1_n_0 ;
  wire \m_axis_tdata[35]_i_2_n_0 ;
  wire \m_axis_tdata[35]_i_3_n_0 ;
  wire \m_axis_tdata[35]_i_4_n_0 ;
  wire \m_axis_tdata[35]_i_5_n_0 ;
  wire \m_axis_tdata[35]_i_6_n_0 ;
  wire \m_axis_tdata[35]_i_7_n_0 ;
  wire \m_axis_tdata[35]_i_8_n_0 ;
  wire \m_axis_tdata[36]_i_1_n_0 ;
  wire \m_axis_tdata[36]_i_2_n_0 ;
  wire \m_axis_tdata[36]_i_3_n_0 ;
  wire \m_axis_tdata[36]_i_4_n_0 ;
  wire \m_axis_tdata[36]_i_5_n_0 ;
  wire \m_axis_tdata[36]_i_6_n_0 ;
  wire \m_axis_tdata[36]_i_7_n_0 ;
  wire \m_axis_tdata[36]_i_8_n_0 ;
  wire \m_axis_tdata[37]_i_1_n_0 ;
  wire \m_axis_tdata[37]_i_2_n_0 ;
  wire \m_axis_tdata[37]_i_3_n_0 ;
  wire \m_axis_tdata[37]_i_4_n_0 ;
  wire \m_axis_tdata[37]_i_5_n_0 ;
  wire \m_axis_tdata[37]_i_6_n_0 ;
  wire \m_axis_tdata[37]_i_7_n_0 ;
  wire \m_axis_tdata[37]_i_8_n_0 ;
  wire \m_axis_tdata[38]_i_1_n_0 ;
  wire \m_axis_tdata[38]_i_2_n_0 ;
  wire \m_axis_tdata[38]_i_3_n_0 ;
  wire \m_axis_tdata[38]_i_4_n_0 ;
  wire \m_axis_tdata[38]_i_5_n_0 ;
  wire \m_axis_tdata[38]_i_6_n_0 ;
  wire \m_axis_tdata[38]_i_7_n_0 ;
  wire \m_axis_tdata[38]_i_8_n_0 ;
  wire \m_axis_tdata[39]_i_1_n_0 ;
  wire \m_axis_tdata[39]_i_2_n_0 ;
  wire \m_axis_tdata[39]_i_3_n_0 ;
  wire \m_axis_tdata[39]_i_4_n_0 ;
  wire \m_axis_tdata[39]_i_5_n_0 ;
  wire \m_axis_tdata[39]_i_6_n_0 ;
  wire \m_axis_tdata[39]_i_7_n_0 ;
  wire \m_axis_tdata[39]_i_8_n_0 ;
  wire \m_axis_tdata[3]_i_1_n_0 ;
  wire \m_axis_tdata[3]_i_2_n_0 ;
  wire \m_axis_tdata[3]_i_3_n_0 ;
  wire \m_axis_tdata[3]_i_4_n_0 ;
  wire \m_axis_tdata[3]_i_5_n_0 ;
  wire \m_axis_tdata[40]_i_1_n_0 ;
  wire \m_axis_tdata[40]_i_2_n_0 ;
  wire \m_axis_tdata[40]_i_3_n_0 ;
  wire \m_axis_tdata[40]_i_4_n_0 ;
  wire \m_axis_tdata[40]_i_5_n_0 ;
  wire \m_axis_tdata[40]_i_6_n_0 ;
  wire \m_axis_tdata[40]_i_7_n_0 ;
  wire \m_axis_tdata[40]_i_8_n_0 ;
  wire \m_axis_tdata[41]_i_1_n_0 ;
  wire \m_axis_tdata[41]_i_2_n_0 ;
  wire \m_axis_tdata[41]_i_3_n_0 ;
  wire \m_axis_tdata[41]_i_4_n_0 ;
  wire \m_axis_tdata[41]_i_5_n_0 ;
  wire \m_axis_tdata[41]_i_6_n_0 ;
  wire \m_axis_tdata[41]_i_7_n_0 ;
  wire \m_axis_tdata[41]_i_8_n_0 ;
  wire \m_axis_tdata[42]_i_1_n_0 ;
  wire \m_axis_tdata[42]_i_2_n_0 ;
  wire \m_axis_tdata[42]_i_3_n_0 ;
  wire \m_axis_tdata[42]_i_4_n_0 ;
  wire \m_axis_tdata[42]_i_5_n_0 ;
  wire \m_axis_tdata[42]_i_6_n_0 ;
  wire \m_axis_tdata[42]_i_7_n_0 ;
  wire \m_axis_tdata[42]_i_8_n_0 ;
  wire \m_axis_tdata[43]_i_1_n_0 ;
  wire \m_axis_tdata[43]_i_2_n_0 ;
  wire \m_axis_tdata[43]_i_3_n_0 ;
  wire \m_axis_tdata[43]_i_4_n_0 ;
  wire \m_axis_tdata[43]_i_5_n_0 ;
  wire \m_axis_tdata[43]_i_6_n_0 ;
  wire \m_axis_tdata[43]_i_7_n_0 ;
  wire \m_axis_tdata[43]_i_8_n_0 ;
  wire \m_axis_tdata[44]_i_1_n_0 ;
  wire \m_axis_tdata[44]_i_2_n_0 ;
  wire \m_axis_tdata[44]_i_3_n_0 ;
  wire \m_axis_tdata[44]_i_4_n_0 ;
  wire \m_axis_tdata[44]_i_5_n_0 ;
  wire \m_axis_tdata[44]_i_6_n_0 ;
  wire \m_axis_tdata[44]_i_7_n_0 ;
  wire \m_axis_tdata[44]_i_8_n_0 ;
  wire \m_axis_tdata[45]_i_1_n_0 ;
  wire \m_axis_tdata[45]_i_2_n_0 ;
  wire \m_axis_tdata[45]_i_3_n_0 ;
  wire \m_axis_tdata[45]_i_4_n_0 ;
  wire \m_axis_tdata[45]_i_5_n_0 ;
  wire \m_axis_tdata[45]_i_6_n_0 ;
  wire \m_axis_tdata[45]_i_7_n_0 ;
  wire \m_axis_tdata[45]_i_8_n_0 ;
  wire \m_axis_tdata[46]_i_1_n_0 ;
  wire \m_axis_tdata[46]_i_2_n_0 ;
  wire \m_axis_tdata[46]_i_3_n_0 ;
  wire \m_axis_tdata[46]_i_4_n_0 ;
  wire \m_axis_tdata[46]_i_5_n_0 ;
  wire \m_axis_tdata[46]_i_6_n_0 ;
  wire \m_axis_tdata[46]_i_7_n_0 ;
  wire \m_axis_tdata[46]_i_8_n_0 ;
  wire \m_axis_tdata[47]_i_10_n_0 ;
  wire \m_axis_tdata[47]_i_11_n_0 ;
  wire \m_axis_tdata[47]_i_12_n_0 ;
  wire \m_axis_tdata[47]_i_1_n_0 ;
  wire \m_axis_tdata[47]_i_2_n_0 ;
  wire \m_axis_tdata[47]_i_3_n_0 ;
  wire \m_axis_tdata[47]_i_5_n_0 ;
  wire \m_axis_tdata[47]_i_6_n_0 ;
  wire \m_axis_tdata[47]_i_7_n_0 ;
  wire \m_axis_tdata[47]_i_8_n_0 ;
  wire \m_axis_tdata[47]_i_9_n_0 ;
  wire \m_axis_tdata[4]_i_1_n_0 ;
  wire \m_axis_tdata[4]_i_2_n_0 ;
  wire \m_axis_tdata[4]_i_3_n_0 ;
  wire \m_axis_tdata[4]_i_4_n_0 ;
  wire \m_axis_tdata[4]_i_5_n_0 ;
  wire \m_axis_tdata[5]_i_1_n_0 ;
  wire \m_axis_tdata[5]_i_2_n_0 ;
  wire \m_axis_tdata[5]_i_3_n_0 ;
  wire \m_axis_tdata[5]_i_4_n_0 ;
  wire \m_axis_tdata[5]_i_5_n_0 ;
  wire \m_axis_tdata[63]_i_10_n_0 ;
  wire \m_axis_tdata[63]_i_11_n_0 ;
  wire \m_axis_tdata[63]_i_12_n_0 ;
  wire \m_axis_tdata[63]_i_13_n_0 ;
  wire \m_axis_tdata[63]_i_14_n_0 ;
  wire \m_axis_tdata[63]_i_1_n_0 ;
  wire \m_axis_tdata[63]_i_2_n_0 ;
  wire \m_axis_tdata[63]_i_4_n_0 ;
  wire \m_axis_tdata[63]_i_5_n_0 ;
  wire \m_axis_tdata[63]_i_7_n_0 ;
  wire \m_axis_tdata[63]_i_8_n_0 ;
  wire \m_axis_tdata[63]_i_9_n_0 ;
  wire \m_axis_tdata[6]_i_1_n_0 ;
  wire \m_axis_tdata[6]_i_2_n_0 ;
  wire \m_axis_tdata[6]_i_3_n_0 ;
  wire \m_axis_tdata[6]_i_4_n_0 ;
  wire \m_axis_tdata[6]_i_5_n_0 ;
  wire \m_axis_tdata[7]_i_1_n_0 ;
  wire \m_axis_tdata[7]_i_2_n_0 ;
  wire \m_axis_tdata[7]_i_3_n_0 ;
  wire \m_axis_tdata[7]_i_4_n_0 ;
  wire \m_axis_tdata[7]_i_5_n_0 ;
  wire \m_axis_tdata[8]_i_1_n_0 ;
  wire \m_axis_tdata[8]_i_2_n_0 ;
  wire \m_axis_tdata[8]_i_3_n_0 ;
  wire \m_axis_tdata[8]_i_4_n_0 ;
  wire \m_axis_tdata[8]_i_5_n_0 ;
  wire \m_axis_tdata[9]_i_1_n_0 ;
  wire \m_axis_tdata[9]_i_2_n_0 ;
  wire \m_axis_tdata[9]_i_3_n_0 ;
  wire \m_axis_tdata[9]_i_4_n_0 ;
  wire \m_axis_tdata[9]_i_5_n_0 ;
  wire \m_axis_tdata_reg[47]_i_4_n_0 ;
  wire \m_axis_tdata_reg[47]_i_4_n_1 ;
  wire \m_axis_tdata_reg[47]_i_4_n_2 ;
  wire \m_axis_tdata_reg[47]_i_4_n_3 ;
  wire \m_axis_tdata_reg[47]_i_4_n_4 ;
  wire \m_axis_tdata_reg[47]_i_4_n_5 ;
  wire \m_axis_tdata_reg[47]_i_4_n_6 ;
  wire \m_axis_tdata_reg[47]_i_4_n_7 ;
  wire m_axis_tlast;
  wire m_axis_tlast_i_1_n_0;
  wire m_axis_tready;
  wire m_axis_tvalid;
  wire p_irq;
  wire p_rdy;
  wire [383:0]res_mem;
  wire start;
  wire start_i_1_n_0;
  wire t_valid_flag_i_1_n_0;
  wire [3:0]\NLW_m_axis_tdata_reg[63]_i_6_CO_UNCONNECTED ;
  wire [3:1]\NLW_m_axis_tdata_reg[63]_i_6_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hFFFF0FFF00007000)) 
    \counter[0]__0_i_1 
       (.I0(counter_reg[2]),
        .I1(counter_reg[1]),
        .I2(last_flag),
        .I3(m_axis_tvalid),
        .I4(last_p_reg),
        .I5(counter_reg[0]),
        .O(\counter[0]__0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF007000000080)) 
    \counter[0]_i_1__0 
       (.I0(m_axis_tvalid),
        .I1(m_axis_tready),
        .I2(start),
        .I3(\m_axis_tdata[63]_i_7_n_0 ),
        .I4(p_rdy),
        .I5(counter[0]),
        .O(\counter[0]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF1FFF0000A000)) 
    \counter[1]__0_i_1 
       (.I0(counter_reg[0]),
        .I1(counter_reg[2]),
        .I2(last_flag),
        .I3(m_axis_tvalid),
        .I4(last_p_reg),
        .I5(counter_reg[1]),
        .O(\counter[1]__0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFF080008)) 
    \counter[1]_i_1 
       (.I0(\counter[1]_i_2__0_n_0 ),
        .I1(start),
        .I2(\m_axis_tdata[63]_i_7_n_0 ),
        .I3(p_rdy),
        .I4(counter[1]),
        .O(\counter[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \counter[1]_i_2__0 
       (.I0(counter[0]),
        .I1(m_axis_tvalid),
        .I2(m_axis_tready),
        .I3(counter[1]),
        .O(\counter[1]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF5FFF00008000)) 
    \counter[2]__0_i_1 
       (.I0(counter_reg[1]),
        .I1(counter_reg[0]),
        .I2(last_flag),
        .I3(m_axis_tvalid),
        .I4(last_p_reg),
        .I5(counter_reg[2]),
        .O(\counter[2]__0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFF080008)) 
    \counter[2]_i_1 
       (.I0(\counter[2]_i_2_n_0 ),
        .I1(start),
        .I2(\m_axis_tdata[63]_i_7_n_0 ),
        .I3(p_rdy),
        .I4(counter[2]),
        .O(\counter[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \counter[2]_i_2 
       (.I0(counter[2]),
        .I1(m_axis_tready),
        .I2(m_axis_tvalid),
        .I3(counter[0]),
        .I4(counter[1]),
        .O(\counter[2]_i_2_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .CLR(aresetn_0),
        .D(\counter[0]_i_1__0_n_0 ),
        .Q(counter[0]));
  FDCE \counter_reg[0]__0 
       (.C(clk),
        .CE(1'b1),
        .CLR(aresetn_0),
        .D(\counter[0]__0_i_1_n_0 ),
        .Q(counter_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .CLR(aresetn_0),
        .D(\counter[1]_i_1_n_0 ),
        .Q(counter[1]));
  FDCE \counter_reg[1]__0 
       (.C(clk),
        .CE(1'b1),
        .CLR(aresetn_0),
        .D(\counter[1]__0_i_1_n_0 ),
        .Q(counter_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .CLR(aresetn_0),
        .D(\counter[2]_i_1_n_0 ),
        .Q(counter[2]));
  FDCE \counter_reg[2]__0 
       (.C(clk),
        .CE(1'b1),
        .CLR(aresetn_0),
        .D(\counter[2]__0_i_1_n_0 ),
        .Q(counter_reg[2]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT5 #(
    .INIT(32'hF7F0FFF0)) 
    last_flag_i_1
       (.I0(counter_reg[2]),
        .I1(counter_reg[1]),
        .I2(last_p_reg),
        .I3(last_flag),
        .I4(m_axis_tvalid),
        .O(last_flag_i_1_n_0));
  FDCE last_flag_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(aresetn_0),
        .D(last_flag_i_1_n_0),
        .Q(last_flag));
  FDCE last_t_valid_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(aresetn_0),
        .D(m_axis_tvalid),
        .Q(last_t_valid));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[0]_i_1 
       (.I0(\m_axis_tdata[0]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[0]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[16]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[0]_i_2 
       (.I0(\m_axis_tdata[32]_i_6_n_0 ),
        .I1(\m_axis_tdata[16]_i_4_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[32]_i_8_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[0]_i_4_n_0 ),
        .O(\m_axis_tdata[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5353535050505350)) 
    \m_axis_tdata[0]_i_3 
       (.I0(\m_axis_tdata[0]_i_5_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I3(res_mem[256]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I5(res_mem[320]),
        .O(\m_axis_tdata[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[0]_i_4 
       (.I0(res_mem[192]),
        .I1(res_mem[64]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[128]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[0]),
        .O(\m_axis_tdata[0]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[0]_i_5 
       (.I0(res_mem[352]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[288]),
        .O(\m_axis_tdata[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000222EE2EE)) 
    \m_axis_tdata[10]_i_1 
       (.I0(\m_axis_tdata[10]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[10]_i_3_n_0 ),
        .I4(\m_axis_tdata[26]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[10]_i_2 
       (.I0(\m_axis_tdata[42]_i_6_n_0 ),
        .I1(\m_axis_tdata[26]_i_4_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[42]_i_8_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[10]_i_4_n_0 ),
        .O(\m_axis_tdata[10]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBBBB8B88BBBB8BBB)) 
    \m_axis_tdata[10]_i_3 
       (.I0(\m_axis_tdata[10]_i_5_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I2(res_mem[330]),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[266]),
        .O(\m_axis_tdata[10]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[10]_i_4 
       (.I0(res_mem[202]),
        .I1(res_mem[74]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[138]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[10]),
        .O(\m_axis_tdata[10]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[10]_i_5 
       (.I0(res_mem[362]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[298]),
        .O(\m_axis_tdata[10]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[11]_i_1 
       (.I0(\m_axis_tdata[11]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[11]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[27]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[11]_i_2 
       (.I0(\m_axis_tdata[43]_i_6_n_0 ),
        .I1(\m_axis_tdata[27]_i_4_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[43]_i_8_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[11]_i_4_n_0 ),
        .O(\m_axis_tdata[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5353535050505350)) 
    \m_axis_tdata[11]_i_3 
       (.I0(\m_axis_tdata[11]_i_5_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I3(res_mem[267]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I5(res_mem[331]),
        .O(\m_axis_tdata[11]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[11]_i_4 
       (.I0(res_mem[203]),
        .I1(res_mem[75]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[139]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[11]),
        .O(\m_axis_tdata[11]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[11]_i_5 
       (.I0(res_mem[363]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[299]),
        .O(\m_axis_tdata[11]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000222EE2EE)) 
    \m_axis_tdata[12]_i_1 
       (.I0(\m_axis_tdata[12]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[12]_i_3_n_0 ),
        .I4(\m_axis_tdata[28]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[12]_i_2 
       (.I0(\m_axis_tdata[44]_i_6_n_0 ),
        .I1(\m_axis_tdata[28]_i_4_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[44]_i_8_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[12]_i_4_n_0 ),
        .O(\m_axis_tdata[12]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBBBB8B88BBBB8BBB)) 
    \m_axis_tdata[12]_i_3 
       (.I0(\m_axis_tdata[12]_i_5_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I2(res_mem[332]),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[268]),
        .O(\m_axis_tdata[12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[12]_i_4 
       (.I0(res_mem[204]),
        .I1(res_mem[76]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[140]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[12]),
        .O(\m_axis_tdata[12]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[12]_i_5 
       (.I0(res_mem[364]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[300]),
        .O(\m_axis_tdata[12]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[13]_i_1 
       (.I0(\m_axis_tdata[13]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[13]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[29]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[13]_i_2 
       (.I0(\m_axis_tdata[45]_i_6_n_0 ),
        .I1(\m_axis_tdata[29]_i_4_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[45]_i_8_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[13]_i_4_n_0 ),
        .O(\m_axis_tdata[13]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5353535050505350)) 
    \m_axis_tdata[13]_i_3 
       (.I0(\m_axis_tdata[13]_i_5_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I3(res_mem[269]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I5(res_mem[333]),
        .O(\m_axis_tdata[13]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[13]_i_4 
       (.I0(res_mem[205]),
        .I1(res_mem[77]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[141]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[13]),
        .O(\m_axis_tdata[13]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[13]_i_5 
       (.I0(res_mem[365]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[301]),
        .O(\m_axis_tdata[13]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000222EE2EE)) 
    \m_axis_tdata[14]_i_1 
       (.I0(\m_axis_tdata[14]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[14]_i_3_n_0 ),
        .I4(\m_axis_tdata[30]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[14]_i_2 
       (.I0(\m_axis_tdata[46]_i_6_n_0 ),
        .I1(\m_axis_tdata[30]_i_4_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[46]_i_8_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[14]_i_4_n_0 ),
        .O(\m_axis_tdata[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBBBB8B88BBBB8BBB)) 
    \m_axis_tdata[14]_i_3 
       (.I0(\m_axis_tdata[14]_i_5_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I2(res_mem[334]),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[270]),
        .O(\m_axis_tdata[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[14]_i_4 
       (.I0(res_mem[206]),
        .I1(res_mem[78]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[142]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[14]),
        .O(\m_axis_tdata[14]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[14]_i_5 
       (.I0(res_mem[366]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[302]),
        .O(\m_axis_tdata[14]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000222EE2EE)) 
    \m_axis_tdata[15]_i_1 
       (.I0(\m_axis_tdata[15]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[15]_i_3_n_0 ),
        .I4(\m_axis_tdata[31]_i_2_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[15]_i_2 
       (.I0(\m_axis_tdata[63]_i_10_n_0 ),
        .I1(\m_axis_tdata[63]_i_11_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[63]_i_9_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[15]_i_4_n_0 ),
        .O(\m_axis_tdata[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBBBB8B88BBBB8BBB)) 
    \m_axis_tdata[15]_i_3 
       (.I0(\m_axis_tdata[15]_i_5_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I2(res_mem[335]),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[271]),
        .O(\m_axis_tdata[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[15]_i_4 
       (.I0(res_mem[207]),
        .I1(res_mem[79]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[143]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[15]),
        .O(\m_axis_tdata[15]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[15]_i_5 
       (.I0(res_mem[367]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[303]),
        .O(\m_axis_tdata[15]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[16]_i_1 
       (.I0(\m_axis_tdata[16]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[16]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[32]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[16]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[16]_i_2 
       (.I0(\m_axis_tdata[32]_i_7_n_0 ),
        .I1(\m_axis_tdata[32]_i_8_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[32]_i_6_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[16]_i_4_n_0 ),
        .O(\m_axis_tdata[16]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF45404540)) 
    \m_axis_tdata[16]_i_3 
       (.I0(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I1(res_mem[336]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[272]),
        .I4(\m_axis_tdata[16]_i_5_n_0 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .O(\m_axis_tdata[16]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[16]_i_4 
       (.I0(res_mem[208]),
        .I1(res_mem[80]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[144]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[16]),
        .O(\m_axis_tdata[16]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[16]_i_5 
       (.I0(res_mem[368]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[304]),
        .O(\m_axis_tdata[16]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[17]_i_1 
       (.I0(\m_axis_tdata[17]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[17]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[33]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[17]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[17]_i_2 
       (.I0(\m_axis_tdata[33]_i_7_n_0 ),
        .I1(\m_axis_tdata[33]_i_8_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[33]_i_6_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[17]_i_4_n_0 ),
        .O(\m_axis_tdata[17]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF45404540)) 
    \m_axis_tdata[17]_i_3 
       (.I0(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I1(res_mem[337]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[273]),
        .I4(\m_axis_tdata[17]_i_5_n_0 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .O(\m_axis_tdata[17]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[17]_i_4 
       (.I0(res_mem[209]),
        .I1(res_mem[81]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[145]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[17]),
        .O(\m_axis_tdata[17]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[17]_i_5 
       (.I0(res_mem[369]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[305]),
        .O(\m_axis_tdata[17]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[18]_i_1 
       (.I0(\m_axis_tdata[18]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[18]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[34]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[18]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[18]_i_2 
       (.I0(\m_axis_tdata[34]_i_7_n_0 ),
        .I1(\m_axis_tdata[34]_i_8_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[34]_i_6_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[18]_i_4_n_0 ),
        .O(\m_axis_tdata[18]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF45404540)) 
    \m_axis_tdata[18]_i_3 
       (.I0(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I1(res_mem[338]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[274]),
        .I4(\m_axis_tdata[18]_i_5_n_0 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .O(\m_axis_tdata[18]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[18]_i_4 
       (.I0(res_mem[210]),
        .I1(res_mem[82]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[146]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[18]),
        .O(\m_axis_tdata[18]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[18]_i_5 
       (.I0(res_mem[370]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[306]),
        .O(\m_axis_tdata[18]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[19]_i_1 
       (.I0(\m_axis_tdata[19]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[19]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[35]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[19]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[19]_i_2 
       (.I0(\m_axis_tdata[35]_i_7_n_0 ),
        .I1(\m_axis_tdata[35]_i_8_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[35]_i_6_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[19]_i_4_n_0 ),
        .O(\m_axis_tdata[19]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF45404540)) 
    \m_axis_tdata[19]_i_3 
       (.I0(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I1(res_mem[339]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[275]),
        .I4(\m_axis_tdata[19]_i_5_n_0 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .O(\m_axis_tdata[19]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[19]_i_4 
       (.I0(res_mem[211]),
        .I1(res_mem[83]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[147]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[19]),
        .O(\m_axis_tdata[19]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[19]_i_5 
       (.I0(res_mem[371]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[307]),
        .O(\m_axis_tdata[19]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[1]_i_1 
       (.I0(\m_axis_tdata[1]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[1]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[17]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[1]_i_2 
       (.I0(\m_axis_tdata[33]_i_6_n_0 ),
        .I1(\m_axis_tdata[17]_i_4_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[33]_i_8_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[1]_i_4_n_0 ),
        .O(\m_axis_tdata[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5353535050505350)) 
    \m_axis_tdata[1]_i_3 
       (.I0(\m_axis_tdata[1]_i_5_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I3(res_mem[257]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I5(res_mem[321]),
        .O(\m_axis_tdata[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[1]_i_4 
       (.I0(res_mem[193]),
        .I1(res_mem[65]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[129]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[1]),
        .O(\m_axis_tdata[1]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[1]_i_5 
       (.I0(res_mem[353]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[289]),
        .O(\m_axis_tdata[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[20]_i_1 
       (.I0(\m_axis_tdata[20]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[20]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[36]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[20]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[20]_i_2 
       (.I0(\m_axis_tdata[36]_i_7_n_0 ),
        .I1(\m_axis_tdata[36]_i_8_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[36]_i_6_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[20]_i_4_n_0 ),
        .O(\m_axis_tdata[20]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF45404540)) 
    \m_axis_tdata[20]_i_3 
       (.I0(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I1(res_mem[340]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[276]),
        .I4(\m_axis_tdata[20]_i_5_n_0 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .O(\m_axis_tdata[20]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[20]_i_4 
       (.I0(res_mem[212]),
        .I1(res_mem[84]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[148]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[20]),
        .O(\m_axis_tdata[20]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[20]_i_5 
       (.I0(res_mem[372]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[308]),
        .O(\m_axis_tdata[20]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000E222E2EE)) 
    \m_axis_tdata[21]_i_1 
       (.I0(\m_axis_tdata[21]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[37]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[21]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[21]_i_2 
       (.I0(\m_axis_tdata[37]_i_7_n_0 ),
        .I1(\m_axis_tdata[37]_i_8_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[37]_i_6_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[21]_i_4_n_0 ),
        .O(\m_axis_tdata[21]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBBBB8B88BBBB8BBB)) 
    \m_axis_tdata[21]_i_3 
       (.I0(\m_axis_tdata[21]_i_5_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I2(res_mem[341]),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[277]),
        .O(\m_axis_tdata[21]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[21]_i_4 
       (.I0(res_mem[213]),
        .I1(res_mem[85]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[149]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[21]),
        .O(\m_axis_tdata[21]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[21]_i_5 
       (.I0(res_mem[373]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[309]),
        .O(\m_axis_tdata[21]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[22]_i_1 
       (.I0(\m_axis_tdata[22]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[22]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[38]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[22]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[22]_i_2 
       (.I0(\m_axis_tdata[38]_i_7_n_0 ),
        .I1(\m_axis_tdata[38]_i_8_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[38]_i_6_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[22]_i_4_n_0 ),
        .O(\m_axis_tdata[22]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF45404540)) 
    \m_axis_tdata[22]_i_3 
       (.I0(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I1(res_mem[342]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[278]),
        .I4(\m_axis_tdata[22]_i_5_n_0 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .O(\m_axis_tdata[22]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[22]_i_4 
       (.I0(res_mem[214]),
        .I1(res_mem[86]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[150]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[22]),
        .O(\m_axis_tdata[22]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[22]_i_5 
       (.I0(res_mem[374]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[310]),
        .O(\m_axis_tdata[22]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[23]_i_1 
       (.I0(\m_axis_tdata[23]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[23]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[39]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[23]_i_2 
       (.I0(\m_axis_tdata[39]_i_7_n_0 ),
        .I1(\m_axis_tdata[39]_i_8_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[39]_i_6_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[23]_i_4_n_0 ),
        .O(\m_axis_tdata[23]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF45404540)) 
    \m_axis_tdata[23]_i_3 
       (.I0(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I1(res_mem[343]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[279]),
        .I4(\m_axis_tdata[23]_i_5_n_0 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .O(\m_axis_tdata[23]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[23]_i_4 
       (.I0(res_mem[215]),
        .I1(res_mem[87]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[151]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[23]),
        .O(\m_axis_tdata[23]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[23]_i_5 
       (.I0(res_mem[375]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[311]),
        .O(\m_axis_tdata[23]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000E222E2EE)) 
    \m_axis_tdata[24]_i_1 
       (.I0(\m_axis_tdata[24]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[40]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[24]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[24]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[24]_i_2 
       (.I0(\m_axis_tdata[40]_i_7_n_0 ),
        .I1(\m_axis_tdata[40]_i_8_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[40]_i_6_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[24]_i_4_n_0 ),
        .O(\m_axis_tdata[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBBBB8B88BBBB8BBB)) 
    \m_axis_tdata[24]_i_3 
       (.I0(\m_axis_tdata[24]_i_5_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I2(res_mem[344]),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[280]),
        .O(\m_axis_tdata[24]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[24]_i_4 
       (.I0(res_mem[216]),
        .I1(res_mem[88]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[152]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[24]),
        .O(\m_axis_tdata[24]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[24]_i_5 
       (.I0(res_mem[376]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[312]),
        .O(\m_axis_tdata[24]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[25]_i_1 
       (.I0(\m_axis_tdata[25]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[25]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[41]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[25]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[25]_i_2 
       (.I0(\m_axis_tdata[41]_i_7_n_0 ),
        .I1(\m_axis_tdata[41]_i_8_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[41]_i_6_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[25]_i_4_n_0 ),
        .O(\m_axis_tdata[25]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF45404540)) 
    \m_axis_tdata[25]_i_3 
       (.I0(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I1(res_mem[345]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[281]),
        .I4(\m_axis_tdata[25]_i_5_n_0 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .O(\m_axis_tdata[25]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[25]_i_4 
       (.I0(res_mem[217]),
        .I1(res_mem[89]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[153]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[25]),
        .O(\m_axis_tdata[25]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[25]_i_5 
       (.I0(res_mem[377]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[313]),
        .O(\m_axis_tdata[25]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000E222E2EE)) 
    \m_axis_tdata[26]_i_1 
       (.I0(\m_axis_tdata[26]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[42]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[26]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[26]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[26]_i_2 
       (.I0(\m_axis_tdata[42]_i_7_n_0 ),
        .I1(\m_axis_tdata[42]_i_8_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[42]_i_6_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[26]_i_4_n_0 ),
        .O(\m_axis_tdata[26]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBBBB8B88BBBB8BBB)) 
    \m_axis_tdata[26]_i_3 
       (.I0(\m_axis_tdata[26]_i_5_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I2(res_mem[346]),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[282]),
        .O(\m_axis_tdata[26]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[26]_i_4 
       (.I0(res_mem[218]),
        .I1(res_mem[90]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[154]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[26]),
        .O(\m_axis_tdata[26]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[26]_i_5 
       (.I0(res_mem[378]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[314]),
        .O(\m_axis_tdata[26]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[27]_i_1 
       (.I0(\m_axis_tdata[27]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[27]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[43]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[27]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[27]_i_2 
       (.I0(\m_axis_tdata[43]_i_7_n_0 ),
        .I1(\m_axis_tdata[43]_i_8_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[43]_i_6_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[27]_i_4_n_0 ),
        .O(\m_axis_tdata[27]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF45404540)) 
    \m_axis_tdata[27]_i_3 
       (.I0(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I1(res_mem[347]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[283]),
        .I4(\m_axis_tdata[27]_i_5_n_0 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .O(\m_axis_tdata[27]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[27]_i_4 
       (.I0(res_mem[219]),
        .I1(res_mem[91]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[155]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[27]),
        .O(\m_axis_tdata[27]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[27]_i_5 
       (.I0(res_mem[379]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[315]),
        .O(\m_axis_tdata[27]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000E222E2EE)) 
    \m_axis_tdata[28]_i_1 
       (.I0(\m_axis_tdata[28]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[44]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[28]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[28]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[28]_i_2 
       (.I0(\m_axis_tdata[44]_i_7_n_0 ),
        .I1(\m_axis_tdata[44]_i_8_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[44]_i_6_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[28]_i_4_n_0 ),
        .O(\m_axis_tdata[28]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBBBB8B88BBBB8BBB)) 
    \m_axis_tdata[28]_i_3 
       (.I0(\m_axis_tdata[28]_i_5_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I2(res_mem[348]),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[284]),
        .O(\m_axis_tdata[28]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[28]_i_4 
       (.I0(res_mem[220]),
        .I1(res_mem[92]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[156]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[28]),
        .O(\m_axis_tdata[28]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[28]_i_5 
       (.I0(res_mem[380]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[316]),
        .O(\m_axis_tdata[28]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[29]_i_1 
       (.I0(\m_axis_tdata[29]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[29]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[45]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[29]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[29]_i_2 
       (.I0(\m_axis_tdata[45]_i_7_n_0 ),
        .I1(\m_axis_tdata[45]_i_8_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[45]_i_6_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[29]_i_4_n_0 ),
        .O(\m_axis_tdata[29]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF45404540)) 
    \m_axis_tdata[29]_i_3 
       (.I0(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I1(res_mem[349]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[285]),
        .I4(\m_axis_tdata[29]_i_5_n_0 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .O(\m_axis_tdata[29]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[29]_i_4 
       (.I0(res_mem[221]),
        .I1(res_mem[93]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[157]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[29]),
        .O(\m_axis_tdata[29]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[29]_i_5 
       (.I0(res_mem[381]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[317]),
        .O(\m_axis_tdata[29]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[2]_i_1 
       (.I0(\m_axis_tdata[2]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[2]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[18]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[2]_i_2 
       (.I0(\m_axis_tdata[34]_i_6_n_0 ),
        .I1(\m_axis_tdata[18]_i_4_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[34]_i_8_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[2]_i_4_n_0 ),
        .O(\m_axis_tdata[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF45404540)) 
    \m_axis_tdata[2]_i_3 
       (.I0(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I1(res_mem[322]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[258]),
        .I4(\m_axis_tdata[2]_i_5_n_0 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .O(\m_axis_tdata[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[2]_i_4 
       (.I0(res_mem[194]),
        .I1(res_mem[66]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[130]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[2]),
        .O(\m_axis_tdata[2]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[2]_i_5 
       (.I0(res_mem[354]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[290]),
        .O(\m_axis_tdata[2]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000E222E2EE)) 
    \m_axis_tdata[30]_i_1 
       (.I0(\m_axis_tdata[30]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[46]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[30]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[30]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[30]_i_2 
       (.I0(\m_axis_tdata[46]_i_7_n_0 ),
        .I1(\m_axis_tdata[46]_i_8_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[46]_i_6_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[30]_i_4_n_0 ),
        .O(\m_axis_tdata[30]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBBBB8B88BBBB8BBB)) 
    \m_axis_tdata[30]_i_3 
       (.I0(\m_axis_tdata[30]_i_5_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I2(res_mem[350]),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[286]),
        .O(\m_axis_tdata[30]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[30]_i_4 
       (.I0(res_mem[222]),
        .I1(res_mem[94]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[158]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[30]),
        .O(\m_axis_tdata[30]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[30]_i_5 
       (.I0(res_mem[382]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[318]),
        .O(\m_axis_tdata[30]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000E222E2EE)) 
    \m_axis_tdata[31]_i_1 
       (.I0(\m_axis_tdata[63]_i_4_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[47]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[31]_i_2_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBBBB8B88BBBB8BBB)) 
    \m_axis_tdata[31]_i_2 
       (.I0(\m_axis_tdata[63]_i_12_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I2(res_mem[351]),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[287]),
        .O(\m_axis_tdata[31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[32]_i_1 
       (.I0(\m_axis_tdata[32]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[32]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[32]_i_4_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[32]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[32]_i_2 
       (.I0(\m_axis_tdata[32]_i_5_n_0 ),
        .I1(\m_axis_tdata[32]_i_6_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[32]_i_7_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[32]_i_8_n_0 ),
        .O(\m_axis_tdata[32]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000F0A0C00000A0C)) 
    \m_axis_tdata[32]_i_3 
       (.I0(res_mem[352]),
        .I1(res_mem[288]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(res_mem[320]),
        .O(\m_axis_tdata[32]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000A0A0F000C0C)) 
    \m_axis_tdata[32]_i_4 
       (.I0(res_mem[368]),
        .I1(res_mem[304]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[336]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .O(\m_axis_tdata[32]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[32]_i_5 
       (.I0(res_mem[272]),
        .I1(res_mem[144]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[208]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[80]),
        .O(\m_axis_tdata[32]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[32]_i_6 
       (.I0(res_mem[240]),
        .I1(res_mem[112]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[176]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[48]),
        .O(\m_axis_tdata[32]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[32]_i_7 
       (.I0(res_mem[256]),
        .I1(res_mem[128]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[192]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[64]),
        .O(\m_axis_tdata[32]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[32]_i_8 
       (.I0(res_mem[224]),
        .I1(res_mem[96]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[160]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[32]),
        .O(\m_axis_tdata[32]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[33]_i_1 
       (.I0(\m_axis_tdata[33]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[33]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[33]_i_4_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[33]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[33]_i_2 
       (.I0(\m_axis_tdata[33]_i_5_n_0 ),
        .I1(\m_axis_tdata[33]_i_6_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[33]_i_7_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[33]_i_8_n_0 ),
        .O(\m_axis_tdata[33]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000F0A0C00000A0C)) 
    \m_axis_tdata[33]_i_3 
       (.I0(res_mem[353]),
        .I1(res_mem[289]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(res_mem[321]),
        .O(\m_axis_tdata[33]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000A0A0F000C0C)) 
    \m_axis_tdata[33]_i_4 
       (.I0(res_mem[369]),
        .I1(res_mem[305]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[337]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .O(\m_axis_tdata[33]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[33]_i_5 
       (.I0(res_mem[273]),
        .I1(res_mem[145]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[209]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[81]),
        .O(\m_axis_tdata[33]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[33]_i_6 
       (.I0(res_mem[241]),
        .I1(res_mem[113]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[177]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[49]),
        .O(\m_axis_tdata[33]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[33]_i_7 
       (.I0(res_mem[257]),
        .I1(res_mem[129]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[193]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[65]),
        .O(\m_axis_tdata[33]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[33]_i_8 
       (.I0(res_mem[225]),
        .I1(res_mem[97]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[161]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[33]),
        .O(\m_axis_tdata[33]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[34]_i_1 
       (.I0(\m_axis_tdata[34]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[34]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[34]_i_4_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[34]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[34]_i_2 
       (.I0(\m_axis_tdata[34]_i_5_n_0 ),
        .I1(\m_axis_tdata[34]_i_6_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[34]_i_7_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[34]_i_8_n_0 ),
        .O(\m_axis_tdata[34]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000A0A0F000C0C)) 
    \m_axis_tdata[34]_i_3 
       (.I0(res_mem[354]),
        .I1(res_mem[290]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[322]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .O(\m_axis_tdata[34]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000A0A0F000C0C)) 
    \m_axis_tdata[34]_i_4 
       (.I0(res_mem[370]),
        .I1(res_mem[306]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[338]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .O(\m_axis_tdata[34]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[34]_i_5 
       (.I0(res_mem[274]),
        .I1(res_mem[146]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[210]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[82]),
        .O(\m_axis_tdata[34]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[34]_i_6 
       (.I0(res_mem[242]),
        .I1(res_mem[114]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[178]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[50]),
        .O(\m_axis_tdata[34]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[34]_i_7 
       (.I0(res_mem[258]),
        .I1(res_mem[130]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[194]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[66]),
        .O(\m_axis_tdata[34]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[34]_i_8 
       (.I0(res_mem[226]),
        .I1(res_mem[98]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[162]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[34]),
        .O(\m_axis_tdata[34]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[35]_i_1 
       (.I0(\m_axis_tdata[35]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[35]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[35]_i_4_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[35]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[35]_i_2 
       (.I0(\m_axis_tdata[35]_i_5_n_0 ),
        .I1(\m_axis_tdata[35]_i_6_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[35]_i_7_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[35]_i_8_n_0 ),
        .O(\m_axis_tdata[35]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000A0A0F000C0C)) 
    \m_axis_tdata[35]_i_3 
       (.I0(res_mem[355]),
        .I1(res_mem[291]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[323]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .O(\m_axis_tdata[35]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000A0A0F000C0C)) 
    \m_axis_tdata[35]_i_4 
       (.I0(res_mem[371]),
        .I1(res_mem[307]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[339]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .O(\m_axis_tdata[35]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[35]_i_5 
       (.I0(res_mem[275]),
        .I1(res_mem[147]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[211]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[83]),
        .O(\m_axis_tdata[35]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[35]_i_6 
       (.I0(res_mem[243]),
        .I1(res_mem[115]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[179]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[51]),
        .O(\m_axis_tdata[35]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[35]_i_7 
       (.I0(res_mem[259]),
        .I1(res_mem[131]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[195]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[67]),
        .O(\m_axis_tdata[35]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[35]_i_8 
       (.I0(res_mem[227]),
        .I1(res_mem[99]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[163]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[35]),
        .O(\m_axis_tdata[35]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[36]_i_1 
       (.I0(\m_axis_tdata[36]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[36]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[36]_i_4_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[36]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[36]_i_2 
       (.I0(\m_axis_tdata[36]_i_5_n_0 ),
        .I1(\m_axis_tdata[36]_i_6_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[36]_i_7_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[36]_i_8_n_0 ),
        .O(\m_axis_tdata[36]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000F0A0C00000A0C)) 
    \m_axis_tdata[36]_i_3 
       (.I0(res_mem[356]),
        .I1(res_mem[292]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(res_mem[324]),
        .O(\m_axis_tdata[36]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000A0A0F000C0C)) 
    \m_axis_tdata[36]_i_4 
       (.I0(res_mem[372]),
        .I1(res_mem[308]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[340]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .O(\m_axis_tdata[36]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[36]_i_5 
       (.I0(res_mem[276]),
        .I1(res_mem[148]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[212]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[84]),
        .O(\m_axis_tdata[36]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[36]_i_6 
       (.I0(res_mem[244]),
        .I1(res_mem[116]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[180]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[52]),
        .O(\m_axis_tdata[36]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[36]_i_7 
       (.I0(res_mem[260]),
        .I1(res_mem[132]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[196]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[68]),
        .O(\m_axis_tdata[36]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[36]_i_8 
       (.I0(res_mem[228]),
        .I1(res_mem[100]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[164]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[36]),
        .O(\m_axis_tdata[36]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[37]_i_1 
       (.I0(\m_axis_tdata[37]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[37]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[37]_i_4_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[37]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[37]_i_2 
       (.I0(\m_axis_tdata[37]_i_5_n_0 ),
        .I1(\m_axis_tdata[37]_i_6_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[37]_i_7_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[37]_i_8_n_0 ),
        .O(\m_axis_tdata[37]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000F0A0C00000A0C)) 
    \m_axis_tdata[37]_i_3 
       (.I0(res_mem[357]),
        .I1(res_mem[293]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(res_mem[325]),
        .O(\m_axis_tdata[37]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000A0A0F000C0C)) 
    \m_axis_tdata[37]_i_4 
       (.I0(res_mem[373]),
        .I1(res_mem[309]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[341]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .O(\m_axis_tdata[37]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[37]_i_5 
       (.I0(res_mem[277]),
        .I1(res_mem[149]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[213]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[85]),
        .O(\m_axis_tdata[37]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[37]_i_6 
       (.I0(res_mem[245]),
        .I1(res_mem[117]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[181]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[53]),
        .O(\m_axis_tdata[37]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[37]_i_7 
       (.I0(res_mem[261]),
        .I1(res_mem[133]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[197]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[69]),
        .O(\m_axis_tdata[37]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[37]_i_8 
       (.I0(res_mem[229]),
        .I1(res_mem[101]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[165]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[37]),
        .O(\m_axis_tdata[37]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[38]_i_1 
       (.I0(\m_axis_tdata[38]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[38]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[38]_i_4_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[38]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[38]_i_2 
       (.I0(\m_axis_tdata[38]_i_5_n_0 ),
        .I1(\m_axis_tdata[38]_i_6_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[38]_i_7_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[38]_i_8_n_0 ),
        .O(\m_axis_tdata[38]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000F0A0C00000A0C)) 
    \m_axis_tdata[38]_i_3 
       (.I0(res_mem[358]),
        .I1(res_mem[294]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(res_mem[326]),
        .O(\m_axis_tdata[38]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000A0A0F000C0C)) 
    \m_axis_tdata[38]_i_4 
       (.I0(res_mem[374]),
        .I1(res_mem[310]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[342]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .O(\m_axis_tdata[38]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[38]_i_5 
       (.I0(res_mem[278]),
        .I1(res_mem[150]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[214]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[86]),
        .O(\m_axis_tdata[38]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[38]_i_6 
       (.I0(res_mem[246]),
        .I1(res_mem[118]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[182]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[54]),
        .O(\m_axis_tdata[38]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[38]_i_7 
       (.I0(res_mem[262]),
        .I1(res_mem[134]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[198]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[70]),
        .O(\m_axis_tdata[38]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[38]_i_8 
       (.I0(res_mem[230]),
        .I1(res_mem[102]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[166]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[38]),
        .O(\m_axis_tdata[38]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[39]_i_1 
       (.I0(\m_axis_tdata[39]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[39]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[39]_i_4_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[39]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[39]_i_2 
       (.I0(\m_axis_tdata[39]_i_5_n_0 ),
        .I1(\m_axis_tdata[39]_i_6_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[39]_i_7_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[39]_i_8_n_0 ),
        .O(\m_axis_tdata[39]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000F0A0C00000A0C)) 
    \m_axis_tdata[39]_i_3 
       (.I0(res_mem[359]),
        .I1(res_mem[295]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(res_mem[327]),
        .O(\m_axis_tdata[39]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000A0A0F000C0C)) 
    \m_axis_tdata[39]_i_4 
       (.I0(res_mem[375]),
        .I1(res_mem[311]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[343]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .O(\m_axis_tdata[39]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[39]_i_5 
       (.I0(res_mem[279]),
        .I1(res_mem[151]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[215]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[87]),
        .O(\m_axis_tdata[39]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[39]_i_6 
       (.I0(res_mem[247]),
        .I1(res_mem[119]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[183]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[55]),
        .O(\m_axis_tdata[39]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[39]_i_7 
       (.I0(res_mem[263]),
        .I1(res_mem[135]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[199]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[71]),
        .O(\m_axis_tdata[39]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[39]_i_8 
       (.I0(res_mem[231]),
        .I1(res_mem[103]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[167]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[39]),
        .O(\m_axis_tdata[39]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[3]_i_1 
       (.I0(\m_axis_tdata[3]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[3]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[19]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[3]_i_2 
       (.I0(\m_axis_tdata[35]_i_6_n_0 ),
        .I1(\m_axis_tdata[19]_i_4_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[35]_i_8_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[3]_i_4_n_0 ),
        .O(\m_axis_tdata[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF45404540)) 
    \m_axis_tdata[3]_i_3 
       (.I0(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I1(res_mem[323]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[259]),
        .I4(\m_axis_tdata[3]_i_5_n_0 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .O(\m_axis_tdata[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[3]_i_4 
       (.I0(res_mem[195]),
        .I1(res_mem[67]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[131]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[3]),
        .O(\m_axis_tdata[3]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[3]_i_5 
       (.I0(res_mem[355]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[291]),
        .O(\m_axis_tdata[3]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[40]_i_1 
       (.I0(\m_axis_tdata[40]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[40]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[40]_i_4_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[40]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[40]_i_2 
       (.I0(\m_axis_tdata[40]_i_5_n_0 ),
        .I1(\m_axis_tdata[40]_i_6_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[40]_i_7_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[40]_i_8_n_0 ),
        .O(\m_axis_tdata[40]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000F0A0C00000A0C)) 
    \m_axis_tdata[40]_i_3 
       (.I0(res_mem[360]),
        .I1(res_mem[296]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(res_mem[328]),
        .O(\m_axis_tdata[40]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000A0A0F000C0C)) 
    \m_axis_tdata[40]_i_4 
       (.I0(res_mem[376]),
        .I1(res_mem[312]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[344]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .O(\m_axis_tdata[40]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[40]_i_5 
       (.I0(res_mem[280]),
        .I1(res_mem[152]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[216]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[88]),
        .O(\m_axis_tdata[40]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[40]_i_6 
       (.I0(res_mem[248]),
        .I1(res_mem[120]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[184]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[56]),
        .O(\m_axis_tdata[40]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[40]_i_7 
       (.I0(res_mem[264]),
        .I1(res_mem[136]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[200]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[72]),
        .O(\m_axis_tdata[40]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[40]_i_8 
       (.I0(res_mem[232]),
        .I1(res_mem[104]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[168]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[40]),
        .O(\m_axis_tdata[40]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[41]_i_1 
       (.I0(\m_axis_tdata[41]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[41]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[41]_i_4_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[41]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[41]_i_2 
       (.I0(\m_axis_tdata[41]_i_5_n_0 ),
        .I1(\m_axis_tdata[41]_i_6_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[41]_i_7_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[41]_i_8_n_0 ),
        .O(\m_axis_tdata[41]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000A0A0F000C0C)) 
    \m_axis_tdata[41]_i_3 
       (.I0(res_mem[361]),
        .I1(res_mem[297]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[329]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .O(\m_axis_tdata[41]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000A0A0F000C0C)) 
    \m_axis_tdata[41]_i_4 
       (.I0(res_mem[377]),
        .I1(res_mem[313]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[345]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .O(\m_axis_tdata[41]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[41]_i_5 
       (.I0(res_mem[281]),
        .I1(res_mem[153]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[217]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[89]),
        .O(\m_axis_tdata[41]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[41]_i_6 
       (.I0(res_mem[249]),
        .I1(res_mem[121]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[185]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[57]),
        .O(\m_axis_tdata[41]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[41]_i_7 
       (.I0(res_mem[265]),
        .I1(res_mem[137]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[201]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[73]),
        .O(\m_axis_tdata[41]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[41]_i_8 
       (.I0(res_mem[233]),
        .I1(res_mem[105]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[169]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[41]),
        .O(\m_axis_tdata[41]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[42]_i_1 
       (.I0(\m_axis_tdata[42]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[42]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[42]_i_4_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[42]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[42]_i_2 
       (.I0(\m_axis_tdata[42]_i_5_n_0 ),
        .I1(\m_axis_tdata[42]_i_6_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[42]_i_7_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[42]_i_8_n_0 ),
        .O(\m_axis_tdata[42]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000F0A0C00000A0C)) 
    \m_axis_tdata[42]_i_3 
       (.I0(res_mem[362]),
        .I1(res_mem[298]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(res_mem[330]),
        .O(\m_axis_tdata[42]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000A0A0F000C0C)) 
    \m_axis_tdata[42]_i_4 
       (.I0(res_mem[378]),
        .I1(res_mem[314]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[346]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .O(\m_axis_tdata[42]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[42]_i_5 
       (.I0(res_mem[282]),
        .I1(res_mem[154]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[218]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[90]),
        .O(\m_axis_tdata[42]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[42]_i_6 
       (.I0(res_mem[250]),
        .I1(res_mem[122]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[186]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[58]),
        .O(\m_axis_tdata[42]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[42]_i_7 
       (.I0(res_mem[266]),
        .I1(res_mem[138]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[202]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[74]),
        .O(\m_axis_tdata[42]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[42]_i_8 
       (.I0(res_mem[234]),
        .I1(res_mem[106]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[170]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[42]),
        .O(\m_axis_tdata[42]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[43]_i_1 
       (.I0(\m_axis_tdata[43]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[43]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[43]_i_4_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[43]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[43]_i_2 
       (.I0(\m_axis_tdata[43]_i_5_n_0 ),
        .I1(\m_axis_tdata[43]_i_6_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[43]_i_7_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[43]_i_8_n_0 ),
        .O(\m_axis_tdata[43]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000F0A0C00000A0C)) 
    \m_axis_tdata[43]_i_3 
       (.I0(res_mem[363]),
        .I1(res_mem[299]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(res_mem[331]),
        .O(\m_axis_tdata[43]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000A0A0F000C0C)) 
    \m_axis_tdata[43]_i_4 
       (.I0(res_mem[379]),
        .I1(res_mem[315]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[347]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .O(\m_axis_tdata[43]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[43]_i_5 
       (.I0(res_mem[283]),
        .I1(res_mem[155]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[219]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[91]),
        .O(\m_axis_tdata[43]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[43]_i_6 
       (.I0(res_mem[251]),
        .I1(res_mem[123]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[187]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[59]),
        .O(\m_axis_tdata[43]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[43]_i_7 
       (.I0(res_mem[267]),
        .I1(res_mem[139]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[203]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[75]),
        .O(\m_axis_tdata[43]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[43]_i_8 
       (.I0(res_mem[235]),
        .I1(res_mem[107]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[171]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[43]),
        .O(\m_axis_tdata[43]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[44]_i_1 
       (.I0(\m_axis_tdata[44]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[44]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[44]_i_4_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[44]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[44]_i_2 
       (.I0(\m_axis_tdata[44]_i_5_n_0 ),
        .I1(\m_axis_tdata[44]_i_6_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[44]_i_7_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[44]_i_8_n_0 ),
        .O(\m_axis_tdata[44]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000F0A0C00000A0C)) 
    \m_axis_tdata[44]_i_3 
       (.I0(res_mem[364]),
        .I1(res_mem[300]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(res_mem[332]),
        .O(\m_axis_tdata[44]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000A0A0F000C0C)) 
    \m_axis_tdata[44]_i_4 
       (.I0(res_mem[380]),
        .I1(res_mem[316]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[348]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .O(\m_axis_tdata[44]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[44]_i_5 
       (.I0(res_mem[284]),
        .I1(res_mem[156]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[220]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[92]),
        .O(\m_axis_tdata[44]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[44]_i_6 
       (.I0(res_mem[252]),
        .I1(res_mem[124]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[188]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[60]),
        .O(\m_axis_tdata[44]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[44]_i_7 
       (.I0(res_mem[268]),
        .I1(res_mem[140]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[204]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[76]),
        .O(\m_axis_tdata[44]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[44]_i_8 
       (.I0(res_mem[236]),
        .I1(res_mem[108]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[172]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[44]),
        .O(\m_axis_tdata[44]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[45]_i_1 
       (.I0(\m_axis_tdata[45]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[45]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[45]_i_4_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[45]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[45]_i_2 
       (.I0(\m_axis_tdata[45]_i_5_n_0 ),
        .I1(\m_axis_tdata[45]_i_6_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[45]_i_7_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[45]_i_8_n_0 ),
        .O(\m_axis_tdata[45]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000F0A0C00000A0C)) 
    \m_axis_tdata[45]_i_3 
       (.I0(res_mem[365]),
        .I1(res_mem[301]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(res_mem[333]),
        .O(\m_axis_tdata[45]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000A0A0F000C0C)) 
    \m_axis_tdata[45]_i_4 
       (.I0(res_mem[381]),
        .I1(res_mem[317]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[349]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .O(\m_axis_tdata[45]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[45]_i_5 
       (.I0(res_mem[285]),
        .I1(res_mem[157]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[221]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[93]),
        .O(\m_axis_tdata[45]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[45]_i_6 
       (.I0(res_mem[253]),
        .I1(res_mem[125]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[189]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[61]),
        .O(\m_axis_tdata[45]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[45]_i_7 
       (.I0(res_mem[269]),
        .I1(res_mem[141]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[205]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[77]),
        .O(\m_axis_tdata[45]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[45]_i_8 
       (.I0(res_mem[237]),
        .I1(res_mem[109]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[173]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[45]),
        .O(\m_axis_tdata[45]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[46]_i_1 
       (.I0(\m_axis_tdata[46]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[46]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[46]_i_4_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[46]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[46]_i_2 
       (.I0(\m_axis_tdata[46]_i_5_n_0 ),
        .I1(\m_axis_tdata[46]_i_6_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[46]_i_7_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[46]_i_8_n_0 ),
        .O(\m_axis_tdata[46]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000A0A0F000C0C)) 
    \m_axis_tdata[46]_i_3 
       (.I0(res_mem[366]),
        .I1(res_mem[302]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[334]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .O(\m_axis_tdata[46]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h000F0A0C00000A0C)) 
    \m_axis_tdata[46]_i_4 
       (.I0(res_mem[382]),
        .I1(res_mem[318]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(res_mem[350]),
        .O(\m_axis_tdata[46]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[46]_i_5 
       (.I0(res_mem[286]),
        .I1(res_mem[158]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[222]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[94]),
        .O(\m_axis_tdata[46]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[46]_i_6 
       (.I0(res_mem[254]),
        .I1(res_mem[126]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[190]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[62]),
        .O(\m_axis_tdata[46]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[46]_i_7 
       (.I0(res_mem[270]),
        .I1(res_mem[142]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[206]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[78]),
        .O(\m_axis_tdata[46]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[46]_i_8 
       (.I0(res_mem[238]),
        .I1(res_mem[110]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[174]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[46]),
        .O(\m_axis_tdata[46]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[47]_i_1 
       (.I0(\m_axis_tdata[47]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[47]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[47]_i_5_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[47]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hBCCC4333)) 
    \m_axis_tdata[47]_i_10 
       (.I0(counter[1]),
        .I1(counter[0]),
        .I2(m_axis_tvalid),
        .I3(m_axis_tready),
        .I4(counter[2]),
        .O(\m_axis_tdata[47]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'h9555)) 
    \m_axis_tdata[47]_i_11 
       (.I0(counter[1]),
        .I1(m_axis_tready),
        .I2(m_axis_tvalid),
        .I3(counter[0]),
        .O(\m_axis_tdata[47]_i_11_n_0 ));
  LUT3 #(
    .INIT(8'h6A)) 
    \m_axis_tdata[47]_i_12 
       (.I0(counter[0]),
        .I1(m_axis_tready),
        .I2(m_axis_tvalid),
        .O(\m_axis_tdata[47]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[47]_i_2 
       (.I0(\m_axis_tdata[47]_i_6_n_0 ),
        .I1(\m_axis_tdata[63]_i_10_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[63]_i_8_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[63]_i_9_n_0 ),
        .O(\m_axis_tdata[47]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000F0A0C00000A0C)) 
    \m_axis_tdata[47]_i_3 
       (.I0(res_mem[367]),
        .I1(res_mem[303]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(res_mem[335]),
        .O(\m_axis_tdata[47]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h000F0A0C00000A0C)) 
    \m_axis_tdata[47]_i_5 
       (.I0(res_mem[383]),
        .I1(res_mem[319]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(res_mem[351]),
        .O(\m_axis_tdata[47]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[47]_i_6 
       (.I0(res_mem[287]),
        .I1(res_mem[159]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[223]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[95]),
        .O(\m_axis_tdata[47]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h7F80)) 
    \m_axis_tdata[47]_i_7 
       (.I0(counter[0]),
        .I1(m_axis_tvalid),
        .I2(m_axis_tready),
        .I3(counter[1]),
        .O(\m_axis_tdata[47]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h80007FFF)) 
    \m_axis_tdata[47]_i_8 
       (.I0(counter[1]),
        .I1(counter[0]),
        .I2(m_axis_tvalid),
        .I3(m_axis_tready),
        .I4(counter[2]),
        .O(\m_axis_tdata[47]_i_8_n_0 ));
  LUT5 #(
    .INIT(32'h15559555)) 
    \m_axis_tdata[47]_i_9 
       (.I0(counter[1]),
        .I1(counter[0]),
        .I2(m_axis_tvalid),
        .I3(m_axis_tready),
        .I4(counter[2]),
        .O(\m_axis_tdata[47]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[4]_i_1 
       (.I0(\m_axis_tdata[4]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[4]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[20]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[4]_i_2 
       (.I0(\m_axis_tdata[36]_i_6_n_0 ),
        .I1(\m_axis_tdata[20]_i_4_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[36]_i_8_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[4]_i_4_n_0 ),
        .O(\m_axis_tdata[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5353535050505350)) 
    \m_axis_tdata[4]_i_3 
       (.I0(\m_axis_tdata[4]_i_5_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I3(res_mem[260]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I5(res_mem[324]),
        .O(\m_axis_tdata[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[4]_i_4 
       (.I0(res_mem[196]),
        .I1(res_mem[68]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[132]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[4]),
        .O(\m_axis_tdata[4]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[4]_i_5 
       (.I0(res_mem[356]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[292]),
        .O(\m_axis_tdata[4]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000222EE2EE)) 
    \m_axis_tdata[5]_i_1 
       (.I0(\m_axis_tdata[5]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[5]_i_3_n_0 ),
        .I4(\m_axis_tdata[21]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[5]_i_2 
       (.I0(\m_axis_tdata[37]_i_6_n_0 ),
        .I1(\m_axis_tdata[21]_i_4_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[37]_i_8_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[5]_i_4_n_0 ),
        .O(\m_axis_tdata[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBBBB8B88BBBB8BBB)) 
    \m_axis_tdata[5]_i_3 
       (.I0(\m_axis_tdata[5]_i_5_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I2(res_mem[325]),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[261]),
        .O(\m_axis_tdata[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[5]_i_4 
       (.I0(res_mem[197]),
        .I1(res_mem[69]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[133]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[5]),
        .O(\m_axis_tdata[5]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[5]_i_5 
       (.I0(res_mem[357]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[293]),
        .O(\m_axis_tdata[5]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \m_axis_tdata[63]_i_1 
       (.I0(start),
        .I1(p_rdy),
        .O(\m_axis_tdata[63]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[63]_i_10 
       (.I0(res_mem[255]),
        .I1(res_mem[127]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[191]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[63]),
        .O(\m_axis_tdata[63]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[63]_i_11 
       (.I0(res_mem[223]),
        .I1(res_mem[95]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[159]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[31]),
        .O(\m_axis_tdata[63]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[63]_i_12 
       (.I0(res_mem[383]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[319]),
        .O(\m_axis_tdata[63]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[63]_i_13 
       (.I0(res_mem[351]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[287]),
        .O(\m_axis_tdata[63]_i_13_n_0 ));
  LUT5 #(
    .INIT(32'h80007FFF)) 
    \m_axis_tdata[63]_i_14 
       (.I0(counter[1]),
        .I1(counter[0]),
        .I2(m_axis_tvalid),
        .I3(m_axis_tready),
        .I4(counter[2]),
        .O(\m_axis_tdata[63]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'h00CA)) 
    \m_axis_tdata[63]_i_2 
       (.I0(\m_axis_tdata[63]_i_4_n_0 ),
        .I1(\m_axis_tdata[63]_i_5_n_0 ),
        .I2(m_axis_tdata3),
        .I3(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[63]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[63]_i_4 
       (.I0(\m_axis_tdata[63]_i_8_n_0 ),
        .I1(\m_axis_tdata[63]_i_9_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[63]_i_10_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[63]_i_11_n_0 ),
        .O(\m_axis_tdata[63]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFF470047)) 
    \m_axis_tdata[63]_i_5 
       (.I0(\m_axis_tdata[63]_i_12_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I2(\m_axis_tdata[63]_i_13_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[47]_i_3_n_0 ),
        .O(\m_axis_tdata[63]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \m_axis_tdata[63]_i_7 
       (.I0(counter[2]),
        .I1(m_axis_tready),
        .I2(m_axis_tvalid),
        .I3(counter[0]),
        .I4(counter[1]),
        .O(\m_axis_tdata[63]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[63]_i_8 
       (.I0(res_mem[271]),
        .I1(res_mem[143]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[207]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[79]),
        .O(\m_axis_tdata[63]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[63]_i_9 
       (.I0(res_mem[239]),
        .I1(res_mem[111]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[175]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[47]),
        .O(\m_axis_tdata[63]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[6]_i_1 
       (.I0(\m_axis_tdata[6]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[6]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[22]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[6]_i_2 
       (.I0(\m_axis_tdata[38]_i_6_n_0 ),
        .I1(\m_axis_tdata[22]_i_4_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[38]_i_8_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[6]_i_4_n_0 ),
        .O(\m_axis_tdata[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5353535050505350)) 
    \m_axis_tdata[6]_i_3 
       (.I0(\m_axis_tdata[6]_i_5_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I3(res_mem[262]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I5(res_mem[326]),
        .O(\m_axis_tdata[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[6]_i_4 
       (.I0(res_mem[198]),
        .I1(res_mem[70]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[134]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[6]),
        .O(\m_axis_tdata[6]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[6]_i_5 
       (.I0(res_mem[358]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[294]),
        .O(\m_axis_tdata[6]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[7]_i_1 
       (.I0(\m_axis_tdata[7]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[7]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[23]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[7]_i_2 
       (.I0(\m_axis_tdata[39]_i_6_n_0 ),
        .I1(\m_axis_tdata[23]_i_4_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[39]_i_8_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[7]_i_4_n_0 ),
        .O(\m_axis_tdata[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5350535353505050)) 
    \m_axis_tdata[7]_i_3 
       (.I0(\m_axis_tdata[7]_i_5_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I3(res_mem[327]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I5(res_mem[263]),
        .O(\m_axis_tdata[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[7]_i_4 
       (.I0(res_mem[199]),
        .I1(res_mem[71]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[135]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[7]),
        .O(\m_axis_tdata[7]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[7]_i_5 
       (.I0(res_mem[359]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[295]),
        .O(\m_axis_tdata[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000222EE2EE)) 
    \m_axis_tdata[8]_i_1 
       (.I0(\m_axis_tdata[8]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[8]_i_3_n_0 ),
        .I4(\m_axis_tdata[24]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[8]_i_2 
       (.I0(\m_axis_tdata[40]_i_6_n_0 ),
        .I1(\m_axis_tdata[24]_i_4_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[40]_i_8_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[8]_i_4_n_0 ),
        .O(\m_axis_tdata[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBBBB8B88BBBB8BBB)) 
    \m_axis_tdata[8]_i_3 
       (.I0(\m_axis_tdata[8]_i_5_n_0 ),
        .I1(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I2(res_mem[328]),
        .I3(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[264]),
        .O(\m_axis_tdata[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[8]_i_4 
       (.I0(res_mem[200]),
        .I1(res_mem[72]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[136]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[8]),
        .O(\m_axis_tdata[8]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[8]_i_5 
       (.I0(res_mem[360]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[296]),
        .O(\m_axis_tdata[8]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEE222E2)) 
    \m_axis_tdata[9]_i_1 
       (.I0(\m_axis_tdata[9]_i_2_n_0 ),
        .I1(m_axis_tdata3),
        .I2(\m_axis_tdata[9]_i_3_n_0 ),
        .I3(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I4(\m_axis_tdata[25]_i_3_n_0 ),
        .I5(\m_axis_tdata[63]_i_7_n_0 ),
        .O(\m_axis_tdata[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[9]_i_2 
       (.I0(\m_axis_tdata[41]_i_6_n_0 ),
        .I1(\m_axis_tdata[25]_i_4_n_0 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_7 ),
        .I3(\m_axis_tdata[41]_i_8_n_0 ),
        .I4(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .I5(\m_axis_tdata[9]_i_4_n_0 ),
        .O(\m_axis_tdata[9]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF45404540)) 
    \m_axis_tdata[9]_i_3 
       (.I0(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I1(res_mem[329]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[265]),
        .I4(\m_axis_tdata[9]_i_5_n_0 ),
        .I5(\m_axis_tdata_reg[47]_i_4_n_6 ),
        .O(\m_axis_tdata[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \m_axis_tdata[9]_i_4 
       (.I0(res_mem[201]),
        .I1(res_mem[73]),
        .I2(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I3(res_mem[137]),
        .I4(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I5(res_mem[9]),
        .O(\m_axis_tdata[9]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF4F7)) 
    \m_axis_tdata[9]_i_5 
       (.I0(res_mem[361]),
        .I1(\m_axis_tdata_reg[47]_i_4_n_5 ),
        .I2(\m_axis_tdata_reg[47]_i_4_n_4 ),
        .I3(res_mem[297]),
        .O(\m_axis_tdata[9]_i_5_n_0 ));
  FDCE \m_axis_tdata_reg[0] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[0]_i_1_n_0 ),
        .Q(m_axis_tdata[0]));
  FDCE \m_axis_tdata_reg[10] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[10]_i_1_n_0 ),
        .Q(m_axis_tdata[10]));
  FDCE \m_axis_tdata_reg[11] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[11]_i_1_n_0 ),
        .Q(m_axis_tdata[11]));
  FDCE \m_axis_tdata_reg[12] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[12]_i_1_n_0 ),
        .Q(m_axis_tdata[12]));
  FDCE \m_axis_tdata_reg[13] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[13]_i_1_n_0 ),
        .Q(m_axis_tdata[13]));
  FDCE \m_axis_tdata_reg[14] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[14]_i_1_n_0 ),
        .Q(m_axis_tdata[14]));
  FDCE \m_axis_tdata_reg[15] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[15]_i_1_n_0 ),
        .Q(m_axis_tdata[15]));
  FDCE \m_axis_tdata_reg[16] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[16]_i_1_n_0 ),
        .Q(m_axis_tdata[16]));
  FDCE \m_axis_tdata_reg[17] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[17]_i_1_n_0 ),
        .Q(m_axis_tdata[17]));
  FDCE \m_axis_tdata_reg[18] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[18]_i_1_n_0 ),
        .Q(m_axis_tdata[18]));
  FDCE \m_axis_tdata_reg[19] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[19]_i_1_n_0 ),
        .Q(m_axis_tdata[19]));
  FDCE \m_axis_tdata_reg[1] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[1]_i_1_n_0 ),
        .Q(m_axis_tdata[1]));
  FDCE \m_axis_tdata_reg[20] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[20]_i_1_n_0 ),
        .Q(m_axis_tdata[20]));
  FDCE \m_axis_tdata_reg[21] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[21]_i_1_n_0 ),
        .Q(m_axis_tdata[21]));
  FDCE \m_axis_tdata_reg[22] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[22]_i_1_n_0 ),
        .Q(m_axis_tdata[22]));
  FDCE \m_axis_tdata_reg[23] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[23]_i_1_n_0 ),
        .Q(m_axis_tdata[23]));
  FDCE \m_axis_tdata_reg[24] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[24]_i_1_n_0 ),
        .Q(m_axis_tdata[24]));
  FDCE \m_axis_tdata_reg[25] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[25]_i_1_n_0 ),
        .Q(m_axis_tdata[25]));
  FDCE \m_axis_tdata_reg[26] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[26]_i_1_n_0 ),
        .Q(m_axis_tdata[26]));
  FDCE \m_axis_tdata_reg[27] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[27]_i_1_n_0 ),
        .Q(m_axis_tdata[27]));
  FDCE \m_axis_tdata_reg[28] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[28]_i_1_n_0 ),
        .Q(m_axis_tdata[28]));
  FDCE \m_axis_tdata_reg[29] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[29]_i_1_n_0 ),
        .Q(m_axis_tdata[29]));
  FDCE \m_axis_tdata_reg[2] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[2]_i_1_n_0 ),
        .Q(m_axis_tdata[2]));
  FDCE \m_axis_tdata_reg[30] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[30]_i_1_n_0 ),
        .Q(m_axis_tdata[30]));
  FDCE \m_axis_tdata_reg[31] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[31]_i_1_n_0 ),
        .Q(m_axis_tdata[31]));
  FDCE \m_axis_tdata_reg[32] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[32]_i_1_n_0 ),
        .Q(m_axis_tdata[32]));
  FDCE \m_axis_tdata_reg[33] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[33]_i_1_n_0 ),
        .Q(m_axis_tdata[33]));
  FDCE \m_axis_tdata_reg[34] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[34]_i_1_n_0 ),
        .Q(m_axis_tdata[34]));
  FDCE \m_axis_tdata_reg[35] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[35]_i_1_n_0 ),
        .Q(m_axis_tdata[35]));
  FDCE \m_axis_tdata_reg[36] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[36]_i_1_n_0 ),
        .Q(m_axis_tdata[36]));
  FDCE \m_axis_tdata_reg[37] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[37]_i_1_n_0 ),
        .Q(m_axis_tdata[37]));
  FDCE \m_axis_tdata_reg[38] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[38]_i_1_n_0 ),
        .Q(m_axis_tdata[38]));
  FDCE \m_axis_tdata_reg[39] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[39]_i_1_n_0 ),
        .Q(m_axis_tdata[39]));
  FDCE \m_axis_tdata_reg[3] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[3]_i_1_n_0 ),
        .Q(m_axis_tdata[3]));
  FDCE \m_axis_tdata_reg[40] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[40]_i_1_n_0 ),
        .Q(m_axis_tdata[40]));
  FDCE \m_axis_tdata_reg[41] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[41]_i_1_n_0 ),
        .Q(m_axis_tdata[41]));
  FDCE \m_axis_tdata_reg[42] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[42]_i_1_n_0 ),
        .Q(m_axis_tdata[42]));
  FDCE \m_axis_tdata_reg[43] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[43]_i_1_n_0 ),
        .Q(m_axis_tdata[43]));
  FDCE \m_axis_tdata_reg[44] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[44]_i_1_n_0 ),
        .Q(m_axis_tdata[44]));
  FDCE \m_axis_tdata_reg[45] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[45]_i_1_n_0 ),
        .Q(m_axis_tdata[45]));
  FDCE \m_axis_tdata_reg[46] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[46]_i_1_n_0 ),
        .Q(m_axis_tdata[46]));
  FDCE \m_axis_tdata_reg[47] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[47]_i_1_n_0 ),
        .Q(m_axis_tdata[47]));
  CARRY4 \m_axis_tdata_reg[47]_i_4 
       (.CI(1'b0),
        .CO({\m_axis_tdata_reg[47]_i_4_n_0 ,\m_axis_tdata_reg[47]_i_4_n_1 ,\m_axis_tdata_reg[47]_i_4_n_2 ,\m_axis_tdata_reg[47]_i_4_n_3 }),
        .CYINIT(1'b0),
        .DI({\m_axis_tdata[47]_i_7_n_0 ,\m_axis_tdata[47]_i_8_n_0 ,1'b0,1'b1}),
        .O({\m_axis_tdata_reg[47]_i_4_n_4 ,\m_axis_tdata_reg[47]_i_4_n_5 ,\m_axis_tdata_reg[47]_i_4_n_6 ,\m_axis_tdata_reg[47]_i_4_n_7 }),
        .S({\m_axis_tdata[47]_i_9_n_0 ,\m_axis_tdata[47]_i_10_n_0 ,\m_axis_tdata[47]_i_11_n_0 ,\m_axis_tdata[47]_i_12_n_0 }));
  FDCE \m_axis_tdata_reg[4] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[4]_i_1_n_0 ),
        .Q(m_axis_tdata[4]));
  FDCE \m_axis_tdata_reg[5] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[5]_i_1_n_0 ),
        .Q(m_axis_tdata[5]));
  FDCE \m_axis_tdata_reg[63] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[63]_i_2_n_0 ),
        .Q(m_axis_tdata[48]));
  CARRY4 \m_axis_tdata_reg[63]_i_6 
       (.CI(\m_axis_tdata_reg[47]_i_4_n_0 ),
        .CO(\NLW_m_axis_tdata_reg[63]_i_6_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_m_axis_tdata_reg[63]_i_6_O_UNCONNECTED [3:1],m_axis_tdata3}),
        .S({1'b0,1'b0,1'b0,\m_axis_tdata[63]_i_14_n_0 }));
  FDCE \m_axis_tdata_reg[6] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[6]_i_1_n_0 ),
        .Q(m_axis_tdata[6]));
  FDCE \m_axis_tdata_reg[7] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[7]_i_1_n_0 ),
        .Q(m_axis_tdata[7]));
  FDCE \m_axis_tdata_reg[8] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[8]_i_1_n_0 ),
        .Q(m_axis_tdata[8]));
  FDCE \m_axis_tdata_reg[9] 
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(\m_axis_tdata[9]_i_1_n_0 ),
        .Q(m_axis_tdata[9]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT5 #(
    .INIT(32'h40000000)) 
    m_axis_tlast_i_1
       (.I0(last_p_reg),
        .I1(m_axis_tvalid),
        .I2(last_flag),
        .I3(counter_reg[2]),
        .I4(counter_reg[1]),
        .O(m_axis_tlast_i_1_n_0));
  FDCE m_axis_tlast_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(aresetn_0),
        .D(m_axis_tlast_i_1_n_0),
        .Q(m_axis_tlast));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT2 #(
    .INIT(4'h2)) 
    p_irq_INST_0
       (.I0(last_t_valid),
        .I1(m_axis_tvalid),
        .O(p_irq));
  FDRE \res_mem_reg[0] 
       (.C(clk),
        .CE(E),
        .D(D[0]),
        .Q(res_mem[0]),
        .R(1'b0));
  FDRE \res_mem_reg[100] 
       (.C(clk),
        .CE(E),
        .D(D[100]),
        .Q(res_mem[100]),
        .R(1'b0));
  FDRE \res_mem_reg[101] 
       (.C(clk),
        .CE(E),
        .D(D[101]),
        .Q(res_mem[101]),
        .R(1'b0));
  FDRE \res_mem_reg[102] 
       (.C(clk),
        .CE(E),
        .D(D[102]),
        .Q(res_mem[102]),
        .R(1'b0));
  FDRE \res_mem_reg[103] 
       (.C(clk),
        .CE(E),
        .D(D[103]),
        .Q(res_mem[103]),
        .R(1'b0));
  FDRE \res_mem_reg[104] 
       (.C(clk),
        .CE(E),
        .D(D[104]),
        .Q(res_mem[104]),
        .R(1'b0));
  FDRE \res_mem_reg[105] 
       (.C(clk),
        .CE(E),
        .D(D[105]),
        .Q(res_mem[105]),
        .R(1'b0));
  FDRE \res_mem_reg[106] 
       (.C(clk),
        .CE(E),
        .D(D[106]),
        .Q(res_mem[106]),
        .R(1'b0));
  FDRE \res_mem_reg[107] 
       (.C(clk),
        .CE(E),
        .D(D[107]),
        .Q(res_mem[107]),
        .R(1'b0));
  FDRE \res_mem_reg[108] 
       (.C(clk),
        .CE(E),
        .D(D[108]),
        .Q(res_mem[108]),
        .R(1'b0));
  FDRE \res_mem_reg[109] 
       (.C(clk),
        .CE(E),
        .D(D[109]),
        .Q(res_mem[109]),
        .R(1'b0));
  FDRE \res_mem_reg[10] 
       (.C(clk),
        .CE(E),
        .D(D[10]),
        .Q(res_mem[10]),
        .R(1'b0));
  FDRE \res_mem_reg[110] 
       (.C(clk),
        .CE(E),
        .D(D[110]),
        .Q(res_mem[110]),
        .R(1'b0));
  FDRE \res_mem_reg[111] 
       (.C(clk),
        .CE(E),
        .D(D[111]),
        .Q(res_mem[111]),
        .R(1'b0));
  FDRE \res_mem_reg[112] 
       (.C(clk),
        .CE(E),
        .D(D[112]),
        .Q(res_mem[112]),
        .R(1'b0));
  FDRE \res_mem_reg[113] 
       (.C(clk),
        .CE(E),
        .D(D[113]),
        .Q(res_mem[113]),
        .R(1'b0));
  FDRE \res_mem_reg[114] 
       (.C(clk),
        .CE(E),
        .D(D[114]),
        .Q(res_mem[114]),
        .R(1'b0));
  FDRE \res_mem_reg[115] 
       (.C(clk),
        .CE(E),
        .D(D[115]),
        .Q(res_mem[115]),
        .R(1'b0));
  FDRE \res_mem_reg[116] 
       (.C(clk),
        .CE(E),
        .D(D[116]),
        .Q(res_mem[116]),
        .R(1'b0));
  FDRE \res_mem_reg[117] 
       (.C(clk),
        .CE(E),
        .D(D[117]),
        .Q(res_mem[117]),
        .R(1'b0));
  FDRE \res_mem_reg[118] 
       (.C(clk),
        .CE(E),
        .D(D[118]),
        .Q(res_mem[118]),
        .R(1'b0));
  FDRE \res_mem_reg[119] 
       (.C(clk),
        .CE(E),
        .D(D[119]),
        .Q(res_mem[119]),
        .R(1'b0));
  FDRE \res_mem_reg[11] 
       (.C(clk),
        .CE(E),
        .D(D[11]),
        .Q(res_mem[11]),
        .R(1'b0));
  FDRE \res_mem_reg[120] 
       (.C(clk),
        .CE(E),
        .D(D[120]),
        .Q(res_mem[120]),
        .R(1'b0));
  FDRE \res_mem_reg[121] 
       (.C(clk),
        .CE(E),
        .D(D[121]),
        .Q(res_mem[121]),
        .R(1'b0));
  FDRE \res_mem_reg[122] 
       (.C(clk),
        .CE(E),
        .D(D[122]),
        .Q(res_mem[122]),
        .R(1'b0));
  FDRE \res_mem_reg[123] 
       (.C(clk),
        .CE(E),
        .D(D[123]),
        .Q(res_mem[123]),
        .R(1'b0));
  FDRE \res_mem_reg[124] 
       (.C(clk),
        .CE(E),
        .D(D[124]),
        .Q(res_mem[124]),
        .R(1'b0));
  FDRE \res_mem_reg[125] 
       (.C(clk),
        .CE(E),
        .D(D[125]),
        .Q(res_mem[125]),
        .R(1'b0));
  FDRE \res_mem_reg[126] 
       (.C(clk),
        .CE(E),
        .D(D[126]),
        .Q(res_mem[126]),
        .R(1'b0));
  FDRE \res_mem_reg[127] 
       (.C(clk),
        .CE(E),
        .D(D[127]),
        .Q(res_mem[127]),
        .R(1'b0));
  FDRE \res_mem_reg[128] 
       (.C(clk),
        .CE(E),
        .D(D[128]),
        .Q(res_mem[128]),
        .R(1'b0));
  FDRE \res_mem_reg[129] 
       (.C(clk),
        .CE(E),
        .D(D[129]),
        .Q(res_mem[129]),
        .R(1'b0));
  FDRE \res_mem_reg[12] 
       (.C(clk),
        .CE(E),
        .D(D[12]),
        .Q(res_mem[12]),
        .R(1'b0));
  FDRE \res_mem_reg[130] 
       (.C(clk),
        .CE(E),
        .D(D[130]),
        .Q(res_mem[130]),
        .R(1'b0));
  FDRE \res_mem_reg[131] 
       (.C(clk),
        .CE(E),
        .D(D[131]),
        .Q(res_mem[131]),
        .R(1'b0));
  FDRE \res_mem_reg[132] 
       (.C(clk),
        .CE(E),
        .D(D[132]),
        .Q(res_mem[132]),
        .R(1'b0));
  FDRE \res_mem_reg[133] 
       (.C(clk),
        .CE(E),
        .D(D[133]),
        .Q(res_mem[133]),
        .R(1'b0));
  FDRE \res_mem_reg[134] 
       (.C(clk),
        .CE(E),
        .D(D[134]),
        .Q(res_mem[134]),
        .R(1'b0));
  FDRE \res_mem_reg[135] 
       (.C(clk),
        .CE(E),
        .D(D[135]),
        .Q(res_mem[135]),
        .R(1'b0));
  FDRE \res_mem_reg[136] 
       (.C(clk),
        .CE(E),
        .D(D[136]),
        .Q(res_mem[136]),
        .R(1'b0));
  FDRE \res_mem_reg[137] 
       (.C(clk),
        .CE(E),
        .D(D[137]),
        .Q(res_mem[137]),
        .R(1'b0));
  FDRE \res_mem_reg[138] 
       (.C(clk),
        .CE(E),
        .D(D[138]),
        .Q(res_mem[138]),
        .R(1'b0));
  FDRE \res_mem_reg[139] 
       (.C(clk),
        .CE(E),
        .D(D[139]),
        .Q(res_mem[139]),
        .R(1'b0));
  FDRE \res_mem_reg[13] 
       (.C(clk),
        .CE(E),
        .D(D[13]),
        .Q(res_mem[13]),
        .R(1'b0));
  FDRE \res_mem_reg[140] 
       (.C(clk),
        .CE(E),
        .D(D[140]),
        .Q(res_mem[140]),
        .R(1'b0));
  FDRE \res_mem_reg[141] 
       (.C(clk),
        .CE(E),
        .D(D[141]),
        .Q(res_mem[141]),
        .R(1'b0));
  FDRE \res_mem_reg[142] 
       (.C(clk),
        .CE(E),
        .D(D[142]),
        .Q(res_mem[142]),
        .R(1'b0));
  FDRE \res_mem_reg[143] 
       (.C(clk),
        .CE(E),
        .D(D[143]),
        .Q(res_mem[143]),
        .R(1'b0));
  FDRE \res_mem_reg[144] 
       (.C(clk),
        .CE(E),
        .D(D[144]),
        .Q(res_mem[144]),
        .R(1'b0));
  FDRE \res_mem_reg[145] 
       (.C(clk),
        .CE(E),
        .D(D[145]),
        .Q(res_mem[145]),
        .R(1'b0));
  FDRE \res_mem_reg[146] 
       (.C(clk),
        .CE(E),
        .D(D[146]),
        .Q(res_mem[146]),
        .R(1'b0));
  FDRE \res_mem_reg[147] 
       (.C(clk),
        .CE(E),
        .D(D[147]),
        .Q(res_mem[147]),
        .R(1'b0));
  FDRE \res_mem_reg[148] 
       (.C(clk),
        .CE(E),
        .D(D[148]),
        .Q(res_mem[148]),
        .R(1'b0));
  FDRE \res_mem_reg[149] 
       (.C(clk),
        .CE(E),
        .D(D[149]),
        .Q(res_mem[149]),
        .R(1'b0));
  FDRE \res_mem_reg[14] 
       (.C(clk),
        .CE(E),
        .D(D[14]),
        .Q(res_mem[14]),
        .R(1'b0));
  FDRE \res_mem_reg[150] 
       (.C(clk),
        .CE(E),
        .D(D[150]),
        .Q(res_mem[150]),
        .R(1'b0));
  FDRE \res_mem_reg[151] 
       (.C(clk),
        .CE(E),
        .D(D[151]),
        .Q(res_mem[151]),
        .R(1'b0));
  FDRE \res_mem_reg[152] 
       (.C(clk),
        .CE(E),
        .D(D[152]),
        .Q(res_mem[152]),
        .R(1'b0));
  FDRE \res_mem_reg[153] 
       (.C(clk),
        .CE(E),
        .D(D[153]),
        .Q(res_mem[153]),
        .R(1'b0));
  FDRE \res_mem_reg[154] 
       (.C(clk),
        .CE(E),
        .D(D[154]),
        .Q(res_mem[154]),
        .R(1'b0));
  FDRE \res_mem_reg[155] 
       (.C(clk),
        .CE(E),
        .D(D[155]),
        .Q(res_mem[155]),
        .R(1'b0));
  FDRE \res_mem_reg[156] 
       (.C(clk),
        .CE(E),
        .D(D[156]),
        .Q(res_mem[156]),
        .R(1'b0));
  FDRE \res_mem_reg[157] 
       (.C(clk),
        .CE(E),
        .D(D[157]),
        .Q(res_mem[157]),
        .R(1'b0));
  FDRE \res_mem_reg[158] 
       (.C(clk),
        .CE(E),
        .D(D[158]),
        .Q(res_mem[158]),
        .R(1'b0));
  FDRE \res_mem_reg[159] 
       (.C(clk),
        .CE(E),
        .D(D[159]),
        .Q(res_mem[159]),
        .R(1'b0));
  FDRE \res_mem_reg[15] 
       (.C(clk),
        .CE(E),
        .D(D[15]),
        .Q(res_mem[15]),
        .R(1'b0));
  FDRE \res_mem_reg[160] 
       (.C(clk),
        .CE(E),
        .D(D[160]),
        .Q(res_mem[160]),
        .R(1'b0));
  FDRE \res_mem_reg[161] 
       (.C(clk),
        .CE(E),
        .D(D[161]),
        .Q(res_mem[161]),
        .R(1'b0));
  FDRE \res_mem_reg[162] 
       (.C(clk),
        .CE(E),
        .D(D[162]),
        .Q(res_mem[162]),
        .R(1'b0));
  FDRE \res_mem_reg[163] 
       (.C(clk),
        .CE(E),
        .D(D[163]),
        .Q(res_mem[163]),
        .R(1'b0));
  FDRE \res_mem_reg[164] 
       (.C(clk),
        .CE(E),
        .D(D[164]),
        .Q(res_mem[164]),
        .R(1'b0));
  FDRE \res_mem_reg[165] 
       (.C(clk),
        .CE(E),
        .D(D[165]),
        .Q(res_mem[165]),
        .R(1'b0));
  FDRE \res_mem_reg[166] 
       (.C(clk),
        .CE(E),
        .D(D[166]),
        .Q(res_mem[166]),
        .R(1'b0));
  FDRE \res_mem_reg[167] 
       (.C(clk),
        .CE(E),
        .D(D[167]),
        .Q(res_mem[167]),
        .R(1'b0));
  FDRE \res_mem_reg[168] 
       (.C(clk),
        .CE(E),
        .D(D[168]),
        .Q(res_mem[168]),
        .R(1'b0));
  FDRE \res_mem_reg[169] 
       (.C(clk),
        .CE(E),
        .D(D[169]),
        .Q(res_mem[169]),
        .R(1'b0));
  FDRE \res_mem_reg[16] 
       (.C(clk),
        .CE(E),
        .D(D[16]),
        .Q(res_mem[16]),
        .R(1'b0));
  FDRE \res_mem_reg[170] 
       (.C(clk),
        .CE(E),
        .D(D[170]),
        .Q(res_mem[170]),
        .R(1'b0));
  FDRE \res_mem_reg[171] 
       (.C(clk),
        .CE(E),
        .D(D[171]),
        .Q(res_mem[171]),
        .R(1'b0));
  FDRE \res_mem_reg[172] 
       (.C(clk),
        .CE(E),
        .D(D[172]),
        .Q(res_mem[172]),
        .R(1'b0));
  FDRE \res_mem_reg[173] 
       (.C(clk),
        .CE(E),
        .D(D[173]),
        .Q(res_mem[173]),
        .R(1'b0));
  FDRE \res_mem_reg[174] 
       (.C(clk),
        .CE(E),
        .D(D[174]),
        .Q(res_mem[174]),
        .R(1'b0));
  FDRE \res_mem_reg[175] 
       (.C(clk),
        .CE(E),
        .D(D[175]),
        .Q(res_mem[175]),
        .R(1'b0));
  FDRE \res_mem_reg[176] 
       (.C(clk),
        .CE(E),
        .D(D[176]),
        .Q(res_mem[176]),
        .R(1'b0));
  FDRE \res_mem_reg[177] 
       (.C(clk),
        .CE(E),
        .D(D[177]),
        .Q(res_mem[177]),
        .R(1'b0));
  FDRE \res_mem_reg[178] 
       (.C(clk),
        .CE(E),
        .D(D[178]),
        .Q(res_mem[178]),
        .R(1'b0));
  FDRE \res_mem_reg[179] 
       (.C(clk),
        .CE(E),
        .D(D[179]),
        .Q(res_mem[179]),
        .R(1'b0));
  FDRE \res_mem_reg[17] 
       (.C(clk),
        .CE(E),
        .D(D[17]),
        .Q(res_mem[17]),
        .R(1'b0));
  FDRE \res_mem_reg[180] 
       (.C(clk),
        .CE(E),
        .D(D[180]),
        .Q(res_mem[180]),
        .R(1'b0));
  FDRE \res_mem_reg[181] 
       (.C(clk),
        .CE(E),
        .D(D[181]),
        .Q(res_mem[181]),
        .R(1'b0));
  FDRE \res_mem_reg[182] 
       (.C(clk),
        .CE(E),
        .D(D[182]),
        .Q(res_mem[182]),
        .R(1'b0));
  FDRE \res_mem_reg[183] 
       (.C(clk),
        .CE(E),
        .D(D[183]),
        .Q(res_mem[183]),
        .R(1'b0));
  FDRE \res_mem_reg[184] 
       (.C(clk),
        .CE(E),
        .D(D[184]),
        .Q(res_mem[184]),
        .R(1'b0));
  FDRE \res_mem_reg[185] 
       (.C(clk),
        .CE(E),
        .D(D[185]),
        .Q(res_mem[185]),
        .R(1'b0));
  FDRE \res_mem_reg[186] 
       (.C(clk),
        .CE(E),
        .D(D[186]),
        .Q(res_mem[186]),
        .R(1'b0));
  FDRE \res_mem_reg[187] 
       (.C(clk),
        .CE(E),
        .D(D[187]),
        .Q(res_mem[187]),
        .R(1'b0));
  FDRE \res_mem_reg[188] 
       (.C(clk),
        .CE(E),
        .D(D[188]),
        .Q(res_mem[188]),
        .R(1'b0));
  FDRE \res_mem_reg[189] 
       (.C(clk),
        .CE(E),
        .D(D[189]),
        .Q(res_mem[189]),
        .R(1'b0));
  FDRE \res_mem_reg[18] 
       (.C(clk),
        .CE(E),
        .D(D[18]),
        .Q(res_mem[18]),
        .R(1'b0));
  FDRE \res_mem_reg[190] 
       (.C(clk),
        .CE(E),
        .D(D[190]),
        .Q(res_mem[190]),
        .R(1'b0));
  FDRE \res_mem_reg[191] 
       (.C(clk),
        .CE(E),
        .D(D[191]),
        .Q(res_mem[191]),
        .R(1'b0));
  FDRE \res_mem_reg[192] 
       (.C(clk),
        .CE(E),
        .D(D[192]),
        .Q(res_mem[192]),
        .R(1'b0));
  FDRE \res_mem_reg[193] 
       (.C(clk),
        .CE(E),
        .D(D[193]),
        .Q(res_mem[193]),
        .R(1'b0));
  FDRE \res_mem_reg[194] 
       (.C(clk),
        .CE(E),
        .D(D[194]),
        .Q(res_mem[194]),
        .R(1'b0));
  FDRE \res_mem_reg[195] 
       (.C(clk),
        .CE(E),
        .D(D[195]),
        .Q(res_mem[195]),
        .R(1'b0));
  FDRE \res_mem_reg[196] 
       (.C(clk),
        .CE(E),
        .D(D[196]),
        .Q(res_mem[196]),
        .R(1'b0));
  FDRE \res_mem_reg[197] 
       (.C(clk),
        .CE(E),
        .D(D[197]),
        .Q(res_mem[197]),
        .R(1'b0));
  FDRE \res_mem_reg[198] 
       (.C(clk),
        .CE(E),
        .D(D[198]),
        .Q(res_mem[198]),
        .R(1'b0));
  FDRE \res_mem_reg[199] 
       (.C(clk),
        .CE(E),
        .D(D[199]),
        .Q(res_mem[199]),
        .R(1'b0));
  FDRE \res_mem_reg[19] 
       (.C(clk),
        .CE(E),
        .D(D[19]),
        .Q(res_mem[19]),
        .R(1'b0));
  FDRE \res_mem_reg[1] 
       (.C(clk),
        .CE(E),
        .D(D[1]),
        .Q(res_mem[1]),
        .R(1'b0));
  FDRE \res_mem_reg[200] 
       (.C(clk),
        .CE(E),
        .D(D[200]),
        .Q(res_mem[200]),
        .R(1'b0));
  FDRE \res_mem_reg[201] 
       (.C(clk),
        .CE(E),
        .D(D[201]),
        .Q(res_mem[201]),
        .R(1'b0));
  FDRE \res_mem_reg[202] 
       (.C(clk),
        .CE(E),
        .D(D[202]),
        .Q(res_mem[202]),
        .R(1'b0));
  FDRE \res_mem_reg[203] 
       (.C(clk),
        .CE(E),
        .D(D[203]),
        .Q(res_mem[203]),
        .R(1'b0));
  FDRE \res_mem_reg[204] 
       (.C(clk),
        .CE(E),
        .D(D[204]),
        .Q(res_mem[204]),
        .R(1'b0));
  FDRE \res_mem_reg[205] 
       (.C(clk),
        .CE(E),
        .D(D[205]),
        .Q(res_mem[205]),
        .R(1'b0));
  FDRE \res_mem_reg[206] 
       (.C(clk),
        .CE(E),
        .D(D[206]),
        .Q(res_mem[206]),
        .R(1'b0));
  FDRE \res_mem_reg[207] 
       (.C(clk),
        .CE(E),
        .D(D[207]),
        .Q(res_mem[207]),
        .R(1'b0));
  FDRE \res_mem_reg[208] 
       (.C(clk),
        .CE(E),
        .D(D[208]),
        .Q(res_mem[208]),
        .R(1'b0));
  FDRE \res_mem_reg[209] 
       (.C(clk),
        .CE(E),
        .D(D[209]),
        .Q(res_mem[209]),
        .R(1'b0));
  FDRE \res_mem_reg[20] 
       (.C(clk),
        .CE(E),
        .D(D[20]),
        .Q(res_mem[20]),
        .R(1'b0));
  FDRE \res_mem_reg[210] 
       (.C(clk),
        .CE(E),
        .D(D[210]),
        .Q(res_mem[210]),
        .R(1'b0));
  FDRE \res_mem_reg[211] 
       (.C(clk),
        .CE(E),
        .D(D[211]),
        .Q(res_mem[211]),
        .R(1'b0));
  FDRE \res_mem_reg[212] 
       (.C(clk),
        .CE(E),
        .D(D[212]),
        .Q(res_mem[212]),
        .R(1'b0));
  FDRE \res_mem_reg[213] 
       (.C(clk),
        .CE(E),
        .D(D[213]),
        .Q(res_mem[213]),
        .R(1'b0));
  FDRE \res_mem_reg[214] 
       (.C(clk),
        .CE(E),
        .D(D[214]),
        .Q(res_mem[214]),
        .R(1'b0));
  FDRE \res_mem_reg[215] 
       (.C(clk),
        .CE(E),
        .D(D[215]),
        .Q(res_mem[215]),
        .R(1'b0));
  FDRE \res_mem_reg[216] 
       (.C(clk),
        .CE(E),
        .D(D[216]),
        .Q(res_mem[216]),
        .R(1'b0));
  FDRE \res_mem_reg[217] 
       (.C(clk),
        .CE(E),
        .D(D[217]),
        .Q(res_mem[217]),
        .R(1'b0));
  FDRE \res_mem_reg[218] 
       (.C(clk),
        .CE(E),
        .D(D[218]),
        .Q(res_mem[218]),
        .R(1'b0));
  FDRE \res_mem_reg[219] 
       (.C(clk),
        .CE(E),
        .D(D[219]),
        .Q(res_mem[219]),
        .R(1'b0));
  FDRE \res_mem_reg[21] 
       (.C(clk),
        .CE(E),
        .D(D[21]),
        .Q(res_mem[21]),
        .R(1'b0));
  FDRE \res_mem_reg[220] 
       (.C(clk),
        .CE(E),
        .D(D[220]),
        .Q(res_mem[220]),
        .R(1'b0));
  FDRE \res_mem_reg[221] 
       (.C(clk),
        .CE(E),
        .D(D[221]),
        .Q(res_mem[221]),
        .R(1'b0));
  FDRE \res_mem_reg[222] 
       (.C(clk),
        .CE(E),
        .D(D[222]),
        .Q(res_mem[222]),
        .R(1'b0));
  FDRE \res_mem_reg[223] 
       (.C(clk),
        .CE(E),
        .D(D[223]),
        .Q(res_mem[223]),
        .R(1'b0));
  FDRE \res_mem_reg[224] 
       (.C(clk),
        .CE(E),
        .D(D[224]),
        .Q(res_mem[224]),
        .R(1'b0));
  FDRE \res_mem_reg[225] 
       (.C(clk),
        .CE(E),
        .D(D[225]),
        .Q(res_mem[225]),
        .R(1'b0));
  FDRE \res_mem_reg[226] 
       (.C(clk),
        .CE(E),
        .D(D[226]),
        .Q(res_mem[226]),
        .R(1'b0));
  FDRE \res_mem_reg[227] 
       (.C(clk),
        .CE(E),
        .D(D[227]),
        .Q(res_mem[227]),
        .R(1'b0));
  FDRE \res_mem_reg[228] 
       (.C(clk),
        .CE(E),
        .D(D[228]),
        .Q(res_mem[228]),
        .R(1'b0));
  FDRE \res_mem_reg[229] 
       (.C(clk),
        .CE(E),
        .D(D[229]),
        .Q(res_mem[229]),
        .R(1'b0));
  FDRE \res_mem_reg[22] 
       (.C(clk),
        .CE(E),
        .D(D[22]),
        .Q(res_mem[22]),
        .R(1'b0));
  FDRE \res_mem_reg[230] 
       (.C(clk),
        .CE(E),
        .D(D[230]),
        .Q(res_mem[230]),
        .R(1'b0));
  FDRE \res_mem_reg[231] 
       (.C(clk),
        .CE(E),
        .D(D[231]),
        .Q(res_mem[231]),
        .R(1'b0));
  FDRE \res_mem_reg[232] 
       (.C(clk),
        .CE(E),
        .D(D[232]),
        .Q(res_mem[232]),
        .R(1'b0));
  FDRE \res_mem_reg[233] 
       (.C(clk),
        .CE(E),
        .D(D[233]),
        .Q(res_mem[233]),
        .R(1'b0));
  FDRE \res_mem_reg[234] 
       (.C(clk),
        .CE(E),
        .D(D[234]),
        .Q(res_mem[234]),
        .R(1'b0));
  FDRE \res_mem_reg[235] 
       (.C(clk),
        .CE(E),
        .D(D[235]),
        .Q(res_mem[235]),
        .R(1'b0));
  FDRE \res_mem_reg[236] 
       (.C(clk),
        .CE(E),
        .D(D[236]),
        .Q(res_mem[236]),
        .R(1'b0));
  FDRE \res_mem_reg[237] 
       (.C(clk),
        .CE(E),
        .D(D[237]),
        .Q(res_mem[237]),
        .R(1'b0));
  FDRE \res_mem_reg[238] 
       (.C(clk),
        .CE(E),
        .D(D[238]),
        .Q(res_mem[238]),
        .R(1'b0));
  FDRE \res_mem_reg[239] 
       (.C(clk),
        .CE(E),
        .D(D[239]),
        .Q(res_mem[239]),
        .R(1'b0));
  FDRE \res_mem_reg[23] 
       (.C(clk),
        .CE(E),
        .D(D[23]),
        .Q(res_mem[23]),
        .R(1'b0));
  FDRE \res_mem_reg[240] 
       (.C(clk),
        .CE(E),
        .D(D[240]),
        .Q(res_mem[240]),
        .R(1'b0));
  FDRE \res_mem_reg[241] 
       (.C(clk),
        .CE(E),
        .D(D[241]),
        .Q(res_mem[241]),
        .R(1'b0));
  FDRE \res_mem_reg[242] 
       (.C(clk),
        .CE(E),
        .D(D[242]),
        .Q(res_mem[242]),
        .R(1'b0));
  FDRE \res_mem_reg[243] 
       (.C(clk),
        .CE(E),
        .D(D[243]),
        .Q(res_mem[243]),
        .R(1'b0));
  FDRE \res_mem_reg[244] 
       (.C(clk),
        .CE(E),
        .D(D[244]),
        .Q(res_mem[244]),
        .R(1'b0));
  FDRE \res_mem_reg[245] 
       (.C(clk),
        .CE(E),
        .D(D[245]),
        .Q(res_mem[245]),
        .R(1'b0));
  FDRE \res_mem_reg[246] 
       (.C(clk),
        .CE(E),
        .D(D[246]),
        .Q(res_mem[246]),
        .R(1'b0));
  FDRE \res_mem_reg[247] 
       (.C(clk),
        .CE(E),
        .D(D[247]),
        .Q(res_mem[247]),
        .R(1'b0));
  FDRE \res_mem_reg[248] 
       (.C(clk),
        .CE(E),
        .D(D[248]),
        .Q(res_mem[248]),
        .R(1'b0));
  FDRE \res_mem_reg[249] 
       (.C(clk),
        .CE(E),
        .D(D[249]),
        .Q(res_mem[249]),
        .R(1'b0));
  FDRE \res_mem_reg[24] 
       (.C(clk),
        .CE(E),
        .D(D[24]),
        .Q(res_mem[24]),
        .R(1'b0));
  FDRE \res_mem_reg[250] 
       (.C(clk),
        .CE(E),
        .D(D[250]),
        .Q(res_mem[250]),
        .R(1'b0));
  FDRE \res_mem_reg[251] 
       (.C(clk),
        .CE(E),
        .D(D[251]),
        .Q(res_mem[251]),
        .R(1'b0));
  FDRE \res_mem_reg[252] 
       (.C(clk),
        .CE(E),
        .D(D[252]),
        .Q(res_mem[252]),
        .R(1'b0));
  FDRE \res_mem_reg[253] 
       (.C(clk),
        .CE(E),
        .D(D[253]),
        .Q(res_mem[253]),
        .R(1'b0));
  FDRE \res_mem_reg[254] 
       (.C(clk),
        .CE(E),
        .D(D[254]),
        .Q(res_mem[254]),
        .R(1'b0));
  FDRE \res_mem_reg[255] 
       (.C(clk),
        .CE(E),
        .D(D[255]),
        .Q(res_mem[255]),
        .R(1'b0));
  FDRE \res_mem_reg[256] 
       (.C(clk),
        .CE(E),
        .D(D[256]),
        .Q(res_mem[256]),
        .R(1'b0));
  FDRE \res_mem_reg[257] 
       (.C(clk),
        .CE(E),
        .D(D[257]),
        .Q(res_mem[257]),
        .R(1'b0));
  FDRE \res_mem_reg[258] 
       (.C(clk),
        .CE(E),
        .D(D[258]),
        .Q(res_mem[258]),
        .R(1'b0));
  FDRE \res_mem_reg[259] 
       (.C(clk),
        .CE(E),
        .D(D[259]),
        .Q(res_mem[259]),
        .R(1'b0));
  FDRE \res_mem_reg[25] 
       (.C(clk),
        .CE(E),
        .D(D[25]),
        .Q(res_mem[25]),
        .R(1'b0));
  FDRE \res_mem_reg[260] 
       (.C(clk),
        .CE(E),
        .D(D[260]),
        .Q(res_mem[260]),
        .R(1'b0));
  FDRE \res_mem_reg[261] 
       (.C(clk),
        .CE(E),
        .D(D[261]),
        .Q(res_mem[261]),
        .R(1'b0));
  FDRE \res_mem_reg[262] 
       (.C(clk),
        .CE(E),
        .D(D[262]),
        .Q(res_mem[262]),
        .R(1'b0));
  FDRE \res_mem_reg[263] 
       (.C(clk),
        .CE(E),
        .D(D[263]),
        .Q(res_mem[263]),
        .R(1'b0));
  FDRE \res_mem_reg[264] 
       (.C(clk),
        .CE(E),
        .D(D[264]),
        .Q(res_mem[264]),
        .R(1'b0));
  FDRE \res_mem_reg[265] 
       (.C(clk),
        .CE(E),
        .D(D[265]),
        .Q(res_mem[265]),
        .R(1'b0));
  FDRE \res_mem_reg[266] 
       (.C(clk),
        .CE(E),
        .D(D[266]),
        .Q(res_mem[266]),
        .R(1'b0));
  FDRE \res_mem_reg[267] 
       (.C(clk),
        .CE(E),
        .D(D[267]),
        .Q(res_mem[267]),
        .R(1'b0));
  FDRE \res_mem_reg[268] 
       (.C(clk),
        .CE(E),
        .D(D[268]),
        .Q(res_mem[268]),
        .R(1'b0));
  FDRE \res_mem_reg[269] 
       (.C(clk),
        .CE(E),
        .D(D[269]),
        .Q(res_mem[269]),
        .R(1'b0));
  FDRE \res_mem_reg[26] 
       (.C(clk),
        .CE(E),
        .D(D[26]),
        .Q(res_mem[26]),
        .R(1'b0));
  FDRE \res_mem_reg[270] 
       (.C(clk),
        .CE(E),
        .D(D[270]),
        .Q(res_mem[270]),
        .R(1'b0));
  FDRE \res_mem_reg[271] 
       (.C(clk),
        .CE(E),
        .D(D[271]),
        .Q(res_mem[271]),
        .R(1'b0));
  FDRE \res_mem_reg[272] 
       (.C(clk),
        .CE(E),
        .D(D[272]),
        .Q(res_mem[272]),
        .R(1'b0));
  FDRE \res_mem_reg[273] 
       (.C(clk),
        .CE(E),
        .D(D[273]),
        .Q(res_mem[273]),
        .R(1'b0));
  FDRE \res_mem_reg[274] 
       (.C(clk),
        .CE(E),
        .D(D[274]),
        .Q(res_mem[274]),
        .R(1'b0));
  FDRE \res_mem_reg[275] 
       (.C(clk),
        .CE(E),
        .D(D[275]),
        .Q(res_mem[275]),
        .R(1'b0));
  FDRE \res_mem_reg[276] 
       (.C(clk),
        .CE(E),
        .D(D[276]),
        .Q(res_mem[276]),
        .R(1'b0));
  FDRE \res_mem_reg[277] 
       (.C(clk),
        .CE(E),
        .D(D[277]),
        .Q(res_mem[277]),
        .R(1'b0));
  FDRE \res_mem_reg[278] 
       (.C(clk),
        .CE(E),
        .D(D[278]),
        .Q(res_mem[278]),
        .R(1'b0));
  FDRE \res_mem_reg[279] 
       (.C(clk),
        .CE(E),
        .D(D[279]),
        .Q(res_mem[279]),
        .R(1'b0));
  FDRE \res_mem_reg[27] 
       (.C(clk),
        .CE(E),
        .D(D[27]),
        .Q(res_mem[27]),
        .R(1'b0));
  FDRE \res_mem_reg[280] 
       (.C(clk),
        .CE(E),
        .D(D[280]),
        .Q(res_mem[280]),
        .R(1'b0));
  FDRE \res_mem_reg[281] 
       (.C(clk),
        .CE(E),
        .D(D[281]),
        .Q(res_mem[281]),
        .R(1'b0));
  FDRE \res_mem_reg[282] 
       (.C(clk),
        .CE(E),
        .D(D[282]),
        .Q(res_mem[282]),
        .R(1'b0));
  FDRE \res_mem_reg[283] 
       (.C(clk),
        .CE(E),
        .D(D[283]),
        .Q(res_mem[283]),
        .R(1'b0));
  FDRE \res_mem_reg[284] 
       (.C(clk),
        .CE(E),
        .D(D[284]),
        .Q(res_mem[284]),
        .R(1'b0));
  FDRE \res_mem_reg[285] 
       (.C(clk),
        .CE(E),
        .D(D[285]),
        .Q(res_mem[285]),
        .R(1'b0));
  FDRE \res_mem_reg[286] 
       (.C(clk),
        .CE(E),
        .D(D[286]),
        .Q(res_mem[286]),
        .R(1'b0));
  FDRE \res_mem_reg[287] 
       (.C(clk),
        .CE(E),
        .D(D[287]),
        .Q(res_mem[287]),
        .R(1'b0));
  FDRE \res_mem_reg[288] 
       (.C(clk),
        .CE(E),
        .D(D[288]),
        .Q(res_mem[288]),
        .R(1'b0));
  FDRE \res_mem_reg[289] 
       (.C(clk),
        .CE(E),
        .D(D[289]),
        .Q(res_mem[289]),
        .R(1'b0));
  FDRE \res_mem_reg[28] 
       (.C(clk),
        .CE(E),
        .D(D[28]),
        .Q(res_mem[28]),
        .R(1'b0));
  FDRE \res_mem_reg[290] 
       (.C(clk),
        .CE(E),
        .D(D[290]),
        .Q(res_mem[290]),
        .R(1'b0));
  FDRE \res_mem_reg[291] 
       (.C(clk),
        .CE(E),
        .D(D[291]),
        .Q(res_mem[291]),
        .R(1'b0));
  FDRE \res_mem_reg[292] 
       (.C(clk),
        .CE(E),
        .D(D[292]),
        .Q(res_mem[292]),
        .R(1'b0));
  FDRE \res_mem_reg[293] 
       (.C(clk),
        .CE(E),
        .D(D[293]),
        .Q(res_mem[293]),
        .R(1'b0));
  FDRE \res_mem_reg[294] 
       (.C(clk),
        .CE(E),
        .D(D[294]),
        .Q(res_mem[294]),
        .R(1'b0));
  FDRE \res_mem_reg[295] 
       (.C(clk),
        .CE(E),
        .D(D[295]),
        .Q(res_mem[295]),
        .R(1'b0));
  FDRE \res_mem_reg[296] 
       (.C(clk),
        .CE(E),
        .D(D[296]),
        .Q(res_mem[296]),
        .R(1'b0));
  FDRE \res_mem_reg[297] 
       (.C(clk),
        .CE(E),
        .D(D[297]),
        .Q(res_mem[297]),
        .R(1'b0));
  FDRE \res_mem_reg[298] 
       (.C(clk),
        .CE(E),
        .D(D[298]),
        .Q(res_mem[298]),
        .R(1'b0));
  FDRE \res_mem_reg[299] 
       (.C(clk),
        .CE(E),
        .D(D[299]),
        .Q(res_mem[299]),
        .R(1'b0));
  FDRE \res_mem_reg[29] 
       (.C(clk),
        .CE(E),
        .D(D[29]),
        .Q(res_mem[29]),
        .R(1'b0));
  FDRE \res_mem_reg[2] 
       (.C(clk),
        .CE(E),
        .D(D[2]),
        .Q(res_mem[2]),
        .R(1'b0));
  FDRE \res_mem_reg[300] 
       (.C(clk),
        .CE(E),
        .D(D[300]),
        .Q(res_mem[300]),
        .R(1'b0));
  FDRE \res_mem_reg[301] 
       (.C(clk),
        .CE(E),
        .D(D[301]),
        .Q(res_mem[301]),
        .R(1'b0));
  FDRE \res_mem_reg[302] 
       (.C(clk),
        .CE(E),
        .D(D[302]),
        .Q(res_mem[302]),
        .R(1'b0));
  FDRE \res_mem_reg[303] 
       (.C(clk),
        .CE(E),
        .D(D[303]),
        .Q(res_mem[303]),
        .R(1'b0));
  FDRE \res_mem_reg[304] 
       (.C(clk),
        .CE(E),
        .D(D[304]),
        .Q(res_mem[304]),
        .R(1'b0));
  FDRE \res_mem_reg[305] 
       (.C(clk),
        .CE(E),
        .D(D[305]),
        .Q(res_mem[305]),
        .R(1'b0));
  FDRE \res_mem_reg[306] 
       (.C(clk),
        .CE(E),
        .D(D[306]),
        .Q(res_mem[306]),
        .R(1'b0));
  FDRE \res_mem_reg[307] 
       (.C(clk),
        .CE(E),
        .D(D[307]),
        .Q(res_mem[307]),
        .R(1'b0));
  FDRE \res_mem_reg[308] 
       (.C(clk),
        .CE(E),
        .D(D[308]),
        .Q(res_mem[308]),
        .R(1'b0));
  FDRE \res_mem_reg[309] 
       (.C(clk),
        .CE(E),
        .D(D[309]),
        .Q(res_mem[309]),
        .R(1'b0));
  FDRE \res_mem_reg[30] 
       (.C(clk),
        .CE(E),
        .D(D[30]),
        .Q(res_mem[30]),
        .R(1'b0));
  FDRE \res_mem_reg[310] 
       (.C(clk),
        .CE(E),
        .D(D[310]),
        .Q(res_mem[310]),
        .R(1'b0));
  FDRE \res_mem_reg[311] 
       (.C(clk),
        .CE(E),
        .D(D[311]),
        .Q(res_mem[311]),
        .R(1'b0));
  FDRE \res_mem_reg[312] 
       (.C(clk),
        .CE(E),
        .D(D[312]),
        .Q(res_mem[312]),
        .R(1'b0));
  FDRE \res_mem_reg[313] 
       (.C(clk),
        .CE(E),
        .D(D[313]),
        .Q(res_mem[313]),
        .R(1'b0));
  FDRE \res_mem_reg[314] 
       (.C(clk),
        .CE(E),
        .D(D[314]),
        .Q(res_mem[314]),
        .R(1'b0));
  FDRE \res_mem_reg[315] 
       (.C(clk),
        .CE(E),
        .D(D[315]),
        .Q(res_mem[315]),
        .R(1'b0));
  FDRE \res_mem_reg[316] 
       (.C(clk),
        .CE(E),
        .D(D[316]),
        .Q(res_mem[316]),
        .R(1'b0));
  FDRE \res_mem_reg[317] 
       (.C(clk),
        .CE(E),
        .D(D[317]),
        .Q(res_mem[317]),
        .R(1'b0));
  FDRE \res_mem_reg[318] 
       (.C(clk),
        .CE(E),
        .D(D[318]),
        .Q(res_mem[318]),
        .R(1'b0));
  FDRE \res_mem_reg[319] 
       (.C(clk),
        .CE(E),
        .D(D[319]),
        .Q(res_mem[319]),
        .R(1'b0));
  FDRE \res_mem_reg[31] 
       (.C(clk),
        .CE(E),
        .D(D[31]),
        .Q(res_mem[31]),
        .R(1'b0));
  FDRE \res_mem_reg[320] 
       (.C(clk),
        .CE(E),
        .D(D[320]),
        .Q(res_mem[320]),
        .R(1'b0));
  FDRE \res_mem_reg[321] 
       (.C(clk),
        .CE(E),
        .D(D[321]),
        .Q(res_mem[321]),
        .R(1'b0));
  FDRE \res_mem_reg[322] 
       (.C(clk),
        .CE(E),
        .D(D[322]),
        .Q(res_mem[322]),
        .R(1'b0));
  FDRE \res_mem_reg[323] 
       (.C(clk),
        .CE(E),
        .D(D[323]),
        .Q(res_mem[323]),
        .R(1'b0));
  FDRE \res_mem_reg[324] 
       (.C(clk),
        .CE(E),
        .D(D[324]),
        .Q(res_mem[324]),
        .R(1'b0));
  FDRE \res_mem_reg[325] 
       (.C(clk),
        .CE(E),
        .D(D[325]),
        .Q(res_mem[325]),
        .R(1'b0));
  FDRE \res_mem_reg[326] 
       (.C(clk),
        .CE(E),
        .D(D[326]),
        .Q(res_mem[326]),
        .R(1'b0));
  FDRE \res_mem_reg[327] 
       (.C(clk),
        .CE(E),
        .D(D[327]),
        .Q(res_mem[327]),
        .R(1'b0));
  FDRE \res_mem_reg[328] 
       (.C(clk),
        .CE(E),
        .D(D[328]),
        .Q(res_mem[328]),
        .R(1'b0));
  FDRE \res_mem_reg[329] 
       (.C(clk),
        .CE(E),
        .D(D[329]),
        .Q(res_mem[329]),
        .R(1'b0));
  FDRE \res_mem_reg[32] 
       (.C(clk),
        .CE(E),
        .D(D[32]),
        .Q(res_mem[32]),
        .R(1'b0));
  FDRE \res_mem_reg[330] 
       (.C(clk),
        .CE(E),
        .D(D[330]),
        .Q(res_mem[330]),
        .R(1'b0));
  FDRE \res_mem_reg[331] 
       (.C(clk),
        .CE(E),
        .D(D[331]),
        .Q(res_mem[331]),
        .R(1'b0));
  FDRE \res_mem_reg[332] 
       (.C(clk),
        .CE(E),
        .D(D[332]),
        .Q(res_mem[332]),
        .R(1'b0));
  FDRE \res_mem_reg[333] 
       (.C(clk),
        .CE(E),
        .D(D[333]),
        .Q(res_mem[333]),
        .R(1'b0));
  FDRE \res_mem_reg[334] 
       (.C(clk),
        .CE(E),
        .D(D[334]),
        .Q(res_mem[334]),
        .R(1'b0));
  FDRE \res_mem_reg[335] 
       (.C(clk),
        .CE(E),
        .D(D[335]),
        .Q(res_mem[335]),
        .R(1'b0));
  FDRE \res_mem_reg[336] 
       (.C(clk),
        .CE(E),
        .D(D[336]),
        .Q(res_mem[336]),
        .R(1'b0));
  FDRE \res_mem_reg[337] 
       (.C(clk),
        .CE(E),
        .D(D[337]),
        .Q(res_mem[337]),
        .R(1'b0));
  FDRE \res_mem_reg[338] 
       (.C(clk),
        .CE(E),
        .D(D[338]),
        .Q(res_mem[338]),
        .R(1'b0));
  FDRE \res_mem_reg[339] 
       (.C(clk),
        .CE(E),
        .D(D[339]),
        .Q(res_mem[339]),
        .R(1'b0));
  FDRE \res_mem_reg[33] 
       (.C(clk),
        .CE(E),
        .D(D[33]),
        .Q(res_mem[33]),
        .R(1'b0));
  FDRE \res_mem_reg[340] 
       (.C(clk),
        .CE(E),
        .D(D[340]),
        .Q(res_mem[340]),
        .R(1'b0));
  FDRE \res_mem_reg[341] 
       (.C(clk),
        .CE(E),
        .D(D[341]),
        .Q(res_mem[341]),
        .R(1'b0));
  FDRE \res_mem_reg[342] 
       (.C(clk),
        .CE(E),
        .D(D[342]),
        .Q(res_mem[342]),
        .R(1'b0));
  FDRE \res_mem_reg[343] 
       (.C(clk),
        .CE(E),
        .D(D[343]),
        .Q(res_mem[343]),
        .R(1'b0));
  FDRE \res_mem_reg[344] 
       (.C(clk),
        .CE(E),
        .D(D[344]),
        .Q(res_mem[344]),
        .R(1'b0));
  FDRE \res_mem_reg[345] 
       (.C(clk),
        .CE(E),
        .D(D[345]),
        .Q(res_mem[345]),
        .R(1'b0));
  FDRE \res_mem_reg[346] 
       (.C(clk),
        .CE(E),
        .D(D[346]),
        .Q(res_mem[346]),
        .R(1'b0));
  FDRE \res_mem_reg[347] 
       (.C(clk),
        .CE(E),
        .D(D[347]),
        .Q(res_mem[347]),
        .R(1'b0));
  FDRE \res_mem_reg[348] 
       (.C(clk),
        .CE(E),
        .D(D[348]),
        .Q(res_mem[348]),
        .R(1'b0));
  FDRE \res_mem_reg[349] 
       (.C(clk),
        .CE(E),
        .D(D[349]),
        .Q(res_mem[349]),
        .R(1'b0));
  FDRE \res_mem_reg[34] 
       (.C(clk),
        .CE(E),
        .D(D[34]),
        .Q(res_mem[34]),
        .R(1'b0));
  FDRE \res_mem_reg[350] 
       (.C(clk),
        .CE(E),
        .D(D[350]),
        .Q(res_mem[350]),
        .R(1'b0));
  FDRE \res_mem_reg[351] 
       (.C(clk),
        .CE(E),
        .D(D[351]),
        .Q(res_mem[351]),
        .R(1'b0));
  FDRE \res_mem_reg[352] 
       (.C(clk),
        .CE(E),
        .D(D[352]),
        .Q(res_mem[352]),
        .R(1'b0));
  FDRE \res_mem_reg[353] 
       (.C(clk),
        .CE(E),
        .D(D[353]),
        .Q(res_mem[353]),
        .R(1'b0));
  FDRE \res_mem_reg[354] 
       (.C(clk),
        .CE(E),
        .D(D[354]),
        .Q(res_mem[354]),
        .R(1'b0));
  FDRE \res_mem_reg[355] 
       (.C(clk),
        .CE(E),
        .D(D[355]),
        .Q(res_mem[355]),
        .R(1'b0));
  FDRE \res_mem_reg[356] 
       (.C(clk),
        .CE(E),
        .D(D[356]),
        .Q(res_mem[356]),
        .R(1'b0));
  FDRE \res_mem_reg[357] 
       (.C(clk),
        .CE(E),
        .D(D[357]),
        .Q(res_mem[357]),
        .R(1'b0));
  FDRE \res_mem_reg[358] 
       (.C(clk),
        .CE(E),
        .D(D[358]),
        .Q(res_mem[358]),
        .R(1'b0));
  FDRE \res_mem_reg[359] 
       (.C(clk),
        .CE(E),
        .D(D[359]),
        .Q(res_mem[359]),
        .R(1'b0));
  FDRE \res_mem_reg[35] 
       (.C(clk),
        .CE(E),
        .D(D[35]),
        .Q(res_mem[35]),
        .R(1'b0));
  FDRE \res_mem_reg[360] 
       (.C(clk),
        .CE(E),
        .D(D[360]),
        .Q(res_mem[360]),
        .R(1'b0));
  FDRE \res_mem_reg[361] 
       (.C(clk),
        .CE(E),
        .D(D[361]),
        .Q(res_mem[361]),
        .R(1'b0));
  FDRE \res_mem_reg[362] 
       (.C(clk),
        .CE(E),
        .D(D[362]),
        .Q(res_mem[362]),
        .R(1'b0));
  FDRE \res_mem_reg[363] 
       (.C(clk),
        .CE(E),
        .D(D[363]),
        .Q(res_mem[363]),
        .R(1'b0));
  FDRE \res_mem_reg[364] 
       (.C(clk),
        .CE(E),
        .D(D[364]),
        .Q(res_mem[364]),
        .R(1'b0));
  FDRE \res_mem_reg[365] 
       (.C(clk),
        .CE(E),
        .D(D[365]),
        .Q(res_mem[365]),
        .R(1'b0));
  FDRE \res_mem_reg[366] 
       (.C(clk),
        .CE(E),
        .D(D[366]),
        .Q(res_mem[366]),
        .R(1'b0));
  FDRE \res_mem_reg[367] 
       (.C(clk),
        .CE(E),
        .D(D[367]),
        .Q(res_mem[367]),
        .R(1'b0));
  FDRE \res_mem_reg[368] 
       (.C(clk),
        .CE(E),
        .D(D[368]),
        .Q(res_mem[368]),
        .R(1'b0));
  FDRE \res_mem_reg[369] 
       (.C(clk),
        .CE(E),
        .D(D[369]),
        .Q(res_mem[369]),
        .R(1'b0));
  FDRE \res_mem_reg[36] 
       (.C(clk),
        .CE(E),
        .D(D[36]),
        .Q(res_mem[36]),
        .R(1'b0));
  FDRE \res_mem_reg[370] 
       (.C(clk),
        .CE(E),
        .D(D[370]),
        .Q(res_mem[370]),
        .R(1'b0));
  FDRE \res_mem_reg[371] 
       (.C(clk),
        .CE(E),
        .D(D[371]),
        .Q(res_mem[371]),
        .R(1'b0));
  FDRE \res_mem_reg[372] 
       (.C(clk),
        .CE(E),
        .D(D[372]),
        .Q(res_mem[372]),
        .R(1'b0));
  FDRE \res_mem_reg[373] 
       (.C(clk),
        .CE(E),
        .D(D[373]),
        .Q(res_mem[373]),
        .R(1'b0));
  FDRE \res_mem_reg[374] 
       (.C(clk),
        .CE(E),
        .D(D[374]),
        .Q(res_mem[374]),
        .R(1'b0));
  FDRE \res_mem_reg[375] 
       (.C(clk),
        .CE(E),
        .D(D[375]),
        .Q(res_mem[375]),
        .R(1'b0));
  FDRE \res_mem_reg[376] 
       (.C(clk),
        .CE(E),
        .D(D[376]),
        .Q(res_mem[376]),
        .R(1'b0));
  FDRE \res_mem_reg[377] 
       (.C(clk),
        .CE(E),
        .D(D[377]),
        .Q(res_mem[377]),
        .R(1'b0));
  FDRE \res_mem_reg[378] 
       (.C(clk),
        .CE(E),
        .D(D[378]),
        .Q(res_mem[378]),
        .R(1'b0));
  FDRE \res_mem_reg[379] 
       (.C(clk),
        .CE(E),
        .D(D[379]),
        .Q(res_mem[379]),
        .R(1'b0));
  FDRE \res_mem_reg[37] 
       (.C(clk),
        .CE(E),
        .D(D[37]),
        .Q(res_mem[37]),
        .R(1'b0));
  FDRE \res_mem_reg[380] 
       (.C(clk),
        .CE(E),
        .D(D[380]),
        .Q(res_mem[380]),
        .R(1'b0));
  FDRE \res_mem_reg[381] 
       (.C(clk),
        .CE(E),
        .D(D[381]),
        .Q(res_mem[381]),
        .R(1'b0));
  FDRE \res_mem_reg[382] 
       (.C(clk),
        .CE(E),
        .D(D[382]),
        .Q(res_mem[382]),
        .R(1'b0));
  FDRE \res_mem_reg[383] 
       (.C(clk),
        .CE(E),
        .D(D[383]),
        .Q(res_mem[383]),
        .R(1'b0));
  FDRE \res_mem_reg[38] 
       (.C(clk),
        .CE(E),
        .D(D[38]),
        .Q(res_mem[38]),
        .R(1'b0));
  FDRE \res_mem_reg[39] 
       (.C(clk),
        .CE(E),
        .D(D[39]),
        .Q(res_mem[39]),
        .R(1'b0));
  FDRE \res_mem_reg[3] 
       (.C(clk),
        .CE(E),
        .D(D[3]),
        .Q(res_mem[3]),
        .R(1'b0));
  FDRE \res_mem_reg[40] 
       (.C(clk),
        .CE(E),
        .D(D[40]),
        .Q(res_mem[40]),
        .R(1'b0));
  FDRE \res_mem_reg[41] 
       (.C(clk),
        .CE(E),
        .D(D[41]),
        .Q(res_mem[41]),
        .R(1'b0));
  FDRE \res_mem_reg[42] 
       (.C(clk),
        .CE(E),
        .D(D[42]),
        .Q(res_mem[42]),
        .R(1'b0));
  FDRE \res_mem_reg[43] 
       (.C(clk),
        .CE(E),
        .D(D[43]),
        .Q(res_mem[43]),
        .R(1'b0));
  FDRE \res_mem_reg[44] 
       (.C(clk),
        .CE(E),
        .D(D[44]),
        .Q(res_mem[44]),
        .R(1'b0));
  FDRE \res_mem_reg[45] 
       (.C(clk),
        .CE(E),
        .D(D[45]),
        .Q(res_mem[45]),
        .R(1'b0));
  FDRE \res_mem_reg[46] 
       (.C(clk),
        .CE(E),
        .D(D[46]),
        .Q(res_mem[46]),
        .R(1'b0));
  FDRE \res_mem_reg[47] 
       (.C(clk),
        .CE(E),
        .D(D[47]),
        .Q(res_mem[47]),
        .R(1'b0));
  FDRE \res_mem_reg[48] 
       (.C(clk),
        .CE(E),
        .D(D[48]),
        .Q(res_mem[48]),
        .R(1'b0));
  FDRE \res_mem_reg[49] 
       (.C(clk),
        .CE(E),
        .D(D[49]),
        .Q(res_mem[49]),
        .R(1'b0));
  FDRE \res_mem_reg[4] 
       (.C(clk),
        .CE(E),
        .D(D[4]),
        .Q(res_mem[4]),
        .R(1'b0));
  FDRE \res_mem_reg[50] 
       (.C(clk),
        .CE(E),
        .D(D[50]),
        .Q(res_mem[50]),
        .R(1'b0));
  FDRE \res_mem_reg[51] 
       (.C(clk),
        .CE(E),
        .D(D[51]),
        .Q(res_mem[51]),
        .R(1'b0));
  FDRE \res_mem_reg[52] 
       (.C(clk),
        .CE(E),
        .D(D[52]),
        .Q(res_mem[52]),
        .R(1'b0));
  FDRE \res_mem_reg[53] 
       (.C(clk),
        .CE(E),
        .D(D[53]),
        .Q(res_mem[53]),
        .R(1'b0));
  FDRE \res_mem_reg[54] 
       (.C(clk),
        .CE(E),
        .D(D[54]),
        .Q(res_mem[54]),
        .R(1'b0));
  FDRE \res_mem_reg[55] 
       (.C(clk),
        .CE(E),
        .D(D[55]),
        .Q(res_mem[55]),
        .R(1'b0));
  FDRE \res_mem_reg[56] 
       (.C(clk),
        .CE(E),
        .D(D[56]),
        .Q(res_mem[56]),
        .R(1'b0));
  FDRE \res_mem_reg[57] 
       (.C(clk),
        .CE(E),
        .D(D[57]),
        .Q(res_mem[57]),
        .R(1'b0));
  FDRE \res_mem_reg[58] 
       (.C(clk),
        .CE(E),
        .D(D[58]),
        .Q(res_mem[58]),
        .R(1'b0));
  FDRE \res_mem_reg[59] 
       (.C(clk),
        .CE(E),
        .D(D[59]),
        .Q(res_mem[59]),
        .R(1'b0));
  FDRE \res_mem_reg[5] 
       (.C(clk),
        .CE(E),
        .D(D[5]),
        .Q(res_mem[5]),
        .R(1'b0));
  FDRE \res_mem_reg[60] 
       (.C(clk),
        .CE(E),
        .D(D[60]),
        .Q(res_mem[60]),
        .R(1'b0));
  FDRE \res_mem_reg[61] 
       (.C(clk),
        .CE(E),
        .D(D[61]),
        .Q(res_mem[61]),
        .R(1'b0));
  FDRE \res_mem_reg[62] 
       (.C(clk),
        .CE(E),
        .D(D[62]),
        .Q(res_mem[62]),
        .R(1'b0));
  FDRE \res_mem_reg[63] 
       (.C(clk),
        .CE(E),
        .D(D[63]),
        .Q(res_mem[63]),
        .R(1'b0));
  FDRE \res_mem_reg[64] 
       (.C(clk),
        .CE(E),
        .D(D[64]),
        .Q(res_mem[64]),
        .R(1'b0));
  FDRE \res_mem_reg[65] 
       (.C(clk),
        .CE(E),
        .D(D[65]),
        .Q(res_mem[65]),
        .R(1'b0));
  FDRE \res_mem_reg[66] 
       (.C(clk),
        .CE(E),
        .D(D[66]),
        .Q(res_mem[66]),
        .R(1'b0));
  FDRE \res_mem_reg[67] 
       (.C(clk),
        .CE(E),
        .D(D[67]),
        .Q(res_mem[67]),
        .R(1'b0));
  FDRE \res_mem_reg[68] 
       (.C(clk),
        .CE(E),
        .D(D[68]),
        .Q(res_mem[68]),
        .R(1'b0));
  FDRE \res_mem_reg[69] 
       (.C(clk),
        .CE(E),
        .D(D[69]),
        .Q(res_mem[69]),
        .R(1'b0));
  FDRE \res_mem_reg[6] 
       (.C(clk),
        .CE(E),
        .D(D[6]),
        .Q(res_mem[6]),
        .R(1'b0));
  FDRE \res_mem_reg[70] 
       (.C(clk),
        .CE(E),
        .D(D[70]),
        .Q(res_mem[70]),
        .R(1'b0));
  FDRE \res_mem_reg[71] 
       (.C(clk),
        .CE(E),
        .D(D[71]),
        .Q(res_mem[71]),
        .R(1'b0));
  FDRE \res_mem_reg[72] 
       (.C(clk),
        .CE(E),
        .D(D[72]),
        .Q(res_mem[72]),
        .R(1'b0));
  FDRE \res_mem_reg[73] 
       (.C(clk),
        .CE(E),
        .D(D[73]),
        .Q(res_mem[73]),
        .R(1'b0));
  FDRE \res_mem_reg[74] 
       (.C(clk),
        .CE(E),
        .D(D[74]),
        .Q(res_mem[74]),
        .R(1'b0));
  FDRE \res_mem_reg[75] 
       (.C(clk),
        .CE(E),
        .D(D[75]),
        .Q(res_mem[75]),
        .R(1'b0));
  FDRE \res_mem_reg[76] 
       (.C(clk),
        .CE(E),
        .D(D[76]),
        .Q(res_mem[76]),
        .R(1'b0));
  FDRE \res_mem_reg[77] 
       (.C(clk),
        .CE(E),
        .D(D[77]),
        .Q(res_mem[77]),
        .R(1'b0));
  FDRE \res_mem_reg[78] 
       (.C(clk),
        .CE(E),
        .D(D[78]),
        .Q(res_mem[78]),
        .R(1'b0));
  FDRE \res_mem_reg[79] 
       (.C(clk),
        .CE(E),
        .D(D[79]),
        .Q(res_mem[79]),
        .R(1'b0));
  FDRE \res_mem_reg[7] 
       (.C(clk),
        .CE(E),
        .D(D[7]),
        .Q(res_mem[7]),
        .R(1'b0));
  FDRE \res_mem_reg[80] 
       (.C(clk),
        .CE(E),
        .D(D[80]),
        .Q(res_mem[80]),
        .R(1'b0));
  FDRE \res_mem_reg[81] 
       (.C(clk),
        .CE(E),
        .D(D[81]),
        .Q(res_mem[81]),
        .R(1'b0));
  FDRE \res_mem_reg[82] 
       (.C(clk),
        .CE(E),
        .D(D[82]),
        .Q(res_mem[82]),
        .R(1'b0));
  FDRE \res_mem_reg[83] 
       (.C(clk),
        .CE(E),
        .D(D[83]),
        .Q(res_mem[83]),
        .R(1'b0));
  FDRE \res_mem_reg[84] 
       (.C(clk),
        .CE(E),
        .D(D[84]),
        .Q(res_mem[84]),
        .R(1'b0));
  FDRE \res_mem_reg[85] 
       (.C(clk),
        .CE(E),
        .D(D[85]),
        .Q(res_mem[85]),
        .R(1'b0));
  FDRE \res_mem_reg[86] 
       (.C(clk),
        .CE(E),
        .D(D[86]),
        .Q(res_mem[86]),
        .R(1'b0));
  FDRE \res_mem_reg[87] 
       (.C(clk),
        .CE(E),
        .D(D[87]),
        .Q(res_mem[87]),
        .R(1'b0));
  FDRE \res_mem_reg[88] 
       (.C(clk),
        .CE(E),
        .D(D[88]),
        .Q(res_mem[88]),
        .R(1'b0));
  FDRE \res_mem_reg[89] 
       (.C(clk),
        .CE(E),
        .D(D[89]),
        .Q(res_mem[89]),
        .R(1'b0));
  FDRE \res_mem_reg[8] 
       (.C(clk),
        .CE(E),
        .D(D[8]),
        .Q(res_mem[8]),
        .R(1'b0));
  FDRE \res_mem_reg[90] 
       (.C(clk),
        .CE(E),
        .D(D[90]),
        .Q(res_mem[90]),
        .R(1'b0));
  FDRE \res_mem_reg[91] 
       (.C(clk),
        .CE(E),
        .D(D[91]),
        .Q(res_mem[91]),
        .R(1'b0));
  FDRE \res_mem_reg[92] 
       (.C(clk),
        .CE(E),
        .D(D[92]),
        .Q(res_mem[92]),
        .R(1'b0));
  FDRE \res_mem_reg[93] 
       (.C(clk),
        .CE(E),
        .D(D[93]),
        .Q(res_mem[93]),
        .R(1'b0));
  FDRE \res_mem_reg[94] 
       (.C(clk),
        .CE(E),
        .D(D[94]),
        .Q(res_mem[94]),
        .R(1'b0));
  FDRE \res_mem_reg[95] 
       (.C(clk),
        .CE(E),
        .D(D[95]),
        .Q(res_mem[95]),
        .R(1'b0));
  FDRE \res_mem_reg[96] 
       (.C(clk),
        .CE(E),
        .D(D[96]),
        .Q(res_mem[96]),
        .R(1'b0));
  FDRE \res_mem_reg[97] 
       (.C(clk),
        .CE(E),
        .D(D[97]),
        .Q(res_mem[97]),
        .R(1'b0));
  FDRE \res_mem_reg[98] 
       (.C(clk),
        .CE(E),
        .D(D[98]),
        .Q(res_mem[98]),
        .R(1'b0));
  FDRE \res_mem_reg[99] 
       (.C(clk),
        .CE(E),
        .D(D[99]),
        .Q(res_mem[99]),
        .R(1'b0));
  FDRE \res_mem_reg[9] 
       (.C(clk),
        .CE(E),
        .D(D[9]),
        .Q(res_mem[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFF4C)) 
    start_i_1
       (.I0(aresetn),
        .I1(start),
        .I2(\m_axis_tdata[63]_i_7_n_0 ),
        .I3(E),
        .O(start_i_1_n_0));
  FDRE start_reg
       (.C(clk),
        .CE(1'b1),
        .D(start_i_1_n_0),
        .Q(start),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    t_valid_flag_i_1
       (.I0(counter[1]),
        .I1(counter[0]),
        .I2(m_axis_tvalid),
        .I3(m_axis_tready),
        .I4(counter[2]),
        .O(t_valid_flag_i_1_n_0));
  FDCE t_valid_flag_reg
       (.C(clk),
        .CE(\m_axis_tdata[63]_i_1_n_0 ),
        .CLR(aresetn_0),
        .D(t_valid_flag_i_1_n_0),
        .Q(m_axis_tvalid));
endmodule

(* ORIG_REF_NAME = "b_ram_bank" *) 
module design_1_top_0_0_b_ram_bank
   (s_axi_ctrl_status_awready,
    s_axi_ctrl_status_wready,
    s_axi_ctrl_status_arready,
    b_ram_out,
    Q,
    s_axi_ctrl_status_bvalid,
    s_axi_ctrl_status_rvalid,
    in_stream_ready_reg,
    SR,
    \counter_reg[2]_0 ,
    \counter_reg[2]_1 ,
    \counter_reg[2]_2 ,
    \counter_reg[2]_3 ,
    \counter_reg[2]_4 ,
    \counter_reg[2]_5 ,
    \counter_reg[2]_6 ,
    E,
    dp_enable,
    p_rdy,
    \counter_reg[1]_0 ,
    S,
    \counter_reg[1]_1 ,
    \counter_reg[1]_2 ,
    \counter_reg[1]_3 ,
    \counter_reg[1]_4 ,
    \counter_reg[1]_5 ,
    \counter_reg[1]_6 ,
    \counter_reg[1]_7 ,
    \counter_reg[1]_8 ,
    O,
    s_axi_ctrl_status_rdata,
    aresetn_0,
    clk,
    s_axi_ctrl_status_wvalid,
    s_axi_ctrl_status_awvalid,
    s_axi_ctrl_status_arvalid,
    s_axis_tready,
    aresetn,
    s_axis_tvalid,
    dp_extend_end,
    in_stream_handshake_delay,
    CO,
    \slv_regs_reg[0][11] ,
    \slv_regs_reg[0][11]_0 ,
    \slv_regs_reg[0][11]_1 ,
    \slv_regs_reg[0][11]_2 ,
    \slv_regs_reg[0][11]_3 ,
    \slv_regs_reg[0][11]_4 ,
    \slv_regs_reg[0][11]_5 ,
    \slv_regs_reg[0][11]_6 ,
    \counter_reg[8] ,
    \counter_reg[5]_0 ,
    \counter_reg[5]_1 ,
    \counter_reg[8]_0 ,
    \counter_reg[8]_1 ,
    \counter_reg[8]_2 ,
    \counter_reg[5]_2 ,
    \counter_reg[5]_3 ,
    s_axi_ctrl_status_bready,
    s_axi_ctrl_status_rready,
    s_axi_ctrl_status_awaddr,
    s_axi_ctrl_status_wdata,
    s_axi_ctrl_status_araddr,
    s_axi_ctrl_status_wstrb);
  output s_axi_ctrl_status_awready;
  output s_axi_ctrl_status_wready;
  output s_axi_ctrl_status_arready;
  output [255:0]b_ram_out;
  output [0:0]Q;
  output s_axi_ctrl_status_bvalid;
  output s_axi_ctrl_status_rvalid;
  output in_stream_ready_reg;
  output [0:0]SR;
  output [0:0]\counter_reg[2]_0 ;
  output [0:0]\counter_reg[2]_1 ;
  output [0:0]\counter_reg[2]_2 ;
  output [0:0]\counter_reg[2]_3 ;
  output [0:0]\counter_reg[2]_4 ;
  output [0:0]\counter_reg[2]_5 ;
  output [0:0]\counter_reg[2]_6 ;
  output [0:0]E;
  output dp_enable;
  output p_rdy;
  output \counter_reg[1]_0 ;
  output [2:0]S;
  output [4:0]\counter_reg[1]_1 ;
  output [1:0]\counter_reg[1]_2 ;
  output [1:0]\counter_reg[1]_3 ;
  output [2:0]\counter_reg[1]_4 ;
  output [2:0]\counter_reg[1]_5 ;
  output [2:0]\counter_reg[1]_6 ;
  output [1:0]\counter_reg[1]_7 ;
  output [1:0]\counter_reg[1]_8 ;
  output [0:0]O;
  output [31:0]s_axi_ctrl_status_rdata;
  input aresetn_0;
  input clk;
  input s_axi_ctrl_status_wvalid;
  input s_axi_ctrl_status_awvalid;
  input s_axi_ctrl_status_arvalid;
  input s_axis_tready;
  input aresetn;
  input s_axis_tvalid;
  input dp_extend_end;
  input in_stream_handshake_delay;
  input [0:0]CO;
  input [0:0]\slv_regs_reg[0][11] ;
  input [0:0]\slv_regs_reg[0][11]_0 ;
  input [0:0]\slv_regs_reg[0][11]_1 ;
  input [0:0]\slv_regs_reg[0][11]_2 ;
  input [0:0]\slv_regs_reg[0][11]_3 ;
  input [0:0]\slv_regs_reg[0][11]_4 ;
  input [0:0]\slv_regs_reg[0][11]_5 ;
  input \slv_regs_reg[0][11]_6 ;
  input [5:0]\counter_reg[8] ;
  input [2:0]\counter_reg[5]_0 ;
  input [2:0]\counter_reg[5]_1 ;
  input [5:0]\counter_reg[8]_0 ;
  input [5:0]\counter_reg[8]_1 ;
  input [5:0]\counter_reg[8]_2 ;
  input [2:0]\counter_reg[5]_2 ;
  input [2:0]\counter_reg[5]_3 ;
  input s_axi_ctrl_status_bready;
  input s_axi_ctrl_status_rready;
  input [3:0]s_axi_ctrl_status_awaddr;
  input [31:0]s_axi_ctrl_status_wdata;
  input [3:0]s_axi_ctrl_status_araddr;
  input [3:0]s_axi_ctrl_status_wstrb;

  wire [0:0]CO;
  wire [0:0]E;
  wire [0:0]O;
  wire [0:0]Q;
  wire [2:0]S;
  wire [0:0]SR;
  wire aresetn;
  wire aresetn_0;
  wire [255:0]b_ram_out;
  wire [7:0]b_ram_sel;
  wire clk;
  wire [6:0]counter;
  wire counter1;
  wire counter1_carry__0_n_3;
  wire counter1_carry_n_0;
  wire counter1_carry_n_1;
  wire counter1_carry_n_2;
  wire counter1_carry_n_3;
  wire \counter[1]_i_1__0_n_0 ;
  wire \counter[2]_i_1__0_n_0 ;
  wire \counter[3]_i_1_n_0 ;
  wire \counter[4]_i_1__5_n_0 ;
  wire \counter[5]_i_1_n_0 ;
  wire \counter[6]_i_3_n_0 ;
  wire \counter[6]_i_4_n_0 ;
  wire \counter_reg[1]_0 ;
  wire [4:0]\counter_reg[1]_1 ;
  wire [1:0]\counter_reg[1]_2 ;
  wire [1:0]\counter_reg[1]_3 ;
  wire [2:0]\counter_reg[1]_4 ;
  wire [2:0]\counter_reg[1]_5 ;
  wire [2:0]\counter_reg[1]_6 ;
  wire [1:0]\counter_reg[1]_7 ;
  wire [1:0]\counter_reg[1]_8 ;
  wire [0:0]\counter_reg[2]_0 ;
  wire [0:0]\counter_reg[2]_1 ;
  wire [0:0]\counter_reg[2]_2 ;
  wire [0:0]\counter_reg[2]_3 ;
  wire [0:0]\counter_reg[2]_4 ;
  wire [0:0]\counter_reg[2]_5 ;
  wire [0:0]\counter_reg[2]_6 ;
  wire [2:0]\counter_reg[5]_0 ;
  wire [2:0]\counter_reg[5]_1 ;
  wire [2:0]\counter_reg[5]_2 ;
  wire [2:0]\counter_reg[5]_3 ;
  wire [5:0]\counter_reg[8] ;
  wire [5:0]\counter_reg[8]_0 ;
  wire [5:0]\counter_reg[8]_1 ;
  wire [5:0]\counter_reg[8]_2 ;
  wire [31:0]data_in_w;
  wire dp_enable;
  wire dp_extend_end;
  wire \i___1/slv_regs[10][15]_i_1_n_0 ;
  wire \i___1/slv_regs[10][23]_i_1_n_0 ;
  wire \i___1/slv_regs[10][31]_i_1_n_0 ;
  wire \i___1/slv_regs[10][7]_i_1_n_0 ;
  wire \i___1/slv_regs[11][15]_i_1_n_0 ;
  wire \i___1/slv_regs[11][23]_i_1_n_0 ;
  wire \i___1/slv_regs[11][31]_i_1_n_0 ;
  wire \i___1/slv_regs[11][7]_i_1_n_0 ;
  wire \i___1/slv_regs[12][15]_i_1_n_0 ;
  wire \i___1/slv_regs[12][23]_i_1_n_0 ;
  wire \i___1/slv_regs[12][31]_i_1_n_0 ;
  wire \i___1/slv_regs[12][7]_i_1_n_0 ;
  wire \i___1/slv_regs[13][15]_i_1_n_0 ;
  wire \i___1/slv_regs[13][23]_i_1_n_0 ;
  wire \i___1/slv_regs[13][31]_i_1_n_0 ;
  wire \i___1/slv_regs[13][7]_i_1_n_0 ;
  wire \i___1/slv_regs[14][15]_i_1_n_0 ;
  wire \i___1/slv_regs[14][23]_i_1_n_0 ;
  wire \i___1/slv_regs[14][31]_i_1_n_0 ;
  wire \i___1/slv_regs[14][7]_i_1_n_0 ;
  wire \i___1/slv_regs[15][15]_i_1_n_0 ;
  wire \i___1/slv_regs[15][23]_i_1_n_0 ;
  wire \i___1/slv_regs[15][31]_i_1_n_0 ;
  wire \i___1/slv_regs[15][7]_i_1_n_0 ;
  wire \i___1/slv_regs[1][15]_i_1_n_0 ;
  wire \i___1/slv_regs[1][23]_i_1_n_0 ;
  wire \i___1/slv_regs[1][31]_i_1_n_0 ;
  wire \i___1/slv_regs[1][7]_i_1_n_0 ;
  wire \i___1/slv_regs[2][15]_i_1_n_0 ;
  wire \i___1/slv_regs[2][23]_i_1_n_0 ;
  wire \i___1/slv_regs[2][31]_i_1_n_0 ;
  wire \i___1/slv_regs[2][7]_i_1_n_0 ;
  wire \i___1/slv_regs[3][15]_i_1_n_0 ;
  wire \i___1/slv_regs[3][23]_i_1_n_0 ;
  wire \i___1/slv_regs[3][31]_i_1_n_0 ;
  wire \i___1/slv_regs[3][7]_i_1_n_0 ;
  wire \i___1/slv_regs[4][15]_i_1_n_0 ;
  wire \i___1/slv_regs[4][23]_i_1_n_0 ;
  wire \i___1/slv_regs[4][31]_i_1_n_0 ;
  wire \i___1/slv_regs[4][7]_i_1_n_0 ;
  wire \i___1/slv_regs[5][15]_i_1_n_0 ;
  wire \i___1/slv_regs[5][23]_i_1_n_0 ;
  wire \i___1/slv_regs[5][31]_i_1_n_0 ;
  wire \i___1/slv_regs[5][7]_i_1_n_0 ;
  wire \i___1/slv_regs[6][15]_i_1_n_0 ;
  wire \i___1/slv_regs[6][23]_i_1_n_0 ;
  wire \i___1/slv_regs[6][31]_i_1_n_0 ;
  wire \i___1/slv_regs[6][7]_i_1_n_0 ;
  wire \i___1/slv_regs[7][15]_i_1_n_0 ;
  wire \i___1/slv_regs[7][23]_i_1_n_0 ;
  wire \i___1/slv_regs[7][31]_i_1_n_0 ;
  wire \i___1/slv_regs[7][7]_i_1_n_0 ;
  wire \i___1/slv_regs[8][15]_i_1_n_0 ;
  wire \i___1/slv_regs[8][23]_i_1_n_0 ;
  wire \i___1/slv_regs[8][31]_i_1_n_0 ;
  wire \i___1/slv_regs[8][7]_i_1_n_0 ;
  wire \i___1/slv_regs[9][15]_i_1_n_0 ;
  wire \i___1/slv_regs[9][23]_i_1_n_0 ;
  wire \i___1/slv_regs[9][31]_i_1_n_0 ;
  wire \i___1/slv_regs[9][7]_i_1_n_0 ;
  wire i__carry__0_i_2_n_0;
  wire i__carry__0_i_3_n_0;
  wire i__carry_i_1__0_n_0;
  wire i__carry_i_2_n_0;
  wire i__carry_i_3_n_0;
  wire i__carry_i_5_n_0;
  wire i__carry_i_6_n_0;
  wire i__carry_i_7_n_0;
  wire in_stream_handshake_delay;
  wire in_stream_ready_reg;
  wire initialized;
  wire [3:0]p_0_in;
  wire [6:0]p_0_in_0;
  wire p_1_in;
  wire [31:7]p_1_in_0;
  wire p_1_in__0;
  wire [7:0]p_2_in;
  wire p_rdy;
  wire \read_address[4]_i_2_n_0 ;
  wire \read_address[5]_i_2_n_0 ;
  wire \read_address[6]_i_4_n_0 ;
  wire \read_address_reg_n_0_[0] ;
  wire \read_address_reg_n_0_[1] ;
  wire \read_address_reg_n_0_[2] ;
  wire \read_address_reg_n_0_[3] ;
  wire \read_address_reg_n_0_[4] ;
  wire \read_address_reg_n_0_[5] ;
  wire \read_address_reg_n_0_[6] ;
  wire register_interface_n_124;
  wire register_interface_n_125;
  wire register_interface_n_126;
  wire register_interface_n_127;
  wire register_interface_n_128;
  wire register_interface_n_21;
  wire register_interface_n_22;
  wire register_interface_n_23;
  wire register_interface_n_24;
  wire register_interface_n_26;
  wire register_interface_n_36;
  wire register_interface_n_37;
  wire register_interface_n_38;
  wire register_interface_n_39;
  wire register_interface_n_40;
  wire register_interface_n_41;
  wire register_interface_n_42;
  wire register_interface_n_43;
  wire register_interface_n_44;
  wire register_interface_n_45;
  wire register_interface_n_46;
  wire register_interface_n_47;
  wire register_interface_n_48;
  wire register_interface_n_5;
  wire register_interface_n_50;
  wire register_interface_n_51;
  wire register_interface_n_52;
  wire register_interface_n_6;
  wire register_interface_n_7;
  wire register_interface_n_8;
  wire register_interface_n_81;
  wire register_interface_n_82;
  wire register_interface_n_83;
  wire register_interface_n_84;
  wire register_interface_n_85;
  wire register_interface_n_86;
  wire register_interface_n_87;
  wire register_interface_n_88;
  wire [3:0]s_axi_ctrl_status_araddr;
  wire s_axi_ctrl_status_arready;
  wire s_axi_ctrl_status_arvalid;
  wire [3:0]s_axi_ctrl_status_awaddr;
  wire s_axi_ctrl_status_awready;
  wire s_axi_ctrl_status_awvalid;
  wire s_axi_ctrl_status_bready;
  wire s_axi_ctrl_status_bvalid;
  wire [31:0]s_axi_ctrl_status_rdata;
  wire s_axi_ctrl_status_rready;
  wire s_axi_ctrl_status_rvalid;
  wire [31:0]s_axi_ctrl_status_wdata;
  wire s_axi_ctrl_status_wready;
  wire [3:0]s_axi_ctrl_status_wstrb;
  wire s_axi_ctrl_status_wvalid;
  wire s_axis_tready;
  wire s_axis_tvalid;
  wire slv_reg_wren__2;
  wire [0:0]\slv_regs_reg[0][11] ;
  wire [0:0]\slv_regs_reg[0][11]_0 ;
  wire [0:0]\slv_regs_reg[0][11]_1 ;
  wire [0:0]\slv_regs_reg[0][11]_2 ;
  wire [0:0]\slv_regs_reg[0][11]_3 ;
  wire [0:0]\slv_regs_reg[0][11]_4 ;
  wire [0:0]\slv_regs_reg[0][11]_5 ;
  wire \slv_regs_reg[0][11]_6 ;
  wire state0;
  wire \state0_inferred__3/i__carry__0_n_2 ;
  wire \state0_inferred__3/i__carry__0_n_3 ;
  wire \state0_inferred__3/i__carry_n_0 ;
  wire \state0_inferred__3/i__carry_n_1 ;
  wire \state0_inferred__3/i__carry_n_2 ;
  wire \state0_inferred__3/i__carry_n_3 ;
  wire [12:1]state1;
  wire state1_carry__0_n_0;
  wire state1_carry__0_n_1;
  wire state1_carry__0_n_2;
  wire state1_carry__0_n_3;
  wire state1_carry__1_n_0;
  wire state1_carry__1_n_2;
  wire state1_carry__1_n_3;
  wire state1_carry_n_0;
  wire state1_carry_n_1;
  wire state1_carry_n_2;
  wire state1_carry_n_3;
  wire \state_reg_n_0_[0] ;
  wire \state_reg_n_0_[1] ;
  wire [11:1]v_len;
  wire valid_input1_out;
  wire [3:0]NLW_counter1_carry_O_UNCONNECTED;
  wire [3:2]NLW_counter1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_counter1_carry__0_O_UNCONNECTED;
  wire [3:0]\NLW_state0_inferred__3/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_state0_inferred__3/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_state0_inferred__3/i__carry__0_O_UNCONNECTED ;
  wire [2:2]NLW_state1_carry__1_CO_UNCONNECTED;
  wire [3:3]NLW_state1_carry__1_O_UNCONNECTED;

  design_1_top_0_0_block_ram \b_ram[0].DUT 
       (.Q({\read_address_reg_n_0_[6] ,\read_address_reg_n_0_[5] ,\read_address_reg_n_0_[4] ,\read_address_reg_n_0_[3] ,\read_address_reg_n_0_[2] ,\read_address_reg_n_0_[1] ,\read_address_reg_n_0_[0] }),
        .WEBWE(register_interface_n_87),
        .aresetn(aresetn_0),
        .b_ram_out(b_ram_out[31:0]),
        .clk(clk),
        .data_in_w(data_in_w),
        .initialized_reg(register_interface_n_26));
  design_1_top_0_0_block_ram_7 \b_ram[1].DUT 
       (.Q({\read_address_reg_n_0_[6] ,\read_address_reg_n_0_[5] ,\read_address_reg_n_0_[4] ,\read_address_reg_n_0_[3] ,\read_address_reg_n_0_[2] ,\read_address_reg_n_0_[1] ,\read_address_reg_n_0_[0] }),
        .WEBWE(register_interface_n_86),
        .aresetn(aresetn_0),
        .b_ram_out(b_ram_out[63:32]),
        .clk(clk),
        .data_in_w(data_in_w),
        .initialized_reg(register_interface_n_26));
  design_1_top_0_0_block_ram_8 \b_ram[2].DUT 
       (.Q({\read_address_reg_n_0_[6] ,\read_address_reg_n_0_[5] ,\read_address_reg_n_0_[4] ,\read_address_reg_n_0_[3] ,\read_address_reg_n_0_[2] ,\read_address_reg_n_0_[1] ,\read_address_reg_n_0_[0] }),
        .WEBWE(register_interface_n_85),
        .aresetn(aresetn_0),
        .b_ram_out(b_ram_out[95:64]),
        .clk(clk),
        .data_in_w(data_in_w),
        .initialized_reg(register_interface_n_26));
  design_1_top_0_0_block_ram_9 \b_ram[3].DUT 
       (.Q({\read_address_reg_n_0_[6] ,\read_address_reg_n_0_[5] ,\read_address_reg_n_0_[4] ,\read_address_reg_n_0_[3] ,\read_address_reg_n_0_[2] ,\read_address_reg_n_0_[1] ,\read_address_reg_n_0_[0] }),
        .WEBWE(register_interface_n_84),
        .aresetn(aresetn_0),
        .b_ram_out(b_ram_out[127:96]),
        .clk(clk),
        .data_in_w(data_in_w),
        .initialized_reg(register_interface_n_26));
  design_1_top_0_0_block_ram_10 \b_ram[4].DUT 
       (.Q({\read_address_reg_n_0_[6] ,\read_address_reg_n_0_[5] ,\read_address_reg_n_0_[4] ,\read_address_reg_n_0_[3] ,\read_address_reg_n_0_[2] ,\read_address_reg_n_0_[1] ,\read_address_reg_n_0_[0] }),
        .WEBWE(register_interface_n_83),
        .aresetn(aresetn_0),
        .b_ram_out(b_ram_out[159:128]),
        .clk(clk),
        .data_in_w(data_in_w),
        .initialized_reg(register_interface_n_26));
  design_1_top_0_0_block_ram_11 \b_ram[5].DUT 
       (.Q({\read_address_reg_n_0_[6] ,\read_address_reg_n_0_[5] ,\read_address_reg_n_0_[4] ,\read_address_reg_n_0_[3] ,\read_address_reg_n_0_[2] ,\read_address_reg_n_0_[1] ,\read_address_reg_n_0_[0] }),
        .WEBWE(register_interface_n_82),
        .aresetn(aresetn_0),
        .b_ram_out(b_ram_out[191:160]),
        .clk(clk),
        .data_in_w(data_in_w),
        .initialized_reg(register_interface_n_26));
  design_1_top_0_0_block_ram_12 \b_ram[6].DUT 
       (.Q({\read_address_reg_n_0_[6] ,\read_address_reg_n_0_[5] ,\read_address_reg_n_0_[4] ,\read_address_reg_n_0_[3] ,\read_address_reg_n_0_[2] ,\read_address_reg_n_0_[1] ,\read_address_reg_n_0_[0] }),
        .WEBWE(register_interface_n_81),
        .aresetn(aresetn_0),
        .b_ram_out(b_ram_out[223:192]),
        .clk(clk),
        .data_in_w(data_in_w),
        .initialized_reg(register_interface_n_26));
  design_1_top_0_0_block_ram_13 \b_ram[7].DUT 
       (.E(p_1_in),
        .Q({\read_address_reg_n_0_[6] ,\read_address_reg_n_0_[5] ,\read_address_reg_n_0_[4] ,\read_address_reg_n_0_[3] ,\read_address_reg_n_0_[2] ,\read_address_reg_n_0_[1] ,\read_address_reg_n_0_[0] }),
        .aresetn(aresetn_0),
        .b_ram_out(b_ram_out[255:224]),
        .clk(clk),
        .data_in_w(data_in_w),
        .initialized_reg(register_interface_n_26));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \b_ram_sel[0]_i_1 
       (.I0(\state_reg_n_0_[0] ),
        .O(p_2_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \b_ram_sel[1]_i_1 
       (.I0(\state_reg_n_0_[0] ),
        .I1(b_ram_sel[0]),
        .O(p_2_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \b_ram_sel[2]_i_1 
       (.I0(\state_reg_n_0_[0] ),
        .I1(b_ram_sel[1]),
        .O(p_2_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \b_ram_sel[3]_i_1 
       (.I0(\state_reg_n_0_[0] ),
        .I1(b_ram_sel[2]),
        .O(p_2_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \b_ram_sel[4]_i_1 
       (.I0(\state_reg_n_0_[0] ),
        .I1(b_ram_sel[3]),
        .O(p_2_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \b_ram_sel[5]_i_1 
       (.I0(\state_reg_n_0_[0] ),
        .I1(b_ram_sel[4]),
        .O(p_2_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \b_ram_sel[6]_i_1 
       (.I0(\state_reg_n_0_[0] ),
        .I1(b_ram_sel[5]),
        .O(p_2_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \b_ram_sel[7]_i_2 
       (.I0(\state_reg_n_0_[0] ),
        .I1(b_ram_sel[6]),
        .O(p_2_in[7]));
  FDCE \b_ram_sel_reg[0] 
       (.C(clk),
        .CE(p_1_in__0),
        .CLR(aresetn_0),
        .D(p_2_in[0]),
        .Q(b_ram_sel[0]));
  FDCE \b_ram_sel_reg[1] 
       (.C(clk),
        .CE(p_1_in__0),
        .CLR(aresetn_0),
        .D(p_2_in[1]),
        .Q(b_ram_sel[1]));
  FDCE \b_ram_sel_reg[2] 
       (.C(clk),
        .CE(p_1_in__0),
        .CLR(aresetn_0),
        .D(p_2_in[2]),
        .Q(b_ram_sel[2]));
  FDCE \b_ram_sel_reg[3] 
       (.C(clk),
        .CE(p_1_in__0),
        .CLR(aresetn_0),
        .D(p_2_in[3]),
        .Q(b_ram_sel[3]));
  FDCE \b_ram_sel_reg[4] 
       (.C(clk),
        .CE(p_1_in__0),
        .CLR(aresetn_0),
        .D(p_2_in[4]),
        .Q(b_ram_sel[4]));
  FDCE \b_ram_sel_reg[5] 
       (.C(clk),
        .CE(p_1_in__0),
        .CLR(aresetn_0),
        .D(p_2_in[5]),
        .Q(b_ram_sel[5]));
  FDCE \b_ram_sel_reg[6] 
       (.C(clk),
        .CE(p_1_in__0),
        .CLR(aresetn_0),
        .D(p_2_in[6]),
        .Q(b_ram_sel[6]));
  FDCE \b_ram_sel_reg[7] 
       (.C(clk),
        .CE(p_1_in__0),
        .CLR(aresetn_0),
        .D(p_2_in[7]),
        .Q(b_ram_sel[7]));
  CARRY4 counter1_carry
       (.CI(1'b0),
        .CO({counter1_carry_n_0,counter1_carry_n_1,counter1_carry_n_2,counter1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({register_interface_n_21,register_interface_n_22,register_interface_n_23,register_interface_n_24}),
        .O(NLW_counter1_carry_O_UNCONNECTED[3:0]),
        .S({register_interface_n_5,register_interface_n_6,register_interface_n_7,register_interface_n_8}));
  CARRY4 counter1_carry__0
       (.CI(counter1_carry_n_0),
        .CO({NLW_counter1_carry__0_CO_UNCONNECTED[3:2],counter1,counter1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_counter1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,register_interface_n_44,register_interface_n_45}));
  LUT2 #(
    .INIT(4'h6)) 
    \counter[1]_i_1__0 
       (.I0(counter[0]),
        .I1(counter[1]),
        .O(\counter[1]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \counter[2]_i_1__0 
       (.I0(counter[2]),
        .I1(counter[0]),
        .I2(counter[1]),
        .O(\counter[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \counter[3]_i_1 
       (.I0(counter[3]),
        .I1(counter[1]),
        .I2(counter[0]),
        .I3(counter[2]),
        .O(\counter[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \counter[4]_i_1__5 
       (.I0(counter[4]),
        .I1(counter[3]),
        .I2(counter[1]),
        .I3(counter[0]),
        .I4(counter[2]),
        .O(\counter[4]_i_1__5_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \counter[5]_i_1 
       (.I0(counter[5]),
        .I1(counter[2]),
        .I2(counter[0]),
        .I3(counter[1]),
        .I4(counter[3]),
        .I5(counter[4]),
        .O(\counter[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hDF20)) 
    \counter[6]_i_3 
       (.I0(counter[5]),
        .I1(\counter[6]_i_4_n_0 ),
        .I2(counter[4]),
        .I3(counter[6]),
        .O(\counter[6]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \counter[6]_i_4 
       (.I0(counter[2]),
        .I1(counter[0]),
        .I2(counter[1]),
        .I3(counter[3]),
        .O(\counter[6]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(register_interface_n_126),
        .Q(counter[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[1] 
       (.C(clk),
        .CE(register_interface_n_88),
        .D(\counter[1]_i_1__0_n_0 ),
        .Q(counter[1]),
        .R(register_interface_n_125));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[2] 
       (.C(clk),
        .CE(register_interface_n_88),
        .D(\counter[2]_i_1__0_n_0 ),
        .Q(counter[2]),
        .R(register_interface_n_125));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[3] 
       (.C(clk),
        .CE(register_interface_n_88),
        .D(\counter[3]_i_1_n_0 ),
        .Q(counter[3]),
        .R(register_interface_n_125));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[4] 
       (.C(clk),
        .CE(register_interface_n_88),
        .D(\counter[4]_i_1__5_n_0 ),
        .Q(counter[4]),
        .R(register_interface_n_125));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[5] 
       (.C(clk),
        .CE(register_interface_n_88),
        .D(\counter[5]_i_1_n_0 ),
        .Q(counter[5]),
        .R(register_interface_n_125));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[6] 
       (.C(clk),
        .CE(register_interface_n_88),
        .D(\counter[6]_i_3_n_0 ),
        .Q(counter[6]),
        .R(register_interface_n_125));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \i___1/slv_regs[0][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(p_1_in_0[12]));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \i___1/slv_regs[0][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(p_1_in_0[23]));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \i___1/slv_regs[0][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(p_1_in_0[31]));
  LUT4 #(
    .INIT(16'h8000)) 
    \i___1/slv_regs[0][31]_i_2 
       (.I0(s_axi_ctrl_status_wready),
        .I1(s_axi_ctrl_status_wvalid),
        .I2(s_axi_ctrl_status_awready),
        .I3(s_axi_ctrl_status_awvalid),
        .O(slv_reg_wren__2));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \i___1/slv_regs[0][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(p_1_in_0[7]));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[10][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\i___1/slv_regs[10][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[10][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\i___1/slv_regs[10][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[10][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\i___1/slv_regs[10][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[10][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\i___1/slv_regs[10][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \i___1/slv_regs[11][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\i___1/slv_regs[11][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \i___1/slv_regs[11][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\i___1/slv_regs[11][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \i___1/slv_regs[11][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\i___1/slv_regs[11][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \i___1/slv_regs[11][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\i___1/slv_regs[11][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[12][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\i___1/slv_regs[12][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[12][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\i___1/slv_regs[12][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[12][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\i___1/slv_regs[12][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[12][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\i___1/slv_regs[12][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \i___1/slv_regs[13][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\i___1/slv_regs[13][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \i___1/slv_regs[13][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\i___1/slv_regs[13][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \i___1/slv_regs[13][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\i___1/slv_regs[13][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \i___1/slv_regs[13][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\i___1/slv_regs[13][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \i___1/slv_regs[14][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\i___1/slv_regs[14][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \i___1/slv_regs[14][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\i___1/slv_regs[14][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \i___1/slv_regs[14][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\i___1/slv_regs[14][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \i___1/slv_regs[14][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\i___1/slv_regs[14][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \i___1/slv_regs[15][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\i___1/slv_regs[15][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \i___1/slv_regs[15][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\i___1/slv_regs[15][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \i___1/slv_regs[15][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\i___1/slv_regs[15][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \i___1/slv_regs[15][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\i___1/slv_regs[15][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \i___1/slv_regs[1][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\i___1/slv_regs[1][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \i___1/slv_regs[1][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\i___1/slv_regs[1][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \i___1/slv_regs[1][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\i___1/slv_regs[1][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \i___1/slv_regs[1][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\i___1/slv_regs[1][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \i___1/slv_regs[2][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\i___1/slv_regs[2][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \i___1/slv_regs[2][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\i___1/slv_regs[2][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \i___1/slv_regs[2][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\i___1/slv_regs[2][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \i___1/slv_regs[2][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\i___1/slv_regs[2][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[3][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(p_0_in[3]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\i___1/slv_regs[3][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[3][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(p_0_in[3]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\i___1/slv_regs[3][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[3][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(p_0_in[3]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\i___1/slv_regs[3][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[3][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(p_0_in[3]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\i___1/slv_regs[3][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \i___1/slv_regs[4][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\i___1/slv_regs[4][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \i___1/slv_regs[4][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\i___1/slv_regs[4][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \i___1/slv_regs[4][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\i___1/slv_regs[4][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \i___1/slv_regs[4][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\i___1/slv_regs[4][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[5][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\i___1/slv_regs[5][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[5][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\i___1/slv_regs[5][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[5][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\i___1/slv_regs[5][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[5][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\i___1/slv_regs[5][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[6][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\i___1/slv_regs[6][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[6][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\i___1/slv_regs[6][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[6][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\i___1/slv_regs[6][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[6][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(p_0_in[0]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\i___1/slv_regs[6][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \i___1/slv_regs[7][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\i___1/slv_regs[7][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \i___1/slv_regs[7][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\i___1/slv_regs[7][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \i___1/slv_regs[7][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\i___1/slv_regs[7][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \i___1/slv_regs[7][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[3]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\i___1/slv_regs[7][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \i___1/slv_regs[8][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\i___1/slv_regs[8][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \i___1/slv_regs[8][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\i___1/slv_regs[8][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \i___1/slv_regs[8][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\i___1/slv_regs[8][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \i___1/slv_regs[8][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\i___1/slv_regs[8][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[9][15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[1]),
        .O(\i___1/slv_regs[9][15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[9][23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[2]),
        .O(\i___1/slv_regs[9][23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[9][31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[3]),
        .O(\i___1/slv_regs[9][31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \i___1/slv_regs[9][7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s_axi_ctrl_status_wstrb[0]),
        .O(\i___1/slv_regs[9][7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00040000)) 
    \i___1/valid_input_i_1 
       (.I0(p_0_in[1]),
        .I1(p_0_in[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[3]),
        .I4(slv_reg_wren__2),
        .O(valid_input1_out));
  LUT2 #(
    .INIT(4'h1)) 
    i__carry__0_i_2
       (.I0(state1[11]),
        .I1(state1[10]),
        .O(i__carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    i__carry__0_i_3
       (.I0(state1[9]),
        .I1(state1[8]),
        .O(i__carry__0_i_3_n_0));
  LUT3 #(
    .INIT(8'h04)) 
    i__carry_i_1__0
       (.I0(state1[7]),
        .I1(\read_address_reg_n_0_[6] ),
        .I2(state1[6]),
        .O(i__carry_i_1__0_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_2
       (.I0(\read_address_reg_n_0_[5] ),
        .I1(state1[5]),
        .I2(\read_address_reg_n_0_[4] ),
        .I3(state1[4]),
        .O(i__carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_3
       (.I0(\read_address_reg_n_0_[3] ),
        .I1(state1[3]),
        .I2(\read_address_reg_n_0_[2] ),
        .I3(state1[2]),
        .O(i__carry_i_3_n_0));
  LUT3 #(
    .INIT(8'h41)) 
    i__carry_i_5
       (.I0(state1[7]),
        .I1(state1[6]),
        .I2(\read_address_reg_n_0_[6] ),
        .O(i__carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6
       (.I0(state1[5]),
        .I1(\read_address_reg_n_0_[5] ),
        .I2(state1[4]),
        .I3(\read_address_reg_n_0_[4] ),
        .O(i__carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7
       (.I0(state1[3]),
        .I1(\read_address_reg_n_0_[3] ),
        .I2(state1[2]),
        .I3(\read_address_reg_n_0_[2] ),
        .O(i__carry_i_7_n_0));
  FDCE initialized_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(aresetn_0),
        .D(register_interface_n_124),
        .Q(initialized));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'h15)) 
    \read_address[0]_i_1 
       (.I0(\read_address_reg_n_0_[0] ),
        .I1(\state_reg_n_0_[1] ),
        .I2(state0),
        .O(p_0_in_0[0]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'h0666)) 
    \read_address[1]_i_1 
       (.I0(\read_address_reg_n_0_[0] ),
        .I1(\read_address_reg_n_0_[1] ),
        .I2(state0),
        .I3(\state_reg_n_0_[1] ),
        .O(p_0_in_0[1]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT5 #(
    .INIT(32'h00787878)) 
    \read_address[2]_i_1 
       (.I0(\read_address_reg_n_0_[1] ),
        .I1(\read_address_reg_n_0_[0] ),
        .I2(\read_address_reg_n_0_[2] ),
        .I3(state0),
        .I4(\state_reg_n_0_[1] ),
        .O(p_0_in_0[2]));
  LUT6 #(
    .INIT(64'h00007F807F807F80)) 
    \read_address[3]_i_1 
       (.I0(\read_address_reg_n_0_[2] ),
        .I1(\read_address_reg_n_0_[0] ),
        .I2(\read_address_reg_n_0_[1] ),
        .I3(\read_address_reg_n_0_[3] ),
        .I4(state0),
        .I5(\state_reg_n_0_[1] ),
        .O(p_0_in_0[3]));
  LUT6 #(
    .INIT(64'h000000007FFF8000)) 
    \read_address[4]_i_1 
       (.I0(\read_address_reg_n_0_[3] ),
        .I1(\read_address_reg_n_0_[1] ),
        .I2(\read_address_reg_n_0_[0] ),
        .I3(\read_address_reg_n_0_[2] ),
        .I4(\read_address_reg_n_0_[4] ),
        .I5(\read_address[4]_i_2_n_0 ),
        .O(p_0_in_0[4]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \read_address[4]_i_2 
       (.I0(state0),
        .I1(\state_reg_n_0_[1] ),
        .O(\read_address[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT4 #(
    .INIT(16'h0999)) 
    \read_address[5]_i_1 
       (.I0(\read_address[5]_i_2_n_0 ),
        .I1(\read_address_reg_n_0_[5] ),
        .I2(state0),
        .I3(\state_reg_n_0_[1] ),
        .O(p_0_in_0[5]));
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \read_address[5]_i_2 
       (.I0(\read_address_reg_n_0_[3] ),
        .I1(\read_address_reg_n_0_[1] ),
        .I2(\read_address_reg_n_0_[0] ),
        .I3(\read_address_reg_n_0_[2] ),
        .I4(\read_address_reg_n_0_[4] ),
        .O(\read_address[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT4 #(
    .INIT(16'h7007)) 
    \read_address[6]_i_2 
       (.I0(state0),
        .I1(\state_reg_n_0_[1] ),
        .I2(\read_address[6]_i_4_n_0 ),
        .I3(\read_address_reg_n_0_[6] ),
        .O(p_0_in_0[6]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \read_address[6]_i_4 
       (.I0(\read_address_reg_n_0_[4] ),
        .I1(\read_address_reg_n_0_[2] ),
        .I2(\read_address_reg_n_0_[0] ),
        .I3(\read_address_reg_n_0_[1] ),
        .I4(\read_address_reg_n_0_[3] ),
        .I5(\read_address_reg_n_0_[5] ),
        .O(\read_address[6]_i_4_n_0 ));
  FDRE \read_address_reg[0] 
       (.C(clk),
        .CE(register_interface_n_52),
        .D(p_0_in_0[0]),
        .Q(\read_address_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \read_address_reg[1] 
       (.C(clk),
        .CE(register_interface_n_52),
        .D(p_0_in_0[1]),
        .Q(\read_address_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \read_address_reg[2] 
       (.C(clk),
        .CE(register_interface_n_52),
        .D(p_0_in_0[2]),
        .Q(\read_address_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \read_address_reg[3] 
       (.C(clk),
        .CE(register_interface_n_52),
        .D(p_0_in_0[3]),
        .Q(\read_address_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \read_address_reg[4] 
       (.C(clk),
        .CE(register_interface_n_52),
        .D(p_0_in_0[4]),
        .Q(\read_address_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \read_address_reg[5] 
       (.C(clk),
        .CE(register_interface_n_52),
        .D(p_0_in_0[5]),
        .Q(\read_address_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \read_address_reg[6] 
       (.C(clk),
        .CE(register_interface_n_52),
        .D(p_0_in_0[6]),
        .Q(\read_address_reg_n_0_[6] ),
        .R(1'b0));
  design_1_top_0_0_register_interface register_interface
       (.CO(CO),
        .DI({register_interface_n_21,register_interface_n_22,register_interface_n_23,register_interface_n_24}),
        .E(E),
        .O(state1[1]),
        .Q({v_len,Q}),
        .S({register_interface_n_5,register_interface_n_6,register_interface_n_7,register_interface_n_8}),
        .SR(SR),
        .WEBWE(register_interface_n_81),
        .aresetn(aresetn),
        .aresetn_0(aresetn_0),
        .\axi_awaddr_reg[2]_0 ({\i___1/slv_regs[1][31]_i_1_n_0 ,\i___1/slv_regs[1][23]_i_1_n_0 ,\i___1/slv_regs[1][15]_i_1_n_0 ,\i___1/slv_regs[1][7]_i_1_n_0 }),
        .\axi_awaddr_reg[2]_1 ({\i___1/slv_regs[3][31]_i_1_n_0 ,\i___1/slv_regs[3][23]_i_1_n_0 ,\i___1/slv_regs[3][15]_i_1_n_0 ,\i___1/slv_regs[3][7]_i_1_n_0 }),
        .\axi_awaddr_reg[2]_2 ({\i___1/slv_regs[5][31]_i_1_n_0 ,\i___1/slv_regs[5][23]_i_1_n_0 ,\i___1/slv_regs[5][15]_i_1_n_0 ,\i___1/slv_regs[5][7]_i_1_n_0 }),
        .\axi_awaddr_reg[2]_3 ({\i___1/slv_regs[9][31]_i_1_n_0 ,\i___1/slv_regs[9][23]_i_1_n_0 ,\i___1/slv_regs[9][15]_i_1_n_0 ,\i___1/slv_regs[9][7]_i_1_n_0 }),
        .\axi_awaddr_reg[3]_0 ({\i___1/slv_regs[2][31]_i_1_n_0 ,\i___1/slv_regs[2][23]_i_1_n_0 ,\i___1/slv_regs[2][15]_i_1_n_0 ,\i___1/slv_regs[2][7]_i_1_n_0 }),
        .\axi_awaddr_reg[3]_1 ({\i___1/slv_regs[6][31]_i_1_n_0 ,\i___1/slv_regs[6][23]_i_1_n_0 ,\i___1/slv_regs[6][15]_i_1_n_0 ,\i___1/slv_regs[6][7]_i_1_n_0 }),
        .\axi_awaddr_reg[3]_2 ({\i___1/slv_regs[10][31]_i_1_n_0 ,\i___1/slv_regs[10][23]_i_1_n_0 ,\i___1/slv_regs[10][15]_i_1_n_0 ,\i___1/slv_regs[10][7]_i_1_n_0 }),
        .\axi_awaddr_reg[4]_0 ({\i___1/slv_regs[4][31]_i_1_n_0 ,\i___1/slv_regs[4][23]_i_1_n_0 ,\i___1/slv_regs[4][15]_i_1_n_0 ,\i___1/slv_regs[4][7]_i_1_n_0 }),
        .\axi_awaddr_reg[4]_1 ({\i___1/slv_regs[7][31]_i_1_n_0 ,\i___1/slv_regs[7][23]_i_1_n_0 ,\i___1/slv_regs[7][15]_i_1_n_0 ,\i___1/slv_regs[7][7]_i_1_n_0 }),
        .\axi_awaddr_reg[4]_2 ({\i___1/slv_regs[13][31]_i_1_n_0 ,\i___1/slv_regs[13][23]_i_1_n_0 ,\i___1/slv_regs[13][15]_i_1_n_0 ,\i___1/slv_regs[13][7]_i_1_n_0 }),
        .\axi_awaddr_reg[4]_3 ({\i___1/slv_regs[14][31]_i_1_n_0 ,\i___1/slv_regs[14][23]_i_1_n_0 ,\i___1/slv_regs[14][15]_i_1_n_0 ,\i___1/slv_regs[14][7]_i_1_n_0 }),
        .\axi_awaddr_reg[5]_0 ({p_1_in_0[31],p_1_in_0[23],p_1_in_0[12],p_1_in_0[7]}),
        .\axi_awaddr_reg[5]_1 ({\i___1/slv_regs[8][31]_i_1_n_0 ,\i___1/slv_regs[8][23]_i_1_n_0 ,\i___1/slv_regs[8][15]_i_1_n_0 ,\i___1/slv_regs[8][7]_i_1_n_0 }),
        .\axi_awaddr_reg[5]_2 ({\i___1/slv_regs[11][31]_i_1_n_0 ,\i___1/slv_regs[11][23]_i_1_n_0 ,\i___1/slv_regs[11][15]_i_1_n_0 ,\i___1/slv_regs[11][7]_i_1_n_0 }),
        .\axi_awaddr_reg[5]_3 ({\i___1/slv_regs[12][31]_i_1_n_0 ,\i___1/slv_regs[12][23]_i_1_n_0 ,\i___1/slv_regs[12][15]_i_1_n_0 ,\i___1/slv_regs[12][7]_i_1_n_0 }),
        .\axi_awaddr_reg[5]_4 ({\i___1/slv_regs[15][31]_i_1_n_0 ,\i___1/slv_regs[15][23]_i_1_n_0 ,\i___1/slv_regs[15][15]_i_1_n_0 ,\i___1/slv_regs[15][7]_i_1_n_0 }),
        .b_ram_data_reg(register_interface_n_26),
        .b_ram_data_reg_0(p_1_in),
        .\b_ram_sel_reg[0] (p_1_in__0),
        .\b_ram_sel_reg[7] (b_ram_sel),
        .clk(clk),
        .\count_i_reg[6] (register_interface_n_82),
        .\count_i_reg[6]_0 (register_interface_n_83),
        .\count_i_reg[6]_1 (register_interface_n_84),
        .\count_i_reg[6]_2 (register_interface_n_85),
        .\count_i_reg[6]_3 (register_interface_n_86),
        .\count_i_reg[6]_4 (register_interface_n_87),
        .counter(counter),
        .\counter_reg[0] (register_interface_n_126),
        .\counter_reg[1] ({register_interface_n_44,register_interface_n_45}),
        .\counter_reg[1]_0 (\counter_reg[1]_0 ),
        .\counter_reg[1]_1 (S),
        .\counter_reg[1]_10 (register_interface_n_88),
        .\counter_reg[1]_11 (\counter_reg[1]_1 [1:0]),
        .\counter_reg[1]_12 (O),
        .\counter_reg[1]_13 (register_interface_n_125),
        .\counter_reg[1]_2 (\counter_reg[1]_1 [4:2]),
        .\counter_reg[1]_3 (\counter_reg[1]_2 ),
        .\counter_reg[1]_4 (\counter_reg[1]_3 ),
        .\counter_reg[1]_5 (\counter_reg[1]_4 ),
        .\counter_reg[1]_6 (\counter_reg[1]_5 ),
        .\counter_reg[1]_7 (\counter_reg[1]_6 ),
        .\counter_reg[1]_8 (\counter_reg[1]_7 ),
        .\counter_reg[1]_9 (\counter_reg[1]_8 ),
        .\counter_reg[2] (\counter_reg[2]_0 ),
        .\counter_reg[2]_0 (\counter_reg[2]_1 ),
        .\counter_reg[2]_1 (\counter_reg[2]_2 ),
        .\counter_reg[2]_2 (\counter_reg[2]_3 ),
        .\counter_reg[2]_3 (\counter_reg[2]_4 ),
        .\counter_reg[2]_4 (\counter_reg[2]_5 ),
        .\counter_reg[2]_5 (\counter_reg[2]_6 ),
        .\counter_reg[2]_6 (\counter[6]_i_4_n_0 ),
        .\counter_reg[5] (\counter_reg[5]_0 ),
        .\counter_reg[5]_0 (\counter_reg[5]_1 ),
        .\counter_reg[5]_1 (\counter_reg[5]_2 ),
        .\counter_reg[5]_2 (\counter_reg[5]_3 ),
        .\counter_reg[8] (\counter_reg[8] ),
        .\counter_reg[8]_0 (\counter_reg[8]_0 ),
        .\counter_reg[8]_1 (\counter_reg[8]_1 ),
        .\counter_reg[8]_2 (\counter_reg[8]_2 ),
        .data_in_w(data_in_w),
        .dp_enable(dp_enable),
        .dp_extend_end(dp_extend_end),
        .in_stream_handshake_delay(in_stream_handshake_delay),
        .in_stream_ready_reg(in_stream_ready_reg),
        .initialized(initialized),
        .initialized_reg(register_interface_n_124),
        .p_rdy(p_rdy),
        .\read_address_reg[0] ({register_interface_n_36,register_interface_n_37,register_interface_n_38,register_interface_n_39}),
        .\read_address_reg[0]_0 ({register_interface_n_40,register_interface_n_41,register_interface_n_42,register_interface_n_43}),
        .\read_address_reg[0]_1 ({register_interface_n_46,register_interface_n_47,register_interface_n_48}),
        .\read_address_reg[0]_2 (state1[12]),
        .\read_address_reg[0]_3 (register_interface_n_50),
        .\read_address_reg[0]_4 (register_interface_n_51),
        .\read_address_reg[0]_5 (register_interface_n_52),
        .\read_address_reg[1] ({\read_address_reg_n_0_[1] ,\read_address_reg_n_0_[0] }),
        .s_axi_ctrl_status_araddr(s_axi_ctrl_status_araddr),
        .s_axi_ctrl_status_arready(s_axi_ctrl_status_arready),
        .s_axi_ctrl_status_arvalid(s_axi_ctrl_status_arvalid),
        .s_axi_ctrl_status_awaddr(s_axi_ctrl_status_awaddr),
        .s_axi_ctrl_status_awready(s_axi_ctrl_status_awready),
        .s_axi_ctrl_status_awvalid(s_axi_ctrl_status_awvalid),
        .s_axi_ctrl_status_bready(s_axi_ctrl_status_bready),
        .s_axi_ctrl_status_bvalid(s_axi_ctrl_status_bvalid),
        .s_axi_ctrl_status_rdata(s_axi_ctrl_status_rdata),
        .s_axi_ctrl_status_rready(s_axi_ctrl_status_rready),
        .s_axi_ctrl_status_rvalid(s_axi_ctrl_status_rvalid),
        .s_axi_ctrl_status_wdata(s_axi_ctrl_status_wdata),
        .s_axi_ctrl_status_wready(s_axi_ctrl_status_wready),
        .s_axi_ctrl_status_wvalid(s_axi_ctrl_status_wvalid),
        .s_axis_tready(s_axis_tready),
        .s_axis_tvalid(s_axis_tvalid),
        .\slv_regs_reg[0][11]_0 (\slv_regs_reg[0][11] ),
        .\slv_regs_reg[0][11]_1 (\slv_regs_reg[0][11]_0 ),
        .\slv_regs_reg[0][11]_10 (counter1),
        .\slv_regs_reg[0][11]_2 (\slv_regs_reg[0][11]_1 ),
        .\slv_regs_reg[0][11]_3 (\slv_regs_reg[0][11]_2 ),
        .\slv_regs_reg[0][11]_4 (\slv_regs_reg[0][11]_3 ),
        .\slv_regs_reg[0][11]_5 (\slv_regs_reg[0][11]_4 ),
        .\slv_regs_reg[0][11]_6 (\slv_regs_reg[0][11]_5 ),
        .\slv_regs_reg[0][11]_7 (\slv_regs_reg[0][11]_6 ),
        .\slv_regs_reg[0][11]_8 (state1_carry__1_n_0),
        .\slv_regs_reg[0][11]_9 (state0),
        .\slv_regs_reg[0][31]_0 (p_0_in),
        .\state_reg[0] (register_interface_n_128),
        .\state_reg[0]_0 (\state_reg_n_0_[0] ),
        .\state_reg[1] (register_interface_n_127),
        .\state_reg[1]_0 (\state_reg_n_0_[1] ),
        .valid_input1_out(valid_input1_out));
  CARRY4 \state0_inferred__3/i__carry 
       (.CI(1'b0),
        .CO({\state0_inferred__3/i__carry_n_0 ,\state0_inferred__3/i__carry_n_1 ,\state0_inferred__3/i__carry_n_2 ,\state0_inferred__3/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({i__carry_i_1__0_n_0,i__carry_i_2_n_0,i__carry_i_3_n_0,register_interface_n_51}),
        .O(\NLW_state0_inferred__3/i__carry_O_UNCONNECTED [3:0]),
        .S({i__carry_i_5_n_0,i__carry_i_6_n_0,i__carry_i_7_n_0,register_interface_n_50}));
  CARRY4 \state0_inferred__3/i__carry__0 
       (.CI(\state0_inferred__3/i__carry_n_0 ),
        .CO({\NLW_state0_inferred__3/i__carry__0_CO_UNCONNECTED [3],state0,\state0_inferred__3/i__carry__0_n_2 ,\state0_inferred__3/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,state1[12],1'b0,1'b0}),
        .O(\NLW_state0_inferred__3/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,state1_carry__1_n_0,i__carry__0_i_2_n_0,i__carry__0_i_3_n_0}));
  CARRY4 state1_carry
       (.CI(1'b0),
        .CO({state1_carry_n_0,state1_carry_n_1,state1_carry_n_2,state1_carry_n_3}),
        .CYINIT(Q),
        .DI(v_len[4:1]),
        .O(state1[4:1]),
        .S({register_interface_n_36,register_interface_n_37,register_interface_n_38,register_interface_n_39}));
  CARRY4 state1_carry__0
       (.CI(state1_carry_n_0),
        .CO({state1_carry__0_n_0,state1_carry__0_n_1,state1_carry__0_n_2,state1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(v_len[8:5]),
        .O(state1[8:5]),
        .S({register_interface_n_40,register_interface_n_41,register_interface_n_42,register_interface_n_43}));
  CARRY4 state1_carry__1
       (.CI(state1_carry__0_n_0),
        .CO({state1_carry__1_n_0,NLW_state1_carry__1_CO_UNCONNECTED[2],state1_carry__1_n_2,state1_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,v_len[11:9]}),
        .O({NLW_state1_carry__1_O_UNCONNECTED[3],state1[11:9]}),
        .S({1'b1,register_interface_n_46,register_interface_n_47,register_interface_n_48}));
  FDCE \state_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .CLR(aresetn_0),
        .D(register_interface_n_128),
        .Q(\state_reg_n_0_[0] ));
  FDCE \state_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .CLR(aresetn_0),
        .D(register_interface_n_127),
        .Q(\state_reg_n_0_[1] ));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_top_0_0_block_ram
   (b_ram_out,
    clk,
    initialized_reg,
    aresetn,
    Q,
    data_in_w,
    WEBWE);
  output [31:0]b_ram_out;
  input clk;
  input initialized_reg;
  input aresetn;
  input [6:0]Q;
  input [31:0]data_in_w;
  input [0:0]WEBWE;

  wire [6:0]Q;
  wire [0:0]WEBWE;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \count_i[6]_i_2_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [31:0]data_in_w;
  wire initialized_reg;
  wire [6:0]p_0_in;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,Q,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(data_in_w[15:0]),
        .DIBDI(data_in_w[31:16]),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(b_ram_out[15:0]),
        .DOBDO(b_ram_out[31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(initialized_reg),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(aresetn),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({WEBWE,WEBWE,WEBWE,WEBWE}));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \count_i[2]_i_1 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \count_i[4]_i_1 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[3]),
        .O(p_0_in[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \count_i[5]_i_1 
       (.I0(count_i_reg__0[5]),
        .I1(count_i_reg__0[3]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[0]),
        .I4(count_i_reg__0[1]),
        .I5(count_i_reg__0[4]),
        .O(p_0_in[5]));
  LUT4 #(
    .INIT(16'h6AAA)) 
    \count_i[6]_i_1 
       (.I0(count_i_reg__0[6]),
        .I1(count_i_reg__0[4]),
        .I2(\count_i[6]_i_2_n_0 ),
        .I3(count_i_reg__0[5]),
        .O(p_0_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \count_i[6]_i_2 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .O(\count_i[6]_i_2_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in[0]),
        .Q(count_i_reg__0[0]),
        .R(aresetn));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in[1]),
        .Q(count_i_reg__0[1]),
        .R(aresetn));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in[2]),
        .Q(count_i_reg__0[2]),
        .R(aresetn));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in[3]),
        .Q(count_i_reg__0[3]),
        .R(aresetn));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in[4]),
        .Q(count_i_reg__0[4]),
        .R(aresetn));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in[5]),
        .Q(count_i_reg__0[5]),
        .R(aresetn));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in[6]),
        .Q(count_i_reg__0[6]),
        .R(aresetn));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_top_0_0_block_ram_10
   (b_ram_out,
    clk,
    initialized_reg,
    aresetn,
    Q,
    data_in_w,
    WEBWE);
  output [31:0]b_ram_out;
  input clk;
  input initialized_reg;
  input aresetn;
  input [6:0]Q;
  input [31:0]data_in_w;
  input [0:0]WEBWE;

  wire [6:0]Q;
  wire [0:0]WEBWE;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \count_i[6]_i_2__3_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [31:0]data_in_w;
  wire initialized_reg;
  wire [6:0]p_0_in__3;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,Q,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(data_in_w[15:0]),
        .DIBDI(data_in_w[31:16]),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(b_ram_out[15:0]),
        .DOBDO(b_ram_out[31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(initialized_reg),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(aresetn),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({WEBWE,WEBWE,WEBWE,WEBWE}));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1__3 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in__3[0]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1__3 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in__3[1]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \count_i[2]_i_1__3 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .O(p_0_in__3[2]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1__3 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in__3[3]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \count_i[4]_i_1__3 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[3]),
        .O(p_0_in__3[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \count_i[5]_i_1__3 
       (.I0(count_i_reg__0[5]),
        .I1(count_i_reg__0[3]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[0]),
        .I4(count_i_reg__0[1]),
        .I5(count_i_reg__0[4]),
        .O(p_0_in__3[5]));
  LUT4 #(
    .INIT(16'h6AAA)) 
    \count_i[6]_i_1__3 
       (.I0(count_i_reg__0[6]),
        .I1(count_i_reg__0[4]),
        .I2(\count_i[6]_i_2__3_n_0 ),
        .I3(count_i_reg__0[5]),
        .O(p_0_in__3[6]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \count_i[6]_i_2__3 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .O(\count_i[6]_i_2__3_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__3[0]),
        .Q(count_i_reg__0[0]),
        .R(aresetn));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__3[1]),
        .Q(count_i_reg__0[1]),
        .R(aresetn));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__3[2]),
        .Q(count_i_reg__0[2]),
        .R(aresetn));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__3[3]),
        .Q(count_i_reg__0[3]),
        .R(aresetn));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__3[4]),
        .Q(count_i_reg__0[4]),
        .R(aresetn));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__3[5]),
        .Q(count_i_reg__0[5]),
        .R(aresetn));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__3[6]),
        .Q(count_i_reg__0[6]),
        .R(aresetn));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_top_0_0_block_ram_11
   (b_ram_out,
    clk,
    initialized_reg,
    aresetn,
    Q,
    data_in_w,
    WEBWE);
  output [31:0]b_ram_out;
  input clk;
  input initialized_reg;
  input aresetn;
  input [6:0]Q;
  input [31:0]data_in_w;
  input [0:0]WEBWE;

  wire [6:0]Q;
  wire [0:0]WEBWE;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \count_i[6]_i_2__4_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [31:0]data_in_w;
  wire initialized_reg;
  wire [6:0]p_0_in__4;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,Q,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(data_in_w[15:0]),
        .DIBDI(data_in_w[31:16]),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(b_ram_out[15:0]),
        .DOBDO(b_ram_out[31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(initialized_reg),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(aresetn),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({WEBWE,WEBWE,WEBWE,WEBWE}));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1__4 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in__4[0]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1__4 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in__4[1]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \count_i[2]_i_1__4 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .O(p_0_in__4[2]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1__4 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in__4[3]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \count_i[4]_i_1__4 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[3]),
        .O(p_0_in__4[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \count_i[5]_i_1__4 
       (.I0(count_i_reg__0[5]),
        .I1(count_i_reg__0[3]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[0]),
        .I4(count_i_reg__0[1]),
        .I5(count_i_reg__0[4]),
        .O(p_0_in__4[5]));
  LUT4 #(
    .INIT(16'h6AAA)) 
    \count_i[6]_i_1__4 
       (.I0(count_i_reg__0[6]),
        .I1(count_i_reg__0[4]),
        .I2(\count_i[6]_i_2__4_n_0 ),
        .I3(count_i_reg__0[5]),
        .O(p_0_in__4[6]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \count_i[6]_i_2__4 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .O(\count_i[6]_i_2__4_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__4[0]),
        .Q(count_i_reg__0[0]),
        .R(aresetn));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__4[1]),
        .Q(count_i_reg__0[1]),
        .R(aresetn));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__4[2]),
        .Q(count_i_reg__0[2]),
        .R(aresetn));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__4[3]),
        .Q(count_i_reg__0[3]),
        .R(aresetn));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__4[4]),
        .Q(count_i_reg__0[4]),
        .R(aresetn));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__4[5]),
        .Q(count_i_reg__0[5]),
        .R(aresetn));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__4[6]),
        .Q(count_i_reg__0[6]),
        .R(aresetn));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_top_0_0_block_ram_12
   (b_ram_out,
    clk,
    initialized_reg,
    aresetn,
    Q,
    data_in_w,
    WEBWE);
  output [31:0]b_ram_out;
  input clk;
  input initialized_reg;
  input aresetn;
  input [6:0]Q;
  input [31:0]data_in_w;
  input [0:0]WEBWE;

  wire [6:0]Q;
  wire [0:0]WEBWE;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \count_i[6]_i_2__5_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [31:0]data_in_w;
  wire initialized_reg;
  wire [6:0]p_0_in__5;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,Q,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(data_in_w[15:0]),
        .DIBDI(data_in_w[31:16]),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(b_ram_out[15:0]),
        .DOBDO(b_ram_out[31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(initialized_reg),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(aresetn),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({WEBWE,WEBWE,WEBWE,WEBWE}));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1__5 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in__5[0]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1__5 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in__5[1]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \count_i[2]_i_1__5 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .O(p_0_in__5[2]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1__5 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in__5[3]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \count_i[4]_i_1__5 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[3]),
        .O(p_0_in__5[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \count_i[5]_i_1__5 
       (.I0(count_i_reg__0[5]),
        .I1(count_i_reg__0[3]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[0]),
        .I4(count_i_reg__0[1]),
        .I5(count_i_reg__0[4]),
        .O(p_0_in__5[5]));
  LUT4 #(
    .INIT(16'h6AAA)) 
    \count_i[6]_i_1__5 
       (.I0(count_i_reg__0[6]),
        .I1(count_i_reg__0[4]),
        .I2(\count_i[6]_i_2__5_n_0 ),
        .I3(count_i_reg__0[5]),
        .O(p_0_in__5[6]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \count_i[6]_i_2__5 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .O(\count_i[6]_i_2__5_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__5[0]),
        .Q(count_i_reg__0[0]),
        .R(aresetn));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__5[1]),
        .Q(count_i_reg__0[1]),
        .R(aresetn));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__5[2]),
        .Q(count_i_reg__0[2]),
        .R(aresetn));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__5[3]),
        .Q(count_i_reg__0[3]),
        .R(aresetn));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__5[4]),
        .Q(count_i_reg__0[4]),
        .R(aresetn));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__5[5]),
        .Q(count_i_reg__0[5]),
        .R(aresetn));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__5[6]),
        .Q(count_i_reg__0[6]),
        .R(aresetn));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_top_0_0_block_ram_13
   (b_ram_out,
    clk,
    initialized_reg,
    aresetn,
    Q,
    data_in_w,
    E);
  output [31:0]b_ram_out;
  input clk;
  input initialized_reg;
  input aresetn;
  input [6:0]Q;
  input [31:0]data_in_w;
  input [0:0]E;

  wire [0:0]E;
  wire [6:0]Q;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \count_i[6]_i_2__6_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [31:0]data_in_w;
  wire initialized_reg;
  wire [6:0]p_0_in__6;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,Q,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(data_in_w[15:0]),
        .DIBDI(data_in_w[31:16]),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(b_ram_out[15:0]),
        .DOBDO(b_ram_out[31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(initialized_reg),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(aresetn),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({E,E,E,E}));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1__6 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in__6[0]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1__6 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in__6[1]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \count_i[2]_i_1__6 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .O(p_0_in__6[2]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1__6 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in__6[3]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \count_i[4]_i_1__6 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[3]),
        .O(p_0_in__6[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \count_i[5]_i_1__6 
       (.I0(count_i_reg__0[5]),
        .I1(count_i_reg__0[3]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[0]),
        .I4(count_i_reg__0[1]),
        .I5(count_i_reg__0[4]),
        .O(p_0_in__6[5]));
  LUT4 #(
    .INIT(16'h6AAA)) 
    \count_i[6]_i_1__6 
       (.I0(count_i_reg__0[6]),
        .I1(count_i_reg__0[4]),
        .I2(\count_i[6]_i_2__6_n_0 ),
        .I3(count_i_reg__0[5]),
        .O(p_0_in__6[6]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \count_i[6]_i_2__6 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .O(\count_i[6]_i_2__6_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__6[0]),
        .Q(count_i_reg__0[0]),
        .R(aresetn));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__6[1]),
        .Q(count_i_reg__0[1]),
        .R(aresetn));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__6[2]),
        .Q(count_i_reg__0[2]),
        .R(aresetn));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__6[3]),
        .Q(count_i_reg__0[3]),
        .R(aresetn));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__6[4]),
        .Q(count_i_reg__0[4]),
        .R(aresetn));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__6[5]),
        .Q(count_i_reg__0[5]),
        .R(aresetn));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(E),
        .D(p_0_in__6[6]),
        .Q(count_i_reg__0[6]),
        .R(aresetn));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_top_0_0_block_ram_7
   (b_ram_out,
    clk,
    initialized_reg,
    aresetn,
    Q,
    data_in_w,
    WEBWE);
  output [31:0]b_ram_out;
  input clk;
  input initialized_reg;
  input aresetn;
  input [6:0]Q;
  input [31:0]data_in_w;
  input [0:0]WEBWE;

  wire [6:0]Q;
  wire [0:0]WEBWE;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \count_i[6]_i_2__0_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [31:0]data_in_w;
  wire initialized_reg;
  wire [6:0]p_0_in__0;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,Q,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(data_in_w[15:0]),
        .DIBDI(data_in_w[31:16]),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(b_ram_out[15:0]),
        .DOBDO(b_ram_out[31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(initialized_reg),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(aresetn),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({WEBWE,WEBWE,WEBWE,WEBWE}));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1__0 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1__0 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \count_i[2]_i_1__0 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1__0 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \count_i[4]_i_1__0 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[3]),
        .O(p_0_in__0[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \count_i[5]_i_1__0 
       (.I0(count_i_reg__0[5]),
        .I1(count_i_reg__0[3]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[0]),
        .I4(count_i_reg__0[1]),
        .I5(count_i_reg__0[4]),
        .O(p_0_in__0[5]));
  LUT4 #(
    .INIT(16'h6AAA)) 
    \count_i[6]_i_1__0 
       (.I0(count_i_reg__0[6]),
        .I1(count_i_reg__0[4]),
        .I2(\count_i[6]_i_2__0_n_0 ),
        .I3(count_i_reg__0[5]),
        .O(p_0_in__0[6]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \count_i[6]_i_2__0 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .O(\count_i[6]_i_2__0_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__0[0]),
        .Q(count_i_reg__0[0]),
        .R(aresetn));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__0[1]),
        .Q(count_i_reg__0[1]),
        .R(aresetn));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__0[2]),
        .Q(count_i_reg__0[2]),
        .R(aresetn));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__0[3]),
        .Q(count_i_reg__0[3]),
        .R(aresetn));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__0[4]),
        .Q(count_i_reg__0[4]),
        .R(aresetn));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__0[5]),
        .Q(count_i_reg__0[5]),
        .R(aresetn));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__0[6]),
        .Q(count_i_reg__0[6]),
        .R(aresetn));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_top_0_0_block_ram_8
   (b_ram_out,
    clk,
    initialized_reg,
    aresetn,
    Q,
    data_in_w,
    WEBWE);
  output [31:0]b_ram_out;
  input clk;
  input initialized_reg;
  input aresetn;
  input [6:0]Q;
  input [31:0]data_in_w;
  input [0:0]WEBWE;

  wire [6:0]Q;
  wire [0:0]WEBWE;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \count_i[6]_i_2__1_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [31:0]data_in_w;
  wire initialized_reg;
  wire [6:0]p_0_in__1;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,Q,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(data_in_w[15:0]),
        .DIBDI(data_in_w[31:16]),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(b_ram_out[15:0]),
        .DOBDO(b_ram_out[31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(initialized_reg),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(aresetn),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({WEBWE,WEBWE,WEBWE,WEBWE}));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1__1 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1__1 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \count_i[2]_i_1__1 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .O(p_0_in__1[2]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1__1 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in__1[3]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \count_i[4]_i_1__1 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[3]),
        .O(p_0_in__1[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \count_i[5]_i_1__1 
       (.I0(count_i_reg__0[5]),
        .I1(count_i_reg__0[3]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[0]),
        .I4(count_i_reg__0[1]),
        .I5(count_i_reg__0[4]),
        .O(p_0_in__1[5]));
  LUT4 #(
    .INIT(16'h6AAA)) 
    \count_i[6]_i_1__1 
       (.I0(count_i_reg__0[6]),
        .I1(count_i_reg__0[4]),
        .I2(\count_i[6]_i_2__1_n_0 ),
        .I3(count_i_reg__0[5]),
        .O(p_0_in__1[6]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \count_i[6]_i_2__1 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .O(\count_i[6]_i_2__1_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__1[0]),
        .Q(count_i_reg__0[0]),
        .R(aresetn));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__1[1]),
        .Q(count_i_reg__0[1]),
        .R(aresetn));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__1[2]),
        .Q(count_i_reg__0[2]),
        .R(aresetn));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__1[3]),
        .Q(count_i_reg__0[3]),
        .R(aresetn));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__1[4]),
        .Q(count_i_reg__0[4]),
        .R(aresetn));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__1[5]),
        .Q(count_i_reg__0[5]),
        .R(aresetn));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__1[6]),
        .Q(count_i_reg__0[6]),
        .R(aresetn));
endmodule

(* ORIG_REF_NAME = "block_ram" *) 
module design_1_top_0_0_block_ram_9
   (b_ram_out,
    clk,
    initialized_reg,
    aresetn,
    Q,
    data_in_w,
    WEBWE);
  output [31:0]b_ram_out;
  input clk;
  input initialized_reg;
  input aresetn;
  input [6:0]Q;
  input [31:0]data_in_w;
  input [0:0]WEBWE;

  wire [6:0]Q;
  wire [0:0]WEBWE;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \count_i[6]_i_2__2_n_0 ;
  wire [6:0]count_i_reg__0;
  wire [31:0]data_in_w;
  wire initialized_reg;
  wire [6:0]p_0_in__2;
  wire [1:0]NLW_b_ram_data_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d32" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "3200" *) 
  (* RTL_RAM_NAME = "b_ram_data" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "31" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    b_ram_data_reg
       (.ADDRARDADDR({1'b1,1'b1,Q,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,count_i_reg__0,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI(data_in_w[15:0]),
        .DIBDI(data_in_w[31:16]),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(b_ram_out[15:0]),
        .DOBDO(b_ram_out[31:16]),
        .DOPADOP(NLW_b_ram_data_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_b_ram_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(initialized_reg),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(aresetn),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({WEBWE,WEBWE,WEBWE,WEBWE}));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_i[0]_i_1__2 
       (.I0(count_i_reg__0[0]),
        .O(p_0_in__2[0]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count_i[1]_i_1__2 
       (.I0(count_i_reg__0[0]),
        .I1(count_i_reg__0[1]),
        .O(p_0_in__2[1]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \count_i[2]_i_1__2 
       (.I0(count_i_reg__0[2]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[1]),
        .O(p_0_in__2[2]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count_i[3]_i_1__2 
       (.I0(count_i_reg__0[1]),
        .I1(count_i_reg__0[0]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[3]),
        .O(p_0_in__2[3]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \count_i[4]_i_1__2 
       (.I0(count_i_reg__0[4]),
        .I1(count_i_reg__0[1]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[2]),
        .I4(count_i_reg__0[3]),
        .O(p_0_in__2[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \count_i[5]_i_1__2 
       (.I0(count_i_reg__0[5]),
        .I1(count_i_reg__0[3]),
        .I2(count_i_reg__0[2]),
        .I3(count_i_reg__0[0]),
        .I4(count_i_reg__0[1]),
        .I5(count_i_reg__0[4]),
        .O(p_0_in__2[5]));
  LUT4 #(
    .INIT(16'h6AAA)) 
    \count_i[6]_i_1__2 
       (.I0(count_i_reg__0[6]),
        .I1(count_i_reg__0[4]),
        .I2(\count_i[6]_i_2__2_n_0 ),
        .I3(count_i_reg__0[5]),
        .O(p_0_in__2[6]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \count_i[6]_i_2__2 
       (.I0(count_i_reg__0[3]),
        .I1(count_i_reg__0[2]),
        .I2(count_i_reg__0[0]),
        .I3(count_i_reg__0[1]),
        .O(\count_i[6]_i_2__2_n_0 ));
  FDRE \count_i_reg[0] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__2[0]),
        .Q(count_i_reg__0[0]),
        .R(aresetn));
  FDRE \count_i_reg[1] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__2[1]),
        .Q(count_i_reg__0[1]),
        .R(aresetn));
  FDRE \count_i_reg[2] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__2[2]),
        .Q(count_i_reg__0[2]),
        .R(aresetn));
  FDRE \count_i_reg[3] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__2[3]),
        .Q(count_i_reg__0[3]),
        .R(aresetn));
  FDRE \count_i_reg[4] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__2[4]),
        .Q(count_i_reg__0[4]),
        .R(aresetn));
  FDRE \count_i_reg[5] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__2[5]),
        .Q(count_i_reg__0[5]),
        .R(aresetn));
  FDRE \count_i_reg[6] 
       (.C(clk),
        .CE(WEBWE),
        .D(p_0_in__2[6]),
        .Q(count_i_reg__0[6]),
        .R(aresetn));
endmodule

(* ORIG_REF_NAME = "dot_product" *) 
module design_1_top_0_0_dot_product
   (D,
    CO,
    Q,
    A,
    b_ram_out,
    dp_enable,
    clk,
    aresetn,
    S,
    O,
    \slv_regs_reg[0][0] ,
    \slv_regs_reg[0][0]_0 ,
    SR,
    dp_extend_end_reg);
  output [47:0]D;
  output [0:0]CO;
  output [5:0]Q;
  input [15:0]A;
  input [31:0]b_ram_out;
  input dp_enable;
  input clk;
  input aresetn;
  input [2:0]S;
  input [0:0]O;
  input [0:0]\slv_regs_reg[0][0] ;
  input [1:0]\slv_regs_reg[0][0]_0 ;
  input [0:0]SR;
  input dp_extend_end_reg;

  wire [15:0]A;
  wire [0:0]CO;
  wire [47:0]D;
  wire [0:0]O;
  wire [5:0]Q;
  wire [2:0]S;
  wire [0:0]SR;
  wire add_r1_carry_i_4_n_0;
  wire add_r1_carry_n_0;
  wire add_r1_carry_n_1;
  wire add_r1_carry_n_2;
  wire add_r1_carry_n_3;
  wire \add_r[0]_i_2_n_0 ;
  wire \add_r[0]_i_3_n_0 ;
  wire \add_r[0]_i_4_n_0 ;
  wire \add_r[0]_i_5_n_0 ;
  wire \add_r[0]_i_6_n_0 ;
  wire \add_r[0]_i_7_n_0 ;
  wire \add_r[0]_i_8_n_0 ;
  wire \add_r[0]_i_9_n_0 ;
  wire \add_r[12]_i_2_n_0 ;
  wire \add_r[12]_i_3_n_0 ;
  wire \add_r[12]_i_4_n_0 ;
  wire \add_r[12]_i_5_n_0 ;
  wire \add_r[12]_i_6_n_0 ;
  wire \add_r[12]_i_7_n_0 ;
  wire \add_r[12]_i_8_n_0 ;
  wire \add_r[12]_i_9_n_0 ;
  wire \add_r[16]_i_2_n_0 ;
  wire \add_r[16]_i_3_n_0 ;
  wire \add_r[16]_i_4_n_0 ;
  wire \add_r[16]_i_5_n_0 ;
  wire \add_r[16]_i_6_n_0 ;
  wire \add_r[16]_i_7_n_0 ;
  wire \add_r[16]_i_8_n_0 ;
  wire \add_r[16]_i_9_n_0 ;
  wire \add_r[20]_i_2_n_0 ;
  wire \add_r[20]_i_3_n_0 ;
  wire \add_r[20]_i_4_n_0 ;
  wire \add_r[20]_i_5_n_0 ;
  wire \add_r[20]_i_6_n_0 ;
  wire \add_r[20]_i_7_n_0 ;
  wire \add_r[20]_i_8_n_0 ;
  wire \add_r[20]_i_9_n_0 ;
  wire \add_r[24]_i_2_n_0 ;
  wire \add_r[24]_i_3_n_0 ;
  wire \add_r[24]_i_4_n_0 ;
  wire \add_r[24]_i_5_n_0 ;
  wire \add_r[24]_i_6_n_0 ;
  wire \add_r[24]_i_7_n_0 ;
  wire \add_r[24]_i_8_n_0 ;
  wire \add_r[24]_i_9_n_0 ;
  wire \add_r[28]_i_2_n_0 ;
  wire \add_r[28]_i_3_n_0 ;
  wire \add_r[28]_i_4_n_0 ;
  wire \add_r[28]_i_5_n_0 ;
  wire \add_r[28]_i_6_n_0 ;
  wire \add_r[28]_i_7_n_0 ;
  wire \add_r[28]_i_8_n_0 ;
  wire \add_r[28]_i_9_n_0 ;
  wire \add_r[32]_i_2_n_0 ;
  wire \add_r[32]_i_3_n_0 ;
  wire \add_r[32]_i_4_n_0 ;
  wire \add_r[32]_i_5_n_0 ;
  wire \add_r[32]_i_6_n_0 ;
  wire \add_r[32]_i_7_n_0 ;
  wire \add_r[32]_i_8_n_0 ;
  wire \add_r[32]_i_9_n_0 ;
  wire \add_r[36]_i_2_n_0 ;
  wire \add_r[36]_i_3_n_0 ;
  wire \add_r[36]_i_4_n_0 ;
  wire \add_r[36]_i_5_n_0 ;
  wire \add_r[36]_i_6_n_0 ;
  wire \add_r[36]_i_7_n_0 ;
  wire \add_r[36]_i_8_n_0 ;
  wire \add_r[36]_i_9_n_0 ;
  wire \add_r[40]_i_2_n_0 ;
  wire \add_r[40]_i_3_n_0 ;
  wire \add_r[40]_i_4_n_0 ;
  wire \add_r[40]_i_5_n_0 ;
  wire \add_r[40]_i_6_n_0 ;
  wire \add_r[40]_i_7_n_0 ;
  wire \add_r[40]_i_8_n_0 ;
  wire \add_r[40]_i_9_n_0 ;
  wire \add_r[44]_i_2_n_0 ;
  wire \add_r[44]_i_3_n_0 ;
  wire \add_r[44]_i_4_n_0 ;
  wire \add_r[44]_i_5_n_0 ;
  wire \add_r[44]_i_6_n_0 ;
  wire \add_r[44]_i_7_n_0 ;
  wire \add_r[44]_i_8_n_0 ;
  wire \add_r[4]_i_2_n_0 ;
  wire \add_r[4]_i_3_n_0 ;
  wire \add_r[4]_i_4_n_0 ;
  wire \add_r[4]_i_5_n_0 ;
  wire \add_r[4]_i_6_n_0 ;
  wire \add_r[4]_i_7_n_0 ;
  wire \add_r[4]_i_8_n_0 ;
  wire \add_r[4]_i_9_n_0 ;
  wire \add_r[8]_i_2_n_0 ;
  wire \add_r[8]_i_3_n_0 ;
  wire \add_r[8]_i_4_n_0 ;
  wire \add_r[8]_i_5_n_0 ;
  wire \add_r[8]_i_6_n_0 ;
  wire \add_r[8]_i_7_n_0 ;
  wire \add_r[8]_i_8_n_0 ;
  wire \add_r[8]_i_9_n_0 ;
  wire \add_r_reg[0]_i_1_n_0 ;
  wire \add_r_reg[0]_i_1_n_1 ;
  wire \add_r_reg[0]_i_1_n_2 ;
  wire \add_r_reg[0]_i_1_n_3 ;
  wire \add_r_reg[0]_i_1_n_4 ;
  wire \add_r_reg[0]_i_1_n_5 ;
  wire \add_r_reg[0]_i_1_n_6 ;
  wire \add_r_reg[0]_i_1_n_7 ;
  wire \add_r_reg[12]_i_1_n_0 ;
  wire \add_r_reg[12]_i_1_n_1 ;
  wire \add_r_reg[12]_i_1_n_2 ;
  wire \add_r_reg[12]_i_1_n_3 ;
  wire \add_r_reg[12]_i_1_n_4 ;
  wire \add_r_reg[12]_i_1_n_5 ;
  wire \add_r_reg[12]_i_1_n_6 ;
  wire \add_r_reg[12]_i_1_n_7 ;
  wire \add_r_reg[16]_i_1_n_0 ;
  wire \add_r_reg[16]_i_1_n_1 ;
  wire \add_r_reg[16]_i_1_n_2 ;
  wire \add_r_reg[16]_i_1_n_3 ;
  wire \add_r_reg[16]_i_1_n_4 ;
  wire \add_r_reg[16]_i_1_n_5 ;
  wire \add_r_reg[16]_i_1_n_6 ;
  wire \add_r_reg[16]_i_1_n_7 ;
  wire \add_r_reg[20]_i_1_n_0 ;
  wire \add_r_reg[20]_i_1_n_1 ;
  wire \add_r_reg[20]_i_1_n_2 ;
  wire \add_r_reg[20]_i_1_n_3 ;
  wire \add_r_reg[20]_i_1_n_4 ;
  wire \add_r_reg[20]_i_1_n_5 ;
  wire \add_r_reg[20]_i_1_n_6 ;
  wire \add_r_reg[20]_i_1_n_7 ;
  wire \add_r_reg[24]_i_1_n_0 ;
  wire \add_r_reg[24]_i_1_n_1 ;
  wire \add_r_reg[24]_i_1_n_2 ;
  wire \add_r_reg[24]_i_1_n_3 ;
  wire \add_r_reg[24]_i_1_n_4 ;
  wire \add_r_reg[24]_i_1_n_5 ;
  wire \add_r_reg[24]_i_1_n_6 ;
  wire \add_r_reg[24]_i_1_n_7 ;
  wire \add_r_reg[28]_i_1_n_0 ;
  wire \add_r_reg[28]_i_1_n_1 ;
  wire \add_r_reg[28]_i_1_n_2 ;
  wire \add_r_reg[28]_i_1_n_3 ;
  wire \add_r_reg[28]_i_1_n_4 ;
  wire \add_r_reg[28]_i_1_n_5 ;
  wire \add_r_reg[28]_i_1_n_6 ;
  wire \add_r_reg[28]_i_1_n_7 ;
  wire \add_r_reg[32]_i_1_n_0 ;
  wire \add_r_reg[32]_i_1_n_1 ;
  wire \add_r_reg[32]_i_1_n_2 ;
  wire \add_r_reg[32]_i_1_n_3 ;
  wire \add_r_reg[32]_i_1_n_4 ;
  wire \add_r_reg[32]_i_1_n_5 ;
  wire \add_r_reg[32]_i_1_n_6 ;
  wire \add_r_reg[32]_i_1_n_7 ;
  wire \add_r_reg[36]_i_1_n_0 ;
  wire \add_r_reg[36]_i_1_n_1 ;
  wire \add_r_reg[36]_i_1_n_2 ;
  wire \add_r_reg[36]_i_1_n_3 ;
  wire \add_r_reg[36]_i_1_n_4 ;
  wire \add_r_reg[36]_i_1_n_5 ;
  wire \add_r_reg[36]_i_1_n_6 ;
  wire \add_r_reg[36]_i_1_n_7 ;
  wire \add_r_reg[40]_i_1_n_0 ;
  wire \add_r_reg[40]_i_1_n_1 ;
  wire \add_r_reg[40]_i_1_n_2 ;
  wire \add_r_reg[40]_i_1_n_3 ;
  wire \add_r_reg[40]_i_1_n_4 ;
  wire \add_r_reg[40]_i_1_n_5 ;
  wire \add_r_reg[40]_i_1_n_6 ;
  wire \add_r_reg[40]_i_1_n_7 ;
  wire \add_r_reg[44]_i_1_n_1 ;
  wire \add_r_reg[44]_i_1_n_2 ;
  wire \add_r_reg[44]_i_1_n_3 ;
  wire \add_r_reg[44]_i_1_n_4 ;
  wire \add_r_reg[44]_i_1_n_5 ;
  wire \add_r_reg[44]_i_1_n_6 ;
  wire \add_r_reg[44]_i_1_n_7 ;
  wire \add_r_reg[4]_i_1_n_0 ;
  wire \add_r_reg[4]_i_1_n_1 ;
  wire \add_r_reg[4]_i_1_n_2 ;
  wire \add_r_reg[4]_i_1_n_3 ;
  wire \add_r_reg[4]_i_1_n_4 ;
  wire \add_r_reg[4]_i_1_n_5 ;
  wire \add_r_reg[4]_i_1_n_6 ;
  wire \add_r_reg[4]_i_1_n_7 ;
  wire \add_r_reg[8]_i_1_n_0 ;
  wire \add_r_reg[8]_i_1_n_1 ;
  wire \add_r_reg[8]_i_1_n_2 ;
  wire \add_r_reg[8]_i_1_n_3 ;
  wire \add_r_reg[8]_i_1_n_4 ;
  wire \add_r_reg[8]_i_1_n_5 ;
  wire \add_r_reg[8]_i_1_n_6 ;
  wire \add_r_reg[8]_i_1_n_7 ;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \counter[1]_i_1_n_0 ;
  wire \counter[4]_i_1__6_n_0 ;
  wire \counter[6]_i_1__4_n_0 ;
  wire \counter[8]_i_4__0_n_0 ;
  wire \counter_reg_n_0_[0] ;
  wire \counter_reg_n_0_[1] ;
  wire \counter_reg_n_0_[2] ;
  wire dp_enable;
  wire dp_extend_end_reg;
  wire mul_r0_n_100;
  wire mul_r0_n_101;
  wire mul_r0_n_102;
  wire mul_r0_n_103;
  wire mul_r0_n_104;
  wire mul_r0_n_105;
  wire mul_r0_n_106;
  wire mul_r0_n_107;
  wire mul_r0_n_108;
  wire mul_r0_n_109;
  wire mul_r0_n_110;
  wire mul_r0_n_111;
  wire mul_r0_n_112;
  wire mul_r0_n_113;
  wire mul_r0_n_114;
  wire mul_r0_n_115;
  wire mul_r0_n_116;
  wire mul_r0_n_117;
  wire mul_r0_n_118;
  wire mul_r0_n_119;
  wire mul_r0_n_120;
  wire mul_r0_n_121;
  wire mul_r0_n_122;
  wire mul_r0_n_123;
  wire mul_r0_n_124;
  wire mul_r0_n_125;
  wire mul_r0_n_126;
  wire mul_r0_n_127;
  wire mul_r0_n_128;
  wire mul_r0_n_129;
  wire mul_r0_n_130;
  wire mul_r0_n_131;
  wire mul_r0_n_132;
  wire mul_r0_n_133;
  wire mul_r0_n_134;
  wire mul_r0_n_135;
  wire mul_r0_n_136;
  wire mul_r0_n_137;
  wire mul_r0_n_138;
  wire mul_r0_n_139;
  wire mul_r0_n_140;
  wire mul_r0_n_141;
  wire mul_r0_n_142;
  wire mul_r0_n_143;
  wire mul_r0_n_144;
  wire mul_r0_n_145;
  wire mul_r0_n_146;
  wire mul_r0_n_147;
  wire mul_r0_n_148;
  wire mul_r0_n_149;
  wire mul_r0_n_150;
  wire mul_r0_n_151;
  wire mul_r0_n_152;
  wire mul_r0_n_153;
  wire mul_r0_n_58;
  wire mul_r0_n_59;
  wire mul_r0_n_60;
  wire mul_r0_n_61;
  wire mul_r0_n_62;
  wire mul_r0_n_63;
  wire mul_r0_n_64;
  wire mul_r0_n_65;
  wire mul_r0_n_66;
  wire mul_r0_n_67;
  wire mul_r0_n_68;
  wire mul_r0_n_69;
  wire mul_r0_n_70;
  wire mul_r0_n_71;
  wire mul_r0_n_72;
  wire mul_r0_n_73;
  wire mul_r0_n_74;
  wire mul_r0_n_75;
  wire mul_r0_n_76;
  wire mul_r0_n_77;
  wire mul_r0_n_78;
  wire mul_r0_n_79;
  wire mul_r0_n_80;
  wire mul_r0_n_81;
  wire mul_r0_n_82;
  wire mul_r0_n_83;
  wire mul_r0_n_84;
  wire mul_r0_n_85;
  wire mul_r0_n_86;
  wire mul_r0_n_87;
  wire mul_r0_n_88;
  wire mul_r0_n_89;
  wire mul_r0_n_90;
  wire mul_r0_n_91;
  wire mul_r0_n_92;
  wire mul_r0_n_93;
  wire mul_r0_n_94;
  wire mul_r0_n_95;
  wire mul_r0_n_96;
  wire mul_r0_n_97;
  wire mul_r0_n_98;
  wire mul_r0_n_99;
  wire mul_r_reg__0_n_58;
  wire mul_r_reg__0_n_59;
  wire mul_r_reg__0_n_60;
  wire mul_r_reg__0_n_61;
  wire mul_r_reg__0_n_62;
  wire mul_r_reg__0_n_63;
  wire mul_r_reg__0_n_64;
  wire mul_r_reg__0_n_65;
  wire mul_r_reg__0_n_66;
  wire mul_r_reg__0_n_67;
  wire mul_r_reg__0_n_68;
  wire mul_r_reg__0_n_69;
  wire mul_r_reg__0_n_70;
  wire mul_r_reg__0_n_71;
  wire mul_r_reg__0_n_72;
  wire mul_r_reg__0_n_73;
  wire mul_r_reg__0_n_74;
  wire [47:0]mul_r_reg__1;
  wire [8:0]p_1_in;
  wire [0:0]\slv_regs_reg[0][0] ;
  wire [1:0]\slv_regs_reg[0][0]_0 ;
  wire [3:0]NLW_add_r1_carry_O_UNCONNECTED;
  wire [3:1]NLW_add_r1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_add_r1_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_add_r_reg[44]_i_1_CO_UNCONNECTED ;
  wire NLW_mul_r0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r0_CARRYOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r_reg__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r_reg__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_r_reg__0_PCOUT_UNCONNECTED;

  CARRY4 add_r1_carry
       (.CI(1'b0),
        .CO({add_r1_carry_n_0,add_r1_carry_n_1,add_r1_carry_n_2,add_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry_O_UNCONNECTED[3:0]),
        .S({S,add_r1_carry_i_4_n_0}));
  CARRY4 add_r1_carry__0
       (.CI(add_r1_carry_n_0),
        .CO({NLW_add_r1_carry__0_CO_UNCONNECTED[3:1],CO}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,O}));
  LUT6 #(
    .INIT(64'h6006000000006006)) 
    add_r1_carry_i_4
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\slv_regs_reg[0][0] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\slv_regs_reg[0][0]_0 [1]),
        .I4(\counter_reg_n_0_[1] ),
        .I5(\slv_regs_reg[0][0]_0 [0]),
        .O(add_r1_carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_2 
       (.I0(mul_r_reg__1[3]),
        .I1(CO),
        .O(\add_r[0]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_3 
       (.I0(mul_r_reg__1[2]),
        .I1(CO),
        .O(\add_r[0]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_4 
       (.I0(mul_r_reg__1[1]),
        .I1(CO),
        .O(\add_r[0]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_5 
       (.I0(mul_r_reg__1[0]),
        .I1(CO),
        .O(\add_r[0]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_6 
       (.I0(D[3]),
        .I1(CO),
        .I2(mul_r_reg__1[3]),
        .O(\add_r[0]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_7 
       (.I0(D[2]),
        .I1(CO),
        .I2(mul_r_reg__1[2]),
        .O(\add_r[0]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_8 
       (.I0(D[1]),
        .I1(CO),
        .I2(mul_r_reg__1[1]),
        .O(\add_r[0]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_9 
       (.I0(D[0]),
        .I1(CO),
        .I2(mul_r_reg__1[0]),
        .O(\add_r[0]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_2 
       (.I0(mul_r_reg__1[15]),
        .I1(CO),
        .O(\add_r[12]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_3 
       (.I0(mul_r_reg__1[14]),
        .I1(CO),
        .O(\add_r[12]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_4 
       (.I0(mul_r_reg__1[13]),
        .I1(CO),
        .O(\add_r[12]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_5 
       (.I0(mul_r_reg__1[12]),
        .I1(CO),
        .O(\add_r[12]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_6 
       (.I0(D[15]),
        .I1(CO),
        .I2(mul_r_reg__1[15]),
        .O(\add_r[12]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_7 
       (.I0(D[14]),
        .I1(CO),
        .I2(mul_r_reg__1[14]),
        .O(\add_r[12]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_8 
       (.I0(D[13]),
        .I1(CO),
        .I2(mul_r_reg__1[13]),
        .O(\add_r[12]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_9 
       (.I0(D[12]),
        .I1(CO),
        .I2(mul_r_reg__1[12]),
        .O(\add_r[12]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_2 
       (.I0(mul_r_reg__1[19]),
        .I1(CO),
        .O(\add_r[16]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_3 
       (.I0(mul_r_reg__1[18]),
        .I1(CO),
        .O(\add_r[16]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_4 
       (.I0(mul_r_reg__1[17]),
        .I1(CO),
        .O(\add_r[16]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_5 
       (.I0(mul_r_reg__1[16]),
        .I1(CO),
        .O(\add_r[16]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_6 
       (.I0(D[19]),
        .I1(CO),
        .I2(mul_r_reg__1[19]),
        .O(\add_r[16]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_7 
       (.I0(D[18]),
        .I1(CO),
        .I2(mul_r_reg__1[18]),
        .O(\add_r[16]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_8 
       (.I0(D[17]),
        .I1(CO),
        .I2(mul_r_reg__1[17]),
        .O(\add_r[16]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_9 
       (.I0(D[16]),
        .I1(CO),
        .I2(mul_r_reg__1[16]),
        .O(\add_r[16]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_2 
       (.I0(mul_r_reg__1[23]),
        .I1(CO),
        .O(\add_r[20]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_3 
       (.I0(mul_r_reg__1[22]),
        .I1(CO),
        .O(\add_r[20]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_4 
       (.I0(mul_r_reg__1[21]),
        .I1(CO),
        .O(\add_r[20]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_5 
       (.I0(mul_r_reg__1[20]),
        .I1(CO),
        .O(\add_r[20]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_6 
       (.I0(D[23]),
        .I1(CO),
        .I2(mul_r_reg__1[23]),
        .O(\add_r[20]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_7 
       (.I0(D[22]),
        .I1(CO),
        .I2(mul_r_reg__1[22]),
        .O(\add_r[20]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_8 
       (.I0(D[21]),
        .I1(CO),
        .I2(mul_r_reg__1[21]),
        .O(\add_r[20]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_9 
       (.I0(D[20]),
        .I1(CO),
        .I2(mul_r_reg__1[20]),
        .O(\add_r[20]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_2 
       (.I0(mul_r_reg__1[27]),
        .I1(CO),
        .O(\add_r[24]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_3 
       (.I0(mul_r_reg__1[26]),
        .I1(CO),
        .O(\add_r[24]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_4 
       (.I0(mul_r_reg__1[25]),
        .I1(CO),
        .O(\add_r[24]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_5 
       (.I0(mul_r_reg__1[24]),
        .I1(CO),
        .O(\add_r[24]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_6 
       (.I0(D[27]),
        .I1(CO),
        .I2(mul_r_reg__1[27]),
        .O(\add_r[24]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_7 
       (.I0(D[26]),
        .I1(CO),
        .I2(mul_r_reg__1[26]),
        .O(\add_r[24]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_8 
       (.I0(D[25]),
        .I1(CO),
        .I2(mul_r_reg__1[25]),
        .O(\add_r[24]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_9 
       (.I0(D[24]),
        .I1(CO),
        .I2(mul_r_reg__1[24]),
        .O(\add_r[24]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_2 
       (.I0(mul_r_reg__1[31]),
        .I1(CO),
        .O(\add_r[28]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_3 
       (.I0(mul_r_reg__1[30]),
        .I1(CO),
        .O(\add_r[28]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_4 
       (.I0(mul_r_reg__1[29]),
        .I1(CO),
        .O(\add_r[28]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_5 
       (.I0(mul_r_reg__1[28]),
        .I1(CO),
        .O(\add_r[28]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_6 
       (.I0(D[31]),
        .I1(CO),
        .I2(mul_r_reg__1[31]),
        .O(\add_r[28]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_7 
       (.I0(D[30]),
        .I1(CO),
        .I2(mul_r_reg__1[30]),
        .O(\add_r[28]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_8 
       (.I0(D[29]),
        .I1(CO),
        .I2(mul_r_reg__1[29]),
        .O(\add_r[28]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_9 
       (.I0(D[28]),
        .I1(CO),
        .I2(mul_r_reg__1[28]),
        .O(\add_r[28]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_2 
       (.I0(mul_r_reg__1[35]),
        .I1(CO),
        .O(\add_r[32]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_3 
       (.I0(mul_r_reg__1[34]),
        .I1(CO),
        .O(\add_r[32]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_4 
       (.I0(mul_r_reg__1[33]),
        .I1(CO),
        .O(\add_r[32]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_5 
       (.I0(mul_r_reg__1[32]),
        .I1(CO),
        .O(\add_r[32]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_6 
       (.I0(D[35]),
        .I1(CO),
        .I2(mul_r_reg__1[35]),
        .O(\add_r[32]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_7 
       (.I0(D[34]),
        .I1(CO),
        .I2(mul_r_reg__1[34]),
        .O(\add_r[32]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_8 
       (.I0(D[33]),
        .I1(CO),
        .I2(mul_r_reg__1[33]),
        .O(\add_r[32]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_9 
       (.I0(D[32]),
        .I1(CO),
        .I2(mul_r_reg__1[32]),
        .O(\add_r[32]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_2 
       (.I0(mul_r_reg__1[39]),
        .I1(CO),
        .O(\add_r[36]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_3 
       (.I0(mul_r_reg__1[38]),
        .I1(CO),
        .O(\add_r[36]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_4 
       (.I0(mul_r_reg__1[37]),
        .I1(CO),
        .O(\add_r[36]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_5 
       (.I0(mul_r_reg__1[36]),
        .I1(CO),
        .O(\add_r[36]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_6 
       (.I0(D[39]),
        .I1(CO),
        .I2(mul_r_reg__1[39]),
        .O(\add_r[36]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_7 
       (.I0(D[38]),
        .I1(CO),
        .I2(mul_r_reg__1[38]),
        .O(\add_r[36]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_8 
       (.I0(D[37]),
        .I1(CO),
        .I2(mul_r_reg__1[37]),
        .O(\add_r[36]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_9 
       (.I0(D[36]),
        .I1(CO),
        .I2(mul_r_reg__1[36]),
        .O(\add_r[36]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_2 
       (.I0(mul_r_reg__1[43]),
        .I1(CO),
        .O(\add_r[40]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_3 
       (.I0(mul_r_reg__1[42]),
        .I1(CO),
        .O(\add_r[40]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_4 
       (.I0(mul_r_reg__1[41]),
        .I1(CO),
        .O(\add_r[40]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_5 
       (.I0(mul_r_reg__1[40]),
        .I1(CO),
        .O(\add_r[40]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_6 
       (.I0(D[43]),
        .I1(CO),
        .I2(mul_r_reg__1[43]),
        .O(\add_r[40]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_7 
       (.I0(D[42]),
        .I1(CO),
        .I2(mul_r_reg__1[42]),
        .O(\add_r[40]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_8 
       (.I0(D[41]),
        .I1(CO),
        .I2(mul_r_reg__1[41]),
        .O(\add_r[40]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_9 
       (.I0(D[40]),
        .I1(CO),
        .I2(mul_r_reg__1[40]),
        .O(\add_r[40]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_2 
       (.I0(mul_r_reg__1[46]),
        .I1(CO),
        .O(\add_r[44]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_3 
       (.I0(mul_r_reg__1[45]),
        .I1(CO),
        .O(\add_r[44]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_4 
       (.I0(mul_r_reg__1[44]),
        .I1(CO),
        .O(\add_r[44]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_5 
       (.I0(mul_r_reg__1[47]),
        .I1(CO),
        .I2(D[47]),
        .O(\add_r[44]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_6 
       (.I0(D[46]),
        .I1(CO),
        .I2(mul_r_reg__1[46]),
        .O(\add_r[44]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_7 
       (.I0(D[45]),
        .I1(CO),
        .I2(mul_r_reg__1[45]),
        .O(\add_r[44]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_8 
       (.I0(D[44]),
        .I1(CO),
        .I2(mul_r_reg__1[44]),
        .O(\add_r[44]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_2 
       (.I0(mul_r_reg__1[7]),
        .I1(CO),
        .O(\add_r[4]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_3 
       (.I0(mul_r_reg__1[6]),
        .I1(CO),
        .O(\add_r[4]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_4 
       (.I0(mul_r_reg__1[5]),
        .I1(CO),
        .O(\add_r[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_5 
       (.I0(mul_r_reg__1[4]),
        .I1(CO),
        .O(\add_r[4]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_6 
       (.I0(D[7]),
        .I1(CO),
        .I2(mul_r_reg__1[7]),
        .O(\add_r[4]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_7 
       (.I0(D[6]),
        .I1(CO),
        .I2(mul_r_reg__1[6]),
        .O(\add_r[4]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_8 
       (.I0(D[5]),
        .I1(CO),
        .I2(mul_r_reg__1[5]),
        .O(\add_r[4]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_9 
       (.I0(D[4]),
        .I1(CO),
        .I2(mul_r_reg__1[4]),
        .O(\add_r[4]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_2 
       (.I0(mul_r_reg__1[11]),
        .I1(CO),
        .O(\add_r[8]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_3 
       (.I0(mul_r_reg__1[10]),
        .I1(CO),
        .O(\add_r[8]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_4 
       (.I0(mul_r_reg__1[9]),
        .I1(CO),
        .O(\add_r[8]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_5 
       (.I0(mul_r_reg__1[8]),
        .I1(CO),
        .O(\add_r[8]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_6 
       (.I0(D[11]),
        .I1(CO),
        .I2(mul_r_reg__1[11]),
        .O(\add_r[8]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_7 
       (.I0(D[10]),
        .I1(CO),
        .I2(mul_r_reg__1[10]),
        .O(\add_r[8]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_8 
       (.I0(D[9]),
        .I1(CO),
        .I2(mul_r_reg__1[9]),
        .O(\add_r[8]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_9 
       (.I0(D[8]),
        .I1(CO),
        .I2(mul_r_reg__1[8]),
        .O(\add_r[8]_i_9_n_0 ));
  FDRE \add_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1_n_7 ),
        .Q(D[0]),
        .R(aresetn));
  CARRY4 \add_r_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\add_r_reg[0]_i_1_n_0 ,\add_r_reg[0]_i_1_n_1 ,\add_r_reg[0]_i_1_n_2 ,\add_r_reg[0]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[0]_i_2_n_0 ,\add_r[0]_i_3_n_0 ,\add_r[0]_i_4_n_0 ,\add_r[0]_i_5_n_0 }),
        .O({\add_r_reg[0]_i_1_n_4 ,\add_r_reg[0]_i_1_n_5 ,\add_r_reg[0]_i_1_n_6 ,\add_r_reg[0]_i_1_n_7 }),
        .S({\add_r[0]_i_6_n_0 ,\add_r[0]_i_7_n_0 ,\add_r[0]_i_8_n_0 ,\add_r[0]_i_9_n_0 }));
  FDRE \add_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1_n_5 ),
        .Q(D[10]),
        .R(aresetn));
  FDRE \add_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1_n_4 ),
        .Q(D[11]),
        .R(aresetn));
  FDRE \add_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1_n_7 ),
        .Q(D[12]),
        .R(aresetn));
  CARRY4 \add_r_reg[12]_i_1 
       (.CI(\add_r_reg[8]_i_1_n_0 ),
        .CO({\add_r_reg[12]_i_1_n_0 ,\add_r_reg[12]_i_1_n_1 ,\add_r_reg[12]_i_1_n_2 ,\add_r_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[12]_i_2_n_0 ,\add_r[12]_i_3_n_0 ,\add_r[12]_i_4_n_0 ,\add_r[12]_i_5_n_0 }),
        .O({\add_r_reg[12]_i_1_n_4 ,\add_r_reg[12]_i_1_n_5 ,\add_r_reg[12]_i_1_n_6 ,\add_r_reg[12]_i_1_n_7 }),
        .S({\add_r[12]_i_6_n_0 ,\add_r[12]_i_7_n_0 ,\add_r[12]_i_8_n_0 ,\add_r[12]_i_9_n_0 }));
  FDRE \add_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1_n_6 ),
        .Q(D[13]),
        .R(aresetn));
  FDRE \add_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1_n_5 ),
        .Q(D[14]),
        .R(aresetn));
  FDRE \add_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1_n_4 ),
        .Q(D[15]),
        .R(aresetn));
  FDRE \add_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1_n_7 ),
        .Q(D[16]),
        .R(aresetn));
  CARRY4 \add_r_reg[16]_i_1 
       (.CI(\add_r_reg[12]_i_1_n_0 ),
        .CO({\add_r_reg[16]_i_1_n_0 ,\add_r_reg[16]_i_1_n_1 ,\add_r_reg[16]_i_1_n_2 ,\add_r_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[16]_i_2_n_0 ,\add_r[16]_i_3_n_0 ,\add_r[16]_i_4_n_0 ,\add_r[16]_i_5_n_0 }),
        .O({\add_r_reg[16]_i_1_n_4 ,\add_r_reg[16]_i_1_n_5 ,\add_r_reg[16]_i_1_n_6 ,\add_r_reg[16]_i_1_n_7 }),
        .S({\add_r[16]_i_6_n_0 ,\add_r[16]_i_7_n_0 ,\add_r[16]_i_8_n_0 ,\add_r[16]_i_9_n_0 }));
  FDRE \add_r_reg[17] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1_n_6 ),
        .Q(D[17]),
        .R(aresetn));
  FDRE \add_r_reg[18] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1_n_5 ),
        .Q(D[18]),
        .R(aresetn));
  FDRE \add_r_reg[19] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1_n_4 ),
        .Q(D[19]),
        .R(aresetn));
  FDRE \add_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1_n_6 ),
        .Q(D[1]),
        .R(aresetn));
  FDRE \add_r_reg[20] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1_n_7 ),
        .Q(D[20]),
        .R(aresetn));
  CARRY4 \add_r_reg[20]_i_1 
       (.CI(\add_r_reg[16]_i_1_n_0 ),
        .CO({\add_r_reg[20]_i_1_n_0 ,\add_r_reg[20]_i_1_n_1 ,\add_r_reg[20]_i_1_n_2 ,\add_r_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[20]_i_2_n_0 ,\add_r[20]_i_3_n_0 ,\add_r[20]_i_4_n_0 ,\add_r[20]_i_5_n_0 }),
        .O({\add_r_reg[20]_i_1_n_4 ,\add_r_reg[20]_i_1_n_5 ,\add_r_reg[20]_i_1_n_6 ,\add_r_reg[20]_i_1_n_7 }),
        .S({\add_r[20]_i_6_n_0 ,\add_r[20]_i_7_n_0 ,\add_r[20]_i_8_n_0 ,\add_r[20]_i_9_n_0 }));
  FDRE \add_r_reg[21] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1_n_6 ),
        .Q(D[21]),
        .R(aresetn));
  FDRE \add_r_reg[22] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1_n_5 ),
        .Q(D[22]),
        .R(aresetn));
  FDRE \add_r_reg[23] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1_n_4 ),
        .Q(D[23]),
        .R(aresetn));
  FDRE \add_r_reg[24] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1_n_7 ),
        .Q(D[24]),
        .R(aresetn));
  CARRY4 \add_r_reg[24]_i_1 
       (.CI(\add_r_reg[20]_i_1_n_0 ),
        .CO({\add_r_reg[24]_i_1_n_0 ,\add_r_reg[24]_i_1_n_1 ,\add_r_reg[24]_i_1_n_2 ,\add_r_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[24]_i_2_n_0 ,\add_r[24]_i_3_n_0 ,\add_r[24]_i_4_n_0 ,\add_r[24]_i_5_n_0 }),
        .O({\add_r_reg[24]_i_1_n_4 ,\add_r_reg[24]_i_1_n_5 ,\add_r_reg[24]_i_1_n_6 ,\add_r_reg[24]_i_1_n_7 }),
        .S({\add_r[24]_i_6_n_0 ,\add_r[24]_i_7_n_0 ,\add_r[24]_i_8_n_0 ,\add_r[24]_i_9_n_0 }));
  FDRE \add_r_reg[25] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1_n_6 ),
        .Q(D[25]),
        .R(aresetn));
  FDRE \add_r_reg[26] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1_n_5 ),
        .Q(D[26]),
        .R(aresetn));
  FDRE \add_r_reg[27] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1_n_4 ),
        .Q(D[27]),
        .R(aresetn));
  FDRE \add_r_reg[28] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1_n_7 ),
        .Q(D[28]),
        .R(aresetn));
  CARRY4 \add_r_reg[28]_i_1 
       (.CI(\add_r_reg[24]_i_1_n_0 ),
        .CO({\add_r_reg[28]_i_1_n_0 ,\add_r_reg[28]_i_1_n_1 ,\add_r_reg[28]_i_1_n_2 ,\add_r_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[28]_i_2_n_0 ,\add_r[28]_i_3_n_0 ,\add_r[28]_i_4_n_0 ,\add_r[28]_i_5_n_0 }),
        .O({\add_r_reg[28]_i_1_n_4 ,\add_r_reg[28]_i_1_n_5 ,\add_r_reg[28]_i_1_n_6 ,\add_r_reg[28]_i_1_n_7 }),
        .S({\add_r[28]_i_6_n_0 ,\add_r[28]_i_7_n_0 ,\add_r[28]_i_8_n_0 ,\add_r[28]_i_9_n_0 }));
  FDRE \add_r_reg[29] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1_n_6 ),
        .Q(D[29]),
        .R(aresetn));
  FDRE \add_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1_n_5 ),
        .Q(D[2]),
        .R(aresetn));
  FDRE \add_r_reg[30] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1_n_5 ),
        .Q(D[30]),
        .R(aresetn));
  FDRE \add_r_reg[31] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1_n_4 ),
        .Q(D[31]),
        .R(aresetn));
  FDRE \add_r_reg[32] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1_n_7 ),
        .Q(D[32]),
        .R(aresetn));
  CARRY4 \add_r_reg[32]_i_1 
       (.CI(\add_r_reg[28]_i_1_n_0 ),
        .CO({\add_r_reg[32]_i_1_n_0 ,\add_r_reg[32]_i_1_n_1 ,\add_r_reg[32]_i_1_n_2 ,\add_r_reg[32]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[32]_i_2_n_0 ,\add_r[32]_i_3_n_0 ,\add_r[32]_i_4_n_0 ,\add_r[32]_i_5_n_0 }),
        .O({\add_r_reg[32]_i_1_n_4 ,\add_r_reg[32]_i_1_n_5 ,\add_r_reg[32]_i_1_n_6 ,\add_r_reg[32]_i_1_n_7 }),
        .S({\add_r[32]_i_6_n_0 ,\add_r[32]_i_7_n_0 ,\add_r[32]_i_8_n_0 ,\add_r[32]_i_9_n_0 }));
  FDRE \add_r_reg[33] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1_n_6 ),
        .Q(D[33]),
        .R(aresetn));
  FDRE \add_r_reg[34] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1_n_5 ),
        .Q(D[34]),
        .R(aresetn));
  FDRE \add_r_reg[35] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1_n_4 ),
        .Q(D[35]),
        .R(aresetn));
  FDRE \add_r_reg[36] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1_n_7 ),
        .Q(D[36]),
        .R(aresetn));
  CARRY4 \add_r_reg[36]_i_1 
       (.CI(\add_r_reg[32]_i_1_n_0 ),
        .CO({\add_r_reg[36]_i_1_n_0 ,\add_r_reg[36]_i_1_n_1 ,\add_r_reg[36]_i_1_n_2 ,\add_r_reg[36]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[36]_i_2_n_0 ,\add_r[36]_i_3_n_0 ,\add_r[36]_i_4_n_0 ,\add_r[36]_i_5_n_0 }),
        .O({\add_r_reg[36]_i_1_n_4 ,\add_r_reg[36]_i_1_n_5 ,\add_r_reg[36]_i_1_n_6 ,\add_r_reg[36]_i_1_n_7 }),
        .S({\add_r[36]_i_6_n_0 ,\add_r[36]_i_7_n_0 ,\add_r[36]_i_8_n_0 ,\add_r[36]_i_9_n_0 }));
  FDRE \add_r_reg[37] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1_n_6 ),
        .Q(D[37]),
        .R(aresetn));
  FDRE \add_r_reg[38] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1_n_5 ),
        .Q(D[38]),
        .R(aresetn));
  FDRE \add_r_reg[39] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1_n_4 ),
        .Q(D[39]),
        .R(aresetn));
  FDRE \add_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1_n_4 ),
        .Q(D[3]),
        .R(aresetn));
  FDRE \add_r_reg[40] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1_n_7 ),
        .Q(D[40]),
        .R(aresetn));
  CARRY4 \add_r_reg[40]_i_1 
       (.CI(\add_r_reg[36]_i_1_n_0 ),
        .CO({\add_r_reg[40]_i_1_n_0 ,\add_r_reg[40]_i_1_n_1 ,\add_r_reg[40]_i_1_n_2 ,\add_r_reg[40]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[40]_i_2_n_0 ,\add_r[40]_i_3_n_0 ,\add_r[40]_i_4_n_0 ,\add_r[40]_i_5_n_0 }),
        .O({\add_r_reg[40]_i_1_n_4 ,\add_r_reg[40]_i_1_n_5 ,\add_r_reg[40]_i_1_n_6 ,\add_r_reg[40]_i_1_n_7 }),
        .S({\add_r[40]_i_6_n_0 ,\add_r[40]_i_7_n_0 ,\add_r[40]_i_8_n_0 ,\add_r[40]_i_9_n_0 }));
  FDRE \add_r_reg[41] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1_n_6 ),
        .Q(D[41]),
        .R(aresetn));
  FDRE \add_r_reg[42] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1_n_5 ),
        .Q(D[42]),
        .R(aresetn));
  FDRE \add_r_reg[43] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1_n_4 ),
        .Q(D[43]),
        .R(aresetn));
  FDRE \add_r_reg[44] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1_n_7 ),
        .Q(D[44]),
        .R(aresetn));
  CARRY4 \add_r_reg[44]_i_1 
       (.CI(\add_r_reg[40]_i_1_n_0 ),
        .CO({\NLW_add_r_reg[44]_i_1_CO_UNCONNECTED [3],\add_r_reg[44]_i_1_n_1 ,\add_r_reg[44]_i_1_n_2 ,\add_r_reg[44]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\add_r[44]_i_2_n_0 ,\add_r[44]_i_3_n_0 ,\add_r[44]_i_4_n_0 }),
        .O({\add_r_reg[44]_i_1_n_4 ,\add_r_reg[44]_i_1_n_5 ,\add_r_reg[44]_i_1_n_6 ,\add_r_reg[44]_i_1_n_7 }),
        .S({\add_r[44]_i_5_n_0 ,\add_r[44]_i_6_n_0 ,\add_r[44]_i_7_n_0 ,\add_r[44]_i_8_n_0 }));
  FDRE \add_r_reg[45] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1_n_6 ),
        .Q(D[45]),
        .R(aresetn));
  FDRE \add_r_reg[46] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1_n_5 ),
        .Q(D[46]),
        .R(aresetn));
  FDRE \add_r_reg[47] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1_n_4 ),
        .Q(D[47]),
        .R(aresetn));
  FDRE \add_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1_n_7 ),
        .Q(D[4]),
        .R(aresetn));
  CARRY4 \add_r_reg[4]_i_1 
       (.CI(\add_r_reg[0]_i_1_n_0 ),
        .CO({\add_r_reg[4]_i_1_n_0 ,\add_r_reg[4]_i_1_n_1 ,\add_r_reg[4]_i_1_n_2 ,\add_r_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[4]_i_2_n_0 ,\add_r[4]_i_3_n_0 ,\add_r[4]_i_4_n_0 ,\add_r[4]_i_5_n_0 }),
        .O({\add_r_reg[4]_i_1_n_4 ,\add_r_reg[4]_i_1_n_5 ,\add_r_reg[4]_i_1_n_6 ,\add_r_reg[4]_i_1_n_7 }),
        .S({\add_r[4]_i_6_n_0 ,\add_r[4]_i_7_n_0 ,\add_r[4]_i_8_n_0 ,\add_r[4]_i_9_n_0 }));
  FDRE \add_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1_n_6 ),
        .Q(D[5]),
        .R(aresetn));
  FDRE \add_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1_n_5 ),
        .Q(D[6]),
        .R(aresetn));
  FDRE \add_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1_n_4 ),
        .Q(D[7]),
        .R(aresetn));
  FDRE \add_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1_n_7 ),
        .Q(D[8]),
        .R(aresetn));
  CARRY4 \add_r_reg[8]_i_1 
       (.CI(\add_r_reg[4]_i_1_n_0 ),
        .CO({\add_r_reg[8]_i_1_n_0 ,\add_r_reg[8]_i_1_n_1 ,\add_r_reg[8]_i_1_n_2 ,\add_r_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[8]_i_2_n_0 ,\add_r[8]_i_3_n_0 ,\add_r[8]_i_4_n_0 ,\add_r[8]_i_5_n_0 }),
        .O({\add_r_reg[8]_i_1_n_4 ,\add_r_reg[8]_i_1_n_5 ,\add_r_reg[8]_i_1_n_6 ,\add_r_reg[8]_i_1_n_7 }),
        .S({\add_r[8]_i_6_n_0 ,\add_r[8]_i_7_n_0 ,\add_r[8]_i_8_n_0 ,\add_r[8]_i_9_n_0 }));
  FDRE \add_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1_n_6 ),
        .Q(D[9]),
        .R(aresetn));
  LUT1 #(
    .INIT(2'h1)) 
    \counter[0]_i_1__2 
       (.I0(\counter_reg_n_0_[0] ),
        .O(p_1_in[0]));
  LUT4 #(
    .INIT(16'hFD0E)) 
    \counter[1]_i_1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(CO),
        .I2(dp_extend_end_reg),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \counter[2]_i_1__1 
       (.I0(\counter_reg_n_0_[2] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[1] ),
        .O(p_1_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \counter[3]_i_1__0 
       (.I0(Q[0]),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[0] ),
        .I3(\counter_reg_n_0_[2] ),
        .O(p_1_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \counter[4]_i_1__6 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[2] ),
        .O(\counter[4]_i_1__6_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \counter[5]_i_1__0 
       (.I0(Q[2]),
        .I1(\counter_reg_n_0_[2] ),
        .I2(\counter_reg_n_0_[0] ),
        .I3(\counter_reg_n_0_[1] ),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(p_1_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT4 #(
    .INIT(16'hA6AA)) 
    \counter[6]_i_1__4 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(\counter[8]_i_4__0_n_0 ),
        .I3(Q[2]),
        .O(\counter[6]_i_1__4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT5 #(
    .INIT(32'hA6AAAAAA)) 
    \counter[7]_i_1 
       (.I0(Q[4]),
        .I1(Q[2]),
        .I2(\counter[8]_i_4__0_n_0 ),
        .I3(Q[1]),
        .I4(Q[3]),
        .O(p_1_in[7]));
  LUT6 #(
    .INIT(64'hAA6AAAAAAAAAAAAA)) 
    \counter[8]_i_3__0 
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(\counter[8]_i_4__0_n_0 ),
        .I4(Q[2]),
        .I5(Q[4]),
        .O(p_1_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \counter[8]_i_4__0 
       (.I0(\counter_reg_n_0_[2] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[1] ),
        .I3(Q[0]),
        .O(\counter[8]_i_4__0_n_0 ));
  FDRE \counter_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(p_1_in[0]),
        .Q(\counter_reg_n_0_[0] ),
        .R(SR));
  FDRE \counter_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\counter[1]_i_1_n_0 ),
        .Q(\counter_reg_n_0_[1] ),
        .R(aresetn));
  FDRE \counter_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(p_1_in[2]),
        .Q(\counter_reg_n_0_[2] ),
        .R(SR));
  FDRE \counter_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(p_1_in[3]),
        .Q(Q[0]),
        .R(SR));
  FDRE \counter_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[4]_i_1__6_n_0 ),
        .Q(Q[1]),
        .R(SR));
  FDRE \counter_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(p_1_in[5]),
        .Q(Q[2]),
        .R(SR));
  FDRE \counter_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[6]_i_1__4_n_0 ),
        .Q(Q[3]),
        .R(SR));
  FDRE \counter_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(p_1_in[7]),
        .Q(Q[4]),
        .R(SR));
  FDRE \counter_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(p_1_in[8]),
        .Q(Q[5]),
        .R(SR));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,b_ram_out[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({A[15],A[15],A}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r0_OVERFLOW_UNCONNECTED),
        .P({mul_r0_n_58,mul_r0_n_59,mul_r0_n_60,mul_r0_n_61,mul_r0_n_62,mul_r0_n_63,mul_r0_n_64,mul_r0_n_65,mul_r0_n_66,mul_r0_n_67,mul_r0_n_68,mul_r0_n_69,mul_r0_n_70,mul_r0_n_71,mul_r0_n_72,mul_r0_n_73,mul_r0_n_74,mul_r0_n_75,mul_r0_n_76,mul_r0_n_77,mul_r0_n_78,mul_r0_n_79,mul_r0_n_80,mul_r0_n_81,mul_r0_n_82,mul_r0_n_83,mul_r0_n_84,mul_r0_n_85,mul_r0_n_86,mul_r0_n_87,mul_r0_n_88,mul_r0_n_89,mul_r0_n_90,mul_r0_n_91,mul_r0_n_92,mul_r0_n_93,mul_r0_n_94,mul_r0_n_95,mul_r0_n_96,mul_r0_n_97,mul_r0_n_98,mul_r0_n_99,mul_r0_n_100,mul_r0_n_101,mul_r0_n_102,mul_r0_n_103,mul_r0_n_104,mul_r0_n_105}),
        .PATTERNBDETECT(NLW_mul_r0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_r0_UNDERFLOW_UNCONNECTED));
  FDRE \mul_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_105),
        .Q(mul_r_reg__1[0]),
        .R(aresetn));
  FDRE \mul_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_95),
        .Q(mul_r_reg__1[10]),
        .R(aresetn));
  FDRE \mul_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_94),
        .Q(mul_r_reg__1[11]),
        .R(aresetn));
  FDRE \mul_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_93),
        .Q(mul_r_reg__1[12]),
        .R(aresetn));
  FDRE \mul_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_92),
        .Q(mul_r_reg__1[13]),
        .R(aresetn));
  FDRE \mul_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_91),
        .Q(mul_r_reg__1[14]),
        .R(aresetn));
  FDRE \mul_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_90),
        .Q(mul_r_reg__1[15]),
        .R(aresetn));
  FDRE \mul_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_89),
        .Q(mul_r_reg__1[16]),
        .R(aresetn));
  FDRE \mul_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_104),
        .Q(mul_r_reg__1[1]),
        .R(aresetn));
  FDRE \mul_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_103),
        .Q(mul_r_reg__1[2]),
        .R(aresetn));
  FDRE \mul_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_102),
        .Q(mul_r_reg__1[3]),
        .R(aresetn));
  FDRE \mul_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_101),
        .Q(mul_r_reg__1[4]),
        .R(aresetn));
  FDRE \mul_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_100),
        .Q(mul_r_reg__1[5]),
        .R(aresetn));
  FDRE \mul_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_99),
        .Q(mul_r_reg__1[6]),
        .R(aresetn));
  FDRE \mul_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_98),
        .Q(mul_r_reg__1[7]),
        .R(aresetn));
  FDRE \mul_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_97),
        .Q(mul_r_reg__1[8]),
        .R(aresetn));
  FDRE \mul_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_96),
        .Q(mul_r_reg__1[9]),
        .R(aresetn));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-12 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r_reg__0
       (.A({A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r_reg__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({b_ram_out[31],b_ram_out[31],b_ram_out[31],b_ram_out[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r_reg__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(dp_enable),
        .CLK(clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED),
        .P({mul_r_reg__0_n_58,mul_r_reg__0_n_59,mul_r_reg__0_n_60,mul_r_reg__0_n_61,mul_r_reg__0_n_62,mul_r_reg__0_n_63,mul_r_reg__0_n_64,mul_r_reg__0_n_65,mul_r_reg__0_n_66,mul_r_reg__0_n_67,mul_r_reg__0_n_68,mul_r_reg__0_n_69,mul_r_reg__0_n_70,mul_r_reg__0_n_71,mul_r_reg__0_n_72,mul_r_reg__0_n_73,mul_r_reg__0_n_74,mul_r_reg__1[47:17]}),
        .PATTERNBDETECT(NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .PCOUT(NLW_mul_r_reg__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(aresetn),
        .UNDERFLOW(NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "dot_product" *) 
module design_1_top_0_0_dot_product_0
   (D,
    CO,
    Q,
    A,
    b_ram_out,
    dp_enable,
    clk,
    aresetn,
    \slv_regs_reg[0][11] ,
    O,
    \slv_regs_reg[0][0] ,
    \slv_regs_reg[0][8] ,
    dp_extend_end_reg,
    dp_extend_end_reg_0);
  output [47:0]D;
  output [0:0]CO;
  output [2:0]Q;
  input [15:0]A;
  input [31:0]b_ram_out;
  input dp_enable;
  input clk;
  input aresetn;
  input [1:0]\slv_regs_reg[0][11] ;
  input [0:0]O;
  input [0:0]\slv_regs_reg[0][0] ;
  input [4:0]\slv_regs_reg[0][8] ;
  input [0:0]dp_extend_end_reg;
  input dp_extend_end_reg_0;

  wire [15:0]A;
  wire [0:0]CO;
  wire [47:0]D;
  wire [0:0]O;
  wire [2:0]Q;
  wire add_r1_carry_i_2__0_n_0;
  wire add_r1_carry_i_4__0_n_0;
  wire add_r1_carry_n_0;
  wire add_r1_carry_n_1;
  wire add_r1_carry_n_2;
  wire add_r1_carry_n_3;
  wire \add_r[0]_i_2__0_n_0 ;
  wire \add_r[0]_i_3__0_n_0 ;
  wire \add_r[0]_i_4__0_n_0 ;
  wire \add_r[0]_i_5__0_n_0 ;
  wire \add_r[0]_i_6__0_n_0 ;
  wire \add_r[0]_i_7__0_n_0 ;
  wire \add_r[0]_i_8__0_n_0 ;
  wire \add_r[0]_i_9__0_n_0 ;
  wire \add_r[12]_i_2__0_n_0 ;
  wire \add_r[12]_i_3__0_n_0 ;
  wire \add_r[12]_i_4__0_n_0 ;
  wire \add_r[12]_i_5__0_n_0 ;
  wire \add_r[12]_i_6__0_n_0 ;
  wire \add_r[12]_i_7__0_n_0 ;
  wire \add_r[12]_i_8__0_n_0 ;
  wire \add_r[12]_i_9__0_n_0 ;
  wire \add_r[16]_i_2__0_n_0 ;
  wire \add_r[16]_i_3__0_n_0 ;
  wire \add_r[16]_i_4__0_n_0 ;
  wire \add_r[16]_i_5__0_n_0 ;
  wire \add_r[16]_i_6__0_n_0 ;
  wire \add_r[16]_i_7__0_n_0 ;
  wire \add_r[16]_i_8__0_n_0 ;
  wire \add_r[16]_i_9__0_n_0 ;
  wire \add_r[20]_i_2__0_n_0 ;
  wire \add_r[20]_i_3__0_n_0 ;
  wire \add_r[20]_i_4__0_n_0 ;
  wire \add_r[20]_i_5__0_n_0 ;
  wire \add_r[20]_i_6__0_n_0 ;
  wire \add_r[20]_i_7__0_n_0 ;
  wire \add_r[20]_i_8__0_n_0 ;
  wire \add_r[20]_i_9__0_n_0 ;
  wire \add_r[24]_i_2__0_n_0 ;
  wire \add_r[24]_i_3__0_n_0 ;
  wire \add_r[24]_i_4__0_n_0 ;
  wire \add_r[24]_i_5__0_n_0 ;
  wire \add_r[24]_i_6__0_n_0 ;
  wire \add_r[24]_i_7__0_n_0 ;
  wire \add_r[24]_i_8__0_n_0 ;
  wire \add_r[24]_i_9__0_n_0 ;
  wire \add_r[28]_i_2__0_n_0 ;
  wire \add_r[28]_i_3__0_n_0 ;
  wire \add_r[28]_i_4__0_n_0 ;
  wire \add_r[28]_i_5__0_n_0 ;
  wire \add_r[28]_i_6__0_n_0 ;
  wire \add_r[28]_i_7__0_n_0 ;
  wire \add_r[28]_i_8__0_n_0 ;
  wire \add_r[28]_i_9__0_n_0 ;
  wire \add_r[32]_i_2__0_n_0 ;
  wire \add_r[32]_i_3__0_n_0 ;
  wire \add_r[32]_i_4__0_n_0 ;
  wire \add_r[32]_i_5__0_n_0 ;
  wire \add_r[32]_i_6__0_n_0 ;
  wire \add_r[32]_i_7__0_n_0 ;
  wire \add_r[32]_i_8__0_n_0 ;
  wire \add_r[32]_i_9__0_n_0 ;
  wire \add_r[36]_i_2__0_n_0 ;
  wire \add_r[36]_i_3__0_n_0 ;
  wire \add_r[36]_i_4__0_n_0 ;
  wire \add_r[36]_i_5__0_n_0 ;
  wire \add_r[36]_i_6__0_n_0 ;
  wire \add_r[36]_i_7__0_n_0 ;
  wire \add_r[36]_i_8__0_n_0 ;
  wire \add_r[36]_i_9__0_n_0 ;
  wire \add_r[40]_i_2__0_n_0 ;
  wire \add_r[40]_i_3__0_n_0 ;
  wire \add_r[40]_i_4__0_n_0 ;
  wire \add_r[40]_i_5__0_n_0 ;
  wire \add_r[40]_i_6__0_n_0 ;
  wire \add_r[40]_i_7__0_n_0 ;
  wire \add_r[40]_i_8__0_n_0 ;
  wire \add_r[40]_i_9__0_n_0 ;
  wire \add_r[44]_i_2__0_n_0 ;
  wire \add_r[44]_i_3__0_n_0 ;
  wire \add_r[44]_i_4__0_n_0 ;
  wire \add_r[44]_i_5__0_n_0 ;
  wire \add_r[44]_i_6__0_n_0 ;
  wire \add_r[44]_i_7__0_n_0 ;
  wire \add_r[44]_i_8__0_n_0 ;
  wire \add_r[4]_i_2__0_n_0 ;
  wire \add_r[4]_i_3__0_n_0 ;
  wire \add_r[4]_i_4__0_n_0 ;
  wire \add_r[4]_i_5__0_n_0 ;
  wire \add_r[4]_i_6__0_n_0 ;
  wire \add_r[4]_i_7__0_n_0 ;
  wire \add_r[4]_i_8__0_n_0 ;
  wire \add_r[4]_i_9__0_n_0 ;
  wire \add_r[8]_i_2__0_n_0 ;
  wire \add_r[8]_i_3__0_n_0 ;
  wire \add_r[8]_i_4__0_n_0 ;
  wire \add_r[8]_i_5__0_n_0 ;
  wire \add_r[8]_i_6__0_n_0 ;
  wire \add_r[8]_i_7__0_n_0 ;
  wire \add_r[8]_i_8__0_n_0 ;
  wire \add_r[8]_i_9__0_n_0 ;
  wire \add_r_reg[0]_i_1__0_n_0 ;
  wire \add_r_reg[0]_i_1__0_n_1 ;
  wire \add_r_reg[0]_i_1__0_n_2 ;
  wire \add_r_reg[0]_i_1__0_n_3 ;
  wire \add_r_reg[0]_i_1__0_n_4 ;
  wire \add_r_reg[0]_i_1__0_n_5 ;
  wire \add_r_reg[0]_i_1__0_n_6 ;
  wire \add_r_reg[0]_i_1__0_n_7 ;
  wire \add_r_reg[12]_i_1__0_n_0 ;
  wire \add_r_reg[12]_i_1__0_n_1 ;
  wire \add_r_reg[12]_i_1__0_n_2 ;
  wire \add_r_reg[12]_i_1__0_n_3 ;
  wire \add_r_reg[12]_i_1__0_n_4 ;
  wire \add_r_reg[12]_i_1__0_n_5 ;
  wire \add_r_reg[12]_i_1__0_n_6 ;
  wire \add_r_reg[12]_i_1__0_n_7 ;
  wire \add_r_reg[16]_i_1__0_n_0 ;
  wire \add_r_reg[16]_i_1__0_n_1 ;
  wire \add_r_reg[16]_i_1__0_n_2 ;
  wire \add_r_reg[16]_i_1__0_n_3 ;
  wire \add_r_reg[16]_i_1__0_n_4 ;
  wire \add_r_reg[16]_i_1__0_n_5 ;
  wire \add_r_reg[16]_i_1__0_n_6 ;
  wire \add_r_reg[16]_i_1__0_n_7 ;
  wire \add_r_reg[20]_i_1__0_n_0 ;
  wire \add_r_reg[20]_i_1__0_n_1 ;
  wire \add_r_reg[20]_i_1__0_n_2 ;
  wire \add_r_reg[20]_i_1__0_n_3 ;
  wire \add_r_reg[20]_i_1__0_n_4 ;
  wire \add_r_reg[20]_i_1__0_n_5 ;
  wire \add_r_reg[20]_i_1__0_n_6 ;
  wire \add_r_reg[20]_i_1__0_n_7 ;
  wire \add_r_reg[24]_i_1__0_n_0 ;
  wire \add_r_reg[24]_i_1__0_n_1 ;
  wire \add_r_reg[24]_i_1__0_n_2 ;
  wire \add_r_reg[24]_i_1__0_n_3 ;
  wire \add_r_reg[24]_i_1__0_n_4 ;
  wire \add_r_reg[24]_i_1__0_n_5 ;
  wire \add_r_reg[24]_i_1__0_n_6 ;
  wire \add_r_reg[24]_i_1__0_n_7 ;
  wire \add_r_reg[28]_i_1__0_n_0 ;
  wire \add_r_reg[28]_i_1__0_n_1 ;
  wire \add_r_reg[28]_i_1__0_n_2 ;
  wire \add_r_reg[28]_i_1__0_n_3 ;
  wire \add_r_reg[28]_i_1__0_n_4 ;
  wire \add_r_reg[28]_i_1__0_n_5 ;
  wire \add_r_reg[28]_i_1__0_n_6 ;
  wire \add_r_reg[28]_i_1__0_n_7 ;
  wire \add_r_reg[32]_i_1__0_n_0 ;
  wire \add_r_reg[32]_i_1__0_n_1 ;
  wire \add_r_reg[32]_i_1__0_n_2 ;
  wire \add_r_reg[32]_i_1__0_n_3 ;
  wire \add_r_reg[32]_i_1__0_n_4 ;
  wire \add_r_reg[32]_i_1__0_n_5 ;
  wire \add_r_reg[32]_i_1__0_n_6 ;
  wire \add_r_reg[32]_i_1__0_n_7 ;
  wire \add_r_reg[36]_i_1__0_n_0 ;
  wire \add_r_reg[36]_i_1__0_n_1 ;
  wire \add_r_reg[36]_i_1__0_n_2 ;
  wire \add_r_reg[36]_i_1__0_n_3 ;
  wire \add_r_reg[36]_i_1__0_n_4 ;
  wire \add_r_reg[36]_i_1__0_n_5 ;
  wire \add_r_reg[36]_i_1__0_n_6 ;
  wire \add_r_reg[36]_i_1__0_n_7 ;
  wire \add_r_reg[40]_i_1__0_n_0 ;
  wire \add_r_reg[40]_i_1__0_n_1 ;
  wire \add_r_reg[40]_i_1__0_n_2 ;
  wire \add_r_reg[40]_i_1__0_n_3 ;
  wire \add_r_reg[40]_i_1__0_n_4 ;
  wire \add_r_reg[40]_i_1__0_n_5 ;
  wire \add_r_reg[40]_i_1__0_n_6 ;
  wire \add_r_reg[40]_i_1__0_n_7 ;
  wire \add_r_reg[44]_i_1__0_n_1 ;
  wire \add_r_reg[44]_i_1__0_n_2 ;
  wire \add_r_reg[44]_i_1__0_n_3 ;
  wire \add_r_reg[44]_i_1__0_n_4 ;
  wire \add_r_reg[44]_i_1__0_n_5 ;
  wire \add_r_reg[44]_i_1__0_n_6 ;
  wire \add_r_reg[44]_i_1__0_n_7 ;
  wire \add_r_reg[4]_i_1__0_n_0 ;
  wire \add_r_reg[4]_i_1__0_n_1 ;
  wire \add_r_reg[4]_i_1__0_n_2 ;
  wire \add_r_reg[4]_i_1__0_n_3 ;
  wire \add_r_reg[4]_i_1__0_n_4 ;
  wire \add_r_reg[4]_i_1__0_n_5 ;
  wire \add_r_reg[4]_i_1__0_n_6 ;
  wire \add_r_reg[4]_i_1__0_n_7 ;
  wire \add_r_reg[8]_i_1__0_n_0 ;
  wire \add_r_reg[8]_i_1__0_n_1 ;
  wire \add_r_reg[8]_i_1__0_n_2 ;
  wire \add_r_reg[8]_i_1__0_n_3 ;
  wire \add_r_reg[8]_i_1__0_n_4 ;
  wire \add_r_reg[8]_i_1__0_n_5 ;
  wire \add_r_reg[8]_i_1__0_n_6 ;
  wire \add_r_reg[8]_i_1__0_n_7 ;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \counter[0]_i_1__3_n_0 ;
  wire \counter[1]_i_1_n_0 ;
  wire \counter[2]_i_1__2_n_0 ;
  wire \counter[3]_i_1__2_n_0 ;
  wire \counter[4]_i_1_n_0 ;
  wire \counter[5]_i_1__1_n_0 ;
  wire \counter[6]_i_1__0_n_0 ;
  wire \counter[7]_i_1__0_n_0 ;
  wire \counter[7]_i_2_n_0 ;
  wire \counter[8]_i_2__1_n_0 ;
  wire \counter[8]_i_3__1_n_0 ;
  wire \counter_reg_n_0_[0] ;
  wire \counter_reg_n_0_[1] ;
  wire \counter_reg_n_0_[2] ;
  wire \counter_reg_n_0_[6] ;
  wire \counter_reg_n_0_[7] ;
  wire \counter_reg_n_0_[8] ;
  wire dp_enable;
  wire [0:0]dp_extend_end_reg;
  wire dp_extend_end_reg_0;
  wire mul_r0_n_100;
  wire mul_r0_n_101;
  wire mul_r0_n_102;
  wire mul_r0_n_103;
  wire mul_r0_n_104;
  wire mul_r0_n_105;
  wire mul_r0_n_106;
  wire mul_r0_n_107;
  wire mul_r0_n_108;
  wire mul_r0_n_109;
  wire mul_r0_n_110;
  wire mul_r0_n_111;
  wire mul_r0_n_112;
  wire mul_r0_n_113;
  wire mul_r0_n_114;
  wire mul_r0_n_115;
  wire mul_r0_n_116;
  wire mul_r0_n_117;
  wire mul_r0_n_118;
  wire mul_r0_n_119;
  wire mul_r0_n_120;
  wire mul_r0_n_121;
  wire mul_r0_n_122;
  wire mul_r0_n_123;
  wire mul_r0_n_124;
  wire mul_r0_n_125;
  wire mul_r0_n_126;
  wire mul_r0_n_127;
  wire mul_r0_n_128;
  wire mul_r0_n_129;
  wire mul_r0_n_130;
  wire mul_r0_n_131;
  wire mul_r0_n_132;
  wire mul_r0_n_133;
  wire mul_r0_n_134;
  wire mul_r0_n_135;
  wire mul_r0_n_136;
  wire mul_r0_n_137;
  wire mul_r0_n_138;
  wire mul_r0_n_139;
  wire mul_r0_n_140;
  wire mul_r0_n_141;
  wire mul_r0_n_142;
  wire mul_r0_n_143;
  wire mul_r0_n_144;
  wire mul_r0_n_145;
  wire mul_r0_n_146;
  wire mul_r0_n_147;
  wire mul_r0_n_148;
  wire mul_r0_n_149;
  wire mul_r0_n_150;
  wire mul_r0_n_151;
  wire mul_r0_n_152;
  wire mul_r0_n_153;
  wire mul_r0_n_58;
  wire mul_r0_n_59;
  wire mul_r0_n_60;
  wire mul_r0_n_61;
  wire mul_r0_n_62;
  wire mul_r0_n_63;
  wire mul_r0_n_64;
  wire mul_r0_n_65;
  wire mul_r0_n_66;
  wire mul_r0_n_67;
  wire mul_r0_n_68;
  wire mul_r0_n_69;
  wire mul_r0_n_70;
  wire mul_r0_n_71;
  wire mul_r0_n_72;
  wire mul_r0_n_73;
  wire mul_r0_n_74;
  wire mul_r0_n_75;
  wire mul_r0_n_76;
  wire mul_r0_n_77;
  wire mul_r0_n_78;
  wire mul_r0_n_79;
  wire mul_r0_n_80;
  wire mul_r0_n_81;
  wire mul_r0_n_82;
  wire mul_r0_n_83;
  wire mul_r0_n_84;
  wire mul_r0_n_85;
  wire mul_r0_n_86;
  wire mul_r0_n_87;
  wire mul_r0_n_88;
  wire mul_r0_n_89;
  wire mul_r0_n_90;
  wire mul_r0_n_91;
  wire mul_r0_n_92;
  wire mul_r0_n_93;
  wire mul_r0_n_94;
  wire mul_r0_n_95;
  wire mul_r0_n_96;
  wire mul_r0_n_97;
  wire mul_r0_n_98;
  wire mul_r0_n_99;
  wire mul_r_reg__0_n_58;
  wire mul_r_reg__0_n_59;
  wire mul_r_reg__0_n_60;
  wire mul_r_reg__0_n_61;
  wire mul_r_reg__0_n_62;
  wire mul_r_reg__0_n_63;
  wire mul_r_reg__0_n_64;
  wire mul_r_reg__0_n_65;
  wire mul_r_reg__0_n_66;
  wire mul_r_reg__0_n_67;
  wire mul_r_reg__0_n_68;
  wire mul_r_reg__0_n_69;
  wire mul_r_reg__0_n_70;
  wire mul_r_reg__0_n_71;
  wire mul_r_reg__0_n_72;
  wire mul_r_reg__0_n_73;
  wire mul_r_reg__0_n_74;
  wire [47:0]mul_r_reg__1;
  wire [0:0]\slv_regs_reg[0][0] ;
  wire [1:0]\slv_regs_reg[0][11] ;
  wire [4:0]\slv_regs_reg[0][8] ;
  wire [3:0]NLW_add_r1_carry_O_UNCONNECTED;
  wire [3:1]NLW_add_r1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_add_r1_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_add_r_reg[44]_i_1__0_CO_UNCONNECTED ;
  wire NLW_mul_r0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r0_CARRYOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r_reg__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r_reg__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_r_reg__0_PCOUT_UNCONNECTED;

  CARRY4 add_r1_carry
       (.CI(1'b0),
        .CO({add_r1_carry_n_0,add_r1_carry_n_1,add_r1_carry_n_2,add_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry_O_UNCONNECTED[3:0]),
        .S({\slv_regs_reg[0][11] [1],add_r1_carry_i_2__0_n_0,\slv_regs_reg[0][11] [0],add_r1_carry_i_4__0_n_0}));
  CARRY4 add_r1_carry__0
       (.CI(add_r1_carry_n_0),
        .CO({NLW_add_r1_carry__0_CO_UNCONNECTED[3:1],CO}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,O}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_2__0
       (.I0(\counter_reg_n_0_[8] ),
        .I1(\slv_regs_reg[0][8] [4]),
        .I2(\counter_reg_n_0_[6] ),
        .I3(\slv_regs_reg[0][8] [2]),
        .I4(\slv_regs_reg[0][8] [3]),
        .I5(\counter_reg_n_0_[7] ),
        .O(add_r1_carry_i_2__0_n_0));
  LUT6 #(
    .INIT(64'h6006000000006006)) 
    add_r1_carry_i_4__0
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\slv_regs_reg[0][0] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\slv_regs_reg[0][8] [1]),
        .I4(\counter_reg_n_0_[1] ),
        .I5(\slv_regs_reg[0][8] [0]),
        .O(add_r1_carry_i_4__0_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_2__0 
       (.I0(mul_r_reg__1[3]),
        .I1(CO),
        .O(\add_r[0]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_3__0 
       (.I0(mul_r_reg__1[2]),
        .I1(CO),
        .O(\add_r[0]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_4__0 
       (.I0(mul_r_reg__1[1]),
        .I1(CO),
        .O(\add_r[0]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_5__0 
       (.I0(mul_r_reg__1[0]),
        .I1(CO),
        .O(\add_r[0]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_6__0 
       (.I0(D[3]),
        .I1(CO),
        .I2(mul_r_reg__1[3]),
        .O(\add_r[0]_i_6__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_7__0 
       (.I0(D[2]),
        .I1(CO),
        .I2(mul_r_reg__1[2]),
        .O(\add_r[0]_i_7__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_8__0 
       (.I0(D[1]),
        .I1(CO),
        .I2(mul_r_reg__1[1]),
        .O(\add_r[0]_i_8__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_9__0 
       (.I0(D[0]),
        .I1(CO),
        .I2(mul_r_reg__1[0]),
        .O(\add_r[0]_i_9__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_2__0 
       (.I0(mul_r_reg__1[15]),
        .I1(CO),
        .O(\add_r[12]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_3__0 
       (.I0(mul_r_reg__1[14]),
        .I1(CO),
        .O(\add_r[12]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_4__0 
       (.I0(mul_r_reg__1[13]),
        .I1(CO),
        .O(\add_r[12]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_5__0 
       (.I0(mul_r_reg__1[12]),
        .I1(CO),
        .O(\add_r[12]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_6__0 
       (.I0(D[15]),
        .I1(CO),
        .I2(mul_r_reg__1[15]),
        .O(\add_r[12]_i_6__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_7__0 
       (.I0(D[14]),
        .I1(CO),
        .I2(mul_r_reg__1[14]),
        .O(\add_r[12]_i_7__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_8__0 
       (.I0(D[13]),
        .I1(CO),
        .I2(mul_r_reg__1[13]),
        .O(\add_r[12]_i_8__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_9__0 
       (.I0(D[12]),
        .I1(CO),
        .I2(mul_r_reg__1[12]),
        .O(\add_r[12]_i_9__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_2__0 
       (.I0(mul_r_reg__1[19]),
        .I1(CO),
        .O(\add_r[16]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_3__0 
       (.I0(mul_r_reg__1[18]),
        .I1(CO),
        .O(\add_r[16]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_4__0 
       (.I0(mul_r_reg__1[17]),
        .I1(CO),
        .O(\add_r[16]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_5__0 
       (.I0(mul_r_reg__1[16]),
        .I1(CO),
        .O(\add_r[16]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_6__0 
       (.I0(D[19]),
        .I1(CO),
        .I2(mul_r_reg__1[19]),
        .O(\add_r[16]_i_6__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_7__0 
       (.I0(D[18]),
        .I1(CO),
        .I2(mul_r_reg__1[18]),
        .O(\add_r[16]_i_7__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_8__0 
       (.I0(D[17]),
        .I1(CO),
        .I2(mul_r_reg__1[17]),
        .O(\add_r[16]_i_8__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_9__0 
       (.I0(D[16]),
        .I1(CO),
        .I2(mul_r_reg__1[16]),
        .O(\add_r[16]_i_9__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_2__0 
       (.I0(mul_r_reg__1[23]),
        .I1(CO),
        .O(\add_r[20]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_3__0 
       (.I0(mul_r_reg__1[22]),
        .I1(CO),
        .O(\add_r[20]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_4__0 
       (.I0(mul_r_reg__1[21]),
        .I1(CO),
        .O(\add_r[20]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_5__0 
       (.I0(mul_r_reg__1[20]),
        .I1(CO),
        .O(\add_r[20]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_6__0 
       (.I0(D[23]),
        .I1(CO),
        .I2(mul_r_reg__1[23]),
        .O(\add_r[20]_i_6__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_7__0 
       (.I0(D[22]),
        .I1(CO),
        .I2(mul_r_reg__1[22]),
        .O(\add_r[20]_i_7__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_8__0 
       (.I0(D[21]),
        .I1(CO),
        .I2(mul_r_reg__1[21]),
        .O(\add_r[20]_i_8__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_9__0 
       (.I0(D[20]),
        .I1(CO),
        .I2(mul_r_reg__1[20]),
        .O(\add_r[20]_i_9__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_2__0 
       (.I0(mul_r_reg__1[27]),
        .I1(CO),
        .O(\add_r[24]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_3__0 
       (.I0(mul_r_reg__1[26]),
        .I1(CO),
        .O(\add_r[24]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_4__0 
       (.I0(mul_r_reg__1[25]),
        .I1(CO),
        .O(\add_r[24]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_5__0 
       (.I0(mul_r_reg__1[24]),
        .I1(CO),
        .O(\add_r[24]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_6__0 
       (.I0(D[27]),
        .I1(CO),
        .I2(mul_r_reg__1[27]),
        .O(\add_r[24]_i_6__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_7__0 
       (.I0(D[26]),
        .I1(CO),
        .I2(mul_r_reg__1[26]),
        .O(\add_r[24]_i_7__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_8__0 
       (.I0(D[25]),
        .I1(CO),
        .I2(mul_r_reg__1[25]),
        .O(\add_r[24]_i_8__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_9__0 
       (.I0(D[24]),
        .I1(CO),
        .I2(mul_r_reg__1[24]),
        .O(\add_r[24]_i_9__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_2__0 
       (.I0(mul_r_reg__1[31]),
        .I1(CO),
        .O(\add_r[28]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_3__0 
       (.I0(mul_r_reg__1[30]),
        .I1(CO),
        .O(\add_r[28]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_4__0 
       (.I0(mul_r_reg__1[29]),
        .I1(CO),
        .O(\add_r[28]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_5__0 
       (.I0(mul_r_reg__1[28]),
        .I1(CO),
        .O(\add_r[28]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_6__0 
       (.I0(D[31]),
        .I1(CO),
        .I2(mul_r_reg__1[31]),
        .O(\add_r[28]_i_6__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_7__0 
       (.I0(D[30]),
        .I1(CO),
        .I2(mul_r_reg__1[30]),
        .O(\add_r[28]_i_7__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_8__0 
       (.I0(D[29]),
        .I1(CO),
        .I2(mul_r_reg__1[29]),
        .O(\add_r[28]_i_8__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_9__0 
       (.I0(D[28]),
        .I1(CO),
        .I2(mul_r_reg__1[28]),
        .O(\add_r[28]_i_9__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_2__0 
       (.I0(mul_r_reg__1[35]),
        .I1(CO),
        .O(\add_r[32]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_3__0 
       (.I0(mul_r_reg__1[34]),
        .I1(CO),
        .O(\add_r[32]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_4__0 
       (.I0(mul_r_reg__1[33]),
        .I1(CO),
        .O(\add_r[32]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_5__0 
       (.I0(mul_r_reg__1[32]),
        .I1(CO),
        .O(\add_r[32]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_6__0 
       (.I0(D[35]),
        .I1(CO),
        .I2(mul_r_reg__1[35]),
        .O(\add_r[32]_i_6__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_7__0 
       (.I0(D[34]),
        .I1(CO),
        .I2(mul_r_reg__1[34]),
        .O(\add_r[32]_i_7__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_8__0 
       (.I0(D[33]),
        .I1(CO),
        .I2(mul_r_reg__1[33]),
        .O(\add_r[32]_i_8__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_9__0 
       (.I0(D[32]),
        .I1(CO),
        .I2(mul_r_reg__1[32]),
        .O(\add_r[32]_i_9__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_2__0 
       (.I0(mul_r_reg__1[39]),
        .I1(CO),
        .O(\add_r[36]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_3__0 
       (.I0(mul_r_reg__1[38]),
        .I1(CO),
        .O(\add_r[36]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_4__0 
       (.I0(mul_r_reg__1[37]),
        .I1(CO),
        .O(\add_r[36]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_5__0 
       (.I0(mul_r_reg__1[36]),
        .I1(CO),
        .O(\add_r[36]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_6__0 
       (.I0(D[39]),
        .I1(CO),
        .I2(mul_r_reg__1[39]),
        .O(\add_r[36]_i_6__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_7__0 
       (.I0(D[38]),
        .I1(CO),
        .I2(mul_r_reg__1[38]),
        .O(\add_r[36]_i_7__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_8__0 
       (.I0(D[37]),
        .I1(CO),
        .I2(mul_r_reg__1[37]),
        .O(\add_r[36]_i_8__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_9__0 
       (.I0(D[36]),
        .I1(CO),
        .I2(mul_r_reg__1[36]),
        .O(\add_r[36]_i_9__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_2__0 
       (.I0(mul_r_reg__1[43]),
        .I1(CO),
        .O(\add_r[40]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_3__0 
       (.I0(mul_r_reg__1[42]),
        .I1(CO),
        .O(\add_r[40]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_4__0 
       (.I0(mul_r_reg__1[41]),
        .I1(CO),
        .O(\add_r[40]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_5__0 
       (.I0(mul_r_reg__1[40]),
        .I1(CO),
        .O(\add_r[40]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_6__0 
       (.I0(D[43]),
        .I1(CO),
        .I2(mul_r_reg__1[43]),
        .O(\add_r[40]_i_6__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_7__0 
       (.I0(D[42]),
        .I1(CO),
        .I2(mul_r_reg__1[42]),
        .O(\add_r[40]_i_7__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_8__0 
       (.I0(D[41]),
        .I1(CO),
        .I2(mul_r_reg__1[41]),
        .O(\add_r[40]_i_8__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_9__0 
       (.I0(D[40]),
        .I1(CO),
        .I2(mul_r_reg__1[40]),
        .O(\add_r[40]_i_9__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_2__0 
       (.I0(mul_r_reg__1[46]),
        .I1(CO),
        .O(\add_r[44]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_3__0 
       (.I0(mul_r_reg__1[45]),
        .I1(CO),
        .O(\add_r[44]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_4__0 
       (.I0(mul_r_reg__1[44]),
        .I1(CO),
        .O(\add_r[44]_i_4__0_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_5__0 
       (.I0(mul_r_reg__1[47]),
        .I1(CO),
        .I2(D[47]),
        .O(\add_r[44]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_6__0 
       (.I0(D[46]),
        .I1(CO),
        .I2(mul_r_reg__1[46]),
        .O(\add_r[44]_i_6__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_7__0 
       (.I0(D[45]),
        .I1(CO),
        .I2(mul_r_reg__1[45]),
        .O(\add_r[44]_i_7__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_8__0 
       (.I0(D[44]),
        .I1(CO),
        .I2(mul_r_reg__1[44]),
        .O(\add_r[44]_i_8__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_2__0 
       (.I0(mul_r_reg__1[7]),
        .I1(CO),
        .O(\add_r[4]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_3__0 
       (.I0(mul_r_reg__1[6]),
        .I1(CO),
        .O(\add_r[4]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_4__0 
       (.I0(mul_r_reg__1[5]),
        .I1(CO),
        .O(\add_r[4]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_5__0 
       (.I0(mul_r_reg__1[4]),
        .I1(CO),
        .O(\add_r[4]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_6__0 
       (.I0(D[7]),
        .I1(CO),
        .I2(mul_r_reg__1[7]),
        .O(\add_r[4]_i_6__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_7__0 
       (.I0(D[6]),
        .I1(CO),
        .I2(mul_r_reg__1[6]),
        .O(\add_r[4]_i_7__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_8__0 
       (.I0(D[5]),
        .I1(CO),
        .I2(mul_r_reg__1[5]),
        .O(\add_r[4]_i_8__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_9__0 
       (.I0(D[4]),
        .I1(CO),
        .I2(mul_r_reg__1[4]),
        .O(\add_r[4]_i_9__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_2__0 
       (.I0(mul_r_reg__1[11]),
        .I1(CO),
        .O(\add_r[8]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_3__0 
       (.I0(mul_r_reg__1[10]),
        .I1(CO),
        .O(\add_r[8]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_4__0 
       (.I0(mul_r_reg__1[9]),
        .I1(CO),
        .O(\add_r[8]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_5__0 
       (.I0(mul_r_reg__1[8]),
        .I1(CO),
        .O(\add_r[8]_i_5__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_6__0 
       (.I0(D[11]),
        .I1(CO),
        .I2(mul_r_reg__1[11]),
        .O(\add_r[8]_i_6__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_7__0 
       (.I0(D[10]),
        .I1(CO),
        .I2(mul_r_reg__1[10]),
        .O(\add_r[8]_i_7__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_8__0 
       (.I0(D[9]),
        .I1(CO),
        .I2(mul_r_reg__1[9]),
        .O(\add_r[8]_i_8__0_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_9__0 
       (.I0(D[8]),
        .I1(CO),
        .I2(mul_r_reg__1[8]),
        .O(\add_r[8]_i_9__0_n_0 ));
  FDRE \add_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__0_n_7 ),
        .Q(D[0]),
        .R(aresetn));
  CARRY4 \add_r_reg[0]_i_1__0 
       (.CI(1'b0),
        .CO({\add_r_reg[0]_i_1__0_n_0 ,\add_r_reg[0]_i_1__0_n_1 ,\add_r_reg[0]_i_1__0_n_2 ,\add_r_reg[0]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[0]_i_2__0_n_0 ,\add_r[0]_i_3__0_n_0 ,\add_r[0]_i_4__0_n_0 ,\add_r[0]_i_5__0_n_0 }),
        .O({\add_r_reg[0]_i_1__0_n_4 ,\add_r_reg[0]_i_1__0_n_5 ,\add_r_reg[0]_i_1__0_n_6 ,\add_r_reg[0]_i_1__0_n_7 }),
        .S({\add_r[0]_i_6__0_n_0 ,\add_r[0]_i_7__0_n_0 ,\add_r[0]_i_8__0_n_0 ,\add_r[0]_i_9__0_n_0 }));
  FDRE \add_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__0_n_5 ),
        .Q(D[10]),
        .R(aresetn));
  FDRE \add_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__0_n_4 ),
        .Q(D[11]),
        .R(aresetn));
  FDRE \add_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__0_n_7 ),
        .Q(D[12]),
        .R(aresetn));
  CARRY4 \add_r_reg[12]_i_1__0 
       (.CI(\add_r_reg[8]_i_1__0_n_0 ),
        .CO({\add_r_reg[12]_i_1__0_n_0 ,\add_r_reg[12]_i_1__0_n_1 ,\add_r_reg[12]_i_1__0_n_2 ,\add_r_reg[12]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[12]_i_2__0_n_0 ,\add_r[12]_i_3__0_n_0 ,\add_r[12]_i_4__0_n_0 ,\add_r[12]_i_5__0_n_0 }),
        .O({\add_r_reg[12]_i_1__0_n_4 ,\add_r_reg[12]_i_1__0_n_5 ,\add_r_reg[12]_i_1__0_n_6 ,\add_r_reg[12]_i_1__0_n_7 }),
        .S({\add_r[12]_i_6__0_n_0 ,\add_r[12]_i_7__0_n_0 ,\add_r[12]_i_8__0_n_0 ,\add_r[12]_i_9__0_n_0 }));
  FDRE \add_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__0_n_6 ),
        .Q(D[13]),
        .R(aresetn));
  FDRE \add_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__0_n_5 ),
        .Q(D[14]),
        .R(aresetn));
  FDRE \add_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__0_n_4 ),
        .Q(D[15]),
        .R(aresetn));
  FDRE \add_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__0_n_7 ),
        .Q(D[16]),
        .R(aresetn));
  CARRY4 \add_r_reg[16]_i_1__0 
       (.CI(\add_r_reg[12]_i_1__0_n_0 ),
        .CO({\add_r_reg[16]_i_1__0_n_0 ,\add_r_reg[16]_i_1__0_n_1 ,\add_r_reg[16]_i_1__0_n_2 ,\add_r_reg[16]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[16]_i_2__0_n_0 ,\add_r[16]_i_3__0_n_0 ,\add_r[16]_i_4__0_n_0 ,\add_r[16]_i_5__0_n_0 }),
        .O({\add_r_reg[16]_i_1__0_n_4 ,\add_r_reg[16]_i_1__0_n_5 ,\add_r_reg[16]_i_1__0_n_6 ,\add_r_reg[16]_i_1__0_n_7 }),
        .S({\add_r[16]_i_6__0_n_0 ,\add_r[16]_i_7__0_n_0 ,\add_r[16]_i_8__0_n_0 ,\add_r[16]_i_9__0_n_0 }));
  FDRE \add_r_reg[17] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__0_n_6 ),
        .Q(D[17]),
        .R(aresetn));
  FDRE \add_r_reg[18] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__0_n_5 ),
        .Q(D[18]),
        .R(aresetn));
  FDRE \add_r_reg[19] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__0_n_4 ),
        .Q(D[19]),
        .R(aresetn));
  FDRE \add_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__0_n_6 ),
        .Q(D[1]),
        .R(aresetn));
  FDRE \add_r_reg[20] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__0_n_7 ),
        .Q(D[20]),
        .R(aresetn));
  CARRY4 \add_r_reg[20]_i_1__0 
       (.CI(\add_r_reg[16]_i_1__0_n_0 ),
        .CO({\add_r_reg[20]_i_1__0_n_0 ,\add_r_reg[20]_i_1__0_n_1 ,\add_r_reg[20]_i_1__0_n_2 ,\add_r_reg[20]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[20]_i_2__0_n_0 ,\add_r[20]_i_3__0_n_0 ,\add_r[20]_i_4__0_n_0 ,\add_r[20]_i_5__0_n_0 }),
        .O({\add_r_reg[20]_i_1__0_n_4 ,\add_r_reg[20]_i_1__0_n_5 ,\add_r_reg[20]_i_1__0_n_6 ,\add_r_reg[20]_i_1__0_n_7 }),
        .S({\add_r[20]_i_6__0_n_0 ,\add_r[20]_i_7__0_n_0 ,\add_r[20]_i_8__0_n_0 ,\add_r[20]_i_9__0_n_0 }));
  FDRE \add_r_reg[21] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__0_n_6 ),
        .Q(D[21]),
        .R(aresetn));
  FDRE \add_r_reg[22] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__0_n_5 ),
        .Q(D[22]),
        .R(aresetn));
  FDRE \add_r_reg[23] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__0_n_4 ),
        .Q(D[23]),
        .R(aresetn));
  FDRE \add_r_reg[24] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__0_n_7 ),
        .Q(D[24]),
        .R(aresetn));
  CARRY4 \add_r_reg[24]_i_1__0 
       (.CI(\add_r_reg[20]_i_1__0_n_0 ),
        .CO({\add_r_reg[24]_i_1__0_n_0 ,\add_r_reg[24]_i_1__0_n_1 ,\add_r_reg[24]_i_1__0_n_2 ,\add_r_reg[24]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[24]_i_2__0_n_0 ,\add_r[24]_i_3__0_n_0 ,\add_r[24]_i_4__0_n_0 ,\add_r[24]_i_5__0_n_0 }),
        .O({\add_r_reg[24]_i_1__0_n_4 ,\add_r_reg[24]_i_1__0_n_5 ,\add_r_reg[24]_i_1__0_n_6 ,\add_r_reg[24]_i_1__0_n_7 }),
        .S({\add_r[24]_i_6__0_n_0 ,\add_r[24]_i_7__0_n_0 ,\add_r[24]_i_8__0_n_0 ,\add_r[24]_i_9__0_n_0 }));
  FDRE \add_r_reg[25] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__0_n_6 ),
        .Q(D[25]),
        .R(aresetn));
  FDRE \add_r_reg[26] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__0_n_5 ),
        .Q(D[26]),
        .R(aresetn));
  FDRE \add_r_reg[27] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__0_n_4 ),
        .Q(D[27]),
        .R(aresetn));
  FDRE \add_r_reg[28] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__0_n_7 ),
        .Q(D[28]),
        .R(aresetn));
  CARRY4 \add_r_reg[28]_i_1__0 
       (.CI(\add_r_reg[24]_i_1__0_n_0 ),
        .CO({\add_r_reg[28]_i_1__0_n_0 ,\add_r_reg[28]_i_1__0_n_1 ,\add_r_reg[28]_i_1__0_n_2 ,\add_r_reg[28]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[28]_i_2__0_n_0 ,\add_r[28]_i_3__0_n_0 ,\add_r[28]_i_4__0_n_0 ,\add_r[28]_i_5__0_n_0 }),
        .O({\add_r_reg[28]_i_1__0_n_4 ,\add_r_reg[28]_i_1__0_n_5 ,\add_r_reg[28]_i_1__0_n_6 ,\add_r_reg[28]_i_1__0_n_7 }),
        .S({\add_r[28]_i_6__0_n_0 ,\add_r[28]_i_7__0_n_0 ,\add_r[28]_i_8__0_n_0 ,\add_r[28]_i_9__0_n_0 }));
  FDRE \add_r_reg[29] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__0_n_6 ),
        .Q(D[29]),
        .R(aresetn));
  FDRE \add_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__0_n_5 ),
        .Q(D[2]),
        .R(aresetn));
  FDRE \add_r_reg[30] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__0_n_5 ),
        .Q(D[30]),
        .R(aresetn));
  FDRE \add_r_reg[31] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__0_n_4 ),
        .Q(D[31]),
        .R(aresetn));
  FDRE \add_r_reg[32] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__0_n_7 ),
        .Q(D[32]),
        .R(aresetn));
  CARRY4 \add_r_reg[32]_i_1__0 
       (.CI(\add_r_reg[28]_i_1__0_n_0 ),
        .CO({\add_r_reg[32]_i_1__0_n_0 ,\add_r_reg[32]_i_1__0_n_1 ,\add_r_reg[32]_i_1__0_n_2 ,\add_r_reg[32]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[32]_i_2__0_n_0 ,\add_r[32]_i_3__0_n_0 ,\add_r[32]_i_4__0_n_0 ,\add_r[32]_i_5__0_n_0 }),
        .O({\add_r_reg[32]_i_1__0_n_4 ,\add_r_reg[32]_i_1__0_n_5 ,\add_r_reg[32]_i_1__0_n_6 ,\add_r_reg[32]_i_1__0_n_7 }),
        .S({\add_r[32]_i_6__0_n_0 ,\add_r[32]_i_7__0_n_0 ,\add_r[32]_i_8__0_n_0 ,\add_r[32]_i_9__0_n_0 }));
  FDRE \add_r_reg[33] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__0_n_6 ),
        .Q(D[33]),
        .R(aresetn));
  FDRE \add_r_reg[34] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__0_n_5 ),
        .Q(D[34]),
        .R(aresetn));
  FDRE \add_r_reg[35] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__0_n_4 ),
        .Q(D[35]),
        .R(aresetn));
  FDRE \add_r_reg[36] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__0_n_7 ),
        .Q(D[36]),
        .R(aresetn));
  CARRY4 \add_r_reg[36]_i_1__0 
       (.CI(\add_r_reg[32]_i_1__0_n_0 ),
        .CO({\add_r_reg[36]_i_1__0_n_0 ,\add_r_reg[36]_i_1__0_n_1 ,\add_r_reg[36]_i_1__0_n_2 ,\add_r_reg[36]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[36]_i_2__0_n_0 ,\add_r[36]_i_3__0_n_0 ,\add_r[36]_i_4__0_n_0 ,\add_r[36]_i_5__0_n_0 }),
        .O({\add_r_reg[36]_i_1__0_n_4 ,\add_r_reg[36]_i_1__0_n_5 ,\add_r_reg[36]_i_1__0_n_6 ,\add_r_reg[36]_i_1__0_n_7 }),
        .S({\add_r[36]_i_6__0_n_0 ,\add_r[36]_i_7__0_n_0 ,\add_r[36]_i_8__0_n_0 ,\add_r[36]_i_9__0_n_0 }));
  FDRE \add_r_reg[37] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__0_n_6 ),
        .Q(D[37]),
        .R(aresetn));
  FDRE \add_r_reg[38] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__0_n_5 ),
        .Q(D[38]),
        .R(aresetn));
  FDRE \add_r_reg[39] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__0_n_4 ),
        .Q(D[39]),
        .R(aresetn));
  FDRE \add_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__0_n_4 ),
        .Q(D[3]),
        .R(aresetn));
  FDRE \add_r_reg[40] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__0_n_7 ),
        .Q(D[40]),
        .R(aresetn));
  CARRY4 \add_r_reg[40]_i_1__0 
       (.CI(\add_r_reg[36]_i_1__0_n_0 ),
        .CO({\add_r_reg[40]_i_1__0_n_0 ,\add_r_reg[40]_i_1__0_n_1 ,\add_r_reg[40]_i_1__0_n_2 ,\add_r_reg[40]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[40]_i_2__0_n_0 ,\add_r[40]_i_3__0_n_0 ,\add_r[40]_i_4__0_n_0 ,\add_r[40]_i_5__0_n_0 }),
        .O({\add_r_reg[40]_i_1__0_n_4 ,\add_r_reg[40]_i_1__0_n_5 ,\add_r_reg[40]_i_1__0_n_6 ,\add_r_reg[40]_i_1__0_n_7 }),
        .S({\add_r[40]_i_6__0_n_0 ,\add_r[40]_i_7__0_n_0 ,\add_r[40]_i_8__0_n_0 ,\add_r[40]_i_9__0_n_0 }));
  FDRE \add_r_reg[41] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__0_n_6 ),
        .Q(D[41]),
        .R(aresetn));
  FDRE \add_r_reg[42] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__0_n_5 ),
        .Q(D[42]),
        .R(aresetn));
  FDRE \add_r_reg[43] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__0_n_4 ),
        .Q(D[43]),
        .R(aresetn));
  FDRE \add_r_reg[44] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__0_n_7 ),
        .Q(D[44]),
        .R(aresetn));
  CARRY4 \add_r_reg[44]_i_1__0 
       (.CI(\add_r_reg[40]_i_1__0_n_0 ),
        .CO({\NLW_add_r_reg[44]_i_1__0_CO_UNCONNECTED [3],\add_r_reg[44]_i_1__0_n_1 ,\add_r_reg[44]_i_1__0_n_2 ,\add_r_reg[44]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\add_r[44]_i_2__0_n_0 ,\add_r[44]_i_3__0_n_0 ,\add_r[44]_i_4__0_n_0 }),
        .O({\add_r_reg[44]_i_1__0_n_4 ,\add_r_reg[44]_i_1__0_n_5 ,\add_r_reg[44]_i_1__0_n_6 ,\add_r_reg[44]_i_1__0_n_7 }),
        .S({\add_r[44]_i_5__0_n_0 ,\add_r[44]_i_6__0_n_0 ,\add_r[44]_i_7__0_n_0 ,\add_r[44]_i_8__0_n_0 }));
  FDRE \add_r_reg[45] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__0_n_6 ),
        .Q(D[45]),
        .R(aresetn));
  FDRE \add_r_reg[46] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__0_n_5 ),
        .Q(D[46]),
        .R(aresetn));
  FDRE \add_r_reg[47] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__0_n_4 ),
        .Q(D[47]),
        .R(aresetn));
  FDRE \add_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__0_n_7 ),
        .Q(D[4]),
        .R(aresetn));
  CARRY4 \add_r_reg[4]_i_1__0 
       (.CI(\add_r_reg[0]_i_1__0_n_0 ),
        .CO({\add_r_reg[4]_i_1__0_n_0 ,\add_r_reg[4]_i_1__0_n_1 ,\add_r_reg[4]_i_1__0_n_2 ,\add_r_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[4]_i_2__0_n_0 ,\add_r[4]_i_3__0_n_0 ,\add_r[4]_i_4__0_n_0 ,\add_r[4]_i_5__0_n_0 }),
        .O({\add_r_reg[4]_i_1__0_n_4 ,\add_r_reg[4]_i_1__0_n_5 ,\add_r_reg[4]_i_1__0_n_6 ,\add_r_reg[4]_i_1__0_n_7 }),
        .S({\add_r[4]_i_6__0_n_0 ,\add_r[4]_i_7__0_n_0 ,\add_r[4]_i_8__0_n_0 ,\add_r[4]_i_9__0_n_0 }));
  FDRE \add_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__0_n_6 ),
        .Q(D[5]),
        .R(aresetn));
  FDRE \add_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__0_n_5 ),
        .Q(D[6]),
        .R(aresetn));
  FDRE \add_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__0_n_4 ),
        .Q(D[7]),
        .R(aresetn));
  FDRE \add_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__0_n_7 ),
        .Q(D[8]),
        .R(aresetn));
  CARRY4 \add_r_reg[8]_i_1__0 
       (.CI(\add_r_reg[4]_i_1__0_n_0 ),
        .CO({\add_r_reg[8]_i_1__0_n_0 ,\add_r_reg[8]_i_1__0_n_1 ,\add_r_reg[8]_i_1__0_n_2 ,\add_r_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[8]_i_2__0_n_0 ,\add_r[8]_i_3__0_n_0 ,\add_r[8]_i_4__0_n_0 ,\add_r[8]_i_5__0_n_0 }),
        .O({\add_r_reg[8]_i_1__0_n_4 ,\add_r_reg[8]_i_1__0_n_5 ,\add_r_reg[8]_i_1__0_n_6 ,\add_r_reg[8]_i_1__0_n_7 }),
        .S({\add_r[8]_i_6__0_n_0 ,\add_r[8]_i_7__0_n_0 ,\add_r[8]_i_8__0_n_0 ,\add_r[8]_i_9__0_n_0 }));
  FDRE \add_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__0_n_6 ),
        .Q(D[9]),
        .R(aresetn));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter[0]_i_1__3 
       (.I0(\counter_reg_n_0_[0] ),
        .O(\counter[0]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT4 #(
    .INIT(16'hFD0E)) 
    \counter[1]_i_1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(CO),
        .I2(dp_extend_end_reg_0),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \counter[2]_i_1__2 
       (.I0(\counter_reg_n_0_[2] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[1] ),
        .O(\counter[2]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \counter[3]_i_1__2 
       (.I0(Q[0]),
        .I1(\counter_reg_n_0_[2] ),
        .I2(\counter_reg_n_0_[0] ),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[3]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \counter[4]_i_1 
       (.I0(Q[1]),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[0] ),
        .I3(\counter_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(\counter[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \counter[5]_i_1__1 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[1] ),
        .I5(Q[1]),
        .O(\counter[5]_i_1__1_n_0 ));
  LUT5 #(
    .INIT(32'hA6AAAAAA)) 
    \counter[6]_i_1__0 
       (.I0(\counter_reg_n_0_[6] ),
        .I1(Q[1]),
        .I2(\counter[7]_i_2_n_0 ),
        .I3(Q[0]),
        .I4(Q[2]),
        .O(\counter[6]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hAA6AAAAAAAAAAAAA)) 
    \counter[7]_i_1__0 
       (.I0(\counter_reg_n_0_[7] ),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(\counter[7]_i_2_n_0 ),
        .I4(Q[1]),
        .I5(\counter_reg_n_0_[6] ),
        .O(\counter[7]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \counter[7]_i_2 
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[2] ),
        .O(\counter[7]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h6AAA)) 
    \counter[8]_i_2__1 
       (.I0(\counter_reg_n_0_[8] ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(\counter[8]_i_3__1_n_0 ),
        .I3(\counter_reg_n_0_[7] ),
        .O(\counter[8]_i_2__1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter[8]_i_3__1 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[1] ),
        .I5(Q[1]),
        .O(\counter[8]_i_3__1_n_0 ));
  FDRE \counter_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[0]_i_1__3_n_0 ),
        .Q(\counter_reg_n_0_[0] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\counter[1]_i_1_n_0 ),
        .Q(\counter_reg_n_0_[1] ),
        .R(aresetn));
  FDRE \counter_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[2]_i_1__2_n_0 ),
        .Q(\counter_reg_n_0_[2] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[3]_i_1__2_n_0 ),
        .Q(Q[0]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[4]_i_1_n_0 ),
        .Q(Q[1]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[5]_i_1__1_n_0 ),
        .Q(Q[2]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[6]_i_1__0_n_0 ),
        .Q(\counter_reg_n_0_[6] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[7]_i_1__0_n_0 ),
        .Q(\counter_reg_n_0_[7] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[8]_i_2__1_n_0 ),
        .Q(\counter_reg_n_0_[8] ),
        .R(dp_extend_end_reg));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,b_ram_out[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({A[15],A[15],A}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r0_OVERFLOW_UNCONNECTED),
        .P({mul_r0_n_58,mul_r0_n_59,mul_r0_n_60,mul_r0_n_61,mul_r0_n_62,mul_r0_n_63,mul_r0_n_64,mul_r0_n_65,mul_r0_n_66,mul_r0_n_67,mul_r0_n_68,mul_r0_n_69,mul_r0_n_70,mul_r0_n_71,mul_r0_n_72,mul_r0_n_73,mul_r0_n_74,mul_r0_n_75,mul_r0_n_76,mul_r0_n_77,mul_r0_n_78,mul_r0_n_79,mul_r0_n_80,mul_r0_n_81,mul_r0_n_82,mul_r0_n_83,mul_r0_n_84,mul_r0_n_85,mul_r0_n_86,mul_r0_n_87,mul_r0_n_88,mul_r0_n_89,mul_r0_n_90,mul_r0_n_91,mul_r0_n_92,mul_r0_n_93,mul_r0_n_94,mul_r0_n_95,mul_r0_n_96,mul_r0_n_97,mul_r0_n_98,mul_r0_n_99,mul_r0_n_100,mul_r0_n_101,mul_r0_n_102,mul_r0_n_103,mul_r0_n_104,mul_r0_n_105}),
        .PATTERNBDETECT(NLW_mul_r0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_r0_UNDERFLOW_UNCONNECTED));
  FDRE \mul_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_105),
        .Q(mul_r_reg__1[0]),
        .R(aresetn));
  FDRE \mul_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_95),
        .Q(mul_r_reg__1[10]),
        .R(aresetn));
  FDRE \mul_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_94),
        .Q(mul_r_reg__1[11]),
        .R(aresetn));
  FDRE \mul_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_93),
        .Q(mul_r_reg__1[12]),
        .R(aresetn));
  FDRE \mul_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_92),
        .Q(mul_r_reg__1[13]),
        .R(aresetn));
  FDRE \mul_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_91),
        .Q(mul_r_reg__1[14]),
        .R(aresetn));
  FDRE \mul_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_90),
        .Q(mul_r_reg__1[15]),
        .R(aresetn));
  FDRE \mul_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_89),
        .Q(mul_r_reg__1[16]),
        .R(aresetn));
  FDRE \mul_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_104),
        .Q(mul_r_reg__1[1]),
        .R(aresetn));
  FDRE \mul_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_103),
        .Q(mul_r_reg__1[2]),
        .R(aresetn));
  FDRE \mul_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_102),
        .Q(mul_r_reg__1[3]),
        .R(aresetn));
  FDRE \mul_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_101),
        .Q(mul_r_reg__1[4]),
        .R(aresetn));
  FDRE \mul_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_100),
        .Q(mul_r_reg__1[5]),
        .R(aresetn));
  FDRE \mul_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_99),
        .Q(mul_r_reg__1[6]),
        .R(aresetn));
  FDRE \mul_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_98),
        .Q(mul_r_reg__1[7]),
        .R(aresetn));
  FDRE \mul_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_97),
        .Q(mul_r_reg__1[8]),
        .R(aresetn));
  FDRE \mul_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_96),
        .Q(mul_r_reg__1[9]),
        .R(aresetn));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-12 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r_reg__0
       (.A({A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r_reg__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({b_ram_out[31],b_ram_out[31],b_ram_out[31],b_ram_out[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r_reg__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(dp_enable),
        .CLK(clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED),
        .P({mul_r_reg__0_n_58,mul_r_reg__0_n_59,mul_r_reg__0_n_60,mul_r_reg__0_n_61,mul_r_reg__0_n_62,mul_r_reg__0_n_63,mul_r_reg__0_n_64,mul_r_reg__0_n_65,mul_r_reg__0_n_66,mul_r_reg__0_n_67,mul_r_reg__0_n_68,mul_r_reg__0_n_69,mul_r_reg__0_n_70,mul_r_reg__0_n_71,mul_r_reg__0_n_72,mul_r_reg__0_n_73,mul_r_reg__0_n_74,mul_r_reg__1[47:17]}),
        .PATTERNBDETECT(NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .PCOUT(NLW_mul_r_reg__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(aresetn),
        .UNDERFLOW(NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "dot_product" *) 
module design_1_top_0_0_dot_product_1
   (D,
    CO,
    Q,
    \res_mem_reg[383] ,
    A,
    b_ram_out,
    dp_enable,
    clk,
    aresetn,
    \slv_regs_reg[0][11] ,
    O,
    \slv_regs_reg[0][0] ,
    \slv_regs_reg[0][8] ,
    \slv_regs_reg[0][11]_0 ,
    \slv_regs_reg[0][11]_1 ,
    \slv_regs_reg[0][11]_2 ,
    dp_extend_end_reg,
    dp_extend_end_reg_0);
  output [47:0]D;
  output [0:0]CO;
  output [2:0]Q;
  output \res_mem_reg[383] ;
  input [16:0]A;
  input [31:0]b_ram_out;
  input dp_enable;
  input clk;
  input aresetn;
  input [1:0]\slv_regs_reg[0][11] ;
  input [0:0]O;
  input [0:0]\slv_regs_reg[0][0] ;
  input [4:0]\slv_regs_reg[0][8] ;
  input [0:0]\slv_regs_reg[0][11]_0 ;
  input [0:0]\slv_regs_reg[0][11]_1 ;
  input [0:0]\slv_regs_reg[0][11]_2 ;
  input [0:0]dp_extend_end_reg;
  input dp_extend_end_reg_0;

  wire [16:0]A;
  wire [0:0]CO;
  wire [47:0]D;
  wire [0:0]O;
  wire [2:0]Q;
  wire add_r1_carry_i_2__1_n_0;
  wire add_r1_carry_i_4__1_n_0;
  wire add_r1_carry_n_0;
  wire add_r1_carry_n_1;
  wire add_r1_carry_n_2;
  wire add_r1_carry_n_3;
  wire \add_r[0]_i_2__1_n_0 ;
  wire \add_r[0]_i_3__1_n_0 ;
  wire \add_r[0]_i_4__1_n_0 ;
  wire \add_r[0]_i_5__1_n_0 ;
  wire \add_r[0]_i_6__1_n_0 ;
  wire \add_r[0]_i_7__1_n_0 ;
  wire \add_r[0]_i_8__1_n_0 ;
  wire \add_r[0]_i_9__1_n_0 ;
  wire \add_r[12]_i_2__1_n_0 ;
  wire \add_r[12]_i_3__1_n_0 ;
  wire \add_r[12]_i_4__1_n_0 ;
  wire \add_r[12]_i_5__1_n_0 ;
  wire \add_r[12]_i_6__1_n_0 ;
  wire \add_r[12]_i_7__1_n_0 ;
  wire \add_r[12]_i_8__1_n_0 ;
  wire \add_r[12]_i_9__1_n_0 ;
  wire \add_r[16]_i_2__1_n_0 ;
  wire \add_r[16]_i_3__1_n_0 ;
  wire \add_r[16]_i_4__1_n_0 ;
  wire \add_r[16]_i_5__1_n_0 ;
  wire \add_r[16]_i_6__1_n_0 ;
  wire \add_r[16]_i_7__1_n_0 ;
  wire \add_r[16]_i_8__1_n_0 ;
  wire \add_r[16]_i_9__1_n_0 ;
  wire \add_r[20]_i_2__1_n_0 ;
  wire \add_r[20]_i_3__1_n_0 ;
  wire \add_r[20]_i_4__1_n_0 ;
  wire \add_r[20]_i_5__1_n_0 ;
  wire \add_r[20]_i_6__1_n_0 ;
  wire \add_r[20]_i_7__1_n_0 ;
  wire \add_r[20]_i_8__1_n_0 ;
  wire \add_r[20]_i_9__1_n_0 ;
  wire \add_r[24]_i_2__1_n_0 ;
  wire \add_r[24]_i_3__1_n_0 ;
  wire \add_r[24]_i_4__1_n_0 ;
  wire \add_r[24]_i_5__1_n_0 ;
  wire \add_r[24]_i_6__1_n_0 ;
  wire \add_r[24]_i_7__1_n_0 ;
  wire \add_r[24]_i_8__1_n_0 ;
  wire \add_r[24]_i_9__1_n_0 ;
  wire \add_r[28]_i_2__1_n_0 ;
  wire \add_r[28]_i_3__1_n_0 ;
  wire \add_r[28]_i_4__1_n_0 ;
  wire \add_r[28]_i_5__1_n_0 ;
  wire \add_r[28]_i_6__1_n_0 ;
  wire \add_r[28]_i_7__1_n_0 ;
  wire \add_r[28]_i_8__1_n_0 ;
  wire \add_r[28]_i_9__1_n_0 ;
  wire \add_r[32]_i_2__1_n_0 ;
  wire \add_r[32]_i_3__1_n_0 ;
  wire \add_r[32]_i_4__1_n_0 ;
  wire \add_r[32]_i_5__1_n_0 ;
  wire \add_r[32]_i_6__1_n_0 ;
  wire \add_r[32]_i_7__1_n_0 ;
  wire \add_r[32]_i_8__1_n_0 ;
  wire \add_r[32]_i_9__1_n_0 ;
  wire \add_r[36]_i_2__1_n_0 ;
  wire \add_r[36]_i_3__1_n_0 ;
  wire \add_r[36]_i_4__1_n_0 ;
  wire \add_r[36]_i_5__1_n_0 ;
  wire \add_r[36]_i_6__1_n_0 ;
  wire \add_r[36]_i_7__1_n_0 ;
  wire \add_r[36]_i_8__1_n_0 ;
  wire \add_r[36]_i_9__1_n_0 ;
  wire \add_r[40]_i_2__1_n_0 ;
  wire \add_r[40]_i_3__1_n_0 ;
  wire \add_r[40]_i_4__1_n_0 ;
  wire \add_r[40]_i_5__1_n_0 ;
  wire \add_r[40]_i_6__1_n_0 ;
  wire \add_r[40]_i_7__1_n_0 ;
  wire \add_r[40]_i_8__1_n_0 ;
  wire \add_r[40]_i_9__1_n_0 ;
  wire \add_r[44]_i_2__1_n_0 ;
  wire \add_r[44]_i_3__1_n_0 ;
  wire \add_r[44]_i_4__1_n_0 ;
  wire \add_r[44]_i_5__1_n_0 ;
  wire \add_r[44]_i_6__1_n_0 ;
  wire \add_r[44]_i_7__1_n_0 ;
  wire \add_r[44]_i_8__1_n_0 ;
  wire \add_r[4]_i_2__1_n_0 ;
  wire \add_r[4]_i_3__1_n_0 ;
  wire \add_r[4]_i_4__1_n_0 ;
  wire \add_r[4]_i_5__1_n_0 ;
  wire \add_r[4]_i_6__1_n_0 ;
  wire \add_r[4]_i_7__1_n_0 ;
  wire \add_r[4]_i_8__1_n_0 ;
  wire \add_r[4]_i_9__1_n_0 ;
  wire \add_r[8]_i_2__1_n_0 ;
  wire \add_r[8]_i_3__1_n_0 ;
  wire \add_r[8]_i_4__1_n_0 ;
  wire \add_r[8]_i_5__1_n_0 ;
  wire \add_r[8]_i_6__1_n_0 ;
  wire \add_r[8]_i_7__1_n_0 ;
  wire \add_r[8]_i_8__1_n_0 ;
  wire \add_r[8]_i_9__1_n_0 ;
  wire \add_r_reg[0]_i_1__1_n_0 ;
  wire \add_r_reg[0]_i_1__1_n_1 ;
  wire \add_r_reg[0]_i_1__1_n_2 ;
  wire \add_r_reg[0]_i_1__1_n_3 ;
  wire \add_r_reg[0]_i_1__1_n_4 ;
  wire \add_r_reg[0]_i_1__1_n_5 ;
  wire \add_r_reg[0]_i_1__1_n_6 ;
  wire \add_r_reg[0]_i_1__1_n_7 ;
  wire \add_r_reg[12]_i_1__1_n_0 ;
  wire \add_r_reg[12]_i_1__1_n_1 ;
  wire \add_r_reg[12]_i_1__1_n_2 ;
  wire \add_r_reg[12]_i_1__1_n_3 ;
  wire \add_r_reg[12]_i_1__1_n_4 ;
  wire \add_r_reg[12]_i_1__1_n_5 ;
  wire \add_r_reg[12]_i_1__1_n_6 ;
  wire \add_r_reg[12]_i_1__1_n_7 ;
  wire \add_r_reg[16]_i_1__1_n_0 ;
  wire \add_r_reg[16]_i_1__1_n_1 ;
  wire \add_r_reg[16]_i_1__1_n_2 ;
  wire \add_r_reg[16]_i_1__1_n_3 ;
  wire \add_r_reg[16]_i_1__1_n_4 ;
  wire \add_r_reg[16]_i_1__1_n_5 ;
  wire \add_r_reg[16]_i_1__1_n_6 ;
  wire \add_r_reg[16]_i_1__1_n_7 ;
  wire \add_r_reg[20]_i_1__1_n_0 ;
  wire \add_r_reg[20]_i_1__1_n_1 ;
  wire \add_r_reg[20]_i_1__1_n_2 ;
  wire \add_r_reg[20]_i_1__1_n_3 ;
  wire \add_r_reg[20]_i_1__1_n_4 ;
  wire \add_r_reg[20]_i_1__1_n_5 ;
  wire \add_r_reg[20]_i_1__1_n_6 ;
  wire \add_r_reg[20]_i_1__1_n_7 ;
  wire \add_r_reg[24]_i_1__1_n_0 ;
  wire \add_r_reg[24]_i_1__1_n_1 ;
  wire \add_r_reg[24]_i_1__1_n_2 ;
  wire \add_r_reg[24]_i_1__1_n_3 ;
  wire \add_r_reg[24]_i_1__1_n_4 ;
  wire \add_r_reg[24]_i_1__1_n_5 ;
  wire \add_r_reg[24]_i_1__1_n_6 ;
  wire \add_r_reg[24]_i_1__1_n_7 ;
  wire \add_r_reg[28]_i_1__1_n_0 ;
  wire \add_r_reg[28]_i_1__1_n_1 ;
  wire \add_r_reg[28]_i_1__1_n_2 ;
  wire \add_r_reg[28]_i_1__1_n_3 ;
  wire \add_r_reg[28]_i_1__1_n_4 ;
  wire \add_r_reg[28]_i_1__1_n_5 ;
  wire \add_r_reg[28]_i_1__1_n_6 ;
  wire \add_r_reg[28]_i_1__1_n_7 ;
  wire \add_r_reg[32]_i_1__1_n_0 ;
  wire \add_r_reg[32]_i_1__1_n_1 ;
  wire \add_r_reg[32]_i_1__1_n_2 ;
  wire \add_r_reg[32]_i_1__1_n_3 ;
  wire \add_r_reg[32]_i_1__1_n_4 ;
  wire \add_r_reg[32]_i_1__1_n_5 ;
  wire \add_r_reg[32]_i_1__1_n_6 ;
  wire \add_r_reg[32]_i_1__1_n_7 ;
  wire \add_r_reg[36]_i_1__1_n_0 ;
  wire \add_r_reg[36]_i_1__1_n_1 ;
  wire \add_r_reg[36]_i_1__1_n_2 ;
  wire \add_r_reg[36]_i_1__1_n_3 ;
  wire \add_r_reg[36]_i_1__1_n_4 ;
  wire \add_r_reg[36]_i_1__1_n_5 ;
  wire \add_r_reg[36]_i_1__1_n_6 ;
  wire \add_r_reg[36]_i_1__1_n_7 ;
  wire \add_r_reg[40]_i_1__1_n_0 ;
  wire \add_r_reg[40]_i_1__1_n_1 ;
  wire \add_r_reg[40]_i_1__1_n_2 ;
  wire \add_r_reg[40]_i_1__1_n_3 ;
  wire \add_r_reg[40]_i_1__1_n_4 ;
  wire \add_r_reg[40]_i_1__1_n_5 ;
  wire \add_r_reg[40]_i_1__1_n_6 ;
  wire \add_r_reg[40]_i_1__1_n_7 ;
  wire \add_r_reg[44]_i_1__1_n_1 ;
  wire \add_r_reg[44]_i_1__1_n_2 ;
  wire \add_r_reg[44]_i_1__1_n_3 ;
  wire \add_r_reg[44]_i_1__1_n_4 ;
  wire \add_r_reg[44]_i_1__1_n_5 ;
  wire \add_r_reg[44]_i_1__1_n_6 ;
  wire \add_r_reg[44]_i_1__1_n_7 ;
  wire \add_r_reg[4]_i_1__1_n_0 ;
  wire \add_r_reg[4]_i_1__1_n_1 ;
  wire \add_r_reg[4]_i_1__1_n_2 ;
  wire \add_r_reg[4]_i_1__1_n_3 ;
  wire \add_r_reg[4]_i_1__1_n_4 ;
  wire \add_r_reg[4]_i_1__1_n_5 ;
  wire \add_r_reg[4]_i_1__1_n_6 ;
  wire \add_r_reg[4]_i_1__1_n_7 ;
  wire \add_r_reg[8]_i_1__1_n_0 ;
  wire \add_r_reg[8]_i_1__1_n_1 ;
  wire \add_r_reg[8]_i_1__1_n_2 ;
  wire \add_r_reg[8]_i_1__1_n_3 ;
  wire \add_r_reg[8]_i_1__1_n_4 ;
  wire \add_r_reg[8]_i_1__1_n_5 ;
  wire \add_r_reg[8]_i_1__1_n_6 ;
  wire \add_r_reg[8]_i_1__1_n_7 ;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \counter[0]_i_1__4_n_0 ;
  wire \counter[1]_i_1_n_0 ;
  wire \counter[2]_i_1__3_n_0 ;
  wire \counter[3]_i_1__3_n_0 ;
  wire \counter[4]_i_1__0_n_0 ;
  wire \counter[5]_i_1__2_n_0 ;
  wire \counter[6]_i_1__1_n_0 ;
  wire \counter[7]_i_1__1_n_0 ;
  wire \counter[7]_i_2__0_n_0 ;
  wire \counter[8]_i_2__2_n_0 ;
  wire \counter[8]_i_3__2_n_0 ;
  wire \counter_reg_n_0_[0] ;
  wire \counter_reg_n_0_[1] ;
  wire \counter_reg_n_0_[2] ;
  wire \counter_reg_n_0_[6] ;
  wire \counter_reg_n_0_[7] ;
  wire \counter_reg_n_0_[8] ;
  wire dp_enable;
  wire [0:0]dp_extend_end_reg;
  wire dp_extend_end_reg_0;
  wire mul_r0_n_100;
  wire mul_r0_n_101;
  wire mul_r0_n_102;
  wire mul_r0_n_103;
  wire mul_r0_n_104;
  wire mul_r0_n_105;
  wire mul_r0_n_106;
  wire mul_r0_n_107;
  wire mul_r0_n_108;
  wire mul_r0_n_109;
  wire mul_r0_n_110;
  wire mul_r0_n_111;
  wire mul_r0_n_112;
  wire mul_r0_n_113;
  wire mul_r0_n_114;
  wire mul_r0_n_115;
  wire mul_r0_n_116;
  wire mul_r0_n_117;
  wire mul_r0_n_118;
  wire mul_r0_n_119;
  wire mul_r0_n_120;
  wire mul_r0_n_121;
  wire mul_r0_n_122;
  wire mul_r0_n_123;
  wire mul_r0_n_124;
  wire mul_r0_n_125;
  wire mul_r0_n_126;
  wire mul_r0_n_127;
  wire mul_r0_n_128;
  wire mul_r0_n_129;
  wire mul_r0_n_130;
  wire mul_r0_n_131;
  wire mul_r0_n_132;
  wire mul_r0_n_133;
  wire mul_r0_n_134;
  wire mul_r0_n_135;
  wire mul_r0_n_136;
  wire mul_r0_n_137;
  wire mul_r0_n_138;
  wire mul_r0_n_139;
  wire mul_r0_n_140;
  wire mul_r0_n_141;
  wire mul_r0_n_142;
  wire mul_r0_n_143;
  wire mul_r0_n_144;
  wire mul_r0_n_145;
  wire mul_r0_n_146;
  wire mul_r0_n_147;
  wire mul_r0_n_148;
  wire mul_r0_n_149;
  wire mul_r0_n_150;
  wire mul_r0_n_151;
  wire mul_r0_n_152;
  wire mul_r0_n_153;
  wire mul_r0_n_58;
  wire mul_r0_n_59;
  wire mul_r0_n_60;
  wire mul_r0_n_61;
  wire mul_r0_n_62;
  wire mul_r0_n_63;
  wire mul_r0_n_64;
  wire mul_r0_n_65;
  wire mul_r0_n_66;
  wire mul_r0_n_67;
  wire mul_r0_n_68;
  wire mul_r0_n_69;
  wire mul_r0_n_70;
  wire mul_r0_n_71;
  wire mul_r0_n_72;
  wire mul_r0_n_73;
  wire mul_r0_n_74;
  wire mul_r0_n_75;
  wire mul_r0_n_76;
  wire mul_r0_n_77;
  wire mul_r0_n_78;
  wire mul_r0_n_79;
  wire mul_r0_n_80;
  wire mul_r0_n_81;
  wire mul_r0_n_82;
  wire mul_r0_n_83;
  wire mul_r0_n_84;
  wire mul_r0_n_85;
  wire mul_r0_n_86;
  wire mul_r0_n_87;
  wire mul_r0_n_88;
  wire mul_r0_n_89;
  wire mul_r0_n_90;
  wire mul_r0_n_91;
  wire mul_r0_n_92;
  wire mul_r0_n_93;
  wire mul_r0_n_94;
  wire mul_r0_n_95;
  wire mul_r0_n_96;
  wire mul_r0_n_97;
  wire mul_r0_n_98;
  wire mul_r0_n_99;
  wire mul_r_reg__0_n_58;
  wire mul_r_reg__0_n_59;
  wire mul_r_reg__0_n_60;
  wire mul_r_reg__0_n_61;
  wire mul_r_reg__0_n_62;
  wire mul_r_reg__0_n_63;
  wire mul_r_reg__0_n_64;
  wire mul_r_reg__0_n_65;
  wire mul_r_reg__0_n_66;
  wire mul_r_reg__0_n_67;
  wire mul_r_reg__0_n_68;
  wire mul_r_reg__0_n_69;
  wire mul_r_reg__0_n_70;
  wire mul_r_reg__0_n_71;
  wire mul_r_reg__0_n_72;
  wire mul_r_reg__0_n_73;
  wire mul_r_reg__0_n_74;
  wire [47:0]mul_r_reg__1;
  wire \res_mem_reg[383] ;
  wire [0:0]\slv_regs_reg[0][0] ;
  wire [1:0]\slv_regs_reg[0][11] ;
  wire [0:0]\slv_regs_reg[0][11]_0 ;
  wire [0:0]\slv_regs_reg[0][11]_1 ;
  wire [0:0]\slv_regs_reg[0][11]_2 ;
  wire [4:0]\slv_regs_reg[0][8] ;
  wire [3:0]NLW_add_r1_carry_O_UNCONNECTED;
  wire [3:1]NLW_add_r1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_add_r1_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_add_r_reg[44]_i_1__1_CO_UNCONNECTED ;
  wire NLW_mul_r0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r0_CARRYOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r_reg__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r_reg__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_r_reg__0_PCOUT_UNCONNECTED;

  CARRY4 add_r1_carry
       (.CI(1'b0),
        .CO({add_r1_carry_n_0,add_r1_carry_n_1,add_r1_carry_n_2,add_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry_O_UNCONNECTED[3:0]),
        .S({\slv_regs_reg[0][11] [1],add_r1_carry_i_2__1_n_0,\slv_regs_reg[0][11] [0],add_r1_carry_i_4__1_n_0}));
  CARRY4 add_r1_carry__0
       (.CI(add_r1_carry_n_0),
        .CO({NLW_add_r1_carry__0_CO_UNCONNECTED[3:1],CO}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,O}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_2__1
       (.I0(\counter_reg_n_0_[8] ),
        .I1(\slv_regs_reg[0][8] [4]),
        .I2(\counter_reg_n_0_[6] ),
        .I3(\slv_regs_reg[0][8] [2]),
        .I4(\slv_regs_reg[0][8] [3]),
        .I5(\counter_reg_n_0_[7] ),
        .O(add_r1_carry_i_2__1_n_0));
  LUT6 #(
    .INIT(64'h6006000000006006)) 
    add_r1_carry_i_4__1
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\slv_regs_reg[0][0] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\slv_regs_reg[0][8] [1]),
        .I4(\counter_reg_n_0_[1] ),
        .I5(\slv_regs_reg[0][8] [0]),
        .O(add_r1_carry_i_4__1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_2__1 
       (.I0(mul_r_reg__1[3]),
        .I1(CO),
        .O(\add_r[0]_i_2__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_3__1 
       (.I0(mul_r_reg__1[2]),
        .I1(CO),
        .O(\add_r[0]_i_3__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_4__1 
       (.I0(mul_r_reg__1[1]),
        .I1(CO),
        .O(\add_r[0]_i_4__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_5__1 
       (.I0(mul_r_reg__1[0]),
        .I1(CO),
        .O(\add_r[0]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_6__1 
       (.I0(D[3]),
        .I1(CO),
        .I2(mul_r_reg__1[3]),
        .O(\add_r[0]_i_6__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_7__1 
       (.I0(D[2]),
        .I1(CO),
        .I2(mul_r_reg__1[2]),
        .O(\add_r[0]_i_7__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_8__1 
       (.I0(D[1]),
        .I1(CO),
        .I2(mul_r_reg__1[1]),
        .O(\add_r[0]_i_8__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_9__1 
       (.I0(D[0]),
        .I1(CO),
        .I2(mul_r_reg__1[0]),
        .O(\add_r[0]_i_9__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_2__1 
       (.I0(mul_r_reg__1[15]),
        .I1(CO),
        .O(\add_r[12]_i_2__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_3__1 
       (.I0(mul_r_reg__1[14]),
        .I1(CO),
        .O(\add_r[12]_i_3__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_4__1 
       (.I0(mul_r_reg__1[13]),
        .I1(CO),
        .O(\add_r[12]_i_4__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_5__1 
       (.I0(mul_r_reg__1[12]),
        .I1(CO),
        .O(\add_r[12]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_6__1 
       (.I0(D[15]),
        .I1(CO),
        .I2(mul_r_reg__1[15]),
        .O(\add_r[12]_i_6__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_7__1 
       (.I0(D[14]),
        .I1(CO),
        .I2(mul_r_reg__1[14]),
        .O(\add_r[12]_i_7__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_8__1 
       (.I0(D[13]),
        .I1(CO),
        .I2(mul_r_reg__1[13]),
        .O(\add_r[12]_i_8__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_9__1 
       (.I0(D[12]),
        .I1(CO),
        .I2(mul_r_reg__1[12]),
        .O(\add_r[12]_i_9__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_2__1 
       (.I0(mul_r_reg__1[19]),
        .I1(CO),
        .O(\add_r[16]_i_2__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_3__1 
       (.I0(mul_r_reg__1[18]),
        .I1(CO),
        .O(\add_r[16]_i_3__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_4__1 
       (.I0(mul_r_reg__1[17]),
        .I1(CO),
        .O(\add_r[16]_i_4__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_5__1 
       (.I0(mul_r_reg__1[16]),
        .I1(CO),
        .O(\add_r[16]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_6__1 
       (.I0(D[19]),
        .I1(CO),
        .I2(mul_r_reg__1[19]),
        .O(\add_r[16]_i_6__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_7__1 
       (.I0(D[18]),
        .I1(CO),
        .I2(mul_r_reg__1[18]),
        .O(\add_r[16]_i_7__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_8__1 
       (.I0(D[17]),
        .I1(CO),
        .I2(mul_r_reg__1[17]),
        .O(\add_r[16]_i_8__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_9__1 
       (.I0(D[16]),
        .I1(CO),
        .I2(mul_r_reg__1[16]),
        .O(\add_r[16]_i_9__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_2__1 
       (.I0(mul_r_reg__1[23]),
        .I1(CO),
        .O(\add_r[20]_i_2__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_3__1 
       (.I0(mul_r_reg__1[22]),
        .I1(CO),
        .O(\add_r[20]_i_3__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_4__1 
       (.I0(mul_r_reg__1[21]),
        .I1(CO),
        .O(\add_r[20]_i_4__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_5__1 
       (.I0(mul_r_reg__1[20]),
        .I1(CO),
        .O(\add_r[20]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_6__1 
       (.I0(D[23]),
        .I1(CO),
        .I2(mul_r_reg__1[23]),
        .O(\add_r[20]_i_6__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_7__1 
       (.I0(D[22]),
        .I1(CO),
        .I2(mul_r_reg__1[22]),
        .O(\add_r[20]_i_7__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_8__1 
       (.I0(D[21]),
        .I1(CO),
        .I2(mul_r_reg__1[21]),
        .O(\add_r[20]_i_8__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_9__1 
       (.I0(D[20]),
        .I1(CO),
        .I2(mul_r_reg__1[20]),
        .O(\add_r[20]_i_9__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_2__1 
       (.I0(mul_r_reg__1[27]),
        .I1(CO),
        .O(\add_r[24]_i_2__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_3__1 
       (.I0(mul_r_reg__1[26]),
        .I1(CO),
        .O(\add_r[24]_i_3__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_4__1 
       (.I0(mul_r_reg__1[25]),
        .I1(CO),
        .O(\add_r[24]_i_4__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_5__1 
       (.I0(mul_r_reg__1[24]),
        .I1(CO),
        .O(\add_r[24]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_6__1 
       (.I0(D[27]),
        .I1(CO),
        .I2(mul_r_reg__1[27]),
        .O(\add_r[24]_i_6__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_7__1 
       (.I0(D[26]),
        .I1(CO),
        .I2(mul_r_reg__1[26]),
        .O(\add_r[24]_i_7__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_8__1 
       (.I0(D[25]),
        .I1(CO),
        .I2(mul_r_reg__1[25]),
        .O(\add_r[24]_i_8__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_9__1 
       (.I0(D[24]),
        .I1(CO),
        .I2(mul_r_reg__1[24]),
        .O(\add_r[24]_i_9__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_2__1 
       (.I0(mul_r_reg__1[31]),
        .I1(CO),
        .O(\add_r[28]_i_2__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_3__1 
       (.I0(mul_r_reg__1[30]),
        .I1(CO),
        .O(\add_r[28]_i_3__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_4__1 
       (.I0(mul_r_reg__1[29]),
        .I1(CO),
        .O(\add_r[28]_i_4__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_5__1 
       (.I0(mul_r_reg__1[28]),
        .I1(CO),
        .O(\add_r[28]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_6__1 
       (.I0(D[31]),
        .I1(CO),
        .I2(mul_r_reg__1[31]),
        .O(\add_r[28]_i_6__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_7__1 
       (.I0(D[30]),
        .I1(CO),
        .I2(mul_r_reg__1[30]),
        .O(\add_r[28]_i_7__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_8__1 
       (.I0(D[29]),
        .I1(CO),
        .I2(mul_r_reg__1[29]),
        .O(\add_r[28]_i_8__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_9__1 
       (.I0(D[28]),
        .I1(CO),
        .I2(mul_r_reg__1[28]),
        .O(\add_r[28]_i_9__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_2__1 
       (.I0(mul_r_reg__1[35]),
        .I1(CO),
        .O(\add_r[32]_i_2__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_3__1 
       (.I0(mul_r_reg__1[34]),
        .I1(CO),
        .O(\add_r[32]_i_3__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_4__1 
       (.I0(mul_r_reg__1[33]),
        .I1(CO),
        .O(\add_r[32]_i_4__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_5__1 
       (.I0(mul_r_reg__1[32]),
        .I1(CO),
        .O(\add_r[32]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_6__1 
       (.I0(D[35]),
        .I1(CO),
        .I2(mul_r_reg__1[35]),
        .O(\add_r[32]_i_6__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_7__1 
       (.I0(D[34]),
        .I1(CO),
        .I2(mul_r_reg__1[34]),
        .O(\add_r[32]_i_7__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_8__1 
       (.I0(D[33]),
        .I1(CO),
        .I2(mul_r_reg__1[33]),
        .O(\add_r[32]_i_8__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_9__1 
       (.I0(D[32]),
        .I1(CO),
        .I2(mul_r_reg__1[32]),
        .O(\add_r[32]_i_9__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_2__1 
       (.I0(mul_r_reg__1[39]),
        .I1(CO),
        .O(\add_r[36]_i_2__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_3__1 
       (.I0(mul_r_reg__1[38]),
        .I1(CO),
        .O(\add_r[36]_i_3__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_4__1 
       (.I0(mul_r_reg__1[37]),
        .I1(CO),
        .O(\add_r[36]_i_4__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_5__1 
       (.I0(mul_r_reg__1[36]),
        .I1(CO),
        .O(\add_r[36]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_6__1 
       (.I0(D[39]),
        .I1(CO),
        .I2(mul_r_reg__1[39]),
        .O(\add_r[36]_i_6__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_7__1 
       (.I0(D[38]),
        .I1(CO),
        .I2(mul_r_reg__1[38]),
        .O(\add_r[36]_i_7__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_8__1 
       (.I0(D[37]),
        .I1(CO),
        .I2(mul_r_reg__1[37]),
        .O(\add_r[36]_i_8__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_9__1 
       (.I0(D[36]),
        .I1(CO),
        .I2(mul_r_reg__1[36]),
        .O(\add_r[36]_i_9__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_2__1 
       (.I0(mul_r_reg__1[43]),
        .I1(CO),
        .O(\add_r[40]_i_2__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_3__1 
       (.I0(mul_r_reg__1[42]),
        .I1(CO),
        .O(\add_r[40]_i_3__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_4__1 
       (.I0(mul_r_reg__1[41]),
        .I1(CO),
        .O(\add_r[40]_i_4__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_5__1 
       (.I0(mul_r_reg__1[40]),
        .I1(CO),
        .O(\add_r[40]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_6__1 
       (.I0(D[43]),
        .I1(CO),
        .I2(mul_r_reg__1[43]),
        .O(\add_r[40]_i_6__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_7__1 
       (.I0(D[42]),
        .I1(CO),
        .I2(mul_r_reg__1[42]),
        .O(\add_r[40]_i_7__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_8__1 
       (.I0(D[41]),
        .I1(CO),
        .I2(mul_r_reg__1[41]),
        .O(\add_r[40]_i_8__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_9__1 
       (.I0(D[40]),
        .I1(CO),
        .I2(mul_r_reg__1[40]),
        .O(\add_r[40]_i_9__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_2__1 
       (.I0(mul_r_reg__1[46]),
        .I1(CO),
        .O(\add_r[44]_i_2__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_3__1 
       (.I0(mul_r_reg__1[45]),
        .I1(CO),
        .O(\add_r[44]_i_3__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_4__1 
       (.I0(mul_r_reg__1[44]),
        .I1(CO),
        .O(\add_r[44]_i_4__1_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_5__1 
       (.I0(mul_r_reg__1[47]),
        .I1(CO),
        .I2(D[47]),
        .O(\add_r[44]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_6__1 
       (.I0(D[46]),
        .I1(CO),
        .I2(mul_r_reg__1[46]),
        .O(\add_r[44]_i_6__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_7__1 
       (.I0(D[45]),
        .I1(CO),
        .I2(mul_r_reg__1[45]),
        .O(\add_r[44]_i_7__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_8__1 
       (.I0(D[44]),
        .I1(CO),
        .I2(mul_r_reg__1[44]),
        .O(\add_r[44]_i_8__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_2__1 
       (.I0(mul_r_reg__1[7]),
        .I1(CO),
        .O(\add_r[4]_i_2__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_3__1 
       (.I0(mul_r_reg__1[6]),
        .I1(CO),
        .O(\add_r[4]_i_3__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_4__1 
       (.I0(mul_r_reg__1[5]),
        .I1(CO),
        .O(\add_r[4]_i_4__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_5__1 
       (.I0(mul_r_reg__1[4]),
        .I1(CO),
        .O(\add_r[4]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_6__1 
       (.I0(D[7]),
        .I1(CO),
        .I2(mul_r_reg__1[7]),
        .O(\add_r[4]_i_6__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_7__1 
       (.I0(D[6]),
        .I1(CO),
        .I2(mul_r_reg__1[6]),
        .O(\add_r[4]_i_7__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_8__1 
       (.I0(D[5]),
        .I1(CO),
        .I2(mul_r_reg__1[5]),
        .O(\add_r[4]_i_8__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_9__1 
       (.I0(D[4]),
        .I1(CO),
        .I2(mul_r_reg__1[4]),
        .O(\add_r[4]_i_9__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_2__1 
       (.I0(mul_r_reg__1[11]),
        .I1(CO),
        .O(\add_r[8]_i_2__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_3__1 
       (.I0(mul_r_reg__1[10]),
        .I1(CO),
        .O(\add_r[8]_i_3__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_4__1 
       (.I0(mul_r_reg__1[9]),
        .I1(CO),
        .O(\add_r[8]_i_4__1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_5__1 
       (.I0(mul_r_reg__1[8]),
        .I1(CO),
        .O(\add_r[8]_i_5__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_6__1 
       (.I0(D[11]),
        .I1(CO),
        .I2(mul_r_reg__1[11]),
        .O(\add_r[8]_i_6__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_7__1 
       (.I0(D[10]),
        .I1(CO),
        .I2(mul_r_reg__1[10]),
        .O(\add_r[8]_i_7__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_8__1 
       (.I0(D[9]),
        .I1(CO),
        .I2(mul_r_reg__1[9]),
        .O(\add_r[8]_i_8__1_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_9__1 
       (.I0(D[8]),
        .I1(CO),
        .I2(mul_r_reg__1[8]),
        .O(\add_r[8]_i_9__1_n_0 ));
  FDRE \add_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__1_n_7 ),
        .Q(D[0]),
        .R(aresetn));
  CARRY4 \add_r_reg[0]_i_1__1 
       (.CI(1'b0),
        .CO({\add_r_reg[0]_i_1__1_n_0 ,\add_r_reg[0]_i_1__1_n_1 ,\add_r_reg[0]_i_1__1_n_2 ,\add_r_reg[0]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[0]_i_2__1_n_0 ,\add_r[0]_i_3__1_n_0 ,\add_r[0]_i_4__1_n_0 ,\add_r[0]_i_5__1_n_0 }),
        .O({\add_r_reg[0]_i_1__1_n_4 ,\add_r_reg[0]_i_1__1_n_5 ,\add_r_reg[0]_i_1__1_n_6 ,\add_r_reg[0]_i_1__1_n_7 }),
        .S({\add_r[0]_i_6__1_n_0 ,\add_r[0]_i_7__1_n_0 ,\add_r[0]_i_8__1_n_0 ,\add_r[0]_i_9__1_n_0 }));
  FDRE \add_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__1_n_5 ),
        .Q(D[10]),
        .R(aresetn));
  FDRE \add_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__1_n_4 ),
        .Q(D[11]),
        .R(aresetn));
  FDRE \add_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__1_n_7 ),
        .Q(D[12]),
        .R(aresetn));
  CARRY4 \add_r_reg[12]_i_1__1 
       (.CI(\add_r_reg[8]_i_1__1_n_0 ),
        .CO({\add_r_reg[12]_i_1__1_n_0 ,\add_r_reg[12]_i_1__1_n_1 ,\add_r_reg[12]_i_1__1_n_2 ,\add_r_reg[12]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[12]_i_2__1_n_0 ,\add_r[12]_i_3__1_n_0 ,\add_r[12]_i_4__1_n_0 ,\add_r[12]_i_5__1_n_0 }),
        .O({\add_r_reg[12]_i_1__1_n_4 ,\add_r_reg[12]_i_1__1_n_5 ,\add_r_reg[12]_i_1__1_n_6 ,\add_r_reg[12]_i_1__1_n_7 }),
        .S({\add_r[12]_i_6__1_n_0 ,\add_r[12]_i_7__1_n_0 ,\add_r[12]_i_8__1_n_0 ,\add_r[12]_i_9__1_n_0 }));
  FDRE \add_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__1_n_6 ),
        .Q(D[13]),
        .R(aresetn));
  FDRE \add_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__1_n_5 ),
        .Q(D[14]),
        .R(aresetn));
  FDRE \add_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__1_n_4 ),
        .Q(D[15]),
        .R(aresetn));
  FDRE \add_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__1_n_7 ),
        .Q(D[16]),
        .R(aresetn));
  CARRY4 \add_r_reg[16]_i_1__1 
       (.CI(\add_r_reg[12]_i_1__1_n_0 ),
        .CO({\add_r_reg[16]_i_1__1_n_0 ,\add_r_reg[16]_i_1__1_n_1 ,\add_r_reg[16]_i_1__1_n_2 ,\add_r_reg[16]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[16]_i_2__1_n_0 ,\add_r[16]_i_3__1_n_0 ,\add_r[16]_i_4__1_n_0 ,\add_r[16]_i_5__1_n_0 }),
        .O({\add_r_reg[16]_i_1__1_n_4 ,\add_r_reg[16]_i_1__1_n_5 ,\add_r_reg[16]_i_1__1_n_6 ,\add_r_reg[16]_i_1__1_n_7 }),
        .S({\add_r[16]_i_6__1_n_0 ,\add_r[16]_i_7__1_n_0 ,\add_r[16]_i_8__1_n_0 ,\add_r[16]_i_9__1_n_0 }));
  FDRE \add_r_reg[17] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__1_n_6 ),
        .Q(D[17]),
        .R(aresetn));
  FDRE \add_r_reg[18] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__1_n_5 ),
        .Q(D[18]),
        .R(aresetn));
  FDRE \add_r_reg[19] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__1_n_4 ),
        .Q(D[19]),
        .R(aresetn));
  FDRE \add_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__1_n_6 ),
        .Q(D[1]),
        .R(aresetn));
  FDRE \add_r_reg[20] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__1_n_7 ),
        .Q(D[20]),
        .R(aresetn));
  CARRY4 \add_r_reg[20]_i_1__1 
       (.CI(\add_r_reg[16]_i_1__1_n_0 ),
        .CO({\add_r_reg[20]_i_1__1_n_0 ,\add_r_reg[20]_i_1__1_n_1 ,\add_r_reg[20]_i_1__1_n_2 ,\add_r_reg[20]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[20]_i_2__1_n_0 ,\add_r[20]_i_3__1_n_0 ,\add_r[20]_i_4__1_n_0 ,\add_r[20]_i_5__1_n_0 }),
        .O({\add_r_reg[20]_i_1__1_n_4 ,\add_r_reg[20]_i_1__1_n_5 ,\add_r_reg[20]_i_1__1_n_6 ,\add_r_reg[20]_i_1__1_n_7 }),
        .S({\add_r[20]_i_6__1_n_0 ,\add_r[20]_i_7__1_n_0 ,\add_r[20]_i_8__1_n_0 ,\add_r[20]_i_9__1_n_0 }));
  FDRE \add_r_reg[21] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__1_n_6 ),
        .Q(D[21]),
        .R(aresetn));
  FDRE \add_r_reg[22] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__1_n_5 ),
        .Q(D[22]),
        .R(aresetn));
  FDRE \add_r_reg[23] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__1_n_4 ),
        .Q(D[23]),
        .R(aresetn));
  FDRE \add_r_reg[24] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__1_n_7 ),
        .Q(D[24]),
        .R(aresetn));
  CARRY4 \add_r_reg[24]_i_1__1 
       (.CI(\add_r_reg[20]_i_1__1_n_0 ),
        .CO({\add_r_reg[24]_i_1__1_n_0 ,\add_r_reg[24]_i_1__1_n_1 ,\add_r_reg[24]_i_1__1_n_2 ,\add_r_reg[24]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[24]_i_2__1_n_0 ,\add_r[24]_i_3__1_n_0 ,\add_r[24]_i_4__1_n_0 ,\add_r[24]_i_5__1_n_0 }),
        .O({\add_r_reg[24]_i_1__1_n_4 ,\add_r_reg[24]_i_1__1_n_5 ,\add_r_reg[24]_i_1__1_n_6 ,\add_r_reg[24]_i_1__1_n_7 }),
        .S({\add_r[24]_i_6__1_n_0 ,\add_r[24]_i_7__1_n_0 ,\add_r[24]_i_8__1_n_0 ,\add_r[24]_i_9__1_n_0 }));
  FDRE \add_r_reg[25] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__1_n_6 ),
        .Q(D[25]),
        .R(aresetn));
  FDRE \add_r_reg[26] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__1_n_5 ),
        .Q(D[26]),
        .R(aresetn));
  FDRE \add_r_reg[27] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__1_n_4 ),
        .Q(D[27]),
        .R(aresetn));
  FDRE \add_r_reg[28] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__1_n_7 ),
        .Q(D[28]),
        .R(aresetn));
  CARRY4 \add_r_reg[28]_i_1__1 
       (.CI(\add_r_reg[24]_i_1__1_n_0 ),
        .CO({\add_r_reg[28]_i_1__1_n_0 ,\add_r_reg[28]_i_1__1_n_1 ,\add_r_reg[28]_i_1__1_n_2 ,\add_r_reg[28]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[28]_i_2__1_n_0 ,\add_r[28]_i_3__1_n_0 ,\add_r[28]_i_4__1_n_0 ,\add_r[28]_i_5__1_n_0 }),
        .O({\add_r_reg[28]_i_1__1_n_4 ,\add_r_reg[28]_i_1__1_n_5 ,\add_r_reg[28]_i_1__1_n_6 ,\add_r_reg[28]_i_1__1_n_7 }),
        .S({\add_r[28]_i_6__1_n_0 ,\add_r[28]_i_7__1_n_0 ,\add_r[28]_i_8__1_n_0 ,\add_r[28]_i_9__1_n_0 }));
  FDRE \add_r_reg[29] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__1_n_6 ),
        .Q(D[29]),
        .R(aresetn));
  FDRE \add_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__1_n_5 ),
        .Q(D[2]),
        .R(aresetn));
  FDRE \add_r_reg[30] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__1_n_5 ),
        .Q(D[30]),
        .R(aresetn));
  FDRE \add_r_reg[31] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__1_n_4 ),
        .Q(D[31]),
        .R(aresetn));
  FDRE \add_r_reg[32] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__1_n_7 ),
        .Q(D[32]),
        .R(aresetn));
  CARRY4 \add_r_reg[32]_i_1__1 
       (.CI(\add_r_reg[28]_i_1__1_n_0 ),
        .CO({\add_r_reg[32]_i_1__1_n_0 ,\add_r_reg[32]_i_1__1_n_1 ,\add_r_reg[32]_i_1__1_n_2 ,\add_r_reg[32]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[32]_i_2__1_n_0 ,\add_r[32]_i_3__1_n_0 ,\add_r[32]_i_4__1_n_0 ,\add_r[32]_i_5__1_n_0 }),
        .O({\add_r_reg[32]_i_1__1_n_4 ,\add_r_reg[32]_i_1__1_n_5 ,\add_r_reg[32]_i_1__1_n_6 ,\add_r_reg[32]_i_1__1_n_7 }),
        .S({\add_r[32]_i_6__1_n_0 ,\add_r[32]_i_7__1_n_0 ,\add_r[32]_i_8__1_n_0 ,\add_r[32]_i_9__1_n_0 }));
  FDRE \add_r_reg[33] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__1_n_6 ),
        .Q(D[33]),
        .R(aresetn));
  FDRE \add_r_reg[34] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__1_n_5 ),
        .Q(D[34]),
        .R(aresetn));
  FDRE \add_r_reg[35] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__1_n_4 ),
        .Q(D[35]),
        .R(aresetn));
  FDRE \add_r_reg[36] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__1_n_7 ),
        .Q(D[36]),
        .R(aresetn));
  CARRY4 \add_r_reg[36]_i_1__1 
       (.CI(\add_r_reg[32]_i_1__1_n_0 ),
        .CO({\add_r_reg[36]_i_1__1_n_0 ,\add_r_reg[36]_i_1__1_n_1 ,\add_r_reg[36]_i_1__1_n_2 ,\add_r_reg[36]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[36]_i_2__1_n_0 ,\add_r[36]_i_3__1_n_0 ,\add_r[36]_i_4__1_n_0 ,\add_r[36]_i_5__1_n_0 }),
        .O({\add_r_reg[36]_i_1__1_n_4 ,\add_r_reg[36]_i_1__1_n_5 ,\add_r_reg[36]_i_1__1_n_6 ,\add_r_reg[36]_i_1__1_n_7 }),
        .S({\add_r[36]_i_6__1_n_0 ,\add_r[36]_i_7__1_n_0 ,\add_r[36]_i_8__1_n_0 ,\add_r[36]_i_9__1_n_0 }));
  FDRE \add_r_reg[37] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__1_n_6 ),
        .Q(D[37]),
        .R(aresetn));
  FDRE \add_r_reg[38] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__1_n_5 ),
        .Q(D[38]),
        .R(aresetn));
  FDRE \add_r_reg[39] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__1_n_4 ),
        .Q(D[39]),
        .R(aresetn));
  FDRE \add_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__1_n_4 ),
        .Q(D[3]),
        .R(aresetn));
  FDRE \add_r_reg[40] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__1_n_7 ),
        .Q(D[40]),
        .R(aresetn));
  CARRY4 \add_r_reg[40]_i_1__1 
       (.CI(\add_r_reg[36]_i_1__1_n_0 ),
        .CO({\add_r_reg[40]_i_1__1_n_0 ,\add_r_reg[40]_i_1__1_n_1 ,\add_r_reg[40]_i_1__1_n_2 ,\add_r_reg[40]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[40]_i_2__1_n_0 ,\add_r[40]_i_3__1_n_0 ,\add_r[40]_i_4__1_n_0 ,\add_r[40]_i_5__1_n_0 }),
        .O({\add_r_reg[40]_i_1__1_n_4 ,\add_r_reg[40]_i_1__1_n_5 ,\add_r_reg[40]_i_1__1_n_6 ,\add_r_reg[40]_i_1__1_n_7 }),
        .S({\add_r[40]_i_6__1_n_0 ,\add_r[40]_i_7__1_n_0 ,\add_r[40]_i_8__1_n_0 ,\add_r[40]_i_9__1_n_0 }));
  FDRE \add_r_reg[41] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__1_n_6 ),
        .Q(D[41]),
        .R(aresetn));
  FDRE \add_r_reg[42] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__1_n_5 ),
        .Q(D[42]),
        .R(aresetn));
  FDRE \add_r_reg[43] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__1_n_4 ),
        .Q(D[43]),
        .R(aresetn));
  FDRE \add_r_reg[44] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__1_n_7 ),
        .Q(D[44]),
        .R(aresetn));
  CARRY4 \add_r_reg[44]_i_1__1 
       (.CI(\add_r_reg[40]_i_1__1_n_0 ),
        .CO({\NLW_add_r_reg[44]_i_1__1_CO_UNCONNECTED [3],\add_r_reg[44]_i_1__1_n_1 ,\add_r_reg[44]_i_1__1_n_2 ,\add_r_reg[44]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\add_r[44]_i_2__1_n_0 ,\add_r[44]_i_3__1_n_0 ,\add_r[44]_i_4__1_n_0 }),
        .O({\add_r_reg[44]_i_1__1_n_4 ,\add_r_reg[44]_i_1__1_n_5 ,\add_r_reg[44]_i_1__1_n_6 ,\add_r_reg[44]_i_1__1_n_7 }),
        .S({\add_r[44]_i_5__1_n_0 ,\add_r[44]_i_6__1_n_0 ,\add_r[44]_i_7__1_n_0 ,\add_r[44]_i_8__1_n_0 }));
  FDRE \add_r_reg[45] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__1_n_6 ),
        .Q(D[45]),
        .R(aresetn));
  FDRE \add_r_reg[46] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__1_n_5 ),
        .Q(D[46]),
        .R(aresetn));
  FDRE \add_r_reg[47] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__1_n_4 ),
        .Q(D[47]),
        .R(aresetn));
  FDRE \add_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__1_n_7 ),
        .Q(D[4]),
        .R(aresetn));
  CARRY4 \add_r_reg[4]_i_1__1 
       (.CI(\add_r_reg[0]_i_1__1_n_0 ),
        .CO({\add_r_reg[4]_i_1__1_n_0 ,\add_r_reg[4]_i_1__1_n_1 ,\add_r_reg[4]_i_1__1_n_2 ,\add_r_reg[4]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[4]_i_2__1_n_0 ,\add_r[4]_i_3__1_n_0 ,\add_r[4]_i_4__1_n_0 ,\add_r[4]_i_5__1_n_0 }),
        .O({\add_r_reg[4]_i_1__1_n_4 ,\add_r_reg[4]_i_1__1_n_5 ,\add_r_reg[4]_i_1__1_n_6 ,\add_r_reg[4]_i_1__1_n_7 }),
        .S({\add_r[4]_i_6__1_n_0 ,\add_r[4]_i_7__1_n_0 ,\add_r[4]_i_8__1_n_0 ,\add_r[4]_i_9__1_n_0 }));
  FDRE \add_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__1_n_6 ),
        .Q(D[5]),
        .R(aresetn));
  FDRE \add_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__1_n_5 ),
        .Q(D[6]),
        .R(aresetn));
  FDRE \add_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__1_n_4 ),
        .Q(D[7]),
        .R(aresetn));
  FDRE \add_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__1_n_7 ),
        .Q(D[8]),
        .R(aresetn));
  CARRY4 \add_r_reg[8]_i_1__1 
       (.CI(\add_r_reg[4]_i_1__1_n_0 ),
        .CO({\add_r_reg[8]_i_1__1_n_0 ,\add_r_reg[8]_i_1__1_n_1 ,\add_r_reg[8]_i_1__1_n_2 ,\add_r_reg[8]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[8]_i_2__1_n_0 ,\add_r[8]_i_3__1_n_0 ,\add_r[8]_i_4__1_n_0 ,\add_r[8]_i_5__1_n_0 }),
        .O({\add_r_reg[8]_i_1__1_n_4 ,\add_r_reg[8]_i_1__1_n_5 ,\add_r_reg[8]_i_1__1_n_6 ,\add_r_reg[8]_i_1__1_n_7 }),
        .S({\add_r[8]_i_6__1_n_0 ,\add_r[8]_i_7__1_n_0 ,\add_r[8]_i_8__1_n_0 ,\add_r[8]_i_9__1_n_0 }));
  FDRE \add_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__1_n_6 ),
        .Q(D[9]),
        .R(aresetn));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter[0]_i_1__4 
       (.I0(\counter_reg_n_0_[0] ),
        .O(\counter[0]_i_1__4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT4 #(
    .INIT(16'hFD0E)) 
    \counter[1]_i_1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(CO),
        .I2(dp_extend_end_reg_0),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \counter[2]_i_1__3 
       (.I0(\counter_reg_n_0_[2] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[1] ),
        .O(\counter[2]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \counter[3]_i_1__3 
       (.I0(Q[0]),
        .I1(\counter_reg_n_0_[2] ),
        .I2(\counter_reg_n_0_[0] ),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[3]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \counter[4]_i_1__0 
       (.I0(Q[1]),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[0] ),
        .I3(\counter_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(\counter[4]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \counter[5]_i_1__2 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[1] ),
        .I5(Q[1]),
        .O(\counter[5]_i_1__2_n_0 ));
  LUT5 #(
    .INIT(32'hA6AAAAAA)) 
    \counter[6]_i_1__1 
       (.I0(\counter_reg_n_0_[6] ),
        .I1(Q[1]),
        .I2(\counter[7]_i_2__0_n_0 ),
        .I3(Q[0]),
        .I4(Q[2]),
        .O(\counter[6]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'hAA6AAAAAAAAAAAAA)) 
    \counter[7]_i_1__1 
       (.I0(\counter_reg_n_0_[7] ),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(\counter[7]_i_2__0_n_0 ),
        .I4(Q[1]),
        .I5(\counter_reg_n_0_[6] ),
        .O(\counter[7]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \counter[7]_i_2__0 
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[2] ),
        .O(\counter[7]_i_2__0_n_0 ));
  LUT4 #(
    .INIT(16'h6AAA)) 
    \counter[8]_i_2__2 
       (.I0(\counter_reg_n_0_[8] ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(\counter[8]_i_3__2_n_0 ),
        .I3(\counter_reg_n_0_[7] ),
        .O(\counter[8]_i_2__2_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter[8]_i_3__2 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[1] ),
        .I5(Q[1]),
        .O(\counter[8]_i_3__2_n_0 ));
  FDRE \counter_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[0]_i_1__4_n_0 ),
        .Q(\counter_reg_n_0_[0] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\counter[1]_i_1_n_0 ),
        .Q(\counter_reg_n_0_[1] ),
        .R(aresetn));
  FDRE \counter_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[2]_i_1__3_n_0 ),
        .Q(\counter_reg_n_0_[2] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[3]_i_1__3_n_0 ),
        .Q(Q[0]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[4]_i_1__0_n_0 ),
        .Q(Q[1]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[5]_i_1__2_n_0 ),
        .Q(Q[2]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[6]_i_1__1_n_0 ),
        .Q(\counter_reg_n_0_[6] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[7]_i_1__1_n_0 ),
        .Q(\counter_reg_n_0_[7] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[8]_i_2__2_n_0 ),
        .Q(\counter_reg_n_0_[8] ),
        .R(dp_extend_end_reg));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,b_ram_out[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({A[16],A[16],A[16],A[14:0]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r0_OVERFLOW_UNCONNECTED),
        .P({mul_r0_n_58,mul_r0_n_59,mul_r0_n_60,mul_r0_n_61,mul_r0_n_62,mul_r0_n_63,mul_r0_n_64,mul_r0_n_65,mul_r0_n_66,mul_r0_n_67,mul_r0_n_68,mul_r0_n_69,mul_r0_n_70,mul_r0_n_71,mul_r0_n_72,mul_r0_n_73,mul_r0_n_74,mul_r0_n_75,mul_r0_n_76,mul_r0_n_77,mul_r0_n_78,mul_r0_n_79,mul_r0_n_80,mul_r0_n_81,mul_r0_n_82,mul_r0_n_83,mul_r0_n_84,mul_r0_n_85,mul_r0_n_86,mul_r0_n_87,mul_r0_n_88,mul_r0_n_89,mul_r0_n_90,mul_r0_n_91,mul_r0_n_92,mul_r0_n_93,mul_r0_n_94,mul_r0_n_95,mul_r0_n_96,mul_r0_n_97,mul_r0_n_98,mul_r0_n_99,mul_r0_n_100,mul_r0_n_101,mul_r0_n_102,mul_r0_n_103,mul_r0_n_104,mul_r0_n_105}),
        .PATTERNBDETECT(NLW_mul_r0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_r0_UNDERFLOW_UNCONNECTED));
  FDRE \mul_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_105),
        .Q(mul_r_reg__1[0]),
        .R(aresetn));
  FDRE \mul_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_95),
        .Q(mul_r_reg__1[10]),
        .R(aresetn));
  FDRE \mul_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_94),
        .Q(mul_r_reg__1[11]),
        .R(aresetn));
  FDRE \mul_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_93),
        .Q(mul_r_reg__1[12]),
        .R(aresetn));
  FDRE \mul_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_92),
        .Q(mul_r_reg__1[13]),
        .R(aresetn));
  FDRE \mul_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_91),
        .Q(mul_r_reg__1[14]),
        .R(aresetn));
  FDRE \mul_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_90),
        .Q(mul_r_reg__1[15]),
        .R(aresetn));
  FDRE \mul_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_89),
        .Q(mul_r_reg__1[16]),
        .R(aresetn));
  FDRE \mul_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_104),
        .Q(mul_r_reg__1[1]),
        .R(aresetn));
  FDRE \mul_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_103),
        .Q(mul_r_reg__1[2]),
        .R(aresetn));
  FDRE \mul_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_102),
        .Q(mul_r_reg__1[3]),
        .R(aresetn));
  FDRE \mul_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_101),
        .Q(mul_r_reg__1[4]),
        .R(aresetn));
  FDRE \mul_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_100),
        .Q(mul_r_reg__1[5]),
        .R(aresetn));
  FDRE \mul_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_99),
        .Q(mul_r_reg__1[6]),
        .R(aresetn));
  FDRE \mul_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_98),
        .Q(mul_r_reg__1[7]),
        .R(aresetn));
  FDRE \mul_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_97),
        .Q(mul_r_reg__1[8]),
        .R(aresetn));
  FDRE \mul_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_96),
        .Q(mul_r_reg__1[9]),
        .R(aresetn));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-12 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r_reg__0
       (.A({A[16],A[16],A[16],A[16],A[16:15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r_reg__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({b_ram_out[31],b_ram_out[31],b_ram_out[31],b_ram_out[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r_reg__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(dp_enable),
        .CLK(clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED),
        .P({mul_r_reg__0_n_58,mul_r_reg__0_n_59,mul_r_reg__0_n_60,mul_r_reg__0_n_61,mul_r_reg__0_n_62,mul_r_reg__0_n_63,mul_r_reg__0_n_64,mul_r_reg__0_n_65,mul_r_reg__0_n_66,mul_r_reg__0_n_67,mul_r_reg__0_n_68,mul_r_reg__0_n_69,mul_r_reg__0_n_70,mul_r_reg__0_n_71,mul_r_reg__0_n_72,mul_r_reg__0_n_73,mul_r_reg__0_n_74,mul_r_reg__1[47:17]}),
        .PATTERNBDETECT(NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .PCOUT(NLW_mul_r_reg__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(aresetn),
        .UNDERFLOW(NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \res_mem[383]_i_2 
       (.I0(CO),
        .I1(\slv_regs_reg[0][11]_0 ),
        .I2(\slv_regs_reg[0][11]_1 ),
        .I3(\slv_regs_reg[0][11]_2 ),
        .O(\res_mem_reg[383] ));
endmodule

(* ORIG_REF_NAME = "dot_product" *) 
module design_1_top_0_0_dot_product_2
   (D,
    CO,
    Q,
    A,
    b_ram_out,
    dp_enable,
    clk,
    aresetn,
    \slv_regs_reg[0][11] ,
    O,
    \slv_regs_reg[0][0] ,
    \slv_regs_reg[0][0]_0 ,
    dp_extend_end_reg,
    dp_extend_end_reg_0);
  output [47:0]D;
  output [0:0]CO;
  output [5:0]Q;
  input [16:0]A;
  input [31:0]b_ram_out;
  input dp_enable;
  input clk;
  input aresetn;
  input [2:0]\slv_regs_reg[0][11] ;
  input [0:0]O;
  input [1:0]\slv_regs_reg[0][0] ;
  input [0:0]\slv_regs_reg[0][0]_0 ;
  input [0:0]dp_extend_end_reg;
  input dp_extend_end_reg_0;

  wire [16:0]A;
  wire [0:0]CO;
  wire [47:0]D;
  wire [0:0]O;
  wire [5:0]Q;
  wire add_r1_carry_i_4__2_n_0;
  wire add_r1_carry_n_0;
  wire add_r1_carry_n_1;
  wire add_r1_carry_n_2;
  wire add_r1_carry_n_3;
  wire \add_r[0]_i_2__2_n_0 ;
  wire \add_r[0]_i_3__2_n_0 ;
  wire \add_r[0]_i_4__2_n_0 ;
  wire \add_r[0]_i_5__2_n_0 ;
  wire \add_r[0]_i_6__2_n_0 ;
  wire \add_r[0]_i_7__2_n_0 ;
  wire \add_r[0]_i_8__2_n_0 ;
  wire \add_r[0]_i_9__2_n_0 ;
  wire \add_r[12]_i_2__2_n_0 ;
  wire \add_r[12]_i_3__2_n_0 ;
  wire \add_r[12]_i_4__2_n_0 ;
  wire \add_r[12]_i_5__2_n_0 ;
  wire \add_r[12]_i_6__2_n_0 ;
  wire \add_r[12]_i_7__2_n_0 ;
  wire \add_r[12]_i_8__2_n_0 ;
  wire \add_r[12]_i_9__2_n_0 ;
  wire \add_r[16]_i_2__2_n_0 ;
  wire \add_r[16]_i_3__2_n_0 ;
  wire \add_r[16]_i_4__2_n_0 ;
  wire \add_r[16]_i_5__2_n_0 ;
  wire \add_r[16]_i_6__2_n_0 ;
  wire \add_r[16]_i_7__2_n_0 ;
  wire \add_r[16]_i_8__2_n_0 ;
  wire \add_r[16]_i_9__2_n_0 ;
  wire \add_r[20]_i_2__2_n_0 ;
  wire \add_r[20]_i_3__2_n_0 ;
  wire \add_r[20]_i_4__2_n_0 ;
  wire \add_r[20]_i_5__2_n_0 ;
  wire \add_r[20]_i_6__2_n_0 ;
  wire \add_r[20]_i_7__2_n_0 ;
  wire \add_r[20]_i_8__2_n_0 ;
  wire \add_r[20]_i_9__2_n_0 ;
  wire \add_r[24]_i_2__2_n_0 ;
  wire \add_r[24]_i_3__2_n_0 ;
  wire \add_r[24]_i_4__2_n_0 ;
  wire \add_r[24]_i_5__2_n_0 ;
  wire \add_r[24]_i_6__2_n_0 ;
  wire \add_r[24]_i_7__2_n_0 ;
  wire \add_r[24]_i_8__2_n_0 ;
  wire \add_r[24]_i_9__2_n_0 ;
  wire \add_r[28]_i_2__2_n_0 ;
  wire \add_r[28]_i_3__2_n_0 ;
  wire \add_r[28]_i_4__2_n_0 ;
  wire \add_r[28]_i_5__2_n_0 ;
  wire \add_r[28]_i_6__2_n_0 ;
  wire \add_r[28]_i_7__2_n_0 ;
  wire \add_r[28]_i_8__2_n_0 ;
  wire \add_r[28]_i_9__2_n_0 ;
  wire \add_r[32]_i_2__2_n_0 ;
  wire \add_r[32]_i_3__2_n_0 ;
  wire \add_r[32]_i_4__2_n_0 ;
  wire \add_r[32]_i_5__2_n_0 ;
  wire \add_r[32]_i_6__2_n_0 ;
  wire \add_r[32]_i_7__2_n_0 ;
  wire \add_r[32]_i_8__2_n_0 ;
  wire \add_r[32]_i_9__2_n_0 ;
  wire \add_r[36]_i_2__2_n_0 ;
  wire \add_r[36]_i_3__2_n_0 ;
  wire \add_r[36]_i_4__2_n_0 ;
  wire \add_r[36]_i_5__2_n_0 ;
  wire \add_r[36]_i_6__2_n_0 ;
  wire \add_r[36]_i_7__2_n_0 ;
  wire \add_r[36]_i_8__2_n_0 ;
  wire \add_r[36]_i_9__2_n_0 ;
  wire \add_r[40]_i_2__2_n_0 ;
  wire \add_r[40]_i_3__2_n_0 ;
  wire \add_r[40]_i_4__2_n_0 ;
  wire \add_r[40]_i_5__2_n_0 ;
  wire \add_r[40]_i_6__2_n_0 ;
  wire \add_r[40]_i_7__2_n_0 ;
  wire \add_r[40]_i_8__2_n_0 ;
  wire \add_r[40]_i_9__2_n_0 ;
  wire \add_r[44]_i_2__2_n_0 ;
  wire \add_r[44]_i_3__2_n_0 ;
  wire \add_r[44]_i_4__2_n_0 ;
  wire \add_r[44]_i_5__2_n_0 ;
  wire \add_r[44]_i_6__2_n_0 ;
  wire \add_r[44]_i_7__2_n_0 ;
  wire \add_r[44]_i_8__2_n_0 ;
  wire \add_r[4]_i_2__2_n_0 ;
  wire \add_r[4]_i_3__2_n_0 ;
  wire \add_r[4]_i_4__2_n_0 ;
  wire \add_r[4]_i_5__2_n_0 ;
  wire \add_r[4]_i_6__2_n_0 ;
  wire \add_r[4]_i_7__2_n_0 ;
  wire \add_r[4]_i_8__2_n_0 ;
  wire \add_r[4]_i_9__2_n_0 ;
  wire \add_r[8]_i_2__2_n_0 ;
  wire \add_r[8]_i_3__2_n_0 ;
  wire \add_r[8]_i_4__2_n_0 ;
  wire \add_r[8]_i_5__2_n_0 ;
  wire \add_r[8]_i_6__2_n_0 ;
  wire \add_r[8]_i_7__2_n_0 ;
  wire \add_r[8]_i_8__2_n_0 ;
  wire \add_r[8]_i_9__2_n_0 ;
  wire \add_r_reg[0]_i_1__2_n_0 ;
  wire \add_r_reg[0]_i_1__2_n_1 ;
  wire \add_r_reg[0]_i_1__2_n_2 ;
  wire \add_r_reg[0]_i_1__2_n_3 ;
  wire \add_r_reg[0]_i_1__2_n_4 ;
  wire \add_r_reg[0]_i_1__2_n_5 ;
  wire \add_r_reg[0]_i_1__2_n_6 ;
  wire \add_r_reg[0]_i_1__2_n_7 ;
  wire \add_r_reg[12]_i_1__2_n_0 ;
  wire \add_r_reg[12]_i_1__2_n_1 ;
  wire \add_r_reg[12]_i_1__2_n_2 ;
  wire \add_r_reg[12]_i_1__2_n_3 ;
  wire \add_r_reg[12]_i_1__2_n_4 ;
  wire \add_r_reg[12]_i_1__2_n_5 ;
  wire \add_r_reg[12]_i_1__2_n_6 ;
  wire \add_r_reg[12]_i_1__2_n_7 ;
  wire \add_r_reg[16]_i_1__2_n_0 ;
  wire \add_r_reg[16]_i_1__2_n_1 ;
  wire \add_r_reg[16]_i_1__2_n_2 ;
  wire \add_r_reg[16]_i_1__2_n_3 ;
  wire \add_r_reg[16]_i_1__2_n_4 ;
  wire \add_r_reg[16]_i_1__2_n_5 ;
  wire \add_r_reg[16]_i_1__2_n_6 ;
  wire \add_r_reg[16]_i_1__2_n_7 ;
  wire \add_r_reg[20]_i_1__2_n_0 ;
  wire \add_r_reg[20]_i_1__2_n_1 ;
  wire \add_r_reg[20]_i_1__2_n_2 ;
  wire \add_r_reg[20]_i_1__2_n_3 ;
  wire \add_r_reg[20]_i_1__2_n_4 ;
  wire \add_r_reg[20]_i_1__2_n_5 ;
  wire \add_r_reg[20]_i_1__2_n_6 ;
  wire \add_r_reg[20]_i_1__2_n_7 ;
  wire \add_r_reg[24]_i_1__2_n_0 ;
  wire \add_r_reg[24]_i_1__2_n_1 ;
  wire \add_r_reg[24]_i_1__2_n_2 ;
  wire \add_r_reg[24]_i_1__2_n_3 ;
  wire \add_r_reg[24]_i_1__2_n_4 ;
  wire \add_r_reg[24]_i_1__2_n_5 ;
  wire \add_r_reg[24]_i_1__2_n_6 ;
  wire \add_r_reg[24]_i_1__2_n_7 ;
  wire \add_r_reg[28]_i_1__2_n_0 ;
  wire \add_r_reg[28]_i_1__2_n_1 ;
  wire \add_r_reg[28]_i_1__2_n_2 ;
  wire \add_r_reg[28]_i_1__2_n_3 ;
  wire \add_r_reg[28]_i_1__2_n_4 ;
  wire \add_r_reg[28]_i_1__2_n_5 ;
  wire \add_r_reg[28]_i_1__2_n_6 ;
  wire \add_r_reg[28]_i_1__2_n_7 ;
  wire \add_r_reg[32]_i_1__2_n_0 ;
  wire \add_r_reg[32]_i_1__2_n_1 ;
  wire \add_r_reg[32]_i_1__2_n_2 ;
  wire \add_r_reg[32]_i_1__2_n_3 ;
  wire \add_r_reg[32]_i_1__2_n_4 ;
  wire \add_r_reg[32]_i_1__2_n_5 ;
  wire \add_r_reg[32]_i_1__2_n_6 ;
  wire \add_r_reg[32]_i_1__2_n_7 ;
  wire \add_r_reg[36]_i_1__2_n_0 ;
  wire \add_r_reg[36]_i_1__2_n_1 ;
  wire \add_r_reg[36]_i_1__2_n_2 ;
  wire \add_r_reg[36]_i_1__2_n_3 ;
  wire \add_r_reg[36]_i_1__2_n_4 ;
  wire \add_r_reg[36]_i_1__2_n_5 ;
  wire \add_r_reg[36]_i_1__2_n_6 ;
  wire \add_r_reg[36]_i_1__2_n_7 ;
  wire \add_r_reg[40]_i_1__2_n_0 ;
  wire \add_r_reg[40]_i_1__2_n_1 ;
  wire \add_r_reg[40]_i_1__2_n_2 ;
  wire \add_r_reg[40]_i_1__2_n_3 ;
  wire \add_r_reg[40]_i_1__2_n_4 ;
  wire \add_r_reg[40]_i_1__2_n_5 ;
  wire \add_r_reg[40]_i_1__2_n_6 ;
  wire \add_r_reg[40]_i_1__2_n_7 ;
  wire \add_r_reg[44]_i_1__2_n_1 ;
  wire \add_r_reg[44]_i_1__2_n_2 ;
  wire \add_r_reg[44]_i_1__2_n_3 ;
  wire \add_r_reg[44]_i_1__2_n_4 ;
  wire \add_r_reg[44]_i_1__2_n_5 ;
  wire \add_r_reg[44]_i_1__2_n_6 ;
  wire \add_r_reg[44]_i_1__2_n_7 ;
  wire \add_r_reg[4]_i_1__2_n_0 ;
  wire \add_r_reg[4]_i_1__2_n_1 ;
  wire \add_r_reg[4]_i_1__2_n_2 ;
  wire \add_r_reg[4]_i_1__2_n_3 ;
  wire \add_r_reg[4]_i_1__2_n_4 ;
  wire \add_r_reg[4]_i_1__2_n_5 ;
  wire \add_r_reg[4]_i_1__2_n_6 ;
  wire \add_r_reg[4]_i_1__2_n_7 ;
  wire \add_r_reg[8]_i_1__2_n_0 ;
  wire \add_r_reg[8]_i_1__2_n_1 ;
  wire \add_r_reg[8]_i_1__2_n_2 ;
  wire \add_r_reg[8]_i_1__2_n_3 ;
  wire \add_r_reg[8]_i_1__2_n_4 ;
  wire \add_r_reg[8]_i_1__2_n_5 ;
  wire \add_r_reg[8]_i_1__2_n_6 ;
  wire \add_r_reg[8]_i_1__2_n_7 ;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \counter[0]_i_1__5_n_0 ;
  wire \counter[1]_i_1_n_0 ;
  wire \counter[2]_i_1__4_n_0 ;
  wire \counter[3]_i_1__4_n_0 ;
  wire \counter[4]_i_1__1_n_0 ;
  wire \counter[5]_i_1__3_n_0 ;
  wire \counter[6]_i_1__5_n_0 ;
  wire \counter[7]_i_1__2_n_0 ;
  wire \counter[7]_i_2__1_n_0 ;
  wire \counter[8]_i_2__3_n_0 ;
  wire \counter[8]_i_3__3_n_0 ;
  wire \counter_reg_n_0_[0] ;
  wire \counter_reg_n_0_[1] ;
  wire \counter_reg_n_0_[2] ;
  wire dp_enable;
  wire [0:0]dp_extend_end_reg;
  wire dp_extend_end_reg_0;
  wire mul_r0_n_100;
  wire mul_r0_n_101;
  wire mul_r0_n_102;
  wire mul_r0_n_103;
  wire mul_r0_n_104;
  wire mul_r0_n_105;
  wire mul_r0_n_106;
  wire mul_r0_n_107;
  wire mul_r0_n_108;
  wire mul_r0_n_109;
  wire mul_r0_n_110;
  wire mul_r0_n_111;
  wire mul_r0_n_112;
  wire mul_r0_n_113;
  wire mul_r0_n_114;
  wire mul_r0_n_115;
  wire mul_r0_n_116;
  wire mul_r0_n_117;
  wire mul_r0_n_118;
  wire mul_r0_n_119;
  wire mul_r0_n_120;
  wire mul_r0_n_121;
  wire mul_r0_n_122;
  wire mul_r0_n_123;
  wire mul_r0_n_124;
  wire mul_r0_n_125;
  wire mul_r0_n_126;
  wire mul_r0_n_127;
  wire mul_r0_n_128;
  wire mul_r0_n_129;
  wire mul_r0_n_130;
  wire mul_r0_n_131;
  wire mul_r0_n_132;
  wire mul_r0_n_133;
  wire mul_r0_n_134;
  wire mul_r0_n_135;
  wire mul_r0_n_136;
  wire mul_r0_n_137;
  wire mul_r0_n_138;
  wire mul_r0_n_139;
  wire mul_r0_n_140;
  wire mul_r0_n_141;
  wire mul_r0_n_142;
  wire mul_r0_n_143;
  wire mul_r0_n_144;
  wire mul_r0_n_145;
  wire mul_r0_n_146;
  wire mul_r0_n_147;
  wire mul_r0_n_148;
  wire mul_r0_n_149;
  wire mul_r0_n_150;
  wire mul_r0_n_151;
  wire mul_r0_n_152;
  wire mul_r0_n_153;
  wire mul_r0_n_58;
  wire mul_r0_n_59;
  wire mul_r0_n_60;
  wire mul_r0_n_61;
  wire mul_r0_n_62;
  wire mul_r0_n_63;
  wire mul_r0_n_64;
  wire mul_r0_n_65;
  wire mul_r0_n_66;
  wire mul_r0_n_67;
  wire mul_r0_n_68;
  wire mul_r0_n_69;
  wire mul_r0_n_70;
  wire mul_r0_n_71;
  wire mul_r0_n_72;
  wire mul_r0_n_73;
  wire mul_r0_n_74;
  wire mul_r0_n_75;
  wire mul_r0_n_76;
  wire mul_r0_n_77;
  wire mul_r0_n_78;
  wire mul_r0_n_79;
  wire mul_r0_n_80;
  wire mul_r0_n_81;
  wire mul_r0_n_82;
  wire mul_r0_n_83;
  wire mul_r0_n_84;
  wire mul_r0_n_85;
  wire mul_r0_n_86;
  wire mul_r0_n_87;
  wire mul_r0_n_88;
  wire mul_r0_n_89;
  wire mul_r0_n_90;
  wire mul_r0_n_91;
  wire mul_r0_n_92;
  wire mul_r0_n_93;
  wire mul_r0_n_94;
  wire mul_r0_n_95;
  wire mul_r0_n_96;
  wire mul_r0_n_97;
  wire mul_r0_n_98;
  wire mul_r0_n_99;
  wire mul_r_reg__0_n_58;
  wire mul_r_reg__0_n_59;
  wire mul_r_reg__0_n_60;
  wire mul_r_reg__0_n_61;
  wire mul_r_reg__0_n_62;
  wire mul_r_reg__0_n_63;
  wire mul_r_reg__0_n_64;
  wire mul_r_reg__0_n_65;
  wire mul_r_reg__0_n_66;
  wire mul_r_reg__0_n_67;
  wire mul_r_reg__0_n_68;
  wire mul_r_reg__0_n_69;
  wire mul_r_reg__0_n_70;
  wire mul_r_reg__0_n_71;
  wire mul_r_reg__0_n_72;
  wire mul_r_reg__0_n_73;
  wire mul_r_reg__0_n_74;
  wire [47:0]mul_r_reg__1;
  wire [1:0]\slv_regs_reg[0][0] ;
  wire [0:0]\slv_regs_reg[0][0]_0 ;
  wire [2:0]\slv_regs_reg[0][11] ;
  wire [3:0]NLW_add_r1_carry_O_UNCONNECTED;
  wire [3:1]NLW_add_r1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_add_r1_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_add_r_reg[44]_i_1__2_CO_UNCONNECTED ;
  wire NLW_mul_r0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r0_CARRYOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r_reg__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r_reg__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_r_reg__0_PCOUT_UNCONNECTED;

  CARRY4 add_r1_carry
       (.CI(1'b0),
        .CO({add_r1_carry_n_0,add_r1_carry_n_1,add_r1_carry_n_2,add_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry_O_UNCONNECTED[3:0]),
        .S({\slv_regs_reg[0][11] ,add_r1_carry_i_4__2_n_0}));
  CARRY4 add_r1_carry__0
       (.CI(add_r1_carry_n_0),
        .CO({NLW_add_r1_carry__0_CO_UNCONNECTED[3:1],CO}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,O}));
  LUT6 #(
    .INIT(64'h0990000000000990)) 
    add_r1_carry_i_4__2
       (.I0(\counter_reg_n_0_[2] ),
        .I1(\slv_regs_reg[0][0] [1]),
        .I2(\slv_regs_reg[0][0]_0 ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\slv_regs_reg[0][0] [0]),
        .I5(\counter_reg_n_0_[1] ),
        .O(add_r1_carry_i_4__2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_2__2 
       (.I0(mul_r_reg__1[3]),
        .I1(CO),
        .O(\add_r[0]_i_2__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_3__2 
       (.I0(mul_r_reg__1[2]),
        .I1(CO),
        .O(\add_r[0]_i_3__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_4__2 
       (.I0(mul_r_reg__1[1]),
        .I1(CO),
        .O(\add_r[0]_i_4__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_5__2 
       (.I0(mul_r_reg__1[0]),
        .I1(CO),
        .O(\add_r[0]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_6__2 
       (.I0(D[3]),
        .I1(CO),
        .I2(mul_r_reg__1[3]),
        .O(\add_r[0]_i_6__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_7__2 
       (.I0(D[2]),
        .I1(CO),
        .I2(mul_r_reg__1[2]),
        .O(\add_r[0]_i_7__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_8__2 
       (.I0(D[1]),
        .I1(CO),
        .I2(mul_r_reg__1[1]),
        .O(\add_r[0]_i_8__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_9__2 
       (.I0(D[0]),
        .I1(CO),
        .I2(mul_r_reg__1[0]),
        .O(\add_r[0]_i_9__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_2__2 
       (.I0(mul_r_reg__1[15]),
        .I1(CO),
        .O(\add_r[12]_i_2__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_3__2 
       (.I0(mul_r_reg__1[14]),
        .I1(CO),
        .O(\add_r[12]_i_3__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_4__2 
       (.I0(mul_r_reg__1[13]),
        .I1(CO),
        .O(\add_r[12]_i_4__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_5__2 
       (.I0(mul_r_reg__1[12]),
        .I1(CO),
        .O(\add_r[12]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_6__2 
       (.I0(D[15]),
        .I1(CO),
        .I2(mul_r_reg__1[15]),
        .O(\add_r[12]_i_6__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_7__2 
       (.I0(D[14]),
        .I1(CO),
        .I2(mul_r_reg__1[14]),
        .O(\add_r[12]_i_7__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_8__2 
       (.I0(D[13]),
        .I1(CO),
        .I2(mul_r_reg__1[13]),
        .O(\add_r[12]_i_8__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_9__2 
       (.I0(D[12]),
        .I1(CO),
        .I2(mul_r_reg__1[12]),
        .O(\add_r[12]_i_9__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_2__2 
       (.I0(mul_r_reg__1[19]),
        .I1(CO),
        .O(\add_r[16]_i_2__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_3__2 
       (.I0(mul_r_reg__1[18]),
        .I1(CO),
        .O(\add_r[16]_i_3__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_4__2 
       (.I0(mul_r_reg__1[17]),
        .I1(CO),
        .O(\add_r[16]_i_4__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_5__2 
       (.I0(mul_r_reg__1[16]),
        .I1(CO),
        .O(\add_r[16]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_6__2 
       (.I0(D[19]),
        .I1(CO),
        .I2(mul_r_reg__1[19]),
        .O(\add_r[16]_i_6__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_7__2 
       (.I0(D[18]),
        .I1(CO),
        .I2(mul_r_reg__1[18]),
        .O(\add_r[16]_i_7__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_8__2 
       (.I0(D[17]),
        .I1(CO),
        .I2(mul_r_reg__1[17]),
        .O(\add_r[16]_i_8__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_9__2 
       (.I0(D[16]),
        .I1(CO),
        .I2(mul_r_reg__1[16]),
        .O(\add_r[16]_i_9__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_2__2 
       (.I0(mul_r_reg__1[23]),
        .I1(CO),
        .O(\add_r[20]_i_2__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_3__2 
       (.I0(mul_r_reg__1[22]),
        .I1(CO),
        .O(\add_r[20]_i_3__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_4__2 
       (.I0(mul_r_reg__1[21]),
        .I1(CO),
        .O(\add_r[20]_i_4__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_5__2 
       (.I0(mul_r_reg__1[20]),
        .I1(CO),
        .O(\add_r[20]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_6__2 
       (.I0(D[23]),
        .I1(CO),
        .I2(mul_r_reg__1[23]),
        .O(\add_r[20]_i_6__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_7__2 
       (.I0(D[22]),
        .I1(CO),
        .I2(mul_r_reg__1[22]),
        .O(\add_r[20]_i_7__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_8__2 
       (.I0(D[21]),
        .I1(CO),
        .I2(mul_r_reg__1[21]),
        .O(\add_r[20]_i_8__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_9__2 
       (.I0(D[20]),
        .I1(CO),
        .I2(mul_r_reg__1[20]),
        .O(\add_r[20]_i_9__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_2__2 
       (.I0(mul_r_reg__1[27]),
        .I1(CO),
        .O(\add_r[24]_i_2__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_3__2 
       (.I0(mul_r_reg__1[26]),
        .I1(CO),
        .O(\add_r[24]_i_3__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_4__2 
       (.I0(mul_r_reg__1[25]),
        .I1(CO),
        .O(\add_r[24]_i_4__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_5__2 
       (.I0(mul_r_reg__1[24]),
        .I1(CO),
        .O(\add_r[24]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_6__2 
       (.I0(D[27]),
        .I1(CO),
        .I2(mul_r_reg__1[27]),
        .O(\add_r[24]_i_6__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_7__2 
       (.I0(D[26]),
        .I1(CO),
        .I2(mul_r_reg__1[26]),
        .O(\add_r[24]_i_7__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_8__2 
       (.I0(D[25]),
        .I1(CO),
        .I2(mul_r_reg__1[25]),
        .O(\add_r[24]_i_8__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_9__2 
       (.I0(D[24]),
        .I1(CO),
        .I2(mul_r_reg__1[24]),
        .O(\add_r[24]_i_9__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_2__2 
       (.I0(mul_r_reg__1[31]),
        .I1(CO),
        .O(\add_r[28]_i_2__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_3__2 
       (.I0(mul_r_reg__1[30]),
        .I1(CO),
        .O(\add_r[28]_i_3__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_4__2 
       (.I0(mul_r_reg__1[29]),
        .I1(CO),
        .O(\add_r[28]_i_4__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_5__2 
       (.I0(mul_r_reg__1[28]),
        .I1(CO),
        .O(\add_r[28]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_6__2 
       (.I0(D[31]),
        .I1(CO),
        .I2(mul_r_reg__1[31]),
        .O(\add_r[28]_i_6__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_7__2 
       (.I0(D[30]),
        .I1(CO),
        .I2(mul_r_reg__1[30]),
        .O(\add_r[28]_i_7__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_8__2 
       (.I0(D[29]),
        .I1(CO),
        .I2(mul_r_reg__1[29]),
        .O(\add_r[28]_i_8__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_9__2 
       (.I0(D[28]),
        .I1(CO),
        .I2(mul_r_reg__1[28]),
        .O(\add_r[28]_i_9__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_2__2 
       (.I0(mul_r_reg__1[35]),
        .I1(CO),
        .O(\add_r[32]_i_2__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_3__2 
       (.I0(mul_r_reg__1[34]),
        .I1(CO),
        .O(\add_r[32]_i_3__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_4__2 
       (.I0(mul_r_reg__1[33]),
        .I1(CO),
        .O(\add_r[32]_i_4__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_5__2 
       (.I0(mul_r_reg__1[32]),
        .I1(CO),
        .O(\add_r[32]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_6__2 
       (.I0(D[35]),
        .I1(CO),
        .I2(mul_r_reg__1[35]),
        .O(\add_r[32]_i_6__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_7__2 
       (.I0(D[34]),
        .I1(CO),
        .I2(mul_r_reg__1[34]),
        .O(\add_r[32]_i_7__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_8__2 
       (.I0(D[33]),
        .I1(CO),
        .I2(mul_r_reg__1[33]),
        .O(\add_r[32]_i_8__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_9__2 
       (.I0(D[32]),
        .I1(CO),
        .I2(mul_r_reg__1[32]),
        .O(\add_r[32]_i_9__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_2__2 
       (.I0(mul_r_reg__1[39]),
        .I1(CO),
        .O(\add_r[36]_i_2__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_3__2 
       (.I0(mul_r_reg__1[38]),
        .I1(CO),
        .O(\add_r[36]_i_3__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_4__2 
       (.I0(mul_r_reg__1[37]),
        .I1(CO),
        .O(\add_r[36]_i_4__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_5__2 
       (.I0(mul_r_reg__1[36]),
        .I1(CO),
        .O(\add_r[36]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_6__2 
       (.I0(D[39]),
        .I1(CO),
        .I2(mul_r_reg__1[39]),
        .O(\add_r[36]_i_6__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_7__2 
       (.I0(D[38]),
        .I1(CO),
        .I2(mul_r_reg__1[38]),
        .O(\add_r[36]_i_7__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_8__2 
       (.I0(D[37]),
        .I1(CO),
        .I2(mul_r_reg__1[37]),
        .O(\add_r[36]_i_8__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_9__2 
       (.I0(D[36]),
        .I1(CO),
        .I2(mul_r_reg__1[36]),
        .O(\add_r[36]_i_9__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_2__2 
       (.I0(mul_r_reg__1[43]),
        .I1(CO),
        .O(\add_r[40]_i_2__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_3__2 
       (.I0(mul_r_reg__1[42]),
        .I1(CO),
        .O(\add_r[40]_i_3__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_4__2 
       (.I0(mul_r_reg__1[41]),
        .I1(CO),
        .O(\add_r[40]_i_4__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_5__2 
       (.I0(mul_r_reg__1[40]),
        .I1(CO),
        .O(\add_r[40]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_6__2 
       (.I0(D[43]),
        .I1(CO),
        .I2(mul_r_reg__1[43]),
        .O(\add_r[40]_i_6__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_7__2 
       (.I0(D[42]),
        .I1(CO),
        .I2(mul_r_reg__1[42]),
        .O(\add_r[40]_i_7__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_8__2 
       (.I0(D[41]),
        .I1(CO),
        .I2(mul_r_reg__1[41]),
        .O(\add_r[40]_i_8__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_9__2 
       (.I0(D[40]),
        .I1(CO),
        .I2(mul_r_reg__1[40]),
        .O(\add_r[40]_i_9__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_2__2 
       (.I0(mul_r_reg__1[46]),
        .I1(CO),
        .O(\add_r[44]_i_2__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_3__2 
       (.I0(mul_r_reg__1[45]),
        .I1(CO),
        .O(\add_r[44]_i_3__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_4__2 
       (.I0(mul_r_reg__1[44]),
        .I1(CO),
        .O(\add_r[44]_i_4__2_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_5__2 
       (.I0(mul_r_reg__1[47]),
        .I1(CO),
        .I2(D[47]),
        .O(\add_r[44]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_6__2 
       (.I0(D[46]),
        .I1(CO),
        .I2(mul_r_reg__1[46]),
        .O(\add_r[44]_i_6__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_7__2 
       (.I0(D[45]),
        .I1(CO),
        .I2(mul_r_reg__1[45]),
        .O(\add_r[44]_i_7__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_8__2 
       (.I0(D[44]),
        .I1(CO),
        .I2(mul_r_reg__1[44]),
        .O(\add_r[44]_i_8__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_2__2 
       (.I0(mul_r_reg__1[7]),
        .I1(CO),
        .O(\add_r[4]_i_2__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_3__2 
       (.I0(mul_r_reg__1[6]),
        .I1(CO),
        .O(\add_r[4]_i_3__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_4__2 
       (.I0(mul_r_reg__1[5]),
        .I1(CO),
        .O(\add_r[4]_i_4__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_5__2 
       (.I0(mul_r_reg__1[4]),
        .I1(CO),
        .O(\add_r[4]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_6__2 
       (.I0(D[7]),
        .I1(CO),
        .I2(mul_r_reg__1[7]),
        .O(\add_r[4]_i_6__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_7__2 
       (.I0(D[6]),
        .I1(CO),
        .I2(mul_r_reg__1[6]),
        .O(\add_r[4]_i_7__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_8__2 
       (.I0(D[5]),
        .I1(CO),
        .I2(mul_r_reg__1[5]),
        .O(\add_r[4]_i_8__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_9__2 
       (.I0(D[4]),
        .I1(CO),
        .I2(mul_r_reg__1[4]),
        .O(\add_r[4]_i_9__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_2__2 
       (.I0(mul_r_reg__1[11]),
        .I1(CO),
        .O(\add_r[8]_i_2__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_3__2 
       (.I0(mul_r_reg__1[10]),
        .I1(CO),
        .O(\add_r[8]_i_3__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_4__2 
       (.I0(mul_r_reg__1[9]),
        .I1(CO),
        .O(\add_r[8]_i_4__2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_5__2 
       (.I0(mul_r_reg__1[8]),
        .I1(CO),
        .O(\add_r[8]_i_5__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_6__2 
       (.I0(D[11]),
        .I1(CO),
        .I2(mul_r_reg__1[11]),
        .O(\add_r[8]_i_6__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_7__2 
       (.I0(D[10]),
        .I1(CO),
        .I2(mul_r_reg__1[10]),
        .O(\add_r[8]_i_7__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_8__2 
       (.I0(D[9]),
        .I1(CO),
        .I2(mul_r_reg__1[9]),
        .O(\add_r[8]_i_8__2_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_9__2 
       (.I0(D[8]),
        .I1(CO),
        .I2(mul_r_reg__1[8]),
        .O(\add_r[8]_i_9__2_n_0 ));
  FDRE \add_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__2_n_7 ),
        .Q(D[0]),
        .R(aresetn));
  CARRY4 \add_r_reg[0]_i_1__2 
       (.CI(1'b0),
        .CO({\add_r_reg[0]_i_1__2_n_0 ,\add_r_reg[0]_i_1__2_n_1 ,\add_r_reg[0]_i_1__2_n_2 ,\add_r_reg[0]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[0]_i_2__2_n_0 ,\add_r[0]_i_3__2_n_0 ,\add_r[0]_i_4__2_n_0 ,\add_r[0]_i_5__2_n_0 }),
        .O({\add_r_reg[0]_i_1__2_n_4 ,\add_r_reg[0]_i_1__2_n_5 ,\add_r_reg[0]_i_1__2_n_6 ,\add_r_reg[0]_i_1__2_n_7 }),
        .S({\add_r[0]_i_6__2_n_0 ,\add_r[0]_i_7__2_n_0 ,\add_r[0]_i_8__2_n_0 ,\add_r[0]_i_9__2_n_0 }));
  FDRE \add_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__2_n_5 ),
        .Q(D[10]),
        .R(aresetn));
  FDRE \add_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__2_n_4 ),
        .Q(D[11]),
        .R(aresetn));
  FDRE \add_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__2_n_7 ),
        .Q(D[12]),
        .R(aresetn));
  CARRY4 \add_r_reg[12]_i_1__2 
       (.CI(\add_r_reg[8]_i_1__2_n_0 ),
        .CO({\add_r_reg[12]_i_1__2_n_0 ,\add_r_reg[12]_i_1__2_n_1 ,\add_r_reg[12]_i_1__2_n_2 ,\add_r_reg[12]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[12]_i_2__2_n_0 ,\add_r[12]_i_3__2_n_0 ,\add_r[12]_i_4__2_n_0 ,\add_r[12]_i_5__2_n_0 }),
        .O({\add_r_reg[12]_i_1__2_n_4 ,\add_r_reg[12]_i_1__2_n_5 ,\add_r_reg[12]_i_1__2_n_6 ,\add_r_reg[12]_i_1__2_n_7 }),
        .S({\add_r[12]_i_6__2_n_0 ,\add_r[12]_i_7__2_n_0 ,\add_r[12]_i_8__2_n_0 ,\add_r[12]_i_9__2_n_0 }));
  FDRE \add_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__2_n_6 ),
        .Q(D[13]),
        .R(aresetn));
  FDRE \add_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__2_n_5 ),
        .Q(D[14]),
        .R(aresetn));
  FDRE \add_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__2_n_4 ),
        .Q(D[15]),
        .R(aresetn));
  FDRE \add_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__2_n_7 ),
        .Q(D[16]),
        .R(aresetn));
  CARRY4 \add_r_reg[16]_i_1__2 
       (.CI(\add_r_reg[12]_i_1__2_n_0 ),
        .CO({\add_r_reg[16]_i_1__2_n_0 ,\add_r_reg[16]_i_1__2_n_1 ,\add_r_reg[16]_i_1__2_n_2 ,\add_r_reg[16]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[16]_i_2__2_n_0 ,\add_r[16]_i_3__2_n_0 ,\add_r[16]_i_4__2_n_0 ,\add_r[16]_i_5__2_n_0 }),
        .O({\add_r_reg[16]_i_1__2_n_4 ,\add_r_reg[16]_i_1__2_n_5 ,\add_r_reg[16]_i_1__2_n_6 ,\add_r_reg[16]_i_1__2_n_7 }),
        .S({\add_r[16]_i_6__2_n_0 ,\add_r[16]_i_7__2_n_0 ,\add_r[16]_i_8__2_n_0 ,\add_r[16]_i_9__2_n_0 }));
  FDRE \add_r_reg[17] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__2_n_6 ),
        .Q(D[17]),
        .R(aresetn));
  FDRE \add_r_reg[18] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__2_n_5 ),
        .Q(D[18]),
        .R(aresetn));
  FDRE \add_r_reg[19] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__2_n_4 ),
        .Q(D[19]),
        .R(aresetn));
  FDRE \add_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__2_n_6 ),
        .Q(D[1]),
        .R(aresetn));
  FDRE \add_r_reg[20] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__2_n_7 ),
        .Q(D[20]),
        .R(aresetn));
  CARRY4 \add_r_reg[20]_i_1__2 
       (.CI(\add_r_reg[16]_i_1__2_n_0 ),
        .CO({\add_r_reg[20]_i_1__2_n_0 ,\add_r_reg[20]_i_1__2_n_1 ,\add_r_reg[20]_i_1__2_n_2 ,\add_r_reg[20]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[20]_i_2__2_n_0 ,\add_r[20]_i_3__2_n_0 ,\add_r[20]_i_4__2_n_0 ,\add_r[20]_i_5__2_n_0 }),
        .O({\add_r_reg[20]_i_1__2_n_4 ,\add_r_reg[20]_i_1__2_n_5 ,\add_r_reg[20]_i_1__2_n_6 ,\add_r_reg[20]_i_1__2_n_7 }),
        .S({\add_r[20]_i_6__2_n_0 ,\add_r[20]_i_7__2_n_0 ,\add_r[20]_i_8__2_n_0 ,\add_r[20]_i_9__2_n_0 }));
  FDRE \add_r_reg[21] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__2_n_6 ),
        .Q(D[21]),
        .R(aresetn));
  FDRE \add_r_reg[22] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__2_n_5 ),
        .Q(D[22]),
        .R(aresetn));
  FDRE \add_r_reg[23] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__2_n_4 ),
        .Q(D[23]),
        .R(aresetn));
  FDRE \add_r_reg[24] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__2_n_7 ),
        .Q(D[24]),
        .R(aresetn));
  CARRY4 \add_r_reg[24]_i_1__2 
       (.CI(\add_r_reg[20]_i_1__2_n_0 ),
        .CO({\add_r_reg[24]_i_1__2_n_0 ,\add_r_reg[24]_i_1__2_n_1 ,\add_r_reg[24]_i_1__2_n_2 ,\add_r_reg[24]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[24]_i_2__2_n_0 ,\add_r[24]_i_3__2_n_0 ,\add_r[24]_i_4__2_n_0 ,\add_r[24]_i_5__2_n_0 }),
        .O({\add_r_reg[24]_i_1__2_n_4 ,\add_r_reg[24]_i_1__2_n_5 ,\add_r_reg[24]_i_1__2_n_6 ,\add_r_reg[24]_i_1__2_n_7 }),
        .S({\add_r[24]_i_6__2_n_0 ,\add_r[24]_i_7__2_n_0 ,\add_r[24]_i_8__2_n_0 ,\add_r[24]_i_9__2_n_0 }));
  FDRE \add_r_reg[25] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__2_n_6 ),
        .Q(D[25]),
        .R(aresetn));
  FDRE \add_r_reg[26] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__2_n_5 ),
        .Q(D[26]),
        .R(aresetn));
  FDRE \add_r_reg[27] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__2_n_4 ),
        .Q(D[27]),
        .R(aresetn));
  FDRE \add_r_reg[28] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__2_n_7 ),
        .Q(D[28]),
        .R(aresetn));
  CARRY4 \add_r_reg[28]_i_1__2 
       (.CI(\add_r_reg[24]_i_1__2_n_0 ),
        .CO({\add_r_reg[28]_i_1__2_n_0 ,\add_r_reg[28]_i_1__2_n_1 ,\add_r_reg[28]_i_1__2_n_2 ,\add_r_reg[28]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[28]_i_2__2_n_0 ,\add_r[28]_i_3__2_n_0 ,\add_r[28]_i_4__2_n_0 ,\add_r[28]_i_5__2_n_0 }),
        .O({\add_r_reg[28]_i_1__2_n_4 ,\add_r_reg[28]_i_1__2_n_5 ,\add_r_reg[28]_i_1__2_n_6 ,\add_r_reg[28]_i_1__2_n_7 }),
        .S({\add_r[28]_i_6__2_n_0 ,\add_r[28]_i_7__2_n_0 ,\add_r[28]_i_8__2_n_0 ,\add_r[28]_i_9__2_n_0 }));
  FDRE \add_r_reg[29] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__2_n_6 ),
        .Q(D[29]),
        .R(aresetn));
  FDRE \add_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__2_n_5 ),
        .Q(D[2]),
        .R(aresetn));
  FDRE \add_r_reg[30] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__2_n_5 ),
        .Q(D[30]),
        .R(aresetn));
  FDRE \add_r_reg[31] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__2_n_4 ),
        .Q(D[31]),
        .R(aresetn));
  FDRE \add_r_reg[32] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__2_n_7 ),
        .Q(D[32]),
        .R(aresetn));
  CARRY4 \add_r_reg[32]_i_1__2 
       (.CI(\add_r_reg[28]_i_1__2_n_0 ),
        .CO({\add_r_reg[32]_i_1__2_n_0 ,\add_r_reg[32]_i_1__2_n_1 ,\add_r_reg[32]_i_1__2_n_2 ,\add_r_reg[32]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[32]_i_2__2_n_0 ,\add_r[32]_i_3__2_n_0 ,\add_r[32]_i_4__2_n_0 ,\add_r[32]_i_5__2_n_0 }),
        .O({\add_r_reg[32]_i_1__2_n_4 ,\add_r_reg[32]_i_1__2_n_5 ,\add_r_reg[32]_i_1__2_n_6 ,\add_r_reg[32]_i_1__2_n_7 }),
        .S({\add_r[32]_i_6__2_n_0 ,\add_r[32]_i_7__2_n_0 ,\add_r[32]_i_8__2_n_0 ,\add_r[32]_i_9__2_n_0 }));
  FDRE \add_r_reg[33] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__2_n_6 ),
        .Q(D[33]),
        .R(aresetn));
  FDRE \add_r_reg[34] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__2_n_5 ),
        .Q(D[34]),
        .R(aresetn));
  FDRE \add_r_reg[35] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__2_n_4 ),
        .Q(D[35]),
        .R(aresetn));
  FDRE \add_r_reg[36] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__2_n_7 ),
        .Q(D[36]),
        .R(aresetn));
  CARRY4 \add_r_reg[36]_i_1__2 
       (.CI(\add_r_reg[32]_i_1__2_n_0 ),
        .CO({\add_r_reg[36]_i_1__2_n_0 ,\add_r_reg[36]_i_1__2_n_1 ,\add_r_reg[36]_i_1__2_n_2 ,\add_r_reg[36]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[36]_i_2__2_n_0 ,\add_r[36]_i_3__2_n_0 ,\add_r[36]_i_4__2_n_0 ,\add_r[36]_i_5__2_n_0 }),
        .O({\add_r_reg[36]_i_1__2_n_4 ,\add_r_reg[36]_i_1__2_n_5 ,\add_r_reg[36]_i_1__2_n_6 ,\add_r_reg[36]_i_1__2_n_7 }),
        .S({\add_r[36]_i_6__2_n_0 ,\add_r[36]_i_7__2_n_0 ,\add_r[36]_i_8__2_n_0 ,\add_r[36]_i_9__2_n_0 }));
  FDRE \add_r_reg[37] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__2_n_6 ),
        .Q(D[37]),
        .R(aresetn));
  FDRE \add_r_reg[38] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__2_n_5 ),
        .Q(D[38]),
        .R(aresetn));
  FDRE \add_r_reg[39] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__2_n_4 ),
        .Q(D[39]),
        .R(aresetn));
  FDRE \add_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__2_n_4 ),
        .Q(D[3]),
        .R(aresetn));
  FDRE \add_r_reg[40] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__2_n_7 ),
        .Q(D[40]),
        .R(aresetn));
  CARRY4 \add_r_reg[40]_i_1__2 
       (.CI(\add_r_reg[36]_i_1__2_n_0 ),
        .CO({\add_r_reg[40]_i_1__2_n_0 ,\add_r_reg[40]_i_1__2_n_1 ,\add_r_reg[40]_i_1__2_n_2 ,\add_r_reg[40]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[40]_i_2__2_n_0 ,\add_r[40]_i_3__2_n_0 ,\add_r[40]_i_4__2_n_0 ,\add_r[40]_i_5__2_n_0 }),
        .O({\add_r_reg[40]_i_1__2_n_4 ,\add_r_reg[40]_i_1__2_n_5 ,\add_r_reg[40]_i_1__2_n_6 ,\add_r_reg[40]_i_1__2_n_7 }),
        .S({\add_r[40]_i_6__2_n_0 ,\add_r[40]_i_7__2_n_0 ,\add_r[40]_i_8__2_n_0 ,\add_r[40]_i_9__2_n_0 }));
  FDRE \add_r_reg[41] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__2_n_6 ),
        .Q(D[41]),
        .R(aresetn));
  FDRE \add_r_reg[42] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__2_n_5 ),
        .Q(D[42]),
        .R(aresetn));
  FDRE \add_r_reg[43] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__2_n_4 ),
        .Q(D[43]),
        .R(aresetn));
  FDRE \add_r_reg[44] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__2_n_7 ),
        .Q(D[44]),
        .R(aresetn));
  CARRY4 \add_r_reg[44]_i_1__2 
       (.CI(\add_r_reg[40]_i_1__2_n_0 ),
        .CO({\NLW_add_r_reg[44]_i_1__2_CO_UNCONNECTED [3],\add_r_reg[44]_i_1__2_n_1 ,\add_r_reg[44]_i_1__2_n_2 ,\add_r_reg[44]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\add_r[44]_i_2__2_n_0 ,\add_r[44]_i_3__2_n_0 ,\add_r[44]_i_4__2_n_0 }),
        .O({\add_r_reg[44]_i_1__2_n_4 ,\add_r_reg[44]_i_1__2_n_5 ,\add_r_reg[44]_i_1__2_n_6 ,\add_r_reg[44]_i_1__2_n_7 }),
        .S({\add_r[44]_i_5__2_n_0 ,\add_r[44]_i_6__2_n_0 ,\add_r[44]_i_7__2_n_0 ,\add_r[44]_i_8__2_n_0 }));
  FDRE \add_r_reg[45] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__2_n_6 ),
        .Q(D[45]),
        .R(aresetn));
  FDRE \add_r_reg[46] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__2_n_5 ),
        .Q(D[46]),
        .R(aresetn));
  FDRE \add_r_reg[47] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__2_n_4 ),
        .Q(D[47]),
        .R(aresetn));
  FDRE \add_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__2_n_7 ),
        .Q(D[4]),
        .R(aresetn));
  CARRY4 \add_r_reg[4]_i_1__2 
       (.CI(\add_r_reg[0]_i_1__2_n_0 ),
        .CO({\add_r_reg[4]_i_1__2_n_0 ,\add_r_reg[4]_i_1__2_n_1 ,\add_r_reg[4]_i_1__2_n_2 ,\add_r_reg[4]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[4]_i_2__2_n_0 ,\add_r[4]_i_3__2_n_0 ,\add_r[4]_i_4__2_n_0 ,\add_r[4]_i_5__2_n_0 }),
        .O({\add_r_reg[4]_i_1__2_n_4 ,\add_r_reg[4]_i_1__2_n_5 ,\add_r_reg[4]_i_1__2_n_6 ,\add_r_reg[4]_i_1__2_n_7 }),
        .S({\add_r[4]_i_6__2_n_0 ,\add_r[4]_i_7__2_n_0 ,\add_r[4]_i_8__2_n_0 ,\add_r[4]_i_9__2_n_0 }));
  FDRE \add_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__2_n_6 ),
        .Q(D[5]),
        .R(aresetn));
  FDRE \add_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__2_n_5 ),
        .Q(D[6]),
        .R(aresetn));
  FDRE \add_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__2_n_4 ),
        .Q(D[7]),
        .R(aresetn));
  FDRE \add_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__2_n_7 ),
        .Q(D[8]),
        .R(aresetn));
  CARRY4 \add_r_reg[8]_i_1__2 
       (.CI(\add_r_reg[4]_i_1__2_n_0 ),
        .CO({\add_r_reg[8]_i_1__2_n_0 ,\add_r_reg[8]_i_1__2_n_1 ,\add_r_reg[8]_i_1__2_n_2 ,\add_r_reg[8]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[8]_i_2__2_n_0 ,\add_r[8]_i_3__2_n_0 ,\add_r[8]_i_4__2_n_0 ,\add_r[8]_i_5__2_n_0 }),
        .O({\add_r_reg[8]_i_1__2_n_4 ,\add_r_reg[8]_i_1__2_n_5 ,\add_r_reg[8]_i_1__2_n_6 ,\add_r_reg[8]_i_1__2_n_7 }),
        .S({\add_r[8]_i_6__2_n_0 ,\add_r[8]_i_7__2_n_0 ,\add_r[8]_i_8__2_n_0 ,\add_r[8]_i_9__2_n_0 }));
  FDRE \add_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__2_n_6 ),
        .Q(D[9]),
        .R(aresetn));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter[0]_i_1__5 
       (.I0(\counter_reg_n_0_[0] ),
        .O(\counter[0]_i_1__5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT4 #(
    .INIT(16'hFD0E)) 
    \counter[1]_i_1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(CO),
        .I2(dp_extend_end_reg_0),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \counter[2]_i_1__4 
       (.I0(\counter_reg_n_0_[2] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[1] ),
        .O(\counter[2]_i_1__4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \counter[3]_i_1__4 
       (.I0(Q[0]),
        .I1(\counter_reg_n_0_[2] ),
        .I2(\counter_reg_n_0_[0] ),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[3]_i_1__4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \counter[4]_i_1__1 
       (.I0(Q[1]),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[0] ),
        .I3(\counter_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(\counter[4]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \counter[5]_i_1__3 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[1] ),
        .I5(Q[1]),
        .O(\counter[5]_i_1__3_n_0 ));
  LUT5 #(
    .INIT(32'hA6AAAAAA)) 
    \counter[6]_i_1__5 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(\counter[7]_i_2__1_n_0 ),
        .I3(Q[0]),
        .I4(Q[2]),
        .O(\counter[6]_i_1__5_n_0 ));
  LUT6 #(
    .INIT(64'hAA6AAAAAAAAAAAAA)) 
    \counter[7]_i_1__2 
       (.I0(Q[4]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(\counter[7]_i_2__1_n_0 ),
        .I4(Q[1]),
        .I5(Q[3]),
        .O(\counter[7]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \counter[7]_i_2__1 
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[2] ),
        .O(\counter[7]_i_2__1_n_0 ));
  LUT4 #(
    .INIT(16'hA6AA)) 
    \counter[8]_i_2__3 
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(\counter[8]_i_3__3_n_0 ),
        .I3(Q[4]),
        .O(\counter[8]_i_2__3_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \counter[8]_i_3__3 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[1] ),
        .I5(Q[1]),
        .O(\counter[8]_i_3__3_n_0 ));
  FDRE \counter_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[0]_i_1__5_n_0 ),
        .Q(\counter_reg_n_0_[0] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\counter[1]_i_1_n_0 ),
        .Q(\counter_reg_n_0_[1] ),
        .R(aresetn));
  FDRE \counter_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[2]_i_1__4_n_0 ),
        .Q(\counter_reg_n_0_[2] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[3]_i_1__4_n_0 ),
        .Q(Q[0]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[4]_i_1__1_n_0 ),
        .Q(Q[1]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[5]_i_1__3_n_0 ),
        .Q(Q[2]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[6]_i_1__5_n_0 ),
        .Q(Q[3]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[7]_i_1__2_n_0 ),
        .Q(Q[4]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[8]_i_2__3_n_0 ),
        .Q(Q[5]),
        .R(dp_extend_end_reg));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,b_ram_out[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({A[16],A[16],A[16],A[14:0]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r0_OVERFLOW_UNCONNECTED),
        .P({mul_r0_n_58,mul_r0_n_59,mul_r0_n_60,mul_r0_n_61,mul_r0_n_62,mul_r0_n_63,mul_r0_n_64,mul_r0_n_65,mul_r0_n_66,mul_r0_n_67,mul_r0_n_68,mul_r0_n_69,mul_r0_n_70,mul_r0_n_71,mul_r0_n_72,mul_r0_n_73,mul_r0_n_74,mul_r0_n_75,mul_r0_n_76,mul_r0_n_77,mul_r0_n_78,mul_r0_n_79,mul_r0_n_80,mul_r0_n_81,mul_r0_n_82,mul_r0_n_83,mul_r0_n_84,mul_r0_n_85,mul_r0_n_86,mul_r0_n_87,mul_r0_n_88,mul_r0_n_89,mul_r0_n_90,mul_r0_n_91,mul_r0_n_92,mul_r0_n_93,mul_r0_n_94,mul_r0_n_95,mul_r0_n_96,mul_r0_n_97,mul_r0_n_98,mul_r0_n_99,mul_r0_n_100,mul_r0_n_101,mul_r0_n_102,mul_r0_n_103,mul_r0_n_104,mul_r0_n_105}),
        .PATTERNBDETECT(NLW_mul_r0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_r0_UNDERFLOW_UNCONNECTED));
  FDRE \mul_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_105),
        .Q(mul_r_reg__1[0]),
        .R(aresetn));
  FDRE \mul_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_95),
        .Q(mul_r_reg__1[10]),
        .R(aresetn));
  FDRE \mul_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_94),
        .Q(mul_r_reg__1[11]),
        .R(aresetn));
  FDRE \mul_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_93),
        .Q(mul_r_reg__1[12]),
        .R(aresetn));
  FDRE \mul_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_92),
        .Q(mul_r_reg__1[13]),
        .R(aresetn));
  FDRE \mul_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_91),
        .Q(mul_r_reg__1[14]),
        .R(aresetn));
  FDRE \mul_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_90),
        .Q(mul_r_reg__1[15]),
        .R(aresetn));
  FDRE \mul_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_89),
        .Q(mul_r_reg__1[16]),
        .R(aresetn));
  FDRE \mul_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_104),
        .Q(mul_r_reg__1[1]),
        .R(aresetn));
  FDRE \mul_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_103),
        .Q(mul_r_reg__1[2]),
        .R(aresetn));
  FDRE \mul_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_102),
        .Q(mul_r_reg__1[3]),
        .R(aresetn));
  FDRE \mul_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_101),
        .Q(mul_r_reg__1[4]),
        .R(aresetn));
  FDRE \mul_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_100),
        .Q(mul_r_reg__1[5]),
        .R(aresetn));
  FDRE \mul_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_99),
        .Q(mul_r_reg__1[6]),
        .R(aresetn));
  FDRE \mul_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_98),
        .Q(mul_r_reg__1[7]),
        .R(aresetn));
  FDRE \mul_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_97),
        .Q(mul_r_reg__1[8]),
        .R(aresetn));
  FDRE \mul_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_96),
        .Q(mul_r_reg__1[9]),
        .R(aresetn));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-12 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r_reg__0
       (.A({A[16],A[16],A[16],A[16],A[16],A[16],A[16:15],A[15],A[15],A[15],A[15],A[15],A[15],A[15:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r_reg__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({b_ram_out[31],b_ram_out[31],b_ram_out[31],b_ram_out[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r_reg__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(dp_enable),
        .CLK(clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED),
        .P({mul_r_reg__0_n_58,mul_r_reg__0_n_59,mul_r_reg__0_n_60,mul_r_reg__0_n_61,mul_r_reg__0_n_62,mul_r_reg__0_n_63,mul_r_reg__0_n_64,mul_r_reg__0_n_65,mul_r_reg__0_n_66,mul_r_reg__0_n_67,mul_r_reg__0_n_68,mul_r_reg__0_n_69,mul_r_reg__0_n_70,mul_r_reg__0_n_71,mul_r_reg__0_n_72,mul_r_reg__0_n_73,mul_r_reg__0_n_74,mul_r_reg__1[47:17]}),
        .PATTERNBDETECT(NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .PCOUT(NLW_mul_r_reg__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(aresetn),
        .UNDERFLOW(NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "dot_product" *) 
module design_1_top_0_0_dot_product_3
   (D,
    CO,
    Q,
    B,
    b_ram_out,
    dp_enable,
    clk,
    aresetn,
    \in_raw_delay_reg[15]_rep__5 ,
    \slv_regs_reg[0][11] ,
    O,
    \slv_regs_reg[0][0] ,
    \slv_regs_reg[0][0]_0 ,
    dp_extend_end_reg,
    dp_extend_end_reg_0);
  output [47:0]D;
  output [0:0]CO;
  output [5:0]Q;
  input [16:0]B;
  input [31:0]b_ram_out;
  input dp_enable;
  input clk;
  input aresetn;
  input [0:0]\in_raw_delay_reg[15]_rep__5 ;
  input [2:0]\slv_regs_reg[0][11] ;
  input [0:0]O;
  input [1:0]\slv_regs_reg[0][0] ;
  input [0:0]\slv_regs_reg[0][0]_0 ;
  input [0:0]dp_extend_end_reg;
  input dp_extend_end_reg_0;

  wire [16:0]B;
  wire [0:0]CO;
  wire [47:0]D;
  wire [0:0]O;
  wire [5:0]Q;
  wire add_r1_carry_i_4__3_n_0;
  wire add_r1_carry_n_0;
  wire add_r1_carry_n_1;
  wire add_r1_carry_n_2;
  wire add_r1_carry_n_3;
  wire \add_r[0]_i_2__3_n_0 ;
  wire \add_r[0]_i_3__3_n_0 ;
  wire \add_r[0]_i_4__3_n_0 ;
  wire \add_r[0]_i_5__3_n_0 ;
  wire \add_r[0]_i_6__3_n_0 ;
  wire \add_r[0]_i_7__3_n_0 ;
  wire \add_r[0]_i_8__3_n_0 ;
  wire \add_r[0]_i_9__3_n_0 ;
  wire \add_r[12]_i_2__3_n_0 ;
  wire \add_r[12]_i_3__3_n_0 ;
  wire \add_r[12]_i_4__3_n_0 ;
  wire \add_r[12]_i_5__3_n_0 ;
  wire \add_r[12]_i_6__3_n_0 ;
  wire \add_r[12]_i_7__3_n_0 ;
  wire \add_r[12]_i_8__3_n_0 ;
  wire \add_r[12]_i_9__3_n_0 ;
  wire \add_r[16]_i_2__3_n_0 ;
  wire \add_r[16]_i_3__3_n_0 ;
  wire \add_r[16]_i_4__3_n_0 ;
  wire \add_r[16]_i_5__3_n_0 ;
  wire \add_r[16]_i_6__3_n_0 ;
  wire \add_r[16]_i_7__3_n_0 ;
  wire \add_r[16]_i_8__3_n_0 ;
  wire \add_r[16]_i_9__3_n_0 ;
  wire \add_r[20]_i_2__3_n_0 ;
  wire \add_r[20]_i_3__3_n_0 ;
  wire \add_r[20]_i_4__3_n_0 ;
  wire \add_r[20]_i_5__3_n_0 ;
  wire \add_r[20]_i_6__3_n_0 ;
  wire \add_r[20]_i_7__3_n_0 ;
  wire \add_r[20]_i_8__3_n_0 ;
  wire \add_r[20]_i_9__3_n_0 ;
  wire \add_r[24]_i_2__3_n_0 ;
  wire \add_r[24]_i_3__3_n_0 ;
  wire \add_r[24]_i_4__3_n_0 ;
  wire \add_r[24]_i_5__3_n_0 ;
  wire \add_r[24]_i_6__3_n_0 ;
  wire \add_r[24]_i_7__3_n_0 ;
  wire \add_r[24]_i_8__3_n_0 ;
  wire \add_r[24]_i_9__3_n_0 ;
  wire \add_r[28]_i_2__3_n_0 ;
  wire \add_r[28]_i_3__3_n_0 ;
  wire \add_r[28]_i_4__3_n_0 ;
  wire \add_r[28]_i_5__3_n_0 ;
  wire \add_r[28]_i_6__3_n_0 ;
  wire \add_r[28]_i_7__3_n_0 ;
  wire \add_r[28]_i_8__3_n_0 ;
  wire \add_r[28]_i_9__3_n_0 ;
  wire \add_r[32]_i_2__3_n_0 ;
  wire \add_r[32]_i_3__3_n_0 ;
  wire \add_r[32]_i_4__3_n_0 ;
  wire \add_r[32]_i_5__3_n_0 ;
  wire \add_r[32]_i_6__3_n_0 ;
  wire \add_r[32]_i_7__3_n_0 ;
  wire \add_r[32]_i_8__3_n_0 ;
  wire \add_r[32]_i_9__3_n_0 ;
  wire \add_r[36]_i_2__3_n_0 ;
  wire \add_r[36]_i_3__3_n_0 ;
  wire \add_r[36]_i_4__3_n_0 ;
  wire \add_r[36]_i_5__3_n_0 ;
  wire \add_r[36]_i_6__3_n_0 ;
  wire \add_r[36]_i_7__3_n_0 ;
  wire \add_r[36]_i_8__3_n_0 ;
  wire \add_r[36]_i_9__3_n_0 ;
  wire \add_r[40]_i_2__3_n_0 ;
  wire \add_r[40]_i_3__3_n_0 ;
  wire \add_r[40]_i_4__3_n_0 ;
  wire \add_r[40]_i_5__3_n_0 ;
  wire \add_r[40]_i_6__3_n_0 ;
  wire \add_r[40]_i_7__3_n_0 ;
  wire \add_r[40]_i_8__3_n_0 ;
  wire \add_r[40]_i_9__3_n_0 ;
  wire \add_r[44]_i_2__3_n_0 ;
  wire \add_r[44]_i_3__3_n_0 ;
  wire \add_r[44]_i_4__3_n_0 ;
  wire \add_r[44]_i_5__3_n_0 ;
  wire \add_r[44]_i_6__3_n_0 ;
  wire \add_r[44]_i_7__3_n_0 ;
  wire \add_r[44]_i_8__3_n_0 ;
  wire \add_r[4]_i_2__3_n_0 ;
  wire \add_r[4]_i_3__3_n_0 ;
  wire \add_r[4]_i_4__3_n_0 ;
  wire \add_r[4]_i_5__3_n_0 ;
  wire \add_r[4]_i_6__3_n_0 ;
  wire \add_r[4]_i_7__3_n_0 ;
  wire \add_r[4]_i_8__3_n_0 ;
  wire \add_r[4]_i_9__3_n_0 ;
  wire \add_r[8]_i_2__3_n_0 ;
  wire \add_r[8]_i_3__3_n_0 ;
  wire \add_r[8]_i_4__3_n_0 ;
  wire \add_r[8]_i_5__3_n_0 ;
  wire \add_r[8]_i_6__3_n_0 ;
  wire \add_r[8]_i_7__3_n_0 ;
  wire \add_r[8]_i_8__3_n_0 ;
  wire \add_r[8]_i_9__3_n_0 ;
  wire \add_r_reg[0]_i_1__3_n_0 ;
  wire \add_r_reg[0]_i_1__3_n_1 ;
  wire \add_r_reg[0]_i_1__3_n_2 ;
  wire \add_r_reg[0]_i_1__3_n_3 ;
  wire \add_r_reg[0]_i_1__3_n_4 ;
  wire \add_r_reg[0]_i_1__3_n_5 ;
  wire \add_r_reg[0]_i_1__3_n_6 ;
  wire \add_r_reg[0]_i_1__3_n_7 ;
  wire \add_r_reg[12]_i_1__3_n_0 ;
  wire \add_r_reg[12]_i_1__3_n_1 ;
  wire \add_r_reg[12]_i_1__3_n_2 ;
  wire \add_r_reg[12]_i_1__3_n_3 ;
  wire \add_r_reg[12]_i_1__3_n_4 ;
  wire \add_r_reg[12]_i_1__3_n_5 ;
  wire \add_r_reg[12]_i_1__3_n_6 ;
  wire \add_r_reg[12]_i_1__3_n_7 ;
  wire \add_r_reg[16]_i_1__3_n_0 ;
  wire \add_r_reg[16]_i_1__3_n_1 ;
  wire \add_r_reg[16]_i_1__3_n_2 ;
  wire \add_r_reg[16]_i_1__3_n_3 ;
  wire \add_r_reg[16]_i_1__3_n_4 ;
  wire \add_r_reg[16]_i_1__3_n_5 ;
  wire \add_r_reg[16]_i_1__3_n_6 ;
  wire \add_r_reg[16]_i_1__3_n_7 ;
  wire \add_r_reg[20]_i_1__3_n_0 ;
  wire \add_r_reg[20]_i_1__3_n_1 ;
  wire \add_r_reg[20]_i_1__3_n_2 ;
  wire \add_r_reg[20]_i_1__3_n_3 ;
  wire \add_r_reg[20]_i_1__3_n_4 ;
  wire \add_r_reg[20]_i_1__3_n_5 ;
  wire \add_r_reg[20]_i_1__3_n_6 ;
  wire \add_r_reg[20]_i_1__3_n_7 ;
  wire \add_r_reg[24]_i_1__3_n_0 ;
  wire \add_r_reg[24]_i_1__3_n_1 ;
  wire \add_r_reg[24]_i_1__3_n_2 ;
  wire \add_r_reg[24]_i_1__3_n_3 ;
  wire \add_r_reg[24]_i_1__3_n_4 ;
  wire \add_r_reg[24]_i_1__3_n_5 ;
  wire \add_r_reg[24]_i_1__3_n_6 ;
  wire \add_r_reg[24]_i_1__3_n_7 ;
  wire \add_r_reg[28]_i_1__3_n_0 ;
  wire \add_r_reg[28]_i_1__3_n_1 ;
  wire \add_r_reg[28]_i_1__3_n_2 ;
  wire \add_r_reg[28]_i_1__3_n_3 ;
  wire \add_r_reg[28]_i_1__3_n_4 ;
  wire \add_r_reg[28]_i_1__3_n_5 ;
  wire \add_r_reg[28]_i_1__3_n_6 ;
  wire \add_r_reg[28]_i_1__3_n_7 ;
  wire \add_r_reg[32]_i_1__3_n_0 ;
  wire \add_r_reg[32]_i_1__3_n_1 ;
  wire \add_r_reg[32]_i_1__3_n_2 ;
  wire \add_r_reg[32]_i_1__3_n_3 ;
  wire \add_r_reg[32]_i_1__3_n_4 ;
  wire \add_r_reg[32]_i_1__3_n_5 ;
  wire \add_r_reg[32]_i_1__3_n_6 ;
  wire \add_r_reg[32]_i_1__3_n_7 ;
  wire \add_r_reg[36]_i_1__3_n_0 ;
  wire \add_r_reg[36]_i_1__3_n_1 ;
  wire \add_r_reg[36]_i_1__3_n_2 ;
  wire \add_r_reg[36]_i_1__3_n_3 ;
  wire \add_r_reg[36]_i_1__3_n_4 ;
  wire \add_r_reg[36]_i_1__3_n_5 ;
  wire \add_r_reg[36]_i_1__3_n_6 ;
  wire \add_r_reg[36]_i_1__3_n_7 ;
  wire \add_r_reg[40]_i_1__3_n_0 ;
  wire \add_r_reg[40]_i_1__3_n_1 ;
  wire \add_r_reg[40]_i_1__3_n_2 ;
  wire \add_r_reg[40]_i_1__3_n_3 ;
  wire \add_r_reg[40]_i_1__3_n_4 ;
  wire \add_r_reg[40]_i_1__3_n_5 ;
  wire \add_r_reg[40]_i_1__3_n_6 ;
  wire \add_r_reg[40]_i_1__3_n_7 ;
  wire \add_r_reg[44]_i_1__3_n_1 ;
  wire \add_r_reg[44]_i_1__3_n_2 ;
  wire \add_r_reg[44]_i_1__3_n_3 ;
  wire \add_r_reg[44]_i_1__3_n_4 ;
  wire \add_r_reg[44]_i_1__3_n_5 ;
  wire \add_r_reg[44]_i_1__3_n_6 ;
  wire \add_r_reg[44]_i_1__3_n_7 ;
  wire \add_r_reg[4]_i_1__3_n_0 ;
  wire \add_r_reg[4]_i_1__3_n_1 ;
  wire \add_r_reg[4]_i_1__3_n_2 ;
  wire \add_r_reg[4]_i_1__3_n_3 ;
  wire \add_r_reg[4]_i_1__3_n_4 ;
  wire \add_r_reg[4]_i_1__3_n_5 ;
  wire \add_r_reg[4]_i_1__3_n_6 ;
  wire \add_r_reg[4]_i_1__3_n_7 ;
  wire \add_r_reg[8]_i_1__3_n_0 ;
  wire \add_r_reg[8]_i_1__3_n_1 ;
  wire \add_r_reg[8]_i_1__3_n_2 ;
  wire \add_r_reg[8]_i_1__3_n_3 ;
  wire \add_r_reg[8]_i_1__3_n_4 ;
  wire \add_r_reg[8]_i_1__3_n_5 ;
  wire \add_r_reg[8]_i_1__3_n_6 ;
  wire \add_r_reg[8]_i_1__3_n_7 ;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \counter[0]_i_1__6_n_0 ;
  wire \counter[1]_i_1_n_0 ;
  wire \counter[2]_i_1__5_n_0 ;
  wire \counter[3]_i_1__5_n_0 ;
  wire \counter[4]_i_1__2_n_0 ;
  wire \counter[5]_i_1__4_n_0 ;
  wire \counter[6]_i_1__6_n_0 ;
  wire \counter[7]_i_1__3_n_0 ;
  wire \counter[7]_i_2__2_n_0 ;
  wire \counter[8]_i_2__4_n_0 ;
  wire \counter[8]_i_3__4_n_0 ;
  wire \counter_reg_n_0_[0] ;
  wire \counter_reg_n_0_[1] ;
  wire \counter_reg_n_0_[2] ;
  wire dp_enable;
  wire [0:0]dp_extend_end_reg;
  wire dp_extend_end_reg_0;
  wire [0:0]\in_raw_delay_reg[15]_rep__5 ;
  wire mul_r0_n_100;
  wire mul_r0_n_101;
  wire mul_r0_n_102;
  wire mul_r0_n_103;
  wire mul_r0_n_104;
  wire mul_r0_n_105;
  wire mul_r0_n_106;
  wire mul_r0_n_107;
  wire mul_r0_n_108;
  wire mul_r0_n_109;
  wire mul_r0_n_110;
  wire mul_r0_n_111;
  wire mul_r0_n_112;
  wire mul_r0_n_113;
  wire mul_r0_n_114;
  wire mul_r0_n_115;
  wire mul_r0_n_116;
  wire mul_r0_n_117;
  wire mul_r0_n_118;
  wire mul_r0_n_119;
  wire mul_r0_n_120;
  wire mul_r0_n_121;
  wire mul_r0_n_122;
  wire mul_r0_n_123;
  wire mul_r0_n_124;
  wire mul_r0_n_125;
  wire mul_r0_n_126;
  wire mul_r0_n_127;
  wire mul_r0_n_128;
  wire mul_r0_n_129;
  wire mul_r0_n_130;
  wire mul_r0_n_131;
  wire mul_r0_n_132;
  wire mul_r0_n_133;
  wire mul_r0_n_134;
  wire mul_r0_n_135;
  wire mul_r0_n_136;
  wire mul_r0_n_137;
  wire mul_r0_n_138;
  wire mul_r0_n_139;
  wire mul_r0_n_140;
  wire mul_r0_n_141;
  wire mul_r0_n_142;
  wire mul_r0_n_143;
  wire mul_r0_n_144;
  wire mul_r0_n_145;
  wire mul_r0_n_146;
  wire mul_r0_n_147;
  wire mul_r0_n_148;
  wire mul_r0_n_149;
  wire mul_r0_n_150;
  wire mul_r0_n_151;
  wire mul_r0_n_152;
  wire mul_r0_n_153;
  wire mul_r0_n_58;
  wire mul_r0_n_59;
  wire mul_r0_n_60;
  wire mul_r0_n_61;
  wire mul_r0_n_62;
  wire mul_r0_n_63;
  wire mul_r0_n_64;
  wire mul_r0_n_65;
  wire mul_r0_n_66;
  wire mul_r0_n_67;
  wire mul_r0_n_68;
  wire mul_r0_n_69;
  wire mul_r0_n_70;
  wire mul_r0_n_71;
  wire mul_r0_n_72;
  wire mul_r0_n_73;
  wire mul_r0_n_74;
  wire mul_r0_n_75;
  wire mul_r0_n_76;
  wire mul_r0_n_77;
  wire mul_r0_n_78;
  wire mul_r0_n_79;
  wire mul_r0_n_80;
  wire mul_r0_n_81;
  wire mul_r0_n_82;
  wire mul_r0_n_83;
  wire mul_r0_n_84;
  wire mul_r0_n_85;
  wire mul_r0_n_86;
  wire mul_r0_n_87;
  wire mul_r0_n_88;
  wire mul_r0_n_89;
  wire mul_r0_n_90;
  wire mul_r0_n_91;
  wire mul_r0_n_92;
  wire mul_r0_n_93;
  wire mul_r0_n_94;
  wire mul_r0_n_95;
  wire mul_r0_n_96;
  wire mul_r0_n_97;
  wire mul_r0_n_98;
  wire mul_r0_n_99;
  wire mul_r_reg__0_n_58;
  wire mul_r_reg__0_n_59;
  wire mul_r_reg__0_n_60;
  wire mul_r_reg__0_n_61;
  wire mul_r_reg__0_n_62;
  wire mul_r_reg__0_n_63;
  wire mul_r_reg__0_n_64;
  wire mul_r_reg__0_n_65;
  wire mul_r_reg__0_n_66;
  wire mul_r_reg__0_n_67;
  wire mul_r_reg__0_n_68;
  wire mul_r_reg__0_n_69;
  wire mul_r_reg__0_n_70;
  wire mul_r_reg__0_n_71;
  wire mul_r_reg__0_n_72;
  wire mul_r_reg__0_n_73;
  wire mul_r_reg__0_n_74;
  wire [47:0]mul_r_reg__1;
  wire [1:0]\slv_regs_reg[0][0] ;
  wire [0:0]\slv_regs_reg[0][0]_0 ;
  wire [2:0]\slv_regs_reg[0][11] ;
  wire [3:0]NLW_add_r1_carry_O_UNCONNECTED;
  wire [3:1]NLW_add_r1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_add_r1_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_add_r_reg[44]_i_1__3_CO_UNCONNECTED ;
  wire NLW_mul_r0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r0_CARRYOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r_reg__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r_reg__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_r_reg__0_PCOUT_UNCONNECTED;

  CARRY4 add_r1_carry
       (.CI(1'b0),
        .CO({add_r1_carry_n_0,add_r1_carry_n_1,add_r1_carry_n_2,add_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry_O_UNCONNECTED[3:0]),
        .S({\slv_regs_reg[0][11] ,add_r1_carry_i_4__3_n_0}));
  CARRY4 add_r1_carry__0
       (.CI(add_r1_carry_n_0),
        .CO({NLW_add_r1_carry__0_CO_UNCONNECTED[3:1],CO}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,O}));
  LUT6 #(
    .INIT(64'h0990000000000990)) 
    add_r1_carry_i_4__3
       (.I0(\counter_reg_n_0_[2] ),
        .I1(\slv_regs_reg[0][0] [1]),
        .I2(\slv_regs_reg[0][0]_0 ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\slv_regs_reg[0][0] [0]),
        .I5(\counter_reg_n_0_[1] ),
        .O(add_r1_carry_i_4__3_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_2__3 
       (.I0(mul_r_reg__1[3]),
        .I1(CO),
        .O(\add_r[0]_i_2__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_3__3 
       (.I0(mul_r_reg__1[2]),
        .I1(CO),
        .O(\add_r[0]_i_3__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_4__3 
       (.I0(mul_r_reg__1[1]),
        .I1(CO),
        .O(\add_r[0]_i_4__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_5__3 
       (.I0(mul_r_reg__1[0]),
        .I1(CO),
        .O(\add_r[0]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_6__3 
       (.I0(D[3]),
        .I1(CO),
        .I2(mul_r_reg__1[3]),
        .O(\add_r[0]_i_6__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_7__3 
       (.I0(D[2]),
        .I1(CO),
        .I2(mul_r_reg__1[2]),
        .O(\add_r[0]_i_7__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_8__3 
       (.I0(D[1]),
        .I1(CO),
        .I2(mul_r_reg__1[1]),
        .O(\add_r[0]_i_8__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_9__3 
       (.I0(D[0]),
        .I1(CO),
        .I2(mul_r_reg__1[0]),
        .O(\add_r[0]_i_9__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_2__3 
       (.I0(mul_r_reg__1[15]),
        .I1(CO),
        .O(\add_r[12]_i_2__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_3__3 
       (.I0(mul_r_reg__1[14]),
        .I1(CO),
        .O(\add_r[12]_i_3__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_4__3 
       (.I0(mul_r_reg__1[13]),
        .I1(CO),
        .O(\add_r[12]_i_4__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_5__3 
       (.I0(mul_r_reg__1[12]),
        .I1(CO),
        .O(\add_r[12]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_6__3 
       (.I0(D[15]),
        .I1(CO),
        .I2(mul_r_reg__1[15]),
        .O(\add_r[12]_i_6__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_7__3 
       (.I0(D[14]),
        .I1(CO),
        .I2(mul_r_reg__1[14]),
        .O(\add_r[12]_i_7__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_8__3 
       (.I0(D[13]),
        .I1(CO),
        .I2(mul_r_reg__1[13]),
        .O(\add_r[12]_i_8__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_9__3 
       (.I0(D[12]),
        .I1(CO),
        .I2(mul_r_reg__1[12]),
        .O(\add_r[12]_i_9__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_2__3 
       (.I0(mul_r_reg__1[19]),
        .I1(CO),
        .O(\add_r[16]_i_2__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_3__3 
       (.I0(mul_r_reg__1[18]),
        .I1(CO),
        .O(\add_r[16]_i_3__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_4__3 
       (.I0(mul_r_reg__1[17]),
        .I1(CO),
        .O(\add_r[16]_i_4__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_5__3 
       (.I0(mul_r_reg__1[16]),
        .I1(CO),
        .O(\add_r[16]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_6__3 
       (.I0(D[19]),
        .I1(CO),
        .I2(mul_r_reg__1[19]),
        .O(\add_r[16]_i_6__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_7__3 
       (.I0(D[18]),
        .I1(CO),
        .I2(mul_r_reg__1[18]),
        .O(\add_r[16]_i_7__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_8__3 
       (.I0(D[17]),
        .I1(CO),
        .I2(mul_r_reg__1[17]),
        .O(\add_r[16]_i_8__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_9__3 
       (.I0(D[16]),
        .I1(CO),
        .I2(mul_r_reg__1[16]),
        .O(\add_r[16]_i_9__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_2__3 
       (.I0(mul_r_reg__1[23]),
        .I1(CO),
        .O(\add_r[20]_i_2__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_3__3 
       (.I0(mul_r_reg__1[22]),
        .I1(CO),
        .O(\add_r[20]_i_3__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_4__3 
       (.I0(mul_r_reg__1[21]),
        .I1(CO),
        .O(\add_r[20]_i_4__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_5__3 
       (.I0(mul_r_reg__1[20]),
        .I1(CO),
        .O(\add_r[20]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_6__3 
       (.I0(D[23]),
        .I1(CO),
        .I2(mul_r_reg__1[23]),
        .O(\add_r[20]_i_6__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_7__3 
       (.I0(D[22]),
        .I1(CO),
        .I2(mul_r_reg__1[22]),
        .O(\add_r[20]_i_7__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_8__3 
       (.I0(D[21]),
        .I1(CO),
        .I2(mul_r_reg__1[21]),
        .O(\add_r[20]_i_8__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_9__3 
       (.I0(D[20]),
        .I1(CO),
        .I2(mul_r_reg__1[20]),
        .O(\add_r[20]_i_9__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_2__3 
       (.I0(mul_r_reg__1[27]),
        .I1(CO),
        .O(\add_r[24]_i_2__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_3__3 
       (.I0(mul_r_reg__1[26]),
        .I1(CO),
        .O(\add_r[24]_i_3__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_4__3 
       (.I0(mul_r_reg__1[25]),
        .I1(CO),
        .O(\add_r[24]_i_4__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_5__3 
       (.I0(mul_r_reg__1[24]),
        .I1(CO),
        .O(\add_r[24]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_6__3 
       (.I0(D[27]),
        .I1(CO),
        .I2(mul_r_reg__1[27]),
        .O(\add_r[24]_i_6__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_7__3 
       (.I0(D[26]),
        .I1(CO),
        .I2(mul_r_reg__1[26]),
        .O(\add_r[24]_i_7__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_8__3 
       (.I0(D[25]),
        .I1(CO),
        .I2(mul_r_reg__1[25]),
        .O(\add_r[24]_i_8__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_9__3 
       (.I0(D[24]),
        .I1(CO),
        .I2(mul_r_reg__1[24]),
        .O(\add_r[24]_i_9__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_2__3 
       (.I0(mul_r_reg__1[31]),
        .I1(CO),
        .O(\add_r[28]_i_2__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_3__3 
       (.I0(mul_r_reg__1[30]),
        .I1(CO),
        .O(\add_r[28]_i_3__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_4__3 
       (.I0(mul_r_reg__1[29]),
        .I1(CO),
        .O(\add_r[28]_i_4__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_5__3 
       (.I0(mul_r_reg__1[28]),
        .I1(CO),
        .O(\add_r[28]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_6__3 
       (.I0(D[31]),
        .I1(CO),
        .I2(mul_r_reg__1[31]),
        .O(\add_r[28]_i_6__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_7__3 
       (.I0(D[30]),
        .I1(CO),
        .I2(mul_r_reg__1[30]),
        .O(\add_r[28]_i_7__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_8__3 
       (.I0(D[29]),
        .I1(CO),
        .I2(mul_r_reg__1[29]),
        .O(\add_r[28]_i_8__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_9__3 
       (.I0(D[28]),
        .I1(CO),
        .I2(mul_r_reg__1[28]),
        .O(\add_r[28]_i_9__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_2__3 
       (.I0(mul_r_reg__1[35]),
        .I1(CO),
        .O(\add_r[32]_i_2__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_3__3 
       (.I0(mul_r_reg__1[34]),
        .I1(CO),
        .O(\add_r[32]_i_3__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_4__3 
       (.I0(mul_r_reg__1[33]),
        .I1(CO),
        .O(\add_r[32]_i_4__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_5__3 
       (.I0(mul_r_reg__1[32]),
        .I1(CO),
        .O(\add_r[32]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_6__3 
       (.I0(D[35]),
        .I1(CO),
        .I2(mul_r_reg__1[35]),
        .O(\add_r[32]_i_6__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_7__3 
       (.I0(D[34]),
        .I1(CO),
        .I2(mul_r_reg__1[34]),
        .O(\add_r[32]_i_7__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_8__3 
       (.I0(D[33]),
        .I1(CO),
        .I2(mul_r_reg__1[33]),
        .O(\add_r[32]_i_8__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_9__3 
       (.I0(D[32]),
        .I1(CO),
        .I2(mul_r_reg__1[32]),
        .O(\add_r[32]_i_9__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_2__3 
       (.I0(mul_r_reg__1[39]),
        .I1(CO),
        .O(\add_r[36]_i_2__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_3__3 
       (.I0(mul_r_reg__1[38]),
        .I1(CO),
        .O(\add_r[36]_i_3__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_4__3 
       (.I0(mul_r_reg__1[37]),
        .I1(CO),
        .O(\add_r[36]_i_4__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_5__3 
       (.I0(mul_r_reg__1[36]),
        .I1(CO),
        .O(\add_r[36]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_6__3 
       (.I0(D[39]),
        .I1(CO),
        .I2(mul_r_reg__1[39]),
        .O(\add_r[36]_i_6__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_7__3 
       (.I0(D[38]),
        .I1(CO),
        .I2(mul_r_reg__1[38]),
        .O(\add_r[36]_i_7__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_8__3 
       (.I0(D[37]),
        .I1(CO),
        .I2(mul_r_reg__1[37]),
        .O(\add_r[36]_i_8__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_9__3 
       (.I0(D[36]),
        .I1(CO),
        .I2(mul_r_reg__1[36]),
        .O(\add_r[36]_i_9__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_2__3 
       (.I0(mul_r_reg__1[43]),
        .I1(CO),
        .O(\add_r[40]_i_2__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_3__3 
       (.I0(mul_r_reg__1[42]),
        .I1(CO),
        .O(\add_r[40]_i_3__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_4__3 
       (.I0(mul_r_reg__1[41]),
        .I1(CO),
        .O(\add_r[40]_i_4__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_5__3 
       (.I0(mul_r_reg__1[40]),
        .I1(CO),
        .O(\add_r[40]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_6__3 
       (.I0(D[43]),
        .I1(CO),
        .I2(mul_r_reg__1[43]),
        .O(\add_r[40]_i_6__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_7__3 
       (.I0(D[42]),
        .I1(CO),
        .I2(mul_r_reg__1[42]),
        .O(\add_r[40]_i_7__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_8__3 
       (.I0(D[41]),
        .I1(CO),
        .I2(mul_r_reg__1[41]),
        .O(\add_r[40]_i_8__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_9__3 
       (.I0(D[40]),
        .I1(CO),
        .I2(mul_r_reg__1[40]),
        .O(\add_r[40]_i_9__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_2__3 
       (.I0(mul_r_reg__1[46]),
        .I1(CO),
        .O(\add_r[44]_i_2__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_3__3 
       (.I0(mul_r_reg__1[45]),
        .I1(CO),
        .O(\add_r[44]_i_3__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_4__3 
       (.I0(mul_r_reg__1[44]),
        .I1(CO),
        .O(\add_r[44]_i_4__3_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_5__3 
       (.I0(mul_r_reg__1[47]),
        .I1(CO),
        .I2(D[47]),
        .O(\add_r[44]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_6__3 
       (.I0(D[46]),
        .I1(CO),
        .I2(mul_r_reg__1[46]),
        .O(\add_r[44]_i_6__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_7__3 
       (.I0(D[45]),
        .I1(CO),
        .I2(mul_r_reg__1[45]),
        .O(\add_r[44]_i_7__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_8__3 
       (.I0(D[44]),
        .I1(CO),
        .I2(mul_r_reg__1[44]),
        .O(\add_r[44]_i_8__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_2__3 
       (.I0(mul_r_reg__1[7]),
        .I1(CO),
        .O(\add_r[4]_i_2__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_3__3 
       (.I0(mul_r_reg__1[6]),
        .I1(CO),
        .O(\add_r[4]_i_3__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_4__3 
       (.I0(mul_r_reg__1[5]),
        .I1(CO),
        .O(\add_r[4]_i_4__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_5__3 
       (.I0(mul_r_reg__1[4]),
        .I1(CO),
        .O(\add_r[4]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_6__3 
       (.I0(D[7]),
        .I1(CO),
        .I2(mul_r_reg__1[7]),
        .O(\add_r[4]_i_6__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_7__3 
       (.I0(D[6]),
        .I1(CO),
        .I2(mul_r_reg__1[6]),
        .O(\add_r[4]_i_7__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_8__3 
       (.I0(D[5]),
        .I1(CO),
        .I2(mul_r_reg__1[5]),
        .O(\add_r[4]_i_8__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_9__3 
       (.I0(D[4]),
        .I1(CO),
        .I2(mul_r_reg__1[4]),
        .O(\add_r[4]_i_9__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_2__3 
       (.I0(mul_r_reg__1[11]),
        .I1(CO),
        .O(\add_r[8]_i_2__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_3__3 
       (.I0(mul_r_reg__1[10]),
        .I1(CO),
        .O(\add_r[8]_i_3__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_4__3 
       (.I0(mul_r_reg__1[9]),
        .I1(CO),
        .O(\add_r[8]_i_4__3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_5__3 
       (.I0(mul_r_reg__1[8]),
        .I1(CO),
        .O(\add_r[8]_i_5__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_6__3 
       (.I0(D[11]),
        .I1(CO),
        .I2(mul_r_reg__1[11]),
        .O(\add_r[8]_i_6__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_7__3 
       (.I0(D[10]),
        .I1(CO),
        .I2(mul_r_reg__1[10]),
        .O(\add_r[8]_i_7__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_8__3 
       (.I0(D[9]),
        .I1(CO),
        .I2(mul_r_reg__1[9]),
        .O(\add_r[8]_i_8__3_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_9__3 
       (.I0(D[8]),
        .I1(CO),
        .I2(mul_r_reg__1[8]),
        .O(\add_r[8]_i_9__3_n_0 ));
  FDRE \add_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__3_n_7 ),
        .Q(D[0]),
        .R(aresetn));
  CARRY4 \add_r_reg[0]_i_1__3 
       (.CI(1'b0),
        .CO({\add_r_reg[0]_i_1__3_n_0 ,\add_r_reg[0]_i_1__3_n_1 ,\add_r_reg[0]_i_1__3_n_2 ,\add_r_reg[0]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[0]_i_2__3_n_0 ,\add_r[0]_i_3__3_n_0 ,\add_r[0]_i_4__3_n_0 ,\add_r[0]_i_5__3_n_0 }),
        .O({\add_r_reg[0]_i_1__3_n_4 ,\add_r_reg[0]_i_1__3_n_5 ,\add_r_reg[0]_i_1__3_n_6 ,\add_r_reg[0]_i_1__3_n_7 }),
        .S({\add_r[0]_i_6__3_n_0 ,\add_r[0]_i_7__3_n_0 ,\add_r[0]_i_8__3_n_0 ,\add_r[0]_i_9__3_n_0 }));
  FDRE \add_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__3_n_5 ),
        .Q(D[10]),
        .R(aresetn));
  FDRE \add_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__3_n_4 ),
        .Q(D[11]),
        .R(aresetn));
  FDRE \add_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__3_n_7 ),
        .Q(D[12]),
        .R(aresetn));
  CARRY4 \add_r_reg[12]_i_1__3 
       (.CI(\add_r_reg[8]_i_1__3_n_0 ),
        .CO({\add_r_reg[12]_i_1__3_n_0 ,\add_r_reg[12]_i_1__3_n_1 ,\add_r_reg[12]_i_1__3_n_2 ,\add_r_reg[12]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[12]_i_2__3_n_0 ,\add_r[12]_i_3__3_n_0 ,\add_r[12]_i_4__3_n_0 ,\add_r[12]_i_5__3_n_0 }),
        .O({\add_r_reg[12]_i_1__3_n_4 ,\add_r_reg[12]_i_1__3_n_5 ,\add_r_reg[12]_i_1__3_n_6 ,\add_r_reg[12]_i_1__3_n_7 }),
        .S({\add_r[12]_i_6__3_n_0 ,\add_r[12]_i_7__3_n_0 ,\add_r[12]_i_8__3_n_0 ,\add_r[12]_i_9__3_n_0 }));
  FDRE \add_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__3_n_6 ),
        .Q(D[13]),
        .R(aresetn));
  FDRE \add_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__3_n_5 ),
        .Q(D[14]),
        .R(aresetn));
  FDRE \add_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__3_n_4 ),
        .Q(D[15]),
        .R(aresetn));
  FDRE \add_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__3_n_7 ),
        .Q(D[16]),
        .R(aresetn));
  CARRY4 \add_r_reg[16]_i_1__3 
       (.CI(\add_r_reg[12]_i_1__3_n_0 ),
        .CO({\add_r_reg[16]_i_1__3_n_0 ,\add_r_reg[16]_i_1__3_n_1 ,\add_r_reg[16]_i_1__3_n_2 ,\add_r_reg[16]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[16]_i_2__3_n_0 ,\add_r[16]_i_3__3_n_0 ,\add_r[16]_i_4__3_n_0 ,\add_r[16]_i_5__3_n_0 }),
        .O({\add_r_reg[16]_i_1__3_n_4 ,\add_r_reg[16]_i_1__3_n_5 ,\add_r_reg[16]_i_1__3_n_6 ,\add_r_reg[16]_i_1__3_n_7 }),
        .S({\add_r[16]_i_6__3_n_0 ,\add_r[16]_i_7__3_n_0 ,\add_r[16]_i_8__3_n_0 ,\add_r[16]_i_9__3_n_0 }));
  FDRE \add_r_reg[17] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__3_n_6 ),
        .Q(D[17]),
        .R(aresetn));
  FDRE \add_r_reg[18] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__3_n_5 ),
        .Q(D[18]),
        .R(aresetn));
  FDRE \add_r_reg[19] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__3_n_4 ),
        .Q(D[19]),
        .R(aresetn));
  FDRE \add_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__3_n_6 ),
        .Q(D[1]),
        .R(aresetn));
  FDRE \add_r_reg[20] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__3_n_7 ),
        .Q(D[20]),
        .R(aresetn));
  CARRY4 \add_r_reg[20]_i_1__3 
       (.CI(\add_r_reg[16]_i_1__3_n_0 ),
        .CO({\add_r_reg[20]_i_1__3_n_0 ,\add_r_reg[20]_i_1__3_n_1 ,\add_r_reg[20]_i_1__3_n_2 ,\add_r_reg[20]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[20]_i_2__3_n_0 ,\add_r[20]_i_3__3_n_0 ,\add_r[20]_i_4__3_n_0 ,\add_r[20]_i_5__3_n_0 }),
        .O({\add_r_reg[20]_i_1__3_n_4 ,\add_r_reg[20]_i_1__3_n_5 ,\add_r_reg[20]_i_1__3_n_6 ,\add_r_reg[20]_i_1__3_n_7 }),
        .S({\add_r[20]_i_6__3_n_0 ,\add_r[20]_i_7__3_n_0 ,\add_r[20]_i_8__3_n_0 ,\add_r[20]_i_9__3_n_0 }));
  FDRE \add_r_reg[21] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__3_n_6 ),
        .Q(D[21]),
        .R(aresetn));
  FDRE \add_r_reg[22] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__3_n_5 ),
        .Q(D[22]),
        .R(aresetn));
  FDRE \add_r_reg[23] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__3_n_4 ),
        .Q(D[23]),
        .R(aresetn));
  FDRE \add_r_reg[24] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__3_n_7 ),
        .Q(D[24]),
        .R(aresetn));
  CARRY4 \add_r_reg[24]_i_1__3 
       (.CI(\add_r_reg[20]_i_1__3_n_0 ),
        .CO({\add_r_reg[24]_i_1__3_n_0 ,\add_r_reg[24]_i_1__3_n_1 ,\add_r_reg[24]_i_1__3_n_2 ,\add_r_reg[24]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[24]_i_2__3_n_0 ,\add_r[24]_i_3__3_n_0 ,\add_r[24]_i_4__3_n_0 ,\add_r[24]_i_5__3_n_0 }),
        .O({\add_r_reg[24]_i_1__3_n_4 ,\add_r_reg[24]_i_1__3_n_5 ,\add_r_reg[24]_i_1__3_n_6 ,\add_r_reg[24]_i_1__3_n_7 }),
        .S({\add_r[24]_i_6__3_n_0 ,\add_r[24]_i_7__3_n_0 ,\add_r[24]_i_8__3_n_0 ,\add_r[24]_i_9__3_n_0 }));
  FDRE \add_r_reg[25] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__3_n_6 ),
        .Q(D[25]),
        .R(aresetn));
  FDRE \add_r_reg[26] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__3_n_5 ),
        .Q(D[26]),
        .R(aresetn));
  FDRE \add_r_reg[27] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__3_n_4 ),
        .Q(D[27]),
        .R(aresetn));
  FDRE \add_r_reg[28] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__3_n_7 ),
        .Q(D[28]),
        .R(aresetn));
  CARRY4 \add_r_reg[28]_i_1__3 
       (.CI(\add_r_reg[24]_i_1__3_n_0 ),
        .CO({\add_r_reg[28]_i_1__3_n_0 ,\add_r_reg[28]_i_1__3_n_1 ,\add_r_reg[28]_i_1__3_n_2 ,\add_r_reg[28]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[28]_i_2__3_n_0 ,\add_r[28]_i_3__3_n_0 ,\add_r[28]_i_4__3_n_0 ,\add_r[28]_i_5__3_n_0 }),
        .O({\add_r_reg[28]_i_1__3_n_4 ,\add_r_reg[28]_i_1__3_n_5 ,\add_r_reg[28]_i_1__3_n_6 ,\add_r_reg[28]_i_1__3_n_7 }),
        .S({\add_r[28]_i_6__3_n_0 ,\add_r[28]_i_7__3_n_0 ,\add_r[28]_i_8__3_n_0 ,\add_r[28]_i_9__3_n_0 }));
  FDRE \add_r_reg[29] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__3_n_6 ),
        .Q(D[29]),
        .R(aresetn));
  FDRE \add_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__3_n_5 ),
        .Q(D[2]),
        .R(aresetn));
  FDRE \add_r_reg[30] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__3_n_5 ),
        .Q(D[30]),
        .R(aresetn));
  FDRE \add_r_reg[31] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__3_n_4 ),
        .Q(D[31]),
        .R(aresetn));
  FDRE \add_r_reg[32] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__3_n_7 ),
        .Q(D[32]),
        .R(aresetn));
  CARRY4 \add_r_reg[32]_i_1__3 
       (.CI(\add_r_reg[28]_i_1__3_n_0 ),
        .CO({\add_r_reg[32]_i_1__3_n_0 ,\add_r_reg[32]_i_1__3_n_1 ,\add_r_reg[32]_i_1__3_n_2 ,\add_r_reg[32]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[32]_i_2__3_n_0 ,\add_r[32]_i_3__3_n_0 ,\add_r[32]_i_4__3_n_0 ,\add_r[32]_i_5__3_n_0 }),
        .O({\add_r_reg[32]_i_1__3_n_4 ,\add_r_reg[32]_i_1__3_n_5 ,\add_r_reg[32]_i_1__3_n_6 ,\add_r_reg[32]_i_1__3_n_7 }),
        .S({\add_r[32]_i_6__3_n_0 ,\add_r[32]_i_7__3_n_0 ,\add_r[32]_i_8__3_n_0 ,\add_r[32]_i_9__3_n_0 }));
  FDRE \add_r_reg[33] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__3_n_6 ),
        .Q(D[33]),
        .R(aresetn));
  FDRE \add_r_reg[34] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__3_n_5 ),
        .Q(D[34]),
        .R(aresetn));
  FDRE \add_r_reg[35] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__3_n_4 ),
        .Q(D[35]),
        .R(aresetn));
  FDRE \add_r_reg[36] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__3_n_7 ),
        .Q(D[36]),
        .R(aresetn));
  CARRY4 \add_r_reg[36]_i_1__3 
       (.CI(\add_r_reg[32]_i_1__3_n_0 ),
        .CO({\add_r_reg[36]_i_1__3_n_0 ,\add_r_reg[36]_i_1__3_n_1 ,\add_r_reg[36]_i_1__3_n_2 ,\add_r_reg[36]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[36]_i_2__3_n_0 ,\add_r[36]_i_3__3_n_0 ,\add_r[36]_i_4__3_n_0 ,\add_r[36]_i_5__3_n_0 }),
        .O({\add_r_reg[36]_i_1__3_n_4 ,\add_r_reg[36]_i_1__3_n_5 ,\add_r_reg[36]_i_1__3_n_6 ,\add_r_reg[36]_i_1__3_n_7 }),
        .S({\add_r[36]_i_6__3_n_0 ,\add_r[36]_i_7__3_n_0 ,\add_r[36]_i_8__3_n_0 ,\add_r[36]_i_9__3_n_0 }));
  FDRE \add_r_reg[37] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__3_n_6 ),
        .Q(D[37]),
        .R(aresetn));
  FDRE \add_r_reg[38] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__3_n_5 ),
        .Q(D[38]),
        .R(aresetn));
  FDRE \add_r_reg[39] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__3_n_4 ),
        .Q(D[39]),
        .R(aresetn));
  FDRE \add_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__3_n_4 ),
        .Q(D[3]),
        .R(aresetn));
  FDRE \add_r_reg[40] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__3_n_7 ),
        .Q(D[40]),
        .R(aresetn));
  CARRY4 \add_r_reg[40]_i_1__3 
       (.CI(\add_r_reg[36]_i_1__3_n_0 ),
        .CO({\add_r_reg[40]_i_1__3_n_0 ,\add_r_reg[40]_i_1__3_n_1 ,\add_r_reg[40]_i_1__3_n_2 ,\add_r_reg[40]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[40]_i_2__3_n_0 ,\add_r[40]_i_3__3_n_0 ,\add_r[40]_i_4__3_n_0 ,\add_r[40]_i_5__3_n_0 }),
        .O({\add_r_reg[40]_i_1__3_n_4 ,\add_r_reg[40]_i_1__3_n_5 ,\add_r_reg[40]_i_1__3_n_6 ,\add_r_reg[40]_i_1__3_n_7 }),
        .S({\add_r[40]_i_6__3_n_0 ,\add_r[40]_i_7__3_n_0 ,\add_r[40]_i_8__3_n_0 ,\add_r[40]_i_9__3_n_0 }));
  FDRE \add_r_reg[41] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__3_n_6 ),
        .Q(D[41]),
        .R(aresetn));
  FDRE \add_r_reg[42] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__3_n_5 ),
        .Q(D[42]),
        .R(aresetn));
  FDRE \add_r_reg[43] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__3_n_4 ),
        .Q(D[43]),
        .R(aresetn));
  FDRE \add_r_reg[44] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__3_n_7 ),
        .Q(D[44]),
        .R(aresetn));
  CARRY4 \add_r_reg[44]_i_1__3 
       (.CI(\add_r_reg[40]_i_1__3_n_0 ),
        .CO({\NLW_add_r_reg[44]_i_1__3_CO_UNCONNECTED [3],\add_r_reg[44]_i_1__3_n_1 ,\add_r_reg[44]_i_1__3_n_2 ,\add_r_reg[44]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\add_r[44]_i_2__3_n_0 ,\add_r[44]_i_3__3_n_0 ,\add_r[44]_i_4__3_n_0 }),
        .O({\add_r_reg[44]_i_1__3_n_4 ,\add_r_reg[44]_i_1__3_n_5 ,\add_r_reg[44]_i_1__3_n_6 ,\add_r_reg[44]_i_1__3_n_7 }),
        .S({\add_r[44]_i_5__3_n_0 ,\add_r[44]_i_6__3_n_0 ,\add_r[44]_i_7__3_n_0 ,\add_r[44]_i_8__3_n_0 }));
  FDRE \add_r_reg[45] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__3_n_6 ),
        .Q(D[45]),
        .R(aresetn));
  FDRE \add_r_reg[46] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__3_n_5 ),
        .Q(D[46]),
        .R(aresetn));
  FDRE \add_r_reg[47] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__3_n_4 ),
        .Q(D[47]),
        .R(aresetn));
  FDRE \add_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__3_n_7 ),
        .Q(D[4]),
        .R(aresetn));
  CARRY4 \add_r_reg[4]_i_1__3 
       (.CI(\add_r_reg[0]_i_1__3_n_0 ),
        .CO({\add_r_reg[4]_i_1__3_n_0 ,\add_r_reg[4]_i_1__3_n_1 ,\add_r_reg[4]_i_1__3_n_2 ,\add_r_reg[4]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[4]_i_2__3_n_0 ,\add_r[4]_i_3__3_n_0 ,\add_r[4]_i_4__3_n_0 ,\add_r[4]_i_5__3_n_0 }),
        .O({\add_r_reg[4]_i_1__3_n_4 ,\add_r_reg[4]_i_1__3_n_5 ,\add_r_reg[4]_i_1__3_n_6 ,\add_r_reg[4]_i_1__3_n_7 }),
        .S({\add_r[4]_i_6__3_n_0 ,\add_r[4]_i_7__3_n_0 ,\add_r[4]_i_8__3_n_0 ,\add_r[4]_i_9__3_n_0 }));
  FDRE \add_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__3_n_6 ),
        .Q(D[5]),
        .R(aresetn));
  FDRE \add_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__3_n_5 ),
        .Q(D[6]),
        .R(aresetn));
  FDRE \add_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__3_n_4 ),
        .Q(D[7]),
        .R(aresetn));
  FDRE \add_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__3_n_7 ),
        .Q(D[8]),
        .R(aresetn));
  CARRY4 \add_r_reg[8]_i_1__3 
       (.CI(\add_r_reg[4]_i_1__3_n_0 ),
        .CO({\add_r_reg[8]_i_1__3_n_0 ,\add_r_reg[8]_i_1__3_n_1 ,\add_r_reg[8]_i_1__3_n_2 ,\add_r_reg[8]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[8]_i_2__3_n_0 ,\add_r[8]_i_3__3_n_0 ,\add_r[8]_i_4__3_n_0 ,\add_r[8]_i_5__3_n_0 }),
        .O({\add_r_reg[8]_i_1__3_n_4 ,\add_r_reg[8]_i_1__3_n_5 ,\add_r_reg[8]_i_1__3_n_6 ,\add_r_reg[8]_i_1__3_n_7 }),
        .S({\add_r[8]_i_6__3_n_0 ,\add_r[8]_i_7__3_n_0 ,\add_r[8]_i_8__3_n_0 ,\add_r[8]_i_9__3_n_0 }));
  FDRE \add_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__3_n_6 ),
        .Q(D[9]),
        .R(aresetn));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter[0]_i_1__6 
       (.I0(\counter_reg_n_0_[0] ),
        .O(\counter[0]_i_1__6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'hFD0E)) 
    \counter[1]_i_1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(CO),
        .I2(dp_extend_end_reg_0),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \counter[2]_i_1__5 
       (.I0(\counter_reg_n_0_[2] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[1] ),
        .O(\counter[2]_i_1__5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \counter[3]_i_1__5 
       (.I0(Q[0]),
        .I1(\counter_reg_n_0_[2] ),
        .I2(\counter_reg_n_0_[0] ),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[3]_i_1__5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \counter[4]_i_1__2 
       (.I0(Q[1]),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[0] ),
        .I3(\counter_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(\counter[4]_i_1__2_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \counter[5]_i_1__4 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[1] ),
        .I5(Q[1]),
        .O(\counter[5]_i_1__4_n_0 ));
  LUT5 #(
    .INIT(32'hA6AAAAAA)) 
    \counter[6]_i_1__6 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(\counter[7]_i_2__2_n_0 ),
        .I3(Q[0]),
        .I4(Q[2]),
        .O(\counter[6]_i_1__6_n_0 ));
  LUT6 #(
    .INIT(64'hAA6AAAAAAAAAAAAA)) 
    \counter[7]_i_1__3 
       (.I0(Q[4]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(\counter[7]_i_2__2_n_0 ),
        .I4(Q[1]),
        .I5(Q[3]),
        .O(\counter[7]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \counter[7]_i_2__2 
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[2] ),
        .O(\counter[7]_i_2__2_n_0 ));
  LUT4 #(
    .INIT(16'hA6AA)) 
    \counter[8]_i_2__4 
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(\counter[8]_i_3__4_n_0 ),
        .I3(Q[4]),
        .O(\counter[8]_i_2__4_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \counter[8]_i_3__4 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[1] ),
        .I5(Q[1]),
        .O(\counter[8]_i_3__4_n_0 ));
  FDRE \counter_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[0]_i_1__6_n_0 ),
        .Q(\counter_reg_n_0_[0] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\counter[1]_i_1_n_0 ),
        .Q(\counter_reg_n_0_[1] ),
        .R(aresetn));
  FDRE \counter_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[2]_i_1__5_n_0 ),
        .Q(\counter_reg_n_0_[2] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[3]_i_1__5_n_0 ),
        .Q(Q[0]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[4]_i_1__2_n_0 ),
        .Q(Q[1]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[5]_i_1__4_n_0 ),
        .Q(Q[2]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[6]_i_1__6_n_0 ),
        .Q(Q[3]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[7]_i_1__3_n_0 ),
        .Q(Q[4]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[8]_i_2__4_n_0 ),
        .Q(Q[5]),
        .R(dp_extend_end_reg));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,b_ram_out[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({B[16],B}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r0_OVERFLOW_UNCONNECTED),
        .P({mul_r0_n_58,mul_r0_n_59,mul_r0_n_60,mul_r0_n_61,mul_r0_n_62,mul_r0_n_63,mul_r0_n_64,mul_r0_n_65,mul_r0_n_66,mul_r0_n_67,mul_r0_n_68,mul_r0_n_69,mul_r0_n_70,mul_r0_n_71,mul_r0_n_72,mul_r0_n_73,mul_r0_n_74,mul_r0_n_75,mul_r0_n_76,mul_r0_n_77,mul_r0_n_78,mul_r0_n_79,mul_r0_n_80,mul_r0_n_81,mul_r0_n_82,mul_r0_n_83,mul_r0_n_84,mul_r0_n_85,mul_r0_n_86,mul_r0_n_87,mul_r0_n_88,mul_r0_n_89,mul_r0_n_90,mul_r0_n_91,mul_r0_n_92,mul_r0_n_93,mul_r0_n_94,mul_r0_n_95,mul_r0_n_96,mul_r0_n_97,mul_r0_n_98,mul_r0_n_99,mul_r0_n_100,mul_r0_n_101,mul_r0_n_102,mul_r0_n_103,mul_r0_n_104,mul_r0_n_105}),
        .PATTERNBDETECT(NLW_mul_r0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_r0_UNDERFLOW_UNCONNECTED));
  FDRE \mul_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_105),
        .Q(mul_r_reg__1[0]),
        .R(aresetn));
  FDRE \mul_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_95),
        .Q(mul_r_reg__1[10]),
        .R(aresetn));
  FDRE \mul_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_94),
        .Q(mul_r_reg__1[11]),
        .R(aresetn));
  FDRE \mul_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_93),
        .Q(mul_r_reg__1[12]),
        .R(aresetn));
  FDRE \mul_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_92),
        .Q(mul_r_reg__1[13]),
        .R(aresetn));
  FDRE \mul_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_91),
        .Q(mul_r_reg__1[14]),
        .R(aresetn));
  FDRE \mul_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_90),
        .Q(mul_r_reg__1[15]),
        .R(aresetn));
  FDRE \mul_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_89),
        .Q(mul_r_reg__1[16]),
        .R(aresetn));
  FDRE \mul_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_104),
        .Q(mul_r_reg__1[1]),
        .R(aresetn));
  FDRE \mul_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_103),
        .Q(mul_r_reg__1[2]),
        .R(aresetn));
  FDRE \mul_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_102),
        .Q(mul_r_reg__1[3]),
        .R(aresetn));
  FDRE \mul_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_101),
        .Q(mul_r_reg__1[4]),
        .R(aresetn));
  FDRE \mul_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_100),
        .Q(mul_r_reg__1[5]),
        .R(aresetn));
  FDRE \mul_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_99),
        .Q(mul_r_reg__1[6]),
        .R(aresetn));
  FDRE \mul_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_98),
        .Q(mul_r_reg__1[7]),
        .R(aresetn));
  FDRE \mul_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_97),
        .Q(mul_r_reg__1[8]),
        .R(aresetn));
  FDRE \mul_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_96),
        .Q(mul_r_reg__1[9]),
        .R(aresetn));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-12 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r_reg__0
       (.A({B[15],B[15],B[15],B[15],B[15],B[15],B[15],B[15],B[15],\in_raw_delay_reg[15]_rep__5 ,\in_raw_delay_reg[15]_rep__5 ,\in_raw_delay_reg[15]_rep__5 ,\in_raw_delay_reg[15]_rep__5 ,\in_raw_delay_reg[15]_rep__5 ,\in_raw_delay_reg[15]_rep__5 ,B[14:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r_reg__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({b_ram_out[31],b_ram_out[31],b_ram_out[31],b_ram_out[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r_reg__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(dp_enable),
        .CLK(clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED),
        .P({mul_r_reg__0_n_58,mul_r_reg__0_n_59,mul_r_reg__0_n_60,mul_r_reg__0_n_61,mul_r_reg__0_n_62,mul_r_reg__0_n_63,mul_r_reg__0_n_64,mul_r_reg__0_n_65,mul_r_reg__0_n_66,mul_r_reg__0_n_67,mul_r_reg__0_n_68,mul_r_reg__0_n_69,mul_r_reg__0_n_70,mul_r_reg__0_n_71,mul_r_reg__0_n_72,mul_r_reg__0_n_73,mul_r_reg__0_n_74,mul_r_reg__1[47:17]}),
        .PATTERNBDETECT(NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .PCOUT(NLW_mul_r_reg__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(aresetn),
        .UNDERFLOW(NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "dot_product" *) 
module design_1_top_0_0_dot_product_4
   (D,
    CO,
    Q,
    A,
    b_ram_out,
    dp_enable,
    clk,
    aresetn,
    \slv_regs_reg[0][11] ,
    O,
    \slv_regs_reg[0][0] ,
    \slv_regs_reg[0][0]_0 ,
    dp_extend_end_reg,
    dp_extend_end_reg_0);
  output [47:0]D;
  output [0:0]CO;
  output [5:0]Q;
  input [17:0]A;
  input [31:0]b_ram_out;
  input dp_enable;
  input clk;
  input aresetn;
  input [2:0]\slv_regs_reg[0][11] ;
  input [0:0]O;
  input [1:0]\slv_regs_reg[0][0] ;
  input [0:0]\slv_regs_reg[0][0]_0 ;
  input [0:0]dp_extend_end_reg;
  input dp_extend_end_reg_0;

  wire [17:0]A;
  wire [0:0]CO;
  wire [47:0]D;
  wire [0:0]O;
  wire [5:0]Q;
  wire add_r1_carry_i_4__4_n_0;
  wire add_r1_carry_n_0;
  wire add_r1_carry_n_1;
  wire add_r1_carry_n_2;
  wire add_r1_carry_n_3;
  wire \add_r[0]_i_2__4_n_0 ;
  wire \add_r[0]_i_3__4_n_0 ;
  wire \add_r[0]_i_4__4_n_0 ;
  wire \add_r[0]_i_5__4_n_0 ;
  wire \add_r[0]_i_6__4_n_0 ;
  wire \add_r[0]_i_7__4_n_0 ;
  wire \add_r[0]_i_8__4_n_0 ;
  wire \add_r[0]_i_9__4_n_0 ;
  wire \add_r[12]_i_2__4_n_0 ;
  wire \add_r[12]_i_3__4_n_0 ;
  wire \add_r[12]_i_4__4_n_0 ;
  wire \add_r[12]_i_5__4_n_0 ;
  wire \add_r[12]_i_6__4_n_0 ;
  wire \add_r[12]_i_7__4_n_0 ;
  wire \add_r[12]_i_8__4_n_0 ;
  wire \add_r[12]_i_9__4_n_0 ;
  wire \add_r[16]_i_2__4_n_0 ;
  wire \add_r[16]_i_3__4_n_0 ;
  wire \add_r[16]_i_4__4_n_0 ;
  wire \add_r[16]_i_5__4_n_0 ;
  wire \add_r[16]_i_6__4_n_0 ;
  wire \add_r[16]_i_7__4_n_0 ;
  wire \add_r[16]_i_8__4_n_0 ;
  wire \add_r[16]_i_9__4_n_0 ;
  wire \add_r[20]_i_2__4_n_0 ;
  wire \add_r[20]_i_3__4_n_0 ;
  wire \add_r[20]_i_4__4_n_0 ;
  wire \add_r[20]_i_5__4_n_0 ;
  wire \add_r[20]_i_6__4_n_0 ;
  wire \add_r[20]_i_7__4_n_0 ;
  wire \add_r[20]_i_8__4_n_0 ;
  wire \add_r[20]_i_9__4_n_0 ;
  wire \add_r[24]_i_2__4_n_0 ;
  wire \add_r[24]_i_3__4_n_0 ;
  wire \add_r[24]_i_4__4_n_0 ;
  wire \add_r[24]_i_5__4_n_0 ;
  wire \add_r[24]_i_6__4_n_0 ;
  wire \add_r[24]_i_7__4_n_0 ;
  wire \add_r[24]_i_8__4_n_0 ;
  wire \add_r[24]_i_9__4_n_0 ;
  wire \add_r[28]_i_2__4_n_0 ;
  wire \add_r[28]_i_3__4_n_0 ;
  wire \add_r[28]_i_4__4_n_0 ;
  wire \add_r[28]_i_5__4_n_0 ;
  wire \add_r[28]_i_6__4_n_0 ;
  wire \add_r[28]_i_7__4_n_0 ;
  wire \add_r[28]_i_8__4_n_0 ;
  wire \add_r[28]_i_9__4_n_0 ;
  wire \add_r[32]_i_2__4_n_0 ;
  wire \add_r[32]_i_3__4_n_0 ;
  wire \add_r[32]_i_4__4_n_0 ;
  wire \add_r[32]_i_5__4_n_0 ;
  wire \add_r[32]_i_6__4_n_0 ;
  wire \add_r[32]_i_7__4_n_0 ;
  wire \add_r[32]_i_8__4_n_0 ;
  wire \add_r[32]_i_9__4_n_0 ;
  wire \add_r[36]_i_2__4_n_0 ;
  wire \add_r[36]_i_3__4_n_0 ;
  wire \add_r[36]_i_4__4_n_0 ;
  wire \add_r[36]_i_5__4_n_0 ;
  wire \add_r[36]_i_6__4_n_0 ;
  wire \add_r[36]_i_7__4_n_0 ;
  wire \add_r[36]_i_8__4_n_0 ;
  wire \add_r[36]_i_9__4_n_0 ;
  wire \add_r[40]_i_2__4_n_0 ;
  wire \add_r[40]_i_3__4_n_0 ;
  wire \add_r[40]_i_4__4_n_0 ;
  wire \add_r[40]_i_5__4_n_0 ;
  wire \add_r[40]_i_6__4_n_0 ;
  wire \add_r[40]_i_7__4_n_0 ;
  wire \add_r[40]_i_8__4_n_0 ;
  wire \add_r[40]_i_9__4_n_0 ;
  wire \add_r[44]_i_2__4_n_0 ;
  wire \add_r[44]_i_3__4_n_0 ;
  wire \add_r[44]_i_4__4_n_0 ;
  wire \add_r[44]_i_5__4_n_0 ;
  wire \add_r[44]_i_6__4_n_0 ;
  wire \add_r[44]_i_7__4_n_0 ;
  wire \add_r[44]_i_8__4_n_0 ;
  wire \add_r[4]_i_2__4_n_0 ;
  wire \add_r[4]_i_3__4_n_0 ;
  wire \add_r[4]_i_4__4_n_0 ;
  wire \add_r[4]_i_5__4_n_0 ;
  wire \add_r[4]_i_6__4_n_0 ;
  wire \add_r[4]_i_7__4_n_0 ;
  wire \add_r[4]_i_8__4_n_0 ;
  wire \add_r[4]_i_9__4_n_0 ;
  wire \add_r[8]_i_2__4_n_0 ;
  wire \add_r[8]_i_3__4_n_0 ;
  wire \add_r[8]_i_4__4_n_0 ;
  wire \add_r[8]_i_5__4_n_0 ;
  wire \add_r[8]_i_6__4_n_0 ;
  wire \add_r[8]_i_7__4_n_0 ;
  wire \add_r[8]_i_8__4_n_0 ;
  wire \add_r[8]_i_9__4_n_0 ;
  wire \add_r_reg[0]_i_1__4_n_0 ;
  wire \add_r_reg[0]_i_1__4_n_1 ;
  wire \add_r_reg[0]_i_1__4_n_2 ;
  wire \add_r_reg[0]_i_1__4_n_3 ;
  wire \add_r_reg[0]_i_1__4_n_4 ;
  wire \add_r_reg[0]_i_1__4_n_5 ;
  wire \add_r_reg[0]_i_1__4_n_6 ;
  wire \add_r_reg[0]_i_1__4_n_7 ;
  wire \add_r_reg[12]_i_1__4_n_0 ;
  wire \add_r_reg[12]_i_1__4_n_1 ;
  wire \add_r_reg[12]_i_1__4_n_2 ;
  wire \add_r_reg[12]_i_1__4_n_3 ;
  wire \add_r_reg[12]_i_1__4_n_4 ;
  wire \add_r_reg[12]_i_1__4_n_5 ;
  wire \add_r_reg[12]_i_1__4_n_6 ;
  wire \add_r_reg[12]_i_1__4_n_7 ;
  wire \add_r_reg[16]_i_1__4_n_0 ;
  wire \add_r_reg[16]_i_1__4_n_1 ;
  wire \add_r_reg[16]_i_1__4_n_2 ;
  wire \add_r_reg[16]_i_1__4_n_3 ;
  wire \add_r_reg[16]_i_1__4_n_4 ;
  wire \add_r_reg[16]_i_1__4_n_5 ;
  wire \add_r_reg[16]_i_1__4_n_6 ;
  wire \add_r_reg[16]_i_1__4_n_7 ;
  wire \add_r_reg[20]_i_1__4_n_0 ;
  wire \add_r_reg[20]_i_1__4_n_1 ;
  wire \add_r_reg[20]_i_1__4_n_2 ;
  wire \add_r_reg[20]_i_1__4_n_3 ;
  wire \add_r_reg[20]_i_1__4_n_4 ;
  wire \add_r_reg[20]_i_1__4_n_5 ;
  wire \add_r_reg[20]_i_1__4_n_6 ;
  wire \add_r_reg[20]_i_1__4_n_7 ;
  wire \add_r_reg[24]_i_1__4_n_0 ;
  wire \add_r_reg[24]_i_1__4_n_1 ;
  wire \add_r_reg[24]_i_1__4_n_2 ;
  wire \add_r_reg[24]_i_1__4_n_3 ;
  wire \add_r_reg[24]_i_1__4_n_4 ;
  wire \add_r_reg[24]_i_1__4_n_5 ;
  wire \add_r_reg[24]_i_1__4_n_6 ;
  wire \add_r_reg[24]_i_1__4_n_7 ;
  wire \add_r_reg[28]_i_1__4_n_0 ;
  wire \add_r_reg[28]_i_1__4_n_1 ;
  wire \add_r_reg[28]_i_1__4_n_2 ;
  wire \add_r_reg[28]_i_1__4_n_3 ;
  wire \add_r_reg[28]_i_1__4_n_4 ;
  wire \add_r_reg[28]_i_1__4_n_5 ;
  wire \add_r_reg[28]_i_1__4_n_6 ;
  wire \add_r_reg[28]_i_1__4_n_7 ;
  wire \add_r_reg[32]_i_1__4_n_0 ;
  wire \add_r_reg[32]_i_1__4_n_1 ;
  wire \add_r_reg[32]_i_1__4_n_2 ;
  wire \add_r_reg[32]_i_1__4_n_3 ;
  wire \add_r_reg[32]_i_1__4_n_4 ;
  wire \add_r_reg[32]_i_1__4_n_5 ;
  wire \add_r_reg[32]_i_1__4_n_6 ;
  wire \add_r_reg[32]_i_1__4_n_7 ;
  wire \add_r_reg[36]_i_1__4_n_0 ;
  wire \add_r_reg[36]_i_1__4_n_1 ;
  wire \add_r_reg[36]_i_1__4_n_2 ;
  wire \add_r_reg[36]_i_1__4_n_3 ;
  wire \add_r_reg[36]_i_1__4_n_4 ;
  wire \add_r_reg[36]_i_1__4_n_5 ;
  wire \add_r_reg[36]_i_1__4_n_6 ;
  wire \add_r_reg[36]_i_1__4_n_7 ;
  wire \add_r_reg[40]_i_1__4_n_0 ;
  wire \add_r_reg[40]_i_1__4_n_1 ;
  wire \add_r_reg[40]_i_1__4_n_2 ;
  wire \add_r_reg[40]_i_1__4_n_3 ;
  wire \add_r_reg[40]_i_1__4_n_4 ;
  wire \add_r_reg[40]_i_1__4_n_5 ;
  wire \add_r_reg[40]_i_1__4_n_6 ;
  wire \add_r_reg[40]_i_1__4_n_7 ;
  wire \add_r_reg[44]_i_1__4_n_1 ;
  wire \add_r_reg[44]_i_1__4_n_2 ;
  wire \add_r_reg[44]_i_1__4_n_3 ;
  wire \add_r_reg[44]_i_1__4_n_4 ;
  wire \add_r_reg[44]_i_1__4_n_5 ;
  wire \add_r_reg[44]_i_1__4_n_6 ;
  wire \add_r_reg[44]_i_1__4_n_7 ;
  wire \add_r_reg[4]_i_1__4_n_0 ;
  wire \add_r_reg[4]_i_1__4_n_1 ;
  wire \add_r_reg[4]_i_1__4_n_2 ;
  wire \add_r_reg[4]_i_1__4_n_3 ;
  wire \add_r_reg[4]_i_1__4_n_4 ;
  wire \add_r_reg[4]_i_1__4_n_5 ;
  wire \add_r_reg[4]_i_1__4_n_6 ;
  wire \add_r_reg[4]_i_1__4_n_7 ;
  wire \add_r_reg[8]_i_1__4_n_0 ;
  wire \add_r_reg[8]_i_1__4_n_1 ;
  wire \add_r_reg[8]_i_1__4_n_2 ;
  wire \add_r_reg[8]_i_1__4_n_3 ;
  wire \add_r_reg[8]_i_1__4_n_4 ;
  wire \add_r_reg[8]_i_1__4_n_5 ;
  wire \add_r_reg[8]_i_1__4_n_6 ;
  wire \add_r_reg[8]_i_1__4_n_7 ;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \counter[0]_i_1__7_n_0 ;
  wire \counter[1]_i_1_n_0 ;
  wire \counter[2]_i_1__6_n_0 ;
  wire \counter[3]_i_1__6_n_0 ;
  wire \counter[4]_i_1__3_n_0 ;
  wire \counter[5]_i_1__5_n_0 ;
  wire \counter[6]_i_1__7_n_0 ;
  wire \counter[7]_i_1__4_n_0 ;
  wire \counter[7]_i_2__3_n_0 ;
  wire \counter[8]_i_2__5_n_0 ;
  wire \counter[8]_i_3__5_n_0 ;
  wire \counter_reg_n_0_[0] ;
  wire \counter_reg_n_0_[1] ;
  wire \counter_reg_n_0_[2] ;
  wire dp_enable;
  wire [0:0]dp_extend_end_reg;
  wire dp_extend_end_reg_0;
  wire mul_r0_n_100;
  wire mul_r0_n_101;
  wire mul_r0_n_102;
  wire mul_r0_n_103;
  wire mul_r0_n_104;
  wire mul_r0_n_105;
  wire mul_r0_n_106;
  wire mul_r0_n_107;
  wire mul_r0_n_108;
  wire mul_r0_n_109;
  wire mul_r0_n_110;
  wire mul_r0_n_111;
  wire mul_r0_n_112;
  wire mul_r0_n_113;
  wire mul_r0_n_114;
  wire mul_r0_n_115;
  wire mul_r0_n_116;
  wire mul_r0_n_117;
  wire mul_r0_n_118;
  wire mul_r0_n_119;
  wire mul_r0_n_120;
  wire mul_r0_n_121;
  wire mul_r0_n_122;
  wire mul_r0_n_123;
  wire mul_r0_n_124;
  wire mul_r0_n_125;
  wire mul_r0_n_126;
  wire mul_r0_n_127;
  wire mul_r0_n_128;
  wire mul_r0_n_129;
  wire mul_r0_n_130;
  wire mul_r0_n_131;
  wire mul_r0_n_132;
  wire mul_r0_n_133;
  wire mul_r0_n_134;
  wire mul_r0_n_135;
  wire mul_r0_n_136;
  wire mul_r0_n_137;
  wire mul_r0_n_138;
  wire mul_r0_n_139;
  wire mul_r0_n_140;
  wire mul_r0_n_141;
  wire mul_r0_n_142;
  wire mul_r0_n_143;
  wire mul_r0_n_144;
  wire mul_r0_n_145;
  wire mul_r0_n_146;
  wire mul_r0_n_147;
  wire mul_r0_n_148;
  wire mul_r0_n_149;
  wire mul_r0_n_150;
  wire mul_r0_n_151;
  wire mul_r0_n_152;
  wire mul_r0_n_153;
  wire mul_r0_n_58;
  wire mul_r0_n_59;
  wire mul_r0_n_60;
  wire mul_r0_n_61;
  wire mul_r0_n_62;
  wire mul_r0_n_63;
  wire mul_r0_n_64;
  wire mul_r0_n_65;
  wire mul_r0_n_66;
  wire mul_r0_n_67;
  wire mul_r0_n_68;
  wire mul_r0_n_69;
  wire mul_r0_n_70;
  wire mul_r0_n_71;
  wire mul_r0_n_72;
  wire mul_r0_n_73;
  wire mul_r0_n_74;
  wire mul_r0_n_75;
  wire mul_r0_n_76;
  wire mul_r0_n_77;
  wire mul_r0_n_78;
  wire mul_r0_n_79;
  wire mul_r0_n_80;
  wire mul_r0_n_81;
  wire mul_r0_n_82;
  wire mul_r0_n_83;
  wire mul_r0_n_84;
  wire mul_r0_n_85;
  wire mul_r0_n_86;
  wire mul_r0_n_87;
  wire mul_r0_n_88;
  wire mul_r0_n_89;
  wire mul_r0_n_90;
  wire mul_r0_n_91;
  wire mul_r0_n_92;
  wire mul_r0_n_93;
  wire mul_r0_n_94;
  wire mul_r0_n_95;
  wire mul_r0_n_96;
  wire mul_r0_n_97;
  wire mul_r0_n_98;
  wire mul_r0_n_99;
  wire mul_r_reg__0_n_58;
  wire mul_r_reg__0_n_59;
  wire mul_r_reg__0_n_60;
  wire mul_r_reg__0_n_61;
  wire mul_r_reg__0_n_62;
  wire mul_r_reg__0_n_63;
  wire mul_r_reg__0_n_64;
  wire mul_r_reg__0_n_65;
  wire mul_r_reg__0_n_66;
  wire mul_r_reg__0_n_67;
  wire mul_r_reg__0_n_68;
  wire mul_r_reg__0_n_69;
  wire mul_r_reg__0_n_70;
  wire mul_r_reg__0_n_71;
  wire mul_r_reg__0_n_72;
  wire mul_r_reg__0_n_73;
  wire mul_r_reg__0_n_74;
  wire [47:0]mul_r_reg__1;
  wire [1:0]\slv_regs_reg[0][0] ;
  wire [0:0]\slv_regs_reg[0][0]_0 ;
  wire [2:0]\slv_regs_reg[0][11] ;
  wire [3:0]NLW_add_r1_carry_O_UNCONNECTED;
  wire [3:1]NLW_add_r1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_add_r1_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_add_r_reg[44]_i_1__4_CO_UNCONNECTED ;
  wire NLW_mul_r0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r0_CARRYOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r_reg__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r_reg__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_r_reg__0_PCOUT_UNCONNECTED;

  CARRY4 add_r1_carry
       (.CI(1'b0),
        .CO({add_r1_carry_n_0,add_r1_carry_n_1,add_r1_carry_n_2,add_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry_O_UNCONNECTED[3:0]),
        .S({\slv_regs_reg[0][11] ,add_r1_carry_i_4__4_n_0}));
  CARRY4 add_r1_carry__0
       (.CI(add_r1_carry_n_0),
        .CO({NLW_add_r1_carry__0_CO_UNCONNECTED[3:1],CO}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,O}));
  LUT6 #(
    .INIT(64'h0990000000000990)) 
    add_r1_carry_i_4__4
       (.I0(\counter_reg_n_0_[2] ),
        .I1(\slv_regs_reg[0][0] [1]),
        .I2(\slv_regs_reg[0][0]_0 ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\slv_regs_reg[0][0] [0]),
        .I5(\counter_reg_n_0_[1] ),
        .O(add_r1_carry_i_4__4_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_2__4 
       (.I0(mul_r_reg__1[3]),
        .I1(CO),
        .O(\add_r[0]_i_2__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_3__4 
       (.I0(mul_r_reg__1[2]),
        .I1(CO),
        .O(\add_r[0]_i_3__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_4__4 
       (.I0(mul_r_reg__1[1]),
        .I1(CO),
        .O(\add_r[0]_i_4__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_5__4 
       (.I0(mul_r_reg__1[0]),
        .I1(CO),
        .O(\add_r[0]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_6__4 
       (.I0(D[3]),
        .I1(CO),
        .I2(mul_r_reg__1[3]),
        .O(\add_r[0]_i_6__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_7__4 
       (.I0(D[2]),
        .I1(CO),
        .I2(mul_r_reg__1[2]),
        .O(\add_r[0]_i_7__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_8__4 
       (.I0(D[1]),
        .I1(CO),
        .I2(mul_r_reg__1[1]),
        .O(\add_r[0]_i_8__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_9__4 
       (.I0(D[0]),
        .I1(CO),
        .I2(mul_r_reg__1[0]),
        .O(\add_r[0]_i_9__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_2__4 
       (.I0(mul_r_reg__1[15]),
        .I1(CO),
        .O(\add_r[12]_i_2__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_3__4 
       (.I0(mul_r_reg__1[14]),
        .I1(CO),
        .O(\add_r[12]_i_3__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_4__4 
       (.I0(mul_r_reg__1[13]),
        .I1(CO),
        .O(\add_r[12]_i_4__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_5__4 
       (.I0(mul_r_reg__1[12]),
        .I1(CO),
        .O(\add_r[12]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_6__4 
       (.I0(D[15]),
        .I1(CO),
        .I2(mul_r_reg__1[15]),
        .O(\add_r[12]_i_6__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_7__4 
       (.I0(D[14]),
        .I1(CO),
        .I2(mul_r_reg__1[14]),
        .O(\add_r[12]_i_7__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_8__4 
       (.I0(D[13]),
        .I1(CO),
        .I2(mul_r_reg__1[13]),
        .O(\add_r[12]_i_8__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_9__4 
       (.I0(D[12]),
        .I1(CO),
        .I2(mul_r_reg__1[12]),
        .O(\add_r[12]_i_9__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_2__4 
       (.I0(mul_r_reg__1[19]),
        .I1(CO),
        .O(\add_r[16]_i_2__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_3__4 
       (.I0(mul_r_reg__1[18]),
        .I1(CO),
        .O(\add_r[16]_i_3__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_4__4 
       (.I0(mul_r_reg__1[17]),
        .I1(CO),
        .O(\add_r[16]_i_4__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_5__4 
       (.I0(mul_r_reg__1[16]),
        .I1(CO),
        .O(\add_r[16]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_6__4 
       (.I0(D[19]),
        .I1(CO),
        .I2(mul_r_reg__1[19]),
        .O(\add_r[16]_i_6__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_7__4 
       (.I0(D[18]),
        .I1(CO),
        .I2(mul_r_reg__1[18]),
        .O(\add_r[16]_i_7__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_8__4 
       (.I0(D[17]),
        .I1(CO),
        .I2(mul_r_reg__1[17]),
        .O(\add_r[16]_i_8__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_9__4 
       (.I0(D[16]),
        .I1(CO),
        .I2(mul_r_reg__1[16]),
        .O(\add_r[16]_i_9__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_2__4 
       (.I0(mul_r_reg__1[23]),
        .I1(CO),
        .O(\add_r[20]_i_2__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_3__4 
       (.I0(mul_r_reg__1[22]),
        .I1(CO),
        .O(\add_r[20]_i_3__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_4__4 
       (.I0(mul_r_reg__1[21]),
        .I1(CO),
        .O(\add_r[20]_i_4__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_5__4 
       (.I0(mul_r_reg__1[20]),
        .I1(CO),
        .O(\add_r[20]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_6__4 
       (.I0(D[23]),
        .I1(CO),
        .I2(mul_r_reg__1[23]),
        .O(\add_r[20]_i_6__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_7__4 
       (.I0(D[22]),
        .I1(CO),
        .I2(mul_r_reg__1[22]),
        .O(\add_r[20]_i_7__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_8__4 
       (.I0(D[21]),
        .I1(CO),
        .I2(mul_r_reg__1[21]),
        .O(\add_r[20]_i_8__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_9__4 
       (.I0(D[20]),
        .I1(CO),
        .I2(mul_r_reg__1[20]),
        .O(\add_r[20]_i_9__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_2__4 
       (.I0(mul_r_reg__1[27]),
        .I1(CO),
        .O(\add_r[24]_i_2__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_3__4 
       (.I0(mul_r_reg__1[26]),
        .I1(CO),
        .O(\add_r[24]_i_3__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_4__4 
       (.I0(mul_r_reg__1[25]),
        .I1(CO),
        .O(\add_r[24]_i_4__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_5__4 
       (.I0(mul_r_reg__1[24]),
        .I1(CO),
        .O(\add_r[24]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_6__4 
       (.I0(D[27]),
        .I1(CO),
        .I2(mul_r_reg__1[27]),
        .O(\add_r[24]_i_6__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_7__4 
       (.I0(D[26]),
        .I1(CO),
        .I2(mul_r_reg__1[26]),
        .O(\add_r[24]_i_7__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_8__4 
       (.I0(D[25]),
        .I1(CO),
        .I2(mul_r_reg__1[25]),
        .O(\add_r[24]_i_8__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_9__4 
       (.I0(D[24]),
        .I1(CO),
        .I2(mul_r_reg__1[24]),
        .O(\add_r[24]_i_9__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_2__4 
       (.I0(mul_r_reg__1[31]),
        .I1(CO),
        .O(\add_r[28]_i_2__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_3__4 
       (.I0(mul_r_reg__1[30]),
        .I1(CO),
        .O(\add_r[28]_i_3__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_4__4 
       (.I0(mul_r_reg__1[29]),
        .I1(CO),
        .O(\add_r[28]_i_4__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_5__4 
       (.I0(mul_r_reg__1[28]),
        .I1(CO),
        .O(\add_r[28]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_6__4 
       (.I0(D[31]),
        .I1(CO),
        .I2(mul_r_reg__1[31]),
        .O(\add_r[28]_i_6__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_7__4 
       (.I0(D[30]),
        .I1(CO),
        .I2(mul_r_reg__1[30]),
        .O(\add_r[28]_i_7__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_8__4 
       (.I0(D[29]),
        .I1(CO),
        .I2(mul_r_reg__1[29]),
        .O(\add_r[28]_i_8__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_9__4 
       (.I0(D[28]),
        .I1(CO),
        .I2(mul_r_reg__1[28]),
        .O(\add_r[28]_i_9__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_2__4 
       (.I0(mul_r_reg__1[35]),
        .I1(CO),
        .O(\add_r[32]_i_2__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_3__4 
       (.I0(mul_r_reg__1[34]),
        .I1(CO),
        .O(\add_r[32]_i_3__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_4__4 
       (.I0(mul_r_reg__1[33]),
        .I1(CO),
        .O(\add_r[32]_i_4__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_5__4 
       (.I0(mul_r_reg__1[32]),
        .I1(CO),
        .O(\add_r[32]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_6__4 
       (.I0(D[35]),
        .I1(CO),
        .I2(mul_r_reg__1[35]),
        .O(\add_r[32]_i_6__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_7__4 
       (.I0(D[34]),
        .I1(CO),
        .I2(mul_r_reg__1[34]),
        .O(\add_r[32]_i_7__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_8__4 
       (.I0(D[33]),
        .I1(CO),
        .I2(mul_r_reg__1[33]),
        .O(\add_r[32]_i_8__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_9__4 
       (.I0(D[32]),
        .I1(CO),
        .I2(mul_r_reg__1[32]),
        .O(\add_r[32]_i_9__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_2__4 
       (.I0(mul_r_reg__1[39]),
        .I1(CO),
        .O(\add_r[36]_i_2__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_3__4 
       (.I0(mul_r_reg__1[38]),
        .I1(CO),
        .O(\add_r[36]_i_3__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_4__4 
       (.I0(mul_r_reg__1[37]),
        .I1(CO),
        .O(\add_r[36]_i_4__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_5__4 
       (.I0(mul_r_reg__1[36]),
        .I1(CO),
        .O(\add_r[36]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_6__4 
       (.I0(D[39]),
        .I1(CO),
        .I2(mul_r_reg__1[39]),
        .O(\add_r[36]_i_6__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_7__4 
       (.I0(D[38]),
        .I1(CO),
        .I2(mul_r_reg__1[38]),
        .O(\add_r[36]_i_7__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_8__4 
       (.I0(D[37]),
        .I1(CO),
        .I2(mul_r_reg__1[37]),
        .O(\add_r[36]_i_8__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_9__4 
       (.I0(D[36]),
        .I1(CO),
        .I2(mul_r_reg__1[36]),
        .O(\add_r[36]_i_9__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_2__4 
       (.I0(mul_r_reg__1[43]),
        .I1(CO),
        .O(\add_r[40]_i_2__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_3__4 
       (.I0(mul_r_reg__1[42]),
        .I1(CO),
        .O(\add_r[40]_i_3__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_4__4 
       (.I0(mul_r_reg__1[41]),
        .I1(CO),
        .O(\add_r[40]_i_4__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_5__4 
       (.I0(mul_r_reg__1[40]),
        .I1(CO),
        .O(\add_r[40]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_6__4 
       (.I0(D[43]),
        .I1(CO),
        .I2(mul_r_reg__1[43]),
        .O(\add_r[40]_i_6__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_7__4 
       (.I0(D[42]),
        .I1(CO),
        .I2(mul_r_reg__1[42]),
        .O(\add_r[40]_i_7__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_8__4 
       (.I0(D[41]),
        .I1(CO),
        .I2(mul_r_reg__1[41]),
        .O(\add_r[40]_i_8__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_9__4 
       (.I0(D[40]),
        .I1(CO),
        .I2(mul_r_reg__1[40]),
        .O(\add_r[40]_i_9__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_2__4 
       (.I0(mul_r_reg__1[46]),
        .I1(CO),
        .O(\add_r[44]_i_2__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_3__4 
       (.I0(mul_r_reg__1[45]),
        .I1(CO),
        .O(\add_r[44]_i_3__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_4__4 
       (.I0(mul_r_reg__1[44]),
        .I1(CO),
        .O(\add_r[44]_i_4__4_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_5__4 
       (.I0(mul_r_reg__1[47]),
        .I1(CO),
        .I2(D[47]),
        .O(\add_r[44]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_6__4 
       (.I0(D[46]),
        .I1(CO),
        .I2(mul_r_reg__1[46]),
        .O(\add_r[44]_i_6__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_7__4 
       (.I0(D[45]),
        .I1(CO),
        .I2(mul_r_reg__1[45]),
        .O(\add_r[44]_i_7__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_8__4 
       (.I0(D[44]),
        .I1(CO),
        .I2(mul_r_reg__1[44]),
        .O(\add_r[44]_i_8__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_2__4 
       (.I0(mul_r_reg__1[7]),
        .I1(CO),
        .O(\add_r[4]_i_2__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_3__4 
       (.I0(mul_r_reg__1[6]),
        .I1(CO),
        .O(\add_r[4]_i_3__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_4__4 
       (.I0(mul_r_reg__1[5]),
        .I1(CO),
        .O(\add_r[4]_i_4__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_5__4 
       (.I0(mul_r_reg__1[4]),
        .I1(CO),
        .O(\add_r[4]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_6__4 
       (.I0(D[7]),
        .I1(CO),
        .I2(mul_r_reg__1[7]),
        .O(\add_r[4]_i_6__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_7__4 
       (.I0(D[6]),
        .I1(CO),
        .I2(mul_r_reg__1[6]),
        .O(\add_r[4]_i_7__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_8__4 
       (.I0(D[5]),
        .I1(CO),
        .I2(mul_r_reg__1[5]),
        .O(\add_r[4]_i_8__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_9__4 
       (.I0(D[4]),
        .I1(CO),
        .I2(mul_r_reg__1[4]),
        .O(\add_r[4]_i_9__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_2__4 
       (.I0(mul_r_reg__1[11]),
        .I1(CO),
        .O(\add_r[8]_i_2__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_3__4 
       (.I0(mul_r_reg__1[10]),
        .I1(CO),
        .O(\add_r[8]_i_3__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_4__4 
       (.I0(mul_r_reg__1[9]),
        .I1(CO),
        .O(\add_r[8]_i_4__4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_5__4 
       (.I0(mul_r_reg__1[8]),
        .I1(CO),
        .O(\add_r[8]_i_5__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_6__4 
       (.I0(D[11]),
        .I1(CO),
        .I2(mul_r_reg__1[11]),
        .O(\add_r[8]_i_6__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_7__4 
       (.I0(D[10]),
        .I1(CO),
        .I2(mul_r_reg__1[10]),
        .O(\add_r[8]_i_7__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_8__4 
       (.I0(D[9]),
        .I1(CO),
        .I2(mul_r_reg__1[9]),
        .O(\add_r[8]_i_8__4_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_9__4 
       (.I0(D[8]),
        .I1(CO),
        .I2(mul_r_reg__1[8]),
        .O(\add_r[8]_i_9__4_n_0 ));
  FDRE \add_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__4_n_7 ),
        .Q(D[0]),
        .R(aresetn));
  CARRY4 \add_r_reg[0]_i_1__4 
       (.CI(1'b0),
        .CO({\add_r_reg[0]_i_1__4_n_0 ,\add_r_reg[0]_i_1__4_n_1 ,\add_r_reg[0]_i_1__4_n_2 ,\add_r_reg[0]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[0]_i_2__4_n_0 ,\add_r[0]_i_3__4_n_0 ,\add_r[0]_i_4__4_n_0 ,\add_r[0]_i_5__4_n_0 }),
        .O({\add_r_reg[0]_i_1__4_n_4 ,\add_r_reg[0]_i_1__4_n_5 ,\add_r_reg[0]_i_1__4_n_6 ,\add_r_reg[0]_i_1__4_n_7 }),
        .S({\add_r[0]_i_6__4_n_0 ,\add_r[0]_i_7__4_n_0 ,\add_r[0]_i_8__4_n_0 ,\add_r[0]_i_9__4_n_0 }));
  FDRE \add_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__4_n_5 ),
        .Q(D[10]),
        .R(aresetn));
  FDRE \add_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__4_n_4 ),
        .Q(D[11]),
        .R(aresetn));
  FDRE \add_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__4_n_7 ),
        .Q(D[12]),
        .R(aresetn));
  CARRY4 \add_r_reg[12]_i_1__4 
       (.CI(\add_r_reg[8]_i_1__4_n_0 ),
        .CO({\add_r_reg[12]_i_1__4_n_0 ,\add_r_reg[12]_i_1__4_n_1 ,\add_r_reg[12]_i_1__4_n_2 ,\add_r_reg[12]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[12]_i_2__4_n_0 ,\add_r[12]_i_3__4_n_0 ,\add_r[12]_i_4__4_n_0 ,\add_r[12]_i_5__4_n_0 }),
        .O({\add_r_reg[12]_i_1__4_n_4 ,\add_r_reg[12]_i_1__4_n_5 ,\add_r_reg[12]_i_1__4_n_6 ,\add_r_reg[12]_i_1__4_n_7 }),
        .S({\add_r[12]_i_6__4_n_0 ,\add_r[12]_i_7__4_n_0 ,\add_r[12]_i_8__4_n_0 ,\add_r[12]_i_9__4_n_0 }));
  FDRE \add_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__4_n_6 ),
        .Q(D[13]),
        .R(aresetn));
  FDRE \add_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__4_n_5 ),
        .Q(D[14]),
        .R(aresetn));
  FDRE \add_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__4_n_4 ),
        .Q(D[15]),
        .R(aresetn));
  FDRE \add_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__4_n_7 ),
        .Q(D[16]),
        .R(aresetn));
  CARRY4 \add_r_reg[16]_i_1__4 
       (.CI(\add_r_reg[12]_i_1__4_n_0 ),
        .CO({\add_r_reg[16]_i_1__4_n_0 ,\add_r_reg[16]_i_1__4_n_1 ,\add_r_reg[16]_i_1__4_n_2 ,\add_r_reg[16]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[16]_i_2__4_n_0 ,\add_r[16]_i_3__4_n_0 ,\add_r[16]_i_4__4_n_0 ,\add_r[16]_i_5__4_n_0 }),
        .O({\add_r_reg[16]_i_1__4_n_4 ,\add_r_reg[16]_i_1__4_n_5 ,\add_r_reg[16]_i_1__4_n_6 ,\add_r_reg[16]_i_1__4_n_7 }),
        .S({\add_r[16]_i_6__4_n_0 ,\add_r[16]_i_7__4_n_0 ,\add_r[16]_i_8__4_n_0 ,\add_r[16]_i_9__4_n_0 }));
  FDRE \add_r_reg[17] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__4_n_6 ),
        .Q(D[17]),
        .R(aresetn));
  FDRE \add_r_reg[18] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__4_n_5 ),
        .Q(D[18]),
        .R(aresetn));
  FDRE \add_r_reg[19] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__4_n_4 ),
        .Q(D[19]),
        .R(aresetn));
  FDRE \add_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__4_n_6 ),
        .Q(D[1]),
        .R(aresetn));
  FDRE \add_r_reg[20] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__4_n_7 ),
        .Q(D[20]),
        .R(aresetn));
  CARRY4 \add_r_reg[20]_i_1__4 
       (.CI(\add_r_reg[16]_i_1__4_n_0 ),
        .CO({\add_r_reg[20]_i_1__4_n_0 ,\add_r_reg[20]_i_1__4_n_1 ,\add_r_reg[20]_i_1__4_n_2 ,\add_r_reg[20]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[20]_i_2__4_n_0 ,\add_r[20]_i_3__4_n_0 ,\add_r[20]_i_4__4_n_0 ,\add_r[20]_i_5__4_n_0 }),
        .O({\add_r_reg[20]_i_1__4_n_4 ,\add_r_reg[20]_i_1__4_n_5 ,\add_r_reg[20]_i_1__4_n_6 ,\add_r_reg[20]_i_1__4_n_7 }),
        .S({\add_r[20]_i_6__4_n_0 ,\add_r[20]_i_7__4_n_0 ,\add_r[20]_i_8__4_n_0 ,\add_r[20]_i_9__4_n_0 }));
  FDRE \add_r_reg[21] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__4_n_6 ),
        .Q(D[21]),
        .R(aresetn));
  FDRE \add_r_reg[22] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__4_n_5 ),
        .Q(D[22]),
        .R(aresetn));
  FDRE \add_r_reg[23] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__4_n_4 ),
        .Q(D[23]),
        .R(aresetn));
  FDRE \add_r_reg[24] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__4_n_7 ),
        .Q(D[24]),
        .R(aresetn));
  CARRY4 \add_r_reg[24]_i_1__4 
       (.CI(\add_r_reg[20]_i_1__4_n_0 ),
        .CO({\add_r_reg[24]_i_1__4_n_0 ,\add_r_reg[24]_i_1__4_n_1 ,\add_r_reg[24]_i_1__4_n_2 ,\add_r_reg[24]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[24]_i_2__4_n_0 ,\add_r[24]_i_3__4_n_0 ,\add_r[24]_i_4__4_n_0 ,\add_r[24]_i_5__4_n_0 }),
        .O({\add_r_reg[24]_i_1__4_n_4 ,\add_r_reg[24]_i_1__4_n_5 ,\add_r_reg[24]_i_1__4_n_6 ,\add_r_reg[24]_i_1__4_n_7 }),
        .S({\add_r[24]_i_6__4_n_0 ,\add_r[24]_i_7__4_n_0 ,\add_r[24]_i_8__4_n_0 ,\add_r[24]_i_9__4_n_0 }));
  FDRE \add_r_reg[25] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__4_n_6 ),
        .Q(D[25]),
        .R(aresetn));
  FDRE \add_r_reg[26] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__4_n_5 ),
        .Q(D[26]),
        .R(aresetn));
  FDRE \add_r_reg[27] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__4_n_4 ),
        .Q(D[27]),
        .R(aresetn));
  FDRE \add_r_reg[28] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__4_n_7 ),
        .Q(D[28]),
        .R(aresetn));
  CARRY4 \add_r_reg[28]_i_1__4 
       (.CI(\add_r_reg[24]_i_1__4_n_0 ),
        .CO({\add_r_reg[28]_i_1__4_n_0 ,\add_r_reg[28]_i_1__4_n_1 ,\add_r_reg[28]_i_1__4_n_2 ,\add_r_reg[28]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[28]_i_2__4_n_0 ,\add_r[28]_i_3__4_n_0 ,\add_r[28]_i_4__4_n_0 ,\add_r[28]_i_5__4_n_0 }),
        .O({\add_r_reg[28]_i_1__4_n_4 ,\add_r_reg[28]_i_1__4_n_5 ,\add_r_reg[28]_i_1__4_n_6 ,\add_r_reg[28]_i_1__4_n_7 }),
        .S({\add_r[28]_i_6__4_n_0 ,\add_r[28]_i_7__4_n_0 ,\add_r[28]_i_8__4_n_0 ,\add_r[28]_i_9__4_n_0 }));
  FDRE \add_r_reg[29] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__4_n_6 ),
        .Q(D[29]),
        .R(aresetn));
  FDRE \add_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__4_n_5 ),
        .Q(D[2]),
        .R(aresetn));
  FDRE \add_r_reg[30] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__4_n_5 ),
        .Q(D[30]),
        .R(aresetn));
  FDRE \add_r_reg[31] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__4_n_4 ),
        .Q(D[31]),
        .R(aresetn));
  FDRE \add_r_reg[32] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__4_n_7 ),
        .Q(D[32]),
        .R(aresetn));
  CARRY4 \add_r_reg[32]_i_1__4 
       (.CI(\add_r_reg[28]_i_1__4_n_0 ),
        .CO({\add_r_reg[32]_i_1__4_n_0 ,\add_r_reg[32]_i_1__4_n_1 ,\add_r_reg[32]_i_1__4_n_2 ,\add_r_reg[32]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[32]_i_2__4_n_0 ,\add_r[32]_i_3__4_n_0 ,\add_r[32]_i_4__4_n_0 ,\add_r[32]_i_5__4_n_0 }),
        .O({\add_r_reg[32]_i_1__4_n_4 ,\add_r_reg[32]_i_1__4_n_5 ,\add_r_reg[32]_i_1__4_n_6 ,\add_r_reg[32]_i_1__4_n_7 }),
        .S({\add_r[32]_i_6__4_n_0 ,\add_r[32]_i_7__4_n_0 ,\add_r[32]_i_8__4_n_0 ,\add_r[32]_i_9__4_n_0 }));
  FDRE \add_r_reg[33] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__4_n_6 ),
        .Q(D[33]),
        .R(aresetn));
  FDRE \add_r_reg[34] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__4_n_5 ),
        .Q(D[34]),
        .R(aresetn));
  FDRE \add_r_reg[35] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__4_n_4 ),
        .Q(D[35]),
        .R(aresetn));
  FDRE \add_r_reg[36] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__4_n_7 ),
        .Q(D[36]),
        .R(aresetn));
  CARRY4 \add_r_reg[36]_i_1__4 
       (.CI(\add_r_reg[32]_i_1__4_n_0 ),
        .CO({\add_r_reg[36]_i_1__4_n_0 ,\add_r_reg[36]_i_1__4_n_1 ,\add_r_reg[36]_i_1__4_n_2 ,\add_r_reg[36]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[36]_i_2__4_n_0 ,\add_r[36]_i_3__4_n_0 ,\add_r[36]_i_4__4_n_0 ,\add_r[36]_i_5__4_n_0 }),
        .O({\add_r_reg[36]_i_1__4_n_4 ,\add_r_reg[36]_i_1__4_n_5 ,\add_r_reg[36]_i_1__4_n_6 ,\add_r_reg[36]_i_1__4_n_7 }),
        .S({\add_r[36]_i_6__4_n_0 ,\add_r[36]_i_7__4_n_0 ,\add_r[36]_i_8__4_n_0 ,\add_r[36]_i_9__4_n_0 }));
  FDRE \add_r_reg[37] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__4_n_6 ),
        .Q(D[37]),
        .R(aresetn));
  FDRE \add_r_reg[38] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__4_n_5 ),
        .Q(D[38]),
        .R(aresetn));
  FDRE \add_r_reg[39] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__4_n_4 ),
        .Q(D[39]),
        .R(aresetn));
  FDRE \add_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__4_n_4 ),
        .Q(D[3]),
        .R(aresetn));
  FDRE \add_r_reg[40] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__4_n_7 ),
        .Q(D[40]),
        .R(aresetn));
  CARRY4 \add_r_reg[40]_i_1__4 
       (.CI(\add_r_reg[36]_i_1__4_n_0 ),
        .CO({\add_r_reg[40]_i_1__4_n_0 ,\add_r_reg[40]_i_1__4_n_1 ,\add_r_reg[40]_i_1__4_n_2 ,\add_r_reg[40]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[40]_i_2__4_n_0 ,\add_r[40]_i_3__4_n_0 ,\add_r[40]_i_4__4_n_0 ,\add_r[40]_i_5__4_n_0 }),
        .O({\add_r_reg[40]_i_1__4_n_4 ,\add_r_reg[40]_i_1__4_n_5 ,\add_r_reg[40]_i_1__4_n_6 ,\add_r_reg[40]_i_1__4_n_7 }),
        .S({\add_r[40]_i_6__4_n_0 ,\add_r[40]_i_7__4_n_0 ,\add_r[40]_i_8__4_n_0 ,\add_r[40]_i_9__4_n_0 }));
  FDRE \add_r_reg[41] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__4_n_6 ),
        .Q(D[41]),
        .R(aresetn));
  FDRE \add_r_reg[42] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__4_n_5 ),
        .Q(D[42]),
        .R(aresetn));
  FDRE \add_r_reg[43] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__4_n_4 ),
        .Q(D[43]),
        .R(aresetn));
  FDRE \add_r_reg[44] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__4_n_7 ),
        .Q(D[44]),
        .R(aresetn));
  CARRY4 \add_r_reg[44]_i_1__4 
       (.CI(\add_r_reg[40]_i_1__4_n_0 ),
        .CO({\NLW_add_r_reg[44]_i_1__4_CO_UNCONNECTED [3],\add_r_reg[44]_i_1__4_n_1 ,\add_r_reg[44]_i_1__4_n_2 ,\add_r_reg[44]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\add_r[44]_i_2__4_n_0 ,\add_r[44]_i_3__4_n_0 ,\add_r[44]_i_4__4_n_0 }),
        .O({\add_r_reg[44]_i_1__4_n_4 ,\add_r_reg[44]_i_1__4_n_5 ,\add_r_reg[44]_i_1__4_n_6 ,\add_r_reg[44]_i_1__4_n_7 }),
        .S({\add_r[44]_i_5__4_n_0 ,\add_r[44]_i_6__4_n_0 ,\add_r[44]_i_7__4_n_0 ,\add_r[44]_i_8__4_n_0 }));
  FDRE \add_r_reg[45] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__4_n_6 ),
        .Q(D[45]),
        .R(aresetn));
  FDRE \add_r_reg[46] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__4_n_5 ),
        .Q(D[46]),
        .R(aresetn));
  FDRE \add_r_reg[47] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__4_n_4 ),
        .Q(D[47]),
        .R(aresetn));
  FDRE \add_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__4_n_7 ),
        .Q(D[4]),
        .R(aresetn));
  CARRY4 \add_r_reg[4]_i_1__4 
       (.CI(\add_r_reg[0]_i_1__4_n_0 ),
        .CO({\add_r_reg[4]_i_1__4_n_0 ,\add_r_reg[4]_i_1__4_n_1 ,\add_r_reg[4]_i_1__4_n_2 ,\add_r_reg[4]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[4]_i_2__4_n_0 ,\add_r[4]_i_3__4_n_0 ,\add_r[4]_i_4__4_n_0 ,\add_r[4]_i_5__4_n_0 }),
        .O({\add_r_reg[4]_i_1__4_n_4 ,\add_r_reg[4]_i_1__4_n_5 ,\add_r_reg[4]_i_1__4_n_6 ,\add_r_reg[4]_i_1__4_n_7 }),
        .S({\add_r[4]_i_6__4_n_0 ,\add_r[4]_i_7__4_n_0 ,\add_r[4]_i_8__4_n_0 ,\add_r[4]_i_9__4_n_0 }));
  FDRE \add_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__4_n_6 ),
        .Q(D[5]),
        .R(aresetn));
  FDRE \add_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__4_n_5 ),
        .Q(D[6]),
        .R(aresetn));
  FDRE \add_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__4_n_4 ),
        .Q(D[7]),
        .R(aresetn));
  FDRE \add_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__4_n_7 ),
        .Q(D[8]),
        .R(aresetn));
  CARRY4 \add_r_reg[8]_i_1__4 
       (.CI(\add_r_reg[4]_i_1__4_n_0 ),
        .CO({\add_r_reg[8]_i_1__4_n_0 ,\add_r_reg[8]_i_1__4_n_1 ,\add_r_reg[8]_i_1__4_n_2 ,\add_r_reg[8]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[8]_i_2__4_n_0 ,\add_r[8]_i_3__4_n_0 ,\add_r[8]_i_4__4_n_0 ,\add_r[8]_i_5__4_n_0 }),
        .O({\add_r_reg[8]_i_1__4_n_4 ,\add_r_reg[8]_i_1__4_n_5 ,\add_r_reg[8]_i_1__4_n_6 ,\add_r_reg[8]_i_1__4_n_7 }),
        .S({\add_r[8]_i_6__4_n_0 ,\add_r[8]_i_7__4_n_0 ,\add_r[8]_i_8__4_n_0 ,\add_r[8]_i_9__4_n_0 }));
  FDRE \add_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__4_n_6 ),
        .Q(D[9]),
        .R(aresetn));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter[0]_i_1__7 
       (.I0(\counter_reg_n_0_[0] ),
        .O(\counter[0]_i_1__7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT4 #(
    .INIT(16'hFD0E)) 
    \counter[1]_i_1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(CO),
        .I2(dp_extend_end_reg_0),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \counter[2]_i_1__6 
       (.I0(\counter_reg_n_0_[2] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[1] ),
        .O(\counter[2]_i_1__6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \counter[3]_i_1__6 
       (.I0(Q[0]),
        .I1(\counter_reg_n_0_[2] ),
        .I2(\counter_reg_n_0_[0] ),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[3]_i_1__6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \counter[4]_i_1__3 
       (.I0(Q[1]),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[0] ),
        .I3(\counter_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(\counter[4]_i_1__3_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \counter[5]_i_1__5 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[1] ),
        .I5(Q[1]),
        .O(\counter[5]_i_1__5_n_0 ));
  LUT5 #(
    .INIT(32'hA6AAAAAA)) 
    \counter[6]_i_1__7 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(\counter[7]_i_2__3_n_0 ),
        .I3(Q[0]),
        .I4(Q[2]),
        .O(\counter[6]_i_1__7_n_0 ));
  LUT6 #(
    .INIT(64'hAA6AAAAAAAAAAAAA)) 
    \counter[7]_i_1__4 
       (.I0(Q[4]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(\counter[7]_i_2__3_n_0 ),
        .I4(Q[1]),
        .I5(Q[3]),
        .O(\counter[7]_i_1__4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \counter[7]_i_2__3 
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[2] ),
        .O(\counter[7]_i_2__3_n_0 ));
  LUT4 #(
    .INIT(16'hA6AA)) 
    \counter[8]_i_2__5 
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(\counter[8]_i_3__5_n_0 ),
        .I3(Q[4]),
        .O(\counter[8]_i_2__5_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \counter[8]_i_3__5 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[1] ),
        .I5(Q[1]),
        .O(\counter[8]_i_3__5_n_0 ));
  FDRE \counter_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[0]_i_1__7_n_0 ),
        .Q(\counter_reg_n_0_[0] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\counter[1]_i_1_n_0 ),
        .Q(\counter_reg_n_0_[1] ),
        .R(aresetn));
  FDRE \counter_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[2]_i_1__6_n_0 ),
        .Q(\counter_reg_n_0_[2] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[3]_i_1__6_n_0 ),
        .Q(Q[0]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[4]_i_1__3_n_0 ),
        .Q(Q[1]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[5]_i_1__5_n_0 ),
        .Q(Q[2]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[6]_i_1__7_n_0 ),
        .Q(Q[3]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[7]_i_1__4_n_0 ),
        .Q(Q[4]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[8]_i_2__5_n_0 ),
        .Q(Q[5]),
        .R(dp_extend_end_reg));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,b_ram_out[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({A[17],A[17],A[17],A[14:0]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r0_OVERFLOW_UNCONNECTED),
        .P({mul_r0_n_58,mul_r0_n_59,mul_r0_n_60,mul_r0_n_61,mul_r0_n_62,mul_r0_n_63,mul_r0_n_64,mul_r0_n_65,mul_r0_n_66,mul_r0_n_67,mul_r0_n_68,mul_r0_n_69,mul_r0_n_70,mul_r0_n_71,mul_r0_n_72,mul_r0_n_73,mul_r0_n_74,mul_r0_n_75,mul_r0_n_76,mul_r0_n_77,mul_r0_n_78,mul_r0_n_79,mul_r0_n_80,mul_r0_n_81,mul_r0_n_82,mul_r0_n_83,mul_r0_n_84,mul_r0_n_85,mul_r0_n_86,mul_r0_n_87,mul_r0_n_88,mul_r0_n_89,mul_r0_n_90,mul_r0_n_91,mul_r0_n_92,mul_r0_n_93,mul_r0_n_94,mul_r0_n_95,mul_r0_n_96,mul_r0_n_97,mul_r0_n_98,mul_r0_n_99,mul_r0_n_100,mul_r0_n_101,mul_r0_n_102,mul_r0_n_103,mul_r0_n_104,mul_r0_n_105}),
        .PATTERNBDETECT(NLW_mul_r0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_r0_UNDERFLOW_UNCONNECTED));
  FDRE \mul_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_105),
        .Q(mul_r_reg__1[0]),
        .R(aresetn));
  FDRE \mul_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_95),
        .Q(mul_r_reg__1[10]),
        .R(aresetn));
  FDRE \mul_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_94),
        .Q(mul_r_reg__1[11]),
        .R(aresetn));
  FDRE \mul_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_93),
        .Q(mul_r_reg__1[12]),
        .R(aresetn));
  FDRE \mul_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_92),
        .Q(mul_r_reg__1[13]),
        .R(aresetn));
  FDRE \mul_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_91),
        .Q(mul_r_reg__1[14]),
        .R(aresetn));
  FDRE \mul_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_90),
        .Q(mul_r_reg__1[15]),
        .R(aresetn));
  FDRE \mul_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_89),
        .Q(mul_r_reg__1[16]),
        .R(aresetn));
  FDRE \mul_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_104),
        .Q(mul_r_reg__1[1]),
        .R(aresetn));
  FDRE \mul_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_103),
        .Q(mul_r_reg__1[2]),
        .R(aresetn));
  FDRE \mul_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_102),
        .Q(mul_r_reg__1[3]),
        .R(aresetn));
  FDRE \mul_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_101),
        .Q(mul_r_reg__1[4]),
        .R(aresetn));
  FDRE \mul_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_100),
        .Q(mul_r_reg__1[5]),
        .R(aresetn));
  FDRE \mul_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_99),
        .Q(mul_r_reg__1[6]),
        .R(aresetn));
  FDRE \mul_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_98),
        .Q(mul_r_reg__1[7]),
        .R(aresetn));
  FDRE \mul_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_97),
        .Q(mul_r_reg__1[8]),
        .R(aresetn));
  FDRE \mul_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_96),
        .Q(mul_r_reg__1[9]),
        .R(aresetn));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-12 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r_reg__0
       (.A({A[17:16],A[16],A[16],A[16],A[16],A[16],A[16],A[16],A[16],A[16:15],A[15],A[15],A[15:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r_reg__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({b_ram_out[31],b_ram_out[31],b_ram_out[31],b_ram_out[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r_reg__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(dp_enable),
        .CLK(clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED),
        .P({mul_r_reg__0_n_58,mul_r_reg__0_n_59,mul_r_reg__0_n_60,mul_r_reg__0_n_61,mul_r_reg__0_n_62,mul_r_reg__0_n_63,mul_r_reg__0_n_64,mul_r_reg__0_n_65,mul_r_reg__0_n_66,mul_r_reg__0_n_67,mul_r_reg__0_n_68,mul_r_reg__0_n_69,mul_r_reg__0_n_70,mul_r_reg__0_n_71,mul_r_reg__0_n_72,mul_r_reg__0_n_73,mul_r_reg__0_n_74,mul_r_reg__1[47:17]}),
        .PATTERNBDETECT(NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .PCOUT(NLW_mul_r_reg__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(aresetn),
        .UNDERFLOW(NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "dot_product" *) 
module design_1_top_0_0_dot_product_5
   (D,
    CO,
    Q,
    A,
    b_ram_out,
    dp_enable,
    clk,
    aresetn,
    \slv_regs_reg[0][11] ,
    O,
    \slv_regs_reg[0][8] ,
    \slv_regs_reg[0][0] ,
    dp_extend_end_reg,
    dp_extend_end_reg_0);
  output [47:0]D;
  output [0:0]CO;
  output [2:0]Q;
  input [17:0]A;
  input [31:0]b_ram_out;
  input dp_enable;
  input clk;
  input aresetn;
  input [1:0]\slv_regs_reg[0][11] ;
  input [0:0]O;
  input [4:0]\slv_regs_reg[0][8] ;
  input [0:0]\slv_regs_reg[0][0] ;
  input [0:0]dp_extend_end_reg;
  input dp_extend_end_reg_0;

  wire [17:0]A;
  wire [0:0]CO;
  wire [47:0]D;
  wire [0:0]O;
  wire [2:0]Q;
  wire add_r1_carry_i_2__5_n_0;
  wire add_r1_carry_i_4__5_n_0;
  wire add_r1_carry_n_0;
  wire add_r1_carry_n_1;
  wire add_r1_carry_n_2;
  wire add_r1_carry_n_3;
  wire \add_r[0]_i_2__5_n_0 ;
  wire \add_r[0]_i_3__5_n_0 ;
  wire \add_r[0]_i_4__5_n_0 ;
  wire \add_r[0]_i_5__5_n_0 ;
  wire \add_r[0]_i_6__5_n_0 ;
  wire \add_r[0]_i_7__5_n_0 ;
  wire \add_r[0]_i_8__5_n_0 ;
  wire \add_r[0]_i_9__5_n_0 ;
  wire \add_r[12]_i_2__5_n_0 ;
  wire \add_r[12]_i_3__5_n_0 ;
  wire \add_r[12]_i_4__5_n_0 ;
  wire \add_r[12]_i_5__5_n_0 ;
  wire \add_r[12]_i_6__5_n_0 ;
  wire \add_r[12]_i_7__5_n_0 ;
  wire \add_r[12]_i_8__5_n_0 ;
  wire \add_r[12]_i_9__5_n_0 ;
  wire \add_r[16]_i_2__5_n_0 ;
  wire \add_r[16]_i_3__5_n_0 ;
  wire \add_r[16]_i_4__5_n_0 ;
  wire \add_r[16]_i_5__5_n_0 ;
  wire \add_r[16]_i_6__5_n_0 ;
  wire \add_r[16]_i_7__5_n_0 ;
  wire \add_r[16]_i_8__5_n_0 ;
  wire \add_r[16]_i_9__5_n_0 ;
  wire \add_r[20]_i_2__5_n_0 ;
  wire \add_r[20]_i_3__5_n_0 ;
  wire \add_r[20]_i_4__5_n_0 ;
  wire \add_r[20]_i_5__5_n_0 ;
  wire \add_r[20]_i_6__5_n_0 ;
  wire \add_r[20]_i_7__5_n_0 ;
  wire \add_r[20]_i_8__5_n_0 ;
  wire \add_r[20]_i_9__5_n_0 ;
  wire \add_r[24]_i_2__5_n_0 ;
  wire \add_r[24]_i_3__5_n_0 ;
  wire \add_r[24]_i_4__5_n_0 ;
  wire \add_r[24]_i_5__5_n_0 ;
  wire \add_r[24]_i_6__5_n_0 ;
  wire \add_r[24]_i_7__5_n_0 ;
  wire \add_r[24]_i_8__5_n_0 ;
  wire \add_r[24]_i_9__5_n_0 ;
  wire \add_r[28]_i_2__5_n_0 ;
  wire \add_r[28]_i_3__5_n_0 ;
  wire \add_r[28]_i_4__5_n_0 ;
  wire \add_r[28]_i_5__5_n_0 ;
  wire \add_r[28]_i_6__5_n_0 ;
  wire \add_r[28]_i_7__5_n_0 ;
  wire \add_r[28]_i_8__5_n_0 ;
  wire \add_r[28]_i_9__5_n_0 ;
  wire \add_r[32]_i_2__5_n_0 ;
  wire \add_r[32]_i_3__5_n_0 ;
  wire \add_r[32]_i_4__5_n_0 ;
  wire \add_r[32]_i_5__5_n_0 ;
  wire \add_r[32]_i_6__5_n_0 ;
  wire \add_r[32]_i_7__5_n_0 ;
  wire \add_r[32]_i_8__5_n_0 ;
  wire \add_r[32]_i_9__5_n_0 ;
  wire \add_r[36]_i_2__5_n_0 ;
  wire \add_r[36]_i_3__5_n_0 ;
  wire \add_r[36]_i_4__5_n_0 ;
  wire \add_r[36]_i_5__5_n_0 ;
  wire \add_r[36]_i_6__5_n_0 ;
  wire \add_r[36]_i_7__5_n_0 ;
  wire \add_r[36]_i_8__5_n_0 ;
  wire \add_r[36]_i_9__5_n_0 ;
  wire \add_r[40]_i_2__5_n_0 ;
  wire \add_r[40]_i_3__5_n_0 ;
  wire \add_r[40]_i_4__5_n_0 ;
  wire \add_r[40]_i_5__5_n_0 ;
  wire \add_r[40]_i_6__5_n_0 ;
  wire \add_r[40]_i_7__5_n_0 ;
  wire \add_r[40]_i_8__5_n_0 ;
  wire \add_r[40]_i_9__5_n_0 ;
  wire \add_r[44]_i_2__5_n_0 ;
  wire \add_r[44]_i_3__5_n_0 ;
  wire \add_r[44]_i_4__5_n_0 ;
  wire \add_r[44]_i_5__5_n_0 ;
  wire \add_r[44]_i_6__5_n_0 ;
  wire \add_r[44]_i_7__5_n_0 ;
  wire \add_r[44]_i_8__5_n_0 ;
  wire \add_r[4]_i_2__5_n_0 ;
  wire \add_r[4]_i_3__5_n_0 ;
  wire \add_r[4]_i_4__5_n_0 ;
  wire \add_r[4]_i_5__5_n_0 ;
  wire \add_r[4]_i_6__5_n_0 ;
  wire \add_r[4]_i_7__5_n_0 ;
  wire \add_r[4]_i_8__5_n_0 ;
  wire \add_r[4]_i_9__5_n_0 ;
  wire \add_r[8]_i_2__5_n_0 ;
  wire \add_r[8]_i_3__5_n_0 ;
  wire \add_r[8]_i_4__5_n_0 ;
  wire \add_r[8]_i_5__5_n_0 ;
  wire \add_r[8]_i_6__5_n_0 ;
  wire \add_r[8]_i_7__5_n_0 ;
  wire \add_r[8]_i_8__5_n_0 ;
  wire \add_r[8]_i_9__5_n_0 ;
  wire \add_r_reg[0]_i_1__5_n_0 ;
  wire \add_r_reg[0]_i_1__5_n_1 ;
  wire \add_r_reg[0]_i_1__5_n_2 ;
  wire \add_r_reg[0]_i_1__5_n_3 ;
  wire \add_r_reg[0]_i_1__5_n_4 ;
  wire \add_r_reg[0]_i_1__5_n_5 ;
  wire \add_r_reg[0]_i_1__5_n_6 ;
  wire \add_r_reg[0]_i_1__5_n_7 ;
  wire \add_r_reg[12]_i_1__5_n_0 ;
  wire \add_r_reg[12]_i_1__5_n_1 ;
  wire \add_r_reg[12]_i_1__5_n_2 ;
  wire \add_r_reg[12]_i_1__5_n_3 ;
  wire \add_r_reg[12]_i_1__5_n_4 ;
  wire \add_r_reg[12]_i_1__5_n_5 ;
  wire \add_r_reg[12]_i_1__5_n_6 ;
  wire \add_r_reg[12]_i_1__5_n_7 ;
  wire \add_r_reg[16]_i_1__5_n_0 ;
  wire \add_r_reg[16]_i_1__5_n_1 ;
  wire \add_r_reg[16]_i_1__5_n_2 ;
  wire \add_r_reg[16]_i_1__5_n_3 ;
  wire \add_r_reg[16]_i_1__5_n_4 ;
  wire \add_r_reg[16]_i_1__5_n_5 ;
  wire \add_r_reg[16]_i_1__5_n_6 ;
  wire \add_r_reg[16]_i_1__5_n_7 ;
  wire \add_r_reg[20]_i_1__5_n_0 ;
  wire \add_r_reg[20]_i_1__5_n_1 ;
  wire \add_r_reg[20]_i_1__5_n_2 ;
  wire \add_r_reg[20]_i_1__5_n_3 ;
  wire \add_r_reg[20]_i_1__5_n_4 ;
  wire \add_r_reg[20]_i_1__5_n_5 ;
  wire \add_r_reg[20]_i_1__5_n_6 ;
  wire \add_r_reg[20]_i_1__5_n_7 ;
  wire \add_r_reg[24]_i_1__5_n_0 ;
  wire \add_r_reg[24]_i_1__5_n_1 ;
  wire \add_r_reg[24]_i_1__5_n_2 ;
  wire \add_r_reg[24]_i_1__5_n_3 ;
  wire \add_r_reg[24]_i_1__5_n_4 ;
  wire \add_r_reg[24]_i_1__5_n_5 ;
  wire \add_r_reg[24]_i_1__5_n_6 ;
  wire \add_r_reg[24]_i_1__5_n_7 ;
  wire \add_r_reg[28]_i_1__5_n_0 ;
  wire \add_r_reg[28]_i_1__5_n_1 ;
  wire \add_r_reg[28]_i_1__5_n_2 ;
  wire \add_r_reg[28]_i_1__5_n_3 ;
  wire \add_r_reg[28]_i_1__5_n_4 ;
  wire \add_r_reg[28]_i_1__5_n_5 ;
  wire \add_r_reg[28]_i_1__5_n_6 ;
  wire \add_r_reg[28]_i_1__5_n_7 ;
  wire \add_r_reg[32]_i_1__5_n_0 ;
  wire \add_r_reg[32]_i_1__5_n_1 ;
  wire \add_r_reg[32]_i_1__5_n_2 ;
  wire \add_r_reg[32]_i_1__5_n_3 ;
  wire \add_r_reg[32]_i_1__5_n_4 ;
  wire \add_r_reg[32]_i_1__5_n_5 ;
  wire \add_r_reg[32]_i_1__5_n_6 ;
  wire \add_r_reg[32]_i_1__5_n_7 ;
  wire \add_r_reg[36]_i_1__5_n_0 ;
  wire \add_r_reg[36]_i_1__5_n_1 ;
  wire \add_r_reg[36]_i_1__5_n_2 ;
  wire \add_r_reg[36]_i_1__5_n_3 ;
  wire \add_r_reg[36]_i_1__5_n_4 ;
  wire \add_r_reg[36]_i_1__5_n_5 ;
  wire \add_r_reg[36]_i_1__5_n_6 ;
  wire \add_r_reg[36]_i_1__5_n_7 ;
  wire \add_r_reg[40]_i_1__5_n_0 ;
  wire \add_r_reg[40]_i_1__5_n_1 ;
  wire \add_r_reg[40]_i_1__5_n_2 ;
  wire \add_r_reg[40]_i_1__5_n_3 ;
  wire \add_r_reg[40]_i_1__5_n_4 ;
  wire \add_r_reg[40]_i_1__5_n_5 ;
  wire \add_r_reg[40]_i_1__5_n_6 ;
  wire \add_r_reg[40]_i_1__5_n_7 ;
  wire \add_r_reg[44]_i_1__5_n_1 ;
  wire \add_r_reg[44]_i_1__5_n_2 ;
  wire \add_r_reg[44]_i_1__5_n_3 ;
  wire \add_r_reg[44]_i_1__5_n_4 ;
  wire \add_r_reg[44]_i_1__5_n_5 ;
  wire \add_r_reg[44]_i_1__5_n_6 ;
  wire \add_r_reg[44]_i_1__5_n_7 ;
  wire \add_r_reg[4]_i_1__5_n_0 ;
  wire \add_r_reg[4]_i_1__5_n_1 ;
  wire \add_r_reg[4]_i_1__5_n_2 ;
  wire \add_r_reg[4]_i_1__5_n_3 ;
  wire \add_r_reg[4]_i_1__5_n_4 ;
  wire \add_r_reg[4]_i_1__5_n_5 ;
  wire \add_r_reg[4]_i_1__5_n_6 ;
  wire \add_r_reg[4]_i_1__5_n_7 ;
  wire \add_r_reg[8]_i_1__5_n_0 ;
  wire \add_r_reg[8]_i_1__5_n_1 ;
  wire \add_r_reg[8]_i_1__5_n_2 ;
  wire \add_r_reg[8]_i_1__5_n_3 ;
  wire \add_r_reg[8]_i_1__5_n_4 ;
  wire \add_r_reg[8]_i_1__5_n_5 ;
  wire \add_r_reg[8]_i_1__5_n_6 ;
  wire \add_r_reg[8]_i_1__5_n_7 ;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \counter[0]_i_1__8_n_0 ;
  wire \counter[1]_i_1_n_0 ;
  wire \counter[2]_i_1__7_n_0 ;
  wire \counter[3]_i_1__1_n_0 ;
  wire \counter[4]_i_1__7_n_0 ;
  wire \counter[5]_i_1__6_n_0 ;
  wire \counter[6]_i_1__2_n_0 ;
  wire \counter[7]_i_1__5_n_0 ;
  wire \counter[8]_i_2__6_n_0 ;
  wire \counter[8]_i_3__6_n_0 ;
  wire \counter_reg_n_0_[0] ;
  wire \counter_reg_n_0_[1] ;
  wire \counter_reg_n_0_[2] ;
  wire \counter_reg_n_0_[6] ;
  wire \counter_reg_n_0_[7] ;
  wire \counter_reg_n_0_[8] ;
  wire dp_enable;
  wire [0:0]dp_extend_end_reg;
  wire dp_extend_end_reg_0;
  wire mul_r0_n_100;
  wire mul_r0_n_101;
  wire mul_r0_n_102;
  wire mul_r0_n_103;
  wire mul_r0_n_104;
  wire mul_r0_n_105;
  wire mul_r0_n_106;
  wire mul_r0_n_107;
  wire mul_r0_n_108;
  wire mul_r0_n_109;
  wire mul_r0_n_110;
  wire mul_r0_n_111;
  wire mul_r0_n_112;
  wire mul_r0_n_113;
  wire mul_r0_n_114;
  wire mul_r0_n_115;
  wire mul_r0_n_116;
  wire mul_r0_n_117;
  wire mul_r0_n_118;
  wire mul_r0_n_119;
  wire mul_r0_n_120;
  wire mul_r0_n_121;
  wire mul_r0_n_122;
  wire mul_r0_n_123;
  wire mul_r0_n_124;
  wire mul_r0_n_125;
  wire mul_r0_n_126;
  wire mul_r0_n_127;
  wire mul_r0_n_128;
  wire mul_r0_n_129;
  wire mul_r0_n_130;
  wire mul_r0_n_131;
  wire mul_r0_n_132;
  wire mul_r0_n_133;
  wire mul_r0_n_134;
  wire mul_r0_n_135;
  wire mul_r0_n_136;
  wire mul_r0_n_137;
  wire mul_r0_n_138;
  wire mul_r0_n_139;
  wire mul_r0_n_140;
  wire mul_r0_n_141;
  wire mul_r0_n_142;
  wire mul_r0_n_143;
  wire mul_r0_n_144;
  wire mul_r0_n_145;
  wire mul_r0_n_146;
  wire mul_r0_n_147;
  wire mul_r0_n_148;
  wire mul_r0_n_149;
  wire mul_r0_n_150;
  wire mul_r0_n_151;
  wire mul_r0_n_152;
  wire mul_r0_n_153;
  wire mul_r0_n_58;
  wire mul_r0_n_59;
  wire mul_r0_n_60;
  wire mul_r0_n_61;
  wire mul_r0_n_62;
  wire mul_r0_n_63;
  wire mul_r0_n_64;
  wire mul_r0_n_65;
  wire mul_r0_n_66;
  wire mul_r0_n_67;
  wire mul_r0_n_68;
  wire mul_r0_n_69;
  wire mul_r0_n_70;
  wire mul_r0_n_71;
  wire mul_r0_n_72;
  wire mul_r0_n_73;
  wire mul_r0_n_74;
  wire mul_r0_n_75;
  wire mul_r0_n_76;
  wire mul_r0_n_77;
  wire mul_r0_n_78;
  wire mul_r0_n_79;
  wire mul_r0_n_80;
  wire mul_r0_n_81;
  wire mul_r0_n_82;
  wire mul_r0_n_83;
  wire mul_r0_n_84;
  wire mul_r0_n_85;
  wire mul_r0_n_86;
  wire mul_r0_n_87;
  wire mul_r0_n_88;
  wire mul_r0_n_89;
  wire mul_r0_n_90;
  wire mul_r0_n_91;
  wire mul_r0_n_92;
  wire mul_r0_n_93;
  wire mul_r0_n_94;
  wire mul_r0_n_95;
  wire mul_r0_n_96;
  wire mul_r0_n_97;
  wire mul_r0_n_98;
  wire mul_r0_n_99;
  wire mul_r_reg__0_n_58;
  wire mul_r_reg__0_n_59;
  wire mul_r_reg__0_n_60;
  wire mul_r_reg__0_n_61;
  wire mul_r_reg__0_n_62;
  wire mul_r_reg__0_n_63;
  wire mul_r_reg__0_n_64;
  wire mul_r_reg__0_n_65;
  wire mul_r_reg__0_n_66;
  wire mul_r_reg__0_n_67;
  wire mul_r_reg__0_n_68;
  wire mul_r_reg__0_n_69;
  wire mul_r_reg__0_n_70;
  wire mul_r_reg__0_n_71;
  wire mul_r_reg__0_n_72;
  wire mul_r_reg__0_n_73;
  wire mul_r_reg__0_n_74;
  wire [47:0]mul_r_reg__1;
  wire [0:0]\slv_regs_reg[0][0] ;
  wire [1:0]\slv_regs_reg[0][11] ;
  wire [4:0]\slv_regs_reg[0][8] ;
  wire [3:0]NLW_add_r1_carry_O_UNCONNECTED;
  wire [3:1]NLW_add_r1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_add_r1_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_add_r_reg[44]_i_1__5_CO_UNCONNECTED ;
  wire NLW_mul_r0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r0_CARRYOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r_reg__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r_reg__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_r_reg__0_PCOUT_UNCONNECTED;

  CARRY4 add_r1_carry
       (.CI(1'b0),
        .CO({add_r1_carry_n_0,add_r1_carry_n_1,add_r1_carry_n_2,add_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry_O_UNCONNECTED[3:0]),
        .S({\slv_regs_reg[0][11] [1],add_r1_carry_i_2__5_n_0,\slv_regs_reg[0][11] [0],add_r1_carry_i_4__5_n_0}));
  CARRY4 add_r1_carry__0
       (.CI(add_r1_carry_n_0),
        .CO({NLW_add_r1_carry__0_CO_UNCONNECTED[3:1],CO}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,O}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_2__5
       (.I0(\counter_reg_n_0_[8] ),
        .I1(\slv_regs_reg[0][8] [4]),
        .I2(\counter_reg_n_0_[6] ),
        .I3(\slv_regs_reg[0][8] [2]),
        .I4(\slv_regs_reg[0][8] [3]),
        .I5(\counter_reg_n_0_[7] ),
        .O(add_r1_carry_i_2__5_n_0));
  LUT6 #(
    .INIT(64'h0990000000000990)) 
    add_r1_carry_i_4__5
       (.I0(\counter_reg_n_0_[2] ),
        .I1(\slv_regs_reg[0][8] [1]),
        .I2(\slv_regs_reg[0][0] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\slv_regs_reg[0][8] [0]),
        .I5(\counter_reg_n_0_[1] ),
        .O(add_r1_carry_i_4__5_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_2__5 
       (.I0(mul_r_reg__1[3]),
        .I1(CO),
        .O(\add_r[0]_i_2__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_3__5 
       (.I0(mul_r_reg__1[2]),
        .I1(CO),
        .O(\add_r[0]_i_3__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_4__5 
       (.I0(mul_r_reg__1[1]),
        .I1(CO),
        .O(\add_r[0]_i_4__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_5__5 
       (.I0(mul_r_reg__1[0]),
        .I1(CO),
        .O(\add_r[0]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_6__5 
       (.I0(D[3]),
        .I1(CO),
        .I2(mul_r_reg__1[3]),
        .O(\add_r[0]_i_6__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_7__5 
       (.I0(D[2]),
        .I1(CO),
        .I2(mul_r_reg__1[2]),
        .O(\add_r[0]_i_7__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_8__5 
       (.I0(D[1]),
        .I1(CO),
        .I2(mul_r_reg__1[1]),
        .O(\add_r[0]_i_8__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_9__5 
       (.I0(D[0]),
        .I1(CO),
        .I2(mul_r_reg__1[0]),
        .O(\add_r[0]_i_9__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_2__5 
       (.I0(mul_r_reg__1[15]),
        .I1(CO),
        .O(\add_r[12]_i_2__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_3__5 
       (.I0(mul_r_reg__1[14]),
        .I1(CO),
        .O(\add_r[12]_i_3__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_4__5 
       (.I0(mul_r_reg__1[13]),
        .I1(CO),
        .O(\add_r[12]_i_4__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_5__5 
       (.I0(mul_r_reg__1[12]),
        .I1(CO),
        .O(\add_r[12]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_6__5 
       (.I0(D[15]),
        .I1(CO),
        .I2(mul_r_reg__1[15]),
        .O(\add_r[12]_i_6__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_7__5 
       (.I0(D[14]),
        .I1(CO),
        .I2(mul_r_reg__1[14]),
        .O(\add_r[12]_i_7__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_8__5 
       (.I0(D[13]),
        .I1(CO),
        .I2(mul_r_reg__1[13]),
        .O(\add_r[12]_i_8__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_9__5 
       (.I0(D[12]),
        .I1(CO),
        .I2(mul_r_reg__1[12]),
        .O(\add_r[12]_i_9__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_2__5 
       (.I0(mul_r_reg__1[19]),
        .I1(CO),
        .O(\add_r[16]_i_2__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_3__5 
       (.I0(mul_r_reg__1[18]),
        .I1(CO),
        .O(\add_r[16]_i_3__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_4__5 
       (.I0(mul_r_reg__1[17]),
        .I1(CO),
        .O(\add_r[16]_i_4__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_5__5 
       (.I0(mul_r_reg__1[16]),
        .I1(CO),
        .O(\add_r[16]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_6__5 
       (.I0(D[19]),
        .I1(CO),
        .I2(mul_r_reg__1[19]),
        .O(\add_r[16]_i_6__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_7__5 
       (.I0(D[18]),
        .I1(CO),
        .I2(mul_r_reg__1[18]),
        .O(\add_r[16]_i_7__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_8__5 
       (.I0(D[17]),
        .I1(CO),
        .I2(mul_r_reg__1[17]),
        .O(\add_r[16]_i_8__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_9__5 
       (.I0(D[16]),
        .I1(CO),
        .I2(mul_r_reg__1[16]),
        .O(\add_r[16]_i_9__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_2__5 
       (.I0(mul_r_reg__1[23]),
        .I1(CO),
        .O(\add_r[20]_i_2__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_3__5 
       (.I0(mul_r_reg__1[22]),
        .I1(CO),
        .O(\add_r[20]_i_3__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_4__5 
       (.I0(mul_r_reg__1[21]),
        .I1(CO),
        .O(\add_r[20]_i_4__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_5__5 
       (.I0(mul_r_reg__1[20]),
        .I1(CO),
        .O(\add_r[20]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_6__5 
       (.I0(D[23]),
        .I1(CO),
        .I2(mul_r_reg__1[23]),
        .O(\add_r[20]_i_6__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_7__5 
       (.I0(D[22]),
        .I1(CO),
        .I2(mul_r_reg__1[22]),
        .O(\add_r[20]_i_7__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_8__5 
       (.I0(D[21]),
        .I1(CO),
        .I2(mul_r_reg__1[21]),
        .O(\add_r[20]_i_8__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_9__5 
       (.I0(D[20]),
        .I1(CO),
        .I2(mul_r_reg__1[20]),
        .O(\add_r[20]_i_9__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_2__5 
       (.I0(mul_r_reg__1[27]),
        .I1(CO),
        .O(\add_r[24]_i_2__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_3__5 
       (.I0(mul_r_reg__1[26]),
        .I1(CO),
        .O(\add_r[24]_i_3__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_4__5 
       (.I0(mul_r_reg__1[25]),
        .I1(CO),
        .O(\add_r[24]_i_4__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_5__5 
       (.I0(mul_r_reg__1[24]),
        .I1(CO),
        .O(\add_r[24]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_6__5 
       (.I0(D[27]),
        .I1(CO),
        .I2(mul_r_reg__1[27]),
        .O(\add_r[24]_i_6__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_7__5 
       (.I0(D[26]),
        .I1(CO),
        .I2(mul_r_reg__1[26]),
        .O(\add_r[24]_i_7__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_8__5 
       (.I0(D[25]),
        .I1(CO),
        .I2(mul_r_reg__1[25]),
        .O(\add_r[24]_i_8__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_9__5 
       (.I0(D[24]),
        .I1(CO),
        .I2(mul_r_reg__1[24]),
        .O(\add_r[24]_i_9__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_2__5 
       (.I0(mul_r_reg__1[31]),
        .I1(CO),
        .O(\add_r[28]_i_2__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_3__5 
       (.I0(mul_r_reg__1[30]),
        .I1(CO),
        .O(\add_r[28]_i_3__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_4__5 
       (.I0(mul_r_reg__1[29]),
        .I1(CO),
        .O(\add_r[28]_i_4__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_5__5 
       (.I0(mul_r_reg__1[28]),
        .I1(CO),
        .O(\add_r[28]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_6__5 
       (.I0(D[31]),
        .I1(CO),
        .I2(mul_r_reg__1[31]),
        .O(\add_r[28]_i_6__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_7__5 
       (.I0(D[30]),
        .I1(CO),
        .I2(mul_r_reg__1[30]),
        .O(\add_r[28]_i_7__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_8__5 
       (.I0(D[29]),
        .I1(CO),
        .I2(mul_r_reg__1[29]),
        .O(\add_r[28]_i_8__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_9__5 
       (.I0(D[28]),
        .I1(CO),
        .I2(mul_r_reg__1[28]),
        .O(\add_r[28]_i_9__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_2__5 
       (.I0(mul_r_reg__1[35]),
        .I1(CO),
        .O(\add_r[32]_i_2__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_3__5 
       (.I0(mul_r_reg__1[34]),
        .I1(CO),
        .O(\add_r[32]_i_3__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_4__5 
       (.I0(mul_r_reg__1[33]),
        .I1(CO),
        .O(\add_r[32]_i_4__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_5__5 
       (.I0(mul_r_reg__1[32]),
        .I1(CO),
        .O(\add_r[32]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_6__5 
       (.I0(D[35]),
        .I1(CO),
        .I2(mul_r_reg__1[35]),
        .O(\add_r[32]_i_6__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_7__5 
       (.I0(D[34]),
        .I1(CO),
        .I2(mul_r_reg__1[34]),
        .O(\add_r[32]_i_7__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_8__5 
       (.I0(D[33]),
        .I1(CO),
        .I2(mul_r_reg__1[33]),
        .O(\add_r[32]_i_8__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_9__5 
       (.I0(D[32]),
        .I1(CO),
        .I2(mul_r_reg__1[32]),
        .O(\add_r[32]_i_9__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_2__5 
       (.I0(mul_r_reg__1[39]),
        .I1(CO),
        .O(\add_r[36]_i_2__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_3__5 
       (.I0(mul_r_reg__1[38]),
        .I1(CO),
        .O(\add_r[36]_i_3__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_4__5 
       (.I0(mul_r_reg__1[37]),
        .I1(CO),
        .O(\add_r[36]_i_4__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_5__5 
       (.I0(mul_r_reg__1[36]),
        .I1(CO),
        .O(\add_r[36]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_6__5 
       (.I0(D[39]),
        .I1(CO),
        .I2(mul_r_reg__1[39]),
        .O(\add_r[36]_i_6__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_7__5 
       (.I0(D[38]),
        .I1(CO),
        .I2(mul_r_reg__1[38]),
        .O(\add_r[36]_i_7__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_8__5 
       (.I0(D[37]),
        .I1(CO),
        .I2(mul_r_reg__1[37]),
        .O(\add_r[36]_i_8__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_9__5 
       (.I0(D[36]),
        .I1(CO),
        .I2(mul_r_reg__1[36]),
        .O(\add_r[36]_i_9__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_2__5 
       (.I0(mul_r_reg__1[43]),
        .I1(CO),
        .O(\add_r[40]_i_2__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_3__5 
       (.I0(mul_r_reg__1[42]),
        .I1(CO),
        .O(\add_r[40]_i_3__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_4__5 
       (.I0(mul_r_reg__1[41]),
        .I1(CO),
        .O(\add_r[40]_i_4__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_5__5 
       (.I0(mul_r_reg__1[40]),
        .I1(CO),
        .O(\add_r[40]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_6__5 
       (.I0(D[43]),
        .I1(CO),
        .I2(mul_r_reg__1[43]),
        .O(\add_r[40]_i_6__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_7__5 
       (.I0(D[42]),
        .I1(CO),
        .I2(mul_r_reg__1[42]),
        .O(\add_r[40]_i_7__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_8__5 
       (.I0(D[41]),
        .I1(CO),
        .I2(mul_r_reg__1[41]),
        .O(\add_r[40]_i_8__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_9__5 
       (.I0(D[40]),
        .I1(CO),
        .I2(mul_r_reg__1[40]),
        .O(\add_r[40]_i_9__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_2__5 
       (.I0(mul_r_reg__1[46]),
        .I1(CO),
        .O(\add_r[44]_i_2__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_3__5 
       (.I0(mul_r_reg__1[45]),
        .I1(CO),
        .O(\add_r[44]_i_3__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_4__5 
       (.I0(mul_r_reg__1[44]),
        .I1(CO),
        .O(\add_r[44]_i_4__5_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_5__5 
       (.I0(mul_r_reg__1[47]),
        .I1(CO),
        .I2(D[47]),
        .O(\add_r[44]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_6__5 
       (.I0(D[46]),
        .I1(CO),
        .I2(mul_r_reg__1[46]),
        .O(\add_r[44]_i_6__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_7__5 
       (.I0(D[45]),
        .I1(CO),
        .I2(mul_r_reg__1[45]),
        .O(\add_r[44]_i_7__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_8__5 
       (.I0(D[44]),
        .I1(CO),
        .I2(mul_r_reg__1[44]),
        .O(\add_r[44]_i_8__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_2__5 
       (.I0(mul_r_reg__1[7]),
        .I1(CO),
        .O(\add_r[4]_i_2__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_3__5 
       (.I0(mul_r_reg__1[6]),
        .I1(CO),
        .O(\add_r[4]_i_3__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_4__5 
       (.I0(mul_r_reg__1[5]),
        .I1(CO),
        .O(\add_r[4]_i_4__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_5__5 
       (.I0(mul_r_reg__1[4]),
        .I1(CO),
        .O(\add_r[4]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_6__5 
       (.I0(D[7]),
        .I1(CO),
        .I2(mul_r_reg__1[7]),
        .O(\add_r[4]_i_6__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_7__5 
       (.I0(D[6]),
        .I1(CO),
        .I2(mul_r_reg__1[6]),
        .O(\add_r[4]_i_7__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_8__5 
       (.I0(D[5]),
        .I1(CO),
        .I2(mul_r_reg__1[5]),
        .O(\add_r[4]_i_8__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_9__5 
       (.I0(D[4]),
        .I1(CO),
        .I2(mul_r_reg__1[4]),
        .O(\add_r[4]_i_9__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_2__5 
       (.I0(mul_r_reg__1[11]),
        .I1(CO),
        .O(\add_r[8]_i_2__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_3__5 
       (.I0(mul_r_reg__1[10]),
        .I1(CO),
        .O(\add_r[8]_i_3__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_4__5 
       (.I0(mul_r_reg__1[9]),
        .I1(CO),
        .O(\add_r[8]_i_4__5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_5__5 
       (.I0(mul_r_reg__1[8]),
        .I1(CO),
        .O(\add_r[8]_i_5__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_6__5 
       (.I0(D[11]),
        .I1(CO),
        .I2(mul_r_reg__1[11]),
        .O(\add_r[8]_i_6__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_7__5 
       (.I0(D[10]),
        .I1(CO),
        .I2(mul_r_reg__1[10]),
        .O(\add_r[8]_i_7__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_8__5 
       (.I0(D[9]),
        .I1(CO),
        .I2(mul_r_reg__1[9]),
        .O(\add_r[8]_i_8__5_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_9__5 
       (.I0(D[8]),
        .I1(CO),
        .I2(mul_r_reg__1[8]),
        .O(\add_r[8]_i_9__5_n_0 ));
  FDRE \add_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__5_n_7 ),
        .Q(D[0]),
        .R(aresetn));
  CARRY4 \add_r_reg[0]_i_1__5 
       (.CI(1'b0),
        .CO({\add_r_reg[0]_i_1__5_n_0 ,\add_r_reg[0]_i_1__5_n_1 ,\add_r_reg[0]_i_1__5_n_2 ,\add_r_reg[0]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[0]_i_2__5_n_0 ,\add_r[0]_i_3__5_n_0 ,\add_r[0]_i_4__5_n_0 ,\add_r[0]_i_5__5_n_0 }),
        .O({\add_r_reg[0]_i_1__5_n_4 ,\add_r_reg[0]_i_1__5_n_5 ,\add_r_reg[0]_i_1__5_n_6 ,\add_r_reg[0]_i_1__5_n_7 }),
        .S({\add_r[0]_i_6__5_n_0 ,\add_r[0]_i_7__5_n_0 ,\add_r[0]_i_8__5_n_0 ,\add_r[0]_i_9__5_n_0 }));
  FDRE \add_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__5_n_5 ),
        .Q(D[10]),
        .R(aresetn));
  FDRE \add_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__5_n_4 ),
        .Q(D[11]),
        .R(aresetn));
  FDRE \add_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__5_n_7 ),
        .Q(D[12]),
        .R(aresetn));
  CARRY4 \add_r_reg[12]_i_1__5 
       (.CI(\add_r_reg[8]_i_1__5_n_0 ),
        .CO({\add_r_reg[12]_i_1__5_n_0 ,\add_r_reg[12]_i_1__5_n_1 ,\add_r_reg[12]_i_1__5_n_2 ,\add_r_reg[12]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[12]_i_2__5_n_0 ,\add_r[12]_i_3__5_n_0 ,\add_r[12]_i_4__5_n_0 ,\add_r[12]_i_5__5_n_0 }),
        .O({\add_r_reg[12]_i_1__5_n_4 ,\add_r_reg[12]_i_1__5_n_5 ,\add_r_reg[12]_i_1__5_n_6 ,\add_r_reg[12]_i_1__5_n_7 }),
        .S({\add_r[12]_i_6__5_n_0 ,\add_r[12]_i_7__5_n_0 ,\add_r[12]_i_8__5_n_0 ,\add_r[12]_i_9__5_n_0 }));
  FDRE \add_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__5_n_6 ),
        .Q(D[13]),
        .R(aresetn));
  FDRE \add_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__5_n_5 ),
        .Q(D[14]),
        .R(aresetn));
  FDRE \add_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__5_n_4 ),
        .Q(D[15]),
        .R(aresetn));
  FDRE \add_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__5_n_7 ),
        .Q(D[16]),
        .R(aresetn));
  CARRY4 \add_r_reg[16]_i_1__5 
       (.CI(\add_r_reg[12]_i_1__5_n_0 ),
        .CO({\add_r_reg[16]_i_1__5_n_0 ,\add_r_reg[16]_i_1__5_n_1 ,\add_r_reg[16]_i_1__5_n_2 ,\add_r_reg[16]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[16]_i_2__5_n_0 ,\add_r[16]_i_3__5_n_0 ,\add_r[16]_i_4__5_n_0 ,\add_r[16]_i_5__5_n_0 }),
        .O({\add_r_reg[16]_i_1__5_n_4 ,\add_r_reg[16]_i_1__5_n_5 ,\add_r_reg[16]_i_1__5_n_6 ,\add_r_reg[16]_i_1__5_n_7 }),
        .S({\add_r[16]_i_6__5_n_0 ,\add_r[16]_i_7__5_n_0 ,\add_r[16]_i_8__5_n_0 ,\add_r[16]_i_9__5_n_0 }));
  FDRE \add_r_reg[17] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__5_n_6 ),
        .Q(D[17]),
        .R(aresetn));
  FDRE \add_r_reg[18] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__5_n_5 ),
        .Q(D[18]),
        .R(aresetn));
  FDRE \add_r_reg[19] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__5_n_4 ),
        .Q(D[19]),
        .R(aresetn));
  FDRE \add_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__5_n_6 ),
        .Q(D[1]),
        .R(aresetn));
  FDRE \add_r_reg[20] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__5_n_7 ),
        .Q(D[20]),
        .R(aresetn));
  CARRY4 \add_r_reg[20]_i_1__5 
       (.CI(\add_r_reg[16]_i_1__5_n_0 ),
        .CO({\add_r_reg[20]_i_1__5_n_0 ,\add_r_reg[20]_i_1__5_n_1 ,\add_r_reg[20]_i_1__5_n_2 ,\add_r_reg[20]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[20]_i_2__5_n_0 ,\add_r[20]_i_3__5_n_0 ,\add_r[20]_i_4__5_n_0 ,\add_r[20]_i_5__5_n_0 }),
        .O({\add_r_reg[20]_i_1__5_n_4 ,\add_r_reg[20]_i_1__5_n_5 ,\add_r_reg[20]_i_1__5_n_6 ,\add_r_reg[20]_i_1__5_n_7 }),
        .S({\add_r[20]_i_6__5_n_0 ,\add_r[20]_i_7__5_n_0 ,\add_r[20]_i_8__5_n_0 ,\add_r[20]_i_9__5_n_0 }));
  FDRE \add_r_reg[21] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__5_n_6 ),
        .Q(D[21]),
        .R(aresetn));
  FDRE \add_r_reg[22] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__5_n_5 ),
        .Q(D[22]),
        .R(aresetn));
  FDRE \add_r_reg[23] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__5_n_4 ),
        .Q(D[23]),
        .R(aresetn));
  FDRE \add_r_reg[24] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__5_n_7 ),
        .Q(D[24]),
        .R(aresetn));
  CARRY4 \add_r_reg[24]_i_1__5 
       (.CI(\add_r_reg[20]_i_1__5_n_0 ),
        .CO({\add_r_reg[24]_i_1__5_n_0 ,\add_r_reg[24]_i_1__5_n_1 ,\add_r_reg[24]_i_1__5_n_2 ,\add_r_reg[24]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[24]_i_2__5_n_0 ,\add_r[24]_i_3__5_n_0 ,\add_r[24]_i_4__5_n_0 ,\add_r[24]_i_5__5_n_0 }),
        .O({\add_r_reg[24]_i_1__5_n_4 ,\add_r_reg[24]_i_1__5_n_5 ,\add_r_reg[24]_i_1__5_n_6 ,\add_r_reg[24]_i_1__5_n_7 }),
        .S({\add_r[24]_i_6__5_n_0 ,\add_r[24]_i_7__5_n_0 ,\add_r[24]_i_8__5_n_0 ,\add_r[24]_i_9__5_n_0 }));
  FDRE \add_r_reg[25] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__5_n_6 ),
        .Q(D[25]),
        .R(aresetn));
  FDRE \add_r_reg[26] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__5_n_5 ),
        .Q(D[26]),
        .R(aresetn));
  FDRE \add_r_reg[27] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__5_n_4 ),
        .Q(D[27]),
        .R(aresetn));
  FDRE \add_r_reg[28] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__5_n_7 ),
        .Q(D[28]),
        .R(aresetn));
  CARRY4 \add_r_reg[28]_i_1__5 
       (.CI(\add_r_reg[24]_i_1__5_n_0 ),
        .CO({\add_r_reg[28]_i_1__5_n_0 ,\add_r_reg[28]_i_1__5_n_1 ,\add_r_reg[28]_i_1__5_n_2 ,\add_r_reg[28]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[28]_i_2__5_n_0 ,\add_r[28]_i_3__5_n_0 ,\add_r[28]_i_4__5_n_0 ,\add_r[28]_i_5__5_n_0 }),
        .O({\add_r_reg[28]_i_1__5_n_4 ,\add_r_reg[28]_i_1__5_n_5 ,\add_r_reg[28]_i_1__5_n_6 ,\add_r_reg[28]_i_1__5_n_7 }),
        .S({\add_r[28]_i_6__5_n_0 ,\add_r[28]_i_7__5_n_0 ,\add_r[28]_i_8__5_n_0 ,\add_r[28]_i_9__5_n_0 }));
  FDRE \add_r_reg[29] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__5_n_6 ),
        .Q(D[29]),
        .R(aresetn));
  FDRE \add_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__5_n_5 ),
        .Q(D[2]),
        .R(aresetn));
  FDRE \add_r_reg[30] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__5_n_5 ),
        .Q(D[30]),
        .R(aresetn));
  FDRE \add_r_reg[31] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__5_n_4 ),
        .Q(D[31]),
        .R(aresetn));
  FDRE \add_r_reg[32] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__5_n_7 ),
        .Q(D[32]),
        .R(aresetn));
  CARRY4 \add_r_reg[32]_i_1__5 
       (.CI(\add_r_reg[28]_i_1__5_n_0 ),
        .CO({\add_r_reg[32]_i_1__5_n_0 ,\add_r_reg[32]_i_1__5_n_1 ,\add_r_reg[32]_i_1__5_n_2 ,\add_r_reg[32]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[32]_i_2__5_n_0 ,\add_r[32]_i_3__5_n_0 ,\add_r[32]_i_4__5_n_0 ,\add_r[32]_i_5__5_n_0 }),
        .O({\add_r_reg[32]_i_1__5_n_4 ,\add_r_reg[32]_i_1__5_n_5 ,\add_r_reg[32]_i_1__5_n_6 ,\add_r_reg[32]_i_1__5_n_7 }),
        .S({\add_r[32]_i_6__5_n_0 ,\add_r[32]_i_7__5_n_0 ,\add_r[32]_i_8__5_n_0 ,\add_r[32]_i_9__5_n_0 }));
  FDRE \add_r_reg[33] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__5_n_6 ),
        .Q(D[33]),
        .R(aresetn));
  FDRE \add_r_reg[34] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__5_n_5 ),
        .Q(D[34]),
        .R(aresetn));
  FDRE \add_r_reg[35] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__5_n_4 ),
        .Q(D[35]),
        .R(aresetn));
  FDRE \add_r_reg[36] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__5_n_7 ),
        .Q(D[36]),
        .R(aresetn));
  CARRY4 \add_r_reg[36]_i_1__5 
       (.CI(\add_r_reg[32]_i_1__5_n_0 ),
        .CO({\add_r_reg[36]_i_1__5_n_0 ,\add_r_reg[36]_i_1__5_n_1 ,\add_r_reg[36]_i_1__5_n_2 ,\add_r_reg[36]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[36]_i_2__5_n_0 ,\add_r[36]_i_3__5_n_0 ,\add_r[36]_i_4__5_n_0 ,\add_r[36]_i_5__5_n_0 }),
        .O({\add_r_reg[36]_i_1__5_n_4 ,\add_r_reg[36]_i_1__5_n_5 ,\add_r_reg[36]_i_1__5_n_6 ,\add_r_reg[36]_i_1__5_n_7 }),
        .S({\add_r[36]_i_6__5_n_0 ,\add_r[36]_i_7__5_n_0 ,\add_r[36]_i_8__5_n_0 ,\add_r[36]_i_9__5_n_0 }));
  FDRE \add_r_reg[37] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__5_n_6 ),
        .Q(D[37]),
        .R(aresetn));
  FDRE \add_r_reg[38] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__5_n_5 ),
        .Q(D[38]),
        .R(aresetn));
  FDRE \add_r_reg[39] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__5_n_4 ),
        .Q(D[39]),
        .R(aresetn));
  FDRE \add_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__5_n_4 ),
        .Q(D[3]),
        .R(aresetn));
  FDRE \add_r_reg[40] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__5_n_7 ),
        .Q(D[40]),
        .R(aresetn));
  CARRY4 \add_r_reg[40]_i_1__5 
       (.CI(\add_r_reg[36]_i_1__5_n_0 ),
        .CO({\add_r_reg[40]_i_1__5_n_0 ,\add_r_reg[40]_i_1__5_n_1 ,\add_r_reg[40]_i_1__5_n_2 ,\add_r_reg[40]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[40]_i_2__5_n_0 ,\add_r[40]_i_3__5_n_0 ,\add_r[40]_i_4__5_n_0 ,\add_r[40]_i_5__5_n_0 }),
        .O({\add_r_reg[40]_i_1__5_n_4 ,\add_r_reg[40]_i_1__5_n_5 ,\add_r_reg[40]_i_1__5_n_6 ,\add_r_reg[40]_i_1__5_n_7 }),
        .S({\add_r[40]_i_6__5_n_0 ,\add_r[40]_i_7__5_n_0 ,\add_r[40]_i_8__5_n_0 ,\add_r[40]_i_9__5_n_0 }));
  FDRE \add_r_reg[41] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__5_n_6 ),
        .Q(D[41]),
        .R(aresetn));
  FDRE \add_r_reg[42] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__5_n_5 ),
        .Q(D[42]),
        .R(aresetn));
  FDRE \add_r_reg[43] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__5_n_4 ),
        .Q(D[43]),
        .R(aresetn));
  FDRE \add_r_reg[44] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__5_n_7 ),
        .Q(D[44]),
        .R(aresetn));
  CARRY4 \add_r_reg[44]_i_1__5 
       (.CI(\add_r_reg[40]_i_1__5_n_0 ),
        .CO({\NLW_add_r_reg[44]_i_1__5_CO_UNCONNECTED [3],\add_r_reg[44]_i_1__5_n_1 ,\add_r_reg[44]_i_1__5_n_2 ,\add_r_reg[44]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\add_r[44]_i_2__5_n_0 ,\add_r[44]_i_3__5_n_0 ,\add_r[44]_i_4__5_n_0 }),
        .O({\add_r_reg[44]_i_1__5_n_4 ,\add_r_reg[44]_i_1__5_n_5 ,\add_r_reg[44]_i_1__5_n_6 ,\add_r_reg[44]_i_1__5_n_7 }),
        .S({\add_r[44]_i_5__5_n_0 ,\add_r[44]_i_6__5_n_0 ,\add_r[44]_i_7__5_n_0 ,\add_r[44]_i_8__5_n_0 }));
  FDRE \add_r_reg[45] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__5_n_6 ),
        .Q(D[45]),
        .R(aresetn));
  FDRE \add_r_reg[46] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__5_n_5 ),
        .Q(D[46]),
        .R(aresetn));
  FDRE \add_r_reg[47] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__5_n_4 ),
        .Q(D[47]),
        .R(aresetn));
  FDRE \add_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__5_n_7 ),
        .Q(D[4]),
        .R(aresetn));
  CARRY4 \add_r_reg[4]_i_1__5 
       (.CI(\add_r_reg[0]_i_1__5_n_0 ),
        .CO({\add_r_reg[4]_i_1__5_n_0 ,\add_r_reg[4]_i_1__5_n_1 ,\add_r_reg[4]_i_1__5_n_2 ,\add_r_reg[4]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[4]_i_2__5_n_0 ,\add_r[4]_i_3__5_n_0 ,\add_r[4]_i_4__5_n_0 ,\add_r[4]_i_5__5_n_0 }),
        .O({\add_r_reg[4]_i_1__5_n_4 ,\add_r_reg[4]_i_1__5_n_5 ,\add_r_reg[4]_i_1__5_n_6 ,\add_r_reg[4]_i_1__5_n_7 }),
        .S({\add_r[4]_i_6__5_n_0 ,\add_r[4]_i_7__5_n_0 ,\add_r[4]_i_8__5_n_0 ,\add_r[4]_i_9__5_n_0 }));
  FDRE \add_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__5_n_6 ),
        .Q(D[5]),
        .R(aresetn));
  FDRE \add_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__5_n_5 ),
        .Q(D[6]),
        .R(aresetn));
  FDRE \add_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__5_n_4 ),
        .Q(D[7]),
        .R(aresetn));
  FDRE \add_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__5_n_7 ),
        .Q(D[8]),
        .R(aresetn));
  CARRY4 \add_r_reg[8]_i_1__5 
       (.CI(\add_r_reg[4]_i_1__5_n_0 ),
        .CO({\add_r_reg[8]_i_1__5_n_0 ,\add_r_reg[8]_i_1__5_n_1 ,\add_r_reg[8]_i_1__5_n_2 ,\add_r_reg[8]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[8]_i_2__5_n_0 ,\add_r[8]_i_3__5_n_0 ,\add_r[8]_i_4__5_n_0 ,\add_r[8]_i_5__5_n_0 }),
        .O({\add_r_reg[8]_i_1__5_n_4 ,\add_r_reg[8]_i_1__5_n_5 ,\add_r_reg[8]_i_1__5_n_6 ,\add_r_reg[8]_i_1__5_n_7 }),
        .S({\add_r[8]_i_6__5_n_0 ,\add_r[8]_i_7__5_n_0 ,\add_r[8]_i_8__5_n_0 ,\add_r[8]_i_9__5_n_0 }));
  FDRE \add_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__5_n_6 ),
        .Q(D[9]),
        .R(aresetn));
  LUT1 #(
    .INIT(2'h1)) 
    \counter[0]_i_1__8 
       (.I0(\counter_reg_n_0_[0] ),
        .O(\counter[0]_i_1__8_n_0 ));
  LUT4 #(
    .INIT(16'hFD0E)) 
    \counter[1]_i_1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(CO),
        .I2(dp_extend_end_reg_0),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \counter[2]_i_1__7 
       (.I0(\counter_reg_n_0_[2] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[1] ),
        .O(\counter[2]_i_1__7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \counter[3]_i_1__1 
       (.I0(Q[0]),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[0] ),
        .I3(\counter_reg_n_0_[2] ),
        .O(\counter[3]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \counter[4]_i_1__7 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(\counter_reg_n_0_[1] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[2] ),
        .O(\counter[4]_i_1__7_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \counter[5]_i_1__6 
       (.I0(Q[2]),
        .I1(\counter_reg_n_0_[2] ),
        .I2(\counter_reg_n_0_[0] ),
        .I3(\counter_reg_n_0_[1] ),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\counter[5]_i_1__6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT4 #(
    .INIT(16'hA6AA)) 
    \counter[6]_i_1__2 
       (.I0(\counter_reg_n_0_[6] ),
        .I1(Q[1]),
        .I2(\counter[8]_i_3__6_n_0 ),
        .I3(Q[2]),
        .O(\counter[6]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT5 #(
    .INIT(32'hA6AAAAAA)) 
    \counter[7]_i_1__5 
       (.I0(\counter_reg_n_0_[7] ),
        .I1(Q[2]),
        .I2(\counter[8]_i_3__6_n_0 ),
        .I3(Q[1]),
        .I4(\counter_reg_n_0_[6] ),
        .O(\counter[7]_i_1__5_n_0 ));
  LUT6 #(
    .INIT(64'hAA6AAAAAAAAAAAAA)) 
    \counter[8]_i_2__6 
       (.I0(\counter_reg_n_0_[8] ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(Q[1]),
        .I3(\counter[8]_i_3__6_n_0 ),
        .I4(Q[2]),
        .I5(\counter_reg_n_0_[7] ),
        .O(\counter[8]_i_2__6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \counter[8]_i_3__6 
       (.I0(\counter_reg_n_0_[2] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[1] ),
        .I3(Q[0]),
        .O(\counter[8]_i_3__6_n_0 ));
  FDRE \counter_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[0]_i_1__8_n_0 ),
        .Q(\counter_reg_n_0_[0] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\counter[1]_i_1_n_0 ),
        .Q(\counter_reg_n_0_[1] ),
        .R(aresetn));
  FDRE \counter_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[2]_i_1__7_n_0 ),
        .Q(\counter_reg_n_0_[2] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[3]_i_1__1_n_0 ),
        .Q(Q[0]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[4]_i_1__7_n_0 ),
        .Q(Q[1]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[5]_i_1__6_n_0 ),
        .Q(Q[2]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[6]_i_1__2_n_0 ),
        .Q(\counter_reg_n_0_[6] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[7]_i_1__5_n_0 ),
        .Q(\counter_reg_n_0_[7] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[8]_i_2__6_n_0 ),
        .Q(\counter_reg_n_0_[8] ),
        .R(dp_extend_end_reg));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,b_ram_out[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({A[17],A[17],A[17],A[14:0]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r0_OVERFLOW_UNCONNECTED),
        .P({mul_r0_n_58,mul_r0_n_59,mul_r0_n_60,mul_r0_n_61,mul_r0_n_62,mul_r0_n_63,mul_r0_n_64,mul_r0_n_65,mul_r0_n_66,mul_r0_n_67,mul_r0_n_68,mul_r0_n_69,mul_r0_n_70,mul_r0_n_71,mul_r0_n_72,mul_r0_n_73,mul_r0_n_74,mul_r0_n_75,mul_r0_n_76,mul_r0_n_77,mul_r0_n_78,mul_r0_n_79,mul_r0_n_80,mul_r0_n_81,mul_r0_n_82,mul_r0_n_83,mul_r0_n_84,mul_r0_n_85,mul_r0_n_86,mul_r0_n_87,mul_r0_n_88,mul_r0_n_89,mul_r0_n_90,mul_r0_n_91,mul_r0_n_92,mul_r0_n_93,mul_r0_n_94,mul_r0_n_95,mul_r0_n_96,mul_r0_n_97,mul_r0_n_98,mul_r0_n_99,mul_r0_n_100,mul_r0_n_101,mul_r0_n_102,mul_r0_n_103,mul_r0_n_104,mul_r0_n_105}),
        .PATTERNBDETECT(NLW_mul_r0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_r0_UNDERFLOW_UNCONNECTED));
  FDRE \mul_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_105),
        .Q(mul_r_reg__1[0]),
        .R(aresetn));
  FDRE \mul_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_95),
        .Q(mul_r_reg__1[10]),
        .R(aresetn));
  FDRE \mul_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_94),
        .Q(mul_r_reg__1[11]),
        .R(aresetn));
  FDRE \mul_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_93),
        .Q(mul_r_reg__1[12]),
        .R(aresetn));
  FDRE \mul_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_92),
        .Q(mul_r_reg__1[13]),
        .R(aresetn));
  FDRE \mul_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_91),
        .Q(mul_r_reg__1[14]),
        .R(aresetn));
  FDRE \mul_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_90),
        .Q(mul_r_reg__1[15]),
        .R(aresetn));
  FDRE \mul_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_89),
        .Q(mul_r_reg__1[16]),
        .R(aresetn));
  FDRE \mul_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_104),
        .Q(mul_r_reg__1[1]),
        .R(aresetn));
  FDRE \mul_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_103),
        .Q(mul_r_reg__1[2]),
        .R(aresetn));
  FDRE \mul_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_102),
        .Q(mul_r_reg__1[3]),
        .R(aresetn));
  FDRE \mul_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_101),
        .Q(mul_r_reg__1[4]),
        .R(aresetn));
  FDRE \mul_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_100),
        .Q(mul_r_reg__1[5]),
        .R(aresetn));
  FDRE \mul_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_99),
        .Q(mul_r_reg__1[6]),
        .R(aresetn));
  FDRE \mul_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_98),
        .Q(mul_r_reg__1[7]),
        .R(aresetn));
  FDRE \mul_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_97),
        .Q(mul_r_reg__1[8]),
        .R(aresetn));
  FDRE \mul_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_96),
        .Q(mul_r_reg__1[9]),
        .R(aresetn));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-12 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r_reg__0
       (.A({A[17],A[17],A[17:16],A[16],A[16],A[16],A[16],A[16],A[16],A[16],A[16],A[16:15],A[15:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r_reg__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({b_ram_out[31],b_ram_out[31],b_ram_out[31],b_ram_out[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r_reg__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(dp_enable),
        .CLK(clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED),
        .P({mul_r_reg__0_n_58,mul_r_reg__0_n_59,mul_r_reg__0_n_60,mul_r_reg__0_n_61,mul_r_reg__0_n_62,mul_r_reg__0_n_63,mul_r_reg__0_n_64,mul_r_reg__0_n_65,mul_r_reg__0_n_66,mul_r_reg__0_n_67,mul_r_reg__0_n_68,mul_r_reg__0_n_69,mul_r_reg__0_n_70,mul_r_reg__0_n_71,mul_r_reg__0_n_72,mul_r_reg__0_n_73,mul_r_reg__0_n_74,mul_r_reg__1[47:17]}),
        .PATTERNBDETECT(NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .PCOUT(NLW_mul_r_reg__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(aresetn),
        .UNDERFLOW(NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "dot_product" *) 
module design_1_top_0_0_dot_product_6
   (D,
    \counter_reg[1]_0 ,
    Q,
    A,
    b_ram_out,
    dp_enable,
    clk,
    aresetn,
    \slv_regs_reg[0][11] ,
    O,
    \slv_regs_reg[0][0] ,
    \slv_regs_reg[0][8] ,
    dp_extend_end_reg,
    dp_extend_end_reg_0);
  output [47:0]D;
  output [0:0]\counter_reg[1]_0 ;
  output [2:0]Q;
  input [16:0]A;
  input [31:0]b_ram_out;
  input dp_enable;
  input clk;
  input aresetn;
  input [1:0]\slv_regs_reg[0][11] ;
  input [0:0]O;
  input [0:0]\slv_regs_reg[0][0] ;
  input [4:0]\slv_regs_reg[0][8] ;
  input [0:0]dp_extend_end_reg;
  input dp_extend_end_reg_0;

  wire [16:0]A;
  wire [47:0]D;
  wire [0:0]O;
  wire [2:0]Q;
  wire add_r1_carry_i_2__6_n_0;
  wire add_r1_carry_i_4__6_n_0;
  wire add_r1_carry_n_0;
  wire add_r1_carry_n_1;
  wire add_r1_carry_n_2;
  wire add_r1_carry_n_3;
  wire \add_r[0]_i_2__6_n_0 ;
  wire \add_r[0]_i_3__6_n_0 ;
  wire \add_r[0]_i_4__6_n_0 ;
  wire \add_r[0]_i_5__6_n_0 ;
  wire \add_r[0]_i_6__6_n_0 ;
  wire \add_r[0]_i_7__6_n_0 ;
  wire \add_r[0]_i_8__6_n_0 ;
  wire \add_r[0]_i_9__6_n_0 ;
  wire \add_r[12]_i_2__6_n_0 ;
  wire \add_r[12]_i_3__6_n_0 ;
  wire \add_r[12]_i_4__6_n_0 ;
  wire \add_r[12]_i_5__6_n_0 ;
  wire \add_r[12]_i_6__6_n_0 ;
  wire \add_r[12]_i_7__6_n_0 ;
  wire \add_r[12]_i_8__6_n_0 ;
  wire \add_r[12]_i_9__6_n_0 ;
  wire \add_r[16]_i_2__6_n_0 ;
  wire \add_r[16]_i_3__6_n_0 ;
  wire \add_r[16]_i_4__6_n_0 ;
  wire \add_r[16]_i_5__6_n_0 ;
  wire \add_r[16]_i_6__6_n_0 ;
  wire \add_r[16]_i_7__6_n_0 ;
  wire \add_r[16]_i_8__6_n_0 ;
  wire \add_r[16]_i_9__6_n_0 ;
  wire \add_r[20]_i_2__6_n_0 ;
  wire \add_r[20]_i_3__6_n_0 ;
  wire \add_r[20]_i_4__6_n_0 ;
  wire \add_r[20]_i_5__6_n_0 ;
  wire \add_r[20]_i_6__6_n_0 ;
  wire \add_r[20]_i_7__6_n_0 ;
  wire \add_r[20]_i_8__6_n_0 ;
  wire \add_r[20]_i_9__6_n_0 ;
  wire \add_r[24]_i_2__6_n_0 ;
  wire \add_r[24]_i_3__6_n_0 ;
  wire \add_r[24]_i_4__6_n_0 ;
  wire \add_r[24]_i_5__6_n_0 ;
  wire \add_r[24]_i_6__6_n_0 ;
  wire \add_r[24]_i_7__6_n_0 ;
  wire \add_r[24]_i_8__6_n_0 ;
  wire \add_r[24]_i_9__6_n_0 ;
  wire \add_r[28]_i_2__6_n_0 ;
  wire \add_r[28]_i_3__6_n_0 ;
  wire \add_r[28]_i_4__6_n_0 ;
  wire \add_r[28]_i_5__6_n_0 ;
  wire \add_r[28]_i_6__6_n_0 ;
  wire \add_r[28]_i_7__6_n_0 ;
  wire \add_r[28]_i_8__6_n_0 ;
  wire \add_r[28]_i_9__6_n_0 ;
  wire \add_r[32]_i_2__6_n_0 ;
  wire \add_r[32]_i_3__6_n_0 ;
  wire \add_r[32]_i_4__6_n_0 ;
  wire \add_r[32]_i_5__6_n_0 ;
  wire \add_r[32]_i_6__6_n_0 ;
  wire \add_r[32]_i_7__6_n_0 ;
  wire \add_r[32]_i_8__6_n_0 ;
  wire \add_r[32]_i_9__6_n_0 ;
  wire \add_r[36]_i_2__6_n_0 ;
  wire \add_r[36]_i_3__6_n_0 ;
  wire \add_r[36]_i_4__6_n_0 ;
  wire \add_r[36]_i_5__6_n_0 ;
  wire \add_r[36]_i_6__6_n_0 ;
  wire \add_r[36]_i_7__6_n_0 ;
  wire \add_r[36]_i_8__6_n_0 ;
  wire \add_r[36]_i_9__6_n_0 ;
  wire \add_r[40]_i_2__6_n_0 ;
  wire \add_r[40]_i_3__6_n_0 ;
  wire \add_r[40]_i_4__6_n_0 ;
  wire \add_r[40]_i_5__6_n_0 ;
  wire \add_r[40]_i_6__6_n_0 ;
  wire \add_r[40]_i_7__6_n_0 ;
  wire \add_r[40]_i_8__6_n_0 ;
  wire \add_r[40]_i_9__6_n_0 ;
  wire \add_r[44]_i_2__6_n_0 ;
  wire \add_r[44]_i_3__6_n_0 ;
  wire \add_r[44]_i_4__6_n_0 ;
  wire \add_r[44]_i_5__6_n_0 ;
  wire \add_r[44]_i_6__6_n_0 ;
  wire \add_r[44]_i_7__6_n_0 ;
  wire \add_r[44]_i_8__6_n_0 ;
  wire \add_r[4]_i_2__6_n_0 ;
  wire \add_r[4]_i_3__6_n_0 ;
  wire \add_r[4]_i_4__6_n_0 ;
  wire \add_r[4]_i_5__6_n_0 ;
  wire \add_r[4]_i_6__6_n_0 ;
  wire \add_r[4]_i_7__6_n_0 ;
  wire \add_r[4]_i_8__6_n_0 ;
  wire \add_r[4]_i_9__6_n_0 ;
  wire \add_r[8]_i_2__6_n_0 ;
  wire \add_r[8]_i_3__6_n_0 ;
  wire \add_r[8]_i_4__6_n_0 ;
  wire \add_r[8]_i_5__6_n_0 ;
  wire \add_r[8]_i_6__6_n_0 ;
  wire \add_r[8]_i_7__6_n_0 ;
  wire \add_r[8]_i_8__6_n_0 ;
  wire \add_r[8]_i_9__6_n_0 ;
  wire \add_r_reg[0]_i_1__6_n_0 ;
  wire \add_r_reg[0]_i_1__6_n_1 ;
  wire \add_r_reg[0]_i_1__6_n_2 ;
  wire \add_r_reg[0]_i_1__6_n_3 ;
  wire \add_r_reg[0]_i_1__6_n_4 ;
  wire \add_r_reg[0]_i_1__6_n_5 ;
  wire \add_r_reg[0]_i_1__6_n_6 ;
  wire \add_r_reg[0]_i_1__6_n_7 ;
  wire \add_r_reg[12]_i_1__6_n_0 ;
  wire \add_r_reg[12]_i_1__6_n_1 ;
  wire \add_r_reg[12]_i_1__6_n_2 ;
  wire \add_r_reg[12]_i_1__6_n_3 ;
  wire \add_r_reg[12]_i_1__6_n_4 ;
  wire \add_r_reg[12]_i_1__6_n_5 ;
  wire \add_r_reg[12]_i_1__6_n_6 ;
  wire \add_r_reg[12]_i_1__6_n_7 ;
  wire \add_r_reg[16]_i_1__6_n_0 ;
  wire \add_r_reg[16]_i_1__6_n_1 ;
  wire \add_r_reg[16]_i_1__6_n_2 ;
  wire \add_r_reg[16]_i_1__6_n_3 ;
  wire \add_r_reg[16]_i_1__6_n_4 ;
  wire \add_r_reg[16]_i_1__6_n_5 ;
  wire \add_r_reg[16]_i_1__6_n_6 ;
  wire \add_r_reg[16]_i_1__6_n_7 ;
  wire \add_r_reg[20]_i_1__6_n_0 ;
  wire \add_r_reg[20]_i_1__6_n_1 ;
  wire \add_r_reg[20]_i_1__6_n_2 ;
  wire \add_r_reg[20]_i_1__6_n_3 ;
  wire \add_r_reg[20]_i_1__6_n_4 ;
  wire \add_r_reg[20]_i_1__6_n_5 ;
  wire \add_r_reg[20]_i_1__6_n_6 ;
  wire \add_r_reg[20]_i_1__6_n_7 ;
  wire \add_r_reg[24]_i_1__6_n_0 ;
  wire \add_r_reg[24]_i_1__6_n_1 ;
  wire \add_r_reg[24]_i_1__6_n_2 ;
  wire \add_r_reg[24]_i_1__6_n_3 ;
  wire \add_r_reg[24]_i_1__6_n_4 ;
  wire \add_r_reg[24]_i_1__6_n_5 ;
  wire \add_r_reg[24]_i_1__6_n_6 ;
  wire \add_r_reg[24]_i_1__6_n_7 ;
  wire \add_r_reg[28]_i_1__6_n_0 ;
  wire \add_r_reg[28]_i_1__6_n_1 ;
  wire \add_r_reg[28]_i_1__6_n_2 ;
  wire \add_r_reg[28]_i_1__6_n_3 ;
  wire \add_r_reg[28]_i_1__6_n_4 ;
  wire \add_r_reg[28]_i_1__6_n_5 ;
  wire \add_r_reg[28]_i_1__6_n_6 ;
  wire \add_r_reg[28]_i_1__6_n_7 ;
  wire \add_r_reg[32]_i_1__6_n_0 ;
  wire \add_r_reg[32]_i_1__6_n_1 ;
  wire \add_r_reg[32]_i_1__6_n_2 ;
  wire \add_r_reg[32]_i_1__6_n_3 ;
  wire \add_r_reg[32]_i_1__6_n_4 ;
  wire \add_r_reg[32]_i_1__6_n_5 ;
  wire \add_r_reg[32]_i_1__6_n_6 ;
  wire \add_r_reg[32]_i_1__6_n_7 ;
  wire \add_r_reg[36]_i_1__6_n_0 ;
  wire \add_r_reg[36]_i_1__6_n_1 ;
  wire \add_r_reg[36]_i_1__6_n_2 ;
  wire \add_r_reg[36]_i_1__6_n_3 ;
  wire \add_r_reg[36]_i_1__6_n_4 ;
  wire \add_r_reg[36]_i_1__6_n_5 ;
  wire \add_r_reg[36]_i_1__6_n_6 ;
  wire \add_r_reg[36]_i_1__6_n_7 ;
  wire \add_r_reg[40]_i_1__6_n_0 ;
  wire \add_r_reg[40]_i_1__6_n_1 ;
  wire \add_r_reg[40]_i_1__6_n_2 ;
  wire \add_r_reg[40]_i_1__6_n_3 ;
  wire \add_r_reg[40]_i_1__6_n_4 ;
  wire \add_r_reg[40]_i_1__6_n_5 ;
  wire \add_r_reg[40]_i_1__6_n_6 ;
  wire \add_r_reg[40]_i_1__6_n_7 ;
  wire \add_r_reg[44]_i_1__6_n_1 ;
  wire \add_r_reg[44]_i_1__6_n_2 ;
  wire \add_r_reg[44]_i_1__6_n_3 ;
  wire \add_r_reg[44]_i_1__6_n_4 ;
  wire \add_r_reg[44]_i_1__6_n_5 ;
  wire \add_r_reg[44]_i_1__6_n_6 ;
  wire \add_r_reg[44]_i_1__6_n_7 ;
  wire \add_r_reg[4]_i_1__6_n_0 ;
  wire \add_r_reg[4]_i_1__6_n_1 ;
  wire \add_r_reg[4]_i_1__6_n_2 ;
  wire \add_r_reg[4]_i_1__6_n_3 ;
  wire \add_r_reg[4]_i_1__6_n_4 ;
  wire \add_r_reg[4]_i_1__6_n_5 ;
  wire \add_r_reg[4]_i_1__6_n_6 ;
  wire \add_r_reg[4]_i_1__6_n_7 ;
  wire \add_r_reg[8]_i_1__6_n_0 ;
  wire \add_r_reg[8]_i_1__6_n_1 ;
  wire \add_r_reg[8]_i_1__6_n_2 ;
  wire \add_r_reg[8]_i_1__6_n_3 ;
  wire \add_r_reg[8]_i_1__6_n_4 ;
  wire \add_r_reg[8]_i_1__6_n_5 ;
  wire \add_r_reg[8]_i_1__6_n_6 ;
  wire \add_r_reg[8]_i_1__6_n_7 ;
  wire aresetn;
  wire [31:0]b_ram_out;
  wire clk;
  wire \counter[0]_i_1__9_n_0 ;
  wire \counter[1]_i_1_n_0 ;
  wire \counter[2]_i_1__8_n_0 ;
  wire \counter[3]_i_1__7_n_0 ;
  wire \counter[4]_i_1__4_n_0 ;
  wire \counter[5]_i_1__7_n_0 ;
  wire \counter[6]_i_1__3_n_0 ;
  wire \counter[7]_i_1__6_n_0 ;
  wire \counter[7]_i_2__4_n_0 ;
  wire \counter[8]_i_2__7_n_0 ;
  wire \counter[8]_i_3__7_n_0 ;
  wire [0:0]\counter_reg[1]_0 ;
  wire \counter_reg_n_0_[0] ;
  wire \counter_reg_n_0_[1] ;
  wire \counter_reg_n_0_[2] ;
  wire \counter_reg_n_0_[6] ;
  wire \counter_reg_n_0_[7] ;
  wire \counter_reg_n_0_[8] ;
  wire dp_enable;
  wire [0:0]dp_extend_end_reg;
  wire dp_extend_end_reg_0;
  wire mul_r0_n_100;
  wire mul_r0_n_101;
  wire mul_r0_n_102;
  wire mul_r0_n_103;
  wire mul_r0_n_104;
  wire mul_r0_n_105;
  wire mul_r0_n_106;
  wire mul_r0_n_107;
  wire mul_r0_n_108;
  wire mul_r0_n_109;
  wire mul_r0_n_110;
  wire mul_r0_n_111;
  wire mul_r0_n_112;
  wire mul_r0_n_113;
  wire mul_r0_n_114;
  wire mul_r0_n_115;
  wire mul_r0_n_116;
  wire mul_r0_n_117;
  wire mul_r0_n_118;
  wire mul_r0_n_119;
  wire mul_r0_n_120;
  wire mul_r0_n_121;
  wire mul_r0_n_122;
  wire mul_r0_n_123;
  wire mul_r0_n_124;
  wire mul_r0_n_125;
  wire mul_r0_n_126;
  wire mul_r0_n_127;
  wire mul_r0_n_128;
  wire mul_r0_n_129;
  wire mul_r0_n_130;
  wire mul_r0_n_131;
  wire mul_r0_n_132;
  wire mul_r0_n_133;
  wire mul_r0_n_134;
  wire mul_r0_n_135;
  wire mul_r0_n_136;
  wire mul_r0_n_137;
  wire mul_r0_n_138;
  wire mul_r0_n_139;
  wire mul_r0_n_140;
  wire mul_r0_n_141;
  wire mul_r0_n_142;
  wire mul_r0_n_143;
  wire mul_r0_n_144;
  wire mul_r0_n_145;
  wire mul_r0_n_146;
  wire mul_r0_n_147;
  wire mul_r0_n_148;
  wire mul_r0_n_149;
  wire mul_r0_n_150;
  wire mul_r0_n_151;
  wire mul_r0_n_152;
  wire mul_r0_n_153;
  wire mul_r0_n_58;
  wire mul_r0_n_59;
  wire mul_r0_n_60;
  wire mul_r0_n_61;
  wire mul_r0_n_62;
  wire mul_r0_n_63;
  wire mul_r0_n_64;
  wire mul_r0_n_65;
  wire mul_r0_n_66;
  wire mul_r0_n_67;
  wire mul_r0_n_68;
  wire mul_r0_n_69;
  wire mul_r0_n_70;
  wire mul_r0_n_71;
  wire mul_r0_n_72;
  wire mul_r0_n_73;
  wire mul_r0_n_74;
  wire mul_r0_n_75;
  wire mul_r0_n_76;
  wire mul_r0_n_77;
  wire mul_r0_n_78;
  wire mul_r0_n_79;
  wire mul_r0_n_80;
  wire mul_r0_n_81;
  wire mul_r0_n_82;
  wire mul_r0_n_83;
  wire mul_r0_n_84;
  wire mul_r0_n_85;
  wire mul_r0_n_86;
  wire mul_r0_n_87;
  wire mul_r0_n_88;
  wire mul_r0_n_89;
  wire mul_r0_n_90;
  wire mul_r0_n_91;
  wire mul_r0_n_92;
  wire mul_r0_n_93;
  wire mul_r0_n_94;
  wire mul_r0_n_95;
  wire mul_r0_n_96;
  wire mul_r0_n_97;
  wire mul_r0_n_98;
  wire mul_r0_n_99;
  wire mul_r_reg__0_n_58;
  wire mul_r_reg__0_n_59;
  wire mul_r_reg__0_n_60;
  wire mul_r_reg__0_n_61;
  wire mul_r_reg__0_n_62;
  wire mul_r_reg__0_n_63;
  wire mul_r_reg__0_n_64;
  wire mul_r_reg__0_n_65;
  wire mul_r_reg__0_n_66;
  wire mul_r_reg__0_n_67;
  wire mul_r_reg__0_n_68;
  wire mul_r_reg__0_n_69;
  wire mul_r_reg__0_n_70;
  wire mul_r_reg__0_n_71;
  wire mul_r_reg__0_n_72;
  wire mul_r_reg__0_n_73;
  wire mul_r_reg__0_n_74;
  wire [47:0]mul_r_reg__1;
  wire [0:0]\slv_regs_reg[0][0] ;
  wire [1:0]\slv_regs_reg[0][11] ;
  wire [4:0]\slv_regs_reg[0][8] ;
  wire [3:0]NLW_add_r1_carry_O_UNCONNECTED;
  wire [3:1]NLW_add_r1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_add_r1_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_add_r_reg[44]_i_1__6_CO_UNCONNECTED ;
  wire NLW_mul_r0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r0_CARRYOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_r_reg__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_r_reg__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_r_reg__0_PCOUT_UNCONNECTED;

  CARRY4 add_r1_carry
       (.CI(1'b0),
        .CO({add_r1_carry_n_0,add_r1_carry_n_1,add_r1_carry_n_2,add_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry_O_UNCONNECTED[3:0]),
        .S({\slv_regs_reg[0][11] [1],add_r1_carry_i_2__6_n_0,\slv_regs_reg[0][11] [0],add_r1_carry_i_4__6_n_0}));
  CARRY4 add_r1_carry__0
       (.CI(add_r1_carry_n_0),
        .CO({NLW_add_r1_carry__0_CO_UNCONNECTED[3:1],\counter_reg[1]_0 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_add_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,O}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_2__6
       (.I0(\counter_reg_n_0_[8] ),
        .I1(\slv_regs_reg[0][8] [4]),
        .I2(\counter_reg_n_0_[6] ),
        .I3(\slv_regs_reg[0][8] [2]),
        .I4(\slv_regs_reg[0][8] [3]),
        .I5(\counter_reg_n_0_[7] ),
        .O(add_r1_carry_i_2__6_n_0));
  LUT6 #(
    .INIT(64'h6006000000006006)) 
    add_r1_carry_i_4__6
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\slv_regs_reg[0][0] ),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\slv_regs_reg[0][8] [1]),
        .I4(\counter_reg_n_0_[1] ),
        .I5(\slv_regs_reg[0][8] [0]),
        .O(add_r1_carry_i_4__6_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_2__6 
       (.I0(mul_r_reg__1[3]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[0]_i_2__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_3__6 
       (.I0(mul_r_reg__1[2]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[0]_i_3__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_4__6 
       (.I0(mul_r_reg__1[1]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[0]_i_4__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[0]_i_5__6 
       (.I0(mul_r_reg__1[0]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[0]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_6__6 
       (.I0(D[3]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[3]),
        .O(\add_r[0]_i_6__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_7__6 
       (.I0(D[2]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[2]),
        .O(\add_r[0]_i_7__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_8__6 
       (.I0(D[1]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[1]),
        .O(\add_r[0]_i_8__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[0]_i_9__6 
       (.I0(D[0]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[0]),
        .O(\add_r[0]_i_9__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_2__6 
       (.I0(mul_r_reg__1[15]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[12]_i_2__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_3__6 
       (.I0(mul_r_reg__1[14]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[12]_i_3__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_4__6 
       (.I0(mul_r_reg__1[13]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[12]_i_4__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[12]_i_5__6 
       (.I0(mul_r_reg__1[12]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[12]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_6__6 
       (.I0(D[15]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[15]),
        .O(\add_r[12]_i_6__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_7__6 
       (.I0(D[14]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[14]),
        .O(\add_r[12]_i_7__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_8__6 
       (.I0(D[13]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[13]),
        .O(\add_r[12]_i_8__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[12]_i_9__6 
       (.I0(D[12]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[12]),
        .O(\add_r[12]_i_9__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_2__6 
       (.I0(mul_r_reg__1[19]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[16]_i_2__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_3__6 
       (.I0(mul_r_reg__1[18]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[16]_i_3__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_4__6 
       (.I0(mul_r_reg__1[17]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[16]_i_4__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[16]_i_5__6 
       (.I0(mul_r_reg__1[16]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[16]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_6__6 
       (.I0(D[19]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[19]),
        .O(\add_r[16]_i_6__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_7__6 
       (.I0(D[18]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[18]),
        .O(\add_r[16]_i_7__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_8__6 
       (.I0(D[17]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[17]),
        .O(\add_r[16]_i_8__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[16]_i_9__6 
       (.I0(D[16]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[16]),
        .O(\add_r[16]_i_9__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_2__6 
       (.I0(mul_r_reg__1[23]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[20]_i_2__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_3__6 
       (.I0(mul_r_reg__1[22]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[20]_i_3__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_4__6 
       (.I0(mul_r_reg__1[21]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[20]_i_4__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[20]_i_5__6 
       (.I0(mul_r_reg__1[20]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[20]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_6__6 
       (.I0(D[23]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[23]),
        .O(\add_r[20]_i_6__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_7__6 
       (.I0(D[22]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[22]),
        .O(\add_r[20]_i_7__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_8__6 
       (.I0(D[21]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[21]),
        .O(\add_r[20]_i_8__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[20]_i_9__6 
       (.I0(D[20]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[20]),
        .O(\add_r[20]_i_9__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_2__6 
       (.I0(mul_r_reg__1[27]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[24]_i_2__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_3__6 
       (.I0(mul_r_reg__1[26]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[24]_i_3__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_4__6 
       (.I0(mul_r_reg__1[25]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[24]_i_4__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[24]_i_5__6 
       (.I0(mul_r_reg__1[24]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[24]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_6__6 
       (.I0(D[27]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[27]),
        .O(\add_r[24]_i_6__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_7__6 
       (.I0(D[26]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[26]),
        .O(\add_r[24]_i_7__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_8__6 
       (.I0(D[25]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[25]),
        .O(\add_r[24]_i_8__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[24]_i_9__6 
       (.I0(D[24]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[24]),
        .O(\add_r[24]_i_9__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_2__6 
       (.I0(mul_r_reg__1[31]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[28]_i_2__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_3__6 
       (.I0(mul_r_reg__1[30]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[28]_i_3__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_4__6 
       (.I0(mul_r_reg__1[29]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[28]_i_4__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[28]_i_5__6 
       (.I0(mul_r_reg__1[28]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[28]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_6__6 
       (.I0(D[31]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[31]),
        .O(\add_r[28]_i_6__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_7__6 
       (.I0(D[30]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[30]),
        .O(\add_r[28]_i_7__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_8__6 
       (.I0(D[29]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[29]),
        .O(\add_r[28]_i_8__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[28]_i_9__6 
       (.I0(D[28]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[28]),
        .O(\add_r[28]_i_9__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_2__6 
       (.I0(mul_r_reg__1[35]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[32]_i_2__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_3__6 
       (.I0(mul_r_reg__1[34]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[32]_i_3__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_4__6 
       (.I0(mul_r_reg__1[33]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[32]_i_4__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[32]_i_5__6 
       (.I0(mul_r_reg__1[32]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[32]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_6__6 
       (.I0(D[35]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[35]),
        .O(\add_r[32]_i_6__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_7__6 
       (.I0(D[34]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[34]),
        .O(\add_r[32]_i_7__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_8__6 
       (.I0(D[33]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[33]),
        .O(\add_r[32]_i_8__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[32]_i_9__6 
       (.I0(D[32]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[32]),
        .O(\add_r[32]_i_9__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_2__6 
       (.I0(mul_r_reg__1[39]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[36]_i_2__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_3__6 
       (.I0(mul_r_reg__1[38]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[36]_i_3__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_4__6 
       (.I0(mul_r_reg__1[37]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[36]_i_4__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[36]_i_5__6 
       (.I0(mul_r_reg__1[36]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[36]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_6__6 
       (.I0(D[39]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[39]),
        .O(\add_r[36]_i_6__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_7__6 
       (.I0(D[38]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[38]),
        .O(\add_r[36]_i_7__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_8__6 
       (.I0(D[37]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[37]),
        .O(\add_r[36]_i_8__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[36]_i_9__6 
       (.I0(D[36]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[36]),
        .O(\add_r[36]_i_9__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_2__6 
       (.I0(mul_r_reg__1[43]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[40]_i_2__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_3__6 
       (.I0(mul_r_reg__1[42]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[40]_i_3__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_4__6 
       (.I0(mul_r_reg__1[41]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[40]_i_4__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[40]_i_5__6 
       (.I0(mul_r_reg__1[40]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[40]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_6__6 
       (.I0(D[43]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[43]),
        .O(\add_r[40]_i_6__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_7__6 
       (.I0(D[42]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[42]),
        .O(\add_r[40]_i_7__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_8__6 
       (.I0(D[41]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[41]),
        .O(\add_r[40]_i_8__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[40]_i_9__6 
       (.I0(D[40]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[40]),
        .O(\add_r[40]_i_9__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_2__6 
       (.I0(mul_r_reg__1[46]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[44]_i_2__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_3__6 
       (.I0(mul_r_reg__1[45]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[44]_i_3__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[44]_i_4__6 
       (.I0(mul_r_reg__1[44]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[44]_i_4__6_n_0 ));
  LUT3 #(
    .INIT(8'h9A)) 
    \add_r[44]_i_5__6 
       (.I0(mul_r_reg__1[47]),
        .I1(\counter_reg[1]_0 ),
        .I2(D[47]),
        .O(\add_r[44]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_6__6 
       (.I0(D[46]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[46]),
        .O(\add_r[44]_i_6__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_7__6 
       (.I0(D[45]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[45]),
        .O(\add_r[44]_i_7__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[44]_i_8__6 
       (.I0(D[44]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[44]),
        .O(\add_r[44]_i_8__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_2__6 
       (.I0(mul_r_reg__1[7]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[4]_i_2__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_3__6 
       (.I0(mul_r_reg__1[6]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[4]_i_3__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_4__6 
       (.I0(mul_r_reg__1[5]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[4]_i_4__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[4]_i_5__6 
       (.I0(mul_r_reg__1[4]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[4]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_6__6 
       (.I0(D[7]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[7]),
        .O(\add_r[4]_i_6__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_7__6 
       (.I0(D[6]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[6]),
        .O(\add_r[4]_i_7__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_8__6 
       (.I0(D[5]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[5]),
        .O(\add_r[4]_i_8__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[4]_i_9__6 
       (.I0(D[4]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[4]),
        .O(\add_r[4]_i_9__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_2__6 
       (.I0(mul_r_reg__1[11]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[8]_i_2__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_3__6 
       (.I0(mul_r_reg__1[10]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[8]_i_3__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_4__6 
       (.I0(mul_r_reg__1[9]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[8]_i_4__6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \add_r[8]_i_5__6 
       (.I0(mul_r_reg__1[8]),
        .I1(\counter_reg[1]_0 ),
        .O(\add_r[8]_i_5__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_6__6 
       (.I0(D[11]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[11]),
        .O(\add_r[8]_i_6__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_7__6 
       (.I0(D[10]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[10]),
        .O(\add_r[8]_i_7__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_8__6 
       (.I0(D[9]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[9]),
        .O(\add_r[8]_i_8__6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \add_r[8]_i_9__6 
       (.I0(D[8]),
        .I1(\counter_reg[1]_0 ),
        .I2(mul_r_reg__1[8]),
        .O(\add_r[8]_i_9__6_n_0 ));
  FDRE \add_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__6_n_7 ),
        .Q(D[0]),
        .R(aresetn));
  CARRY4 \add_r_reg[0]_i_1__6 
       (.CI(1'b0),
        .CO({\add_r_reg[0]_i_1__6_n_0 ,\add_r_reg[0]_i_1__6_n_1 ,\add_r_reg[0]_i_1__6_n_2 ,\add_r_reg[0]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[0]_i_2__6_n_0 ,\add_r[0]_i_3__6_n_0 ,\add_r[0]_i_4__6_n_0 ,\add_r[0]_i_5__6_n_0 }),
        .O({\add_r_reg[0]_i_1__6_n_4 ,\add_r_reg[0]_i_1__6_n_5 ,\add_r_reg[0]_i_1__6_n_6 ,\add_r_reg[0]_i_1__6_n_7 }),
        .S({\add_r[0]_i_6__6_n_0 ,\add_r[0]_i_7__6_n_0 ,\add_r[0]_i_8__6_n_0 ,\add_r[0]_i_9__6_n_0 }));
  FDRE \add_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__6_n_5 ),
        .Q(D[10]),
        .R(aresetn));
  FDRE \add_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__6_n_4 ),
        .Q(D[11]),
        .R(aresetn));
  FDRE \add_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__6_n_7 ),
        .Q(D[12]),
        .R(aresetn));
  CARRY4 \add_r_reg[12]_i_1__6 
       (.CI(\add_r_reg[8]_i_1__6_n_0 ),
        .CO({\add_r_reg[12]_i_1__6_n_0 ,\add_r_reg[12]_i_1__6_n_1 ,\add_r_reg[12]_i_1__6_n_2 ,\add_r_reg[12]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[12]_i_2__6_n_0 ,\add_r[12]_i_3__6_n_0 ,\add_r[12]_i_4__6_n_0 ,\add_r[12]_i_5__6_n_0 }),
        .O({\add_r_reg[12]_i_1__6_n_4 ,\add_r_reg[12]_i_1__6_n_5 ,\add_r_reg[12]_i_1__6_n_6 ,\add_r_reg[12]_i_1__6_n_7 }),
        .S({\add_r[12]_i_6__6_n_0 ,\add_r[12]_i_7__6_n_0 ,\add_r[12]_i_8__6_n_0 ,\add_r[12]_i_9__6_n_0 }));
  FDRE \add_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__6_n_6 ),
        .Q(D[13]),
        .R(aresetn));
  FDRE \add_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__6_n_5 ),
        .Q(D[14]),
        .R(aresetn));
  FDRE \add_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[12]_i_1__6_n_4 ),
        .Q(D[15]),
        .R(aresetn));
  FDRE \add_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__6_n_7 ),
        .Q(D[16]),
        .R(aresetn));
  CARRY4 \add_r_reg[16]_i_1__6 
       (.CI(\add_r_reg[12]_i_1__6_n_0 ),
        .CO({\add_r_reg[16]_i_1__6_n_0 ,\add_r_reg[16]_i_1__6_n_1 ,\add_r_reg[16]_i_1__6_n_2 ,\add_r_reg[16]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[16]_i_2__6_n_0 ,\add_r[16]_i_3__6_n_0 ,\add_r[16]_i_4__6_n_0 ,\add_r[16]_i_5__6_n_0 }),
        .O({\add_r_reg[16]_i_1__6_n_4 ,\add_r_reg[16]_i_1__6_n_5 ,\add_r_reg[16]_i_1__6_n_6 ,\add_r_reg[16]_i_1__6_n_7 }),
        .S({\add_r[16]_i_6__6_n_0 ,\add_r[16]_i_7__6_n_0 ,\add_r[16]_i_8__6_n_0 ,\add_r[16]_i_9__6_n_0 }));
  FDRE \add_r_reg[17] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__6_n_6 ),
        .Q(D[17]),
        .R(aresetn));
  FDRE \add_r_reg[18] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__6_n_5 ),
        .Q(D[18]),
        .R(aresetn));
  FDRE \add_r_reg[19] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[16]_i_1__6_n_4 ),
        .Q(D[19]),
        .R(aresetn));
  FDRE \add_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__6_n_6 ),
        .Q(D[1]),
        .R(aresetn));
  FDRE \add_r_reg[20] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__6_n_7 ),
        .Q(D[20]),
        .R(aresetn));
  CARRY4 \add_r_reg[20]_i_1__6 
       (.CI(\add_r_reg[16]_i_1__6_n_0 ),
        .CO({\add_r_reg[20]_i_1__6_n_0 ,\add_r_reg[20]_i_1__6_n_1 ,\add_r_reg[20]_i_1__6_n_2 ,\add_r_reg[20]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[20]_i_2__6_n_0 ,\add_r[20]_i_3__6_n_0 ,\add_r[20]_i_4__6_n_0 ,\add_r[20]_i_5__6_n_0 }),
        .O({\add_r_reg[20]_i_1__6_n_4 ,\add_r_reg[20]_i_1__6_n_5 ,\add_r_reg[20]_i_1__6_n_6 ,\add_r_reg[20]_i_1__6_n_7 }),
        .S({\add_r[20]_i_6__6_n_0 ,\add_r[20]_i_7__6_n_0 ,\add_r[20]_i_8__6_n_0 ,\add_r[20]_i_9__6_n_0 }));
  FDRE \add_r_reg[21] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__6_n_6 ),
        .Q(D[21]),
        .R(aresetn));
  FDRE \add_r_reg[22] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__6_n_5 ),
        .Q(D[22]),
        .R(aresetn));
  FDRE \add_r_reg[23] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[20]_i_1__6_n_4 ),
        .Q(D[23]),
        .R(aresetn));
  FDRE \add_r_reg[24] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__6_n_7 ),
        .Q(D[24]),
        .R(aresetn));
  CARRY4 \add_r_reg[24]_i_1__6 
       (.CI(\add_r_reg[20]_i_1__6_n_0 ),
        .CO({\add_r_reg[24]_i_1__6_n_0 ,\add_r_reg[24]_i_1__6_n_1 ,\add_r_reg[24]_i_1__6_n_2 ,\add_r_reg[24]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[24]_i_2__6_n_0 ,\add_r[24]_i_3__6_n_0 ,\add_r[24]_i_4__6_n_0 ,\add_r[24]_i_5__6_n_0 }),
        .O({\add_r_reg[24]_i_1__6_n_4 ,\add_r_reg[24]_i_1__6_n_5 ,\add_r_reg[24]_i_1__6_n_6 ,\add_r_reg[24]_i_1__6_n_7 }),
        .S({\add_r[24]_i_6__6_n_0 ,\add_r[24]_i_7__6_n_0 ,\add_r[24]_i_8__6_n_0 ,\add_r[24]_i_9__6_n_0 }));
  FDRE \add_r_reg[25] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__6_n_6 ),
        .Q(D[25]),
        .R(aresetn));
  FDRE \add_r_reg[26] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__6_n_5 ),
        .Q(D[26]),
        .R(aresetn));
  FDRE \add_r_reg[27] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[24]_i_1__6_n_4 ),
        .Q(D[27]),
        .R(aresetn));
  FDRE \add_r_reg[28] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__6_n_7 ),
        .Q(D[28]),
        .R(aresetn));
  CARRY4 \add_r_reg[28]_i_1__6 
       (.CI(\add_r_reg[24]_i_1__6_n_0 ),
        .CO({\add_r_reg[28]_i_1__6_n_0 ,\add_r_reg[28]_i_1__6_n_1 ,\add_r_reg[28]_i_1__6_n_2 ,\add_r_reg[28]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[28]_i_2__6_n_0 ,\add_r[28]_i_3__6_n_0 ,\add_r[28]_i_4__6_n_0 ,\add_r[28]_i_5__6_n_0 }),
        .O({\add_r_reg[28]_i_1__6_n_4 ,\add_r_reg[28]_i_1__6_n_5 ,\add_r_reg[28]_i_1__6_n_6 ,\add_r_reg[28]_i_1__6_n_7 }),
        .S({\add_r[28]_i_6__6_n_0 ,\add_r[28]_i_7__6_n_0 ,\add_r[28]_i_8__6_n_0 ,\add_r[28]_i_9__6_n_0 }));
  FDRE \add_r_reg[29] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__6_n_6 ),
        .Q(D[29]),
        .R(aresetn));
  FDRE \add_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__6_n_5 ),
        .Q(D[2]),
        .R(aresetn));
  FDRE \add_r_reg[30] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__6_n_5 ),
        .Q(D[30]),
        .R(aresetn));
  FDRE \add_r_reg[31] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[28]_i_1__6_n_4 ),
        .Q(D[31]),
        .R(aresetn));
  FDRE \add_r_reg[32] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__6_n_7 ),
        .Q(D[32]),
        .R(aresetn));
  CARRY4 \add_r_reg[32]_i_1__6 
       (.CI(\add_r_reg[28]_i_1__6_n_0 ),
        .CO({\add_r_reg[32]_i_1__6_n_0 ,\add_r_reg[32]_i_1__6_n_1 ,\add_r_reg[32]_i_1__6_n_2 ,\add_r_reg[32]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[32]_i_2__6_n_0 ,\add_r[32]_i_3__6_n_0 ,\add_r[32]_i_4__6_n_0 ,\add_r[32]_i_5__6_n_0 }),
        .O({\add_r_reg[32]_i_1__6_n_4 ,\add_r_reg[32]_i_1__6_n_5 ,\add_r_reg[32]_i_1__6_n_6 ,\add_r_reg[32]_i_1__6_n_7 }),
        .S({\add_r[32]_i_6__6_n_0 ,\add_r[32]_i_7__6_n_0 ,\add_r[32]_i_8__6_n_0 ,\add_r[32]_i_9__6_n_0 }));
  FDRE \add_r_reg[33] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__6_n_6 ),
        .Q(D[33]),
        .R(aresetn));
  FDRE \add_r_reg[34] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__6_n_5 ),
        .Q(D[34]),
        .R(aresetn));
  FDRE \add_r_reg[35] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[32]_i_1__6_n_4 ),
        .Q(D[35]),
        .R(aresetn));
  FDRE \add_r_reg[36] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__6_n_7 ),
        .Q(D[36]),
        .R(aresetn));
  CARRY4 \add_r_reg[36]_i_1__6 
       (.CI(\add_r_reg[32]_i_1__6_n_0 ),
        .CO({\add_r_reg[36]_i_1__6_n_0 ,\add_r_reg[36]_i_1__6_n_1 ,\add_r_reg[36]_i_1__6_n_2 ,\add_r_reg[36]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[36]_i_2__6_n_0 ,\add_r[36]_i_3__6_n_0 ,\add_r[36]_i_4__6_n_0 ,\add_r[36]_i_5__6_n_0 }),
        .O({\add_r_reg[36]_i_1__6_n_4 ,\add_r_reg[36]_i_1__6_n_5 ,\add_r_reg[36]_i_1__6_n_6 ,\add_r_reg[36]_i_1__6_n_7 }),
        .S({\add_r[36]_i_6__6_n_0 ,\add_r[36]_i_7__6_n_0 ,\add_r[36]_i_8__6_n_0 ,\add_r[36]_i_9__6_n_0 }));
  FDRE \add_r_reg[37] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__6_n_6 ),
        .Q(D[37]),
        .R(aresetn));
  FDRE \add_r_reg[38] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__6_n_5 ),
        .Q(D[38]),
        .R(aresetn));
  FDRE \add_r_reg[39] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[36]_i_1__6_n_4 ),
        .Q(D[39]),
        .R(aresetn));
  FDRE \add_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[0]_i_1__6_n_4 ),
        .Q(D[3]),
        .R(aresetn));
  FDRE \add_r_reg[40] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__6_n_7 ),
        .Q(D[40]),
        .R(aresetn));
  CARRY4 \add_r_reg[40]_i_1__6 
       (.CI(\add_r_reg[36]_i_1__6_n_0 ),
        .CO({\add_r_reg[40]_i_1__6_n_0 ,\add_r_reg[40]_i_1__6_n_1 ,\add_r_reg[40]_i_1__6_n_2 ,\add_r_reg[40]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[40]_i_2__6_n_0 ,\add_r[40]_i_3__6_n_0 ,\add_r[40]_i_4__6_n_0 ,\add_r[40]_i_5__6_n_0 }),
        .O({\add_r_reg[40]_i_1__6_n_4 ,\add_r_reg[40]_i_1__6_n_5 ,\add_r_reg[40]_i_1__6_n_6 ,\add_r_reg[40]_i_1__6_n_7 }),
        .S({\add_r[40]_i_6__6_n_0 ,\add_r[40]_i_7__6_n_0 ,\add_r[40]_i_8__6_n_0 ,\add_r[40]_i_9__6_n_0 }));
  FDRE \add_r_reg[41] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__6_n_6 ),
        .Q(D[41]),
        .R(aresetn));
  FDRE \add_r_reg[42] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__6_n_5 ),
        .Q(D[42]),
        .R(aresetn));
  FDRE \add_r_reg[43] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[40]_i_1__6_n_4 ),
        .Q(D[43]),
        .R(aresetn));
  FDRE \add_r_reg[44] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__6_n_7 ),
        .Q(D[44]),
        .R(aresetn));
  CARRY4 \add_r_reg[44]_i_1__6 
       (.CI(\add_r_reg[40]_i_1__6_n_0 ),
        .CO({\NLW_add_r_reg[44]_i_1__6_CO_UNCONNECTED [3],\add_r_reg[44]_i_1__6_n_1 ,\add_r_reg[44]_i_1__6_n_2 ,\add_r_reg[44]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\add_r[44]_i_2__6_n_0 ,\add_r[44]_i_3__6_n_0 ,\add_r[44]_i_4__6_n_0 }),
        .O({\add_r_reg[44]_i_1__6_n_4 ,\add_r_reg[44]_i_1__6_n_5 ,\add_r_reg[44]_i_1__6_n_6 ,\add_r_reg[44]_i_1__6_n_7 }),
        .S({\add_r[44]_i_5__6_n_0 ,\add_r[44]_i_6__6_n_0 ,\add_r[44]_i_7__6_n_0 ,\add_r[44]_i_8__6_n_0 }));
  FDRE \add_r_reg[45] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__6_n_6 ),
        .Q(D[45]),
        .R(aresetn));
  FDRE \add_r_reg[46] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__6_n_5 ),
        .Q(D[46]),
        .R(aresetn));
  FDRE \add_r_reg[47] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[44]_i_1__6_n_4 ),
        .Q(D[47]),
        .R(aresetn));
  FDRE \add_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__6_n_7 ),
        .Q(D[4]),
        .R(aresetn));
  CARRY4 \add_r_reg[4]_i_1__6 
       (.CI(\add_r_reg[0]_i_1__6_n_0 ),
        .CO({\add_r_reg[4]_i_1__6_n_0 ,\add_r_reg[4]_i_1__6_n_1 ,\add_r_reg[4]_i_1__6_n_2 ,\add_r_reg[4]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[4]_i_2__6_n_0 ,\add_r[4]_i_3__6_n_0 ,\add_r[4]_i_4__6_n_0 ,\add_r[4]_i_5__6_n_0 }),
        .O({\add_r_reg[4]_i_1__6_n_4 ,\add_r_reg[4]_i_1__6_n_5 ,\add_r_reg[4]_i_1__6_n_6 ,\add_r_reg[4]_i_1__6_n_7 }),
        .S({\add_r[4]_i_6__6_n_0 ,\add_r[4]_i_7__6_n_0 ,\add_r[4]_i_8__6_n_0 ,\add_r[4]_i_9__6_n_0 }));
  FDRE \add_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__6_n_6 ),
        .Q(D[5]),
        .R(aresetn));
  FDRE \add_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__6_n_5 ),
        .Q(D[6]),
        .R(aresetn));
  FDRE \add_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[4]_i_1__6_n_4 ),
        .Q(D[7]),
        .R(aresetn));
  FDRE \add_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__6_n_7 ),
        .Q(D[8]),
        .R(aresetn));
  CARRY4 \add_r_reg[8]_i_1__6 
       (.CI(\add_r_reg[4]_i_1__6_n_0 ),
        .CO({\add_r_reg[8]_i_1__6_n_0 ,\add_r_reg[8]_i_1__6_n_1 ,\add_r_reg[8]_i_1__6_n_2 ,\add_r_reg[8]_i_1__6_n_3 }),
        .CYINIT(1'b0),
        .DI({\add_r[8]_i_2__6_n_0 ,\add_r[8]_i_3__6_n_0 ,\add_r[8]_i_4__6_n_0 ,\add_r[8]_i_5__6_n_0 }),
        .O({\add_r_reg[8]_i_1__6_n_4 ,\add_r_reg[8]_i_1__6_n_5 ,\add_r_reg[8]_i_1__6_n_6 ,\add_r_reg[8]_i_1__6_n_7 }),
        .S({\add_r[8]_i_6__6_n_0 ,\add_r[8]_i_7__6_n_0 ,\add_r[8]_i_8__6_n_0 ,\add_r[8]_i_9__6_n_0 }));
  FDRE \add_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(\add_r_reg[8]_i_1__6_n_6 ),
        .Q(D[9]),
        .R(aresetn));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter[0]_i_1__9 
       (.I0(\counter_reg_n_0_[0] ),
        .O(\counter[0]_i_1__9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT4 #(
    .INIT(16'hFD0E)) 
    \counter[1]_i_1 
       (.I0(\counter_reg_n_0_[0] ),
        .I1(\counter_reg[1]_0 ),
        .I2(dp_extend_end_reg_0),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \counter[2]_i_1__8 
       (.I0(\counter_reg_n_0_[2] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[1] ),
        .O(\counter[2]_i_1__8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \counter[3]_i_1__7 
       (.I0(Q[0]),
        .I1(\counter_reg_n_0_[2] ),
        .I2(\counter_reg_n_0_[0] ),
        .I3(\counter_reg_n_0_[1] ),
        .O(\counter[3]_i_1__7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \counter[4]_i_1__4 
       (.I0(Q[1]),
        .I1(\counter_reg_n_0_[1] ),
        .I2(\counter_reg_n_0_[0] ),
        .I3(\counter_reg_n_0_[2] ),
        .I4(Q[0]),
        .O(\counter[4]_i_1__4_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \counter[5]_i_1__7 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[1] ),
        .I5(Q[1]),
        .O(\counter[5]_i_1__7_n_0 ));
  LUT5 #(
    .INIT(32'hA6AAAAAA)) 
    \counter[6]_i_1__3 
       (.I0(\counter_reg_n_0_[6] ),
        .I1(Q[1]),
        .I2(\counter[7]_i_2__4_n_0 ),
        .I3(Q[0]),
        .I4(Q[2]),
        .O(\counter[6]_i_1__3_n_0 ));
  LUT6 #(
    .INIT(64'hAA6AAAAAAAAAAAAA)) 
    \counter[7]_i_1__6 
       (.I0(\counter_reg_n_0_[7] ),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(\counter[7]_i_2__4_n_0 ),
        .I4(Q[1]),
        .I5(\counter_reg_n_0_[6] ),
        .O(\counter[7]_i_1__6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \counter[7]_i_2__4 
       (.I0(\counter_reg_n_0_[1] ),
        .I1(\counter_reg_n_0_[0] ),
        .I2(\counter_reg_n_0_[2] ),
        .O(\counter[7]_i_2__4_n_0 ));
  LUT4 #(
    .INIT(16'h6AAA)) 
    \counter[8]_i_2__7 
       (.I0(\counter_reg_n_0_[8] ),
        .I1(\counter_reg_n_0_[6] ),
        .I2(\counter[8]_i_3__7_n_0 ),
        .I3(\counter_reg_n_0_[7] ),
        .O(\counter[8]_i_2__7_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter[8]_i_3__7 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(\counter_reg_n_0_[2] ),
        .I3(\counter_reg_n_0_[0] ),
        .I4(\counter_reg_n_0_[1] ),
        .I5(Q[1]),
        .O(\counter[8]_i_3__7_n_0 ));
  FDRE \counter_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[0]_i_1__9_n_0 ),
        .Q(\counter_reg_n_0_[0] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\counter[1]_i_1_n_0 ),
        .Q(\counter_reg_n_0_[1] ),
        .R(aresetn));
  FDRE \counter_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[2]_i_1__8_n_0 ),
        .Q(\counter_reg_n_0_[2] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[3]_i_1__7_n_0 ),
        .Q(Q[0]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[4]_i_1__4_n_0 ),
        .Q(Q[1]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[5]_i_1__7_n_0 ),
        .Q(Q[2]),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[6]_i_1__3_n_0 ),
        .Q(\counter_reg_n_0_[6] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[7]_i_1__6_n_0 ),
        .Q(\counter_reg_n_0_[7] ),
        .R(dp_extend_end_reg));
  FDRE \counter_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(\counter[8]_i_2__7_n_0 ),
        .Q(\counter_reg_n_0_[8] ),
        .R(dp_extend_end_reg));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,b_ram_out[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({A[16],A[16],A[16],A[14:0]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r0_OVERFLOW_UNCONNECTED),
        .P({mul_r0_n_58,mul_r0_n_59,mul_r0_n_60,mul_r0_n_61,mul_r0_n_62,mul_r0_n_63,mul_r0_n_64,mul_r0_n_65,mul_r0_n_66,mul_r0_n_67,mul_r0_n_68,mul_r0_n_69,mul_r0_n_70,mul_r0_n_71,mul_r0_n_72,mul_r0_n_73,mul_r0_n_74,mul_r0_n_75,mul_r0_n_76,mul_r0_n_77,mul_r0_n_78,mul_r0_n_79,mul_r0_n_80,mul_r0_n_81,mul_r0_n_82,mul_r0_n_83,mul_r0_n_84,mul_r0_n_85,mul_r0_n_86,mul_r0_n_87,mul_r0_n_88,mul_r0_n_89,mul_r0_n_90,mul_r0_n_91,mul_r0_n_92,mul_r0_n_93,mul_r0_n_94,mul_r0_n_95,mul_r0_n_96,mul_r0_n_97,mul_r0_n_98,mul_r0_n_99,mul_r0_n_100,mul_r0_n_101,mul_r0_n_102,mul_r0_n_103,mul_r0_n_104,mul_r0_n_105}),
        .PATTERNBDETECT(NLW_mul_r0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_r0_UNDERFLOW_UNCONNECTED));
  FDRE \mul_r_reg[0] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_105),
        .Q(mul_r_reg__1[0]),
        .R(aresetn));
  FDRE \mul_r_reg[10] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_95),
        .Q(mul_r_reg__1[10]),
        .R(aresetn));
  FDRE \mul_r_reg[11] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_94),
        .Q(mul_r_reg__1[11]),
        .R(aresetn));
  FDRE \mul_r_reg[12] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_93),
        .Q(mul_r_reg__1[12]),
        .R(aresetn));
  FDRE \mul_r_reg[13] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_92),
        .Q(mul_r_reg__1[13]),
        .R(aresetn));
  FDRE \mul_r_reg[14] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_91),
        .Q(mul_r_reg__1[14]),
        .R(aresetn));
  FDRE \mul_r_reg[15] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_90),
        .Q(mul_r_reg__1[15]),
        .R(aresetn));
  FDRE \mul_r_reg[16] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_89),
        .Q(mul_r_reg__1[16]),
        .R(aresetn));
  FDRE \mul_r_reg[1] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_104),
        .Q(mul_r_reg__1[1]),
        .R(aresetn));
  FDRE \mul_r_reg[2] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_103),
        .Q(mul_r_reg__1[2]),
        .R(aresetn));
  FDRE \mul_r_reg[3] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_102),
        .Q(mul_r_reg__1[3]),
        .R(aresetn));
  FDRE \mul_r_reg[4] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_101),
        .Q(mul_r_reg__1[4]),
        .R(aresetn));
  FDRE \mul_r_reg[5] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_100),
        .Q(mul_r_reg__1[5]),
        .R(aresetn));
  FDRE \mul_r_reg[6] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_99),
        .Q(mul_r_reg__1[6]),
        .R(aresetn));
  FDRE \mul_r_reg[7] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_98),
        .Q(mul_r_reg__1[7]),
        .R(aresetn));
  FDRE \mul_r_reg[8] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_97),
        .Q(mul_r_reg__1[8]),
        .R(aresetn));
  FDRE \mul_r_reg[9] 
       (.C(clk),
        .CE(dp_enable),
        .D(mul_r0_n_96),
        .Q(mul_r_reg__1[9]),
        .R(aresetn));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-12 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_r_reg__0
       (.A({A[16],A[16],A[16],A[16],A[16:15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_r_reg__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({b_ram_out[31],b_ram_out[31],b_ram_out[31],b_ram_out[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_r_reg__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_r_reg__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_r_reg__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(dp_enable),
        .CLK(clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_r_reg__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_r_reg__0_OVERFLOW_UNCONNECTED),
        .P({mul_r_reg__0_n_58,mul_r_reg__0_n_59,mul_r_reg__0_n_60,mul_r_reg__0_n_61,mul_r_reg__0_n_62,mul_r_reg__0_n_63,mul_r_reg__0_n_64,mul_r_reg__0_n_65,mul_r_reg__0_n_66,mul_r_reg__0_n_67,mul_r_reg__0_n_68,mul_r_reg__0_n_69,mul_r_reg__0_n_70,mul_r_reg__0_n_71,mul_r_reg__0_n_72,mul_r_reg__0_n_73,mul_r_reg__0_n_74,mul_r_reg__1[47:17]}),
        .PATTERNBDETECT(NLW_mul_r_reg__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_r_reg__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_r0_n_106,mul_r0_n_107,mul_r0_n_108,mul_r0_n_109,mul_r0_n_110,mul_r0_n_111,mul_r0_n_112,mul_r0_n_113,mul_r0_n_114,mul_r0_n_115,mul_r0_n_116,mul_r0_n_117,mul_r0_n_118,mul_r0_n_119,mul_r0_n_120,mul_r0_n_121,mul_r0_n_122,mul_r0_n_123,mul_r0_n_124,mul_r0_n_125,mul_r0_n_126,mul_r0_n_127,mul_r0_n_128,mul_r0_n_129,mul_r0_n_130,mul_r0_n_131,mul_r0_n_132,mul_r0_n_133,mul_r0_n_134,mul_r0_n_135,mul_r0_n_136,mul_r0_n_137,mul_r0_n_138,mul_r0_n_139,mul_r0_n_140,mul_r0_n_141,mul_r0_n_142,mul_r0_n_143,mul_r0_n_144,mul_r0_n_145,mul_r0_n_146,mul_r0_n_147,mul_r0_n_148,mul_r0_n_149,mul_r0_n_150,mul_r0_n_151,mul_r0_n_152,mul_r0_n_153}),
        .PCOUT(NLW_mul_r_reg__0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(aresetn),
        .UNDERFLOW(NLW_mul_r_reg__0_UNDERFLOW_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "dot_product_module" *) 
module design_1_top_0_0_dot_product_module
   (D,
    CO,
    \counter_reg[1] ,
    \counter_reg[1]_0 ,
    \counter_reg[1]_1 ,
    \counter_reg[1]_2 ,
    \counter_reg[1]_3 ,
    \counter_reg[1]_4 ,
    \counter_reg[1]_5 ,
    Q,
    \counter_reg[7] ,
    \counter_reg[7]_0 ,
    \counter_reg[8] ,
    \counter_reg[8]_0 ,
    \counter_reg[8]_1 ,
    \counter_reg[8]_2 ,
    \counter_reg[7]_1 ,
    \res_mem_reg[383] ,
    A,
    b_ram_out,
    dp_enable,
    clk,
    aresetn,
    B,
    \in_raw_delay_reg[15]_rep__9 ,
    \in_raw_delay_reg[15]_rep__8 ,
    \in_raw_delay_reg[15]_rep__7 ,
    \in_raw_delay_reg[15]_rep__6 ,
    \in_raw_delay_reg[15]_rep__4 ,
    \in_raw_delay_reg[15]_rep__2 ,
    \in_raw_delay_reg[15]_rep__0 ,
    S,
    O,
    \slv_regs_reg[0][11] ,
    \slv_regs_reg[0][11]_0 ,
    \slv_regs_reg[0][11]_1 ,
    \slv_regs_reg[0][11]_2 ,
    \slv_regs_reg[0][11]_3 ,
    \slv_regs_reg[0][11]_4 ,
    \slv_regs_reg[0][11]_5 ,
    \slv_regs_reg[0][0] ,
    \slv_regs_reg[0][8] ,
    SR,
    dp_extend_end_reg,
    dp_extend_end_reg_0,
    dp_extend_end_reg_1,
    dp_extend_end_reg_2,
    dp_extend_end_reg_3,
    dp_extend_end_reg_4,
    dp_extend_end_reg_5,
    dp_extend_end_reg_6);
  output [383:0]D;
  output [0:0]CO;
  output [0:0]\counter_reg[1] ;
  output [0:0]\counter_reg[1]_0 ;
  output [0:0]\counter_reg[1]_1 ;
  output [0:0]\counter_reg[1]_2 ;
  output [0:0]\counter_reg[1]_3 ;
  output [0:0]\counter_reg[1]_4 ;
  output [0:0]\counter_reg[1]_5 ;
  output [5:0]Q;
  output [2:0]\counter_reg[7] ;
  output [2:0]\counter_reg[7]_0 ;
  output [5:0]\counter_reg[8] ;
  output [5:0]\counter_reg[8]_0 ;
  output [5:0]\counter_reg[8]_1 ;
  output [2:0]\counter_reg[8]_2 ;
  output [2:0]\counter_reg[7]_1 ;
  output \res_mem_reg[383] ;
  input [15:0]A;
  input [255:0]b_ram_out;
  input dp_enable;
  input clk;
  input aresetn;
  input [0:0]B;
  input [0:0]\in_raw_delay_reg[15]_rep__9 ;
  input [0:0]\in_raw_delay_reg[15]_rep__8 ;
  input [0:0]\in_raw_delay_reg[15]_rep__7 ;
  input [1:0]\in_raw_delay_reg[15]_rep__6 ;
  input [1:0]\in_raw_delay_reg[15]_rep__4 ;
  input [1:0]\in_raw_delay_reg[15]_rep__2 ;
  input [0:0]\in_raw_delay_reg[15]_rep__0 ;
  input [2:0]S;
  input [0:0]O;
  input [1:0]\slv_regs_reg[0][11] ;
  input [1:0]\slv_regs_reg[0][11]_0 ;
  input [2:0]\slv_regs_reg[0][11]_1 ;
  input [2:0]\slv_regs_reg[0][11]_2 ;
  input [2:0]\slv_regs_reg[0][11]_3 ;
  input [1:0]\slv_regs_reg[0][11]_4 ;
  input [1:0]\slv_regs_reg[0][11]_5 ;
  input [0:0]\slv_regs_reg[0][0] ;
  input [4:0]\slv_regs_reg[0][8] ;
  input [0:0]SR;
  input dp_extend_end_reg;
  input [0:0]dp_extend_end_reg_0;
  input [0:0]dp_extend_end_reg_1;
  input [0:0]dp_extend_end_reg_2;
  input [0:0]dp_extend_end_reg_3;
  input [0:0]dp_extend_end_reg_4;
  input [0:0]dp_extend_end_reg_5;
  input [0:0]dp_extend_end_reg_6;

  wire [15:0]A;
  wire [0:0]B;
  wire [0:0]CO;
  wire [383:0]D;
  wire [0:0]O;
  wire [5:0]Q;
  wire [2:0]S;
  wire [0:0]SR;
  wire aresetn;
  wire [255:0]b_ram_out;
  wire clk;
  wire [0:0]\counter_reg[1] ;
  wire [0:0]\counter_reg[1]_0 ;
  wire [0:0]\counter_reg[1]_1 ;
  wire [0:0]\counter_reg[1]_2 ;
  wire [0:0]\counter_reg[1]_3 ;
  wire [0:0]\counter_reg[1]_4 ;
  wire [0:0]\counter_reg[1]_5 ;
  wire [2:0]\counter_reg[7] ;
  wire [2:0]\counter_reg[7]_0 ;
  wire [2:0]\counter_reg[7]_1 ;
  wire [5:0]\counter_reg[8] ;
  wire [5:0]\counter_reg[8]_0 ;
  wire [5:0]\counter_reg[8]_1 ;
  wire [2:0]\counter_reg[8]_2 ;
  wire dp_enable;
  wire dp_extend_end_reg;
  wire [0:0]dp_extend_end_reg_0;
  wire [0:0]dp_extend_end_reg_1;
  wire [0:0]dp_extend_end_reg_2;
  wire [0:0]dp_extend_end_reg_3;
  wire [0:0]dp_extend_end_reg_4;
  wire [0:0]dp_extend_end_reg_5;
  wire [0:0]dp_extend_end_reg_6;
  wire [0:0]\in_raw_delay_reg[15]_rep__0 ;
  wire [1:0]\in_raw_delay_reg[15]_rep__2 ;
  wire [1:0]\in_raw_delay_reg[15]_rep__4 ;
  wire [1:0]\in_raw_delay_reg[15]_rep__6 ;
  wire [0:0]\in_raw_delay_reg[15]_rep__7 ;
  wire [0:0]\in_raw_delay_reg[15]_rep__8 ;
  wire [0:0]\in_raw_delay_reg[15]_rep__9 ;
  wire \res_mem_reg[383] ;
  wire [0:0]\slv_regs_reg[0][0] ;
  wire [1:0]\slv_regs_reg[0][11] ;
  wire [1:0]\slv_regs_reg[0][11]_0 ;
  wire [2:0]\slv_regs_reg[0][11]_1 ;
  wire [2:0]\slv_regs_reg[0][11]_2 ;
  wire [2:0]\slv_regs_reg[0][11]_3 ;
  wire [1:0]\slv_regs_reg[0][11]_4 ;
  wire [1:0]\slv_regs_reg[0][11]_5 ;
  wire [4:0]\slv_regs_reg[0][8] ;

  design_1_top_0_0_dot_product \dot_prod[0].dp 
       (.A(A),
        .CO(CO),
        .D(D[47:0]),
        .O(O),
        .Q(Q),
        .S(S),
        .SR(SR),
        .aresetn(aresetn),
        .b_ram_out(b_ram_out[31:0]),
        .clk(clk),
        .dp_enable(dp_enable),
        .dp_extend_end_reg(dp_extend_end_reg),
        .\slv_regs_reg[0][0] (\slv_regs_reg[0][0] ),
        .\slv_regs_reg[0][0]_0 (\slv_regs_reg[0][8] [1:0]));
  design_1_top_0_0_dot_product_0 \dot_prod[1].dp 
       (.A(A),
        .CO(\counter_reg[1] ),
        .D(D[95:48]),
        .O(O),
        .Q(\counter_reg[7] ),
        .aresetn(aresetn),
        .b_ram_out(b_ram_out[63:32]),
        .clk(clk),
        .dp_enable(dp_enable),
        .dp_extend_end_reg(dp_extend_end_reg_0),
        .dp_extend_end_reg_0(dp_extend_end_reg),
        .\slv_regs_reg[0][0] (\slv_regs_reg[0][0] ),
        .\slv_regs_reg[0][11] (\slv_regs_reg[0][11] ),
        .\slv_regs_reg[0][8] (\slv_regs_reg[0][8] ));
  design_1_top_0_0_dot_product_1 \dot_prod[2].dp 
       (.A({B,\in_raw_delay_reg[15]_rep__9 ,A[14:0]}),
        .CO(\counter_reg[1]_0 ),
        .D(D[143:96]),
        .O(O),
        .Q(\counter_reg[7]_0 ),
        .aresetn(aresetn),
        .b_ram_out(b_ram_out[95:64]),
        .clk(clk),
        .dp_enable(dp_enable),
        .dp_extend_end_reg(dp_extend_end_reg_1),
        .dp_extend_end_reg_0(dp_extend_end_reg),
        .\res_mem_reg[383] (\res_mem_reg[383] ),
        .\slv_regs_reg[0][0] (\slv_regs_reg[0][0] ),
        .\slv_regs_reg[0][11] (\slv_regs_reg[0][11]_0 ),
        .\slv_regs_reg[0][11]_0 (\counter_reg[1] ),
        .\slv_regs_reg[0][11]_1 (\counter_reg[1]_5 ),
        .\slv_regs_reg[0][11]_2 (CO),
        .\slv_regs_reg[0][8] (\slv_regs_reg[0][8] ));
  design_1_top_0_0_dot_product_2 \dot_prod[3].dp 
       (.A({\in_raw_delay_reg[15]_rep__8 ,\in_raw_delay_reg[15]_rep__7 ,A[14:0]}),
        .CO(\counter_reg[1]_1 ),
        .D(D[191:144]),
        .O(O),
        .Q(\counter_reg[8] ),
        .aresetn(aresetn),
        .b_ram_out(b_ram_out[127:96]),
        .clk(clk),
        .dp_enable(dp_enable),
        .dp_extend_end_reg(dp_extend_end_reg_2),
        .dp_extend_end_reg_0(dp_extend_end_reg),
        .\slv_regs_reg[0][0] (\slv_regs_reg[0][8] [1:0]),
        .\slv_regs_reg[0][0]_0 (\slv_regs_reg[0][0] ),
        .\slv_regs_reg[0][11] (\slv_regs_reg[0][11]_1 ));
  design_1_top_0_0_dot_product_3 \dot_prod[4].dp 
       (.B({\in_raw_delay_reg[15]_rep__7 ,\in_raw_delay_reg[15]_rep__6 [1],A[14:0]}),
        .CO(\counter_reg[1]_2 ),
        .D(D[239:192]),
        .O(O),
        .Q(\counter_reg[8]_0 ),
        .aresetn(aresetn),
        .b_ram_out(b_ram_out[159:128]),
        .clk(clk),
        .dp_enable(dp_enable),
        .dp_extend_end_reg(dp_extend_end_reg_3),
        .dp_extend_end_reg_0(dp_extend_end_reg),
        .\in_raw_delay_reg[15]_rep__5 (\in_raw_delay_reg[15]_rep__6 [0]),
        .\slv_regs_reg[0][0] (\slv_regs_reg[0][8] [1:0]),
        .\slv_regs_reg[0][0]_0 (\slv_regs_reg[0][0] ),
        .\slv_regs_reg[0][11] (\slv_regs_reg[0][11]_2 ));
  design_1_top_0_0_dot_product_4 \dot_prod[5].dp 
       (.A({\in_raw_delay_reg[15]_rep__6 [0],\in_raw_delay_reg[15]_rep__4 ,A[14:0]}),
        .CO(\counter_reg[1]_3 ),
        .D(D[287:240]),
        .O(O),
        .Q(\counter_reg[8]_1 ),
        .aresetn(aresetn),
        .b_ram_out(b_ram_out[191:160]),
        .clk(clk),
        .dp_enable(dp_enable),
        .dp_extend_end_reg(dp_extend_end_reg_4),
        .dp_extend_end_reg_0(dp_extend_end_reg),
        .\slv_regs_reg[0][0] (\slv_regs_reg[0][8] [1:0]),
        .\slv_regs_reg[0][0]_0 (\slv_regs_reg[0][0] ),
        .\slv_regs_reg[0][11] (\slv_regs_reg[0][11]_3 ));
  design_1_top_0_0_dot_product_5 \dot_prod[6].dp 
       (.A({\in_raw_delay_reg[15]_rep__4 [0],\in_raw_delay_reg[15]_rep__2 ,A[14:0]}),
        .CO(\counter_reg[1]_4 ),
        .D(D[335:288]),
        .O(O),
        .Q(\counter_reg[8]_2 ),
        .aresetn(aresetn),
        .b_ram_out(b_ram_out[223:192]),
        .clk(clk),
        .dp_enable(dp_enable),
        .dp_extend_end_reg(dp_extend_end_reg_5),
        .dp_extend_end_reg_0(dp_extend_end_reg),
        .\slv_regs_reg[0][0] (\slv_regs_reg[0][0] ),
        .\slv_regs_reg[0][11] (\slv_regs_reg[0][11]_4 ),
        .\slv_regs_reg[0][8] (\slv_regs_reg[0][8] ));
  design_1_top_0_0_dot_product_6 \dot_prod[7].dp 
       (.A({\in_raw_delay_reg[15]_rep__2 [0],\in_raw_delay_reg[15]_rep__0 ,A[14:0]}),
        .D(D[383:336]),
        .O(O),
        .Q(\counter_reg[7]_1 ),
        .aresetn(aresetn),
        .b_ram_out(b_ram_out[255:224]),
        .clk(clk),
        .\counter_reg[1]_0 (\counter_reg[1]_5 ),
        .dp_enable(dp_enable),
        .dp_extend_end_reg(dp_extend_end_reg_6),
        .dp_extend_end_reg_0(dp_extend_end_reg),
        .\slv_regs_reg[0][0] (\slv_regs_reg[0][0] ),
        .\slv_regs_reg[0][11] (\slv_regs_reg[0][11]_5 ),
        .\slv_regs_reg[0][8] (\slv_regs_reg[0][8] ));
endmodule

(* ORIG_REF_NAME = "register_interface" *) 
module design_1_top_0_0_register_interface
   (s_axi_ctrl_status_awready,
    s_axi_ctrl_status_wready,
    s_axi_ctrl_status_arready,
    s_axi_ctrl_status_bvalid,
    s_axi_ctrl_status_rvalid,
    S,
    Q,
    DI,
    in_stream_ready_reg,
    b_ram_data_reg,
    SR,
    \counter_reg[2] ,
    \counter_reg[2]_0 ,
    \counter_reg[2]_1 ,
    \counter_reg[2]_2 ,
    \counter_reg[2]_3 ,
    \counter_reg[2]_4 ,
    \counter_reg[2]_5 ,
    E,
    \read_address_reg[0] ,
    \read_address_reg[0]_0 ,
    \counter_reg[1] ,
    \read_address_reg[0]_1 ,
    \read_address_reg[0]_2 ,
    \read_address_reg[0]_3 ,
    \read_address_reg[0]_4 ,
    \read_address_reg[0]_5 ,
    dp_enable,
    p_rdy,
    \counter_reg[1]_0 ,
    \counter_reg[1]_1 ,
    \counter_reg[1]_2 ,
    \counter_reg[1]_3 ,
    \counter_reg[1]_4 ,
    \counter_reg[1]_5 ,
    \counter_reg[1]_6 ,
    \counter_reg[1]_7 ,
    \counter_reg[1]_8 ,
    \counter_reg[1]_9 ,
    \b_ram_sel_reg[0] ,
    b_ram_data_reg_0,
    WEBWE,
    \count_i_reg[6] ,
    \count_i_reg[6]_0 ,
    \count_i_reg[6]_1 ,
    \count_i_reg[6]_2 ,
    \count_i_reg[6]_3 ,
    \count_i_reg[6]_4 ,
    \counter_reg[1]_10 ,
    data_in_w,
    \counter_reg[1]_11 ,
    \counter_reg[1]_12 ,
    initialized_reg,
    \counter_reg[1]_13 ,
    \counter_reg[0] ,
    \state_reg[1] ,
    \state_reg[0] ,
    \slv_regs_reg[0][31]_0 ,
    s_axi_ctrl_status_rdata,
    aresetn_0,
    clk,
    valid_input1_out,
    s_axi_ctrl_status_wvalid,
    s_axi_ctrl_status_awvalid,
    s_axi_ctrl_status_arvalid,
    counter,
    \counter_reg[2]_6 ,
    s_axis_tready,
    initialized,
    aresetn,
    s_axis_tvalid,
    dp_extend_end,
    in_stream_handshake_delay,
    CO,
    \slv_regs_reg[0][11]_0 ,
    \slv_regs_reg[0][11]_1 ,
    \slv_regs_reg[0][11]_2 ,
    \slv_regs_reg[0][11]_3 ,
    \slv_regs_reg[0][11]_4 ,
    \slv_regs_reg[0][11]_5 ,
    \slv_regs_reg[0][11]_6 ,
    \slv_regs_reg[0][11]_7 ,
    \slv_regs_reg[0][11]_8 ,
    \read_address_reg[1] ,
    O,
    \state_reg[0]_0 ,
    \state_reg[1]_0 ,
    \slv_regs_reg[0][11]_9 ,
    \counter_reg[8] ,
    \counter_reg[5] ,
    \counter_reg[5]_0 ,
    \counter_reg[8]_0 ,
    \counter_reg[8]_1 ,
    \counter_reg[8]_2 ,
    \counter_reg[5]_1 ,
    \counter_reg[5]_2 ,
    \slv_regs_reg[0][11]_10 ,
    \b_ram_sel_reg[7] ,
    s_axi_ctrl_status_bready,
    s_axi_ctrl_status_rready,
    s_axi_ctrl_status_awaddr,
    \axi_awaddr_reg[5]_0 ,
    s_axi_ctrl_status_wdata,
    \axi_awaddr_reg[2]_0 ,
    \axi_awaddr_reg[3]_0 ,
    \axi_awaddr_reg[2]_1 ,
    \axi_awaddr_reg[4]_0 ,
    \axi_awaddr_reg[2]_2 ,
    \axi_awaddr_reg[3]_1 ,
    \axi_awaddr_reg[4]_1 ,
    \axi_awaddr_reg[5]_1 ,
    \axi_awaddr_reg[2]_3 ,
    \axi_awaddr_reg[3]_2 ,
    \axi_awaddr_reg[5]_2 ,
    \axi_awaddr_reg[5]_3 ,
    \axi_awaddr_reg[4]_2 ,
    \axi_awaddr_reg[4]_3 ,
    \axi_awaddr_reg[5]_4 ,
    s_axi_ctrl_status_araddr);
  output s_axi_ctrl_status_awready;
  output s_axi_ctrl_status_wready;
  output s_axi_ctrl_status_arready;
  output s_axi_ctrl_status_bvalid;
  output s_axi_ctrl_status_rvalid;
  output [3:0]S;
  output [11:0]Q;
  output [3:0]DI;
  output in_stream_ready_reg;
  output b_ram_data_reg;
  output [0:0]SR;
  output [0:0]\counter_reg[2] ;
  output [0:0]\counter_reg[2]_0 ;
  output [0:0]\counter_reg[2]_1 ;
  output [0:0]\counter_reg[2]_2 ;
  output [0:0]\counter_reg[2]_3 ;
  output [0:0]\counter_reg[2]_4 ;
  output [0:0]\counter_reg[2]_5 ;
  output [0:0]E;
  output [3:0]\read_address_reg[0] ;
  output [3:0]\read_address_reg[0]_0 ;
  output [1:0]\counter_reg[1] ;
  output [2:0]\read_address_reg[0]_1 ;
  output [0:0]\read_address_reg[0]_2 ;
  output [0:0]\read_address_reg[0]_3 ;
  output [0:0]\read_address_reg[0]_4 ;
  output [0:0]\read_address_reg[0]_5 ;
  output dp_enable;
  output p_rdy;
  output \counter_reg[1]_0 ;
  output [2:0]\counter_reg[1]_1 ;
  output [2:0]\counter_reg[1]_2 ;
  output [1:0]\counter_reg[1]_3 ;
  output [1:0]\counter_reg[1]_4 ;
  output [2:0]\counter_reg[1]_5 ;
  output [2:0]\counter_reg[1]_6 ;
  output [2:0]\counter_reg[1]_7 ;
  output [1:0]\counter_reg[1]_8 ;
  output [1:0]\counter_reg[1]_9 ;
  output [0:0]\b_ram_sel_reg[0] ;
  output [0:0]b_ram_data_reg_0;
  output [0:0]WEBWE;
  output [0:0]\count_i_reg[6] ;
  output [0:0]\count_i_reg[6]_0 ;
  output [0:0]\count_i_reg[6]_1 ;
  output [0:0]\count_i_reg[6]_2 ;
  output [0:0]\count_i_reg[6]_3 ;
  output [0:0]\count_i_reg[6]_4 ;
  output \counter_reg[1]_10 ;
  output [31:0]data_in_w;
  output [1:0]\counter_reg[1]_11 ;
  output [0:0]\counter_reg[1]_12 ;
  output initialized_reg;
  output \counter_reg[1]_13 ;
  output \counter_reg[0] ;
  output \state_reg[1] ;
  output \state_reg[0] ;
  output [3:0]\slv_regs_reg[0][31]_0 ;
  output [31:0]s_axi_ctrl_status_rdata;
  input aresetn_0;
  input clk;
  input valid_input1_out;
  input s_axi_ctrl_status_wvalid;
  input s_axi_ctrl_status_awvalid;
  input s_axi_ctrl_status_arvalid;
  input [6:0]counter;
  input \counter_reg[2]_6 ;
  input s_axis_tready;
  input initialized;
  input aresetn;
  input s_axis_tvalid;
  input dp_extend_end;
  input in_stream_handshake_delay;
  input [0:0]CO;
  input [0:0]\slv_regs_reg[0][11]_0 ;
  input [0:0]\slv_regs_reg[0][11]_1 ;
  input [0:0]\slv_regs_reg[0][11]_2 ;
  input [0:0]\slv_regs_reg[0][11]_3 ;
  input [0:0]\slv_regs_reg[0][11]_4 ;
  input [0:0]\slv_regs_reg[0][11]_5 ;
  input [0:0]\slv_regs_reg[0][11]_6 ;
  input \slv_regs_reg[0][11]_7 ;
  input [0:0]\slv_regs_reg[0][11]_8 ;
  input [1:0]\read_address_reg[1] ;
  input [0:0]O;
  input \state_reg[0]_0 ;
  input \state_reg[1]_0 ;
  input [0:0]\slv_regs_reg[0][11]_9 ;
  input [5:0]\counter_reg[8] ;
  input [2:0]\counter_reg[5] ;
  input [2:0]\counter_reg[5]_0 ;
  input [5:0]\counter_reg[8]_0 ;
  input [5:0]\counter_reg[8]_1 ;
  input [5:0]\counter_reg[8]_2 ;
  input [2:0]\counter_reg[5]_1 ;
  input [2:0]\counter_reg[5]_2 ;
  input [0:0]\slv_regs_reg[0][11]_10 ;
  input [7:0]\b_ram_sel_reg[7] ;
  input s_axi_ctrl_status_bready;
  input s_axi_ctrl_status_rready;
  input [3:0]s_axi_ctrl_status_awaddr;
  input [3:0]\axi_awaddr_reg[5]_0 ;
  input [31:0]s_axi_ctrl_status_wdata;
  input [3:0]\axi_awaddr_reg[2]_0 ;
  input [3:0]\axi_awaddr_reg[3]_0 ;
  input [3:0]\axi_awaddr_reg[2]_1 ;
  input [3:0]\axi_awaddr_reg[4]_0 ;
  input [3:0]\axi_awaddr_reg[2]_2 ;
  input [3:0]\axi_awaddr_reg[3]_1 ;
  input [3:0]\axi_awaddr_reg[4]_1 ;
  input [3:0]\axi_awaddr_reg[5]_1 ;
  input [3:0]\axi_awaddr_reg[2]_3 ;
  input [3:0]\axi_awaddr_reg[3]_2 ;
  input [3:0]\axi_awaddr_reg[5]_2 ;
  input [3:0]\axi_awaddr_reg[5]_3 ;
  input [3:0]\axi_awaddr_reg[4]_2 ;
  input [3:0]\axi_awaddr_reg[4]_3 ;
  input [3:0]\axi_awaddr_reg[5]_4 ;
  input [3:0]s_axi_ctrl_status_araddr;

  wire [0:0]CO;
  wire [3:0]DI;
  wire [0:0]E;
  wire [0:0]O;
  wire [11:0]Q;
  wire [3:0]S;
  wire [0:0]SR;
  wire [0:0]WEBWE;
  wire add_r1_carry__0_i_1_n_2;
  wire add_r1_carry__0_i_1_n_3;
  wire add_r1_carry_i_5_n_0;
  wire add_r1_carry_i_5_n_1;
  wire add_r1_carry_i_5_n_2;
  wire add_r1_carry_i_5_n_3;
  wire add_r1_carry_i_6_n_0;
  wire add_r1_carry_i_6_n_1;
  wire add_r1_carry_i_6_n_2;
  wire add_r1_carry_i_6_n_3;
  wire [12:3]add_r2;
  wire aresetn;
  wire aresetn_0;
  wire axi_arready_i_1_n_0;
  wire [3:0]\axi_awaddr_reg[2]_0 ;
  wire [3:0]\axi_awaddr_reg[2]_1 ;
  wire [3:0]\axi_awaddr_reg[2]_2 ;
  wire [3:0]\axi_awaddr_reg[2]_3 ;
  wire [3:0]\axi_awaddr_reg[3]_0 ;
  wire [3:0]\axi_awaddr_reg[3]_1 ;
  wire [3:0]\axi_awaddr_reg[3]_2 ;
  wire [3:0]\axi_awaddr_reg[4]_0 ;
  wire [3:0]\axi_awaddr_reg[4]_1 ;
  wire [3:0]\axi_awaddr_reg[4]_2 ;
  wire [3:0]\axi_awaddr_reg[4]_3 ;
  wire [3:0]\axi_awaddr_reg[5]_0 ;
  wire [3:0]\axi_awaddr_reg[5]_1 ;
  wire [3:0]\axi_awaddr_reg[5]_2 ;
  wire [3:0]\axi_awaddr_reg[5]_3 ;
  wire [3:0]\axi_awaddr_reg[5]_4 ;
  wire axi_awready_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[0]_i_4_n_0 ;
  wire \axi_rdata[0]_i_5_n_0 ;
  wire \axi_rdata[0]_i_6_n_0 ;
  wire \axi_rdata[0]_i_7_n_0 ;
  wire \axi_rdata[10]_i_4_n_0 ;
  wire \axi_rdata[10]_i_5_n_0 ;
  wire \axi_rdata[10]_i_6_n_0 ;
  wire \axi_rdata[10]_i_7_n_0 ;
  wire \axi_rdata[11]_i_4_n_0 ;
  wire \axi_rdata[11]_i_5_n_0 ;
  wire \axi_rdata[11]_i_6_n_0 ;
  wire \axi_rdata[11]_i_7_n_0 ;
  wire \axi_rdata[12]_i_4_n_0 ;
  wire \axi_rdata[12]_i_5_n_0 ;
  wire \axi_rdata[12]_i_6_n_0 ;
  wire \axi_rdata[12]_i_7_n_0 ;
  wire \axi_rdata[13]_i_4_n_0 ;
  wire \axi_rdata[13]_i_5_n_0 ;
  wire \axi_rdata[13]_i_6_n_0 ;
  wire \axi_rdata[13]_i_7_n_0 ;
  wire \axi_rdata[14]_i_4_n_0 ;
  wire \axi_rdata[14]_i_5_n_0 ;
  wire \axi_rdata[14]_i_6_n_0 ;
  wire \axi_rdata[14]_i_7_n_0 ;
  wire \axi_rdata[15]_i_4_n_0 ;
  wire \axi_rdata[15]_i_5_n_0 ;
  wire \axi_rdata[15]_i_6_n_0 ;
  wire \axi_rdata[15]_i_7_n_0 ;
  wire \axi_rdata[16]_i_4_n_0 ;
  wire \axi_rdata[16]_i_5_n_0 ;
  wire \axi_rdata[16]_i_6_n_0 ;
  wire \axi_rdata[16]_i_7_n_0 ;
  wire \axi_rdata[17]_i_4_n_0 ;
  wire \axi_rdata[17]_i_5_n_0 ;
  wire \axi_rdata[17]_i_6_n_0 ;
  wire \axi_rdata[17]_i_7_n_0 ;
  wire \axi_rdata[18]_i_4_n_0 ;
  wire \axi_rdata[18]_i_5_n_0 ;
  wire \axi_rdata[18]_i_6_n_0 ;
  wire \axi_rdata[18]_i_7_n_0 ;
  wire \axi_rdata[19]_i_4_n_0 ;
  wire \axi_rdata[19]_i_5_n_0 ;
  wire \axi_rdata[19]_i_6_n_0 ;
  wire \axi_rdata[19]_i_7_n_0 ;
  wire \axi_rdata[1]_i_4_n_0 ;
  wire \axi_rdata[1]_i_5_n_0 ;
  wire \axi_rdata[1]_i_6_n_0 ;
  wire \axi_rdata[1]_i_7_n_0 ;
  wire \axi_rdata[20]_i_4_n_0 ;
  wire \axi_rdata[20]_i_5_n_0 ;
  wire \axi_rdata[20]_i_6_n_0 ;
  wire \axi_rdata[20]_i_7_n_0 ;
  wire \axi_rdata[21]_i_4_n_0 ;
  wire \axi_rdata[21]_i_5_n_0 ;
  wire \axi_rdata[21]_i_6_n_0 ;
  wire \axi_rdata[21]_i_7_n_0 ;
  wire \axi_rdata[22]_i_4_n_0 ;
  wire \axi_rdata[22]_i_5_n_0 ;
  wire \axi_rdata[22]_i_6_n_0 ;
  wire \axi_rdata[22]_i_7_n_0 ;
  wire \axi_rdata[23]_i_4_n_0 ;
  wire \axi_rdata[23]_i_5_n_0 ;
  wire \axi_rdata[23]_i_6_n_0 ;
  wire \axi_rdata[23]_i_7_n_0 ;
  wire \axi_rdata[24]_i_4_n_0 ;
  wire \axi_rdata[24]_i_5_n_0 ;
  wire \axi_rdata[24]_i_6_n_0 ;
  wire \axi_rdata[24]_i_7_n_0 ;
  wire \axi_rdata[25]_i_4_n_0 ;
  wire \axi_rdata[25]_i_5_n_0 ;
  wire \axi_rdata[25]_i_6_n_0 ;
  wire \axi_rdata[25]_i_7_n_0 ;
  wire \axi_rdata[26]_i_4_n_0 ;
  wire \axi_rdata[26]_i_5_n_0 ;
  wire \axi_rdata[26]_i_6_n_0 ;
  wire \axi_rdata[26]_i_7_n_0 ;
  wire \axi_rdata[27]_i_4_n_0 ;
  wire \axi_rdata[27]_i_5_n_0 ;
  wire \axi_rdata[27]_i_6_n_0 ;
  wire \axi_rdata[27]_i_7_n_0 ;
  wire \axi_rdata[28]_i_4_n_0 ;
  wire \axi_rdata[28]_i_5_n_0 ;
  wire \axi_rdata[28]_i_6_n_0 ;
  wire \axi_rdata[28]_i_7_n_0 ;
  wire \axi_rdata[29]_i_4_n_0 ;
  wire \axi_rdata[29]_i_5_n_0 ;
  wire \axi_rdata[29]_i_6_n_0 ;
  wire \axi_rdata[29]_i_7_n_0 ;
  wire \axi_rdata[2]_i_4_n_0 ;
  wire \axi_rdata[2]_i_5_n_0 ;
  wire \axi_rdata[2]_i_6_n_0 ;
  wire \axi_rdata[2]_i_7_n_0 ;
  wire \axi_rdata[30]_i_4_n_0 ;
  wire \axi_rdata[30]_i_5_n_0 ;
  wire \axi_rdata[30]_i_6_n_0 ;
  wire \axi_rdata[30]_i_7_n_0 ;
  wire \axi_rdata[31]_i_1_n_0 ;
  wire \axi_rdata[31]_i_5_n_0 ;
  wire \axi_rdata[31]_i_6_n_0 ;
  wire \axi_rdata[31]_i_7_n_0 ;
  wire \axi_rdata[31]_i_8_n_0 ;
  wire \axi_rdata[3]_i_4_n_0 ;
  wire \axi_rdata[3]_i_5_n_0 ;
  wire \axi_rdata[3]_i_6_n_0 ;
  wire \axi_rdata[3]_i_7_n_0 ;
  wire \axi_rdata[4]_i_4_n_0 ;
  wire \axi_rdata[4]_i_5_n_0 ;
  wire \axi_rdata[4]_i_6_n_0 ;
  wire \axi_rdata[4]_i_7_n_0 ;
  wire \axi_rdata[5]_i_4_n_0 ;
  wire \axi_rdata[5]_i_5_n_0 ;
  wire \axi_rdata[5]_i_6_n_0 ;
  wire \axi_rdata[5]_i_7_n_0 ;
  wire \axi_rdata[6]_i_4_n_0 ;
  wire \axi_rdata[6]_i_5_n_0 ;
  wire \axi_rdata[6]_i_6_n_0 ;
  wire \axi_rdata[6]_i_7_n_0 ;
  wire \axi_rdata[7]_i_4_n_0 ;
  wire \axi_rdata[7]_i_5_n_0 ;
  wire \axi_rdata[7]_i_6_n_0 ;
  wire \axi_rdata[7]_i_7_n_0 ;
  wire \axi_rdata[8]_i_4_n_0 ;
  wire \axi_rdata[8]_i_5_n_0 ;
  wire \axi_rdata[8]_i_6_n_0 ;
  wire \axi_rdata[8]_i_7_n_0 ;
  wire \axi_rdata[9]_i_4_n_0 ;
  wire \axi_rdata[9]_i_5_n_0 ;
  wire \axi_rdata[9]_i_6_n_0 ;
  wire \axi_rdata[9]_i_7_n_0 ;
  wire \axi_rdata_reg[0]_i_2_n_0 ;
  wire \axi_rdata_reg[0]_i_3_n_0 ;
  wire \axi_rdata_reg[10]_i_2_n_0 ;
  wire \axi_rdata_reg[10]_i_3_n_0 ;
  wire \axi_rdata_reg[11]_i_2_n_0 ;
  wire \axi_rdata_reg[11]_i_3_n_0 ;
  wire \axi_rdata_reg[12]_i_2_n_0 ;
  wire \axi_rdata_reg[12]_i_3_n_0 ;
  wire \axi_rdata_reg[13]_i_2_n_0 ;
  wire \axi_rdata_reg[13]_i_3_n_0 ;
  wire \axi_rdata_reg[14]_i_2_n_0 ;
  wire \axi_rdata_reg[14]_i_3_n_0 ;
  wire \axi_rdata_reg[15]_i_2_n_0 ;
  wire \axi_rdata_reg[15]_i_3_n_0 ;
  wire \axi_rdata_reg[16]_i_2_n_0 ;
  wire \axi_rdata_reg[16]_i_3_n_0 ;
  wire \axi_rdata_reg[17]_i_2_n_0 ;
  wire \axi_rdata_reg[17]_i_3_n_0 ;
  wire \axi_rdata_reg[18]_i_2_n_0 ;
  wire \axi_rdata_reg[18]_i_3_n_0 ;
  wire \axi_rdata_reg[19]_i_2_n_0 ;
  wire \axi_rdata_reg[19]_i_3_n_0 ;
  wire \axi_rdata_reg[1]_i_2_n_0 ;
  wire \axi_rdata_reg[1]_i_3_n_0 ;
  wire \axi_rdata_reg[20]_i_2_n_0 ;
  wire \axi_rdata_reg[20]_i_3_n_0 ;
  wire \axi_rdata_reg[21]_i_2_n_0 ;
  wire \axi_rdata_reg[21]_i_3_n_0 ;
  wire \axi_rdata_reg[22]_i_2_n_0 ;
  wire \axi_rdata_reg[22]_i_3_n_0 ;
  wire \axi_rdata_reg[23]_i_2_n_0 ;
  wire \axi_rdata_reg[23]_i_3_n_0 ;
  wire \axi_rdata_reg[24]_i_2_n_0 ;
  wire \axi_rdata_reg[24]_i_3_n_0 ;
  wire \axi_rdata_reg[25]_i_2_n_0 ;
  wire \axi_rdata_reg[25]_i_3_n_0 ;
  wire \axi_rdata_reg[26]_i_2_n_0 ;
  wire \axi_rdata_reg[26]_i_3_n_0 ;
  wire \axi_rdata_reg[27]_i_2_n_0 ;
  wire \axi_rdata_reg[27]_i_3_n_0 ;
  wire \axi_rdata_reg[28]_i_2_n_0 ;
  wire \axi_rdata_reg[28]_i_3_n_0 ;
  wire \axi_rdata_reg[29]_i_2_n_0 ;
  wire \axi_rdata_reg[29]_i_3_n_0 ;
  wire \axi_rdata_reg[2]_i_2_n_0 ;
  wire \axi_rdata_reg[2]_i_3_n_0 ;
  wire \axi_rdata_reg[30]_i_2_n_0 ;
  wire \axi_rdata_reg[30]_i_3_n_0 ;
  wire \axi_rdata_reg[31]_i_3_n_0 ;
  wire \axi_rdata_reg[31]_i_4_n_0 ;
  wire \axi_rdata_reg[3]_i_2_n_0 ;
  wire \axi_rdata_reg[3]_i_3_n_0 ;
  wire \axi_rdata_reg[4]_i_2_n_0 ;
  wire \axi_rdata_reg[4]_i_3_n_0 ;
  wire \axi_rdata_reg[5]_i_2_n_0 ;
  wire \axi_rdata_reg[5]_i_3_n_0 ;
  wire \axi_rdata_reg[6]_i_2_n_0 ;
  wire \axi_rdata_reg[6]_i_3_n_0 ;
  wire \axi_rdata_reg[7]_i_2_n_0 ;
  wire \axi_rdata_reg[7]_i_3_n_0 ;
  wire \axi_rdata_reg[8]_i_2_n_0 ;
  wire \axi_rdata_reg[8]_i_3_n_0 ;
  wire \axi_rdata_reg[9]_i_2_n_0 ;
  wire \axi_rdata_reg[9]_i_3_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready_i_1_n_0;
  wire b_ram_data_reg;
  wire [0:0]b_ram_data_reg_0;
  wire [0:0]\b_ram_sel_reg[0] ;
  wire [7:0]\b_ram_sel_reg[7] ;
  wire clk;
  wire [0:0]\count_i_reg[6] ;
  wire [0:0]\count_i_reg[6]_0 ;
  wire [0:0]\count_i_reg[6]_1 ;
  wire [0:0]\count_i_reg[6]_2 ;
  wire [0:0]\count_i_reg[6]_3 ;
  wire [0:0]\count_i_reg[6]_4 ;
  wire [6:0]counter;
  wire \counter[0]_i_2_n_0 ;
  wire \counter_reg[0] ;
  wire [1:0]\counter_reg[1] ;
  wire \counter_reg[1]_0 ;
  wire [2:0]\counter_reg[1]_1 ;
  wire \counter_reg[1]_10 ;
  wire [1:0]\counter_reg[1]_11 ;
  wire [0:0]\counter_reg[1]_12 ;
  wire \counter_reg[1]_13 ;
  wire [2:0]\counter_reg[1]_2 ;
  wire [1:0]\counter_reg[1]_3 ;
  wire [1:0]\counter_reg[1]_4 ;
  wire [2:0]\counter_reg[1]_5 ;
  wire [2:0]\counter_reg[1]_6 ;
  wire [2:0]\counter_reg[1]_7 ;
  wire [1:0]\counter_reg[1]_8 ;
  wire [1:0]\counter_reg[1]_9 ;
  wire [0:0]\counter_reg[2] ;
  wire [0:0]\counter_reg[2]_0 ;
  wire [0:0]\counter_reg[2]_1 ;
  wire [0:0]\counter_reg[2]_2 ;
  wire [0:0]\counter_reg[2]_3 ;
  wire [0:0]\counter_reg[2]_4 ;
  wire [0:0]\counter_reg[2]_5 ;
  wire \counter_reg[2]_6 ;
  wire [2:0]\counter_reg[5] ;
  wire [2:0]\counter_reg[5]_0 ;
  wire [2:0]\counter_reg[5]_1 ;
  wire [2:0]\counter_reg[5]_2 ;
  wire [5:0]\counter_reg[8] ;
  wire [5:0]\counter_reg[8]_0 ;
  wire [5:0]\counter_reg[8]_1 ;
  wire [5:0]\counter_reg[8]_2 ;
  wire [13:12]cpu2emsc_register;
  wire [31:0]data_in;
  wire [31:0]data_in_w;
  wire dp_enable;
  wire dp_extend_end;
  wire in_stream_handshake_delay;
  wire in_stream_ready_reg;
  wire initialized;
  wire initialized_reg;
  wire p_rdy;
  wire \read_address[6]_i_3_n_0 ;
  wire [3:0]\read_address_reg[0] ;
  wire [3:0]\read_address_reg[0]_0 ;
  wire [2:0]\read_address_reg[0]_1 ;
  wire [0:0]\read_address_reg[0]_2 ;
  wire [0:0]\read_address_reg[0]_3 ;
  wire [0:0]\read_address_reg[0]_4 ;
  wire [0:0]\read_address_reg[0]_5 ;
  wire [1:0]\read_address_reg[1] ;
  wire [31:0]\read_data[0]_14 ;
  wire \res_mem[383]_i_3_n_0 ;
  wire [3:0]s_axi_ctrl_status_araddr;
  wire s_axi_ctrl_status_arready;
  wire s_axi_ctrl_status_arvalid;
  wire [3:0]s_axi_ctrl_status_awaddr;
  wire s_axi_ctrl_status_awready;
  wire s_axi_ctrl_status_awvalid;
  wire s_axi_ctrl_status_bready;
  wire s_axi_ctrl_status_bvalid;
  wire [31:0]s_axi_ctrl_status_rdata;
  wire s_axi_ctrl_status_rready;
  wire s_axi_ctrl_status_rvalid;
  wire [31:0]s_axi_ctrl_status_wdata;
  wire s_axi_ctrl_status_wready;
  wire s_axi_ctrl_status_wvalid;
  wire s_axis_tready;
  wire s_axis_tvalid;
  wire [3:0]sel0;
  wire [0:0]\slv_regs_reg[0][11]_0 ;
  wire [0:0]\slv_regs_reg[0][11]_1 ;
  wire [0:0]\slv_regs_reg[0][11]_10 ;
  wire [0:0]\slv_regs_reg[0][11]_2 ;
  wire [0:0]\slv_regs_reg[0][11]_3 ;
  wire [0:0]\slv_regs_reg[0][11]_4 ;
  wire [0:0]\slv_regs_reg[0][11]_5 ;
  wire [0:0]\slv_regs_reg[0][11]_6 ;
  wire \slv_regs_reg[0][11]_7 ;
  wire [0:0]\slv_regs_reg[0][11]_8 ;
  wire [0:0]\slv_regs_reg[0][11]_9 ;
  wire [3:0]\slv_regs_reg[0][31]_0 ;
  wire [31:0]\slv_regs_reg[10]_8 ;
  wire [31:0]\slv_regs_reg[11]_9 ;
  wire [31:0]\slv_regs_reg[12]_10 ;
  wire [31:0]\slv_regs_reg[13]_11 ;
  wire [31:0]\slv_regs_reg[14]_12 ;
  wire [31:0]\slv_regs_reg[15]_13 ;
  wire [31:0]\slv_regs_reg[2]_0 ;
  wire [31:0]\slv_regs_reg[3]_1 ;
  wire [31:0]\slv_regs_reg[4]_2 ;
  wire [31:0]\slv_regs_reg[5]_3 ;
  wire [31:0]\slv_regs_reg[6]_4 ;
  wire [31:0]\slv_regs_reg[7]_5 ;
  wire [31:0]\slv_regs_reg[8]_6 ;
  wire [31:0]\slv_regs_reg[9]_7 ;
  wire \slv_regs_reg_n_0_[0][14] ;
  wire \slv_regs_reg_n_0_[0][15] ;
  wire \slv_regs_reg_n_0_[0][16] ;
  wire \slv_regs_reg_n_0_[0][17] ;
  wire \slv_regs_reg_n_0_[0][18] ;
  wire \slv_regs_reg_n_0_[0][19] ;
  wire \slv_regs_reg_n_0_[0][20] ;
  wire \slv_regs_reg_n_0_[0][21] ;
  wire \slv_regs_reg_n_0_[0][22] ;
  wire \slv_regs_reg_n_0_[0][23] ;
  wire \slv_regs_reg_n_0_[0][24] ;
  wire \slv_regs_reg_n_0_[0][25] ;
  wire \slv_regs_reg_n_0_[0][26] ;
  wire \slv_regs_reg_n_0_[0][27] ;
  wire \slv_regs_reg_n_0_[0][28] ;
  wire \slv_regs_reg_n_0_[0][29] ;
  wire \slv_regs_reg_n_0_[0][30] ;
  wire \slv_regs_reg_n_0_[0][31] ;
  wire [0:0]state;
  wire \state[1]_i_2_n_0 ;
  wire \state[1]_i_3_n_0 ;
  wire \state_reg[0] ;
  wire \state_reg[0]_0 ;
  wire \state_reg[1] ;
  wire \state_reg[1]_0 ;
  wire valid_input;
  wire valid_input1_out;
  wire [3:3]NLW_add_r1_carry__0_i_1_CO_UNCONNECTED;

  CARRY4 add_r1_carry__0_i_1
       (.CI(add_r1_carry_i_5_n_0),
        .CO({NLW_add_r1_carry__0_i_1_CO_UNCONNECTED[3],add_r2[12],add_r1_carry__0_i_1_n_2,add_r1_carry__0_i_1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_reg[1]_12 ,add_r2[11:9]}),
        .S({1'b1,Q[11:9]}));
  LUT3 #(
    .INIT(8'h01)) 
    add_r1_carry_i_1
       (.I0(add_r2[10]),
        .I1(add_r2[11]),
        .I2(add_r2[9]),
        .O(\counter_reg[1]_9 [1]));
  LUT3 #(
    .INIT(8'h01)) 
    add_r1_carry_i_1__0
       (.I0(add_r2[10]),
        .I1(add_r2[11]),
        .I2(add_r2[9]),
        .O(\counter_reg[1]_1 [2]));
  LUT3 #(
    .INIT(8'h01)) 
    add_r1_carry_i_1__1
       (.I0(add_r2[10]),
        .I1(add_r2[11]),
        .I2(add_r2[9]),
        .O(\counter_reg[1]_3 [1]));
  LUT3 #(
    .INIT(8'h01)) 
    add_r1_carry_i_1__2
       (.I0(add_r2[10]),
        .I1(add_r2[11]),
        .I2(add_r2[9]),
        .O(\counter_reg[1]_4 [1]));
  LUT3 #(
    .INIT(8'h01)) 
    add_r1_carry_i_1__3
       (.I0(add_r2[10]),
        .I1(add_r2[11]),
        .I2(add_r2[9]),
        .O(\counter_reg[1]_5 [2]));
  LUT3 #(
    .INIT(8'h01)) 
    add_r1_carry_i_1__4
       (.I0(add_r2[10]),
        .I1(add_r2[11]),
        .I2(add_r2[9]),
        .O(\counter_reg[1]_6 [2]));
  LUT3 #(
    .INIT(8'h01)) 
    add_r1_carry_i_1__5
       (.I0(add_r2[10]),
        .I1(add_r2[11]),
        .I2(add_r2[9]),
        .O(\counter_reg[1]_7 [2]));
  LUT3 #(
    .INIT(8'h01)) 
    add_r1_carry_i_1__6
       (.I0(add_r2[10]),
        .I1(add_r2[11]),
        .I2(add_r2[9]),
        .O(\counter_reg[1]_8 [1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_2
       (.I0(\counter_reg[1]_2 [0]),
        .I1(\counter_reg[8] [3]),
        .I2(\counter_reg[8] [5]),
        .I3(\counter_reg[1]_2 [2]),
        .I4(\counter_reg[8] [4]),
        .I5(\counter_reg[1]_2 [1]),
        .O(\counter_reg[1]_1 [1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_2__2
       (.I0(\counter_reg[1]_2 [0]),
        .I1(\counter_reg[8]_0 [3]),
        .I2(\counter_reg[8]_0 [5]),
        .I3(\counter_reg[1]_2 [2]),
        .I4(\counter_reg[8]_0 [4]),
        .I5(\counter_reg[1]_2 [1]),
        .O(\counter_reg[1]_5 [1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_2__3
       (.I0(\counter_reg[1]_2 [0]),
        .I1(\counter_reg[8]_1 [3]),
        .I2(\counter_reg[8]_1 [5]),
        .I3(\counter_reg[1]_2 [2]),
        .I4(\counter_reg[8]_1 [4]),
        .I5(\counter_reg[1]_2 [1]),
        .O(\counter_reg[1]_6 [1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_2__4
       (.I0(\counter_reg[1]_2 [0]),
        .I1(\counter_reg[8]_2 [3]),
        .I2(\counter_reg[8]_2 [5]),
        .I3(\counter_reg[1]_2 [2]),
        .I4(\counter_reg[8]_2 [4]),
        .I5(\counter_reg[1]_2 [1]),
        .O(\counter_reg[1]_7 [1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_3
       (.I0(add_r2[4]),
        .I1(\counter_reg[8] [1]),
        .I2(\counter_reg[8] [2]),
        .I3(add_r2[5]),
        .I4(\counter_reg[8] [0]),
        .I5(add_r2[3]),
        .O(\counter_reg[1]_1 [0]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_3__0
       (.I0(add_r2[3]),
        .I1(\counter_reg[5] [0]),
        .I2(\counter_reg[5] [2]),
        .I3(add_r2[5]),
        .I4(\counter_reg[5] [1]),
        .I5(add_r2[4]),
        .O(\counter_reg[1]_3 [0]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_3__1
       (.I0(add_r2[3]),
        .I1(\counter_reg[5]_0 [0]),
        .I2(\counter_reg[5]_0 [2]),
        .I3(add_r2[5]),
        .I4(\counter_reg[5]_0 [1]),
        .I5(add_r2[4]),
        .O(\counter_reg[1]_4 [0]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_3__2
       (.I0(add_r2[3]),
        .I1(\counter_reg[8]_0 [0]),
        .I2(\counter_reg[8]_0 [2]),
        .I3(add_r2[5]),
        .I4(\counter_reg[8]_0 [1]),
        .I5(add_r2[4]),
        .O(\counter_reg[1]_5 [0]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_3__3
       (.I0(add_r2[3]),
        .I1(\counter_reg[8]_1 [0]),
        .I2(\counter_reg[8]_1 [2]),
        .I3(add_r2[5]),
        .I4(\counter_reg[8]_1 [1]),
        .I5(add_r2[4]),
        .O(\counter_reg[1]_6 [0]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_3__4
       (.I0(add_r2[3]),
        .I1(\counter_reg[8]_2 [0]),
        .I2(\counter_reg[8]_2 [2]),
        .I3(add_r2[5]),
        .I4(\counter_reg[8]_2 [1]),
        .I5(add_r2[4]),
        .O(\counter_reg[1]_7 [0]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_3__5
       (.I0(add_r2[4]),
        .I1(\counter_reg[5]_1 [1]),
        .I2(\counter_reg[5]_1 [2]),
        .I3(add_r2[5]),
        .I4(\counter_reg[5]_1 [0]),
        .I5(add_r2[3]),
        .O(\counter_reg[1]_8 [0]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    add_r1_carry_i_3__6
       (.I0(add_r2[3]),
        .I1(\counter_reg[5]_2 [0]),
        .I2(\counter_reg[5]_2 [2]),
        .I3(add_r2[5]),
        .I4(\counter_reg[5]_2 [1]),
        .I5(add_r2[4]),
        .O(\counter_reg[1]_9 [0]));
  CARRY4 add_r1_carry_i_5
       (.CI(add_r1_carry_i_6_n_0),
        .CO({add_r1_carry_i_5_n_0,add_r1_carry_i_5_n_1,add_r1_carry_i_5_n_2,add_r1_carry_i_5_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_reg[1]_2 ,add_r2[5]}),
        .S(Q[8:5]));
  CARRY4 add_r1_carry_i_6
       (.CI(1'b0),
        .CO({add_r1_carry_i_6_n_0,add_r1_carry_i_6_n_1,add_r1_carry_i_6_n_2,add_r1_carry_i_6_n_3}),
        .CYINIT(Q[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({add_r2[4:3],\counter_reg[1]_11 }),
        .S(Q[4:1]));
  FDSE \axi_araddr_reg[2] 
       (.C(clk),
        .CE(axi_arready_i_1_n_0),
        .D(s_axi_ctrl_status_araddr[0]),
        .Q(sel0[0]),
        .S(aresetn_0));
  FDSE \axi_araddr_reg[3] 
       (.C(clk),
        .CE(axi_arready_i_1_n_0),
        .D(s_axi_ctrl_status_araddr[1]),
        .Q(sel0[1]),
        .S(aresetn_0));
  FDSE \axi_araddr_reg[4] 
       (.C(clk),
        .CE(axi_arready_i_1_n_0),
        .D(s_axi_ctrl_status_araddr[2]),
        .Q(sel0[2]),
        .S(aresetn_0));
  FDSE \axi_araddr_reg[5] 
       (.C(clk),
        .CE(axi_arready_i_1_n_0),
        .D(s_axi_ctrl_status_araddr[3]),
        .Q(sel0[3]),
        .S(aresetn_0));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s_axi_ctrl_status_arvalid),
        .I1(s_axi_ctrl_status_arready),
        .O(axi_arready_i_1_n_0));
  FDRE axi_arready_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_arready_i_1_n_0),
        .Q(s_axi_ctrl_status_arready),
        .R(aresetn_0));
  FDRE \axi_awaddr_reg[2] 
       (.C(clk),
        .CE(axi_awready_i_1_n_0),
        .D(s_axi_ctrl_status_awaddr[0]),
        .Q(\slv_regs_reg[0][31]_0 [0]),
        .R(aresetn_0));
  FDRE \axi_awaddr_reg[3] 
       (.C(clk),
        .CE(axi_awready_i_1_n_0),
        .D(s_axi_ctrl_status_awaddr[1]),
        .Q(\slv_regs_reg[0][31]_0 [1]),
        .R(aresetn_0));
  FDRE \axi_awaddr_reg[4] 
       (.C(clk),
        .CE(axi_awready_i_1_n_0),
        .D(s_axi_ctrl_status_awaddr[2]),
        .Q(\slv_regs_reg[0][31]_0 [2]),
        .R(aresetn_0));
  FDRE \axi_awaddr_reg[5] 
       (.C(clk),
        .CE(axi_awready_i_1_n_0),
        .D(s_axi_ctrl_status_awaddr[3]),
        .Q(\slv_regs_reg[0][31]_0 [3]),
        .R(aresetn_0));
  LUT3 #(
    .INIT(8'h08)) 
    axi_awready_i_1
       (.I0(s_axi_ctrl_status_wvalid),
        .I1(s_axi_ctrl_status_awvalid),
        .I2(s_axi_ctrl_status_awready),
        .O(axi_awready_i_1_n_0));
  FDRE axi_awready_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_awready_i_1_n_0),
        .Q(s_axi_ctrl_status_awready),
        .R(aresetn_0));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s_axi_ctrl_status_wready),
        .I1(s_axi_ctrl_status_wvalid),
        .I2(s_axi_ctrl_status_awready),
        .I3(s_axi_ctrl_status_awvalid),
        .I4(s_axi_ctrl_status_bready),
        .I5(s_axi_ctrl_status_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s_axi_ctrl_status_bvalid),
        .R(aresetn_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_4 
       (.I0(\slv_regs_reg[3]_1 [0]),
        .I1(\slv_regs_reg[2]_0 [0]),
        .I2(sel0[1]),
        .I3(initialized),
        .I4(sel0[0]),
        .I5(Q[0]),
        .O(\axi_rdata[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_5 
       (.I0(\slv_regs_reg[7]_5 [0]),
        .I1(\slv_regs_reg[6]_4 [0]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [0]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [0]),
        .O(\axi_rdata[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_6 
       (.I0(\slv_regs_reg[11]_9 [0]),
        .I1(\slv_regs_reg[10]_8 [0]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [0]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [0]),
        .O(\axi_rdata[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_7 
       (.I0(\slv_regs_reg[15]_13 [0]),
        .I1(\slv_regs_reg[14]_12 [0]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [0]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [0]),
        .O(\axi_rdata[0]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[10]_i_4 
       (.I0(\slv_regs_reg[3]_1 [10]),
        .I1(\slv_regs_reg[2]_0 [10]),
        .I2(sel0[1]),
        .I3(Q[10]),
        .I4(sel0[0]),
        .O(\axi_rdata[10]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_5 
       (.I0(\slv_regs_reg[7]_5 [10]),
        .I1(\slv_regs_reg[6]_4 [10]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [10]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [10]),
        .O(\axi_rdata[10]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_6 
       (.I0(\slv_regs_reg[11]_9 [10]),
        .I1(\slv_regs_reg[10]_8 [10]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [10]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [10]),
        .O(\axi_rdata[10]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_7 
       (.I0(\slv_regs_reg[15]_13 [10]),
        .I1(\slv_regs_reg[14]_12 [10]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [10]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [10]),
        .O(\axi_rdata[10]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[11]_i_4 
       (.I0(\slv_regs_reg[3]_1 [11]),
        .I1(\slv_regs_reg[2]_0 [11]),
        .I2(sel0[1]),
        .I3(Q[11]),
        .I4(sel0[0]),
        .O(\axi_rdata[11]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_5 
       (.I0(\slv_regs_reg[7]_5 [11]),
        .I1(\slv_regs_reg[6]_4 [11]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [11]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [11]),
        .O(\axi_rdata[11]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_6 
       (.I0(\slv_regs_reg[11]_9 [11]),
        .I1(\slv_regs_reg[10]_8 [11]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [11]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [11]),
        .O(\axi_rdata[11]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_7 
       (.I0(\slv_regs_reg[15]_13 [11]),
        .I1(\slv_regs_reg[14]_12 [11]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [11]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [11]),
        .O(\axi_rdata[11]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[12]_i_4 
       (.I0(\slv_regs_reg[3]_1 [12]),
        .I1(\slv_regs_reg[2]_0 [12]),
        .I2(sel0[1]),
        .I3(cpu2emsc_register[12]),
        .I4(sel0[0]),
        .O(\axi_rdata[12]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_5 
       (.I0(\slv_regs_reg[7]_5 [12]),
        .I1(\slv_regs_reg[6]_4 [12]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [12]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [12]),
        .O(\axi_rdata[12]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_6 
       (.I0(\slv_regs_reg[11]_9 [12]),
        .I1(\slv_regs_reg[10]_8 [12]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [12]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [12]),
        .O(\axi_rdata[12]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_7 
       (.I0(\slv_regs_reg[15]_13 [12]),
        .I1(\slv_regs_reg[14]_12 [12]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [12]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [12]),
        .O(\axi_rdata[12]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[13]_i_4 
       (.I0(\slv_regs_reg[3]_1 [13]),
        .I1(\slv_regs_reg[2]_0 [13]),
        .I2(sel0[1]),
        .I3(cpu2emsc_register[13]),
        .I4(sel0[0]),
        .O(\axi_rdata[13]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_5 
       (.I0(\slv_regs_reg[7]_5 [13]),
        .I1(\slv_regs_reg[6]_4 [13]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [13]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [13]),
        .O(\axi_rdata[13]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_6 
       (.I0(\slv_regs_reg[11]_9 [13]),
        .I1(\slv_regs_reg[10]_8 [13]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [13]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [13]),
        .O(\axi_rdata[13]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_7 
       (.I0(\slv_regs_reg[15]_13 [13]),
        .I1(\slv_regs_reg[14]_12 [13]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [13]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [13]),
        .O(\axi_rdata[13]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[14]_i_4 
       (.I0(\slv_regs_reg[3]_1 [14]),
        .I1(\slv_regs_reg[2]_0 [14]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][14] ),
        .I4(sel0[0]),
        .O(\axi_rdata[14]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_5 
       (.I0(\slv_regs_reg[7]_5 [14]),
        .I1(\slv_regs_reg[6]_4 [14]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [14]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [14]),
        .O(\axi_rdata[14]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_6 
       (.I0(\slv_regs_reg[11]_9 [14]),
        .I1(\slv_regs_reg[10]_8 [14]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [14]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [14]),
        .O(\axi_rdata[14]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_7 
       (.I0(\slv_regs_reg[15]_13 [14]),
        .I1(\slv_regs_reg[14]_12 [14]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [14]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [14]),
        .O(\axi_rdata[14]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[15]_i_4 
       (.I0(\slv_regs_reg[3]_1 [15]),
        .I1(\slv_regs_reg[2]_0 [15]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][15] ),
        .I4(sel0[0]),
        .O(\axi_rdata[15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_5 
       (.I0(\slv_regs_reg[7]_5 [15]),
        .I1(\slv_regs_reg[6]_4 [15]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [15]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [15]),
        .O(\axi_rdata[15]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_6 
       (.I0(\slv_regs_reg[11]_9 [15]),
        .I1(\slv_regs_reg[10]_8 [15]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [15]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [15]),
        .O(\axi_rdata[15]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_7 
       (.I0(\slv_regs_reg[15]_13 [15]),
        .I1(\slv_regs_reg[14]_12 [15]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [15]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [15]),
        .O(\axi_rdata[15]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[16]_i_4 
       (.I0(\slv_regs_reg[3]_1 [16]),
        .I1(\slv_regs_reg[2]_0 [16]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][16] ),
        .I4(sel0[0]),
        .O(\axi_rdata[16]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_5 
       (.I0(\slv_regs_reg[7]_5 [16]),
        .I1(\slv_regs_reg[6]_4 [16]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [16]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [16]),
        .O(\axi_rdata[16]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_6 
       (.I0(\slv_regs_reg[11]_9 [16]),
        .I1(\slv_regs_reg[10]_8 [16]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [16]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [16]),
        .O(\axi_rdata[16]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_7 
       (.I0(\slv_regs_reg[15]_13 [16]),
        .I1(\slv_regs_reg[14]_12 [16]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [16]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [16]),
        .O(\axi_rdata[16]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[17]_i_4 
       (.I0(\slv_regs_reg[3]_1 [17]),
        .I1(\slv_regs_reg[2]_0 [17]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][17] ),
        .I4(sel0[0]),
        .O(\axi_rdata[17]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_5 
       (.I0(\slv_regs_reg[7]_5 [17]),
        .I1(\slv_regs_reg[6]_4 [17]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [17]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [17]),
        .O(\axi_rdata[17]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_6 
       (.I0(\slv_regs_reg[11]_9 [17]),
        .I1(\slv_regs_reg[10]_8 [17]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [17]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [17]),
        .O(\axi_rdata[17]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_7 
       (.I0(\slv_regs_reg[15]_13 [17]),
        .I1(\slv_regs_reg[14]_12 [17]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [17]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [17]),
        .O(\axi_rdata[17]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[18]_i_4 
       (.I0(\slv_regs_reg[3]_1 [18]),
        .I1(\slv_regs_reg[2]_0 [18]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][18] ),
        .I4(sel0[0]),
        .O(\axi_rdata[18]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_5 
       (.I0(\slv_regs_reg[7]_5 [18]),
        .I1(\slv_regs_reg[6]_4 [18]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [18]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [18]),
        .O(\axi_rdata[18]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_6 
       (.I0(\slv_regs_reg[11]_9 [18]),
        .I1(\slv_regs_reg[10]_8 [18]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [18]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [18]),
        .O(\axi_rdata[18]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_7 
       (.I0(\slv_regs_reg[15]_13 [18]),
        .I1(\slv_regs_reg[14]_12 [18]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [18]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [18]),
        .O(\axi_rdata[18]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[19]_i_4 
       (.I0(\slv_regs_reg[3]_1 [19]),
        .I1(\slv_regs_reg[2]_0 [19]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][19] ),
        .I4(sel0[0]),
        .O(\axi_rdata[19]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_5 
       (.I0(\slv_regs_reg[7]_5 [19]),
        .I1(\slv_regs_reg[6]_4 [19]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [19]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [19]),
        .O(\axi_rdata[19]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_6 
       (.I0(\slv_regs_reg[11]_9 [19]),
        .I1(\slv_regs_reg[10]_8 [19]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [19]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [19]),
        .O(\axi_rdata[19]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_7 
       (.I0(\slv_regs_reg[15]_13 [19]),
        .I1(\slv_regs_reg[14]_12 [19]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [19]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [19]),
        .O(\axi_rdata[19]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[1]_i_4 
       (.I0(\slv_regs_reg[3]_1 [1]),
        .I1(\slv_regs_reg[2]_0 [1]),
        .I2(sel0[1]),
        .I3(Q[1]),
        .I4(sel0[0]),
        .O(\axi_rdata[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_5 
       (.I0(\slv_regs_reg[7]_5 [1]),
        .I1(\slv_regs_reg[6]_4 [1]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [1]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [1]),
        .O(\axi_rdata[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_6 
       (.I0(\slv_regs_reg[11]_9 [1]),
        .I1(\slv_regs_reg[10]_8 [1]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [1]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [1]),
        .O(\axi_rdata[1]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_7 
       (.I0(\slv_regs_reg[15]_13 [1]),
        .I1(\slv_regs_reg[14]_12 [1]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [1]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [1]),
        .O(\axi_rdata[1]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[20]_i_4 
       (.I0(\slv_regs_reg[3]_1 [20]),
        .I1(\slv_regs_reg[2]_0 [20]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][20] ),
        .I4(sel0[0]),
        .O(\axi_rdata[20]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_5 
       (.I0(\slv_regs_reg[7]_5 [20]),
        .I1(\slv_regs_reg[6]_4 [20]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [20]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [20]),
        .O(\axi_rdata[20]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_6 
       (.I0(\slv_regs_reg[11]_9 [20]),
        .I1(\slv_regs_reg[10]_8 [20]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [20]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [20]),
        .O(\axi_rdata[20]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_7 
       (.I0(\slv_regs_reg[15]_13 [20]),
        .I1(\slv_regs_reg[14]_12 [20]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [20]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [20]),
        .O(\axi_rdata[20]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[21]_i_4 
       (.I0(\slv_regs_reg[3]_1 [21]),
        .I1(\slv_regs_reg[2]_0 [21]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][21] ),
        .I4(sel0[0]),
        .O(\axi_rdata[21]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_5 
       (.I0(\slv_regs_reg[7]_5 [21]),
        .I1(\slv_regs_reg[6]_4 [21]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [21]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [21]),
        .O(\axi_rdata[21]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_6 
       (.I0(\slv_regs_reg[11]_9 [21]),
        .I1(\slv_regs_reg[10]_8 [21]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [21]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [21]),
        .O(\axi_rdata[21]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_7 
       (.I0(\slv_regs_reg[15]_13 [21]),
        .I1(\slv_regs_reg[14]_12 [21]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [21]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [21]),
        .O(\axi_rdata[21]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[22]_i_4 
       (.I0(\slv_regs_reg[3]_1 [22]),
        .I1(\slv_regs_reg[2]_0 [22]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][22] ),
        .I4(sel0[0]),
        .O(\axi_rdata[22]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_5 
       (.I0(\slv_regs_reg[7]_5 [22]),
        .I1(\slv_regs_reg[6]_4 [22]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [22]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [22]),
        .O(\axi_rdata[22]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_6 
       (.I0(\slv_regs_reg[11]_9 [22]),
        .I1(\slv_regs_reg[10]_8 [22]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [22]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [22]),
        .O(\axi_rdata[22]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_7 
       (.I0(\slv_regs_reg[15]_13 [22]),
        .I1(\slv_regs_reg[14]_12 [22]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [22]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [22]),
        .O(\axi_rdata[22]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[23]_i_4 
       (.I0(\slv_regs_reg[3]_1 [23]),
        .I1(\slv_regs_reg[2]_0 [23]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][23] ),
        .I4(sel0[0]),
        .O(\axi_rdata[23]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_5 
       (.I0(\slv_regs_reg[7]_5 [23]),
        .I1(\slv_regs_reg[6]_4 [23]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [23]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [23]),
        .O(\axi_rdata[23]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_6 
       (.I0(\slv_regs_reg[11]_9 [23]),
        .I1(\slv_regs_reg[10]_8 [23]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [23]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [23]),
        .O(\axi_rdata[23]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_7 
       (.I0(\slv_regs_reg[15]_13 [23]),
        .I1(\slv_regs_reg[14]_12 [23]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [23]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [23]),
        .O(\axi_rdata[23]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[24]_i_4 
       (.I0(\slv_regs_reg[3]_1 [24]),
        .I1(\slv_regs_reg[2]_0 [24]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][24] ),
        .I4(sel0[0]),
        .O(\axi_rdata[24]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_5 
       (.I0(\slv_regs_reg[7]_5 [24]),
        .I1(\slv_regs_reg[6]_4 [24]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [24]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [24]),
        .O(\axi_rdata[24]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_6 
       (.I0(\slv_regs_reg[11]_9 [24]),
        .I1(\slv_regs_reg[10]_8 [24]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [24]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [24]),
        .O(\axi_rdata[24]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_7 
       (.I0(\slv_regs_reg[15]_13 [24]),
        .I1(\slv_regs_reg[14]_12 [24]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [24]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [24]),
        .O(\axi_rdata[24]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[25]_i_4 
       (.I0(\slv_regs_reg[3]_1 [25]),
        .I1(\slv_regs_reg[2]_0 [25]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][25] ),
        .I4(sel0[0]),
        .O(\axi_rdata[25]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_5 
       (.I0(\slv_regs_reg[7]_5 [25]),
        .I1(\slv_regs_reg[6]_4 [25]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [25]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [25]),
        .O(\axi_rdata[25]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_6 
       (.I0(\slv_regs_reg[11]_9 [25]),
        .I1(\slv_regs_reg[10]_8 [25]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [25]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [25]),
        .O(\axi_rdata[25]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_7 
       (.I0(\slv_regs_reg[15]_13 [25]),
        .I1(\slv_regs_reg[14]_12 [25]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [25]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [25]),
        .O(\axi_rdata[25]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[26]_i_4 
       (.I0(\slv_regs_reg[3]_1 [26]),
        .I1(\slv_regs_reg[2]_0 [26]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][26] ),
        .I4(sel0[0]),
        .O(\axi_rdata[26]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_5 
       (.I0(\slv_regs_reg[7]_5 [26]),
        .I1(\slv_regs_reg[6]_4 [26]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [26]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [26]),
        .O(\axi_rdata[26]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_6 
       (.I0(\slv_regs_reg[11]_9 [26]),
        .I1(\slv_regs_reg[10]_8 [26]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [26]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [26]),
        .O(\axi_rdata[26]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_7 
       (.I0(\slv_regs_reg[15]_13 [26]),
        .I1(\slv_regs_reg[14]_12 [26]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [26]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [26]),
        .O(\axi_rdata[26]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[27]_i_4 
       (.I0(\slv_regs_reg[3]_1 [27]),
        .I1(\slv_regs_reg[2]_0 [27]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][27] ),
        .I4(sel0[0]),
        .O(\axi_rdata[27]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_5 
       (.I0(\slv_regs_reg[7]_5 [27]),
        .I1(\slv_regs_reg[6]_4 [27]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [27]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [27]),
        .O(\axi_rdata[27]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_6 
       (.I0(\slv_regs_reg[11]_9 [27]),
        .I1(\slv_regs_reg[10]_8 [27]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [27]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [27]),
        .O(\axi_rdata[27]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_7 
       (.I0(\slv_regs_reg[15]_13 [27]),
        .I1(\slv_regs_reg[14]_12 [27]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [27]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [27]),
        .O(\axi_rdata[27]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[28]_i_4 
       (.I0(\slv_regs_reg[3]_1 [28]),
        .I1(\slv_regs_reg[2]_0 [28]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][28] ),
        .I4(sel0[0]),
        .O(\axi_rdata[28]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_5 
       (.I0(\slv_regs_reg[7]_5 [28]),
        .I1(\slv_regs_reg[6]_4 [28]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [28]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [28]),
        .O(\axi_rdata[28]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_6 
       (.I0(\slv_regs_reg[11]_9 [28]),
        .I1(\slv_regs_reg[10]_8 [28]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [28]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [28]),
        .O(\axi_rdata[28]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_7 
       (.I0(\slv_regs_reg[15]_13 [28]),
        .I1(\slv_regs_reg[14]_12 [28]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [28]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [28]),
        .O(\axi_rdata[28]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[29]_i_4 
       (.I0(\slv_regs_reg[3]_1 [29]),
        .I1(\slv_regs_reg[2]_0 [29]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][29] ),
        .I4(sel0[0]),
        .O(\axi_rdata[29]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_5 
       (.I0(\slv_regs_reg[7]_5 [29]),
        .I1(\slv_regs_reg[6]_4 [29]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [29]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [29]),
        .O(\axi_rdata[29]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_6 
       (.I0(\slv_regs_reg[11]_9 [29]),
        .I1(\slv_regs_reg[10]_8 [29]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [29]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [29]),
        .O(\axi_rdata[29]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_7 
       (.I0(\slv_regs_reg[15]_13 [29]),
        .I1(\slv_regs_reg[14]_12 [29]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [29]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [29]),
        .O(\axi_rdata[29]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[2]_i_4 
       (.I0(\slv_regs_reg[3]_1 [2]),
        .I1(\slv_regs_reg[2]_0 [2]),
        .I2(sel0[1]),
        .I3(Q[2]),
        .I4(sel0[0]),
        .O(\axi_rdata[2]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_5 
       (.I0(\slv_regs_reg[7]_5 [2]),
        .I1(\slv_regs_reg[6]_4 [2]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [2]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [2]),
        .O(\axi_rdata[2]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_6 
       (.I0(\slv_regs_reg[11]_9 [2]),
        .I1(\slv_regs_reg[10]_8 [2]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [2]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [2]),
        .O(\axi_rdata[2]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_7 
       (.I0(\slv_regs_reg[15]_13 [2]),
        .I1(\slv_regs_reg[14]_12 [2]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [2]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [2]),
        .O(\axi_rdata[2]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[30]_i_4 
       (.I0(\slv_regs_reg[3]_1 [30]),
        .I1(\slv_regs_reg[2]_0 [30]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][30] ),
        .I4(sel0[0]),
        .O(\axi_rdata[30]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_5 
       (.I0(\slv_regs_reg[7]_5 [30]),
        .I1(\slv_regs_reg[6]_4 [30]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [30]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [30]),
        .O(\axi_rdata[30]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_6 
       (.I0(\slv_regs_reg[11]_9 [30]),
        .I1(\slv_regs_reg[10]_8 [30]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [30]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [30]),
        .O(\axi_rdata[30]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_7 
       (.I0(\slv_regs_reg[15]_13 [30]),
        .I1(\slv_regs_reg[14]_12 [30]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [30]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [30]),
        .O(\axi_rdata[30]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(s_axi_ctrl_status_arready),
        .I1(s_axi_ctrl_status_arvalid),
        .I2(s_axi_ctrl_status_rvalid),
        .O(\axi_rdata[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[31]_i_5 
       (.I0(\slv_regs_reg[3]_1 [31]),
        .I1(\slv_regs_reg[2]_0 [31]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg_n_0_[0][31] ),
        .I4(sel0[0]),
        .O(\axi_rdata[31]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_6 
       (.I0(\slv_regs_reg[7]_5 [31]),
        .I1(\slv_regs_reg[6]_4 [31]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [31]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [31]),
        .O(\axi_rdata[31]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_7 
       (.I0(\slv_regs_reg[11]_9 [31]),
        .I1(\slv_regs_reg[10]_8 [31]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [31]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [31]),
        .O(\axi_rdata[31]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_8 
       (.I0(\slv_regs_reg[15]_13 [31]),
        .I1(\slv_regs_reg[14]_12 [31]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [31]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [31]),
        .O(\axi_rdata[31]_i_8_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[3]_i_4 
       (.I0(\slv_regs_reg[3]_1 [3]),
        .I1(\slv_regs_reg[2]_0 [3]),
        .I2(sel0[1]),
        .I3(Q[3]),
        .I4(sel0[0]),
        .O(\axi_rdata[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_5 
       (.I0(\slv_regs_reg[7]_5 [3]),
        .I1(\slv_regs_reg[6]_4 [3]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [3]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [3]),
        .O(\axi_rdata[3]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_6 
       (.I0(\slv_regs_reg[11]_9 [3]),
        .I1(\slv_regs_reg[10]_8 [3]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [3]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [3]),
        .O(\axi_rdata[3]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_7 
       (.I0(\slv_regs_reg[15]_13 [3]),
        .I1(\slv_regs_reg[14]_12 [3]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [3]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [3]),
        .O(\axi_rdata[3]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[4]_i_4 
       (.I0(\slv_regs_reg[3]_1 [4]),
        .I1(\slv_regs_reg[2]_0 [4]),
        .I2(sel0[1]),
        .I3(Q[4]),
        .I4(sel0[0]),
        .O(\axi_rdata[4]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_5 
       (.I0(\slv_regs_reg[7]_5 [4]),
        .I1(\slv_regs_reg[6]_4 [4]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [4]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [4]),
        .O(\axi_rdata[4]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_6 
       (.I0(\slv_regs_reg[11]_9 [4]),
        .I1(\slv_regs_reg[10]_8 [4]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [4]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [4]),
        .O(\axi_rdata[4]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_7 
       (.I0(\slv_regs_reg[15]_13 [4]),
        .I1(\slv_regs_reg[14]_12 [4]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [4]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [4]),
        .O(\axi_rdata[4]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[5]_i_4 
       (.I0(\slv_regs_reg[3]_1 [5]),
        .I1(\slv_regs_reg[2]_0 [5]),
        .I2(sel0[1]),
        .I3(Q[5]),
        .I4(sel0[0]),
        .O(\axi_rdata[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_5 
       (.I0(\slv_regs_reg[7]_5 [5]),
        .I1(\slv_regs_reg[6]_4 [5]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [5]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [5]),
        .O(\axi_rdata[5]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_6 
       (.I0(\slv_regs_reg[11]_9 [5]),
        .I1(\slv_regs_reg[10]_8 [5]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [5]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [5]),
        .O(\axi_rdata[5]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_7 
       (.I0(\slv_regs_reg[15]_13 [5]),
        .I1(\slv_regs_reg[14]_12 [5]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [5]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [5]),
        .O(\axi_rdata[5]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[6]_i_4 
       (.I0(\slv_regs_reg[3]_1 [6]),
        .I1(\slv_regs_reg[2]_0 [6]),
        .I2(sel0[1]),
        .I3(Q[6]),
        .I4(sel0[0]),
        .O(\axi_rdata[6]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_5 
       (.I0(\slv_regs_reg[7]_5 [6]),
        .I1(\slv_regs_reg[6]_4 [6]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [6]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [6]),
        .O(\axi_rdata[6]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_6 
       (.I0(\slv_regs_reg[11]_9 [6]),
        .I1(\slv_regs_reg[10]_8 [6]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [6]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [6]),
        .O(\axi_rdata[6]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_7 
       (.I0(\slv_regs_reg[15]_13 [6]),
        .I1(\slv_regs_reg[14]_12 [6]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [6]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [6]),
        .O(\axi_rdata[6]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[7]_i_4 
       (.I0(\slv_regs_reg[3]_1 [7]),
        .I1(\slv_regs_reg[2]_0 [7]),
        .I2(sel0[1]),
        .I3(Q[7]),
        .I4(sel0[0]),
        .O(\axi_rdata[7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_5 
       (.I0(\slv_regs_reg[7]_5 [7]),
        .I1(\slv_regs_reg[6]_4 [7]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [7]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [7]),
        .O(\axi_rdata[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_6 
       (.I0(\slv_regs_reg[11]_9 [7]),
        .I1(\slv_regs_reg[10]_8 [7]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [7]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [7]),
        .O(\axi_rdata[7]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_7 
       (.I0(\slv_regs_reg[15]_13 [7]),
        .I1(\slv_regs_reg[14]_12 [7]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [7]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [7]),
        .O(\axi_rdata[7]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[8]_i_4 
       (.I0(\slv_regs_reg[3]_1 [8]),
        .I1(\slv_regs_reg[2]_0 [8]),
        .I2(sel0[1]),
        .I3(Q[8]),
        .I4(sel0[0]),
        .O(\axi_rdata[8]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_5 
       (.I0(\slv_regs_reg[7]_5 [8]),
        .I1(\slv_regs_reg[6]_4 [8]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [8]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [8]),
        .O(\axi_rdata[8]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_6 
       (.I0(\slv_regs_reg[11]_9 [8]),
        .I1(\slv_regs_reg[10]_8 [8]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [8]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [8]),
        .O(\axi_rdata[8]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_7 
       (.I0(\slv_regs_reg[15]_13 [8]),
        .I1(\slv_regs_reg[14]_12 [8]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [8]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [8]),
        .O(\axi_rdata[8]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    \axi_rdata[9]_i_4 
       (.I0(\slv_regs_reg[3]_1 [9]),
        .I1(\slv_regs_reg[2]_0 [9]),
        .I2(sel0[1]),
        .I3(Q[9]),
        .I4(sel0[0]),
        .O(\axi_rdata[9]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_5 
       (.I0(\slv_regs_reg[7]_5 [9]),
        .I1(\slv_regs_reg[6]_4 [9]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[5]_3 [9]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[4]_2 [9]),
        .O(\axi_rdata[9]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_6 
       (.I0(\slv_regs_reg[11]_9 [9]),
        .I1(\slv_regs_reg[10]_8 [9]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[9]_7 [9]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[8]_6 [9]),
        .O(\axi_rdata[9]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_7 
       (.I0(\slv_regs_reg[15]_13 [9]),
        .I1(\slv_regs_reg[14]_12 [9]),
        .I2(sel0[1]),
        .I3(\slv_regs_reg[13]_11 [9]),
        .I4(sel0[0]),
        .I5(\slv_regs_reg[12]_10 [9]),
        .O(\axi_rdata[9]_i_7_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [0]),
        .Q(s_axi_ctrl_status_rdata[0]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[0]_i_1 
       (.I0(\axi_rdata_reg[0]_i_2_n_0 ),
        .I1(\axi_rdata_reg[0]_i_3_n_0 ),
        .O(\read_data[0]_14 [0]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[0]_i_2 
       (.I0(\axi_rdata[0]_i_4_n_0 ),
        .I1(\axi_rdata[0]_i_5_n_0 ),
        .O(\axi_rdata_reg[0]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[0]_i_3 
       (.I0(\axi_rdata[0]_i_6_n_0 ),
        .I1(\axi_rdata[0]_i_7_n_0 ),
        .O(\axi_rdata_reg[0]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[10] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [10]),
        .Q(s_axi_ctrl_status_rdata[10]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[10]_i_1 
       (.I0(\axi_rdata_reg[10]_i_2_n_0 ),
        .I1(\axi_rdata_reg[10]_i_3_n_0 ),
        .O(\read_data[0]_14 [10]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[10]_i_2 
       (.I0(\axi_rdata[10]_i_4_n_0 ),
        .I1(\axi_rdata[10]_i_5_n_0 ),
        .O(\axi_rdata_reg[10]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[10]_i_3 
       (.I0(\axi_rdata[10]_i_6_n_0 ),
        .I1(\axi_rdata[10]_i_7_n_0 ),
        .O(\axi_rdata_reg[10]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[11] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [11]),
        .Q(s_axi_ctrl_status_rdata[11]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[11]_i_1 
       (.I0(\axi_rdata_reg[11]_i_2_n_0 ),
        .I1(\axi_rdata_reg[11]_i_3_n_0 ),
        .O(\read_data[0]_14 [11]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[11]_i_2 
       (.I0(\axi_rdata[11]_i_4_n_0 ),
        .I1(\axi_rdata[11]_i_5_n_0 ),
        .O(\axi_rdata_reg[11]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[11]_i_3 
       (.I0(\axi_rdata[11]_i_6_n_0 ),
        .I1(\axi_rdata[11]_i_7_n_0 ),
        .O(\axi_rdata_reg[11]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[12] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [12]),
        .Q(s_axi_ctrl_status_rdata[12]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[12]_i_1 
       (.I0(\axi_rdata_reg[12]_i_2_n_0 ),
        .I1(\axi_rdata_reg[12]_i_3_n_0 ),
        .O(\read_data[0]_14 [12]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[12]_i_2 
       (.I0(\axi_rdata[12]_i_4_n_0 ),
        .I1(\axi_rdata[12]_i_5_n_0 ),
        .O(\axi_rdata_reg[12]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[12]_i_3 
       (.I0(\axi_rdata[12]_i_6_n_0 ),
        .I1(\axi_rdata[12]_i_7_n_0 ),
        .O(\axi_rdata_reg[12]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[13] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [13]),
        .Q(s_axi_ctrl_status_rdata[13]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[13]_i_1 
       (.I0(\axi_rdata_reg[13]_i_2_n_0 ),
        .I1(\axi_rdata_reg[13]_i_3_n_0 ),
        .O(\read_data[0]_14 [13]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[13]_i_2 
       (.I0(\axi_rdata[13]_i_4_n_0 ),
        .I1(\axi_rdata[13]_i_5_n_0 ),
        .O(\axi_rdata_reg[13]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[13]_i_3 
       (.I0(\axi_rdata[13]_i_6_n_0 ),
        .I1(\axi_rdata[13]_i_7_n_0 ),
        .O(\axi_rdata_reg[13]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[14] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [14]),
        .Q(s_axi_ctrl_status_rdata[14]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[14]_i_1 
       (.I0(\axi_rdata_reg[14]_i_2_n_0 ),
        .I1(\axi_rdata_reg[14]_i_3_n_0 ),
        .O(\read_data[0]_14 [14]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[14]_i_2 
       (.I0(\axi_rdata[14]_i_4_n_0 ),
        .I1(\axi_rdata[14]_i_5_n_0 ),
        .O(\axi_rdata_reg[14]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[14]_i_3 
       (.I0(\axi_rdata[14]_i_6_n_0 ),
        .I1(\axi_rdata[14]_i_7_n_0 ),
        .O(\axi_rdata_reg[14]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[15] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [15]),
        .Q(s_axi_ctrl_status_rdata[15]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[15]_i_1 
       (.I0(\axi_rdata_reg[15]_i_2_n_0 ),
        .I1(\axi_rdata_reg[15]_i_3_n_0 ),
        .O(\read_data[0]_14 [15]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[15]_i_2 
       (.I0(\axi_rdata[15]_i_4_n_0 ),
        .I1(\axi_rdata[15]_i_5_n_0 ),
        .O(\axi_rdata_reg[15]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[15]_i_3 
       (.I0(\axi_rdata[15]_i_6_n_0 ),
        .I1(\axi_rdata[15]_i_7_n_0 ),
        .O(\axi_rdata_reg[15]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[16] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [16]),
        .Q(s_axi_ctrl_status_rdata[16]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[16]_i_1 
       (.I0(\axi_rdata_reg[16]_i_2_n_0 ),
        .I1(\axi_rdata_reg[16]_i_3_n_0 ),
        .O(\read_data[0]_14 [16]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[16]_i_2 
       (.I0(\axi_rdata[16]_i_4_n_0 ),
        .I1(\axi_rdata[16]_i_5_n_0 ),
        .O(\axi_rdata_reg[16]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[16]_i_3 
       (.I0(\axi_rdata[16]_i_6_n_0 ),
        .I1(\axi_rdata[16]_i_7_n_0 ),
        .O(\axi_rdata_reg[16]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[17] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [17]),
        .Q(s_axi_ctrl_status_rdata[17]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[17]_i_1 
       (.I0(\axi_rdata_reg[17]_i_2_n_0 ),
        .I1(\axi_rdata_reg[17]_i_3_n_0 ),
        .O(\read_data[0]_14 [17]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[17]_i_2 
       (.I0(\axi_rdata[17]_i_4_n_0 ),
        .I1(\axi_rdata[17]_i_5_n_0 ),
        .O(\axi_rdata_reg[17]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[17]_i_3 
       (.I0(\axi_rdata[17]_i_6_n_0 ),
        .I1(\axi_rdata[17]_i_7_n_0 ),
        .O(\axi_rdata_reg[17]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[18] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [18]),
        .Q(s_axi_ctrl_status_rdata[18]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[18]_i_1 
       (.I0(\axi_rdata_reg[18]_i_2_n_0 ),
        .I1(\axi_rdata_reg[18]_i_3_n_0 ),
        .O(\read_data[0]_14 [18]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[18]_i_2 
       (.I0(\axi_rdata[18]_i_4_n_0 ),
        .I1(\axi_rdata[18]_i_5_n_0 ),
        .O(\axi_rdata_reg[18]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[18]_i_3 
       (.I0(\axi_rdata[18]_i_6_n_0 ),
        .I1(\axi_rdata[18]_i_7_n_0 ),
        .O(\axi_rdata_reg[18]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[19] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [19]),
        .Q(s_axi_ctrl_status_rdata[19]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[19]_i_1 
       (.I0(\axi_rdata_reg[19]_i_2_n_0 ),
        .I1(\axi_rdata_reg[19]_i_3_n_0 ),
        .O(\read_data[0]_14 [19]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[19]_i_2 
       (.I0(\axi_rdata[19]_i_4_n_0 ),
        .I1(\axi_rdata[19]_i_5_n_0 ),
        .O(\axi_rdata_reg[19]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[19]_i_3 
       (.I0(\axi_rdata[19]_i_6_n_0 ),
        .I1(\axi_rdata[19]_i_7_n_0 ),
        .O(\axi_rdata_reg[19]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[1] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [1]),
        .Q(s_axi_ctrl_status_rdata[1]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[1]_i_1 
       (.I0(\axi_rdata_reg[1]_i_2_n_0 ),
        .I1(\axi_rdata_reg[1]_i_3_n_0 ),
        .O(\read_data[0]_14 [1]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[1]_i_2 
       (.I0(\axi_rdata[1]_i_4_n_0 ),
        .I1(\axi_rdata[1]_i_5_n_0 ),
        .O(\axi_rdata_reg[1]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[1]_i_3 
       (.I0(\axi_rdata[1]_i_6_n_0 ),
        .I1(\axi_rdata[1]_i_7_n_0 ),
        .O(\axi_rdata_reg[1]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[20] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [20]),
        .Q(s_axi_ctrl_status_rdata[20]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[20]_i_1 
       (.I0(\axi_rdata_reg[20]_i_2_n_0 ),
        .I1(\axi_rdata_reg[20]_i_3_n_0 ),
        .O(\read_data[0]_14 [20]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[20]_i_2 
       (.I0(\axi_rdata[20]_i_4_n_0 ),
        .I1(\axi_rdata[20]_i_5_n_0 ),
        .O(\axi_rdata_reg[20]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[20]_i_3 
       (.I0(\axi_rdata[20]_i_6_n_0 ),
        .I1(\axi_rdata[20]_i_7_n_0 ),
        .O(\axi_rdata_reg[20]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[21] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [21]),
        .Q(s_axi_ctrl_status_rdata[21]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[21]_i_1 
       (.I0(\axi_rdata_reg[21]_i_2_n_0 ),
        .I1(\axi_rdata_reg[21]_i_3_n_0 ),
        .O(\read_data[0]_14 [21]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[21]_i_2 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(\axi_rdata[21]_i_5_n_0 ),
        .O(\axi_rdata_reg[21]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[21]_i_3 
       (.I0(\axi_rdata[21]_i_6_n_0 ),
        .I1(\axi_rdata[21]_i_7_n_0 ),
        .O(\axi_rdata_reg[21]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[22] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [22]),
        .Q(s_axi_ctrl_status_rdata[22]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[22]_i_1 
       (.I0(\axi_rdata_reg[22]_i_2_n_0 ),
        .I1(\axi_rdata_reg[22]_i_3_n_0 ),
        .O(\read_data[0]_14 [22]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[22]_i_2 
       (.I0(\axi_rdata[22]_i_4_n_0 ),
        .I1(\axi_rdata[22]_i_5_n_0 ),
        .O(\axi_rdata_reg[22]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[22]_i_3 
       (.I0(\axi_rdata[22]_i_6_n_0 ),
        .I1(\axi_rdata[22]_i_7_n_0 ),
        .O(\axi_rdata_reg[22]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[23] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [23]),
        .Q(s_axi_ctrl_status_rdata[23]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[23]_i_1 
       (.I0(\axi_rdata_reg[23]_i_2_n_0 ),
        .I1(\axi_rdata_reg[23]_i_3_n_0 ),
        .O(\read_data[0]_14 [23]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[23]_i_2 
       (.I0(\axi_rdata[23]_i_4_n_0 ),
        .I1(\axi_rdata[23]_i_5_n_0 ),
        .O(\axi_rdata_reg[23]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[23]_i_3 
       (.I0(\axi_rdata[23]_i_6_n_0 ),
        .I1(\axi_rdata[23]_i_7_n_0 ),
        .O(\axi_rdata_reg[23]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[24] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [24]),
        .Q(s_axi_ctrl_status_rdata[24]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[24]_i_1 
       (.I0(\axi_rdata_reg[24]_i_2_n_0 ),
        .I1(\axi_rdata_reg[24]_i_3_n_0 ),
        .O(\read_data[0]_14 [24]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[24]_i_2 
       (.I0(\axi_rdata[24]_i_4_n_0 ),
        .I1(\axi_rdata[24]_i_5_n_0 ),
        .O(\axi_rdata_reg[24]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[24]_i_3 
       (.I0(\axi_rdata[24]_i_6_n_0 ),
        .I1(\axi_rdata[24]_i_7_n_0 ),
        .O(\axi_rdata_reg[24]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[25] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [25]),
        .Q(s_axi_ctrl_status_rdata[25]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[25]_i_1 
       (.I0(\axi_rdata_reg[25]_i_2_n_0 ),
        .I1(\axi_rdata_reg[25]_i_3_n_0 ),
        .O(\read_data[0]_14 [25]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[25]_i_2 
       (.I0(\axi_rdata[25]_i_4_n_0 ),
        .I1(\axi_rdata[25]_i_5_n_0 ),
        .O(\axi_rdata_reg[25]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[25]_i_3 
       (.I0(\axi_rdata[25]_i_6_n_0 ),
        .I1(\axi_rdata[25]_i_7_n_0 ),
        .O(\axi_rdata_reg[25]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[26] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [26]),
        .Q(s_axi_ctrl_status_rdata[26]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[26]_i_1 
       (.I0(\axi_rdata_reg[26]_i_2_n_0 ),
        .I1(\axi_rdata_reg[26]_i_3_n_0 ),
        .O(\read_data[0]_14 [26]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[26]_i_2 
       (.I0(\axi_rdata[26]_i_4_n_0 ),
        .I1(\axi_rdata[26]_i_5_n_0 ),
        .O(\axi_rdata_reg[26]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[26]_i_3 
       (.I0(\axi_rdata[26]_i_6_n_0 ),
        .I1(\axi_rdata[26]_i_7_n_0 ),
        .O(\axi_rdata_reg[26]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[27] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [27]),
        .Q(s_axi_ctrl_status_rdata[27]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[27]_i_1 
       (.I0(\axi_rdata_reg[27]_i_2_n_0 ),
        .I1(\axi_rdata_reg[27]_i_3_n_0 ),
        .O(\read_data[0]_14 [27]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[27]_i_2 
       (.I0(\axi_rdata[27]_i_4_n_0 ),
        .I1(\axi_rdata[27]_i_5_n_0 ),
        .O(\axi_rdata_reg[27]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[27]_i_3 
       (.I0(\axi_rdata[27]_i_6_n_0 ),
        .I1(\axi_rdata[27]_i_7_n_0 ),
        .O(\axi_rdata_reg[27]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[28] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [28]),
        .Q(s_axi_ctrl_status_rdata[28]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[28]_i_1 
       (.I0(\axi_rdata_reg[28]_i_2_n_0 ),
        .I1(\axi_rdata_reg[28]_i_3_n_0 ),
        .O(\read_data[0]_14 [28]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[28]_i_2 
       (.I0(\axi_rdata[28]_i_4_n_0 ),
        .I1(\axi_rdata[28]_i_5_n_0 ),
        .O(\axi_rdata_reg[28]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[28]_i_3 
       (.I0(\axi_rdata[28]_i_6_n_0 ),
        .I1(\axi_rdata[28]_i_7_n_0 ),
        .O(\axi_rdata_reg[28]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[29] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [29]),
        .Q(s_axi_ctrl_status_rdata[29]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[29]_i_1 
       (.I0(\axi_rdata_reg[29]_i_2_n_0 ),
        .I1(\axi_rdata_reg[29]_i_3_n_0 ),
        .O(\read_data[0]_14 [29]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[29]_i_2 
       (.I0(\axi_rdata[29]_i_4_n_0 ),
        .I1(\axi_rdata[29]_i_5_n_0 ),
        .O(\axi_rdata_reg[29]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[29]_i_3 
       (.I0(\axi_rdata[29]_i_6_n_0 ),
        .I1(\axi_rdata[29]_i_7_n_0 ),
        .O(\axi_rdata_reg[29]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[2] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [2]),
        .Q(s_axi_ctrl_status_rdata[2]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[2]_i_1 
       (.I0(\axi_rdata_reg[2]_i_2_n_0 ),
        .I1(\axi_rdata_reg[2]_i_3_n_0 ),
        .O(\read_data[0]_14 [2]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[2]_i_2 
       (.I0(\axi_rdata[2]_i_4_n_0 ),
        .I1(\axi_rdata[2]_i_5_n_0 ),
        .O(\axi_rdata_reg[2]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[2]_i_3 
       (.I0(\axi_rdata[2]_i_6_n_0 ),
        .I1(\axi_rdata[2]_i_7_n_0 ),
        .O(\axi_rdata_reg[2]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[30] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [30]),
        .Q(s_axi_ctrl_status_rdata[30]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[30]_i_1 
       (.I0(\axi_rdata_reg[30]_i_2_n_0 ),
        .I1(\axi_rdata_reg[30]_i_3_n_0 ),
        .O(\read_data[0]_14 [30]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[30]_i_2 
       (.I0(\axi_rdata[30]_i_4_n_0 ),
        .I1(\axi_rdata[30]_i_5_n_0 ),
        .O(\axi_rdata_reg[30]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[30]_i_3 
       (.I0(\axi_rdata[30]_i_6_n_0 ),
        .I1(\axi_rdata[30]_i_7_n_0 ),
        .O(\axi_rdata_reg[30]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[31] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [31]),
        .Q(s_axi_ctrl_status_rdata[31]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[31]_i_2 
       (.I0(\axi_rdata_reg[31]_i_3_n_0 ),
        .I1(\axi_rdata_reg[31]_i_4_n_0 ),
        .O(\read_data[0]_14 [31]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[31]_i_3 
       (.I0(\axi_rdata[31]_i_5_n_0 ),
        .I1(\axi_rdata[31]_i_6_n_0 ),
        .O(\axi_rdata_reg[31]_i_3_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[31]_i_4 
       (.I0(\axi_rdata[31]_i_7_n_0 ),
        .I1(\axi_rdata[31]_i_8_n_0 ),
        .O(\axi_rdata_reg[31]_i_4_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[3] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [3]),
        .Q(s_axi_ctrl_status_rdata[3]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[3]_i_1 
       (.I0(\axi_rdata_reg[3]_i_2_n_0 ),
        .I1(\axi_rdata_reg[3]_i_3_n_0 ),
        .O(\read_data[0]_14 [3]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[3]_i_2 
       (.I0(\axi_rdata[3]_i_4_n_0 ),
        .I1(\axi_rdata[3]_i_5_n_0 ),
        .O(\axi_rdata_reg[3]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[3]_i_3 
       (.I0(\axi_rdata[3]_i_6_n_0 ),
        .I1(\axi_rdata[3]_i_7_n_0 ),
        .O(\axi_rdata_reg[3]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[4] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [4]),
        .Q(s_axi_ctrl_status_rdata[4]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[4]_i_1 
       (.I0(\axi_rdata_reg[4]_i_2_n_0 ),
        .I1(\axi_rdata_reg[4]_i_3_n_0 ),
        .O(\read_data[0]_14 [4]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[4]_i_2 
       (.I0(\axi_rdata[4]_i_4_n_0 ),
        .I1(\axi_rdata[4]_i_5_n_0 ),
        .O(\axi_rdata_reg[4]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[4]_i_3 
       (.I0(\axi_rdata[4]_i_6_n_0 ),
        .I1(\axi_rdata[4]_i_7_n_0 ),
        .O(\axi_rdata_reg[4]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[5] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [5]),
        .Q(s_axi_ctrl_status_rdata[5]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[5]_i_1 
       (.I0(\axi_rdata_reg[5]_i_2_n_0 ),
        .I1(\axi_rdata_reg[5]_i_3_n_0 ),
        .O(\read_data[0]_14 [5]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[5]_i_2 
       (.I0(\axi_rdata[5]_i_4_n_0 ),
        .I1(\axi_rdata[5]_i_5_n_0 ),
        .O(\axi_rdata_reg[5]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[5]_i_3 
       (.I0(\axi_rdata[5]_i_6_n_0 ),
        .I1(\axi_rdata[5]_i_7_n_0 ),
        .O(\axi_rdata_reg[5]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[6] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [6]),
        .Q(s_axi_ctrl_status_rdata[6]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[6]_i_1 
       (.I0(\axi_rdata_reg[6]_i_2_n_0 ),
        .I1(\axi_rdata_reg[6]_i_3_n_0 ),
        .O(\read_data[0]_14 [6]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[6]_i_2 
       (.I0(\axi_rdata[6]_i_4_n_0 ),
        .I1(\axi_rdata[6]_i_5_n_0 ),
        .O(\axi_rdata_reg[6]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[6]_i_3 
       (.I0(\axi_rdata[6]_i_6_n_0 ),
        .I1(\axi_rdata[6]_i_7_n_0 ),
        .O(\axi_rdata_reg[6]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[7] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [7]),
        .Q(s_axi_ctrl_status_rdata[7]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[7]_i_1 
       (.I0(\axi_rdata_reg[7]_i_2_n_0 ),
        .I1(\axi_rdata_reg[7]_i_3_n_0 ),
        .O(\read_data[0]_14 [7]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[7]_i_2 
       (.I0(\axi_rdata[7]_i_4_n_0 ),
        .I1(\axi_rdata[7]_i_5_n_0 ),
        .O(\axi_rdata_reg[7]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[7]_i_3 
       (.I0(\axi_rdata[7]_i_6_n_0 ),
        .I1(\axi_rdata[7]_i_7_n_0 ),
        .O(\axi_rdata_reg[7]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[8] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [8]),
        .Q(s_axi_ctrl_status_rdata[8]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[8]_i_1 
       (.I0(\axi_rdata_reg[8]_i_2_n_0 ),
        .I1(\axi_rdata_reg[8]_i_3_n_0 ),
        .O(\read_data[0]_14 [8]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[8]_i_2 
       (.I0(\axi_rdata[8]_i_4_n_0 ),
        .I1(\axi_rdata[8]_i_5_n_0 ),
        .O(\axi_rdata_reg[8]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[8]_i_3 
       (.I0(\axi_rdata[8]_i_6_n_0 ),
        .I1(\axi_rdata[8]_i_7_n_0 ),
        .O(\axi_rdata_reg[8]_i_3_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[9] 
       (.C(clk),
        .CE(\axi_rdata[31]_i_1_n_0 ),
        .D(\read_data[0]_14 [9]),
        .Q(s_axi_ctrl_status_rdata[9]),
        .R(aresetn_0));
  MUXF8 \axi_rdata_reg[9]_i_1 
       (.I0(\axi_rdata_reg[9]_i_2_n_0 ),
        .I1(\axi_rdata_reg[9]_i_3_n_0 ),
        .O(\read_data[0]_14 [9]),
        .S(sel0[3]));
  MUXF7 \axi_rdata_reg[9]_i_2 
       (.I0(\axi_rdata[9]_i_4_n_0 ),
        .I1(\axi_rdata[9]_i_5_n_0 ),
        .O(\axi_rdata_reg[9]_i_2_n_0 ),
        .S(sel0[2]));
  MUXF7 \axi_rdata_reg[9]_i_3 
       (.I0(\axi_rdata[9]_i_6_n_0 ),
        .I1(\axi_rdata[9]_i_7_n_0 ),
        .O(\axi_rdata_reg[9]_i_3_n_0 ),
        .S(sel0[2]));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s_axi_ctrl_status_arvalid),
        .I1(s_axi_ctrl_status_arready),
        .I2(s_axi_ctrl_status_rvalid),
        .I3(s_axi_ctrl_status_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s_axi_ctrl_status_rvalid),
        .R(aresetn_0));
  LUT3 #(
    .INIT(8'h08)) 
    axi_wready_i_1
       (.I0(s_axi_ctrl_status_wvalid),
        .I1(s_axi_ctrl_status_awvalid),
        .I2(s_axi_ctrl_status_wready),
        .O(axi_wready_i_1_n_0));
  FDRE axi_wready_reg
       (.C(clk),
        .CE(1'b1),
        .D(axi_wready_i_1_n_0),
        .Q(s_axi_ctrl_status_wready),
        .R(aresetn_0));
  LUT4 #(
    .INIT(16'h0800)) 
    b_ram_data_reg_i_1
       (.I0(\b_ram_sel_reg[7] [7]),
        .I1(\state_reg[0]_0 ),
        .I2(\state_reg[1]_0 ),
        .I3(valid_input),
        .O(b_ram_data_reg_0));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_10
       (.I0(aresetn),
        .I1(data_in[7]),
        .O(data_in_w[7]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_11
       (.I0(aresetn),
        .I1(data_in[6]),
        .O(data_in_w[6]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_12
       (.I0(aresetn),
        .I1(data_in[5]),
        .O(data_in_w[5]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_13
       (.I0(aresetn),
        .I1(data_in[4]),
        .O(data_in_w[4]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_14
       (.I0(aresetn),
        .I1(data_in[3]),
        .O(data_in_w[3]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_15
       (.I0(aresetn),
        .I1(data_in[2]),
        .O(data_in_w[2]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_16
       (.I0(aresetn),
        .I1(data_in[1]),
        .O(data_in_w[1]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_17
       (.I0(aresetn),
        .I1(data_in[0]),
        .O(data_in_w[0]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_18
       (.I0(aresetn),
        .I1(data_in[31]),
        .O(data_in_w[31]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_19
       (.I0(aresetn),
        .I1(data_in[30]),
        .O(data_in_w[30]));
  LUT4 #(
    .INIT(16'h0800)) 
    b_ram_data_reg_i_1__0
       (.I0(\b_ram_sel_reg[7] [6]),
        .I1(\state_reg[0]_0 ),
        .I2(\state_reg[1]_0 ),
        .I3(valid_input),
        .O(WEBWE));
  LUT4 #(
    .INIT(16'h0800)) 
    b_ram_data_reg_i_1__1
       (.I0(\b_ram_sel_reg[7] [5]),
        .I1(\state_reg[0]_0 ),
        .I2(\state_reg[1]_0 ),
        .I3(valid_input),
        .O(\count_i_reg[6] ));
  LUT4 #(
    .INIT(16'h0800)) 
    b_ram_data_reg_i_1__2
       (.I0(\b_ram_sel_reg[7] [4]),
        .I1(\state_reg[0]_0 ),
        .I2(\state_reg[1]_0 ),
        .I3(valid_input),
        .O(\count_i_reg[6]_0 ));
  LUT4 #(
    .INIT(16'h0800)) 
    b_ram_data_reg_i_1__3
       (.I0(\b_ram_sel_reg[7] [3]),
        .I1(\state_reg[0]_0 ),
        .I2(\state_reg[1]_0 ),
        .I3(valid_input),
        .O(\count_i_reg[6]_1 ));
  LUT4 #(
    .INIT(16'h0800)) 
    b_ram_data_reg_i_1__4
       (.I0(\b_ram_sel_reg[7] [2]),
        .I1(\state_reg[0]_0 ),
        .I2(\state_reg[1]_0 ),
        .I3(valid_input),
        .O(\count_i_reg[6]_2 ));
  LUT4 #(
    .INIT(16'h0800)) 
    b_ram_data_reg_i_1__5
       (.I0(\b_ram_sel_reg[7] [1]),
        .I1(\state_reg[0]_0 ),
        .I2(\state_reg[1]_0 ),
        .I3(valid_input),
        .O(\count_i_reg[6]_3 ));
  LUT4 #(
    .INIT(16'hD555)) 
    b_ram_data_reg_i_1__6
       (.I0(aresetn),
        .I1(s_axis_tvalid),
        .I2(initialized),
        .I3(cpu2emsc_register[12]),
        .O(b_ram_data_reg));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_2
       (.I0(aresetn),
        .I1(data_in[15]),
        .O(data_in_w[15]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_20
       (.I0(aresetn),
        .I1(data_in[29]),
        .O(data_in_w[29]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_21
       (.I0(aresetn),
        .I1(data_in[28]),
        .O(data_in_w[28]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_22
       (.I0(aresetn),
        .I1(data_in[27]),
        .O(data_in_w[27]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_23
       (.I0(aresetn),
        .I1(data_in[26]),
        .O(data_in_w[26]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_24
       (.I0(aresetn),
        .I1(data_in[25]),
        .O(data_in_w[25]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_25
       (.I0(aresetn),
        .I1(data_in[24]),
        .O(data_in_w[24]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_26
       (.I0(aresetn),
        .I1(data_in[23]),
        .O(data_in_w[23]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_27
       (.I0(aresetn),
        .I1(data_in[22]),
        .O(data_in_w[22]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_28
       (.I0(aresetn),
        .I1(data_in[21]),
        .O(data_in_w[21]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_29
       (.I0(aresetn),
        .I1(data_in[20]),
        .O(data_in_w[20]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_3
       (.I0(aresetn),
        .I1(data_in[14]),
        .O(data_in_w[14]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_30
       (.I0(aresetn),
        .I1(data_in[19]),
        .O(data_in_w[19]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_31
       (.I0(aresetn),
        .I1(data_in[18]),
        .O(data_in_w[18]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_32
       (.I0(aresetn),
        .I1(data_in[17]),
        .O(data_in_w[17]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_33
       (.I0(aresetn),
        .I1(data_in[16]),
        .O(data_in_w[16]));
  LUT5 #(
    .INIT(32'h0C080008)) 
    b_ram_data_reg_i_34
       (.I0(cpu2emsc_register[13]),
        .I1(valid_input),
        .I2(\state_reg[1]_0 ),
        .I3(\state_reg[0]_0 ),
        .I4(\b_ram_sel_reg[7] [0]),
        .O(\count_i_reg[6]_4 ));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_4
       (.I0(aresetn),
        .I1(data_in[13]),
        .O(data_in_w[13]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_5
       (.I0(aresetn),
        .I1(data_in[12]),
        .O(data_in_w[12]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_6
       (.I0(aresetn),
        .I1(data_in[11]),
        .O(data_in_w[11]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_7
       (.I0(aresetn),
        .I1(data_in[10]),
        .O(data_in_w[10]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_8
       (.I0(aresetn),
        .I1(data_in[9]),
        .O(data_in_w[9]));
  LUT2 #(
    .INIT(4'h8)) 
    b_ram_data_reg_i_9
       (.I0(aresetn),
        .I1(data_in[8]),
        .O(data_in_w[8]));
  LUT6 #(
    .INIT(64'h003A0000000A0000)) 
    \b_ram_sel[7]_i_1 
       (.I0(cpu2emsc_register[13]),
        .I1(\b_ram_sel_reg[7] [7]),
        .I2(\state_reg[0]_0 ),
        .I3(\state_reg[1]_0 ),
        .I4(valid_input),
        .I5(\slv_regs_reg[0][11]_10 ),
        .O(\b_ram_sel_reg[0] ));
  LUT2 #(
    .INIT(4'h1)) 
    counter1_carry__0_i_1
       (.I0(Q[11]),
        .I1(Q[10]),
        .O(\counter_reg[1] [1]));
  LUT2 #(
    .INIT(4'h1)) 
    counter1_carry__0_i_2
       (.I0(Q[9]),
        .I1(Q[8]),
        .O(\counter_reg[1] [0]));
  LUT6 #(
    .INIT(64'h0000000044144444)) 
    counter1_carry_i_1
       (.I0(Q[7]),
        .I1(counter[6]),
        .I2(counter[4]),
        .I3(\counter_reg[2]_6 ),
        .I4(counter[5]),
        .I5(Q[6]),
        .O(DI[3]));
  LUT5 #(
    .INIT(32'h5104D345)) 
    counter1_carry_i_2
       (.I0(Q[5]),
        .I1(counter[4]),
        .I2(\counter_reg[2]_6 ),
        .I3(counter[5]),
        .I4(Q[4]),
        .O(DI[2]));
  LUT6 #(
    .INIT(64'h155540003DDD5444)) 
    counter1_carry_i_3
       (.I0(Q[3]),
        .I1(counter[2]),
        .I2(counter[0]),
        .I3(counter[1]),
        .I4(counter[3]),
        .I5(Q[2]),
        .O(DI[1]));
  LUT4 #(
    .INIT(16'h141D)) 
    counter1_carry_i_4
       (.I0(Q[1]),
        .I1(counter[1]),
        .I2(counter[0]),
        .I3(Q[0]),
        .O(DI[0]));
  LUT6 #(
    .INIT(64'h4414444411411111)) 
    counter1_carry_i_5
       (.I0(Q[7]),
        .I1(counter[6]),
        .I2(counter[4]),
        .I3(\counter_reg[2]_6 ),
        .I4(counter[5]),
        .I5(Q[6]),
        .O(S[3]));
  LUT5 #(
    .INIT(32'h90090960)) 
    counter1_carry_i_6
       (.I0(counter[5]),
        .I1(Q[5]),
        .I2(counter[4]),
        .I3(\counter_reg[2]_6 ),
        .I4(Q[4]),
        .O(S[2]));
  LUT6 #(
    .INIT(64'h0990909060090909)) 
    counter1_carry_i_7
       (.I0(counter[3]),
        .I1(Q[3]),
        .I2(counter[2]),
        .I3(counter[0]),
        .I4(counter[1]),
        .I5(Q[2]),
        .O(S[1]));
  LUT4 #(
    .INIT(16'h0960)) 
    counter1_carry_i_8
       (.I0(counter[1]),
        .I1(Q[1]),
        .I2(counter[0]),
        .I3(Q[0]),
        .O(S[0]));
  LUT6 #(
    .INIT(64'h4040FFFF40FF0000)) 
    \counter[0]_i_1 
       (.I0(\state_reg[0]_0 ),
        .I1(valid_input),
        .I2(cpu2emsc_register[13]),
        .I3(\counter[0]_i_2_n_0 ),
        .I4(\counter_reg[1]_10 ),
        .I5(counter[0]),
        .O(\counter_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'hA2FFAAFF)) 
    \counter[0]_i_2 
       (.I0(\slv_regs_reg[0][11]_10 ),
        .I1(valid_input),
        .I2(\state_reg[1]_0 ),
        .I3(\state_reg[0]_0 ),
        .I4(\b_ram_sel_reg[7] [7]),
        .O(\counter[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'h1555)) 
    \counter[1]_i_2 
       (.I0(dp_extend_end),
        .I1(initialized),
        .I2(cpu2emsc_register[12]),
        .I3(in_stream_handshake_delay),
        .O(\counter_reg[1]_0 ));
  LUT6 #(
    .INIT(64'h040000000F000F00)) 
    \counter[6]_i_1 
       (.I0(\b_ram_sel_reg[7] [7]),
        .I1(\slv_regs_reg[0][11]_10 ),
        .I2(\state_reg[1]_0 ),
        .I3(aresetn),
        .I4(valid_input),
        .I5(\state_reg[0]_0 ),
        .O(\counter_reg[1]_13 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'h00D0)) 
    \counter[6]_i_2 
       (.I0(\state_reg[0]_0 ),
        .I1(valid_input),
        .I2(aresetn),
        .I3(\state_reg[1]_0 ),
        .O(\counter_reg[1]_10 ));
  LUT6 #(
    .INIT(64'hEAAA0000FFFFFFFF)) 
    \counter[8]_i_1 
       (.I0(dp_extend_end),
        .I1(initialized),
        .I2(cpu2emsc_register[12]),
        .I3(in_stream_handshake_delay),
        .I4(CO),
        .I5(aresetn),
        .O(SR));
  LUT6 #(
    .INIT(64'hEAAA0000FFFFFFFF)) 
    \counter[8]_i_1__0 
       (.I0(dp_extend_end),
        .I1(initialized),
        .I2(cpu2emsc_register[12]),
        .I3(in_stream_handshake_delay),
        .I4(\slv_regs_reg[0][11]_0 ),
        .I5(aresetn),
        .O(\counter_reg[2] ));
  LUT6 #(
    .INIT(64'hEAAA0000FFFFFFFF)) 
    \counter[8]_i_1__1 
       (.I0(dp_extend_end),
        .I1(initialized),
        .I2(cpu2emsc_register[12]),
        .I3(in_stream_handshake_delay),
        .I4(\slv_regs_reg[0][11]_1 ),
        .I5(aresetn),
        .O(\counter_reg[2]_0 ));
  LUT6 #(
    .INIT(64'hEAAA0000FFFFFFFF)) 
    \counter[8]_i_1__2 
       (.I0(dp_extend_end),
        .I1(initialized),
        .I2(cpu2emsc_register[12]),
        .I3(in_stream_handshake_delay),
        .I4(\slv_regs_reg[0][11]_2 ),
        .I5(aresetn),
        .O(\counter_reg[2]_1 ));
  LUT6 #(
    .INIT(64'hEAAA0000FFFFFFFF)) 
    \counter[8]_i_1__3 
       (.I0(dp_extend_end),
        .I1(initialized),
        .I2(cpu2emsc_register[12]),
        .I3(in_stream_handshake_delay),
        .I4(\slv_regs_reg[0][11]_3 ),
        .I5(aresetn),
        .O(\counter_reg[2]_2 ));
  LUT6 #(
    .INIT(64'hEAAA0000FFFFFFFF)) 
    \counter[8]_i_1__4 
       (.I0(dp_extend_end),
        .I1(initialized),
        .I2(cpu2emsc_register[12]),
        .I3(in_stream_handshake_delay),
        .I4(\slv_regs_reg[0][11]_4 ),
        .I5(aresetn),
        .O(\counter_reg[2]_3 ));
  LUT6 #(
    .INIT(64'hEAAA0000FFFFFFFF)) 
    \counter[8]_i_1__5 
       (.I0(dp_extend_end),
        .I1(initialized),
        .I2(cpu2emsc_register[12]),
        .I3(in_stream_handshake_delay),
        .I4(\slv_regs_reg[0][11]_5 ),
        .I5(aresetn),
        .O(\counter_reg[2]_4 ));
  LUT6 #(
    .INIT(64'hEAAA0000FFFFFFFF)) 
    \counter[8]_i_1__6 
       (.I0(dp_extend_end),
        .I1(initialized),
        .I2(cpu2emsc_register[12]),
        .I3(in_stream_handshake_delay),
        .I4(\slv_regs_reg[0][11]_6 ),
        .I5(aresetn),
        .O(\counter_reg[2]_5 ));
  LUT4 #(
    .INIT(16'hFF80)) 
    \counter[8]_i_2 
       (.I0(in_stream_handshake_delay),
        .I1(cpu2emsc_register[12]),
        .I2(initialized),
        .I3(dp_extend_end),
        .O(dp_enable));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_1
       (.I0(\slv_regs_reg[0][11]_8 ),
        .O(\read_address_reg[0]_2 ));
  LUT4 #(
    .INIT(16'hB222)) 
    i__carry_i_4
       (.I0(\read_address_reg[1] [1]),
        .I1(O),
        .I2(Q[0]),
        .I3(\read_address_reg[1] [0]),
        .O(\read_address_reg[0]_4 ));
  LUT4 #(
    .INIT(16'h6006)) 
    i__carry_i_8
       (.I0(\read_address_reg[1] [0]),
        .I1(Q[0]),
        .I2(O),
        .I3(\read_address_reg[1] [1]),
        .O(\read_address_reg[0]_3 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    in_stream_ready_i_1
       (.I0(s_axis_tready),
        .I1(cpu2emsc_register[12]),
        .I2(initialized),
        .O(in_stream_ready_reg));
  LUT6 #(
    .INIT(64'hFFFFFFFF08000000)) 
    initialized_i_1
       (.I0(\slv_regs_reg[0][11]_10 ),
        .I1(valid_input),
        .I2(\state_reg[1]_0 ),
        .I3(\state_reg[0]_0 ),
        .I4(\b_ram_sel_reg[7] [7]),
        .I5(initialized),
        .O(initialized_reg));
  LUT6 #(
    .INIT(64'h0400000000000000)) 
    \m_axis_tdata[63]_i_3 
       (.I0(\counter_reg[1]_0 ),
        .I1(\slv_regs_reg[0][11]_4 ),
        .I2(\slv_regs_reg[0][11]_7 ),
        .I3(\slv_regs_reg[0][11]_5 ),
        .I4(\slv_regs_reg[0][11]_3 ),
        .I5(\slv_regs_reg[0][11]_2 ),
        .O(p_rdy));
  LUT6 #(
    .INIT(64'h1010101000101010)) 
    \read_address[6]_i_1 
       (.I0(\read_address[6]_i_3_n_0 ),
        .I1(\state_reg[0]_0 ),
        .I2(aresetn),
        .I3(cpu2emsc_register[13]),
        .I4(valid_input),
        .I5(\state_reg[1]_0 ),
        .O(\read_address_reg[0]_5 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \read_address[6]_i_3 
       (.I0(cpu2emsc_register[12]),
        .I1(initialized),
        .I2(s_axis_tvalid),
        .O(\read_address[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0080000000000000)) 
    \res_mem[383]_i_1 
       (.I0(\slv_regs_reg[0][11]_2 ),
        .I1(\slv_regs_reg[0][11]_3 ),
        .I2(\slv_regs_reg[0][11]_5 ),
        .I3(\slv_regs_reg[0][11]_7 ),
        .I4(\res_mem[383]_i_3_n_0 ),
        .I5(aresetn),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'hAAAA8000)) 
    \res_mem[383]_i_3 
       (.I0(\slv_regs_reg[0][11]_4 ),
        .I1(in_stream_handshake_delay),
        .I2(cpu2emsc_register[12]),
        .I3(initialized),
        .I4(dp_extend_end),
        .O(\res_mem[383]_i_3_n_0 ));
  FDRE \slv_regs_reg[0][0] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [0]),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(Q[0]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][10] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [1]),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(Q[10]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][11] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [1]),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(Q[11]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][12] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [1]),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(cpu2emsc_register[12]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][13] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [1]),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(cpu2emsc_register[13]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][14] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [1]),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg_n_0_[0][14] ),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][15] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [1]),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg_n_0_[0][15] ),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][16] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [2]),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg_n_0_[0][16] ),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][17] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [2]),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg_n_0_[0][17] ),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][18] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [2]),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg_n_0_[0][18] ),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][19] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [2]),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg_n_0_[0][19] ),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][1] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [0]),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(Q[1]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][20] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [2]),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg_n_0_[0][20] ),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][21] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [2]),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg_n_0_[0][21] ),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][22] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [2]),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg_n_0_[0][22] ),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][23] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [2]),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg_n_0_[0][23] ),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][24] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [3]),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg_n_0_[0][24] ),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][25] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [3]),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg_n_0_[0][25] ),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][26] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [3]),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg_n_0_[0][26] ),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][27] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [3]),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg_n_0_[0][27] ),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][28] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [3]),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg_n_0_[0][28] ),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][29] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [3]),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg_n_0_[0][29] ),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][2] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [0]),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(Q[2]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][30] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [3]),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg_n_0_[0][30] ),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][31] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [3]),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg_n_0_[0][31] ),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][3] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [0]),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(Q[3]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][4] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [0]),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(Q[4]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][5] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [0]),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(Q[5]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][6] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [0]),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(Q[6]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][7] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [0]),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(Q[7]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][8] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [1]),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(Q[8]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[0][9] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_0 [1]),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(Q[9]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][0] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [0]),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[10]_8 [0]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][10] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [1]),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[10]_8 [10]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][11] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [1]),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[10]_8 [11]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][12] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [1]),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[10]_8 [12]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][13] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [1]),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[10]_8 [13]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][14] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [1]),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[10]_8 [14]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][15] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [1]),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[10]_8 [15]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][16] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [2]),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[10]_8 [16]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][17] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [2]),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[10]_8 [17]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][18] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [2]),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[10]_8 [18]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][19] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [2]),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[10]_8 [19]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][1] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [0]),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[10]_8 [1]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][20] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [2]),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[10]_8 [20]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][21] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [2]),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[10]_8 [21]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][22] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [2]),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[10]_8 [22]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][23] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [2]),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[10]_8 [23]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][24] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [3]),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[10]_8 [24]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][25] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [3]),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[10]_8 [25]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][26] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [3]),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[10]_8 [26]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][27] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [3]),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[10]_8 [27]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][28] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [3]),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[10]_8 [28]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][29] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [3]),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[10]_8 [29]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][2] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [0]),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[10]_8 [2]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][30] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [3]),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[10]_8 [30]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][31] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [3]),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[10]_8 [31]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][3] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [0]),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[10]_8 [3]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][4] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [0]),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[10]_8 [4]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][5] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [0]),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[10]_8 [5]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][6] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [0]),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[10]_8 [6]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][7] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [0]),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[10]_8 [7]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][8] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [1]),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[10]_8 [8]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[10][9] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_2 [1]),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[10]_8 [9]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][0] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [0]),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[11]_9 [0]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][10] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [1]),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[11]_9 [10]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][11] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [1]),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[11]_9 [11]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][12] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [1]),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[11]_9 [12]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][13] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [1]),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[11]_9 [13]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][14] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [1]),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[11]_9 [14]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][15] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [1]),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[11]_9 [15]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][16] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [2]),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[11]_9 [16]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][17] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [2]),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[11]_9 [17]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][18] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [2]),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[11]_9 [18]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][19] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [2]),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[11]_9 [19]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][1] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [0]),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[11]_9 [1]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][20] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [2]),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[11]_9 [20]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][21] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [2]),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[11]_9 [21]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][22] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [2]),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[11]_9 [22]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][23] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [2]),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[11]_9 [23]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][24] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [3]),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[11]_9 [24]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][25] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [3]),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[11]_9 [25]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][26] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [3]),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[11]_9 [26]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][27] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [3]),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[11]_9 [27]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][28] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [3]),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[11]_9 [28]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][29] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [3]),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[11]_9 [29]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][2] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [0]),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[11]_9 [2]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][30] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [3]),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[11]_9 [30]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][31] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [3]),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[11]_9 [31]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][3] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [0]),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[11]_9 [3]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][4] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [0]),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[11]_9 [4]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][5] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [0]),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[11]_9 [5]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][6] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [0]),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[11]_9 [6]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][7] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [0]),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[11]_9 [7]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][8] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [1]),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[11]_9 [8]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[11][9] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_2 [1]),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[11]_9 [9]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][0] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [0]),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[12]_10 [0]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][10] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [1]),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[12]_10 [10]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][11] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [1]),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[12]_10 [11]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][12] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [1]),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[12]_10 [12]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][13] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [1]),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[12]_10 [13]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][14] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [1]),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[12]_10 [14]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][15] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [1]),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[12]_10 [15]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][16] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [2]),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[12]_10 [16]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][17] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [2]),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[12]_10 [17]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][18] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [2]),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[12]_10 [18]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][19] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [2]),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[12]_10 [19]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][1] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [0]),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[12]_10 [1]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][20] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [2]),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[12]_10 [20]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][21] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [2]),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[12]_10 [21]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][22] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [2]),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[12]_10 [22]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][23] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [2]),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[12]_10 [23]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][24] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [3]),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[12]_10 [24]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][25] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [3]),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[12]_10 [25]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][26] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [3]),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[12]_10 [26]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][27] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [3]),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[12]_10 [27]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][28] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [3]),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[12]_10 [28]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][29] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [3]),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[12]_10 [29]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][2] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [0]),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[12]_10 [2]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][30] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [3]),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[12]_10 [30]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][31] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [3]),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[12]_10 [31]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][3] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [0]),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[12]_10 [3]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][4] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [0]),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[12]_10 [4]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][5] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [0]),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[12]_10 [5]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][6] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [0]),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[12]_10 [6]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][7] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [0]),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[12]_10 [7]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][8] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [1]),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[12]_10 [8]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[12][9] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_3 [1]),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[12]_10 [9]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][0] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [0]),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[13]_11 [0]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][10] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [1]),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[13]_11 [10]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][11] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [1]),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[13]_11 [11]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][12] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [1]),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[13]_11 [12]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][13] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [1]),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[13]_11 [13]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][14] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [1]),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[13]_11 [14]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][15] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [1]),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[13]_11 [15]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][16] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [2]),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[13]_11 [16]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][17] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [2]),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[13]_11 [17]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][18] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [2]),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[13]_11 [18]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][19] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [2]),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[13]_11 [19]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][1] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [0]),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[13]_11 [1]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][20] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [2]),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[13]_11 [20]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][21] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [2]),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[13]_11 [21]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][22] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [2]),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[13]_11 [22]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][23] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [2]),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[13]_11 [23]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][24] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [3]),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[13]_11 [24]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][25] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [3]),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[13]_11 [25]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][26] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [3]),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[13]_11 [26]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][27] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [3]),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[13]_11 [27]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][28] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [3]),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[13]_11 [28]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][29] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [3]),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[13]_11 [29]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][2] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [0]),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[13]_11 [2]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][30] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [3]),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[13]_11 [30]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][31] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [3]),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[13]_11 [31]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][3] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [0]),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[13]_11 [3]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][4] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [0]),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[13]_11 [4]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][5] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [0]),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[13]_11 [5]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][6] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [0]),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[13]_11 [6]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][7] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [0]),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[13]_11 [7]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][8] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [1]),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[13]_11 [8]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[13][9] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_2 [1]),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[13]_11 [9]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][0] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [0]),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[14]_12 [0]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][10] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [1]),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[14]_12 [10]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][11] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [1]),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[14]_12 [11]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][12] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [1]),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[14]_12 [12]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][13] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [1]),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[14]_12 [13]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][14] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [1]),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[14]_12 [14]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][15] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [1]),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[14]_12 [15]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][16] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [2]),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[14]_12 [16]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][17] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [2]),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[14]_12 [17]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][18] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [2]),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[14]_12 [18]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][19] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [2]),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[14]_12 [19]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][1] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [0]),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[14]_12 [1]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][20] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [2]),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[14]_12 [20]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][21] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [2]),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[14]_12 [21]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][22] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [2]),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[14]_12 [22]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][23] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [2]),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[14]_12 [23]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][24] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [3]),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[14]_12 [24]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][25] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [3]),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[14]_12 [25]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][26] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [3]),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[14]_12 [26]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][27] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [3]),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[14]_12 [27]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][28] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [3]),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[14]_12 [28]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][29] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [3]),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[14]_12 [29]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][2] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [0]),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[14]_12 [2]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][30] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [3]),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[14]_12 [30]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][31] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [3]),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[14]_12 [31]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][3] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [0]),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[14]_12 [3]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][4] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [0]),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[14]_12 [4]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][5] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [0]),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[14]_12 [5]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][6] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [0]),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[14]_12 [6]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][7] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [0]),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[14]_12 [7]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][8] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [1]),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[14]_12 [8]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[14][9] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_3 [1]),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[14]_12 [9]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][0] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [0]),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[15]_13 [0]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][10] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [1]),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[15]_13 [10]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][11] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [1]),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[15]_13 [11]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][12] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [1]),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[15]_13 [12]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][13] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [1]),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[15]_13 [13]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][14] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [1]),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[15]_13 [14]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][15] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [1]),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[15]_13 [15]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][16] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [2]),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[15]_13 [16]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][17] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [2]),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[15]_13 [17]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][18] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [2]),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[15]_13 [18]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][19] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [2]),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[15]_13 [19]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][1] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [0]),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[15]_13 [1]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][20] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [2]),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[15]_13 [20]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][21] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [2]),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[15]_13 [21]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][22] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [2]),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[15]_13 [22]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][23] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [2]),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[15]_13 [23]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][24] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [3]),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[15]_13 [24]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][25] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [3]),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[15]_13 [25]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][26] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [3]),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[15]_13 [26]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][27] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [3]),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[15]_13 [27]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][28] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [3]),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[15]_13 [28]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][29] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [3]),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[15]_13 [29]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][2] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [0]),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[15]_13 [2]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][30] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [3]),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[15]_13 [30]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][31] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [3]),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[15]_13 [31]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][3] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [0]),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[15]_13 [3]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][4] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [0]),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[15]_13 [4]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][5] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [0]),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[15]_13 [5]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][6] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [0]),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[15]_13 [6]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][7] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [0]),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[15]_13 [7]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][8] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [1]),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[15]_13 [8]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[15][9] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_4 [1]),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[15]_13 [9]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][0] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [0]),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(data_in[0]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][10] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [1]),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(data_in[10]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][11] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [1]),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(data_in[11]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][12] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [1]),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(data_in[12]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][13] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [1]),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(data_in[13]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][14] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [1]),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(data_in[14]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][15] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [1]),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(data_in[15]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][16] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [2]),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(data_in[16]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][17] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [2]),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(data_in[17]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][18] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [2]),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(data_in[18]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][19] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [2]),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(data_in[19]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][1] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [0]),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(data_in[1]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][20] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [2]),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(data_in[20]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][21] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [2]),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(data_in[21]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][22] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [2]),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(data_in[22]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][23] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [2]),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(data_in[23]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][24] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [3]),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(data_in[24]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][25] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [3]),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(data_in[25]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][26] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [3]),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(data_in[26]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][27] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [3]),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(data_in[27]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][28] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [3]),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(data_in[28]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][29] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [3]),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(data_in[29]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][2] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [0]),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(data_in[2]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][30] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [3]),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(data_in[30]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][31] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [3]),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(data_in[31]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][3] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [0]),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(data_in[3]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][4] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [0]),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(data_in[4]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][5] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [0]),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(data_in[5]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][6] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [0]),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(data_in[6]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][7] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [0]),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(data_in[7]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][8] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [1]),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(data_in[8]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[1][9] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_0 [1]),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(data_in[9]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][0] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [0]),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[2]_0 [0]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][10] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [1]),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[2]_0 [10]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][11] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [1]),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[2]_0 [11]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][12] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [1]),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[2]_0 [12]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][13] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [1]),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[2]_0 [13]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][14] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [1]),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[2]_0 [14]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][15] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [1]),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[2]_0 [15]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][16] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [2]),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[2]_0 [16]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][17] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [2]),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[2]_0 [17]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][18] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [2]),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[2]_0 [18]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][19] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [2]),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[2]_0 [19]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][1] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [0]),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[2]_0 [1]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][20] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [2]),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[2]_0 [20]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][21] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [2]),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[2]_0 [21]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][22] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [2]),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[2]_0 [22]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][23] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [2]),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[2]_0 [23]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][24] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [3]),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[2]_0 [24]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][25] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [3]),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[2]_0 [25]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][26] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [3]),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[2]_0 [26]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][27] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [3]),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[2]_0 [27]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][28] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [3]),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[2]_0 [28]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][29] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [3]),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[2]_0 [29]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][2] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [0]),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[2]_0 [2]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][30] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [3]),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[2]_0 [30]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][31] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [3]),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[2]_0 [31]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][3] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [0]),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[2]_0 [3]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][4] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [0]),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[2]_0 [4]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][5] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [0]),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[2]_0 [5]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][6] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [0]),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[2]_0 [6]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][7] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [0]),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[2]_0 [7]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][8] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [1]),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[2]_0 [8]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[2][9] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_0 [1]),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[2]_0 [9]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][0] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [0]),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[3]_1 [0]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][10] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [1]),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[3]_1 [10]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][11] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [1]),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[3]_1 [11]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][12] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [1]),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[3]_1 [12]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][13] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [1]),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[3]_1 [13]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][14] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [1]),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[3]_1 [14]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][15] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [1]),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[3]_1 [15]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][16] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [2]),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[3]_1 [16]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][17] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [2]),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[3]_1 [17]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][18] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [2]),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[3]_1 [18]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][19] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [2]),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[3]_1 [19]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][1] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [0]),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[3]_1 [1]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][20] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [2]),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[3]_1 [20]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][21] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [2]),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[3]_1 [21]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][22] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [2]),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[3]_1 [22]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][23] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [2]),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[3]_1 [23]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][24] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [3]),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[3]_1 [24]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][25] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [3]),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[3]_1 [25]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][26] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [3]),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[3]_1 [26]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][27] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [3]),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[3]_1 [27]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][28] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [3]),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[3]_1 [28]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][29] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [3]),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[3]_1 [29]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][2] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [0]),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[3]_1 [2]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][30] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [3]),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[3]_1 [30]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][31] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [3]),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[3]_1 [31]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][3] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [0]),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[3]_1 [3]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][4] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [0]),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[3]_1 [4]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][5] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [0]),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[3]_1 [5]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][6] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [0]),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[3]_1 [6]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][7] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [0]),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[3]_1 [7]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][8] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [1]),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[3]_1 [8]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[3][9] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_1 [1]),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[3]_1 [9]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][0] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [0]),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[4]_2 [0]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][10] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [1]),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[4]_2 [10]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][11] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [1]),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[4]_2 [11]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][12] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [1]),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[4]_2 [12]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][13] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [1]),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[4]_2 [13]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][14] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [1]),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[4]_2 [14]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][15] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [1]),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[4]_2 [15]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][16] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [2]),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[4]_2 [16]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][17] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [2]),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[4]_2 [17]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][18] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [2]),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[4]_2 [18]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][19] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [2]),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[4]_2 [19]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][1] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [0]),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[4]_2 [1]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][20] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [2]),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[4]_2 [20]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][21] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [2]),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[4]_2 [21]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][22] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [2]),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[4]_2 [22]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][23] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [2]),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[4]_2 [23]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][24] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [3]),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[4]_2 [24]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][25] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [3]),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[4]_2 [25]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][26] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [3]),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[4]_2 [26]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][27] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [3]),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[4]_2 [27]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][28] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [3]),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[4]_2 [28]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][29] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [3]),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[4]_2 [29]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][2] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [0]),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[4]_2 [2]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][30] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [3]),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[4]_2 [30]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][31] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [3]),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[4]_2 [31]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][3] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [0]),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[4]_2 [3]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][4] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [0]),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[4]_2 [4]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][5] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [0]),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[4]_2 [5]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][6] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [0]),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[4]_2 [6]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][7] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [0]),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[4]_2 [7]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][8] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [1]),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[4]_2 [8]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[4][9] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_0 [1]),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[4]_2 [9]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][0] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [0]),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[5]_3 [0]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][10] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [1]),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[5]_3 [10]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][11] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [1]),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[5]_3 [11]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][12] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [1]),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[5]_3 [12]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][13] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [1]),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[5]_3 [13]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][14] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [1]),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[5]_3 [14]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][15] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [1]),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[5]_3 [15]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][16] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [2]),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[5]_3 [16]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][17] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [2]),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[5]_3 [17]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][18] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [2]),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[5]_3 [18]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][19] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [2]),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[5]_3 [19]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][1] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [0]),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[5]_3 [1]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][20] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [2]),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[5]_3 [20]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][21] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [2]),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[5]_3 [21]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][22] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [2]),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[5]_3 [22]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][23] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [2]),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[5]_3 [23]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][24] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [3]),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[5]_3 [24]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][25] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [3]),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[5]_3 [25]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][26] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [3]),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[5]_3 [26]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][27] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [3]),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[5]_3 [27]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][28] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [3]),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[5]_3 [28]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][29] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [3]),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[5]_3 [29]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][2] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [0]),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[5]_3 [2]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][30] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [3]),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[5]_3 [30]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][31] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [3]),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[5]_3 [31]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][3] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [0]),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[5]_3 [3]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][4] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [0]),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[5]_3 [4]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][5] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [0]),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[5]_3 [5]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][6] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [0]),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[5]_3 [6]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][7] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [0]),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[5]_3 [7]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][8] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [1]),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[5]_3 [8]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[5][9] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_2 [1]),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[5]_3 [9]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][0] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [0]),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[6]_4 [0]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][10] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [1]),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[6]_4 [10]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][11] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [1]),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[6]_4 [11]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][12] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [1]),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[6]_4 [12]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][13] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [1]),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[6]_4 [13]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][14] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [1]),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[6]_4 [14]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][15] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [1]),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[6]_4 [15]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][16] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [2]),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[6]_4 [16]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][17] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [2]),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[6]_4 [17]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][18] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [2]),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[6]_4 [18]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][19] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [2]),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[6]_4 [19]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][1] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [0]),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[6]_4 [1]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][20] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [2]),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[6]_4 [20]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][21] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [2]),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[6]_4 [21]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][22] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [2]),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[6]_4 [22]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][23] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [2]),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[6]_4 [23]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][24] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [3]),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[6]_4 [24]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][25] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [3]),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[6]_4 [25]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][26] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [3]),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[6]_4 [26]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][27] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [3]),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[6]_4 [27]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][28] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [3]),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[6]_4 [28]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][29] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [3]),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[6]_4 [29]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][2] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [0]),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[6]_4 [2]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][30] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [3]),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[6]_4 [30]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][31] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [3]),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[6]_4 [31]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][3] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [0]),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[6]_4 [3]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][4] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [0]),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[6]_4 [4]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][5] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [0]),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[6]_4 [5]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][6] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [0]),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[6]_4 [6]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][7] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [0]),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[6]_4 [7]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][8] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [1]),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[6]_4 [8]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[6][9] 
       (.C(clk),
        .CE(\axi_awaddr_reg[3]_1 [1]),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[6]_4 [9]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][0] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [0]),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[7]_5 [0]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][10] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [1]),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[7]_5 [10]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][11] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [1]),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[7]_5 [11]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][12] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [1]),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[7]_5 [12]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][13] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [1]),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[7]_5 [13]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][14] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [1]),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[7]_5 [14]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][15] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [1]),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[7]_5 [15]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][16] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [2]),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[7]_5 [16]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][17] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [2]),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[7]_5 [17]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][18] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [2]),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[7]_5 [18]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][19] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [2]),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[7]_5 [19]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][1] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [0]),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[7]_5 [1]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][20] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [2]),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[7]_5 [20]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][21] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [2]),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[7]_5 [21]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][22] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [2]),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[7]_5 [22]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][23] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [2]),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[7]_5 [23]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][24] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [3]),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[7]_5 [24]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][25] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [3]),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[7]_5 [25]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][26] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [3]),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[7]_5 [26]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][27] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [3]),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[7]_5 [27]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][28] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [3]),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[7]_5 [28]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][29] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [3]),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[7]_5 [29]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][2] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [0]),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[7]_5 [2]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][30] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [3]),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[7]_5 [30]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][31] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [3]),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[7]_5 [31]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][3] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [0]),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[7]_5 [3]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][4] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [0]),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[7]_5 [4]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][5] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [0]),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[7]_5 [5]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][6] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [0]),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[7]_5 [6]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][7] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [0]),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[7]_5 [7]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][8] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [1]),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[7]_5 [8]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[7][9] 
       (.C(clk),
        .CE(\axi_awaddr_reg[4]_1 [1]),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[7]_5 [9]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][0] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [0]),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[8]_6 [0]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][10] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [1]),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[8]_6 [10]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][11] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [1]),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[8]_6 [11]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][12] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [1]),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[8]_6 [12]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][13] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [1]),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[8]_6 [13]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][14] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [1]),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[8]_6 [14]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][15] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [1]),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[8]_6 [15]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][16] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [2]),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[8]_6 [16]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][17] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [2]),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[8]_6 [17]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][18] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [2]),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[8]_6 [18]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][19] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [2]),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[8]_6 [19]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][1] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [0]),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[8]_6 [1]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][20] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [2]),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[8]_6 [20]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][21] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [2]),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[8]_6 [21]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][22] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [2]),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[8]_6 [22]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][23] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [2]),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[8]_6 [23]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][24] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [3]),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[8]_6 [24]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][25] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [3]),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[8]_6 [25]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][26] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [3]),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[8]_6 [26]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][27] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [3]),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[8]_6 [27]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][28] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [3]),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[8]_6 [28]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][29] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [3]),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[8]_6 [29]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][2] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [0]),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[8]_6 [2]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][30] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [3]),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[8]_6 [30]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][31] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [3]),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[8]_6 [31]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][3] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [0]),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[8]_6 [3]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][4] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [0]),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[8]_6 [4]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][5] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [0]),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[8]_6 [5]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][6] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [0]),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[8]_6 [6]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][7] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [0]),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[8]_6 [7]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][8] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [1]),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[8]_6 [8]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[8][9] 
       (.C(clk),
        .CE(\axi_awaddr_reg[5]_1 [1]),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[8]_6 [9]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][0] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [0]),
        .D(s_axi_ctrl_status_wdata[0]),
        .Q(\slv_regs_reg[9]_7 [0]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][10] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [1]),
        .D(s_axi_ctrl_status_wdata[10]),
        .Q(\slv_regs_reg[9]_7 [10]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][11] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [1]),
        .D(s_axi_ctrl_status_wdata[11]),
        .Q(\slv_regs_reg[9]_7 [11]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][12] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [1]),
        .D(s_axi_ctrl_status_wdata[12]),
        .Q(\slv_regs_reg[9]_7 [12]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][13] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [1]),
        .D(s_axi_ctrl_status_wdata[13]),
        .Q(\slv_regs_reg[9]_7 [13]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][14] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [1]),
        .D(s_axi_ctrl_status_wdata[14]),
        .Q(\slv_regs_reg[9]_7 [14]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][15] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [1]),
        .D(s_axi_ctrl_status_wdata[15]),
        .Q(\slv_regs_reg[9]_7 [15]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][16] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [2]),
        .D(s_axi_ctrl_status_wdata[16]),
        .Q(\slv_regs_reg[9]_7 [16]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][17] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [2]),
        .D(s_axi_ctrl_status_wdata[17]),
        .Q(\slv_regs_reg[9]_7 [17]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][18] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [2]),
        .D(s_axi_ctrl_status_wdata[18]),
        .Q(\slv_regs_reg[9]_7 [18]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][19] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [2]),
        .D(s_axi_ctrl_status_wdata[19]),
        .Q(\slv_regs_reg[9]_7 [19]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][1] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [0]),
        .D(s_axi_ctrl_status_wdata[1]),
        .Q(\slv_regs_reg[9]_7 [1]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][20] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [2]),
        .D(s_axi_ctrl_status_wdata[20]),
        .Q(\slv_regs_reg[9]_7 [20]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][21] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [2]),
        .D(s_axi_ctrl_status_wdata[21]),
        .Q(\slv_regs_reg[9]_7 [21]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][22] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [2]),
        .D(s_axi_ctrl_status_wdata[22]),
        .Q(\slv_regs_reg[9]_7 [22]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][23] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [2]),
        .D(s_axi_ctrl_status_wdata[23]),
        .Q(\slv_regs_reg[9]_7 [23]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][24] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [3]),
        .D(s_axi_ctrl_status_wdata[24]),
        .Q(\slv_regs_reg[9]_7 [24]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][25] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [3]),
        .D(s_axi_ctrl_status_wdata[25]),
        .Q(\slv_regs_reg[9]_7 [25]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][26] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [3]),
        .D(s_axi_ctrl_status_wdata[26]),
        .Q(\slv_regs_reg[9]_7 [26]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][27] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [3]),
        .D(s_axi_ctrl_status_wdata[27]),
        .Q(\slv_regs_reg[9]_7 [27]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][28] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [3]),
        .D(s_axi_ctrl_status_wdata[28]),
        .Q(\slv_regs_reg[9]_7 [28]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][29] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [3]),
        .D(s_axi_ctrl_status_wdata[29]),
        .Q(\slv_regs_reg[9]_7 [29]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][2] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [0]),
        .D(s_axi_ctrl_status_wdata[2]),
        .Q(\slv_regs_reg[9]_7 [2]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][30] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [3]),
        .D(s_axi_ctrl_status_wdata[30]),
        .Q(\slv_regs_reg[9]_7 [30]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][31] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [3]),
        .D(s_axi_ctrl_status_wdata[31]),
        .Q(\slv_regs_reg[9]_7 [31]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][3] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [0]),
        .D(s_axi_ctrl_status_wdata[3]),
        .Q(\slv_regs_reg[9]_7 [3]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][4] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [0]),
        .D(s_axi_ctrl_status_wdata[4]),
        .Q(\slv_regs_reg[9]_7 [4]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][5] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [0]),
        .D(s_axi_ctrl_status_wdata[5]),
        .Q(\slv_regs_reg[9]_7 [5]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][6] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [0]),
        .D(s_axi_ctrl_status_wdata[6]),
        .Q(\slv_regs_reg[9]_7 [6]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][7] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [0]),
        .D(s_axi_ctrl_status_wdata[7]),
        .Q(\slv_regs_reg[9]_7 [7]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][8] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [1]),
        .D(s_axi_ctrl_status_wdata[8]),
        .Q(\slv_regs_reg[9]_7 [8]),
        .R(aresetn_0));
  FDRE \slv_regs_reg[9][9] 
       (.C(clk),
        .CE(\axi_awaddr_reg[2]_3 [1]),
        .D(s_axi_ctrl_status_wdata[9]),
        .Q(\slv_regs_reg[9]_7 [9]),
        .R(aresetn_0));
  LUT1 #(
    .INIT(2'h1)) 
    state1_carry__0_i_1
       (.I0(Q[8]),
        .O(\read_address_reg[0]_0 [3]));
  LUT1 #(
    .INIT(2'h1)) 
    state1_carry__0_i_2
       (.I0(Q[7]),
        .O(\read_address_reg[0]_0 [2]));
  LUT1 #(
    .INIT(2'h1)) 
    state1_carry__0_i_3
       (.I0(Q[6]),
        .O(\read_address_reg[0]_0 [1]));
  LUT1 #(
    .INIT(2'h1)) 
    state1_carry__0_i_4
       (.I0(Q[5]),
        .O(\read_address_reg[0]_0 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    state1_carry__1_i_1
       (.I0(Q[11]),
        .O(\read_address_reg[0]_1 [2]));
  LUT1 #(
    .INIT(2'h1)) 
    state1_carry__1_i_2
       (.I0(Q[10]),
        .O(\read_address_reg[0]_1 [1]));
  LUT1 #(
    .INIT(2'h1)) 
    state1_carry__1_i_3
       (.I0(Q[9]),
        .O(\read_address_reg[0]_1 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    state1_carry_i_1
       (.I0(Q[4]),
        .O(\read_address_reg[0] [3]));
  LUT1 #(
    .INIT(2'h1)) 
    state1_carry_i_2
       (.I0(Q[3]),
        .O(\read_address_reg[0] [2]));
  LUT1 #(
    .INIT(2'h1)) 
    state1_carry_i_3
       (.I0(Q[2]),
        .O(\read_address_reg[0] [1]));
  LUT1 #(
    .INIT(2'h1)) 
    state1_carry_i_4
       (.I0(Q[1]),
        .O(\read_address_reg[0] [0]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'h00FF0800)) 
    \state[0]_i_1 
       (.I0(cpu2emsc_register[13]),
        .I1(valid_input),
        .I2(\state_reg[1]_0 ),
        .I3(\state[1]_i_2_n_0 ),
        .I4(\state_reg[0]_0 ),
        .O(\state_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'h00FF0700)) 
    \state[1]_i_1 
       (.I0(cpu2emsc_register[13]),
        .I1(valid_input),
        .I2(\state_reg[0]_0 ),
        .I3(\state[1]_i_2_n_0 ),
        .I4(\state_reg[1]_0 ),
        .O(\state_reg[1] ));
  LUT6 #(
    .INIT(64'hFFFFFFFF0A0AFB0B)) 
    \state[1]_i_2 
       (.I0(\state[1]_i_3_n_0 ),
        .I1(\state_reg[0]_0 ),
        .I2(\state_reg[1]_0 ),
        .I3(\slv_regs_reg[0][11]_9 ),
        .I4(\read_address[6]_i_3_n_0 ),
        .I5(state),
        .O(\state[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'h08000000)) 
    \state[1]_i_3 
       (.I0(\slv_regs_reg[0][11]_10 ),
        .I1(valid_input),
        .I2(\state_reg[1]_0 ),
        .I3(\state_reg[0]_0 ),
        .I4(\b_ram_sel_reg[7] [7]),
        .O(\state[1]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \state[1]_i_4 
       (.I0(cpu2emsc_register[13]),
        .I1(valid_input),
        .I2(\state_reg[1]_0 ),
        .I3(\state_reg[0]_0 ),
        .O(state));
  FDRE valid_input_reg
       (.C(clk),
        .CE(1'b1),
        .D(valid_input1_out),
        .Q(valid_input),
        .R(aresetn_0));
endmodule

(* ORIG_REF_NAME = "top" *) 
module design_1_top_0_0_top
   (s_axi_ctrl_status_awready,
    s_axi_ctrl_status_wready,
    s_axi_ctrl_status_arready,
    s_axi_ctrl_status_rvalid,
    m_axis_tvalid,
    s_axis_tready,
    s_axi_ctrl_status_rdata,
    m_axis_tdata,
    m_axis_tlast,
    p_irq,
    s_axi_ctrl_status_bvalid,
    s_axi_ctrl_status_wvalid,
    s_axi_ctrl_status_awvalid,
    s_axi_ctrl_status_arvalid,
    m_axis_tready,
    aresetn,
    s_axis_tvalid,
    s_axis_tlast,
    clk,
    s_axi_ctrl_status_awaddr,
    s_axi_ctrl_status_wdata,
    s_axi_ctrl_status_araddr,
    s_axi_ctrl_status_wstrb,
    s_axis_tdata,
    s_axi_ctrl_status_bready,
    s_axi_ctrl_status_rready);
  output s_axi_ctrl_status_awready;
  output s_axi_ctrl_status_wready;
  output s_axi_ctrl_status_arready;
  output s_axi_ctrl_status_rvalid;
  output m_axis_tvalid;
  output s_axis_tready;
  output [31:0]s_axi_ctrl_status_rdata;
  output [48:0]m_axis_tdata;
  output m_axis_tlast;
  output p_irq;
  output s_axi_ctrl_status_bvalid;
  input s_axi_ctrl_status_wvalid;
  input s_axi_ctrl_status_awvalid;
  input s_axi_ctrl_status_arvalid;
  input m_axis_tready;
  input aresetn;
  input s_axis_tvalid;
  input s_axis_tlast;
  input clk;
  input [3:0]s_axi_ctrl_status_awaddr;
  input [31:0]s_axi_ctrl_status_wdata;
  input [3:0]s_axi_ctrl_status_araddr;
  input [3:0]s_axi_ctrl_status_wstrb;
  input [15:0]s_axis_tdata;
  input s_axi_ctrl_status_bready;
  input s_axi_ctrl_status_rready;

  wire [8:1]add_r2;
  wire aresetn;
  wire b_ram_n_262;
  wire b_ram_n_263;
  wire b_ram_n_264;
  wire b_ram_n_265;
  wire b_ram_n_266;
  wire b_ram_n_267;
  wire b_ram_n_268;
  wire b_ram_n_269;
  wire b_ram_n_270;
  wire b_ram_n_274;
  wire b_ram_n_275;
  wire b_ram_n_276;
  wire b_ram_n_277;
  wire b_ram_n_283;
  wire b_ram_n_284;
  wire b_ram_n_285;
  wire b_ram_n_286;
  wire b_ram_n_287;
  wire b_ram_n_288;
  wire b_ram_n_289;
  wire b_ram_n_290;
  wire b_ram_n_291;
  wire b_ram_n_292;
  wire b_ram_n_293;
  wire b_ram_n_294;
  wire b_ram_n_295;
  wire b_ram_n_296;
  wire b_ram_n_297;
  wire b_ram_n_298;
  wire b_ram_n_299;
  wire b_ram_n_300;
  wire [255:0]b_ram_out;
  wire clk;
  wire counter;
  wire \counter[0]_i_3_n_0 ;
  wire \counter[0]_i_4_n_0 ;
  wire \counter[0]_i_5_n_0 ;
  wire \counter[0]_i_6_n_0 ;
  wire \counter[0]_i_7_n_0 ;
  wire \counter[12]_i_2_n_0 ;
  wire \counter[12]_i_3_n_0 ;
  wire \counter[12]_i_4_n_0 ;
  wire \counter[12]_i_5_n_0 ;
  wire \counter[16]_i_2_n_0 ;
  wire \counter[16]_i_3_n_0 ;
  wire \counter[16]_i_4_n_0 ;
  wire \counter[16]_i_5_n_0 ;
  wire \counter[20]_i_2_n_0 ;
  wire \counter[20]_i_3_n_0 ;
  wire \counter[20]_i_4_n_0 ;
  wire \counter[20]_i_5_n_0 ;
  wire \counter[24]_i_2_n_0 ;
  wire \counter[24]_i_3_n_0 ;
  wire \counter[24]_i_4_n_0 ;
  wire \counter[24]_i_5_n_0 ;
  wire \counter[28]_i_2_n_0 ;
  wire \counter[28]_i_3_n_0 ;
  wire \counter[28]_i_4_n_0 ;
  wire \counter[28]_i_5_n_0 ;
  wire \counter[4]_i_2_n_0 ;
  wire \counter[4]_i_3_n_0 ;
  wire \counter[4]_i_4_n_0 ;
  wire \counter[4]_i_5_n_0 ;
  wire \counter[8]_i_2__0_n_0 ;
  wire \counter[8]_i_3_n_0 ;
  wire \counter[8]_i_4_n_0 ;
  wire \counter[8]_i_5_n_0 ;
  wire [31:0]counter_reg;
  wire \counter_reg[0]_i_2_n_0 ;
  wire \counter_reg[0]_i_2_n_1 ;
  wire \counter_reg[0]_i_2_n_2 ;
  wire \counter_reg[0]_i_2_n_3 ;
  wire \counter_reg[0]_i_2_n_4 ;
  wire \counter_reg[0]_i_2_n_5 ;
  wire \counter_reg[0]_i_2_n_6 ;
  wire \counter_reg[0]_i_2_n_7 ;
  wire \counter_reg[12]_i_1_n_0 ;
  wire \counter_reg[12]_i_1_n_1 ;
  wire \counter_reg[12]_i_1_n_2 ;
  wire \counter_reg[12]_i_1_n_3 ;
  wire \counter_reg[12]_i_1_n_4 ;
  wire \counter_reg[12]_i_1_n_5 ;
  wire \counter_reg[12]_i_1_n_6 ;
  wire \counter_reg[12]_i_1_n_7 ;
  wire \counter_reg[16]_i_1_n_0 ;
  wire \counter_reg[16]_i_1_n_1 ;
  wire \counter_reg[16]_i_1_n_2 ;
  wire \counter_reg[16]_i_1_n_3 ;
  wire \counter_reg[16]_i_1_n_4 ;
  wire \counter_reg[16]_i_1_n_5 ;
  wire \counter_reg[16]_i_1_n_6 ;
  wire \counter_reg[16]_i_1_n_7 ;
  wire \counter_reg[20]_i_1_n_0 ;
  wire \counter_reg[20]_i_1_n_1 ;
  wire \counter_reg[20]_i_1_n_2 ;
  wire \counter_reg[20]_i_1_n_3 ;
  wire \counter_reg[20]_i_1_n_4 ;
  wire \counter_reg[20]_i_1_n_5 ;
  wire \counter_reg[20]_i_1_n_6 ;
  wire \counter_reg[20]_i_1_n_7 ;
  wire \counter_reg[24]_i_1_n_0 ;
  wire \counter_reg[24]_i_1_n_1 ;
  wire \counter_reg[24]_i_1_n_2 ;
  wire \counter_reg[24]_i_1_n_3 ;
  wire \counter_reg[24]_i_1_n_4 ;
  wire \counter_reg[24]_i_1_n_5 ;
  wire \counter_reg[24]_i_1_n_6 ;
  wire \counter_reg[24]_i_1_n_7 ;
  wire \counter_reg[28]_i_1_n_1 ;
  wire \counter_reg[28]_i_1_n_2 ;
  wire \counter_reg[28]_i_1_n_3 ;
  wire \counter_reg[28]_i_1_n_4 ;
  wire \counter_reg[28]_i_1_n_5 ;
  wire \counter_reg[28]_i_1_n_6 ;
  wire \counter_reg[28]_i_1_n_7 ;
  wire \counter_reg[4]_i_1_n_0 ;
  wire \counter_reg[4]_i_1_n_1 ;
  wire \counter_reg[4]_i_1_n_2 ;
  wire \counter_reg[4]_i_1_n_3 ;
  wire \counter_reg[4]_i_1_n_4 ;
  wire \counter_reg[4]_i_1_n_5 ;
  wire \counter_reg[4]_i_1_n_6 ;
  wire \counter_reg[4]_i_1_n_7 ;
  wire \counter_reg[8]_i_1_n_0 ;
  wire \counter_reg[8]_i_1_n_1 ;
  wire \counter_reg[8]_i_1_n_2 ;
  wire \counter_reg[8]_i_1_n_3 ;
  wire \counter_reg[8]_i_1_n_4 ;
  wire \counter_reg[8]_i_1_n_5 ;
  wire \counter_reg[8]_i_1_n_6 ;
  wire \counter_reg[8]_i_1_n_7 ;
  wire dp_enable;
  wire dp_extend_end;
  wire dp_extend_end_i_1_n_0;
  wire dp_n_384;
  wire dp_n_385;
  wire dp_n_386;
  wire dp_n_387;
  wire dp_n_388;
  wire dp_n_389;
  wire dp_n_390;
  wire dp_n_391;
  wire dp_n_392;
  wire dp_n_393;
  wire dp_n_394;
  wire dp_n_395;
  wire dp_n_396;
  wire dp_n_397;
  wire dp_n_398;
  wire dp_n_399;
  wire dp_n_400;
  wire dp_n_401;
  wire dp_n_402;
  wire dp_n_403;
  wire dp_n_404;
  wire dp_n_405;
  wire dp_n_406;
  wire dp_n_407;
  wire dp_n_408;
  wire dp_n_409;
  wire dp_n_410;
  wire dp_n_411;
  wire dp_n_412;
  wire dp_n_413;
  wire dp_n_414;
  wire dp_n_415;
  wire dp_n_416;
  wire dp_n_417;
  wire dp_n_418;
  wire dp_n_419;
  wire dp_n_420;
  wire dp_n_421;
  wire dp_n_422;
  wire dp_n_423;
  wire dp_n_424;
  wire dp_n_425;
  wire dp_n_426;
  wire dp_n_427;
  wire dp_n_428;
  wire i__carry__0_i_1__0_n_0;
  wire i__carry__0_i_2__0_n_0;
  wire i__carry__0_i_3__0_n_0;
  wire i__carry__0_i_4_n_0;
  wire i__carry__1_i_1_n_0;
  wire i__carry__1_i_2_n_0;
  wire i__carry__1_i_3_n_0;
  wire i__carry_i_1_n_0;
  wire i__carry_i_2__0_n_0;
  wire i__carry_i_3__0_n_0;
  wire i__carry_i_4__0_n_0;
  wire [14:0]in_raw_delay;
  wire \in_raw_delay_reg[15]_rep__0_n_0 ;
  wire \in_raw_delay_reg[15]_rep__10_n_0 ;
  wire \in_raw_delay_reg[15]_rep__1_n_0 ;
  wire \in_raw_delay_reg[15]_rep__2_n_0 ;
  wire \in_raw_delay_reg[15]_rep__3_n_0 ;
  wire \in_raw_delay_reg[15]_rep__4_n_0 ;
  wire \in_raw_delay_reg[15]_rep__5_n_0 ;
  wire \in_raw_delay_reg[15]_rep__6_n_0 ;
  wire \in_raw_delay_reg[15]_rep__7_n_0 ;
  wire \in_raw_delay_reg[15]_rep__8_n_0 ;
  wire \in_raw_delay_reg[15]_rep__9_n_0 ;
  wire \in_raw_delay_reg[15]_rep_n_0 ;
  wire in_stream_handshake;
  wire in_stream_handshake_delay;
  wire in_stream_ready_i_2_n_0;
  wire last_p0;
  wire \last_p0_inferred__0/i__carry__0_n_0 ;
  wire \last_p0_inferred__0/i__carry__0_n_1 ;
  wire \last_p0_inferred__0/i__carry__0_n_2 ;
  wire \last_p0_inferred__0/i__carry__0_n_3 ;
  wire \last_p0_inferred__0/i__carry__1_n_2 ;
  wire \last_p0_inferred__0/i__carry__1_n_3 ;
  wire \last_p0_inferred__0/i__carry_n_0 ;
  wire \last_p0_inferred__0/i__carry_n_1 ;
  wire \last_p0_inferred__0/i__carry_n_2 ;
  wire \last_p0_inferred__0/i__carry_n_3 ;
  wire [31:1]last_p1;
  wire last_p1_carry__0_n_0;
  wire last_p1_carry__0_n_1;
  wire last_p1_carry__0_n_2;
  wire last_p1_carry__0_n_3;
  wire last_p1_carry__1_n_0;
  wire last_p1_carry__1_n_1;
  wire last_p1_carry__1_n_2;
  wire last_p1_carry__1_n_3;
  wire last_p1_carry__2_n_0;
  wire last_p1_carry__2_n_1;
  wire last_p1_carry__2_n_2;
  wire last_p1_carry__2_n_3;
  wire last_p1_carry__3_n_0;
  wire last_p1_carry__3_n_1;
  wire last_p1_carry__3_n_2;
  wire last_p1_carry__3_n_3;
  wire last_p1_carry__4_n_0;
  wire last_p1_carry__4_n_1;
  wire last_p1_carry__4_n_2;
  wire last_p1_carry__4_n_3;
  wire last_p1_carry__5_n_0;
  wire last_p1_carry__5_n_1;
  wire last_p1_carry__5_n_2;
  wire last_p1_carry__5_n_3;
  wire last_p1_carry__6_n_2;
  wire last_p1_carry__6_n_3;
  wire last_p1_carry_n_0;
  wire last_p1_carry_n_1;
  wire last_p1_carry_n_2;
  wire last_p1_carry_n_3;
  wire last_p_i_1_n_0;
  wire last_p_reg_n_0;
  wire [48:0]m_axis_tdata;
  wire m_axis_tlast;
  wire m_axis_tready;
  wire m_axis_tvalid;
  wire p_irq;
  wire [383:0]p_out;
  wire p_rdy;
  wire res_mem0;
  wire [3:0]s_axi_ctrl_status_araddr;
  wire s_axi_ctrl_status_arready;
  wire s_axi_ctrl_status_arvalid;
  wire [3:0]s_axi_ctrl_status_awaddr;
  wire s_axi_ctrl_status_awready;
  wire s_axi_ctrl_status_awvalid;
  wire s_axi_ctrl_status_bready;
  wire s_axi_ctrl_status_bvalid;
  wire [31:0]s_axi_ctrl_status_rdata;
  wire s_axi_ctrl_status_rready;
  wire s_axi_ctrl_status_rvalid;
  wire [31:0]s_axi_ctrl_status_wdata;
  wire s_axi_ctrl_status_wready;
  wire [3:0]s_axi_ctrl_status_wstrb;
  wire s_axi_ctrl_status_wvalid;
  wire [15:0]s_axis_tdata;
  wire s_axis_tlast;
  wire s_axis_tready;
  wire s_axis_tvalid;
  wire [0:0]v_len;
  wire [3:3]\NLW_counter_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:0]\NLW_last_p0_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:0]\NLW_last_p0_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire [3:3]\NLW_last_p0_inferred__0/i__carry__1_CO_UNCONNECTED ;
  wire [3:0]\NLW_last_p0_inferred__0/i__carry__1_O_UNCONNECTED ;
  wire [3:2]NLW_last_p1_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_last_p1_carry__6_O_UNCONNECTED;

  design_1_top_0_0_b_ram_bank b_ram
       (.CO(dp_n_384),
        .E(res_mem0),
        .O(b_ram_n_300),
        .Q(v_len),
        .S({b_ram_n_275,b_ram_n_276,b_ram_n_277}),
        .SR(b_ram_n_263),
        .aresetn(aresetn),
        .aresetn_0(in_stream_ready_i_2_n_0),
        .b_ram_out(b_ram_out),
        .clk(clk),
        .\counter_reg[1]_0 (b_ram_n_274),
        .\counter_reg[1]_1 ({add_r2[8:6],add_r2[2:1]}),
        .\counter_reg[1]_2 ({b_ram_n_283,b_ram_n_284}),
        .\counter_reg[1]_3 ({b_ram_n_285,b_ram_n_286}),
        .\counter_reg[1]_4 ({b_ram_n_287,b_ram_n_288,b_ram_n_289}),
        .\counter_reg[1]_5 ({b_ram_n_290,b_ram_n_291,b_ram_n_292}),
        .\counter_reg[1]_6 ({b_ram_n_293,b_ram_n_294,b_ram_n_295}),
        .\counter_reg[1]_7 ({b_ram_n_296,b_ram_n_297}),
        .\counter_reg[1]_8 ({b_ram_n_298,b_ram_n_299}),
        .\counter_reg[2]_0 (b_ram_n_264),
        .\counter_reg[2]_1 (b_ram_n_265),
        .\counter_reg[2]_2 (b_ram_n_266),
        .\counter_reg[2]_3 (b_ram_n_267),
        .\counter_reg[2]_4 (b_ram_n_268),
        .\counter_reg[2]_5 (b_ram_n_269),
        .\counter_reg[2]_6 (b_ram_n_270),
        .\counter_reg[5]_0 ({dp_n_398,dp_n_399,dp_n_400}),
        .\counter_reg[5]_1 ({dp_n_401,dp_n_402,dp_n_403}),
        .\counter_reg[5]_2 ({dp_n_422,dp_n_423,dp_n_424}),
        .\counter_reg[5]_3 ({dp_n_425,dp_n_426,dp_n_427}),
        .\counter_reg[8] ({dp_n_392,dp_n_393,dp_n_394,dp_n_395,dp_n_396,dp_n_397}),
        .\counter_reg[8]_0 ({dp_n_404,dp_n_405,dp_n_406,dp_n_407,dp_n_408,dp_n_409}),
        .\counter_reg[8]_1 ({dp_n_410,dp_n_411,dp_n_412,dp_n_413,dp_n_414,dp_n_415}),
        .\counter_reg[8]_2 ({dp_n_416,dp_n_417,dp_n_418,dp_n_419,dp_n_420,dp_n_421}),
        .dp_enable(dp_enable),
        .dp_extend_end(dp_extend_end),
        .in_stream_handshake_delay(in_stream_handshake_delay),
        .in_stream_ready_reg(b_ram_n_262),
        .p_rdy(p_rdy),
        .s_axi_ctrl_status_araddr(s_axi_ctrl_status_araddr),
        .s_axi_ctrl_status_arready(s_axi_ctrl_status_arready),
        .s_axi_ctrl_status_arvalid(s_axi_ctrl_status_arvalid),
        .s_axi_ctrl_status_awaddr(s_axi_ctrl_status_awaddr),
        .s_axi_ctrl_status_awready(s_axi_ctrl_status_awready),
        .s_axi_ctrl_status_awvalid(s_axi_ctrl_status_awvalid),
        .s_axi_ctrl_status_bready(s_axi_ctrl_status_bready),
        .s_axi_ctrl_status_bvalid(s_axi_ctrl_status_bvalid),
        .s_axi_ctrl_status_rdata(s_axi_ctrl_status_rdata),
        .s_axi_ctrl_status_rready(s_axi_ctrl_status_rready),
        .s_axi_ctrl_status_rvalid(s_axi_ctrl_status_rvalid),
        .s_axi_ctrl_status_wdata(s_axi_ctrl_status_wdata),
        .s_axi_ctrl_status_wready(s_axi_ctrl_status_wready),
        .s_axi_ctrl_status_wstrb(s_axi_ctrl_status_wstrb),
        .s_axi_ctrl_status_wvalid(s_axi_ctrl_status_wvalid),
        .s_axis_tready(s_axis_tready),
        .s_axis_tvalid(s_axis_tvalid),
        .\slv_regs_reg[0][11] (dp_n_385),
        .\slv_regs_reg[0][11]_0 (dp_n_386),
        .\slv_regs_reg[0][11]_1 (dp_n_387),
        .\slv_regs_reg[0][11]_2 (dp_n_388),
        .\slv_regs_reg[0][11]_3 (dp_n_389),
        .\slv_regs_reg[0][11]_4 (dp_n_390),
        .\slv_regs_reg[0][11]_5 (dp_n_391),
        .\slv_regs_reg[0][11]_6 (dp_n_428));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[0]_i_1__1 
       (.I0(dp_extend_end),
        .I1(s_axis_tlast),
        .O(counter));
  LUT1 #(
    .INIT(2'h1)) 
    \counter[0]_i_3 
       (.I0(last_p0),
        .O(\counter[0]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[0]_i_4 
       (.I0(counter_reg[3]),
        .I1(last_p0),
        .O(\counter[0]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[0]_i_5 
       (.I0(counter_reg[2]),
        .I1(last_p0),
        .O(\counter[0]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[0]_i_6 
       (.I0(counter_reg[1]),
        .I1(last_p0),
        .O(\counter[0]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \counter[0]_i_7 
       (.I0(counter_reg[0]),
        .I1(last_p0),
        .O(\counter[0]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[12]_i_2 
       (.I0(counter_reg[15]),
        .I1(last_p0),
        .O(\counter[12]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[12]_i_3 
       (.I0(counter_reg[14]),
        .I1(last_p0),
        .O(\counter[12]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[12]_i_4 
       (.I0(counter_reg[13]),
        .I1(last_p0),
        .O(\counter[12]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[12]_i_5 
       (.I0(counter_reg[12]),
        .I1(last_p0),
        .O(\counter[12]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[16]_i_2 
       (.I0(counter_reg[19]),
        .I1(last_p0),
        .O(\counter[16]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[16]_i_3 
       (.I0(counter_reg[18]),
        .I1(last_p0),
        .O(\counter[16]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[16]_i_4 
       (.I0(counter_reg[17]),
        .I1(last_p0),
        .O(\counter[16]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[16]_i_5 
       (.I0(counter_reg[16]),
        .I1(last_p0),
        .O(\counter[16]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[20]_i_2 
       (.I0(counter_reg[23]),
        .I1(last_p0),
        .O(\counter[20]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[20]_i_3 
       (.I0(counter_reg[22]),
        .I1(last_p0),
        .O(\counter[20]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[20]_i_4 
       (.I0(counter_reg[21]),
        .I1(last_p0),
        .O(\counter[20]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[20]_i_5 
       (.I0(counter_reg[20]),
        .I1(last_p0),
        .O(\counter[20]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[24]_i_2 
       (.I0(counter_reg[27]),
        .I1(last_p0),
        .O(\counter[24]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[24]_i_3 
       (.I0(counter_reg[26]),
        .I1(last_p0),
        .O(\counter[24]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[24]_i_4 
       (.I0(counter_reg[25]),
        .I1(last_p0),
        .O(\counter[24]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[24]_i_5 
       (.I0(counter_reg[24]),
        .I1(last_p0),
        .O(\counter[24]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[28]_i_2 
       (.I0(counter_reg[31]),
        .I1(last_p0),
        .O(\counter[28]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[28]_i_3 
       (.I0(counter_reg[30]),
        .I1(last_p0),
        .O(\counter[28]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[28]_i_4 
       (.I0(counter_reg[29]),
        .I1(last_p0),
        .O(\counter[28]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[28]_i_5 
       (.I0(counter_reg[28]),
        .I1(last_p0),
        .O(\counter[28]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[4]_i_2 
       (.I0(counter_reg[7]),
        .I1(last_p0),
        .O(\counter[4]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[4]_i_3 
       (.I0(counter_reg[6]),
        .I1(last_p0),
        .O(\counter[4]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[4]_i_4 
       (.I0(counter_reg[5]),
        .I1(last_p0),
        .O(\counter[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[4]_i_5 
       (.I0(counter_reg[4]),
        .I1(last_p0),
        .O(\counter[4]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[8]_i_2__0 
       (.I0(counter_reg[11]),
        .I1(last_p0),
        .O(\counter[8]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[8]_i_3 
       (.I0(counter_reg[10]),
        .I1(last_p0),
        .O(\counter[8]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[8]_i_4 
       (.I0(counter_reg[9]),
        .I1(last_p0),
        .O(\counter[8]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \counter[8]_i_5 
       (.I0(counter_reg[8]),
        .I1(last_p0),
        .O(\counter[8]_i_5_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[0] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[0]_i_2_n_7 ),
        .Q(counter_reg[0]));
  CARRY4 \counter_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\counter_reg[0]_i_2_n_0 ,\counter_reg[0]_i_2_n_1 ,\counter_reg[0]_i_2_n_2 ,\counter_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\counter[0]_i_3_n_0 }),
        .O({\counter_reg[0]_i_2_n_4 ,\counter_reg[0]_i_2_n_5 ,\counter_reg[0]_i_2_n_6 ,\counter_reg[0]_i_2_n_7 }),
        .S({\counter[0]_i_4_n_0 ,\counter[0]_i_5_n_0 ,\counter[0]_i_6_n_0 ,\counter[0]_i_7_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[10] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[8]_i_1_n_5 ),
        .Q(counter_reg[10]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[11] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[8]_i_1_n_4 ),
        .Q(counter_reg[11]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[12] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[12]_i_1_n_7 ),
        .Q(counter_reg[12]));
  CARRY4 \counter_reg[12]_i_1 
       (.CI(\counter_reg[8]_i_1_n_0 ),
        .CO({\counter_reg[12]_i_1_n_0 ,\counter_reg[12]_i_1_n_1 ,\counter_reg[12]_i_1_n_2 ,\counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_reg[12]_i_1_n_4 ,\counter_reg[12]_i_1_n_5 ,\counter_reg[12]_i_1_n_6 ,\counter_reg[12]_i_1_n_7 }),
        .S({\counter[12]_i_2_n_0 ,\counter[12]_i_3_n_0 ,\counter[12]_i_4_n_0 ,\counter[12]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[13] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[12]_i_1_n_6 ),
        .Q(counter_reg[13]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[14] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[12]_i_1_n_5 ),
        .Q(counter_reg[14]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[15] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[12]_i_1_n_4 ),
        .Q(counter_reg[15]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[16] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[16]_i_1_n_7 ),
        .Q(counter_reg[16]));
  CARRY4 \counter_reg[16]_i_1 
       (.CI(\counter_reg[12]_i_1_n_0 ),
        .CO({\counter_reg[16]_i_1_n_0 ,\counter_reg[16]_i_1_n_1 ,\counter_reg[16]_i_1_n_2 ,\counter_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_reg[16]_i_1_n_4 ,\counter_reg[16]_i_1_n_5 ,\counter_reg[16]_i_1_n_6 ,\counter_reg[16]_i_1_n_7 }),
        .S({\counter[16]_i_2_n_0 ,\counter[16]_i_3_n_0 ,\counter[16]_i_4_n_0 ,\counter[16]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[17] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[16]_i_1_n_6 ),
        .Q(counter_reg[17]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[18] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[16]_i_1_n_5 ),
        .Q(counter_reg[18]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[19] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[16]_i_1_n_4 ),
        .Q(counter_reg[19]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[1] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[0]_i_2_n_6 ),
        .Q(counter_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[20] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[20]_i_1_n_7 ),
        .Q(counter_reg[20]));
  CARRY4 \counter_reg[20]_i_1 
       (.CI(\counter_reg[16]_i_1_n_0 ),
        .CO({\counter_reg[20]_i_1_n_0 ,\counter_reg[20]_i_1_n_1 ,\counter_reg[20]_i_1_n_2 ,\counter_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_reg[20]_i_1_n_4 ,\counter_reg[20]_i_1_n_5 ,\counter_reg[20]_i_1_n_6 ,\counter_reg[20]_i_1_n_7 }),
        .S({\counter[20]_i_2_n_0 ,\counter[20]_i_3_n_0 ,\counter[20]_i_4_n_0 ,\counter[20]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[21] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[20]_i_1_n_6 ),
        .Q(counter_reg[21]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[22] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[20]_i_1_n_5 ),
        .Q(counter_reg[22]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[23] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[20]_i_1_n_4 ),
        .Q(counter_reg[23]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[24] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[24]_i_1_n_7 ),
        .Q(counter_reg[24]));
  CARRY4 \counter_reg[24]_i_1 
       (.CI(\counter_reg[20]_i_1_n_0 ),
        .CO({\counter_reg[24]_i_1_n_0 ,\counter_reg[24]_i_1_n_1 ,\counter_reg[24]_i_1_n_2 ,\counter_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_reg[24]_i_1_n_4 ,\counter_reg[24]_i_1_n_5 ,\counter_reg[24]_i_1_n_6 ,\counter_reg[24]_i_1_n_7 }),
        .S({\counter[24]_i_2_n_0 ,\counter[24]_i_3_n_0 ,\counter[24]_i_4_n_0 ,\counter[24]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[25] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[24]_i_1_n_6 ),
        .Q(counter_reg[25]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[26] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[24]_i_1_n_5 ),
        .Q(counter_reg[26]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[27] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[24]_i_1_n_4 ),
        .Q(counter_reg[27]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[28] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[28]_i_1_n_7 ),
        .Q(counter_reg[28]));
  CARRY4 \counter_reg[28]_i_1 
       (.CI(\counter_reg[24]_i_1_n_0 ),
        .CO({\NLW_counter_reg[28]_i_1_CO_UNCONNECTED [3],\counter_reg[28]_i_1_n_1 ,\counter_reg[28]_i_1_n_2 ,\counter_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_reg[28]_i_1_n_4 ,\counter_reg[28]_i_1_n_5 ,\counter_reg[28]_i_1_n_6 ,\counter_reg[28]_i_1_n_7 }),
        .S({\counter[28]_i_2_n_0 ,\counter[28]_i_3_n_0 ,\counter[28]_i_4_n_0 ,\counter[28]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[29] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[28]_i_1_n_6 ),
        .Q(counter_reg[29]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[2] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[0]_i_2_n_5 ),
        .Q(counter_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[30] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[28]_i_1_n_5 ),
        .Q(counter_reg[30]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[31] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[28]_i_1_n_4 ),
        .Q(counter_reg[31]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[3] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[0]_i_2_n_4 ),
        .Q(counter_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[4] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[4]_i_1_n_7 ),
        .Q(counter_reg[4]));
  CARRY4 \counter_reg[4]_i_1 
       (.CI(\counter_reg[0]_i_2_n_0 ),
        .CO({\counter_reg[4]_i_1_n_0 ,\counter_reg[4]_i_1_n_1 ,\counter_reg[4]_i_1_n_2 ,\counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_reg[4]_i_1_n_4 ,\counter_reg[4]_i_1_n_5 ,\counter_reg[4]_i_1_n_6 ,\counter_reg[4]_i_1_n_7 }),
        .S({\counter[4]_i_2_n_0 ,\counter[4]_i_3_n_0 ,\counter[4]_i_4_n_0 ,\counter[4]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[5] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[4]_i_1_n_6 ),
        .Q(counter_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[6] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[4]_i_1_n_5 ),
        .Q(counter_reg[6]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[7] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[4]_i_1_n_4 ),
        .Q(counter_reg[7]));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[8] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[8]_i_1_n_7 ),
        .Q(counter_reg[8]));
  CARRY4 \counter_reg[8]_i_1 
       (.CI(\counter_reg[4]_i_1_n_0 ),
        .CO({\counter_reg[8]_i_1_n_0 ,\counter_reg[8]_i_1_n_1 ,\counter_reg[8]_i_1_n_2 ,\counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_reg[8]_i_1_n_4 ,\counter_reg[8]_i_1_n_5 ,\counter_reg[8]_i_1_n_6 ,\counter_reg[8]_i_1_n_7 }),
        .S({\counter[8]_i_2__0_n_0 ,\counter[8]_i_3_n_0 ,\counter[8]_i_4_n_0 ,\counter[8]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \counter_reg[9] 
       (.C(clk),
        .CE(counter),
        .CLR(in_stream_ready_i_2_n_0),
        .D(\counter_reg[8]_i_1_n_6 ),
        .Q(counter_reg[9]));
  design_1_top_0_0_dot_product_module dp
       (.A({\in_raw_delay_reg[15]_rep_n_0 ,in_raw_delay}),
        .B(\in_raw_delay_reg[15]_rep__10_n_0 ),
        .CO(dp_n_384),
        .D(p_out),
        .O(b_ram_n_300),
        .Q({dp_n_392,dp_n_393,dp_n_394,dp_n_395,dp_n_396,dp_n_397}),
        .S({b_ram_n_275,b_ram_n_276,b_ram_n_277}),
        .SR(b_ram_n_263),
        .aresetn(in_stream_ready_i_2_n_0),
        .b_ram_out(b_ram_out),
        .clk(clk),
        .\counter_reg[1] (dp_n_385),
        .\counter_reg[1]_0 (dp_n_386),
        .\counter_reg[1]_1 (dp_n_387),
        .\counter_reg[1]_2 (dp_n_388),
        .\counter_reg[1]_3 (dp_n_389),
        .\counter_reg[1]_4 (dp_n_390),
        .\counter_reg[1]_5 (dp_n_391),
        .\counter_reg[7] ({dp_n_398,dp_n_399,dp_n_400}),
        .\counter_reg[7]_0 ({dp_n_401,dp_n_402,dp_n_403}),
        .\counter_reg[7]_1 ({dp_n_425,dp_n_426,dp_n_427}),
        .\counter_reg[8] ({dp_n_404,dp_n_405,dp_n_406,dp_n_407,dp_n_408,dp_n_409}),
        .\counter_reg[8]_0 ({dp_n_410,dp_n_411,dp_n_412,dp_n_413,dp_n_414,dp_n_415}),
        .\counter_reg[8]_1 ({dp_n_416,dp_n_417,dp_n_418,dp_n_419,dp_n_420,dp_n_421}),
        .\counter_reg[8]_2 ({dp_n_422,dp_n_423,dp_n_424}),
        .dp_enable(dp_enable),
        .dp_extend_end_reg(b_ram_n_274),
        .dp_extend_end_reg_0(b_ram_n_264),
        .dp_extend_end_reg_1(b_ram_n_265),
        .dp_extend_end_reg_2(b_ram_n_266),
        .dp_extend_end_reg_3(b_ram_n_267),
        .dp_extend_end_reg_4(b_ram_n_268),
        .dp_extend_end_reg_5(b_ram_n_269),
        .dp_extend_end_reg_6(b_ram_n_270),
        .\in_raw_delay_reg[15]_rep__0 (\in_raw_delay_reg[15]_rep__0_n_0 ),
        .\in_raw_delay_reg[15]_rep__2 ({\in_raw_delay_reg[15]_rep__2_n_0 ,\in_raw_delay_reg[15]_rep__1_n_0 }),
        .\in_raw_delay_reg[15]_rep__4 ({\in_raw_delay_reg[15]_rep__4_n_0 ,\in_raw_delay_reg[15]_rep__3_n_0 }),
        .\in_raw_delay_reg[15]_rep__6 ({\in_raw_delay_reg[15]_rep__6_n_0 ,\in_raw_delay_reg[15]_rep__5_n_0 }),
        .\in_raw_delay_reg[15]_rep__7 (\in_raw_delay_reg[15]_rep__7_n_0 ),
        .\in_raw_delay_reg[15]_rep__8 (\in_raw_delay_reg[15]_rep__8_n_0 ),
        .\in_raw_delay_reg[15]_rep__9 (\in_raw_delay_reg[15]_rep__9_n_0 ),
        .\res_mem_reg[383] (dp_n_428),
        .\slv_regs_reg[0][0] (v_len),
        .\slv_regs_reg[0][11] ({b_ram_n_283,b_ram_n_284}),
        .\slv_regs_reg[0][11]_0 ({b_ram_n_285,b_ram_n_286}),
        .\slv_regs_reg[0][11]_1 ({b_ram_n_287,b_ram_n_288,b_ram_n_289}),
        .\slv_regs_reg[0][11]_2 ({b_ram_n_290,b_ram_n_291,b_ram_n_292}),
        .\slv_regs_reg[0][11]_3 ({b_ram_n_293,b_ram_n_294,b_ram_n_295}),
        .\slv_regs_reg[0][11]_4 ({b_ram_n_296,b_ram_n_297}),
        .\slv_regs_reg[0][11]_5 ({b_ram_n_298,b_ram_n_299}),
        .\slv_regs_reg[0][8] ({add_r2[8:6],add_r2[2:1]}));
  LUT4 #(
    .INIT(16'hFA2A)) 
    dp_extend_end_i_1
       (.I0(dp_extend_end),
        .I1(last_p0),
        .I2(aresetn),
        .I3(s_axis_tlast),
        .O(dp_extend_end_i_1_n_0));
  FDRE dp_extend_end_reg
       (.C(clk),
        .CE(1'b1),
        .D(dp_extend_end_i_1_n_0),
        .Q(dp_extend_end),
        .R(1'b0));
  design_1_top_0_0_axi_gearbox gb
       (.D(p_out),
        .E(res_mem0),
        .aresetn(aresetn),
        .aresetn_0(in_stream_ready_i_2_n_0),
        .clk(clk),
        .last_p_reg(last_p_reg_n_0),
        .m_axis_tdata(m_axis_tdata),
        .m_axis_tlast(m_axis_tlast),
        .m_axis_tready(m_axis_tready),
        .m_axis_tvalid(m_axis_tvalid),
        .p_irq(p_irq),
        .p_rdy(p_rdy));
  LUT3 #(
    .INIT(8'h01)) 
    i__carry__0_i_1__0
       (.I0(last_p1[22]),
        .I1(last_p1[23]),
        .I2(last_p1[21]),
        .O(i__carry__0_i_1__0_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    i__carry__0_i_2__0
       (.I0(last_p1[19]),
        .I1(last_p1[20]),
        .I2(last_p1[18]),
        .O(i__carry__0_i_2__0_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    i__carry__0_i_3__0
       (.I0(last_p1[16]),
        .I1(last_p1[17]),
        .I2(last_p1[15]),
        .O(i__carry__0_i_3__0_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    i__carry__0_i_4
       (.I0(last_p1[13]),
        .I1(last_p1[14]),
        .I2(last_p1[12]),
        .O(i__carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    i__carry__1_i_1
       (.I0(last_p1[31]),
        .I1(last_p1[30]),
        .O(i__carry__1_i_1_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    i__carry__1_i_2
       (.I0(last_p1[28]),
        .I1(last_p1[29]),
        .I2(last_p1[27]),
        .O(i__carry__1_i_2_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    i__carry__1_i_3
       (.I0(last_p1[25]),
        .I1(last_p1[26]),
        .I2(last_p1[24]),
        .O(i__carry__1_i_3_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    i__carry_i_1
       (.I0(last_p1[10]),
        .I1(last_p1[11]),
        .I2(last_p1[9]),
        .O(i__carry_i_1_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    i__carry_i_2__0
       (.I0(last_p1[7]),
        .I1(last_p1[8]),
        .I2(last_p1[6]),
        .O(i__carry_i_2__0_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    i__carry_i_3__0
       (.I0(last_p1[4]),
        .I1(last_p1[5]),
        .I2(last_p1[3]),
        .O(i__carry_i_3__0_n_0));
  LUT3 #(
    .INIT(8'h04)) 
    i__carry_i_4__0
       (.I0(last_p1[2]),
        .I1(last_p1[1]),
        .I2(counter_reg[0]),
        .O(i__carry_i_4__0_n_0));
  FDCE \in_raw_delay_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[0]),
        .Q(in_raw_delay[0]));
  FDCE \in_raw_delay_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[10]),
        .Q(in_raw_delay[10]));
  FDCE \in_raw_delay_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[11]),
        .Q(in_raw_delay[11]));
  FDCE \in_raw_delay_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[12]),
        .Q(in_raw_delay[12]));
  FDCE \in_raw_delay_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[13]),
        .Q(in_raw_delay[13]));
  FDCE \in_raw_delay_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[14]),
        .Q(in_raw_delay[14]));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__0 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__0_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__1 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__1_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__10 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__10_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__2 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__2_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__3 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__3_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__4 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__4_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__5 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__5_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__6 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__6_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__7 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__7_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__8 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__8_n_0 ));
  (* ORIG_CELL_NAME = "in_raw_delay_reg[15]" *) 
  FDCE \in_raw_delay_reg[15]_rep__9 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[15]),
        .Q(\in_raw_delay_reg[15]_rep__9_n_0 ));
  FDCE \in_raw_delay_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[1]),
        .Q(in_raw_delay[1]));
  FDCE \in_raw_delay_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[2]),
        .Q(in_raw_delay[2]));
  FDCE \in_raw_delay_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[3]),
        .Q(in_raw_delay[3]));
  FDCE \in_raw_delay_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[4]),
        .Q(in_raw_delay[4]));
  FDCE \in_raw_delay_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[5]),
        .Q(in_raw_delay[5]));
  FDCE \in_raw_delay_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[6]),
        .Q(in_raw_delay[6]));
  FDCE \in_raw_delay_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[7]),
        .Q(in_raw_delay[7]));
  FDCE \in_raw_delay_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[8]),
        .Q(in_raw_delay[8]));
  FDCE \in_raw_delay_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(s_axis_tdata[9]),
        .Q(in_raw_delay[9]));
  LUT2 #(
    .INIT(4'h8)) 
    in_stream_handshake_delay_i_1
       (.I0(s_axis_tvalid),
        .I1(s_axis_tready),
        .O(in_stream_handshake));
  FDCE in_stream_handshake_delay_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(in_stream_handshake),
        .Q(in_stream_handshake_delay));
  LUT1 #(
    .INIT(2'h1)) 
    in_stream_ready_i_2
       (.I0(aresetn),
        .O(in_stream_ready_i_2_n_0));
  FDCE in_stream_ready_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(b_ram_n_262),
        .Q(s_axis_tready));
  CARRY4 \last_p0_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\last_p0_inferred__0/i__carry_n_0 ,\last_p0_inferred__0/i__carry_n_1 ,\last_p0_inferred__0/i__carry_n_2 ,\last_p0_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_last_p0_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S({i__carry_i_1_n_0,i__carry_i_2__0_n_0,i__carry_i_3__0_n_0,i__carry_i_4__0_n_0}));
  CARRY4 \last_p0_inferred__0/i__carry__0 
       (.CI(\last_p0_inferred__0/i__carry_n_0 ),
        .CO({\last_p0_inferred__0/i__carry__0_n_0 ,\last_p0_inferred__0/i__carry__0_n_1 ,\last_p0_inferred__0/i__carry__0_n_2 ,\last_p0_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_last_p0_inferred__0/i__carry__0_O_UNCONNECTED [3:0]),
        .S({i__carry__0_i_1__0_n_0,i__carry__0_i_2__0_n_0,i__carry__0_i_3__0_n_0,i__carry__0_i_4_n_0}));
  CARRY4 \last_p0_inferred__0/i__carry__1 
       (.CI(\last_p0_inferred__0/i__carry__0_n_0 ),
        .CO({\NLW_last_p0_inferred__0/i__carry__1_CO_UNCONNECTED [3],last_p0,\last_p0_inferred__0/i__carry__1_n_2 ,\last_p0_inferred__0/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_last_p0_inferred__0/i__carry__1_O_UNCONNECTED [3:0]),
        .S({1'b0,i__carry__1_i_1_n_0,i__carry__1_i_2_n_0,i__carry__1_i_3_n_0}));
  CARRY4 last_p1_carry
       (.CI(1'b0),
        .CO({last_p1_carry_n_0,last_p1_carry_n_1,last_p1_carry_n_2,last_p1_carry_n_3}),
        .CYINIT(counter_reg[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(last_p1[4:1]),
        .S(counter_reg[4:1]));
  CARRY4 last_p1_carry__0
       (.CI(last_p1_carry_n_0),
        .CO({last_p1_carry__0_n_0,last_p1_carry__0_n_1,last_p1_carry__0_n_2,last_p1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(last_p1[8:5]),
        .S(counter_reg[8:5]));
  CARRY4 last_p1_carry__1
       (.CI(last_p1_carry__0_n_0),
        .CO({last_p1_carry__1_n_0,last_p1_carry__1_n_1,last_p1_carry__1_n_2,last_p1_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(last_p1[12:9]),
        .S(counter_reg[12:9]));
  CARRY4 last_p1_carry__2
       (.CI(last_p1_carry__1_n_0),
        .CO({last_p1_carry__2_n_0,last_p1_carry__2_n_1,last_p1_carry__2_n_2,last_p1_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(last_p1[16:13]),
        .S(counter_reg[16:13]));
  CARRY4 last_p1_carry__3
       (.CI(last_p1_carry__2_n_0),
        .CO({last_p1_carry__3_n_0,last_p1_carry__3_n_1,last_p1_carry__3_n_2,last_p1_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(last_p1[20:17]),
        .S(counter_reg[20:17]));
  CARRY4 last_p1_carry__4
       (.CI(last_p1_carry__3_n_0),
        .CO({last_p1_carry__4_n_0,last_p1_carry__4_n_1,last_p1_carry__4_n_2,last_p1_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(last_p1[24:21]),
        .S(counter_reg[24:21]));
  CARRY4 last_p1_carry__5
       (.CI(last_p1_carry__4_n_0),
        .CO({last_p1_carry__5_n_0,last_p1_carry__5_n_1,last_p1_carry__5_n_2,last_p1_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(last_p1[28:25]),
        .S(counter_reg[28:25]));
  CARRY4 last_p1_carry__6
       (.CI(last_p1_carry__5_n_0),
        .CO({NLW_last_p1_carry__6_CO_UNCONNECTED[3:2],last_p1_carry__6_n_2,last_p1_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_last_p1_carry__6_O_UNCONNECTED[3],last_p1[31:29]}),
        .S({1'b0,counter_reg[31:29]}));
  LUT3 #(
    .INIT(8'h40)) 
    last_p_i_1
       (.I0(s_axis_tlast),
        .I1(dp_extend_end),
        .I2(last_p0),
        .O(last_p_i_1_n_0));
  FDCE last_p_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(in_stream_ready_i_2_n_0),
        .D(last_p_i_1_n_0),
        .Q(last_p_reg_n_0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
