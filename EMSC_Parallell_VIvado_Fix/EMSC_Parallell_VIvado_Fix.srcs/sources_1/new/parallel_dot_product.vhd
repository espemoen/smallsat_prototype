library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity parallel_dot_product is
      Generic(
            RAW_BIT_WIDTH : positive := 16;
            G_BIT_WIDTH   : positive := 32;
            NUM_B_RAM     : positive := 5;
            P_BIT_WIDTH   : positive := 48            
      );
      Port ( 
            clk     :   in  std_logic;
            aresetn :   in  std_logic;
            en      :   in  std_logic;
            in_G    :   in  std_logic_vector(G_BIT_WIDTH*NUM_B_RAM-1 downto 0);
            in_raw  :   in  std_logic_vector(RAW_BIT_WIDTH*4-1 downto 0);
            v_len   :   in  std_logic_vector(11 downto 0);
            p_rdy   :   out std_logic_vector(NUM_B_RAM-1 downto 0);
            p_out   :   out std_logic_vector(NUM_B_RAM*P_BIT_WIDTH-1 downto 0)
      );
end parallel_dot_product;

architecture Behavioral of parallel_dot_product is





begin

process(clk, aresetn)
    begin
        if(aresetn = '0') then
        
        elsif(rising_edge(clk)) then
        
        end if;
end process;

end Behavioral;
