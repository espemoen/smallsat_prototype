library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity axi_mux is
    Generic(
        AXI_DATA_WIDTH : integer := 64
    );
    Port ( 
        s_axis_tdata_1  : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
        --DMA is ready to send data
        s_axis_tvalid_1 : in  std_logic;
        --EMSC is ready to receive data
        s_axis_tready_1 : out std_logic;
        --DMA say this is last data
        s_axis_tlast_1  : in  std_logic;
        
        s_axis_tdata_2  : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
        --DMA is ready to send data
        s_axis_tvalid_2 : in  std_logic;
        --EMSC is ready to receive data
        s_axis_tready_2 : out std_logic;
        --DMA say this is last data
        s_axis_tlast_2  : in  std_logic;
        
        --AXI out-stream
        m_axis_tdata  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
        --EMSC is ready to send to DMA.
        m_axis_tvalid : out std_logic;
        --DMA is ready to receive data
        m_axis_tready : in  std_logic;
        --Tell DMA this is last data
        m_axis_tlast  : out std_logic;
        
        sel : in std_logic
    );
end axi_mux;

architecture Behavioral of axi_mux is
    

begin
    
process(sel)
begin
    case sel is
        when '0' =>
            m_axis_tdata    <= s_axis_tdata_1;
            m_axis_tvalid   <= s_axis_tvalid_1; 
            s_axis_tready_1 <= m_axis_tready;
            m_axis_tlast    <= s_axis_tlast_1;
            s_axis_tready_2 <= '0';
        when '1' =>
            m_axis_tdata    <= s_axis_tdata_2;
            m_axis_tvalid   <= s_axis_tvalid_2; 
            s_axis_tready_2 <= m_axis_tready;
            m_axis_tlast    <= s_axis_tlast_2;
            s_axis_tready_1 <= '0';
    end casE;
end process;

end Behavioral;
