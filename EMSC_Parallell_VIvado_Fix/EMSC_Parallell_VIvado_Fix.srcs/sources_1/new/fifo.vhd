library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
Library xpm;
use xpm.vcomponents.all;



entity fifo is
    Generic(
            FIFO_DEPTH : integer := 16;
            FIFO_SIZE : integer := 52;
            WRITE_DATA_WIDTH : integer := 64;
            WR_DATA_COUNT_WIDTH : integer := 5;
            FIFO_MARGIN : integer :=  15;
            RD_DATA_COUNT_WIDTH : integer := 5;
            READ_DATA_WIDTH : integer := 16;
            LATENCY_CYCLES      : integer := 13
    );
    Port ( 
            clk : in std_logic;
            aresetn : in std_logic;
            wr_en : in std_logic;
            rd_en : in std_logic;
            dp_enable : out std_logic;
            fifo_in : in std_logic_vector(63 downto 0);
            fifo_out: out std_logic_vector(15 downto 0);
            fifo_empty : out std_logic;
            fifo_full : out std_logic
    );
end fifo;

architecture Behavioral of fifo is

signal reset : std_logic;
signal fifo_wren : std_logic;
signal fifo_read : std_logic;




begin

reset <= not aresetn;

fifo_wren <= '1' when wr_en = '1' else '0';

--process(clk, aresetn)
--    variable counter : integer := 0;
--begin
--    if(aresetn = '0') then
--        fifo_read <= '0';
--        dp_enable <= '0';
--    elsif(rising_edge(clk)) then
--        if(wr_en = '1') then
--            if(counter >= LATENCY_CYCLES-1) then
--                counter := 0;
--                fifo_read <= '1';
--            end if;
--            counter := counter + 1;
--        end if;
--    end if;
--end process;


i_fifo : xpm_fifo_sync
    generic map (
      FIFO_MEMORY_TYPE    => "auto",
      ECC_MODE            => "no_ecc",
      FIFO_WRITE_DEPTH    => FIFO_DEPTH,
      WRITE_DATA_WIDTH    => WRITE_DATA_WIDTH,
      WR_DATA_COUNT_WIDTH => WR_DATA_COUNT_WIDTH,
      PROG_FULL_THRESH    => FIFO_DEPTH - FIFO_MARGIN,
      FULL_RESET_VALUE    => 0,
      READ_MODE           => "std",
      FIFO_READ_LATENCY   => 1,
      READ_DATA_WIDTH     => READ_DATA_WIDTH,
      RD_DATA_COUNT_WIDTH => RD_DATA_COUNT_WIDTH,
      PROG_EMPTY_THRESH   => 10,
      DOUT_RESET_VALUE    => "0",
      WAKEUP_TIME         => 0
      )
    port map (
      rst           => reset,
      wr_clk        => clk,
      wr_en         => fifo_wren,
      din           => fifo_in,
      full          => open,
      overflow      => open,
      wr_rst_busy   => open,
      rd_en         => rd_en,
      dout          => fifo_out,
      empty         => fifo_empty,
      underflow     => open,
      rd_rst_busy   => open,
      prog_full     => fifo_full,
      wr_data_count => open,
      prog_empty    => open,
      rd_data_count => open,
      sleep         => '0',
      injectsbiterr => '0',
      injectdbiterr => '0',
      sbiterr       => open,
      dbiterr       => open
      );

end Behavioral;
