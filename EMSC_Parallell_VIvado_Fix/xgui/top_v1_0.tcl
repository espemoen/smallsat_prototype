# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "B_RAM_BIT_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "B_RAM_SIZE" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_S_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_S_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "FIFO_DEPTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "FIFO_MARGIN" -parent ${Page_0}
  ipgui::add_param $IPINST -name "FIFO_SIZE" -parent ${Page_0}
  ipgui::add_param $IPINST -name "G_BIT_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "LATENCY_CYCLES" -parent ${Page_0}
  ipgui::add_param $IPINST -name "NUM_B_RAM" -parent ${Page_0}
  ipgui::add_param $IPINST -name "NUM_PIXELS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "P_BIT_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "RAW_BIT_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "RD_DATA_COUNT_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "READ_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "WRITE_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "WR_DATA_COUNT_WIDTH" -parent ${Page_0}


}

proc update_PARAM_VALUE.B_RAM_BIT_WIDTH { PARAM_VALUE.B_RAM_BIT_WIDTH } {
	# Procedure called to update B_RAM_BIT_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.B_RAM_BIT_WIDTH { PARAM_VALUE.B_RAM_BIT_WIDTH } {
	# Procedure called to validate B_RAM_BIT_WIDTH
	return true
}

proc update_PARAM_VALUE.B_RAM_SIZE { PARAM_VALUE.B_RAM_SIZE } {
	# Procedure called to update B_RAM_SIZE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.B_RAM_SIZE { PARAM_VALUE.B_RAM_SIZE } {
	# Procedure called to validate B_RAM_SIZE
	return true
}

proc update_PARAM_VALUE.C_S_AXI_ADDR_WIDTH { PARAM_VALUE.C_S_AXI_ADDR_WIDTH } {
	# Procedure called to update C_S_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S_AXI_ADDR_WIDTH { PARAM_VALUE.C_S_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_S_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S_AXI_DATA_WIDTH { PARAM_VALUE.C_S_AXI_DATA_WIDTH } {
	# Procedure called to update C_S_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S_AXI_DATA_WIDTH { PARAM_VALUE.C_S_AXI_DATA_WIDTH } {
	# Procedure called to validate C_S_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.FIFO_DEPTH { PARAM_VALUE.FIFO_DEPTH } {
	# Procedure called to update FIFO_DEPTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.FIFO_DEPTH { PARAM_VALUE.FIFO_DEPTH } {
	# Procedure called to validate FIFO_DEPTH
	return true
}

proc update_PARAM_VALUE.FIFO_MARGIN { PARAM_VALUE.FIFO_MARGIN } {
	# Procedure called to update FIFO_MARGIN when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.FIFO_MARGIN { PARAM_VALUE.FIFO_MARGIN } {
	# Procedure called to validate FIFO_MARGIN
	return true
}

proc update_PARAM_VALUE.FIFO_SIZE { PARAM_VALUE.FIFO_SIZE } {
	# Procedure called to update FIFO_SIZE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.FIFO_SIZE { PARAM_VALUE.FIFO_SIZE } {
	# Procedure called to validate FIFO_SIZE
	return true
}

proc update_PARAM_VALUE.G_BIT_WIDTH { PARAM_VALUE.G_BIT_WIDTH } {
	# Procedure called to update G_BIT_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.G_BIT_WIDTH { PARAM_VALUE.G_BIT_WIDTH } {
	# Procedure called to validate G_BIT_WIDTH
	return true
}

proc update_PARAM_VALUE.LATENCY_CYCLES { PARAM_VALUE.LATENCY_CYCLES } {
	# Procedure called to update LATENCY_CYCLES when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.LATENCY_CYCLES { PARAM_VALUE.LATENCY_CYCLES } {
	# Procedure called to validate LATENCY_CYCLES
	return true
}

proc update_PARAM_VALUE.NUM_B_RAM { PARAM_VALUE.NUM_B_RAM } {
	# Procedure called to update NUM_B_RAM when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NUM_B_RAM { PARAM_VALUE.NUM_B_RAM } {
	# Procedure called to validate NUM_B_RAM
	return true
}

proc update_PARAM_VALUE.NUM_PIXELS { PARAM_VALUE.NUM_PIXELS } {
	# Procedure called to update NUM_PIXELS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NUM_PIXELS { PARAM_VALUE.NUM_PIXELS } {
	# Procedure called to validate NUM_PIXELS
	return true
}

proc update_PARAM_VALUE.P_BIT_WIDTH { PARAM_VALUE.P_BIT_WIDTH } {
	# Procedure called to update P_BIT_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.P_BIT_WIDTH { PARAM_VALUE.P_BIT_WIDTH } {
	# Procedure called to validate P_BIT_WIDTH
	return true
}

proc update_PARAM_VALUE.RAW_BIT_WIDTH { PARAM_VALUE.RAW_BIT_WIDTH } {
	# Procedure called to update RAW_BIT_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RAW_BIT_WIDTH { PARAM_VALUE.RAW_BIT_WIDTH } {
	# Procedure called to validate RAW_BIT_WIDTH
	return true
}

proc update_PARAM_VALUE.RD_DATA_COUNT_WIDTH { PARAM_VALUE.RD_DATA_COUNT_WIDTH } {
	# Procedure called to update RD_DATA_COUNT_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RD_DATA_COUNT_WIDTH { PARAM_VALUE.RD_DATA_COUNT_WIDTH } {
	# Procedure called to validate RD_DATA_COUNT_WIDTH
	return true
}

proc update_PARAM_VALUE.READ_DATA_WIDTH { PARAM_VALUE.READ_DATA_WIDTH } {
	# Procedure called to update READ_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.READ_DATA_WIDTH { PARAM_VALUE.READ_DATA_WIDTH } {
	# Procedure called to validate READ_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.WRITE_DATA_WIDTH { PARAM_VALUE.WRITE_DATA_WIDTH } {
	# Procedure called to update WRITE_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.WRITE_DATA_WIDTH { PARAM_VALUE.WRITE_DATA_WIDTH } {
	# Procedure called to validate WRITE_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.WR_DATA_COUNT_WIDTH { PARAM_VALUE.WR_DATA_COUNT_WIDTH } {
	# Procedure called to update WR_DATA_COUNT_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.WR_DATA_COUNT_WIDTH { PARAM_VALUE.WR_DATA_COUNT_WIDTH } {
	# Procedure called to validate WR_DATA_COUNT_WIDTH
	return true
}


proc update_MODELPARAM_VALUE.B_RAM_SIZE { MODELPARAM_VALUE.B_RAM_SIZE PARAM_VALUE.B_RAM_SIZE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.B_RAM_SIZE}] ${MODELPARAM_VALUE.B_RAM_SIZE}
}

proc update_MODELPARAM_VALUE.B_RAM_BIT_WIDTH { MODELPARAM_VALUE.B_RAM_BIT_WIDTH PARAM_VALUE.B_RAM_BIT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.B_RAM_BIT_WIDTH}] ${MODELPARAM_VALUE.B_RAM_BIT_WIDTH}
}

proc update_MODELPARAM_VALUE.NUM_B_RAM { MODELPARAM_VALUE.NUM_B_RAM PARAM_VALUE.NUM_B_RAM } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NUM_B_RAM}] ${MODELPARAM_VALUE.NUM_B_RAM}
}

proc update_MODELPARAM_VALUE.RAW_BIT_WIDTH { MODELPARAM_VALUE.RAW_BIT_WIDTH PARAM_VALUE.RAW_BIT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RAW_BIT_WIDTH}] ${MODELPARAM_VALUE.RAW_BIT_WIDTH}
}

proc update_MODELPARAM_VALUE.G_BIT_WIDTH { MODELPARAM_VALUE.G_BIT_WIDTH PARAM_VALUE.G_BIT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.G_BIT_WIDTH}] ${MODELPARAM_VALUE.G_BIT_WIDTH}
}

proc update_MODELPARAM_VALUE.P_BIT_WIDTH { MODELPARAM_VALUE.P_BIT_WIDTH PARAM_VALUE.P_BIT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.P_BIT_WIDTH}] ${MODELPARAM_VALUE.P_BIT_WIDTH}
}

proc update_MODELPARAM_VALUE.C_S_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_S_AXI_DATA_WIDTH PARAM_VALUE.C_S_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_S_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_S_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_S_AXI_ADDR_WIDTH PARAM_VALUE.C_S_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_S_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.NUM_PIXELS { MODELPARAM_VALUE.NUM_PIXELS PARAM_VALUE.NUM_PIXELS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NUM_PIXELS}] ${MODELPARAM_VALUE.NUM_PIXELS}
}

proc update_MODELPARAM_VALUE.FIFO_DEPTH { MODELPARAM_VALUE.FIFO_DEPTH PARAM_VALUE.FIFO_DEPTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.FIFO_DEPTH}] ${MODELPARAM_VALUE.FIFO_DEPTH}
}

proc update_MODELPARAM_VALUE.FIFO_SIZE { MODELPARAM_VALUE.FIFO_SIZE PARAM_VALUE.FIFO_SIZE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.FIFO_SIZE}] ${MODELPARAM_VALUE.FIFO_SIZE}
}

proc update_MODELPARAM_VALUE.WRITE_DATA_WIDTH { MODELPARAM_VALUE.WRITE_DATA_WIDTH PARAM_VALUE.WRITE_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.WRITE_DATA_WIDTH}] ${MODELPARAM_VALUE.WRITE_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.WR_DATA_COUNT_WIDTH { MODELPARAM_VALUE.WR_DATA_COUNT_WIDTH PARAM_VALUE.WR_DATA_COUNT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.WR_DATA_COUNT_WIDTH}] ${MODELPARAM_VALUE.WR_DATA_COUNT_WIDTH}
}

proc update_MODELPARAM_VALUE.FIFO_MARGIN { MODELPARAM_VALUE.FIFO_MARGIN PARAM_VALUE.FIFO_MARGIN } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.FIFO_MARGIN}] ${MODELPARAM_VALUE.FIFO_MARGIN}
}

proc update_MODELPARAM_VALUE.RD_DATA_COUNT_WIDTH { MODELPARAM_VALUE.RD_DATA_COUNT_WIDTH PARAM_VALUE.RD_DATA_COUNT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RD_DATA_COUNT_WIDTH}] ${MODELPARAM_VALUE.RD_DATA_COUNT_WIDTH}
}

proc update_MODELPARAM_VALUE.READ_DATA_WIDTH { MODELPARAM_VALUE.READ_DATA_WIDTH PARAM_VALUE.READ_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.READ_DATA_WIDTH}] ${MODELPARAM_VALUE.READ_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.LATENCY_CYCLES { MODELPARAM_VALUE.LATENCY_CYCLES PARAM_VALUE.LATENCY_CYCLES } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.LATENCY_CYCLES}] ${MODELPARAM_VALUE.LATENCY_CYCLES}
}

