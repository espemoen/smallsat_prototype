Timingresultater:

Hele EMSC kode:
- 621455906 clock cycles => clock frequency 100 Mhz
	-> 621455906 * (1/100Mhz) = 6,21 sec

Bare potensiell HW kode:
- 566919086 clock cycles => clock frequency 100 Mhz
	-> 566919086 * (1/100Mhz) = 5,70 sec

91% koden brukes i potensiell HW del.
