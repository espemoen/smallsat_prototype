`timescale 1ns / 1ps

module top_tb;
   parameter C_S_AXI_DATA_WIDTH = 32;
   parameter C_S_AXI_ADDR_WIDTH = 6;
   parameter B_RAM_SIZE = 100;
   parameter B_RAM_BIT_WIDTH = 32;
   parameter NUM_B_RAM = 8;
   parameter RAW_BIT_WIDTH = 16;
   parameter G_BIT_WIDTH = 32;
   parameter P_BIT_WIDTH = 48;
   parameter PERIOD = 10;
    
   reg clk, aresetn;
   reg[15:0] s_axis_tdata;
   reg s_axis_tvalid, s_axis_tlast;
   wire s_axis_tready, p_irq;

   reg[32:0]counter;
   wire[63:0] m_axis_tdata;
   wire m_axis_tvalid, m_axis_tlast;
   reg m_axis_tready;
   
    
   reg[5:0]   s_axi_ctrl_status_awaddr;
   reg[2:0]   s_axi_ctrl_status_awprot;
   reg        s_axi_ctrl_status_awvalid;
   reg[31:0]  s_axi_ctrl_status_wdata;
   reg[3:0]   s_axi_ctrl_status_wstrb;
   reg        s_axi_ctrl_status_wvalid;
   reg        s_axi_ctrl_status_bready;
   reg[5:0]   s_axi_ctrl_status_araddr;
   reg[2:0]   s_axi_ctrl_status_arprot;
   reg        s_axi_ctrl_status_arvalid;
   reg        s_axi_ctrl_status_rready;
   wire       s_axi_ctrl_status_awready;
   wire       s_axi_ctrl_status_wready;
   wire[1:0]  s_axi_ctrl_status_bresp;
   wire       s_axi_ctrl_status_bvalid;
   wire       s_axi_ctrl_status_arready;
   wire[31:0] s_axi_ctrl_status_rdata;
   wire[1:0]  s_axi_ctrl_status_rresp;
   wire       s_axi_ctrl_status_rvalid;


   top
      #(.C_S_AXI_DATA_WIDTH(C_S_AXI_DATA_WIDTH),
        .C_S_AXI_ADDR_WIDTH(C_S_AXI_ADDR_WIDTH),
        .B_RAM_SIZE(B_RAM_SIZE),
        .B_RAM_BIT_WIDTH(B_RAM_BIT_WIDTH),
        .NUM_B_RAM(NUM_B_RAM),
        .RAW_BIT_WIDTH(RAW_BIT_WIDTH),
        .G_BIT_WIDTH(G_BIT_WIDTH),
        .P_BIT_WIDTH(P_BIT_WIDTH))
   DUT
       (.clk(clk),
        .aresetn(aresetn),
        .p_irq(p_irq),
        //IN-STREAM
        .s_axis_tdata(s_axis_tdata),
        .s_axis_tvalid(s_axis_tvalid),
        .s_axis_tready(s_axis_tready),
        .s_axis_tlast(s_axis_tlast),
        
        //OUT-STREAM
        .m_axis_tdata(m_axis_tdata),
        .m_axis_tvalid(m_axis_tvalid),
        .m_axis_tready(m_axis_tready),
        .m_axis_tlast(m_axis_tlast),
        
        //REGISTER-INTERFACE
        .s_axi_ctrl_status_awaddr(s_axi_ctrl_status_awaddr),
        .s_axi_ctrl_status_awprot(s_axi_ctrl_status_awprot),
        .s_axi_ctrl_status_awvalid(s_axi_ctrl_status_awvalid),
        .s_axi_ctrl_status_wdata(s_axi_ctrl_status_wdata),
        .s_axi_ctrl_status_wstrb(s_axi_ctrl_status_wstrb),
        .s_axi_ctrl_status_wvalid(s_axi_ctrl_status_wvalid),
        .s_axi_ctrl_status_bready(s_axi_ctrl_status_bready),
        .s_axi_ctrl_status_araddr(s_axi_ctrl_status_araddr),
        .s_axi_ctrl_status_arprot(s_axi_ctrl_status_arprot),
        .s_axi_ctrl_status_arvalid(s_axi_ctrl_status_arvalid),
        .s_axi_ctrl_status_rready(s_axi_ctrl_status_rready),
        .s_axi_ctrl_status_awready(s_axi_ctrl_status_awready),
        .s_axi_ctrl_status_wready(s_axi_ctrl_status_wready),
        .s_axi_ctrl_status_bresp(s_axi_ctrl_status_bresp),
        .s_axi_ctrl_status_bvalid(s_axi_ctrl_status_bvalid),
        .s_axi_ctrl_status_arready(s_axi_ctrl_status_arready),
        .s_axi_ctrl_status_rdata(s_axi_ctrl_status_rdata),
        .s_axi_ctrl_status_rresp(s_axi_ctrl_status_rresp),
        .s_axi_ctrl_status_rvalid(s_axi_ctrl_status_rvalid)
       );

    always #(PERIOD/2) clk = ~clk;
    
    integer          f_in_G, f_in_raw, f_out_P;
    integer          iter, i;
    reg[31:0] in_G_temp;
    reg[31:0] in_raw_temp;  
    reg[31:0] read_holder;
    reg flagg;
    
    initial begin
        clk = 1'b0;
        aresetn = 1'b0;
        counter = 32'b0;
        s_axis_tlast <= 1'b0;
        s_axi_ctrl_status_awprot = 'b0;
        s_axi_ctrl_status_bready = 1'b0;
        s_axi_ctrl_status_wstrb = 4'hF;
        s_axi_ctrl_status_arprot = 'b0;
        

        
        
        f_in_G = $fopen("D:/user/My_Documents/Espen_Moen/prototype/EMSC/in_G.bin", "rb");
        
        if (f_in_G == 0) begin
            $display("Failed to open input file %s", "D:/user/My_Documents/Espen_Moen/prototype/EMSC/in_G.");
            $finish;
        end
        
        f_in_raw = $fopen("D:/user/My_Documents/Espen_Moen/prototype/EMSC/Test/raw_large.bin", "rb");
                
        if (f_in_raw == 0) begin
            $display("Failed to open input file %s", "D:/user/My_Documents/Espen_Moen/prototype/EMSC/Test/raw_large.bin");
            $finish;
        end
        
        f_out_P = $fopen("D:/user/My_Documents/Espen_Moen/prototype/EMSC/Test/P_out_tb.bin", "wb");
        
        if (f_out_P == 0) begin
            $display("Failed to open input file %s", "D:/user/My_Documents/Espen_Moen/prototype/EMSC/Test/P_out_tb.bin");
            $finish;
        end
        
        repeat(2) @(posedge clk);
        aresetn = 1'b1;
        write_to_reg(6'h0, 32'h2034);
        
        repeat(3) @(posedge clk);
        
        for (iter = 0; iter < 416; iter = iter + 1) begin                                                   //|
            for( i = 0; i < 4; i = i + 1) begin                                                             //|
                in_G_temp[i*8  +: 8] = $fgetc(f_in_G);                                                          //|
            end
            write_to_reg(6'h4, in_G_temp);                                                                                             //|
            @(posedge clk);                                                                                          //|
        end  
        
        write_to_reg(6'h0, 32'h34);
        repeat(2) @(posedge clk);
        
        write_to_reg(6'h0, 32'h1034);
        repeat(10) @(posedge clk); 
        
        
        s_axis_tvalid <= 1'b1;
         
        for (iter = 0; iter < 13000000; iter = iter + 1) begin                                                   //|
            for( i = 0; i < 2; i = i + 1) begin                                                             //|
                in_raw_temp[i*8  +: 8] = $fgetc(f_in_raw);                                                          //|
            end
            s_axis_tdata <= in_raw_temp[15:0];
            if(iter == 12999999) begin
                s_axis_tlast <= 1'b1; 
            end                                                                                             //|
            @(posedge clk);
            counter = counter + 1;                                                                                          //|
         end  
        s_axis_tlast <= 1'b0;
        s_axis_tvalid <= 1'b0;
       
        
        
        $fclose(f_in_G);
        $fclose(f_in_raw);
        repeat(100) @(posedge clk); 
        $fclose(f_out_P);
    end


    always @(posedge clk) begin
        if($urandom % 3 == 0) begin
            m_axis_tready <= 1'b0;
        end
        else
            m_axis_tready <= 1'b1;
        end
   
   integer byte_idx, j;
   
    always @(posedge clk) begin
            if(m_axis_tready == 1'b1 && m_axis_tvalid == 1'b1) begin
                for (byte_idx = 0; byte_idx < 8; byte_idx = byte_idx + 1) begin
                    $fwrite(f_out_P, "%c", m_axis_tdata[byte_idx*8+:8]);
                end
            end
    end

    task write_to_reg;
        input [5:0] address;
        input [31:0] data;
        begin
            @(posedge clk);
            s_axi_ctrl_status_awaddr <= address;
            s_axi_ctrl_status_awvalid <= 1'b1;
            s_axi_ctrl_status_wvalid <= 1'b1;
            s_axi_ctrl_status_wdata <= data;
            
            while (!(s_axi_ctrl_status_awready == 1'b1 && s_axi_ctrl_status_wready == 1'b1)) begin
                @(posedge clk);
            end
           
            s_axi_ctrl_status_awvalid <= 1'b0;
            s_axi_ctrl_status_wvalid <= 1'b0;
        end
     endtask
     
     
     
     task read_reg;
             input  [5:0] address;
             output [31:0] data;
             begin
                 @(posedge clk);
                 s_axi_ctrl_status_araddr = address;
                 s_axi_ctrl_status_arvalid = 1'b1;     
                 s_axi_ctrl_status_rready = 1'b1;
                 while (!(s_axi_ctrl_status_rvalid == 1'b1)) begin
                     @(posedge clk);
                 end
                 
                 s_axi_ctrl_status_rready <= 1'b0;
                 s_axi_ctrl_status_arvalid <= 1'b0;
                 data = s_axi_ctrl_status_rdata; 
                 

             end
          endtask

endmodule