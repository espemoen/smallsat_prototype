# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "B_RAM_BIT_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "B_RAM_SIZE" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_S_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_S_AXI_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "NUM_B_RAM" -parent ${Page_0}


}

proc update_PARAM_VALUE.B_RAM_BIT_WIDTH { PARAM_VALUE.B_RAM_BIT_WIDTH } {
	# Procedure called to update B_RAM_BIT_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.B_RAM_BIT_WIDTH { PARAM_VALUE.B_RAM_BIT_WIDTH } {
	# Procedure called to validate B_RAM_BIT_WIDTH
	return true
}

proc update_PARAM_VALUE.B_RAM_SIZE { PARAM_VALUE.B_RAM_SIZE } {
	# Procedure called to update B_RAM_SIZE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.B_RAM_SIZE { PARAM_VALUE.B_RAM_SIZE } {
	# Procedure called to validate B_RAM_SIZE
	return true
}

proc update_PARAM_VALUE.C_S_AXI_ADDR_WIDTH { PARAM_VALUE.C_S_AXI_ADDR_WIDTH } {
	# Procedure called to update C_S_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S_AXI_ADDR_WIDTH { PARAM_VALUE.C_S_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_S_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S_AXI_DATA_WIDTH { PARAM_VALUE.C_S_AXI_DATA_WIDTH } {
	# Procedure called to update C_S_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S_AXI_DATA_WIDTH { PARAM_VALUE.C_S_AXI_DATA_WIDTH } {
	# Procedure called to validate C_S_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.NUM_B_RAM { PARAM_VALUE.NUM_B_RAM } {
	# Procedure called to update NUM_B_RAM when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NUM_B_RAM { PARAM_VALUE.NUM_B_RAM } {
	# Procedure called to validate NUM_B_RAM
	return true
}


proc update_MODELPARAM_VALUE.B_RAM_SIZE { MODELPARAM_VALUE.B_RAM_SIZE PARAM_VALUE.B_RAM_SIZE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.B_RAM_SIZE}] ${MODELPARAM_VALUE.B_RAM_SIZE}
}

proc update_MODELPARAM_VALUE.B_RAM_BIT_WIDTH { MODELPARAM_VALUE.B_RAM_BIT_WIDTH PARAM_VALUE.B_RAM_BIT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.B_RAM_BIT_WIDTH}] ${MODELPARAM_VALUE.B_RAM_BIT_WIDTH}
}

proc update_MODELPARAM_VALUE.NUM_B_RAM { MODELPARAM_VALUE.NUM_B_RAM PARAM_VALUE.NUM_B_RAM } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NUM_B_RAM}] ${MODELPARAM_VALUE.NUM_B_RAM}
}

proc update_MODELPARAM_VALUE.C_S_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_S_AXI_DATA_WIDTH PARAM_VALUE.C_S_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_S_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_S_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_S_AXI_ADDR_WIDTH PARAM_VALUE.C_S_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_S_AXI_ADDR_WIDTH}
}

