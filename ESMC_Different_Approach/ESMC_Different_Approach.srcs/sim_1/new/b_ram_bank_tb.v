`timescale 1ns / 1ps

module b_ram_bank_tb;
   parameter C_S_AXI_DATA_WIDTH = 32;
   parameter C_S_AXI_ADDR_WIDTH = 6;
   parameter B_RAM_SIZE = 100;
   parameter B_RAM_BIT_WIDTH = 32;
   parameter NUM_B_RAM = 8;
   parameter PERIOD = 10;
    
   reg[31:0] in_G_temp; 
   reg[31:0] read_holder;
    
   reg clk, aresetn, read_enable;
   wire enable;
   wire[B_RAM_BIT_WIDTH*NUM_B_RAM-1:0] data_out;
    
   reg[5:0]   s_axi_ctrl_status_awaddr;
   reg[2:0]   s_axi_ctrl_status_awprot;
   reg        s_axi_ctrl_status_awvalid;
   reg[31:0]  s_axi_ctrl_status_wdata;
   reg[3:0]   s_axi_ctrl_status_wstrb;
   reg        s_axi_ctrl_status_wvalid;
   reg        s_axi_ctrl_status_bready;
   reg[5:0]   s_axi_ctrl_status_araddr;
   reg[2:0]   s_axi_ctrl_status_arprot;
   reg        s_axi_ctrl_status_arvalid;
   reg        s_axi_ctrl_status_rready;
   wire       s_axi_ctrl_status_awready;
   wire       s_axi_ctrl_status_wready;
   wire[1:0]  s_axi_ctrl_status_bresp;
   wire       s_axi_ctrl_status_bvalid;
   wire       s_axi_ctrl_status_arready;
   wire[31:0] s_axi_ctrl_status_rdata;
   wire[1:0]  s_axi_ctrl_status_rresp;
   wire       s_axi_ctrl_status_rvalid;

   b_ram_bank
        #(.C_S_AXI_DATA_WIDTH(C_S_AXI_DATA_WIDTH),
          .C_S_AXI_ADDR_WIDTH(C_S_AXI_ADDR_WIDTH),
          .B_RAM_SIZE(B_RAM_SIZE),
          .B_RAM_BIT_WIDTH(B_RAM_BIT_WIDTH),
          .NUM_B_RAM(NUM_B_RAM))
   DUT
        (.clk(clk),
         .aresetn(aresetn),
         .read_enable(read_enable),
         .enable(enable),
         .data_out(data_out),
         .s_axi_ctrl_status_awaddr(s_axi_ctrl_status_awaddr),
         .s_axi_ctrl_status_awprot(s_axi_ctrl_status_awprot),
         .s_axi_ctrl_status_awvalid(s_axi_ctrl_status_awvalid),
         .s_axi_ctrl_status_wdata(s_axi_ctrl_status_wdata),
         .s_axi_ctrl_status_wstrb(s_axi_ctrl_status_wstrb),
         .s_axi_ctrl_status_wvalid(s_axi_ctrl_status_wvalid),
         .s_axi_ctrl_status_bready(s_axi_ctrl_status_bready),
         .s_axi_ctrl_status_araddr(s_axi_ctrl_status_araddr),
         .s_axi_ctrl_status_arprot(s_axi_ctrl_status_arprot),
         .s_axi_ctrl_status_arvalid(s_axi_ctrl_status_arvalid),
         .s_axi_ctrl_status_rready(s_axi_ctrl_status_rready),
         .s_axi_ctrl_status_awready(s_axi_ctrl_status_awready),
         .s_axi_ctrl_status_wready(s_axi_ctrl_status_wready),
         .s_axi_ctrl_status_bresp(s_axi_ctrl_status_bresp),
         .s_axi_ctrl_status_bvalid(s_axi_ctrl_status_bvalid),
         .s_axi_ctrl_status_arready(s_axi_ctrl_status_arready),
         .s_axi_ctrl_status_rdata(s_axi_ctrl_status_rdata),
         .s_axi_ctrl_status_rresp(s_axi_ctrl_status_rresp),
         .s_axi_ctrl_status_rvalid(s_axi_ctrl_status_rvalid)
        );
    
    
    integer          f_in_G, f_in_raw;
    integer          iter, i;
    
    always #(PERIOD/2) clk = ~clk;
    
    initial begin
        clk = 1'b0;
        aresetn = 1'b0;
        read_enable <= 0'b0;
        
        s_axi_ctrl_status_awprot = 'b0;
        s_axi_ctrl_status_bready = 1'b0;
        s_axi_ctrl_status_wstrb = 4'hF;
        s_axi_ctrl_status_arprot = 'b0;
        

        
        
        f_in_G = $fopen("D:/MasterOppgave/smallsat_prototype/EMSC/in_G.bin", "rb");
        
        if (f_in_G == 0) begin
            $display("Failed to open input file %s", "D:/MasterOppgave/smallsat_prototype/EMSC/in_G.bin");
            $finish;
        end
                    
        
        repeat(2) @(posedge clk);
        aresetn = 1'b1;
        write_to_reg(6'h0, 32'h2034);
        
        repeat(3) @(posedge clk);
        
        for (iter = 0; iter < 416; iter = iter + 1) begin                                                   //|
            for( i = 0; i < 4; i = i + 1) begin                                                             //|
                in_G_temp[i*8  +: 8] = $fgetc(f_in_G);                                                          //|
            end
            write_to_reg(6'h4, in_G_temp);                                                                                             //|
            @(posedge clk);                                                                                          //|
        end  
        
        repeat(2)@(posedge clk);
        read_reg(6'h8, read_holder);
        
        repeat(2)@(posedge clk);
        read_reg(6'h8, read_holder);
        
        read_enable <= 1'b1;
        
        repeat(3)@(posedge clk);
        
        read_enable <= 1'b0;
    end
    
    
    
    
     task write_to_reg;
        input [5:0] address;
        input [31:0] data;
        begin
            @(posedge clk);
            s_axi_ctrl_status_awaddr <= address;
            s_axi_ctrl_status_awvalid <= 1'b1;
            s_axi_ctrl_status_wvalid <= 1'b1;
            s_axi_ctrl_status_wdata <= data;
            
            while (!(s_axi_ctrl_status_awready == 1'b1 && s_axi_ctrl_status_wready == 1'b1)) begin
                @(posedge clk);
            end
           
            s_axi_ctrl_status_awvalid <= 1'b0;
            s_axi_ctrl_status_wvalid <= 1'b0;
        end
     endtask
     
     
     
     task read_reg;
             input  [5:0] address;
             output [31:0] data;
             begin
                 @(posedge clk);
                 s_axi_ctrl_status_araddr = address;
                 s_axi_ctrl_status_arvalid = 1'b1;     
                 s_axi_ctrl_status_rready = 1'b1;
                 while (!(s_axi_ctrl_status_rvalid == 1'b1)) begin
                     @(posedge clk);
                 end
                 
                 s_axi_ctrl_status_rready <= 1'b0;
                 s_axi_ctrl_status_arvalid <= 1'b0;
                 data = s_axi_ctrl_status_rdata; 
                 

             end
          endtask
    
    
    
    
endmodule
