library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity top is
    Generic(
        B_RAM_SIZE      : integer := 100;
        B_RAM_BIT_WIDTH : integer := 32;
        NUM_B_RAM       : integer := 8;
        RAW_BIT_WIDTH : positive := 16;
        G_BIT_WIDTH   : positive := 32;
        P_BIT_WIDTH   : positive := 48;
        C_S_AXI_DATA_WIDTH : integer := 32;
        C_S_AXI_ADDR_WIDTH : integer := 6
    );
   Port ( 
        clk     :   in std_logic;
        aresetn :   in std_logic;
        p_irq   :   out std_logic;
        
        --AXI in-stream
        s_axis_tdata  : in  std_logic_vector(RAW_BIT_WIDTH-1 downto 0);
        --DMA is ready to send data
        s_axis_tvalid : in  std_logic;
        --EMSC is ready to receive data
        s_axis_tready : out std_logic;
        --DMA say this is last data
        s_axis_tlast  : in  std_logic;
        
        --AXI out-stream
        m_axis_tdata  : out std_logic_vector(63 downto 0);
        --EMSC is ready to send to DMA.
        m_axis_tvalid : out std_logic;
        --DMA is ready to receive data
        m_axis_tready : in  std_logic;
        --Tell DMA this is last data
        m_axis_tlast  : out std_logic;
        
        
        -- Register interface
        s_axi_ctrl_status_awaddr  : in  std_logic_vector(5 downto 0);
        s_axi_ctrl_status_awprot  : in  std_logic_vector(2 downto 0);
        s_axi_ctrl_status_awvalid : in  std_logic;
        s_axi_ctrl_status_awready : out std_logic;
        s_axi_ctrl_status_wdata   : in  std_logic_vector(31 downto 0);
        s_axi_ctrl_status_wstrb   : in  std_logic_vector(3 downto 0);
        s_axi_ctrl_status_wvalid  : in  std_logic;
        s_axi_ctrl_status_wready  : out std_logic;
        s_axi_ctrl_status_bresp   : out std_logic_vector(1 downto 0);
        s_axi_ctrl_status_bvalid  : out std_logic;
        s_axi_ctrl_status_bready  : in  std_logic;
        s_axi_ctrl_status_araddr  : in  std_logic_vector(5 downto 0);
        s_axi_ctrl_status_arprot  : in  std_logic_vector(2 downto 0);
        s_axi_ctrl_status_arvalid : in  std_logic;
        s_axi_ctrl_status_arready : out std_logic;
        s_axi_ctrl_status_rdata   : out std_logic_vector(31 downto 0);
        s_axi_ctrl_status_rresp   : out std_logic_vector(1 downto 0);
        s_axi_ctrl_status_rvalid  : out std_logic;
        s_axi_ctrl_status_rready  : in  std_logic
   );
end top;

architecture Behavioral of top is

-- AXI in-stream signals
signal in_stream_data  : std_logic_vector(RAW_BIT_WIDTH-1 downto 0);
signal in_stream_valid : std_logic;
signal in_stream_ready : std_logic;
signal in_stream_last  : std_logic;
signal in_stream_handshake : std_logic;
signal in_stream_handshake_delay : std_logic;
signal in_raw_delay :   std_logic_vector(RAW_BIT_WIDTH-1 downto 0);


-- AXI out-stream signals
signal out_stream_data  : std_logic_vector(63 downto 0);
signal out_stream_valid : std_logic;
signal out_stream_ready : std_logic;
signal out_stream_last  : std_logic;
signal out_stream_handshake : std_logic;

-- Signals from/to b_ram_bank
signal read_enable : std_logic;
signal b_ram_out   : std_logic_vector(B_RAM_BIT_WIDTH*NUM_B_RAM-1 downto 0);
signal enable      : std_logic;
signal v_len       : std_logic_vector(11 downto 0);
signal initialized : std_logic;

-- Signals from/to dot_product_module
signal p_rdy_w  :   std_logic_vector(NUM_B_RAM-1 downto 0);
signal p_rdy    :   std_logic;
signal p_out    :   std_logic_vector(NUM_B_RAM*P_BIT_WIDTH-1 downto 0);
signal dp_extend_end : std_logic;
signal dp_enable : std_logic;


-- Signals from/to AXI gear box
signal last_p : std_logic;

begin
--Connections
in_stream_data  <= s_axis_tdata;
in_stream_valid <= s_axis_tvalid;
s_axis_tready   <= in_stream_ready; 
in_stream_last  <= s_axis_tlast;

--m_axis_tdata  <= out_stream_data;
--m_axis_tvalid <= out_stream_valid;
--out_stream_ready <= m_axis_tready;
--m_axis_tlast  <= out_stream_last;

in_stream_handshake <= '1' when (in_stream_valid = '1' and in_stream_ready = '1') else '0';
--out_stream_handshake <= '1' when (out_stream_valid = '1' and out_stream_ready = '1') else '0';

p_rdy <= '1' when p_rdy_w = (p_rdy_w'range => '1') else '0';

dp_enable <= '1' when (in_stream_handshake_delay = '1' and enable = '1' and initialized = '1') or dp_extend_end = '1' else '0';
read_enable <= '1' when (in_stream_valid = '1' and enable = '1') and initialized = '1' else '0';

process(clk, aresetn)
    variable counter : integer := 0;
begin
    if (aresetn = '0') then
        in_stream_ready <= '0';
        --out_stream_valid <= '0';
        --out_stream_data <= (others => '0');
        in_stream_handshake_delay <= '0';
        last_p <= '0';
        counter := 0;
        in_raw_delay <= (others => '0');
    elsif(rising_edge(clk)) then
        last_p <= '0';
        in_raw_delay <= in_stream_data;
        in_stream_handshake_delay <= in_stream_handshake;
        if(enable = '1' and initialized = '1') then
            in_stream_ready <= '1';
        end if;
        if(in_stream_last = '1') then
            dp_extend_end <= '1';
        elsif(dp_extend_end = '1') then
            counter := counter + 1;
            if(counter = 3) then
                last_p <= '1';
                dp_extend_end <= '0';
                counter := 0;
            end if;
        end if;
    end if;
end process;








b_ram: entity work.b_ram_bank
  Generic map(
          B_RAM_SIZE         =>  B_RAM_SIZE,
          B_RAM_BIT_WIDTH    =>  B_RAM_BIT_WIDTH,
          NUM_B_RAM          =>  NUM_B_RAM,
          C_S_AXI_DATA_WIDTH =>  C_S_AXI_DATA_WIDTH,
          C_S_AXI_ADDR_WIDTH =>  C_S_AXI_ADDR_WIDTH
       )
  Port map( 
        clk             =>  clk,
        aresetn         =>  aresetn,
        
        -- B_ram interface
        read_enable     =>  read_enable,

        data_out        =>  b_ram_out,
        v_len           =>  v_len,
        init_flag       =>  initialized,
        
        
        -- Register interface
        enable          =>  enable,
        s_axi_ctrl_status_awaddr    =>  s_axi_ctrl_status_awaddr,
        s_axi_ctrl_status_awprot    =>  s_axi_ctrl_status_awprot,
        s_axi_ctrl_status_awvalid   =>  s_axi_ctrl_status_awvalid,
        s_axi_ctrl_status_awready   =>  s_axi_ctrl_status_awready,
        s_axi_ctrl_status_wdata     =>  s_axi_ctrl_status_wdata,
        s_axi_ctrl_status_wstrb     =>  s_axi_ctrl_status_wstrb,
        s_axi_ctrl_status_wvalid    =>  s_axi_ctrl_status_wvalid,
        s_axi_ctrl_status_wready    =>  s_axi_ctrl_status_wready,
        s_axi_ctrl_status_bresp     =>  s_axi_ctrl_status_bresp,
        s_axi_ctrl_status_bvalid    =>  s_axi_ctrl_status_bvalid,
        s_axi_ctrl_status_bready    =>  s_axi_ctrl_status_bready,
        s_axi_ctrl_status_araddr    =>  s_axi_ctrl_status_araddr,
        s_axi_ctrl_status_arprot    =>  s_axi_ctrl_status_arprot,
        s_axi_ctrl_status_arvalid   =>  s_axi_ctrl_status_arvalid,
        s_axi_ctrl_status_arready   =>  s_axi_ctrl_status_arready,
        s_axi_ctrl_status_rdata     =>  s_axi_ctrl_status_rdata,
        s_axi_ctrl_status_rresp     =>  s_axi_ctrl_status_rresp,
        s_axi_ctrl_status_rvalid    =>  s_axi_ctrl_status_rvalid,
        s_axi_ctrl_status_rready    =>  s_axi_ctrl_status_rready
  );



dp: entity work.dot_product_module
    generic map(
        RAW_BIT_WIDTH   =>  RAW_BIT_WIDTH,
        G_BIT_WIDTH     =>  G_BIT_WIDTH,
        NUM_B_RAM       =>  NUM_B_RAM,
        P_BIT_WIDTH     =>  P_BIT_WIDTH
    )
    port map(
        clk             =>  clk,
        aresetn         =>  aresetn,
        en              =>  dp_enable,
        in_G            =>  b_ram_out,
        in_raw          =>  in_raw_delay,
        v_len           =>  v_len,
        p_rdy           =>  p_rdy_w,
        p_out           =>  p_out
    );

gb: entity work.axi_gearbox
    generic map(
        B_RAM_SIZE  =>  B_RAM_SIZE,
        B_RAM_BIT_WIDTH => B_RAM_BIT_WIDTH,
        NUM_B_RAM   =>  NUM_B_RAM,
        RAW_BIT_WIDTH   =>  RAW_BIT_WIDTH,
        G_BIT_WIDTH     =>  G_BIT_WIDTH,
        P_BIT_WIDTH     =>  P_BIT_WIDTH,
        C_S_AXI_DATA_WIDTH  =>  C_S_AXI_DATA_WIDTH,
        C_S_AXI_ADDR_WIDTH  =>  C_S_AXI_ADDR_WIDTH
    )
    port map(
        clk     =>      clk,
        aresetn =>      aresetn,
        p_out   =>      p_out,
        p_rdy   =>      p_rdy,
        p_int   =>      p_irq,
        last_p  =>      last_p,
        m_axis_tdata    =>  m_axis_tdata,
        m_axis_tvalid   =>  m_axis_tvalid,
        m_axis_tready   =>  m_axis_tready,
        m_axis_tlast    =>  m_axis_tlast
    );

end Behavioral;
