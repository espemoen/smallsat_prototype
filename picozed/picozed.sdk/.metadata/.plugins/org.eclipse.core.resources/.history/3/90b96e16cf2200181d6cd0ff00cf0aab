/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"

#include "xparameters.h"
#include "xgpiops.h"

#define GPIO_DEVICE_ID		XPAR_XGPIOPS_0_DEVICE_ID
#define GPIO_BANK	XGPIOPS_BANK0

static XGpioPs Gpio; /* The Instance of the GPIO Driver */



#define SPI_OE 9
#define T_EXP1 10
#define T_EXP2 11
#define Frame_Request 12
#define SYS_RES_N 13
#define UART_SPDT_SW 51

int main()
{
    init_platform();

    XGpioPs_Config *ConfigPtr;
    int Status;

    /* Initialize the Gpio driver. */
    ConfigPtr = XGpioPs_LookupConfig(GPIO_DEVICE_ID);
    if (ConfigPtr == NULL) {
    	return XST_FAILURE;
    }
    XGpioPs_CfgInitialize(&Gpio, ConfigPtr, ConfigPtr->BaseAddr);

    /* Run a self-test on the GPIO device. */
    Status = XGpioPs_SelfTest(&Gpio);
    if (Status != XST_SUCCESS) {
    	return XST_FAILURE;
    }

    //Set SPDT switch to USB
    XGpioPs_SetDirectionPin(&Gpio, UART_SPDT_SW, 1);
    XGpioPs_SetOutputEnablePin(&Gpio, UART_SPDT_SW, 1);
    XGpioPs_WritePin(&Gpio, UART_SPDT_SW, 1);

    //Output Enable SPI
    XGpioPs_SetDirectionPin(&Gpio, SPI_OE, 1);
    XGpioPs_SetOutputEnablePin(&Gpio, SPI_OE, 1);
    XGpioPs_WritePin(&Gpio, SPI_OE, 0);

    //Set Image Sensor reset low (Active LOW)
    XGpioPs_SetDirectionPin(&Gpio, SYS_RES_N, 1);
    XGpioPs_SetOutputEnablePin(&Gpio, SYS_RES_N, 1);
    XGpioPs_WritePin(&Gpio, SYS_RES_N, 0);

    //Set Frame Request low
    XGpioPs_SetDirectionPin(&Gpio, Frame_Request, 1);
    XGpioPs_SetOutputEnablePin(&Gpio, Frame_Request, 1);
    XGpioPs_WritePin(&Gpio, Frame_Request, 0);

    //Set T_EXP1 low
    XGpioPs_SetDirectionPin(&Gpio, T_EXP1, 1);
    XGpioPs_SetOutputEnablePin(&Gpio, T_EXP1, 1);
    XGpioPs_WritePin(&Gpio, T_EXP1, 0);

    //Set T_EXP2 low
    XGpioPs_SetDirectionPin(&Gpio, T_EXP2, 1);
    XGpioPs_SetOutputEnablePin(&Gpio, T_EXP2, 1);
    XGpioPs_WritePin(&Gpio, T_EXP2, 0);


    print("Hello World\n\r");


    cleanup_platform();
    return 0;
}
